import { Component, OnInit } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import {
  FormControl,
  Validators,
  NgModel,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { DailoughBoxComponent } from "../modal/dailough-box/dailough-box.component";
import { MatDialog } from "@angular/material";
import { JitService } from "../services/jit/jit.service";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import { Validatable } from "@amcharts/amcharts4/core";

@Component({
  selector: "app-vendor-mapping",
  templateUrl: "./vendor-mapping.component.html",
  styleUrls: ["./vendor-mapping.component.scss"],
})
export class VendorMappingComponent implements OnInit {
  displayedColumns: string[] = [
    "Mapping Id",
    "BP Code",
    "Master Vendor Code",
    "Sub Vendor Code",
    "Edit",
    "Delete",
  ];
  DataSource = [];
  pageSize = 200;
  Mapping: FormGroup;

  UpdatedBPCode = new FormControl("", Validators.required);
  UpdatedVendorCode = new FormControl("", Validators.required);
  UpdatedSubVendorCode = new FormControl("", Validators.required);

  // Search
  bpcode = new FormControl();
  masterVendor = new FormControl();
  subVendor = new FormControl();

  constructor(
    private commonService: CommonUtilityService,
    public dialog: MatDialog,
    private http: JitService,
    public cs: MySuggestionService,
    private fb: FormBuilder
  ) {
    this.commonService.changeIsAuthenticate(true);
  }

  ngOnInit() {
    this.Mapping = this.fb.group({
      bpcode: ["", Validators.required],
      vendorCode: ["", Validators.required],
      subVendorCode: ["", Validators.required],
    });
    this.getData(1);
  }

  getData(pageNo) {
    // console.log()
    let body = {
      BPCode: "",
      MasterVendor: "",
      SubVendor: "",
      Flag: "Get",
      PageNo: pageNo,
      MappingId: "",
      BpCodeSearch: this.bpcode.value || "",
      MstVndrSearch: this.masterVendor.value || "",
      SbVndrSearch: this.subVendor.value || "",
      UpdateFlag: ""
    };
    this.http
      .call("VendorEmployee/OperationVendorMapping", body)
      .subscribe((res) => {
        console.log(res);
        this.pageSize = res.TotalCount;
        let data = [];
        res.ResponseData.forEach((element) => {
          data.push({
            "Mapping Id": element.MappingId,
            "BP Code": element.BPCode,
            "Master Vendor Code": element.MasterVendor,
            "Sub Vendor Code": element.SubVendor,
            editing: false,
          });
        });
        this.DataSource = data;
      });
  }

  reset() {
    this.bpcode.reset();
    this.masterVendor.reset();
    this.subVendor.reset();
    this.Mapping.reset();
    this.getData(1);
  }

  edit(element) {
    this.DataSource.forEach((data) => {
      console.log(element["Mapping Id"], data["Mapping Id"]);
      if (element["Mapping Id"] != data["Mapping Id"]) data.editing = false;
      else {
        element.editing = true;
        this.UpdatedVendorCode.setValue(element["Master Vendor Code"]);
        this.UpdatedSubVendorCode.setValue(element["Sub Vendor Code"]);
        this.UpdatedBPCode.setValue(element["BP Code"]);
      }
    });
  }

  createMapping() {
    if (this.Mapping.valid) {
      let body = {
        BPCode: this.Mapping.value.bpcode,
        MasterVendor: this.Mapping.value.vendorCode.toUpperCase(),
        SubVendor: this.Mapping.value.subVendorCode.toUpperCase(),
        Flag: "Insert",
        PageNo: "0",
        MappingId: "0",
        BpCodeSearch: "",
        MstVndrSearch: "",
        SbVndrSearch: "",
        UpdateFlag: ""
      };
      console.log(body)
      this.http
      .call("VendorEmployee/OperationVendorMapping", body)
      .subscribe((res) => {
        if (res.ResponseData[0].msg == "Inserted") {
          location.reload()
          console.log(res);
          this.getData(1)
          this.cs.showSuccess('Mapping Created Successfully')
        }else{
          this.cs.showSuccess(res.ResponseData[0].msg)
        }
      })
    } else {
      this.cs.showError('Kindly Fill The Required Fields')
    }
  }

  update(element) {
    console.log(element);
    console.log(this.UpdatedBPCode.value);
    console.log(this.UpdatedVendorCode.value);
    console.log(this.UpdatedSubVendorCode.value);
    if(this.UpdatedVendorCode.value != element["Master Vendor Code"] || this.UpdatedSubVendorCode.value != element["Sub Vendor Code"] || this.UpdatedBPCode.value != element["BP Code"]){
      if(this.UpdatedSubVendorCode && this.UpdatedVendorCode){
        let body: any = {
          BPCode: this.UpdatedBPCode.value,
          MasterVendor: this.UpdatedVendorCode.value,
          SubVendor: this.UpdatedSubVendorCode.value,
          Flag: "Update",
          PageNo: "0",
          MappingId: element["Mapping Id"],
          BpCodeSearch: "",
          MstVndrSearch: "",
          SbVndrSearch: "",
          UpdateFlag: ""
        };
  
  
        // if(this.UpdatedBPCode.value != element["BP Code"]){
        //   body.UpdateFlag = "BPCode"

        //   if(this.UpdatedVendorCode.value != element["Master Vendor Code"]){
        //     body.UpdateFlag = "BPMSV"
        //     body.SubVendor = ""
        //   }
        //   if(this.UpdatedSubVendorCode.value != element["Sub Vendor Code"]){
        //     body.UpdateFlag = "BPMSV"
        //     body.MasterVendor = ""
        //   }

        //   if(this.UpdatedVendorCode.value != element["Master Vendor Code"] && this.UpdatedSubVendorCode.value != element["Sub Vendor Code"]){
        //     body.UpdateFlag = "AllUpdate"
        //   }
        // }else if(this.UpdatedVendorCode.value != element["Master Vendor Code"] || this.UpdatedSubVendorCode.value != element["Sub Vendor Code"]){
        //   body.UpdateFlag = "MSVCode"
        // }

  
        console.log(body)
        this.http
          .call("VendorEmployee/OperationVendorMapping", body)
          .subscribe((res) => {
            console.log(res);
            if (res.ResponseData[0].msg == "Updated") {
              element.editing = false;
              element["Master Vendor Code"] = this.UpdatedVendorCode.value;
              element["Sub Vendor Code"] = this.UpdatedSubVendorCode.value;
              element["BP Code"] = this.UpdatedBPCode.value;
              this.cs.showSuccess("Updated Successfully");
            } else {
              this.cs.showError(res.ResponseData[0].msg);
            }
          });
      } else {
        this.cs.showError('Kindly Provide the Mandatory Fields')
      }
    } else {
      this.cs.showError('Kindly Change the inputs to update')
    }
    // this.DataSource.findIndex(o=>o)
  }

  delete(element) {
    const dialogRef = this.dialog.open(DailoughBoxComponent, {
      width: "300px",
      height: "130px",
      data: {
        messege: "Are You Sure You Want To Delete This Record",
        element: element,
      },
    });

    dialogRef.afterClosed().subscribe((data) => {
      console.log(data);
      if (data) {
        let body = {
          BPCode: "",
          MasterVendor: "",
          SubVendor: "",
          Flag: "Delete",
          PageNo: "0",
          MappingId: element["Mapping Id"],
          BpCodeSearch: "",
          MstVndrSearch: "",
          SbVndrSearch: "",
          UpdateFlag: ""
        };
        this.http
          .call("VendorEmployee/OperationVendorMapping", body)
          .subscribe((res) => {
            if (res) {
              this.cs.showSuccess("Mapping Deleted");
              this.getData(1);
            }
          });
      }
    });
  }

  download() {
    console.log("Download");
    let body = {
      BPCode: "",
      MasterVendor: "",
      SubVendor: "",
      Flag: "Get",
      PageNo: "0",
      MappingId: "",
      BpCodeSearch: "",
      MstVndrSearch: "",
      SbVndrSearch: "",
      UpdateFlag: ""
    };
    this.http
      .call("VendorEmployee/OperationVendorMapping", body)
      .subscribe((res) => {
        res.ResponseData.forEach((element) => {
          delete element["msg"];
          delete element["TotalCount"];
        });
        console.log(res.ResponseData);
        this.cs.exportJsonAsExcelFile(res.ResponseData, "VendorMapping");
      });
  }
}
