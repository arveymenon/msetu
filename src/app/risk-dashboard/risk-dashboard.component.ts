import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LiquidityModalComponent } from '../modal/liquidity-modal/liquidity-modal.component';
import { resolve } from 'url';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as CryptoJS from 'crypto-js';
import { Workbook } from "exceljs";
import * as fs from "file-saver";

@Component({
  selector: 'app-risk-dashboard',
  templateUrl: './risk-dashboard.component.html',
  styleUrls: ['./risk-dashboard.component.scss']
})
export class RiskDashboardComponent implements OnInit {
  userid = CryptoJS.AES.decrypt(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ=='),"").toString(CryptoJS.enc.Utf8);
  all_year_data = [];
  yearDDL: any;
  vendorCode: string;
  answers_data = [];
  supplierReponses=[];
  roleId: string;

  constructor(public cs: MySuggestionService,private route: ActivatedRoute,public router:Router,public commonService: CommonUtilityService, private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer, private modalService: NgbModal) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon('arrowIcon', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Iconarrow.svg'));

  }

  loginID:any;
  ngOnInit() {
    this.loginID = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
    this.loginID = CryptoJS.AES.decrypt(this.loginID,"").toString(CryptoJS.enc.Utf8);

    this.getAllyear().then(()=>{
      this.route.queryParams.subscribe(params => {
        if(params["VC"]){
          this.vendorCode = params["VC"];
          this.yearDDL = params["FY"]
          this.vendorCode = atob(this.vendorCode)
          this.GetSupplierAnswerReport(this.yearDDL)
        }
        else{
          this.vendorCode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
          this.vendorCode = CryptoJS.AES.decrypt(this.vendorCode,"").toString(CryptoJS.enc.Utf8);
        } 
    });
    this.roleId = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8)

    });
    let analysticsRequest={
      "userClicked":this.userid,//  localStorage.getItem('userToken'),
      "device": "windows",
      "browser": localStorage.getItem('browser'),
      "moduleType": "Risk",
      "module": "RiskDashboard"
    }
   this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})
  }
  openRiskModal(content) {
    this.modalService.open(content, { centered: true, windowClass: "formModal" });
  }
  closeModal(){
    this.modalService.dismissAll();
  }
  downloadRisk(){
    let dataNew = this.answers_data;
    dataNew.splice(13);
    dataNew.splice(14);
    // dataNew.push('Overall Category', this.overAllRating)
 
  
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("Risk Data");
    //Add Header Row
    // console.log(this.COPUploadisplayedColumns)
    let data = ["Category", "Risk Score"]
    let header = Object.keys(data);

    let headerRow = worksheet.addRow(data);
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "00FF0000" },
        bgColor: { argb: "00FF0000" },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };


    });
    dataNew.forEach((d) => {
      var values = Object.keys(d).map(function (key) {
        return d[key];
      });

    

      console.log(values);
      let row = worksheet.addRow(values);
      let qty = row.getCell(2);
      let color = 'FF99FF99';
      if (+qty.value <= 2.9) {
        color = 'FF9999'
      }
      else if (+qty.value <= 1.9) {
        color = '00FF0000'
      }
      else if (+qty.value <= 3.9) {
        color = 'ff6600'
      }
      else if (+qty.value <= 4.9) {
        color = '66ff33'
      }
     else if (+qty.value >=5) {
        color = '008000'
      }

      qty.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: color }
      }
    });
    workbook.xlsx.writeBuffer().then((dataNew) => {
      let blob = new Blob([dataNew], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(blob, "Risk_reportData.xlsx");
    });
  
  }
  openModal(Category) {
    let Category_Wise_Data=[];
    this.supplierReponses.filter(x=>{
      if(x.Category_Name == Category){
        Category_Wise_Data.push(x);
      }
    })
    let modal = this.modalService.open(LiquidityModalComponent, { size: 'xl' as 'lg' });
    modal.componentInstance.LiquidityData = Category_Wise_Data;
    modal.componentInstance.modalTitle = Category;
    console.log(Category)
  }


  getAllyear(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.postMySuggestionData('SupplierAnswers/GetAllYears', '').subscribe((res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.all_year_data = res;
          resolve();
        }
        else {
          this.cs.showError("Failed to load year");
          return;
        }
      })
    })
  }
  overAllRating:any;
  GetSupplierAnswerReport(year_value): Promise<any> {
    return new Promise((resolve: any) => {
      // this.roleId = localStorage.getItem('WTIwNWMxcFZiR3M9');
      // this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8)
      let roleId = localStorage.getItem('PR');
      roleId = CryptoJS.AES.decrypt(roleId,"").toString(CryptoJS.enc.Utf8)
      let Request = {
        "SupplierCode": this.vendorCode, //"DA030A",
        "SchID": year_value,// "FY 2016",// ,
        "IsAdmin": true,
        // "LoginId": roleId =='7' ? '' : this.loginID
        "LoginId" :""
      }
            this.cs.postMySuggestionData('SupplierAnswers/GetSupplierAnswerReport', Request).subscribe((res: any) => {
        if (!this.cs.isUndefinedORNull(res.ResponseData) && res.Message == 'Success') {
          this.answers_data = res.ResponseData.supplierAnswers;
          this.supplierReponses = res.ResponseData.supplierReponses;
          this.overAllRating = res.ResponseData.supplierOverallAnswers[0].Risk;
          resolve();
        }
        else {  
          this.cs.showError("Failed to load data");
          return;
        }
      })
    })
  }

  navigate(){
    if(this.roleId == '7'){
      this.router.navigateByUrl('adminDashboard');
    }else{
      this.router.navigateByUrl('dashboard');
    }
  }

}
