import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { Modal } from '@amcharts/amcharts4/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlasticWasteManagementComponent } from '../modal/plastic-waste-management/plastic-waste-management.component';
import { NotificationOnPlasticUsageComponent } from '../modal/notification-on-plastic-usage/notification-on-plastic-usage.component';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { MySurveyModalComponent } from '../modal/my-survey-modal/my-survey-modal.component';
import { MyPolicyGuidelinesService } from '../services/myPolicyGuideLines/my-policy-guidelines.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import * as CryptoJS from 'crypto-js';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';

@Component({
  selector: 'app-my-policy-guidelines',
  templateUrl: './my-policy-guidelines.component.html',
  styleUrls: ['./my-policy-guidelines.component.scss']
})
export class MyPolicyGuidelinesComponent implements OnInit {
  userid = CryptoJS.AES.decrypt(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ=='),"").toString(CryptoJS.enc.Utf8);
  roleId = localStorage.getItem('WTIwNWMxcFZiR3M9')
  guideLines = [];
  
  constructor(public datepipe: DatePipe,private  router:Router,public domSanitizer: DomSanitizer, public matIconRegistry: MatIconRegistry,
    public modal: NgbModal,
    public cs: MySuggestionService,
    public commonService: CommonUtilityService,
    public myPolicyGuidelinesService: MyPolicyGuidelinesService) {
      matIconRegistry.addSvgIcon('arrowRightRed',domSanitizer.bypassSecurityTrustResourceUrl('assets/Icons/policyGuideLines/Arrow_right_red.svg'));
      this.commonService.changeIsAuthenticate(true)
      window["MyPolicyGuidelinesComponent"] = this;

      this.myPolicyGuidelinesService.getGuidelines('DocumentManagement/Get_DocumentManagementList').subscribe((res: any)=>{
        console.log(res);
        for(let doc of res.ResponseData.MSDOC){
          if(doc.Parent_Id == null ||doc.Parent_Id == 0){
            this.guideLines.push(doc);
            console.log(this.guideLines)
          }
        }
      })
  }

  ngOnInit() {
    this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8)

    let analysticsRequest={
      "userClicked":this.userid,//  localStorage.getItem('userToken'),
      "device": "windows",
      "browser": localStorage.getItem('browser'),
      "moduleType": "Document",
      "module": "MyPolicy"
    }
   this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})
  }

  myPolicyGuideLinesLinks(doc){
    if(doc.File_Type == "html"){
      let modalRef = this.modal.open(PlasticWasteManagementComponent,{ size: 'xl' as 'lg' });
      modalRef.componentInstance.page_data = doc.Html_data
    }

    if(doc.File_Type != "html"){
        window.open(doc.Doc_Link,'_blank')
    }
  }

  viewPlasticWasteManagement(){
    console.log('Waste Managment');
    this.modal.open(PlasticWasteManagementComponent,{ size: 'xl' as 'lg' });
  }
  
  viewNotificationOnPlasticUsage(){
    this.modal.open(NotificationOnPlasticUsageComponent,{ size: 'xl' as 'lg' });
  }
  
  mySurveyModal(){
    this.modal.open(MySurveyModalComponent,{ size: 'xl' as 'lg' });
  }
  
  closeModal(){
    this.modal.dismissAll();
  }
  conductSurvey(val){
    this.router.navigateByUrl(val);
  }
  
  supplierCodeOfConduct(){
    window.open('https://supplier.mahindra.com/sites/supplierportal/SiteAssets/Supplier%20Code%20of%20Conduct/Supplier%20Code%20of%20Conduct.pdf','_blank')
  }

  termsAndCondition(){
    window.open('https://supplier.mahindra.com/sites/supplierportal/Documents/PO%20Terms-%20%20GST_Final_Clean.pdf','_blank')
  }

}
