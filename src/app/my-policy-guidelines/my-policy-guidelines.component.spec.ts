import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPolicyGuidelinesComponent } from './my-policy-guidelines.component';

describe('MyPolicyGuidelinesComponent', () => {
  let component: MyPolicyGuidelinesComponent;
  let fixture: ComponentFixture<MyPolicyGuidelinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPolicyGuidelinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPolicyGuidelinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
