import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { MyPolicyGuidelinesService } from '../services/myPolicyGuideLines/my-policy-guidelines.service';
import * as moment from 'moment';
import * as CryptoJS from 'crypto-js';
import { Workbook } from 'exceljs';
import * as fs from "file-saver";

@Component({
  selector: 'app-source-code-conduct-list',
  templateUrl: './source-code-conduct-list.component.html',
  styleUrls: ['./source-code-conduct-list.component.scss']
})
export class SourceCodeConductListComponent implements OnInit {
  // DataSource = [
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Digital Signature Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   {supplierName: 'Amit Sharma', SupplierDesignation: 'ProDuct Developer', year: '2020',type:'Scan Documents', status: 'Approve'},
  //   ];
  displayedColumns: string[] = ['supplierCode','supplierName', 'SupplierDesignation', 'year', 'type', 'status'];
  DataSource: [];
  pageNo: any;
  pageSize: any;
  startDate: any=null;
  endDate: any=null;
  submittedStatus: any = 0;
  date = new Date();
  constructor(public myPolicyGuidelinesService: MyPolicyGuidelinesService, private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer, public commonService: CommonUtilityService) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon('excelIcon', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/Excel_White_Icon.svg'));
    matIconRegistry.addSvgIcon('trash', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/delete.svg'));
    matIconRegistry.addSvgIcon('edit', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/Edit.svg'));

  }

  roleId: any = localStorage.getItem('WTIwNWMxcFZiR3M9');

  ngOnInit() {
    this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8);
    this.getCOCList({ pageIndex: 0, pageSize: 10, length: this.pageSize });
  }
  getPageChangeData(event) {
    this.getCOCList(event);
  }
  start:any=null;
  end:any=null;
  Json:any=[];
  getCOCList(event) {
    // this.pageNo = event.pageIndex;
    let startDate=null;
    let endDate=null;
    // alert(this.startDate+"---"+this.endDate)
    if(this.startDate !=null){
       startDate = moment(this.startDate).format('DD-MMM-YYYY');
       this.start =startDate;
    }
    if(this.endDate !=null){
      endDate = moment(this.endDate).format('DD-MMM-YYYY');
      this.end = endDate;
   }
    this.pageNo = event.pageIndex;
    let reqjson =
    {
      "PageNo": this.pageNo + 1,
      "FromDate": startDate,
      "ToDate": endDate,
      "IsSubmitted": this.submittedStatus
    }
    this.myPolicyGuidelinesService.getSurveyList(reqjson).subscribe((resp: any) => {
      this.DataSource = resp.ResponseData;
      this.pageSize = resp.Count;
    }, (err) => {

    });
  }
  downloadExcel() {
    let reqjson =
    {
      "PageNo"  : 0,
      "FromDate": this.start,
      "ToDate": this.end,
      "IsSubmitted": this.submittedStatus
    }
    this.myPolicyGuidelinesService.getSurveyList(reqjson).subscribe((response: any) => {
      this.Json = response.ResponseData;
      let dataNew = this.Json;
      console.log(dataNew);
      //Create workbook and worksheet
      let workbook = new Workbook();
      let worksheet = workbook.addWorksheet("ReportData");
      let columns = Object.keys(this.Json[0]);
      let headerRow = worksheet.addRow(columns);
      // Cell Style : Fill and Border
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: "00FF0000" },
          bgColor: { argb: "00FF0000" },
        };
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
      });
      dataNew.forEach((d) => {
        // var obj = JSON.parse(d);
        var values = Object.keys(d).map(function (key) {
          return d[key];
        });
        let row = worksheet.addRow(values);
      });
      workbook.xlsx.writeBuffer().then((dataNew) => {
        let blob = new Blob([dataNew], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        fs.saveAs(
          blob,
          "COCData" +".xlsx"
        );
      });
    }, (err) => {

    });
  }

  ResetData() {
    this.startDate = null;
    this.endDate = null;
    this.submittedStatus = 0;
  }
}
