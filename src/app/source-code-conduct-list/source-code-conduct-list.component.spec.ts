import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceCodeConductListComponent } from './source-code-conduct-list.component';

describe('SourceCodeConductListComponent', () => {
  let component: SourceCodeConductListComponent;
  let fixture: ComponentFixture<SourceCodeConductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceCodeConductListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceCodeConductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
