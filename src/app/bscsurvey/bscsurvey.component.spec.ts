import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BSCSurveyComponent } from './bscsurvey.component';

describe('BSCSurveyComponent', () => {
  let component: BSCSurveyComponent;
  let fixture: ComponentFixture<BSCSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BSCSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BSCSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
