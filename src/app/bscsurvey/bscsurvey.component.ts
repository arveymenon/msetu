import { Component, OnInit, ViewChild } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import {
  FormArray,
  FormControl,
  FormBuilder,
  Validators,
  FormGroup,
} from "@angular/forms";
import { BscSurveyService } from "../services/bsc-survey/bsc-survey.service";
import { ToastrService } from "ngx-toastr";
import * as moment from "moment";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import {
  NativeDateAdapter,
  DateAdapter,
  MAT_DATE_FORMATS,
} from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS } from "../cmsdashboard/date-format";
import { DataServiceService } from "../services/DataService/data-service.service";
import * as CryptoJS from "crypto-js";
import { Workbook } from 'exceljs';
import * as fs from "file-saver";
@Component({
  selector: "app-bscsurvey",
  templateUrl: "./bscsurvey.component.html",
  styleUrls: ["./bscsurvey.component.scss"],
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: APP_DATE_FORMATS,
    },
  ],
})
export class BSCSurveyComponent implements OnInit {

  totalCount = 0

  roleId = CryptoJS.AES.decrypt(
    localStorage.getItem("WTIwNWMxcFZiR3M9"),
    ""
  ).toString(CryptoJS.enc.Utf8);
  userToken = CryptoJS.AES.decrypt(
    localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),
    ""
  ).toString(CryptoJS.enc.Utf8);
  vendorCode = CryptoJS.AES.decrypt(
    localStorage.getItem("WkcxV2RWcEhPWGxSTWpscldsRTlQUT09"),
    ""
  ).toString(CryptoJS.enc.Utf8);
  username = "";
  noSurveyAvailable = true;

  surveyDate = new FormControl();
  public dynamicForm = new FormArray([]);
  dataSource = [];
  displayedColumns: string[] = [
    "srNo",
    "Question",
    "unitMeasurement",
    "Data",
    "tokenImprovement",
    "Remarks",
  ];

  hubs = [];
  sectors = [];
  suppliers = [];
  DashboardDataSource = [];

  dashboardTable = ["SupplierName", "SupplierCode", "Hub", "Sector"];

  surveyBody: any = {};

  masterForm: FormGroup;

  type = new FormControl();
  hub = new FormControl();

  sector = new FormControl();
  supplier = new FormControl();

  dataReport = [];
  displayedColumns2: string[] = [
    "Parameter",
    "unit_of_measurements",
    "2018_2019",
    "targets_2019_2020",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sept",
    "Oct",
    "Nov",
    "Dec",
    "Jan",
    "Feb",
    "Mar",
    "cumulative",
    "BSC_score",
    "measurement_taken_for_improvement",
    "Remarks",
  ];
  isDashboard: {};

  constructor(
    public commonService: CommonUtilityService,
    public dataService: DataServiceService,
    public bscsurveyService: BscSurveyService,
    public toastr: ToastrService,
    public cs: MySuggestionService,
    public formBuilder: FormBuilder
  ) {
    this.commonService.changeIsAuthenticate(true);
    console.log(this.vendorCode);
    // this.dynamicForm.get()

    this.dynamicForm.valueChanges.subscribe((res) => {
      console.log(res);
      res.valueChanges.subscribe((res2) => {
        console.log(res2);
      });
    });
  }

  checkIfRoleExist(roles_string, role){
    if(roles_string.includes(role)){
      return true
    }else{
      return false
    }
  }

  ngOnInit() {
    this.surveyDate.valueChanges.subscribe((res) => {
      console.log(res);
      this.getSurveyForTheDate(res);
    });

    this.getSectorsAndSuppliers();

    let analysticsRequest={
      "userClicked":this.userToken,//  localStorage.getItem('userToken'),
      "device": "windows",
      "browser": localStorage.getItem('browser'),
      "moduleType": "Survey",
      "module": "BSC"
    }
   this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})
  }

  

  resetDashboardReport(){
    this.type.reset()
    this.hub.reset()
    this.sector.reset()
    this.supplier.reset()
    this.DashboardDataSource = []
  }

  getDashboardReport() {
    console.log("Show Report");
    // {   "ReportType":"1","SupplierCode":null,"Hub":"Igatpuri","Sector":"AD" }
    let body = {
      ReportType: this.type.value,
      Hub: this.hub.value,
      Sector: this.sector.value,
      SupplierCode: this.supplier.value,
    };
    console.log(body);
    this.DashboardDataSource = []
    this.bscsurveyService
      .getOpenSurvey("BSCSurvey/Get_BSC_DashBoard", body)
      .subscribe((res: any) => {
        console.log(res);
        this.totalCount = res.ResponseData.length

        this.DashboardDataSource = [...res.ResponseData];
      });
  }

  getSectorsAndSuppliers() {
    // Sector
    this.bscsurveyService
      .getOpenSurvey("BSCSurvey/Get_BSC_Sector_Supplier", {
        SupplierCode: "",
        Flag: 15,
      })
      .subscribe((res: any) => {
        this.sectors = res.ResponseData;
      });

    // Sector
    this.bscsurveyService
      .getOpenSurvey("BSCSurvey/Get_BSC_Sector_Supplier", {
        SupplierCode: "",
        Flag: 7,
      })
      .subscribe((res: any) => {
        this.hubs = res.ResponseData;
      });

    // Supplier
    this.bscsurveyService
      .getOpenSurvey("BSCSurvey/Get_BSC_Sector_Supplier", {
        SupplierCode: "",
        Flag: 5,
      })
      .subscribe((res: any) => {
        this.suppliers = res.ResponseData;
        let exists = res.ResponseData.findIndex(
          (o) => o.SupplierCode == this.vendorCode
        );

        if (exists == -1) {
          this.noSurveyAvailable = true;
        } else {
          this.username = res.ResponseData[exists].SupplierName;
          this.supplier.setValue(this.vendorCode);
          this.noSurveyAvailable = false;
        }
      });
  }

  getSuppliers(value) {
    console.log(value);
    this.bscsurveyService
      .getOpenSurvey("BSCSurvey/Get_BSC_Sector_Supplier", {
        SupplierCode: "",
        Flag: 5,
      })
      .subscribe((res: any) => {
        this.suppliers = res.ResponseData.filter((o) => o.sector == value);
      });
  }

  @ViewChild("BSCReportTable") BSCReportTable;
  download(table) {
    let dataNew = this.dataReport;
    console.log(dataNew);
    let workbook = new Workbook();
      let worksheet = workbook.addWorksheet("ReportData");
      let columns = Object.keys(this.dataReport[0]);
      columns.splice(2, 0, columns.splice(0, 1)[0]);
      columns[2] = '2019';
      columns[3] = 'Targets 2020-2021';
      // columns.splice(0,1)
      let headerRow = worksheet.addRow(columns);
      // Cell Style : Fill and Border
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: "00FF0000" },
          bgColor: { argb: "00FF0000" },
        };
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
      });
      dataNew.forEach((d) => {
        // var obj = JSON.parse(d);
        var values = Object.keys(d).map(function (key) {
          return d[key] || 0;
        });
        values.splice(2, 0, values.splice(0, 1)[0]);
        values[17] = values[17] == 'true' ? '1' : values[17] == 'false' ? '0' : values[17]
        let row = worksheet.addRow(values);
      });
      workbook.xlsx.writeBuffer().then((dataNew) => {
        let blob = new Blob([dataNew], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        fs.saveAs(blob, "BSCReportData.xlsx");
      });
    // this.cs.exportAsExcelFile(table, "BSC Report");
  }
  resetBSCReport() {
    this.sector.reset();
    this.supplier.reset();
    this.dataReport.length = 0;
    this.dataReport.splice(0, 1);
    console.log(this.dataReport.length);
  }

  showReport() {
    var body = { SupplierCode: this.supplier.value || this.vendorCode };
    this.bscsurveyService
      .getOpenSurvey("BSCSurvey/BSCReport", body)
      .subscribe((res: any) => {
        console.log("Showing Report", res);
        let dataSource = [];
        for (const data of res.ResponseData) {
          dataSource.push({
            Parameter: data.Parameter,
            unit_of_measurements: data.UnitOfMeasurement,
            2018_2019: data.PreYear,
            targets_2019_2020: data.CurYear,
            Apr: data.Apr,
            May: data.May,
            Jun: data.Jun,
            Jul: data.Jul,
            Aug: data.Aug,
            Sept: data.Sept,
            Oct: data.Oct,
            Nov: data.Nov,
            Dec: data.Dec,
            Jan: data.Jan,
            Feb: data.Feb,
            Mar: data.Mar,
            cumulative: data.Cumulative,
            BSC_score: data.BSCScore,
            measurement_taken_for_improvement:
            data.MeasuresTakenForImporve || "N/A",
            Remarks: data.Remarks || "N/A",
          });
        }
        dataSource.push({
          Parameter: " ",
          unit_of_measurements: " ",
          2018_2019: "-",
          targets_2019_2020: " ",
          Apr: " ",
          May: " ",
          Jun: " ",
          Jul: " ",
          Aug: " ",
          Sept: " ",
          Oct: " ",
          Nov: " ",
          Dec: " ",
          Jan: " ",
          Feb: " ",
          Mar: " ",
          cumulative: "Total BSC Score",
          BSC_score: this.getscore(res.ResponseData),
          measurement_taken_for_improvement:
          " ",
          Remarks: " ",
        },{
          Parameter: " ",
          unit_of_measurements: " ",
          2018_2019: " ",
          targets_2019_2020: " ",
          Apr: " ",
          May: " ",
          Jun: " ",
          Jul: " ",
          Aug: " ",
          Sept: " ",
          Oct: " ",
          Nov: " ",
          Dec: " ",
          Jan: " ",
          Feb: " ",
          Mar: " ",
          cumulative: "% BSC Score",
          BSC_score: this.getscore(res.ResponseData, true),
          measurement_taken_for_improvement:
          " ",
          Remarks: " ",
        })
        this.dataReport = [...dataSource];
        console.log(this.dataReport);
      });
  }

  getscore(dataSource, flag?: any){
    let total = 0;
    dataSource.forEach(data => {
      total = total + (data.BSCScore == 'true' ? 1 : 0) ; 
    });
    return total*(flag? 12.5: 1)
  }

  getSurveyForTheDate(date?: any) {
    this.dataSource = [];
    this.dynamicForm = new FormArray([]);

    let data: any = {};
    data.fromDate = date
      ? moment(date).format("DD-MMM-YYYY")
      : moment().format("DD-MMM-YYYY");
    data.flag = 1;

    // CHECK IF Survey is available for today
    this.bscsurveyService
      .getOpenSurvey("BSCSurvey/GetOpenSurvey", data)
      .subscribe((res: any) => {
        console.log(res);
        if (res.Message == "Success" && res.ResponseData.length > 0) {
          // get Stored Survey
          this.surveyBody.ScheduleId = res.ResponseData[0].ScheduleId;
          const Form = localStorage.getItem("bscSurveyResponse");
          const storedForm = JSON.parse(Form);

          // Get Survey Questions
          this.bscsurveyService
            .getSurveyQuestion("BSCSurvey/GetSurveyQuestion")
            .subscribe((res2: any) => {
              console.log(res2);
              if (res2.Message == "Success") {
                for (let question in res2.ResponseData) {
                  this.dataSource.push({
                    srNo: res2.ResponseData[question].SrNo,
                    QuestionId: res2.ResponseData[question].QuestionId,
                    Question: res2.ResponseData[question].Question,
                    unitMeasurement:
                      res2.ResponseData[question].UnitOfMeasurement,
                    Data: "",
                    tokenImprovement: "",
                    active: res2.ResponseData[question].Active,
                    readOnly: res2.ResponseData[question].ReadOnly,
                    Remarks: "",
                  });

                  // console.log(this.dataSource[data]);

                  // If srNo. is 2.3 we divide data of srNo. 2 by srNo. 3 and disable srNo. 2.3
                  console.log(this.dataSource[question].srNo.split("."));
                  let readOnly =
                    this.dataSource[question].srNo.split(".").length > 1
                      ? true
                      : false;

                  // Currently Checking against the iteration. A unique Identifier Required.
                  if (storedForm && storedForm[question]) {
                    var form = this.formBuilder.group({
                      QuestionId: [this.dataSource[question].QuestionId],
                      SrNo: [this.dataSource[question].srNo],
                      Answer: [storedForm[question].Answer],
                      Measures: [storedForm[question].Measures],
                      Remarks: [storedForm[question].Remarks],
                    });
                  } else {
                    var form = this.formBuilder.group({
                      QuestionId: [this.dataSource[question].QuestionId],
                      SrNo: [this.dataSource[question].srNo],
                      Answer: ["", Validators.required],
                      Measures: [],
                      Remarks: [],
                    });
                  }
                  // this.dynamicForm.controls[0].valueChanges.subscribe(res=>{
                  //   console.log(res);
                  // })

                  this.dynamicForm.push(form);

                  if (readOnly) {
                    let srNos = this.dataSource[question].srNo.split(".");
                    let index = [];
                    if (this.dynamicForm.value[srNos[1]]) {
                      index.push(
                        this.dynamicForm.value.findIndex(
                          (o) => o.SrNo == [srNos[0]]
                        )
                      );
                      index.push(
                        this.dynamicForm.value.findIndex(
                          (o) => o.SrNo == [srNos[1]]
                        )
                      );

                      console.log(index);
                      for (let ind of index) {
                        this.dynamicForm.controls[ind].valueChanges.subscribe(
                          (res) => {
                            console.log(res);

                            if (
                              this.dynamicForm.controls[index[0]].value
                                .Answer &&
                              this.dynamicForm.controls[index[1]].value.Answer
                            ) {
                              let value =
                                this.dynamicForm.controls[index[0]].value
                                  .Answer /
                                this.dynamicForm.controls[index[1]].value
                                  .Answer;
                              this.dynamicForm.controls[question].controls[
                                "Answer"
                              ].setValue(value);
                            }else{
                                this.dynamicForm.controls[question].controls[
                                  "Answer"
                                ].setValue(0);
                            }
                          }
                        );
                      }
                    }
                  }
                }

                console.log(this.dataSource);
                // this.masterForm = this.formBuilder.group({
                //   providers: this.dynamicForm,
                // });

                // this.masterForm.get('provides').valueChanges.subscribe(res=>{
                //     console.log(res)
                //   })
                // this.masterForm.get.providers.valueChanges.subscribe(res=>{
                //   console.log(res)
                // })
              }
            });
        } else {
          console.log("No Surveys Available");
          this.toastr.error("No Surveys Available");
        }
      });
  }

  tabChange(){
    console.log('tab changed')
    // this.surveyDate.reset()
    this.type.reset();
    this.hub.reset();
    this.sector.reset();
    if(this.roleId == 7){
      this.supplier.reset();
    } else {
      this.supplier.setValue(this.vendorCode);
      this.showReport()
    }
    this.dataReport = []
    this.dataSource = []
    this.totalCount = 0
    this.DashboardDataSource = []
    this.dynamicForm.setValue([]);
    this.masterForm.reset();
  }

  isReadOnly(value): Boolean {
    if (value) {
      return value.split(".").length > 1;
    }
  }

  submit() {
    console.log(this.dynamicForm.value);
    if (this.dynamicForm.valid) {
      // localStorage.setItem("bscSurveyResponse", null);
      // this.dynamicForm.reset();
      this.surveyBody.SupplierCode = this.vendorCode; // localStorage.getItem("vendorCode");
      this.surveyBody.SupplierName = this.username; // localStorage.getItem("userToken");
      this.surveyBody.listSurveys = this.dynamicForm.value;
      this.bscsurveyService
        .insertSurvey("BSCSurvey/InsertSurvey", this.surveyBody)
        .subscribe((res) => {
          console.log(res);
          if (res.Message == "Success") {
            this.dynamicForm.reset();
            if(res.ResponseData[0] == "Survey Already Exist"){
              this.toastr.error("Survey Already Exist")
            } else {
              this.cs.showSuccess("Survey Submitted successfully");
            }
          } else {
            this.cs.showError("Some Error Occured");
          }
        });
    } else {
      this.cs.showError("Please fill all mandatory details and all questions");
    }
  }
  resetBSC() {
    this.dynamicForm.reset();
  }
  async saveAsDraft() {
    // JSON.store
    localStorage.setItem(
      "bscSurveyResponse",
      JSON.stringify(this.dynamicForm.value)
    );
    console.log(localStorage.getItem("bscSurveyResponse"));
    this.toastr.success("Response Saved Successfully");
  }
}
