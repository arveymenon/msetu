import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-under-maintainance',
  templateUrl: './under-maintainance.component.html',
  styleUrls: ['./under-maintainance.component.sass']
})
export class UnderMaintainanceComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
