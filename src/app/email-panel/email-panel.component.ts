import { Component, OnInit } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { ToastrService } from "ngx-toastr";
import { EmailPanelService } from "../services/emailPanel/email-panel.service";
import { FormControl, Validators } from "@angular/forms";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import * as XLSX from "xlsx";
import { Router } from "@angular/router";
import * as CryptoJS from 'crypto-js';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Workbook } from "exceljs";
import * as fs from "file-saver";
import { MatIconRegistry } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: "app-email-panel",
  templateUrl: "./email-panel.component.html",
  styleUrls: ["./email-panel.component.scss"],
})
export class EmailPanelComponent implements OnInit {
  roleId;
  outputMessage = "";
  files = [];
  subject = new FormControl('', Validators.required);
  message = new FormControl(``);

  excel_headers = ["to", "cc", "name"];
  toBeSentExcel: any;
  logDisplayedColumns: string[] = [
    "SearchField",
    "Link",
    "Description",
    "status"
  ];
  emailLogDisplayedColumns:string[] = [
    "SearchField",
    "Link",
    "cc",
    "body",
    "createdby",
    "Description",
    "attachment",
    "status",
  
  ];
  logDataSource:any=[];

  // Edit Configuration
  config = {
    uiColor: '#ffffff',
    toolbarGroups: [
    // { name: 'clipboard', groups: ['clipboard', 'undo'] },
    // { name: 'editing', groups: ['find', 'selection', 'spellchecker'] },
    // { name: 'links' },
    // { name: 'insert' },
    // { name: 'document', groups: ['mode', 'document', 'doctools'] },
    { name: 'basicstyles', groups: ['basicstyles'] },
    { name: 'paragraph', groups: ['list', 'align'] },
    { name: 'styles' },
    { name: 'colors' }],
    skin: 'kama',
    // { name: 'basicstyles', groups: ['basicstyles'] },
    // { name: 'paragraph', groups: ['list', 'align'] },
    // { name: 'colors' }],

    resize_enabled: true,
    removePlugins: 'elementspath,save,magicline',
    extraPlugins: 'divarea,smiley,justify,indentblock,colordialog',
    colorButton_foreStyle: {
       element: 'font',
       attributes: { 'color': '#(color)' }
    },
    height: 30,
    removeDialogTabs: 'image:advanced;link:advanced',
    removeButtons: 'Subscript,Superscript,Anchor,Source,Table',
    format_tags: 'p;h1;h2;h3;pre;div'
 }

  constructor(
    public commonService: CommonUtilityService,
    public toast: ToastrService,
    public cs: MySuggestionService,
    public modal: NgbModal,
    public emailPanelService: EmailPanelService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public router: Router
  ) {
    matIconRegistry.addSvgIcon(
      "excelWhiteIcon ",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    this.commonService.changeIsAuthenticate(true);
  }
  username:any

  ngOnInit() {
    this.username = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==')
    this.username = CryptoJS.AES.decrypt(this.username,"").toString(CryptoJS.enc.Utf8);
    this.roleId = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8);
  }
  openDoc(url) {
    window.open(url,"_blank");
  }
  mesageOutput(ev) {
    console.log(ev);
    this.outputMessage = ev.srcElement.value;
  }

  uploadSendingExcel(ev) {
    var headers = [];
    for (let file of ev.target.files) {
      let ext = file.name.split(".").pop().toLowerCase();
      let reader = new FileReader();
      reader.onload = (e: any) => {
        if (ext != "xlsx") {
          alert("Kindly select excel file");
          return false;
        }
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: "binary" });
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        var range = XLSX.utils.decode_range(ws["!ref"]);
        var C,
          R = range.s.r;
        let excel_headers = this.excel_headers;
        excel_headers = excel_headers.map((v) =>
          v.toLowerCase().replace(/ /g, "")
        );
        for (C = range.s.c; C <= range.e.c; ++C) {
          var cell = ws[XLSX.utils.encode_cell({ c: C, r: R })];
          var hdr = "UNKNOWN " + C;
          if (
            cell &&
            cell.t &&
            excel_headers.includes(cell.v.toLowerCase().replace(/ /g, ""))
          )
            hdr = XLSX.utils.format_cell(cell);
          else {
            alert("Kindly refer/used sample template and try to upload");
            return false;
          }
          // headers.push(hdr.toLowerCase().replace(/ /g, ""));
          headers.push(hdr);
        }
        this.Upload(ev.target.files[0], ev, headers);
      };
      reader.readAsBinaryString(ev.target.files[0]);
    }
    return false
  }
  closeModal(){
    this.modal.dismissAll();
  }
  Upload(data, evt, headers) {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      let arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i)
        arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
      let array = [];
      array = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      if (array.length == 0)
        return this.cs.showError("Kindly fill data in excel");
      else this.validateExcel(array, evt, headers);
    };
    fileReader.readAsArrayBuffer(data);
  }

  validateExcel(array, ev, headers) {
    // for (let i = 0; i < this.excel_headers.length; i++) {
      for (let j = 0; j < array.length; j++) {
        // if (i == 0) {
          if (
            this.cs.isUndefinedORNull(array[j][headers[0]]) &&
            this.cs.isUndefinedORNull(array[j][headers[1]])
          ) {
            console.log(array[j])
            alert(
              "Please enter Email under 'to' or 'cc' at line number " + (j + 2)
            );
            return false;
          }
        // }

        if (
          ((array[j][headers[0]]  && !array[j][headers[0]].toLowerCase().match("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$")) ||
           (array[j][headers[1]] && !array[j][headers[1]].toLowerCase().match("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$")))
        ) {
          alert(
            "Please valid email at line number " +
              (j + 2)
          );
          return false;
        }
        // if(!this.cs.isUndefinedORNull(array[j][headers[i]])&&headers[i]=="Mobile Number"){
        //   if(array[j][headers[i]].includes(/^[^a-zA-Z]$/)){
        //     alert("Please enter only numbers under " + this.excel_headers[i] + " at line number " + (j + 2));
        //     return false;
        //   }
        // }
      // }
    }
    this.toBeSentExcel = ev.target.files[0];
    // this.Excel_File = [];
    // this.form.resetForm();
    // this.createUser.get('upload_excel').setValue(evt.target.files[0].name);
    // this.Excel_File.push(evt.target.files[0]);
  }

  downloadExcelTemplate() {
    window.open("../../assets/mailerTemplate.xlsx", "_blank");
  }
  logType:any='MassMail';
  changeType(){
    this.callMailLogData({pageIndex: 1,pageSize: 10,length: this.pageSize});
  }
openModal(content) {
  this.modal.open(content, {
    size: "xl" as "lg",
    centered: true,
    windowClass: "formModal",
  });
  this.callMailLogData({pageIndex: 1,pageSize: 10,length: this.pageSize});
}
pageSize:any;

callMailLogData(event){
  let reqjson ={
    "PageNo" : event.pageIndex,
    "Type"   : this.logType
  }
  this.emailPanelService
  .call("MaskMailerAdmin/GetMassMailerLists", reqjson)
  .subscribe((response) => {
    console.log(response);
    this.logDataSource = response.ResponseData;
    this.pageSize = response.TotalCount;
  });
}
Json:any=[];
downloadExcel() {
  let reqjson ={
    "PageNo" : 0,
    "Type"   : this.logType
  }
  this.emailPanelService
  .call("MaskMailerAdmin/GetMassMailerLists", reqjson)
  .subscribe((response) => {
    console.log(response);
    this.Json = response.ResponseData;
    let dataNew = this.Json;
    console.log(dataNew);
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("ReportData");
    let columns = Object.keys(this.Json[0]);
    let headerRow = worksheet.addRow(columns);
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "00FF0000" },
        bgColor: { argb: "00FF0000" },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
    });
    dataNew.forEach((d) => {
      // var obj = JSON.parse(d);
      var values = Object.keys(d).map(function (key) {
        return d[key];
      });
      let row = worksheet.addRow(values);
    });
    workbook.xlsx.writeBuffer().then((dataNew) => {
      let blob = new Blob([dataNew], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(
        blob,
        "ReportData" +".xlsx"
      );
    });
  });
}

  addAttachments(ev) {
    console.log(ev);
    if (
      ev.target.files.length <= 3 &&
      ev.target.files.length + this.files.length <= 3
    ) {
      for (let file of ev.target.files) {
        if (file.size < 5000000) {
          var ext = file.name.split(".").pop().toLowerCase();
          if (
            ext != "xlsx" &&
            ext != "pdf" &&
            ext != "doc" &&
            ext != "docx" &&
            ext != "docs" &&
            ext != "jpg" &&
            ext != "jpeg" &&
            ext != "csv" &&
            ext != "png"
          ) {
            this.showError(
              file.name + "Is not a Doc, PDF, Image or Excel type"
            );
          } else {
            this.files.push(file);
          }
        } else {
          this.showError("File size of" + file.name + "is greater than 5MB");
        }
      }
    } else {
      this.showError("Max number of files is 3");
    }
  }

  removeFile(index) {
    this.files.splice(index, 1);
  }

  showError(message) {
    this.toast.error(message);
  }
  downloadMailLog() {
    
  }
  submit() {
    var formData = new FormData();
    if (this.subject.valid && this.toBeSentExcel) {
      let reqbody = {
        subject: this.subject.value,
        message: escape(this.message.value),
        RequestedBy:this.username//  localStorage.getItem("userToken"),
      };
      formData.append("postData", JSON.stringify(reqbody));
      formData.append("sendToExcel", this.toBeSentExcel);
      this.files.forEach((file, index) => {
        formData.append(`attachment${index + 1}`, file);
      });

      this.emailPanelService
        .call("MaskMailerAdmin/Admin_Mailer_Configure", formData)
        .subscribe((response) => {
          console.log(response);
          if (response) {
            this.router.navigateByUrl("adminDashboard");
            this.toast.success("Email Sent Successfully");
          } else {
            this.showError(response);
          }
        });
    } else {
      this.showError("Kindly Fill In the required parameters");
    }
  }
}
