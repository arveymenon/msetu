import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
// import { jsPDF } from "jspdf";
import { Token } from '@angular/compiler/src/ml_parser/lexer';

@Component({
  selector: 'app-capability-scoreboard',
  templateUrl: './capability-scoreboard.component.html',
  styleUrls: ['./capability-scoreboard.component.scss']
})
export class CapabilityScoreboardComponent implements OnInit {

  DataSource:any=[];
  moduleScoreData:any=[];
  pageSize:any;
  average:any;
  displayedColumns = ["ModuleName","ParticipantName", "Stakeholder", "EmailId","Location","m1","m2","m3","m4","m5","m6","total","Mobile","at","date","fname"];
  displayedColumns3 =['m1','m2','m3','m4','m5','m6','avg'];
  constructor(private domSanitizer: DomSanitizer, private matIconRegistry: MatIconRegistry,public cs: MySuggestionService,private modalService: NgbModal) { 
    this.getSupplierData({ pageIndex: 0,pageSize: 10,length: this.pageSize});
    matIconRegistry.addSvgIcon(
      "downloadIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/downloadicon.svg"
      )
    );
  }
  sum(data){
    let numero3 = (Number(data.Mod1) + Number(data.Mod2)+ Number(data.Mod3) +Number(data.Mod4)+
    Number(data.Mod5)+Number(data.Mod6));
    let num = numero3/6
    return  num.toFixed(2);
  }
  async getSupplierData(event) {
    let reqjson={
      "PageNo": event.pageIndex +1
    }
    await this.cs
      .postMySuggestionData("Admin/GetAllCapabilityBuildingList",reqjson)
      .subscribe(async (response) => {
        console.log(response);
        this.pageSize = response.TotalCount;
        this.DataSource = [...response.ResponseData];
      });
    await console.log(this.DataSource);
  }


  

passedData:any;
  @ViewChild('reportContent') reportContent: ElementRef;
  downloadReport(data) {
   this.passedData = data;
    console.log("ss",data.SupplierName);
    console.log("pp",data.ParticipantName);
    let SupplierName = data.SupplierName.split(' ').join('+');
    let ParticipantName = data.ParticipantName.split(' ').join('+');
    let Designation = data.Designation.split(' ').join('+');
    let Location = data.Location.split(' ').join('+');
    let token = data.Token;
    var res = token.slice(0, 11);
    var r = token.slice(12,19);
    let Token = res +"%5c"+r;
    // alert(Token)

    // let Token = data.Token.replace(/[^a-zA-Z ]/g, "%5c")     
     
    let url = "https://supplier.mahindra.com/MSETU_Sustainability_Elearning/GetCertificate.aspx?supCode="+data.SupplierCode+"&SupplierName="+SupplierName+"&ParticipantName="+ParticipantName+"&Designation="+Designation+"&Location="+Location+"&Token="+Token;
    window.open(url,"_blank")
  }
    downloadPdf() {
     
    // const specialElementHandlers = {
    //   '#editor': function (element, renderer) {
    //     return true;
    //   }
    // };

    // const content = this.reportContent.nativeElement;

    // doc.fromHTML(content.innerHTML, 15, 15, {
    //   'width': 190,
    //   'elementHandlers': specialElementHandlers
    // });
    //----------jspadf error--------------working code 09-11-2021----------
    // var pdf = new jsPDF('p', 'pt', 'letter');
    // pdf.setFontSize(5);
    // pdf.html(document.getElementById('reportContent'),{
    //   callback: function (pdf) {
    //     pdf.save('DOC.pdf');
    // }
    // })
//------------code ends here 09-11-2021---------------------------------------
   
  }
  ngOnInit() {
  }
  viewDetails(data,content) {
    let temp =[{
        "vendor": data.SupplierCode,
        "mod1":data.Mod1,
        "mod2":data.Mod2,
        "mod3":data.Mod3,
        "mod4":data.Mod4,
        "mod5":data.Mod5,
        "mod6":data.Mod6
    }]

    this.moduleScoreData = temp;
    if(this.moduleScoreData[0].mod1==null || this.moduleScoreData[0].mod1 ==undefined)
    this.moduleScoreData[0].mod1=0;
    if(this.moduleScoreData[0].mod2==null || this.moduleScoreData[0].mod2 ==undefined)
    this.moduleScoreData[0].mod2=0;
    if(this.moduleScoreData[0].mod3==null || this.moduleScoreData[0].mod3 ==undefined)
    this.moduleScoreData[0].mod3=0;
    if(this.moduleScoreData[0].mod4==null || this.moduleScoreData[0].mod4 ==undefined)
    this.moduleScoreData[0].mod4=0;
    if(this.moduleScoreData[0].mod5==null || this.moduleScoreData[0].mod5 ==undefined)
    this.moduleScoreData[0].mod5=0;
    if(this.moduleScoreData[0].mod6==null || this.moduleScoreData[0].mod6 ==undefined)
    this.moduleScoreData[0].mod6=0;
    let sum = (this.moduleScoreData[0].mod1 + this.moduleScoreData[0].mod2 +
      this.moduleScoreData[0].mod3 +this.moduleScoreData[0].mod4 +this.moduleScoreData[0].mod5 +this.moduleScoreData[0].mod6);
    this.average = sum/6;
    this.modalService.open(content, {  size: 'xl' as 'lg',centered:true, windowClass : "formModal"});

  }
  closeModal(){
    this.modalService.dismissAll();
  }

}
