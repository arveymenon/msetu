import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapabilityScoreboardComponent } from './capability-scoreboard.component';

describe('CapabilityScoreboardComponent', () => {
  let component: CapabilityScoreboardComponent;
  let fixture: ComponentFixture<CapabilityScoreboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapabilityScoreboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapabilityScoreboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
