import { Component, ViewChild, HostListener } from "@angular/core";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { Observable } from "rxjs";
import * as CryptoJS from "crypto-js";
import { map } from "rxjs/operators";
import { MatIconRegistry, MatSidenav } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { NavItem } from "./nav-item";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { MyBusinessServiceService } from "../services/MyBusinessService/my-business-service.service";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { SearchModalComponent } from "../modal/search-modal/search-modal.component";
import { ContactModalComponent } from "../modal/contact-modal/contact-modal.component";
import { AwardsModalComponent } from "../modal/awards-modal/awards-modal.component";
import { WhatsNewModalComponent } from "../modal/whats-new-modal/whats-new-modal.component";
import { AppComponent } from "../app.component";
import { DataServiceService } from "../services/DataService/data-service.service";
import { CreateConcernComponent } from "../modal/create-concern/create-concern.component";
import { DashboardService } from "src/app/services/S_dashboard/dashboard.service";
import { NotificationdashboardmodalComponent } from "../modal/notificationdashboardmodal/notificationdashboardmodal.component";
import * as $ from "jquery";
import { FormBuilder, FormControl } from "@angular/forms";
import { MyHelpDeskService } from "../services/myHelpDesk/my-help-desk.service";
import { ApiService } from '../services/api/api.service';
import { CommonApiService } from '../services/common-api/common-api.service';
import { ToastServiceService } from "../services/toaster/toast-service.service";
import { SurveyService } from "../services/survey/survey.service";

@Component({
  selector: "app-main-nav",
  templateUrl: "./main-nav.component.html",
  styleUrls: ["./main-nav.component.scss"],
})
export class MainNavComponent {
  @ViewChild("divElement") divElement;
  @ViewChild("sideNavecontainer") sideNavecontainer;
  @ViewChild("mloader") mloader;
  sideMenuFlag: boolean = true;
  click_outside_Menu: boolean = false;
  mailText: string = "";
  username: any
  links: any[] = ["link1.com", "link2.com", "link3.com"];
  // sideMenuFlag :boolean=true;
  public selectedVal: string;

  firstName = new FormControl;
  lastName = new FormControl;
  emailId = new FormControl;
  mobileNo = new FormControl;

  navArray = [
    {
      value: "/suggestionDashboard",
      content: "My Suggestion",
      childContent: "Imcr/Cre Dashboard",
    },
    {
      value: "/myExcellenceAward",
      content: "My Excellence",
    },
  ];

  SideMenuButtons = [
    {
      text: "Favourites",
      isClicked: false,
      matMenuTriggerFor: "favourites",
      svgIcon: "Favourites",
      icon: "Favourites",
    },
    {
      text: "Add User",
      isClicked: false,
      matMenuTriggerFor: "",
      icon: "Dashboard",
      svgIcon: "AddUser",
    },
    {
      text: "U-Connect",
      isClicked: false,
      matMenuTriggerFor: "",
      icon: "Project",
      svgIcon: "teams",
    },
    {
      text: "Useful Link",
      isClicked: false,
      matMenuTriggerFor: "usefulLink",
      icon: "Project",
      svgIcon: "UsefulLink",
    },
    {
      text: "Contact",
      isClicked: false,
      matMenuTriggerFor: "",
      icon: "Project",
      svgIcon: "contact",
    },
  ];

  @ViewChild("closeMenu") closeMenu: any;
  menu: any = [];
  Favorites = []
  title = "AngularMaterialGettingStarted";
  isMenuOpen = false;
  contentMargin = 240;
  task: string[] = [
    "Clearning out my closet",
    "Take out trash bins",
    "Wash car",
    "Tank up the motorcycles",
    "Go for flight training",
  ];
  parent_click: boolean = false;
  addRemovButtonClick: boolean = false;
  URL: string;
  isSupplierMeet: boolean = false;
  RoleID: any;
  NumberNotification: any;

  addRemoveClick() {
    this.addRemovButtonClick = true;
  }

  onToolbarMenuToggle() {
    console.log("On toolbar toggled", this.isMenuOpen);
    this.isMenuOpen = !this.isMenuOpen;

    if (!this.isMenuOpen) {
      this.contentMargin = 70;
    } else {
      this.contentMargin = 240;
    }
  }

  
  @HostListener('window:resize', ['$event'])
  windowResize(event?: Event) {
    console.log('orientationChanged');
    if((window.innerHeight* 1.3 < window.innerWidth)){
      $(".mloader").hide();
    } else {
      $(".mloader").show();
    }
  }

  @HostListener('window:orientationchange', ['$event'])
    onOrientationChange(event?: Event) {
    console.log('orientationChanged');
    let landscape = window.screen.orientation.type.includes('landscape')
    console.log(this.mloader)
    if(landscape){
      $(".mloader").hide();
    } else {
      $(".mloader").show();
    }
  }
  showmenusec: boolean = false;
  showmenulogin: boolean = true;

  showmenustakeholder: boolean = false;
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map((result) => result.matches));

    
    constructor(
      public surveyService: SurveyService,
      public helpService: MyHelpDeskService,
      private breakpointObserver: BreakpointObserver,
      private dashboardService: DashboardService,
    public dataService: DataServiceService,
    public cs: MySuggestionService,
    public commonService: CommonUtilityService,
    public commonApiService: CommonApiService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private router: Router,
    private modalService: NgbModal,
    public toastrService: ToastServiceService
  ) {
    console.log(this.RoleID);
    this.windowResize()
    this.commonService.navchange.subscribe((data: any) => {
      console.log(data);
      this.showmenusec = data;
    });
    this.commonService.stakeholderNav.subscribe((data: any) => {
      console.log(data);
      this.showmenustakeholder = data;
    });
    
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    matIconRegistry.addSvgIcon(
      "sideMenu",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/sidemenu/Menu.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "Favourites",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/sidemenu/Favourites.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "AddUser",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/sidemenu/AddUser.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "teams",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/sidemenu/teams.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "UsefulLink",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/sidemenu/UsefulLink.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "help",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/sidemenu/help.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "Kaizala",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/sidemenu/Microsoft_Kaizala_icon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "contact",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/sidemenu/contact.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "notification",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/sidemenu/Notifications.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "search",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/sidemenu/search.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "myBusiness",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/mainmenu/business.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "myExcellenceAward",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/mainmenu/excellenceAward.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "myDocument",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/mainmenu/document.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "mySurveys",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/mainmenu/surveys.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "myRiskProfile",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/mainmenu/riskProfile.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "myHelpDesk",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/mainmenu/helpdesk.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "sustainability",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/mainmenu/sustainability.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "mySuggestion",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/mainmenu/suggestion.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "myPolicyGuidelines",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/mainmenu/policyGuidelines.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "PD",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/mainmenu/PD.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "JIT",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/mainmenu/JIT.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "analytics",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/mainmenu/analytics.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "logoutIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/logout.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "logout",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/sidemenu/log-out.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "chatbotCloseIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/suplierDashboard/signs.svg"
      )
    );
  }

  openJESCAP(isJSCAPE, URL) {
    if (isJSCAPE) {
      this.helpService
        .call("Jscape/GetJscapeURL", {
          UserName: CryptoJS.AES.decrypt(
            localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),
            ''
          ).toString(CryptoJS.enc.Utf8)
        })
        .subscribe((res) => {
          if(res.ID == 1){
            window.open(res.ResponseData, "_blank");
          }
        });
    } else window.open(URL, "_blank");
  }

  openMyHelpdesk() {
    this.router.navigateByUrl("myHelpdesk");
  }
  openAddUser() {
    //  menuClick._element.nativeElement.style.background = '#fff';
    //   if(isNavigate){
    this.RoleID = localStorage.getItem("WTIwNWMxcFZiR3M9");
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(
      CryptoJS.enc.Utf8
    );
    if (this.RoleID == "7") {
      this.router.navigateByUrl("create-user-profile");
    }
    //  }
  }
  navigateToLogin() {
    alert("hello")
    this.router.navigate(["login"]);
  }
  logout() {
    // this.toastrService.closeChatBot();
    this.username = localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ==");
    this.username = CryptoJS.AES.decrypt(this.username, "").toString(
      CryptoJS.enc.Utf8
    );
    let isUserLogin = btoa("isUserLogin");
    // this.cookieService.set(isUserLogin, btoa("No"));
    this.cs.isLogOut = true;
    this.router.navigate(["login"]);
    this.commonService.changeIsAuthenticate(false);
    localStorage.clear();
    let analysticsRequest = {
      userClicked: this.username,
      device: "windows",
      browser: localStorage.getItem("browser"),
      moduleType: "LO",
      module: "Logout",
    };
    this.closeModal();
    this.cs
      .postMySuggestionData("Analytics/InsertAnalytics", analysticsRequest)
      .subscribe();
    let Request = {
      TokenId: this.username,
    };
    this.cs
      .postMySuggestionData("Login/Logout", Request)
      .subscribe((res: any) => {
        if (res) {
        }
      });
  }

  sideMenuVisible(flag) {
    this.sideMenuFlag = flag;
    this.click_outside_Menu = flag;
    this.RoleID = this.RoleID; // localStorage.getItem('roleId');
    this.cs.SideMenus = JSON.parse(localStorage.getItem("SideMenus"));
    this.setFavorites()
  }

  setFavorites(){
    this.username = CryptoJS.AES.decrypt(localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="), "").toString(
      CryptoJS.enc.Utf8
    );
    this.cs
      .postMySuggestionData("Analytics/GetFavouriteClicks", {
        UserName: this.username,
      })
      .subscribe((data: any) => {
        console.log(data)
        if (data.ID == 1){
            this.Favorites = data.ResponseData
            localStorage.setItem("favourites", JSON.stringify(data.ResponseData));
          }else{
            this.Favorites = []
          }
        });
      }

  flag: any;

  setFlage(data) {
    this.flag = data;
  }

  openUserEdit(modalBody){
    let modal = this.modalService.open(modalBody,{
      windowClass: 'postionSetup',
      size: 'sm',
      backdrop: false
    })
  }

  closeModal(){
    this.modalService.dismissAll()
  }

  editUser(){
    if(!this.firstName.value || !this.lastName.value){
      this.cs.showError('Kindly Fill In First And Last Name')
      return
    }

    if(!this.mobileNo.value){
      this.cs.showError('Kindly Fill In Mobile No')
      return
    }

    if(!this.emailId.value){
      this.cs.showError('Kindly Provide An Email ID')
      return
    }

      const usertoken = CryptoJS.AES.decrypt(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ=='),"").toString(CryptoJS.enc.Utf8)
      this.cs.postMySuggestionData("Admin/UpdateVendorUser", {
        "userID": 0,
        "firstName": this.firstName.value,
        "lastName": this.lastName.value,
        "email": this.emailId.value,
        "phoneNumber": this.mobileNo.value,
        "roleId": null,
        "updatedBy": usertoken
      }).subscribe(res=>{
        this.cs.showSuccess('User Updated')
        this.cs.userDetails['name'] = this.firstName.value + " " + this.lastName.value
        
        localStorage.setItem(
          "WW0xR2RGcFJQVDA9",
          CryptoJS.AES.encrypt(this.cs.userDetails['name'], "").toString()
        )

        this.closeModal()
      })
  }
  
  ngOnInit() {
    this.username = CryptoJS.AES.decrypt(
      localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),
      ''
    ).toString(CryptoJS.enc.Utf8);

    this.RoleID = CryptoJS.AES.decrypt(
      localStorage.getItem("WTIwNWMxcFZiR3M9"),
      ""
    ).toString(CryptoJS.enc.Utf8);
    console.log(this.RoleID);
    const fullName = this.cs.userDetails['name'].split(" ")
    this.firstName.setValue(fullName[0])
    this.lastName.setValue(fullName[1])
    let mobileNo = CryptoJS.AES.decrypt(
      localStorage.getItem("VkZjNWFXRlhlR3hVYmxaMFdXMVdlUT09"),
      ""
    ).toString(CryptoJS.enc.Utf8);
    let emailId = CryptoJS.AES.decrypt(
      localStorage.getItem("V2xjeGFHRlhkejA9"),
      ""
    ).toString(CryptoJS.enc.Utf8);
    this.mobileNo.setValue(mobileNo)
    this.emailId.setValue(emailId)


    this.username = localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ==");
    this.username = CryptoJS.AES.decrypt(this.username, "").toString(
      CryptoJS.enc.Utf8
    );
    this.vendorcode = localStorage.getItem("WkcxV2RWcEhPWGxSTWpscldsRTlQUT09");
    this.vendorcode = CryptoJS.AES.decrypt(this.vendorcode, "").toString(
      CryptoJS.enc.Utf8
    );
    this.mailText = "mailto:msetuhelpdesk@mahindra.com";
    this.emailidReq = localStorage.getItem("V2xjeGFHRlhkejA9");
    this.emailidReq = CryptoJS.AES.decrypt(this.emailidReq, "").toString(
      CryptoJS.enc.Utf8
    );
    this.cs.SideMenus = JSON.parse(localStorage.getItem("SideMenus"));
    this.getNotification();
    this.URL = this.router.url;
    this.selectedVal = "option1";
    this.setFavorites()
  }

  openPaymentReport(menuName, route) {
    this.RoleID = localStorage.getItem("WTIwNWMxcFZiR3M9");
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(
      CryptoJS.enc.Utf8
    );
    let RoleID = this.RoleID;
    if (menuName == "OE Supplies") {
      this.dataService.setOption("Page", "OS");
      this.getSideMenuForReports(45, RoleID);
    } else if (menuName == "Spares Supplies") {
      this.dataService.setOption("Page", "SS");
      this.getSideMenuForReports(69, RoleID);
      this.router.navigateByUrl("OESupplies");
    } else if (menuName == "Quality") {
      this.dataService.setOption("Page", "Q");
      this.getSideMenuForReports(76, RoleID);
    } else if (menuName == "Payments") {
      this.dataService.setOption("Page", "P");
      this.getSideMenuForReports(95, RoleID);
    } else if (menuName == "New Part Dev") {
      this.dataService.setOption("Page", "NPD");
      this.getSideMenuForReports(111, RoleID);
      this.router.navigateByUrl("OESupplies");
    } else if (menuName == "Performance") {
      this.dataService.setOption("Page", "PF");
      this.getSideMenuForReports(131, RoleID);
      this.router.navigateByUrl("OESupplies");
    } else if (menuName == "CordysPDMS") {
      window.open(route, "_blank");
    } else if (menuName == "APQP"){
      window.open(route, "_blank");
    }
    else
      {
      // if(menuName =="BSC Dashboard")
      // this.cs.isBSCReport = true;
      // else
      // this.cs.isBSCReport = false;
      this.router.navigateByUrl(route);
    }
  }

  openCOC(){
    this.router.navigateByUrl('/supplierCodeConduct')
  }

  safetyAudit(){
    // window.open("http://mmkndmobdev.corp.mahindra.com:8080/PortalCoE/jsp/login.jsp","_blank")
    window.open("https://assessment.mahindra.com/PortalCoE/jsp/login.jsp","_blank")
  }

  auditManagement(){
    // window.open("https://assessment.mahindra.com/PortalCoE/jsp/login.jsp","_blank")
  }

  extAd(){
    window.open("https://msetuportalold.mahindra.com/sites/helpdesk/_layouts/Picker.aspx?MultiSelect=False&CustomProperty=User%3B%3B15%3B%3B%3BFalse&DialogTitle=Select%20People&DialogImage=%2F%5Flayouts%2Fimages%2Fppeople%2Egif&PickerDialogType=Microsoft%2ESharePoint%2EWebControls%2EPeoplePickerDialog%2C%20Microsoft%2ESharePoint%2C%20Version%3D14%2E0%2E0%2E0%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3D71e9bce111e9429c&ForceClaims=False&DisableClaims=False&EnabledClaimProviders=&EntitySeparator=%3B%EF%BC%9B%EF%B9%94%EF%B8%94%E2%8D%AE%E2%81%8F%E1%8D%A4%D8%9B&DefaultSearch=",
    "_blank")
  }

  getMainSideMenu() {
    let Request = {
      RoleID: this.RoleID,
    };
    this.cs
      .postMySuggestionData("menuadmin/GetLeftMenuByRole", Request)
      .subscribe((data: any) => {
        if (data.Message == "Success") this.menu = data.ResponseData;
        else
          this.cs.showError(
            "Error while fetching sidemenu please try again later"
          );
      });
  }
  // public selectedVal: string;
  notificationData: any;
  notificationTo: any;

  vendorcode: any;
  emailid: any;

  emailidReq: any;

  getNotification() {
    this.notificationTo = this.username; // localStorage.getItem('userToken');
    (this.RoleID = this.RoleID), // localStorage.getItem('roleId');
      (this.vendorcode = this.vendorcode), // localStorage.getItem('vendorCode');
      // this.emailid = JSON.parse(localStorage.getItem('userDetails'));
      (this.emailidReq = this.emailidReq);
    let reqData = {
      NotificationTo: this.notificationTo,
      VendorCode: this.vendorcode,
      RoleID: this.RoleID,
      EmailId: this.emailidReq,
    };
    this.dashboardService
      .getNotificationdata(reqData)
      .subscribe((resp: any) => {
        console.log(resp);
        this.notificationData = resp.ResponseData;
        this.NumberNotification = this.notificationData[0].UnReadCount;
        //  localStorage.setItem('noNotifi',this.notificationData[0].UnReadCount);
      });
  }

  getSideMenuForReports(ParentId, RoleID) {
    let postData = {
      RoleID: RoleID,
      ParentId: ParentId,
    };
    this.cs
      .postMySuggestionData("MenuAdmin/GetMenuByRole", postData)
      .subscribe((res: any) => {
        let SideMenuForReports = [];
        SideMenuForReports = res.ResponseData;
        this.cs.SideMenuForReports = SideMenuForReports;
        this.dataService.ParentId = ParentId;
        this.dataService.setOptionForReport(
          SideMenuForReports[0].ParentName,
          SideMenuForReports[0].NavigationName
        );
        this.router.navigateByUrl("OESupplies");
      });
    
  }

  ngAfterViewInit() {
    this.URL = this.router.url;
  }

  parentNavigation(item) {
    // this.alert(item)
    // if(item =='my-policy-guidelines') {

    // let body = {
    //   code: null,
    //   fromDate: null,
    //   toDate: null,
    //   pageNo: 0,
    // };
    // this.surveyService
    //   .getSurveysOfType("SupplierDealerCOC/GetAll_SupplierDealerCOC", body)
    //   .subscribe((res) => {
    //     console.log(res);
    //     if (res.ID == 1) {
    //       this.cs.exportJsonAsExcelFile(res.ResponseData, "Ext_COC_Survey");
    //     }
    //   });
  
    // }
    // else 
    this.router.navigateByUrl(item);
  }

  tcsSurvey(){
    this.router.navigateByUrl("tcs-survey");
  }

  navigateToDashboard() {
    this.RoleID = localStorage.getItem("WTIwNWMxcFZiR3M9");
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(
      CryptoJS.enc.Utf8
    );
    let RoleID: any = this.RoleID; // localStorage.getItem('roleId');
    if (RoleID != 7) this.router.navigateByUrl("dashboard");
    else this.router.navigateByUrl("adminDashboard");
  }

  alert(data) {
    alert(data);
  }
  onCollapse() {
    this.sideMenuFlag = true;
  }

  public onValChange(val: string) {
    this.selectedVal = val;
  }

  openModal(val) {
    this.isSupplierMeet = false;
    // let componentName;
    // val=='search'?componentName=SearchModalComponent:val=='contact'?componentName=ContactModalComponent:componentName=AwardsModalComponent;
    if (val == "search") {
      this.modalService.open(SearchModalComponent, {
        size: "xl" as "lg",
        windowClass: "searchModal",
      });
    } else if (val == "contact") {
      this.modalService.open(ContactModalComponent, {
        centered: true,
        size: "lg",
      });
    } else if (val == "whatsnew") {
      this.modalService.open(WhatsNewModalComponent, { centered: true });
    } else if (val == "Supplier") {
      this.isSupplierMeet = true;
      this.router.navigateByUrl("Supplier-meet-landing");
    } else if (val == "createConcern") {
      if (this.cs.roleID == "7")
        window.open(
          "https://teams.microsoft.com/_?lm=deeplink&lmsrc=homePageWeb&cmpid=WebSignIn",
          "",
          "height=" + screen.height + ", width=" + screen.width
        );
      else
        this.modalService.open(CreateConcernComponent, { size: "xl" as "lg" });
    } else if (val == "notificationModal") {
      this.modalService.open(NotificationdashboardmodalComponent, {
        size: "lg",
        windowClass: "notificationModal",
      });
    } else {
      this.modalService.open(AwardsModalComponent, { size: "lg" });
    }
  }

  @ViewChild("sidenav") sidenav: MatSidenav;
  @ViewChild("NotificationdashboardmodalComponent")
  NotificationdashboardmodalComponent: NotificationdashboardmodalComponent;

  reason = "";

  close(reason: string) {
    this.reason = reason;
    this.sidenav.close();
  }

  openFavouriteMenu(link) {
    window.open(this.commonApiService.baseUrl2+link,'_self')
    // if (favouriteMenu == "Myhelpdesk") {
    //   this.router.navigateByUrl("myHelpdesk");
    // } else if (favouriteMenu == "IMCRSuggestion") {
    //   this.router.navigateByUrl("mySuggestion");
    // } else if (favouriteMenu == "OESupplies") {
    //   let RoleID = this.RoleID; // localStorage.getItem('roleId');
    //   this.dataService.setOption("Page", "OS");
    //   this.getSideMenuForReports(45, RoleID);
    //   // this.router.navigateByUrl('OESupplies');
    // } else if (favouriteMenu == "Mylibrary") {
    //   this.router.navigateByUrl("my-library");
    // } else if (favouriteMenu == "Mypolicyguidelines") {
    //   this.router.navigateByUrl("my-policy-guidelines");
    // }
  }
}
