import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SrmmMISYOYAnalysisComponent } from './srmm-misyoyanalysis.component';

describe('SrmmMISYOYAnalysisComponent', () => {
  let component: SrmmMISYOYAnalysisComponent;
  let fixture: ComponentFixture<SrmmMISYOYAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SrmmMISYOYAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SrmmMISYOYAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
