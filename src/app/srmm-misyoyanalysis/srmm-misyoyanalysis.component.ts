import { Component, OnInit } from "@angular/core";
import { MatIconRegistry } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { SurveyService } from '../services/survey/survey.service';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { FormControl } from '@angular/forms';
import { Workbook } from "exceljs";
import * as fs from "file-saver";

@Component({
  selector: "app-srmm-misyoyanalysis",
  templateUrl: "./srmm-misyoyanalysis.component.html",
  styleUrls: ["./srmm-misyoyanalysis.component.scss"],
})
export class SrmmMISYOYAnalysisComponent implements OnInit {
  
  searchBy =  new FormControl()
  searchByValue = new FormControl()

  masterDataSource = []
  DataSource = [];
  displayedColumns: string[] = [
    "supplierCode",
    "supplierName",
    "FYScore1",
    "FYScore2",
    "FYScore3",
    "FYScore4",
    "FYScore5",
  ];

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public commonService: CommonUtilityService,
    private modalService: NgbModal,
    public http: SurveyService,
    public cs: MySuggestionService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "viewIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/View_Icon_Red.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "edit",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Edit.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "trash",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/delete.svg"
      )
    );

    this.search()
  }

  ngOnInit() {}

  closeModal() {
    this.modalService.dismissAll();
  }

  search(){
    this.http.call('SRMM_MIS_YOYAnalysis/GetReportSRMM_MIS_YOY').subscribe(res=>{
      console.log(res);
      let dataSource = [];

      
      for(let data of res){

        dataSource.push({
          supplierCode: data.SupplierCode,
          supplierName: data.SupplierName,
          FYScore1: data.Score1,
          FYScore2: data.Score2,
          FYScore3: data.Score3,
          FYScore4: data.Score4,
          FYScore5: data.Score5,
        })
      }

      this.DataSource = [...dataSource];
      this.masterDataSource = [...dataSource];
    })
  }

  exportToExcel(){
  let dataNew = this.DataSource;
  
  //Create workbook and worksheet
  let workbook = new Workbook();
  let worksheet = workbook.addWorksheet('SRMMAssessmentReport');
   //Add Header Row
   let header: any 
  //  if(this.CategoryName == 'Retro Debit Note' || this.CategoryName == 'Retro Credit Note'){
  //    header = Object.keys(dataNew[0])
  //  } else {
  //    header = this.displayedColumns
  //  }
  header=['Supplier Code',	'Supplier Name','	FY 2021 Score',	'FY 2020 Score',	'FY 2019 Score',	'FY 2018 Score',	'FY 2017 Score']
 let headerRow = worksheet.addRow(header);
    // Cell Style : Fill and Border
headerRow.eachCell((cell, number) => {
 cell.fill = {
   type: 'pattern',
   pattern: 'solid',
   fgColor: { argb: '00FF0000' },
   bgColor: { argb: '00FF0000' }
 }
 cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
})
dataNew.forEach(d => {
 var values = Object.keys(d).map(function (key) { return d[key]; });
 console.log(values);
//  if(this.CategoryName == 'LR & RA')
 // values.splice(12,1);
 // values.splice(13,1);
//  if(this.CategoryName == 'VQI')
//  [values[0], values[1]] = [values[1], values[0]];
  let row = worksheet.addRow(values);
});
 workbook.xlsx.writeBuffer().then((dataNew) => {
   let blob = new Blob([dataNew], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
   fs.saveAs(blob,'SRMM-YOYAnalysis.xlsx');
  
});
}

  filter(){
    let dataSource = []
    this.masterDataSource.forEach((data)=>{
      if(this.searchBy.value == 'supplierCode'){
        if(data.supplierCode.includes(this.searchByValue.value.toLowerCase()) || data.supplierCode.includes(this.searchByValue.value.toUpperCase())){
          dataSource.push(data)
        }
      } else if(this.searchBy.value == 'supplierName'){
        if(data.supplierName.includes(this.searchByValue.value.toLowerCase())|| data.supplierName.includes(this.searchByValue.value.toUpperCase())){
          dataSource.push(data)
        }
      }
    })
    this.DataSource = [...dataSource]
  }

  openModal(content) {
    this.modalService.open(content, {
      centered: true,
      windowClass: "formModal",
    });
  }
}
