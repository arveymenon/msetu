import { Component, OnInit } from '@angular/core';
import { CommonApiService } from '../services/common-api/common-api.service';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { MyLibraryService } from '../services/myLibrary/my-library.service';
import { FormControl } from '@angular/forms';
import * as CryptoJS from 'crypto-js';
declare var require: any
const FileSaver = require('file-saver');
import { NavItem } from './nav-item';
import * as moment from 'moment';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
@Component({
  selector: 'app-my-library',
  templateUrl: './my-library.component.html',
  styleUrls: ['./my-library.component.scss']
})
export class MyLibraryComponent implements OnInit {
  userid = CryptoJS.AES.decrypt(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ=='),"").toString(CryptoJS.enc.Utf8);
  selectedTabIndex = 0;
  moment = moment;
  public folders = [];
  public selectedFolder = 0;

  fileUrl:any;
  public search = new FormControl;
  public searchResults = [];

  public api_docs= [];
  menu: NavItem [] = [
    {
      displayName: 'C Form',
      iconName: '',
      route: '',
    },    
    {
      displayName: 'Economic Updates',
      iconName: '',
      route: '',
    },   
    {
      displayName: 'Industry Updates',
      iconName: '',
      route: '',
    },    
    {
      displayName: 'Sustainability',
      iconName: '',
      route: '',
    },    
    {
      displayName: 'Thought Leadership Reading Series',
      iconName: '',
      route: '',
    },    
    {
      displayName: 'Twitter Posting',
      iconName: '',
      route: '',
    },    
    {
      displayName: 'Way To Wellness(W2W)',
      iconName: '',
      route: '',
    },    
    {
      displayName: 'Supplier Champion',
      iconName: '',
      route: '',
    },    
    {
      displayName: 'Help',
      iconName: '',
      route: '',
    },    
    {
      displayName: 'Others',
      iconName: '',
      route: '',
    }      
  ];
  
  constructor(
    public commonService: CommonUtilityService,
    public matIconRegistry: MatIconRegistry,
    public domSanitizer: DomSanitizer,
    public myLibraryService: MyLibraryService,
    public cs: MySuggestionService ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon('excelIcon',domSanitizer.bypassSecurityTrustResourceUrl('assets/Icons/innerPages/excel.svg'));
    matIconRegistry.addSvgIcon('pdfIcon',domSanitizer.bypassSecurityTrustResourceUrl('assets/Icons/innerPages/Pdf_Icon.svg'));
    matIconRegistry.addSvgIcon('docIcon',domSanitizer.bypassSecurityTrustResourceUrl('assets/Icons/innerPages/word_Icon.svg'));
    matIconRegistry.addSvgIcon('folderIcon',domSanitizer.bypassSecurityTrustResourceUrl('assets/Icons/myLibrary/Folder.svg'));
    matIconRegistry.addSvgIcon('downIcon',domSanitizer.bypassSecurityTrustResourceUrl('assets/Icons/innerPages/down-arrow_red.svg')); 
    
    this.search.valueChanges.subscribe(res=>{
      console.log(res);
      if(res){
        this.selectedTabIndex = 0;
        console.log(this.selectedTabIndex);
        this.searchResults = [];
        if(res.length != 0){
          console.log(this.api_docs);
          let files = this.api_docs.filter(o=> (o.Doc_Name.toLowerCase().includes(res.toLowerCase())));
          console.log(files);
          if(files.length > 0){
            this.searchResults = files;
          }
         
        } else {
          this.searchResults = [];
        }
      }

    })
  }

  ngOnInit() {
    let body = {
      "BusinessCategoryID":"5",
      "BusinessSubCategoryID":"0",
      "DocTypeId":"0"
    }
    this.myLibraryService.getMyLibraryData('DocumentManagement/Get_DocumentManagementList',body)
      .subscribe((res: any)=>{
        console.log(res)

        if(res.Message == "Success"){
          this.api_docs = res.ResponseData.MSDOC

          this.api_docs.map((fol_file, itt)=> {

            // if(fol_file.Parent_Id == null || fol_file.Parent_Id == 0){
            //   fol_file.files = this.api_docs.filter(o=> o.Parent_Id == fol_file.Doc_Id);
            //   this.folders.push(fol_file)
            // }
            let index = this.folders.findIndex(o=>o.folder_id == fol_file.FolderId)
            if(index > -1){
              this.folders[index].files.push(fol_file);
              
            }else{
              this.folders.push({
                folder_id: fol_file.FolderId,
                folder_name: fol_file.FolderName || "No Folder Name",
                files: [fol_file]
              })
            }

          })
          console.log(this.folders);
         
        }
      })
      
      let analysticsRequest={
        "userClicked":this.userid,//  localStorage.getItem('userToken'),
        "device": "windows",
        "browser": localStorage.getItem('browser'),
        "moduleType": "Document",
        "module": "MyLibary"
      }
     this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})
  }

  view(doc){
    window.open(doc,'_blank')
  }
  
  downloadPdf(pdfUrl: string, pdfName: string){
    FileSaver.saveAs(pdfUrl, pdfName);
  }

  getFiles(itt){
    this.searchResults = [];
    this.search.setValue(null);
    this.selectedFolder = itt
  }
  // download(downloadLink){
  //  window.location.href = downloadLink;
    // const a = document.createElement('a');
    // a.href = URL.createObjectURL(downloadLink);
    // a.download = downloadLink;
    // document.body.appendChild(a);
    // a.click();
  // }
}
