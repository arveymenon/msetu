import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyExcellenceAwardComponent } from './my-excellence-award.component';

describe('MyExcellenceAwardComponent', () => {
  let component: MyExcellenceAwardComponent;
  let fixture: ComponentFixture<MyExcellenceAwardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyExcellenceAwardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyExcellenceAwardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
