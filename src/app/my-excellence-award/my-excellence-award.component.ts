import { Component, ChangeDetectionStrategy, OnInit, ViewChild, HostListener } from '@angular/core';
import { Gallery, GalleryItem, ImageItem, ThumbnailsPosition, ImageSize,GalleryComponent } from '@ngx-gallery/core';
import { map } from 'rxjs/operators';
import { MyexcellenceawardsService } from '../services/MyExcellenceAwards/myexcellenceawards.service';
import { MyBusinessServiceService } from '../services/MyBusinessService/my-business-service.service';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { resolve } from 'url';
import { CommonUtilityService } from '../services/common/common-utility.service';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ExcellenceAwardModalComponent } from '../modal/excellence-award-modal/excellence-award-modal.component';
import { Router } from '@angular/router';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-my-excellence-award',
  templateUrl: './my-excellence-award.component.html',
  styleUrls: ['./my-excellence-award.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyExcellenceAwardComponent implements OnInit {
  userid = CryptoJS.AES.decrypt(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ=='),"").toString(CryptoJS.enc.Utf8);
  products: any = [];
  items: any=[];
  Images=[];
  itemsUrl: any=[
];

  itemsArray:any={};
  @ViewChild('divElement')divElement:any;

  previewImages=[]
  openPort: boolean;
  getpdfExcelURL=[];
  RoleID: any;
  constructor(private router:Router,public cs :MySuggestionService,public gallery: Gallery, private modalService: NgbModal,
    public commonService: CommonUtilityService,private getexawardsService: MyexcellenceawardsService) { 
    this.commonService.changeIsAuthenticate(true);
  }

  ngOnInit() {
    this.RoleID = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID,"").toString(CryptoJS.enc.Utf8);
    this.GetAwardsGallery().then(()=>{
        let array:any = Object.keys(this.itemsArray)
        let reverArray = array.reverse();

    for(let i=0;i<reverArray.length;i++){
      this.previewImages.push(this.itemsArray[array[i]][0]);
       };
      });
      this.divElement._viewContainerRef.element.nativeElement.click()

      let analysticsRequest={
        "userClicked":this.userid,//  localStorage.getItem('userToken'),
        "device": "windows",
        "browser": localStorage.getItem('browser'),
        "moduleType": "Award",
        "module": "MyExcellenceAward"
      }
     this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})
  }

  GetAwardsGallery(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.postMySuggestionData('ExcellenceAwards/GetAwardsGalleryNew','').subscribe((res) => {
        this.items = res.ResponseData[0].gallery;
          let splitYear=[];
         for(let i=0;i<this.items.length;i++){
            let year=[];
            if(!this.cs.isUndefinedORNull(this.items[i].Event_Year)){
              splitYear.push(this.items[i].Event_Year);
            }  
        }
        splitYear = Array.from(new Set(splitYear))
        splitYear.sort((a, b)=> b - a);
        for (let i = 0; i < splitYear.length; i++) {
          if (i <= 7)
            this.itemsArray[splitYear[i]] = this.items.filter((x: any) => {
              if (!this.cs.isUndefinedORNull(x.Event_Year)) {
                return x.Event_Year.includes(splitYear[i]);
              }
            })
        }
        resolve();
      });
    });
  }

  basicLightboxExample() {
    this.gallery.ref().load(this.itemsUrl);
  }

  withCustomGalleryConfig() {
    const lightboxGalleryRef = this.gallery.ref('anotherLightbox');
      lightboxGalleryRef.setConfig({
      imageSize: ImageSize.Contain,
      thumbPosition: ThumbnailsPosition.Bottom

    });
    this.openPort=true;
    lightboxGalleryRef.load(this.itemsUrl);
    lightboxGalleryRef.itemClick.subscribe((data)=>{
       let url = this.getpdfExcelURL[0][data].ImagePath;
       window.open(url,"_blank");  
     })
  }

  openImages(img:any){
    this.getpdfExcelURL=[]
       this.getpdfExcelURL.push(this.itemsArray[img.Event_Year])
       this.itemsUrl = this.itemsArray[img.Event_Year].map((item:any) =>{
         if(item.ImagePath.includes('.xls')){
           return new ImageItem({ src: "https://images.idgesg.net/images/article/2017/06/microsoft_excel_logo_primary_resized2-100726640-large.3x2.jpg",
            thumb: "https://images.idgesg.net/images/article/2017/06/microsoft_excel_logo_primary_resized2-100726640-large.3x2.jpg" })
         }
         else if(item.ImagePath.includes('pdf')){
          return new ImageItem({ src: "https://thumbs.dreamstime.com/z/download-pdf-file-vector-icon-illustration-82153063.jpg",
           thumb: "https://thumbs.dreamstime.com/z/download-pdf-file-vector-icon-illustration-82153063.jpg" })
        }
         else{
           return new ImageItem({ src: item.ImagePath, thumb: item.ImagePath })
         }
       }
      );

     this.basicLightboxExample();
     this.withCustomGalleryConfig();
}


navigate(){
  if(this.RoleID == 7 ||  this.RoleID == '7'){
    this.router.navigateByUrl('adminDashboard');
  }else{
    this.router.navigateByUrl('dashboard');
  }
}


}
