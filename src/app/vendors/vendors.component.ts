import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CommonUtilityService } from "../common-utility.service";
import { NgbModal, NgbModalRef, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { EmployeeApprovalComponent } from "../modal/employee-approval/employee-approval/employee-approval.component";
import { VendorEmployeesComponent } from "../modal/vendor-employees/vendor-employees/vendor-employees.component";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import * as XLSX from "xlsx";
import { MatDialog, MatIconRegistry } from "@angular/material";

import * as fs from "file-saver";
import * as CryptoJS from "crypto-js";
import { DomSanitizer } from "@angular/platform-browser";
import { ToastrService } from "ngx-toastr";
import { DailoughBoxComponent } from '../modal/dailough-box/dailough-box.component';
import { Workbook } from 'exceljs';
// import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: "app-vendors",
  templateUrl: "./vendors.component.html",
  styleUrls: ["./vendors.component.scss"],
  providers: [NgbActiveModal]
})
export class VendorsComponent {
  roleId = CryptoJS.AES.decrypt(
    localStorage.getItem("WTIwNWMxcFZiR3M9"),
    ""
  ).toString(CryptoJS.enc.Utf8);

  createUser: FormGroup;
  editEmpFormdata: FormGroup;
  excelFile: any;
  excel_headers = [
    "Supplier Code ",
    "WorkEmail",
    "WorkPhone",
    "CellPhone",
    "FirstName",
    "LastName",
    "PreferredName",
    "SupplierToPlants",
    "SupplierToSectors",
    "Role",
    "IsSupplier",
    "Commodity",
    "SupplierName",
    "SupplierCode",
    "OfficeLocation",
    "MainVendorCode",
  ];
  @ViewChild("form") form;
  displayedColumns: string[] = [
    "SupplierCode",
    "firstName",
    "lastName",
    "location",
    "EmpCount",
    "action",
  ];
  displayedColumns2: string[] = [
    "Select",
    "EmployeeName",
    "VendorCode",
    "Designation",
    "action",
  ];
  displayedColumns3: string[] = [
    "EmployeeName",
    "VendorCode",
    "Role",
    "UserName",
    "Email",
    "primarRole",
    "commodity",
    "location",
    "modifiedBy",
    "modifiedDate",
    "status",
    "action",
  ];
  EmployeeSource = [];
  csvContent: any;
  ExcelsheetData = [];
  finalExcelKeys = [];
  supplierPlants = [];
  commodityData = [];
  sectorData = [];
  uploaded_by_Excel: boolean = false;
  Excel_File: any = [];
  uploaded_by_Form: boolean = true;
  vendorList = [];
  sample_template_used: boolean = false;
  pageNo = 0;
  pageNo1 = 0;
  Total_EMP_Count: number = 0;
  pageSize;
  pageSize1;
  pageSizeEmployeeData;
  empDetailsCount;
  Vendor_Code_DDL = [];
  roleDetails_DDL = [];
  DDLValue: any;
  reqRoleId: any;
  filterText: any
  filterPageNo: any = 1;
  master_vendors = [];
  tempbody:any;
  masterDDL: any;
  addVendorForm: any;
  username: any;
  updatedEmpDetails = [];
  editableData: any;
  updatedVendorUser: [];
  pagenumber: any;
  dropdownSettings: any = {};
  updateEmployeeUserName: any;
  constructor(
    public dialog: MatDialog,
    public commonService: CommonUtilityService,
    public formBuilder: FormBuilder,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public modal: NgbModal,
    public cs: MySuggestionService,
    private toastr: ToastrService,
    public activeModal: NgbActiveModal,
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "edit",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Edit.svg"
      )
    );
  }

  activateDeactivate(flag,element) {
    let postData = {
      "ID"       :element.ID,
      "UpdatedBy":this.username,
      "UserName" :element.UserName,
      "Type"     :flag
    };
    this.cs
      .postMySuggestionData("Admin/UpdateVendorActiveOrDeactive", postData)
      .subscribe((data) => {
        this.toastr.success(data.Message);
        this.getUpdateEmpDetails(1, "", 0, 1);
      }); 
  }
  ngOnInit() {
    this.username = localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ==");
    this.username = CryptoJS.AES.decrypt(this.username, "").toString(
      CryptoJS.enc.Utf8
    );
    this.createUserForm();
    this.editEmpForm();
    this.getSupplierPlants();
    this.getCommodityData();
    this.getMasterVendors();
    this.getSectorData();
    this.getEmployeeApprovalData(1, "");
    this.getUpdateEmpDetails(1, "", "", 0);
    this.getVendorDDL();
    this.getRolesDDL();
    this.createaddvendorForm();
    this.dropdownSettings = {
      singleSelection: false,
      idField: "RoleId",
      textField: "RoleName",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 2,
      // allowSearchFilter: true
    };
  }
  checkError = (Master_Vendor: string, errorName: string) => {
    return this.createUser.controls[Master_Vendor].hasError(errorName);
  };
  editEmpForm() {
    this.editEmpFormdata = this.formBuilder.group({
      UserId: [0],
      SupplierName: ["", Validators.required],
      FirstName: ["", Validators.required],
      LastName: ["", Validators.required],
      Role: ["", Validators.required],
      PrimaryRole: ["", Validators.required],
      PhoneNumber: ["", Validators.required],
      Email: [""],
      Commodity: [""],
      Location: [""],
      masterVendor: ["", Validators.required],
      isOTP : [""]
    });
  }
  createUserForm() {
    this.createUser = this.formBuilder.group({
      Master_Vendor: ["", Validators.required],
      supplier_code: ["", Validators.required],
      UserName: ["", Validators.required],
      SPS_Location: ["", Validators.required],
      Manager: ["", Validators.required],
      Function: ["", Validators.required],
      muspDepartment: ["", Validators.required],
      isSupplier: [""],
      colectionURL: ["", Validators.required],
      City: ["", Validators.required],
      State: ["", Validators.required],
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$"),
        ],
      ],
      work_phone: ["", Validators.required],
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
      cordys_user_id: ["", Validators.required],
      commodity: ["", Validators.required],
      office_location: ["", Validators.required],
      upload_excel: [{ value: "", disabled: true }, Validators.required],
      designation: ["", Validators.required],
      cell_phone: [
        "",
        [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")],
      ],
      preferred_name: ["", Validators.required],
      supplier_to_plants: ["", Validators.required],
      supplier_to_sector: ["", Validators.required],
      supplier_name: ["", Validators.required],
    });
  }

  validateForm(Form: FormGroup, stepper, id) {
    return new Promise((resolve: any, reject: any) => {
      if (id == 1) {
        if (
          Form.get("Master_Vendor").valid &&
          Form.get("UserName").valid &&
          Form.get("last_name").valid &&
          Form.get("email").valid &&
          Form.get("cell_phone").valid &&
          Form.get("Function").valid &&
          Form.get("muspDepartment").valid &&
          Form.get("first_name").valid &&
          Form.get("preferred_name").valid &&
          Form.get("work_phone").valid &&
          Form.get("Manager").valid &&
          Form.get("designation").valid &&
          Form.get("cordys_user_id").valid &&
          Form.get("Master_Vendor").valid &&
          Form.get("Master_Vendor").valid
        ) {
          this.isLinear = false;
          stepper.selected.completed = true;
          stepper.next();
          resolve();
        } else {
          this.openDialogForError("Please fill all mandatory fields");
          // this.cs.showAndHoldError("Please fill all mandatory fields",16000);
          this.isLinear = true;
          stepper.selected.completed = false;
          return;
        }
      }
      if (id == 2) {
        if (
          Form.get("Master_Vendor").valid &&
          Form.get("UserName").valid &&
          Form.get("last_name").valid &&
          Form.get("email").valid &&
          Form.get("cell_phone").valid &&
          Form.get("Function").valid &&
          Form.get("muspDepartment").valid &&
          Form.get("first_name").valid &&
          Form.get("preferred_name").valid &&
          Form.get("work_phone").valid &&
          Form.get("Manager").valid &&
          Form.get("designation").valid &&
          Form.get("cordys_user_id").valid &&
          Form.get("Master_Vendor").valid &&
          Form.get("Master_Vendor").valid
        ) {
          this.isLinear = false;
          stepper.selected.completed = true;
          stepper.next();
          resolve();
        } else {
          this.openDialogForError("Please fill all mandatory fields");
          // this.cs.showAndHoldError("Please fill all mandatory fields",16000);
          this.isLinear = true;
          stepper.selected.completed = false;
          return;
        }
      }
      if (id == 3) {
        if (
          Form.get("supplier_to_plants").valid &&
          Form.get("supplier_name").valid &&
          Form.get("colectionURL").valid &&
          Form.get("supplier_to_sector").valid &&
          Form.get("commodity").valid &&
          Form.get("supplier_code").valid
        ) {
          this.isLinear = false;
          stepper.selected.completed = true;
          stepper.next();
          resolve();
        } else {
          this.openDialogForError("Please fill all mandatory fields");
          // this.cs.showAndHoldError("Please fill all mandatory fields",16000);
          this.isLinear = true;
          stepper.selected.completed = false;
          return;
        }
      }
    });
  }

  createaddvendorForm() {
    this.addVendorForm = this.formBuilder.group({
      vendor_name: ["", Validators.required],
      vendor_code: ["", Validators.required],
    });
  }

  getVendorDDL() {
    this.cs.postMySuggestionData("Vendor/GetVendorDDL", "").subscribe(
      (res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.Vendor_Code_DDL = res.ResponseData;
          // this.cs.showSuccess("Success");
        } else {
          this.cs.showSwapLoader = false;
          this.openDialogForError("Something went wrong please try again later!");
          // this.cs.showAndHoldError("Something went wrong please try again later!",16000);
        }
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    );
  }

  getMasterVendors() {
    this.cs.postMySuggestionData("Vendor/GetMasterVendorDDL", "").subscribe(
      (res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.master_vendors = res.ResponseData;
        } else {
          this.cs.showSwapLoader = false;
          this.openDialogForError("Something went wrong please try again later!");
          // this.cs.showAndHoldError("Something went wrong please try again later!",16000);
        }
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    );
  }

  getSupplierPlants() {
    this.cs.postMySuggestionData("Vendor/GetPlantsDropdown", "").subscribe(
      (res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.supplierPlants = res.ResponseData;
          // this.cs.showSuccess("Success");
        } else {
          this.cs.showSwapLoader = false;
          this.openDialogForError("Something went wrong please try again later!");
          // this.cs.showAndHoldError("Something went wrong please try again later!",16000);
        }
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    );
  }

  tabClick(ev: any) {
    this.DDLValue = "";
    this.reqRoleId = "";
    this.filterPageNo = 1;
    // this.pageNo=;
    this.getVendorDDL();
    this.getRolesDDL();
    if (ev.index == 1) this.getVendorList(1, "");
    if (ev.index == 2) this.getEmployeeApprovalData(1, "");
    if (ev.index == 3) this.getUpdateEmpDetails(1, "", "", 0);
  }

  getCommodityData() {
    this.cs.postMySuggestionData("Vendor/GetCommodityDropdown", "").subscribe(
      (res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.commodityData = res.ResponseData;
          // this.cs.showSuccess("Success");
        } else {
          this.cs.showSwapLoader = false;
          this.openDialogForError("Something went wrong please try again later!");
            // this.cs.showAndHoldError("Something went wrong please try again later!",16000);
        }
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    );
  }

  getSectorData() {
    this.cs.postMySuggestionData("Vendor/GetSectorsDropdown", "").subscribe(
      (res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.sectorData = res.ResponseData;
          // this.cs.showSuccess("Success");
        } else {
          this.cs.showSwapLoader = false;
          this.openDialogForError("Something went wrong please try again later!");
          // this.cs.showAndHoldError("Something went wrong please try again later!",16000);
        }
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    );
  }

  viewEmployeeDataModal(data) {
    const modalRef = this.modal.open(EmployeeApprovalComponent, { size: "lg" });
    modalRef.componentInstance.UserName = data.UserName;

    modalRef.result.then((data) => {
      this.getEmployeeApprovalData(1, this.DDLValue);
      if (data) {
        if (data.status == "R") {
          setTimeout(()=>{ 
            this.toastr.warning("Employee Rejected");
           }, 5000)    
        } else {
          setTimeout(()=>{ 
            this.toastr.success("Employee Approved");
           }, 5000) 
        }
      }
    });
  }

  selectedEmployees: number[] = []
  async updateSelected(val, employee){
    console.log(val)
    console.log(employee)
    if(val.checked == false){
      let exists = this.selectedEmployees.findIndex(o => o == employee.ID)
      this.selectedEmployees.splice(exists, 1)
    } else {
      this.selectedEmployees.push(employee.ID)
    }
    await console.log(this.selectedEmployees)
  }

  openDialog(value): void {
    const dialogRef = this.dialog.open(DailoughBoxComponent, {
      width: "300px",
      height: "130px",
      data: {
        messege:
          value == "R"
            ? "Are you sure want to reject all these record?"
            : "Are you sure want to approve all these record?"
      },
    });

    dialogRef.afterClosed().subscribe(async (data) => {
      console.log(data);
      // let body = {
      //   VendorEmployeeIds: data.ID,
      //   Action: data.status,
      // };
      // this.cs.postMySuggestionData("Admin/ApproveOrRejectVendorEmployee", body).subscribe((res) => {
      //   console.log(res);
      // });
      await this.updateEmployees(value)
      this.activeModal.close(data);
      this.getEmployeeApprovalData(1,'')
    });
  }

  async updateEmployees(status){
    console.log(this.selectedEmployees)
    this.selectedEmployees.forEach(employeeId=>{
      let body = {
        VendorEmployeeIds: employeeId,
        Action: status,
      };
      this.cs.postMySuggestionData("Admin/ApproveOrRejectVendorEmployee", body).subscribe((res) => {
        console.log(res);
        location.reload()
      });
    })
    await console.log('Updated All Employees')
  }

  AcceptlettersOnlyVendor(event) {
    const charCode = event.which ? event.which : event.keyCode;
    if (
      !(charCode >= 65 && charCode <= 90) &&
      !(charCode >= 97 && charCode <= 122) &&
      charCode != 32 &&
      charCode != 0
    ) {
      event.preventDefault();
    }
  }
  viewVendorsDataModal(data) {
    const modalRef = this.modal.open(VendorEmployeesComponent, { size: "lg" });
    modalRef.componentInstance.UserName = data.VendorCode;
  }

  detectFiles(evt) {
    this.form.resetForm();
    var newHeadrs = [];
    let target: any = <any>evt.target;
    var headers = [];
    for (let file of target.files) {
      let ext = file.name.split(".").pop().toLowerCase();
      let reader = new FileReader();
      reader.onload = (e: any) => {
        if (ext != "xlsx") {
          this.openDialogForError("Kindly select excel file");
          // alert("Kindly select excel file");
          return false;
        }
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: "binary" });
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        var range = XLSX.utils.decode_range(ws["!ref"]);
        var C,
          R = range.s.r;

        let excel_headers = this.excel_headers;
        excel_headers = excel_headers.map((v) =>
          v.toLowerCase().replace(/ /g, "")
        );
        for (C = range.s.c; C <= range.e.c; ++C) {
          var cell = ws[XLSX.utils.encode_cell({ c: C, r: R })];
          var hdr = "UNKNOWN " + C;
          if (cell && cell.t) hdr = XLSX.utils.format_cell(cell);
          headers.push(hdr.toLowerCase().replace(/ /g, ""));
          newHeadrs.push(hdr);
        }
        excel_headers.forEach((data) => {
          if (headers.includes(data.replace(/ /g, "")))
            this.sample_template_used = true;
          else this.sample_template_used = false;
        });
        if (this.sample_template_used)
          this.Upload(target.files[0], evt, newHeadrs);
        else {
          this.openDialogForError("Kindly refer/used sample template and try to upload");
          // alert("Kindly refer/used sample template and try to upload");
          return false;
        }
      };
      reader.readAsBinaryString(target.files[0]);
    }
  }

  Upload(data, evt, newHeadrs) {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      let arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i)
        arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
      let array = [];
      array = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      if (array.length == 0)
        return this.openDialogForError("Kindly fill data in excel");
      else this.validateExcel(array, evt, newHeadrs);
      ``;
    };
    fileReader.readAsArrayBuffer(data);
  }

  validateExcel(array, evt, newHeadrs) {
    for (let i = 0; i < this.excel_headers.length; i++) {
      for (let j = 0; j < array.length; j++) {
        if (this.cs.isUndefinedORNull(array[j][newHeadrs[i]])) {
          alert(
            "Please enter values under " +
              newHeadrs[i] +
              " at line number " +
              (j + 2)
          );
          return false;
        }
        if (
          newHeadrs[i] == "WorkEmail" &&
          !/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(
            array[j][newHeadrs[i]]
          )
        ) {
          alert(
            "Please enter valid email id under " +
              newHeadrs[i] +
              " at line number " +
              (j + 2)
          );
          return false;
        }
        if (newHeadrs[i] == "WorkPhone" || newHeadrs[i] == "CellPhone") {
          if (!/^[^a-zA-Z]*$/.test(array[j][newHeadrs[i]])) {
            alert(
              "Please enter numbers only under " +
                newHeadrs[i] +
                " at line number " +
                (j + 2)
            );
            return false;
          }
          if (
            array[j][newHeadrs[i]].length > 10 ||
            array[j][newHeadrs[i]].length <= 9
          ) {
            alert(
              "Please enter 10 digit number under " +
                newHeadrs[i] +
                " at line number " +
                (j + 2)
            );
            return false;
          }
        }
        // console.log(this.master_vendors)

        // if(this.master_vendors.indexOf(o=> o.MasterVendorCode == array[j][]) > -1){

        // }else{

        //   return false;
        // }
      }
    }
    this.Excel_File = [];
    this.form.resetForm();
    this.uploaded_by_Excel = true;
    this.uploaded_by_Form = false;
    this.isFormtouched = false;
    this.isExceltouched = true;
    this.createUser.get("upload_excel").setValue(evt.target.files[0].name);
    this.Excel_File.push(evt.target.files[0]);
  }

  isFormtouched: boolean = true;
  isExceltouched: boolean = false;

  isFormvalid() {
    this.isFormtouched = true;
    this.isExceltouched = false;
    this.createUser.get("upload_excel").setValue("");
    if (this.createUser.valid) {
      this.uploaded_by_Excel = false;
      this.Excel_File = [];
      this.uploaded_by_Form = true;
    }
  }

  emailValidate() {
    this.uploaded_by_Excel = false;
  }

  showMobileValidation: boolean;

  mobileValidator(event: any) {
    if (event.target.value.length == 10) {
      this.showMobileValidation = false;
      return true;
    } else {
      this.showMobileValidation = true;
      return false;
    }
  }

  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    console.log(charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  DownloadTemplate() {
    window.open("../../assets/Vendor.xlsx", "_blank");
  }

  exportAsExcelFile(table: any, name: any) {
    if (name == "vendorList") this.getExportToExcelData();
    else if (name == "EmployeeList") this.getExportToExcelData1();
    else {
      this.getExportToExcelData2();
    }
  }

  getExportToExcelData() {
    let postData = {
      PageNo: 0,
      VendorCode: "",
    };
    this.cs
      .postMySuggestionData("Admin/GetVendorList", postData)
      .subscribe((data) => {
        let json = data.ResponseData["VendorList"];
        this.cs.exportJsonAsExcelFile(json, "VendorList");
      });
  }

  getExportToExcelData1() {
    let postData = {
      PageNo: 0,
      VendorCode: "",
    };
    this.cs
      .postMySuggestionData("Admin/GetPendingVendorEmployees", postData)
      .subscribe((data) => {
        let json = data.ResponseData["PendingVendorEmployees"];
        this.cs.exportJsonAsExcelFile(json, "EmployeeList");
      });
  }
  getExportToExcelData2() {
    let postData = {
      PageNo: 0,
      VendorCode: "",
      RoleId: 0,
    };
    this.cs
      .postMySuggestionData("admin/GetUserList", postData)
      .subscribe(async (data) => {
        let json = data.ResponseData.map((e) => {
          return {
            'Employee Name': e.FirstName+ e.LastName,
            'Vendor Code': e.VendorCode,
            'Role Name': e.RoleName,
            'Username': e.UserName,
            'Email': e.Email,
            'Primary Role Name': e.PrimaryRoleName,
            'Commodity': e.Commodity,
            'Location': e.OfficeLocation
          }
        });
        await this.exportExcel(json);
      });
  }

  
  exportExcel(json){
    let dataNew = json;
    console.log(dataNew);
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("ReportData");
    let columns = Object.keys(json[0]);
    let headerRow = worksheet.addRow(columns);
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "00FF0000" },
        bgColor: { argb: "00FF0000" },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
    });
    dataNew.forEach((d) => {
      // var obj = JSON.parse(d);
      var values = Object.keys(d).map(function (key) {
        return d[key];
      });
      let row = worksheet.addRow(values);
    });
    workbook.xlsx.writeBuffer().then((dataNew) => {
      let blob = new Blob([dataNew], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(
        blob,
        "DashboardDetails.xlsx"
      );
    });
  }

  getEmployeeApprovalData(pageNo: any, VendorCode) {
    let postData = {
      PageNo: pageNo,
      VendorCode: this.cs.isUndefinedORNull(VendorCode) ? "" : VendorCode,
    };
    this.cs
      .postMySuggestionData("Admin/GetPendingVendorEmployees", postData)
      .subscribe(
        (res: any) => {
          if (!this.cs.isUndefinedORNull(res)) {
            this.EmployeeSource = res.ResponseData.PendingVendorEmployees;
            // this.pageSize1 = res.Count;
            this.pageSizeEmployeeData = res.ResponseData.Count;
            this.EmployeeSource = [...this.EmployeeSource];
          } else {
            // this.cs.showError('Something went wrong please try again later!')
          }
        },
        (error) => {
          this.cs.showSwapLoader = false;
          console.log(error.message);
        }
      );
  }

  getVendorList(pageNo: any, VendorCode: any) {
    let postData = {
      PageNo: pageNo,
      VendorCode: this.cs.isUndefinedORNull(VendorCode) ? "" : VendorCode,
    };
    this.cs.postMySuggestionData("Admin/GetVendorList", postData).subscribe(
      (res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.isFormtouched = true;
          this.isExceltouched = false;
          this.createUser.get("upload_excel").setValue("");
          this.vendorList = res.ResponseData.VendorList;
          this.pageSize = res.ResponseData.Count;
          this.Total_EMP_Count = 0;
          this.vendorList.forEach((data) => {
            if (!this.cs.isUndefinedORNull(data.EmpCount))
              this.Total_EMP_Count = this.Total_EMP_Count + data.EmpCount;
            else this.Total_EMP_Count = this.Total_EMP_Count + 0;
          });
          this.vendorList = [...this.vendorList];
        } else {
            this.openDialogForError("Something went wrong please try again later!");
            // this.cs.showAndHoldError("Something went wrong please try again later!",16000);
        }
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    );
  }
  getRolesDDL() {
    this.cs.postMySuggestionData("CRUDMasterRole/GetRoleDDL", "").subscribe(
      (res: any) => {
        this.roleDetails_DDL = res.ResponseData;
      },
      (error) => {
        console.log(error.message);
      }
    );
  }

  selectedItems = [];
  primary_roles = [];
  isDDLTouched = false;
  selectedItems1: any;

  onItemSelect(item: any) {
    this.primary_roles = [];
    this.roleDetails_DDL.filter((x) => {
      item.value.forEach((element) => {
        if (x.RoleId == element) {
          this.primary_roles.push(x);
        }
      });
    });
  }

  onItemDeSelect(deselectedSID) {
    this.selectedItems = this.selectedItems.filter(
      (s) => s != deselectedSID.RoleId
    );
    this.primary_roles = this.primary_roles.filter(
      (id) => id.RoleId != deselectedSID.RoleId
    );
    if (this.selectedItems.length == 0) this.isDDLTouched = true;
  }

  updateDetails() {
    if (this.editEmpFormdata.valid) {
      let formData: FormData = new FormData();
      let postData = {
        userNAme: this.updateEmployeeUserName,
        roleId: this.editEmpFormdata.controls.Role.value.join(),
        firstName: this.editEmpFormdata.controls.FirstName.value,
        lastName: this.editEmpFormdata.controls.LastName.value,
        supplierName: this.editEmpFormdata.controls.SupplierName.value,
        userID: this.editEmpFormdata.controls.UserId.value,
        updatedBy: "system",
        primaryRole: this.editEmpFormdata.controls.PrimaryRole.value,
        phoneNo: this.editEmpFormdata.controls.PhoneNumber.value,
        email: this.editEmpFormdata.controls.Email.value,
        officeLocation: this.editEmpFormdata.controls.Location.value,
        commodity: this.editEmpFormdata.controls.Commodity.value,
        MasterVendor: this.editEmpFormdata.controls.masterVendor.value,
        IsOTPAllow:this.editEmpFormdata.controls.isOTP.value
      };
      this.cs
        .postMySuggestionData("Admin/UpdateVendorRole", postData)
        .subscribe(
          (res: any) => {
            this.updatedVendorUser = res.ResponseData;
            this.getUpdateEmpDetails(this.tempbody.PageNo, this.tempbody.VendorCode, this.tempbody.RoleId, 0);
            console.log(this.pageNo)
            this.openDialogForError("Success");
            // this.cs.showAndHoldSuccess("Success",16000);
            this.selectedItems = [];
            this.selectedItems1 = "";
            this.primary_roles = [];
            this.editEmpFormdata.reset();
            // this.DDLValue = ''
            // this.reqRoleId = ''
            this.closeModal();
          },
          (error) => {
            this.selectedItems = [];
            this.selectedItems1 = "";
            this.primary_roles = [];
            console.log(error.message);
          }
        );
    } else {
      this.openDialogForError("Please fill all mandatory fields");
      // this.cs.showAndHoldError("Please fill all mandatory fields",16000);
      return;
    }
  }
  resetgetUpdateEmpDetails() {
    this.DDLValue = "";
    this.reqRoleId = "";
    this.getUpdateEmpDetails(1, "", 0, 1);
  }
  getUpdateEmpDetails(
    pageNo: any,
    VendorCode: any,
    roleID: any,
    filternbr?: any
  ) {
    let postData = {
      PageNo: pageNo,
      filterText: this.filterText,
      VendorCode: this.cs.isUndefinedORNull(VendorCode) ? "" : VendorCode,
      RoleId: this.cs.isUndefinedORNull(roleID) ? 0 : roleID,
    };
    this.tempbody = postData;
    this.cs.postMySuggestionData("admin/GetUserList", postData).subscribe(
      (res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.updatedEmpDetails = res.ResponseData;
          // this.updatedEmpDetails = [...this.updatedEmpDetails];
          this.empDetailsCount = res.TotalCount;
          // this.cs.showSuccess("Success");
        } else {
          this.cs.showSwapLoader = false;
            this.openDialogForError("Something went wrong please try again later!'")
            // this.cs.showAndHoldError("Something went wrong please try again later!",16000);
        }
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    );
  }
  // approve(data) {
  //   let postData = {
  //     "VendorEmployeeIds": data
  //   }
  //   this.cs.postMySuggestionData('Admin/ApproveOrRejectVendorEmployee', postData).
  //     subscribe(
  //       (res: any) => {
  //         if (!this.cs.isUndefinedORNull(res)) {
  //           this.cs.showSuccess(res.ResponseData);
  //           this.getEmployeeApprovalData(1, this.DDLValue);
  //         } else {
  //           this.cs.showError('Something went wrong please try again later!')
  //         }
  //       },
  //       (error) => {
  //         this.cs.showSwapLoader = false;
  //         console.log(error.message);
  //       }
  //     )
  // }

  isLinear = true;
  isMatStepComplete: boolean = false;

  submit(form) {
    let formData;
    let url;
    if (!form.valid && !this.uploaded_by_Excel) {
      return;
    } else if (this.uploaded_by_Form && !this.uploaded_by_Excel) {
      url = "Vendor/VendorRegistration";
      formData = {
        MainVendorCode: this.createUser.controls.Master_Vendor.value,
        userName: this.createUser.controls.UserName.value, // this.username,
        supplierCode: this.createUser.controls.supplier_code.value,
        designation: this.createUser.controls.designation.value,
        workEmail: this.createUser.controls.email.value,
        workPhone: this.createUser.controls.work_phone.value,
        cellPhone: this.createUser.controls.cell_phone.value,
        firstName: this.createUser.controls.first_name.value,
        lastName: this.createUser.controls.last_name.value,
        preferredName: this.createUser.controls.preferred_name.value,
        supplierToPlants: this.createUser.controls.supplier_to_plants.value,
        supplierToSectors: this.createUser.controls.supplier_to_sector.value,
        cordysUserId: this.createUser.controls.cordys_user_id.value,
        commodity: this.createUser.controls.commodity.value,
        supplierName: this.createUser.controls.supplier_name.value,
        officeLocation: this.createUser.controls.office_location.value,
        createdBy: this.createUser.controls.supplier_code.value,
        createdDate: new Date(),
        spsLocation: this.createUser.controls.SPS_Location.value,
        manager: this.createUser.controls.Manager.value,
        function: this.createUser.controls.Function.value,
        muspDepartment: this.createUser.controls.muspDepartment.value,
        isSupplier: "1",
        supplierPrivateSiteCollectionURL: this.createUser.controls.colectionURL
          .value,
        city: this.createUser.controls.City.value,
        state: this.createUser.controls.State.value,
        isActive: "1",
      };
    } else if (!this.uploaded_by_Form && this.uploaded_by_Excel) {
      url = "Vendor/BulkUploadVendorDetails";
      formData = new FormData();
      formData.append("ExcelFile", this.Excel_File[0]);
      formData.append("CreatedBy", this.username);
    } else {
      return;
    }
    this.cs.showSwapLoader = true;
    this.cs.postMySuggestionData(url, formData).subscribe(
      (res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          if (res.ID == 1) {
            this.cs.showSwapLoader = false;
            this.openDialogForError("Success")
            // this.cs.showAndHoldSuccess("Success",16000);
            // this.toastr.success("Success");
            form.resetForm();
          } else {
            this.openDialogForError(res.Message);
            // setTimeout(()=>{ 
            //   this.toastr.error(res.Message);
            //  }, 5000)
            
            // this.toastr.error(res.Message);
          }
        } else {
          this.cs.showSwapLoader = false;
          this.openDialogForError("Something went wrong please try again later!")
            // this.cs.showAndHoldError('Something went wrong please try again later!',5000) 
          // this.cs.showError("Something went wrong please try again later!");
        }
      },
      (error) => {
        this.cs.showSwapLoader = false;
        this.openDialogForError("Failed")
        // setTimeout(()=>{ 
        //   this.toastr.error("Failed");
        //  }, 5000) 
        console.log(error.message);
      }
    );
  }

  getPageChangeData(ev, id) {
    this.selectedEmployees = []
    if (id == 1) {
      this.filterPageNo = ev.pageIndex + 1;
      this.getVendorList(ev.pageIndex + 1, this.DDLValue);
    } else if (id == 2) {
      this.filterPageNo = ev.pageIndex + 1;
      this.getEmployeeApprovalData(ev.pageIndex + 1, this.DDLValue);
    } else {
      this.filterPageNo = ev.pageIndex + 1;
      this.getUpdateEmpDetails(
        ev.pageIndex + 1,
        this.DDLValue,
        this.reqRoleId,
        0
      );
    }
  }
  otpFlag:any='N';
  changeOTPFlag(flag) {
    this.otpFlag =flag; 
    if(flag)
    {
      // this.editEmpFormdata.get('PhoneNumber').setValidators([Validators.required]);
      // this.editEmpFormdata.get('Email').setValidators([Validators.required]);
      this.editEmpFormdata.controls['PhoneNumber'].setValidators(Validators.required);
      this.editEmpFormdata.controls['Email'].setValidators(Validators.required);
      let flagText ='Y';
      this.editEmpFormdata.controls["isOTP"].setValue(flagText);
    }   
    else
    {
      this.editEmpFormdata.get("PhoneNumber").clearValidators();
      this.editEmpFormdata.get("Email").clearValidators();
      // this.form.controls["firstName"].setValidators([Validators.minLength(1), Validators.maxLength(30)]);
      let flagText ='N';
      this.editEmpFormdata.controls["isOTP"].setValue(flagText);
    }
  }
  openModal(content, id?: any, userName?: any) {
    this.editEmpFormdata.reset();
    this.updateEmployeeUserName = userName;
    this.modal.open(content, {
      size: "lg",
      centered: true,
      windowClass: "formModal",
    });
    this.getVendorUser(id);
  }
  closeModal() {
    this.modal.dismissAll();
  }

  selectmulti = [];
  getVendorUser(id: any) {
    let data = [];
    // this.pagenumber= currentPage;
    let postData = {
      UserID: id,
    };
    this.cs
      .postMySuggestionData("admin/GetVendorUser", postData)
      .subscribe((res: any) => {
        this.editableData = res.ResponseData;
        this.editEmpFormdata.controls["FirstName"].setValue(
          this.editableData.FirstName
        );
        this.editEmpFormdata.controls["LastName"].setValue(
          this.editableData.LastName
        );
        this.editEmpFormdata.controls["Email"].setValue(
          this.editableData.Email
        );
        this.editEmpFormdata.controls["PhoneNumber"].setValue(
          this.editableData.PhoneNumber
        );
        this.editEmpFormdata.controls["SupplierName"].setValue(
          this.editableData.SupplierName
        );
        this.editEmpFormdata.controls["Commodity"].setValue(
          this.editableData.Commodity
        );
        this.editEmpFormdata.controls["Location"].setValue(
          this.editableData.OfficeLocation
        );
        this.editEmpFormdata.controls["UserId"].setValue(
          this.editableData.ID
        );
        this.editEmpFormdata.controls["masterVendor"].setValue(
          this.editableData.MasterVendor || ''
        );
      
        if(this.editableData.IsOTPAllow == null || this.editableData.IsOTPAllow == undefined || this.editableData.IsOTPAllow == 'N') {
          this.otpFlag =false;
          this.editEmpFormdata.controls["isOTP"].setValue(false);
          this.editEmpFormdata.get('PhoneNumber').clearValidators();
          this.editEmpFormdata.get('Email').clearValidators();
        }
        else if(this.editableData.IsOTPAllow == 'Y'){
          this.otpFlag =true;
          this.editEmpFormdata.controls["isOTP"].setValue(true);
          // this.editEmpFormdata.get('PhoneNumber').setValidators([Validators.required]);
          // this.editEmpFormdata.get('Email').setValidators([Validators.required]);

          this.editEmpFormdata.controls['PhoneNumber'].setValidators(Validators.required);
          this.editEmpFormdata.controls['Email'].setValidators(Validators.required);
         
        }
        
        
        let data = this.editableData.RoleId.split(",").map(Number);
        this.editEmpFormdata.controls["Role"].setValue(data);
        this.primary_roles = [];
        this.roleDetails_DDL.filter((x) => {
          data.forEach((element) => {
            if (x.RoleId == element) {
              this.primary_roles.push(x);
            }
          });
        });
        this.editEmpFormdata.controls["PrimaryRole"].setValue(
          this.editableData.PrimaryRoleID
        );
      });
  }

  @ViewChild("newform") vendorForm;

  addMasterVendor(Form: any) {
    if (Form.valid) {
      let Request = {
        MasterVendorCode: Form.controls.vendor_code.value,
        MasterVendorName: Form.controls.vendor_name.value,
        CreatedBy: this.username,
      };
      this.cs
        .postMySuggestionData("Vendor/AddMasterVendor", Request)
        .subscribe((res: any) => {
          if (!this.cs.isUndefinedORNull(res) && res.Message == "Success") {
            if (res.ResponseData[0].ID == 1) {
              this.openDialogForError("Vendor added succesfully")
              // this.cs.showAndHoldSuccess("Vendor added succesfully",16000);
              this.closeModal();
              Form.reset();
              this.getMasterVendors();
            } else {
              this.openDialogForError(res.ResponseData[0].Message);
              // this.cs.showAndHoldError(res.ResponseData[0].Message,16000);
            }
          } else {
              this.openDialogForError("Something went wrong please try again later !");
              // this.cs.showAndHoldError("Something went wrong please try again later!",16000);
            this.vendorForm.resetForm();
            return;
          }
        });
    } else {
        this.openDialogForError("Please enter all valid fields");
        // this.cs.showAndHoldError("Please enter all valid fields",16000); 
      return;
    }
  }
  openDialogForError(value): void {
    const dialogRef = this.dialog.open(DailoughBoxComponent, {
      width: "300px",
      height: "130px",
      data: {
        messege: value       
          },
    });

    dialogRef.afterClosed().subscribe(async (data) => {
      // await this.updateEmployees(value)
      // this.activeModal.close(data);
      // this.getEmployeeApprovalData(1,'')
    });
  }
  DownloadSampleTemplate(filename:any){
    window.open(`${'../../assets/'}${filename}${'.xlsx'}`, '_blank');
    // window.open('../../assets/Navigation Sample.xlsx')
  }
  excel_headers_addvendor:any=[];
  bulkUploadFile:any='';
  detectFilesForBulkUpload(evt) {
    this.excel_headers_addvendor=["VendorCode","VendorName"];
    let target: any = <any>(evt.target);
    var headers = [];
    for (let file of target.files) {
      let ext = file.name.split(".").pop().toLowerCase();
      let reader = new FileReader();
      reader.onload = (e: any) => {
        if (ext != 'xlsx') {
          this.cs.showError("Kindly select file with extension '.xlsx");
          return false;
        }
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        var range = XLSX.utils.decode_range(ws['!ref']);
        var C, R = range.s.r;
        let excel_headers = this.excel_headers_addvendor;
        excel_headers = excel_headers.map(v => v.toLowerCase().replace(/ /g, ""));
        for (C = range.s.c; C <= range.e.c; ++C) {
          var cell = ws[XLSX.utils.encode_cell({ c: C, r: R })]
          var hdr = "UNKNOWN " + C;
          if (cell && cell.t && excel_headers.includes(cell.v.toLowerCase().replace(/ /g, "")))
            hdr = XLSX.utils.format_cell(cell);
          else {
            this.cs.showError("Kindly refer/used sample template and try to upload")
            return false;
          }
          // headers.push(hdr.toLowerCase().replace(/ /g, ""));
          headers.push(hdr);
        }
        this.Upload_addVendor(target.files[0], evt,headers)
      }
      reader.readAsBinaryString(target.files[0]);
    }
  }

  Upload_addVendor(data, evt,headers) {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      let arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
      let array = [];
      array = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      if (array.length == 0)
        return this.cs.showError("Kindly fill data in excel");
      else
        this.validateExcel_addVendor(array, evt,headers);
    }
    fileReader.readAsArrayBuffer(data);
  }

  validateExcel_addVendor(array, evt,headers) {
    for (let i = 0; i < this.excel_headers_addvendor.length; i++) {
      for (let j = 0; j < array.length; j++) {
        if (this.cs.isUndefinedORNull(array[j][headers[i]])) {
          this.cs.showError("Please enter values under " + this.excel_headers_addvendor[i] + " at line number " + (j + 2));
          return false;
        }
        // if(!this.cs.isUndefinedORNull(array[j][headers[i]])&&headers[i]=="Mobile Number"){
        //   if(array[j][headers[i]].includes(/^[^a-zA-Z]$/)){
        //     alert("Please enter only numbers under " + this.excel_headers[i] + " at line number " + (j + 2));
        //     return false;
        //   }
        // }
      }
    }
    this.Excel_File = [];
    // form.resetForm();
    this.bulkUploadFile = evt.target.files[0].name;
    // form.control.get('Bulk_Upload').setValue(evt.target.files[0].name)
    this.Excel_File.push(evt.target.files[0]);
  }
bulkUpload_vendors(){
      let url = "AD/BulkInsertVendorData";
      let formData = new FormData();
      formData.append("ExcelFile", this.Excel_File[0]);
      formData.append("CreatedBy", this.username);
      this.cs.postMySuggestionData(url, formData).subscribe(
        (res: any) => {
          this.cs.showSuccess("Data uploaded successfully")
          this.bulkUploadFile = '';
        },
        (error) => {
          this.cs.showError("Something went wrong please try again later!")
        }
      );
}
// otp bulk upload
otpBulkFileName:any;
otpBulkFile:any=[];
detectOTPFile(event) {
 
  console.log(event);
  let files = event.target.files; 
  if (files) {
    for (let file of files) {
      console.log(file);
      this.otpBulkFileName = file.name;
    
      let ext=file.name.split(".").pop().toLowerCase();
      console.log(ext);
      let reader = new FileReader();
      reader.onload = (e: any) => {
       
              if(ext =='xlsx'){
                this.otpBulkFile=[];
                this.otpBulkFile.push(file);
                this.otpBulkFile.push({url:e.target.result,ext:ext});   
              }
              else{
                alert("please select proper format");
              //  this.utilityProvider.presentToast('Please select file in proper format','4000','top')
              }
      }
      reader.readAsDataURL(file);
    }
  }
}
openOTPModal(otpModalcontent){
  const modalRef = this.modal.open(otpModalcontent, { size: "lg" });
}
uploadOTPFile(){
  let url = "AD/BulkUpdationForOTP";
  let formData = new FormData();
  formData.append("ExcelFile", this.otpBulkFile[0]);
  // formData.append("CreatedBy", this.username);
  this.cs.postMySuggestionData(url, formData).subscribe(
    (res: any) => {
      this.cs.showSuccess("Data uploaded successfully")
      this.otpBulkFile = '';
      this.otpBulkFileName = '';
    },
    (error) => {
      this.cs.showError("Something went wrong please try again later!")
    }
  );
}
closeOTPModal(){
  this.otpBulkFile = '';
  this.otpBulkFileName = '';
}
}
