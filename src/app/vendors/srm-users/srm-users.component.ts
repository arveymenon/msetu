import { Component, OnInit } from "@angular/core";

import * as XLSX from "xlsx";
import { ToastrService } from "ngx-toastr";
import { MySuggestionService } from "src/app/services/MySuggestion/my-suggestion.service";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-srm-users",
  templateUrl: "./srm-users.component.html",
  styleUrls: ["./srm-users.component.sass"],
})
export class SrmUsersComponent implements OnInit {
  pageNo = 1;
  pageSize = 0;
  displayedColumns: string[] = [
    "SrNo",
    "MasterVendor",
    "Vendor",
    "User_ID",
    "First_Name",
    "Last_Name",
    "SRM_User",
    "SRM_Password",
    "Action",
  ];
  excel_headers = ["USERNAME", "SRMUSERID", "SRMUSERPASSWORD"];
  // download_excel_headers = [
  //   { user_id: "" },
  //   { srm_user: "" },
  //   { srm_password: "" },
  // ];

  DataSource = [
    {
      SrNo: "",
      MasterVendor: "",
      Vendor: "",
      User_ID: "",
      First_Name: "",
      Last_Name: "",
      SRM_User: "",
      SRM_Password: "",
      Action: "",
    },
  ];

  MasterVendorCode = new FormControl();
  vendorCode = new FormControl();
  srmusername = new FormControl();
  username = new FormControl();

  constructor(public toastr: ToastrService, public cs: MySuggestionService) {
    this.getData(1);
  }

  ngOnInit() {}

  getData(pageNo?: any) {
    let body = {
      MasterVendor: this.MasterVendorCode.value,
      Vendor: this.vendorCode.value,
      SRMUser: this.srmusername.value,
      UserName: this.username.value,
      PageNo: pageNo ? pageNo : 0,
    };
    
    this.cs
      .postMySuggestionData("SRMSSO/GetSRMUsers", body)
      .subscribe((res) => {
        console.log(res);
        let dataSource = [];
        if (res) {
          this.pageSize = res.ResponseData[0].TOTALRECORDS;
          res.ResponseData.forEach((data) => {
            dataSource.push({
              SrNo: data.SRNO,
              MasterVendor: data.Mastervendorcode,
              Vendor: data.VENDORCODE,
              User_ID: data.USERNAME,
              FirstName: data.FirstName,
              LASTNAME: data.LASTNAME,
              SRM_User: new FormControl(data.SRMUSERID),
              SRM_Password: new FormControl(data.SRMUSERPASSWORD),
              Action: data.SrNo,
            });
          });

          this.DataSource = [...dataSource];
        }
      });
  }

  update(element) {
    console.log(element);
    if (element.SRM_User.value != "" && element.SRM_Password.value != "") {
      this.sendExcel([
        {
          USERNAME: element.User_ID,
          SRMUSERID: element.SRM_User.value,
          SRMUSERPASSWORD: element.SRM_Password.value,
        },
      ]);
    } else {
      this.toastr.error("Kindly Provide Valid Inputs For Fields To Be Update");
    }
  }

  async exportAsExcelFile(name) {
    let body = {
      UserName: null,
      PageNo: 0,
    };
    this.cs
      .postMySuggestionData("SRMSSO/GetSRMUsers", body)
      .subscribe(async (res) => {
        let data = [];
        res.ResponseData.forEach((element) => {
          data.push({
            Mastervendorcode: element.Mastervendorcode,
            VENDORCODE: element.VENDORCODE,
            USERNAME: element.USERNAME,
            FirstName: element.FirstName,
            LASTNAME: element.LASTNAME,
            SRMUSERID: element.SRMUSERID,
            SRMUSERPASSWORD: element.SRMUSERPASSWORD
          });
        });
        await this.cs.exportJsonAsExcelFile(data, name);
      });
  }

  uploadExcel(ev, type?: any) {
    var headers = [];
    for (let file of ev.target.files) {
      let ext = file.name.split(".").pop().toLowerCase();
      let reader = new FileReader();
      reader.onload = (e: any) => {
        if (ext != "xlsx") {
          alert("Kindly select excel file");
          return false;
        }
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: "binary" });
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        var range = XLSX.utils.decode_range(ws["!ref"]);
        var C,
          R = range.s.r;
        let excel_headers = this.excel_headers;
        excel_headers = excel_headers.map((v) =>
          v.toLowerCase().replace(/ /g, "")
        );
        for (C = range.s.c; C <= range.e.c; ++C) {
          var cell = ws[XLSX.utils.encode_cell({ c: C, r: R })];
          var hdr = "UNKNOWN " + C;
          if (
            cell &&
            cell.t &&
            excel_headers.includes(cell.v.toLowerCase().replace(/ /g, ""))
          ) {
            hdr = XLSX.utils.format_cell(cell);
            headers.push(hdr);
          } else {
            alert(
              "Invalid Excel Inputs. Kindly Download An Excel File For Excel Template"
            );
            return false;
          }
          // headers.push(hdr.toLowerCase().replace(/ /g, ""));
          headers.push(hdr);
        }
        this.Upload(ev.target.files[0], headers);
      };
      reader.readAsBinaryString(ev.target.files[0]);
    }
  }

  Upload(data, headers) {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      let arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i)
        arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });

      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
      let array = [];
      array = XLSX.utils.sheet_to_json(worksheet, { raw: true });

      // array = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      if (array.length == 0)
        return this.toastr.error("Kindly fill data in excel");
      else {
        this.validateExcel(array, headers);
      }
    };
    fileReader.readAsArrayBuffer(data);
  }

  validateExcel(array, headers) {
    for (let i = 0; i < headers.length; i++) {
      for (let j = 0; j < array.length; j++) {
        if (this.cs.isUndefinedORNull(array[j][headers[i]])) {
          if (i == 0) {
            console.log(array[j]);
            console.log(headers[i]);
            alert(
              "Please enter values under " +
                headers[i] +
                " at line number " +
                (j + 2)
            );
            return false;
          } else {
            array[j][headers[i]] = "";
          }
        }
      }
    }
    // array is the variable to be sent

    // this.toBeSentExcel = array;
    this.sendExcel(array);
  }

  sendExcel(array) {
    console.log(array);
    let body: any;
    body = {
      SRMUSER: [],
    };
    array.forEach((element) => {
      body.SRMUSER.push({
        USER: element.USERNAME,
        SRMUSER: element.SRMUSERID,
        SRMPWD: element.SRMUSERPASSWORD,
      });
    });
    this.cs
      .postMySuggestionData("SRMSSO/UpdateSRMUsers", body)
      .subscribe((res) => {
        console.log(res);
        if (res) {
          this.toastr.success("Excel Uploaded Successfully");
        }
      });
  }
}
