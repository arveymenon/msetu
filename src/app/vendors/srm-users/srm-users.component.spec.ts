import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SrmUsersComponent } from './srm-users.component';

describe('SrmUsersComponent', () => {
  let component: SrmUsersComponent;
  let fixture: ComponentFixture<SrmUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SrmUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SrmUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
