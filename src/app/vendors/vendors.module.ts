import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorsComponent } from './vendors.component';
import { VendorEmployeesComponent } from '../modal/vendor-employees/vendor-employees/vendor-employees.component';
import { EmployeeApprovalComponent } from '../modal/employee-approval/employee-approval/employee-approval.component';
import { CreateUserProfileComponent } from '../create-user-profile/create-user-profile.component';
import { RoleBasedAccessComponent } from '../role-based-access/role-based-access.component';
import { VendorRoutingModule } from './vendor-routing/vendor-routing.module';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/material.module';
import { MatPaginatorModule, MatDialogModule, MatSortModule, MatFormFieldModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { SrmUsersComponent } from './srm-users/srm-users.component';
import { VendorMappingComponent } from '../vendor-mapping/vendor-mapping.component';

@NgModule({
  declarations: [
    VendorsComponent, 
    VendorEmployeesComponent,
    EmployeeApprovalComponent,
    CreateUserProfileComponent,
    RoleBasedAccessComponent,
    SrmUsersComponent,

    VendorMappingComponent
  ],
  imports: [
    CommonModule,
    VendorRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
    MaterialModule,ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    FormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSortModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    NgxBarcodeModule
  ],
  entryComponents: [
    VendorEmployeesComponent,
    EmployeeApprovalComponent,
    
  ],
  providers: [
    VendorEmployeesComponent,
    EmployeeApprovalComponent,

  ]
})
export class VendorsModule { }
