import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CreateUserProfileComponent } from 'src/app/create-user-profile/create-user-profile.component';
import { RoleBasedAccessComponent } from 'src/app/role-based-access/role-based-access.component';
import { VendorsComponent } from '../vendors.component';
import { AuthGuard } from 'src/app/services/auth-guards/auth.guard';
import { VendorMappingComponent } from 'src/app/vendor-mapping/vendor-mapping.component';

const routes: Routes = [
  {
    path: "",
    component: VendorsComponent,
    canActivate: [AuthGuard],
    data: { roles: ['vendors'] }
  },
  {
    path: "create-user-profile",
    component: CreateUserProfileComponent,
    canActivate: [AuthGuard],
    data: { roles: ['create-user-profile'] }
  },
  {
    path: "vendor-mapping",
    component: VendorMappingComponent
  },
  {
    path: "RoleBasedAccess",
    component: RoleBasedAccessComponent,
    canActivate: [AuthGuard],
    data: { roles: ['RoleBasedAccess'] }
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VendorRoutingModule { }
