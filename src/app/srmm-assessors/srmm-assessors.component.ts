import { Component, OnInit } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { MyHelpDeskService } from "../services/myHelpDesk/my-help-desk.service";
import * as _ from "lodash";
import { DomSanitizer } from "@angular/platform-browser";
import { MatIconRegistry } from "@angular/material";
import { Router } from "@angular/router";

import * as CryptoJS from 'crypto-js';
import { NgModel } from '@angular/forms';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';

@Component({
  selector: "app-srmm-assessors",
  templateUrl: "./srmm-assessors.component.html",
  styleUrls: ["./srmm-assessors.component.scss"],
})
export class SrmmAssessorsComponent implements OnInit {
  pageNo = 1;
  pageSize = 100;
  userToken = CryptoJS.AES.decrypt(localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),"").toString(CryptoJS.enc.Utf8);
  DataSource: any = [];
  all_year_data = []
  all_status = ["All", "Pending with Supplier", "Pending with Assessor", "Completed"]
  yearDDL: NgModel
  status: NgModel
  displayedColumns: string[] = [
      "SupplierCode",
      "SupplierName",
      "Email",
      "CompletionDate",
      "Assessor",
      "Status",
      "Action",
      "ViewDashboard"
  ];

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public commonService: CommonUtilityService,
    public router: Router,
    public http: MyHelpDeskService,
    public cs: MySuggestionService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
  }

  ngOnInit() {
    this.getAllyear()
    this.getData("FY 2021");
  }

  getAllyear(): Promise<any> {
    return new Promise((resolve: any) => {
      this.http.call('SupplierAnswers/GetAllYears', '').subscribe((res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.all_year_data = res;
          resolve();
        }
        else {
          this.cs.showError("Failed to load year");
          return;
        }
      })
    })
  }

  getData(year,status?: any) {
    let body = {
      "year": year || "FY 2021",
      "commodity": null,
      "supplierCode": null,
      "supplierName": null,
      "supplierLocation": null,
      "assessor": this.userToken,
      "status": status || 'All'
    }
    this.http.call('SRMMAssesment/GetReportSRMMAssessor', body).subscribe(res=>{
      console.log(res)
      if(res.Id == 1){
        this.DataSource = res.ResponseData
        
        // this.DataSource = []
        // if(status && status != "All"){
        //   let data = res.ResponseData
        //   data.forEach((element,itt) => {
        //     if(element.Status == status){
        //       res.ResponseData.splice(itt,1)
        //       this.DataSource.push(element)
        //     }
        //   });
        // } else {
        // }
      }
    })
  }

  capitalize(string) {
    return _.upperFirst(string);
  }

  viewDashboard(element) {
    // ALL DETAILS OF THAT SRMM SURVEY
    console.log(element);
    this.router.navigateByUrl("riskDashboard?VC="+btoa(element.SupplierCode)+"&FY="+element.SchID);
  }

  respond(element) {
    console.log(element);
    //let year = element.SchID;
    this.router.navigateByUrl("selfAssessment?si="+element.SupplierCode+"&FY="+element.SchID);
  }
}