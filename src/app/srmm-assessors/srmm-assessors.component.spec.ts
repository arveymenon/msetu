import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SrmmAssessorsComponent } from './srmm-assessors.component';

describe('SrmmAssessorsComponent', () => {
  let component: SrmmAssessorsComponent;
  let fixture: ComponentFixture<SrmmAssessorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SrmmAssessorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SrmmAssessorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
