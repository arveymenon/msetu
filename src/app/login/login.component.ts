import { Component, ViewChild, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbCarouselConfig, NgbCarousel } from "@ng-bootstrap/ng-bootstrap";
import { MatIconRegistry } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { DatePipe } from "@angular/common";
import { LoginServiceService } from "../services/LoginService/login-service.service";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { CookieService } from "ngx-cookie-service";
import { WhatsNewModalComponent } from "../modal/whats-new-modal/whats-new-modal.component";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import * as CryptoJS from "crypto-js";
import { ToastrService } from "ngx-toastr";
import { catchError, map } from "rxjs/operators";
import { empty } from "rxjs";
import * as XLSX from 'xlsx';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { LoginModalComponent } from "../modal/login-modal/login-modal.component";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  providers: [NgbCarouselConfig],
})
export class LoginComponent implements OnInit {
  safeURL: any;
  public cookieValue: any;
  public cookieValuePwd: any;
  loginForm: FormGroup;
  newsEvents: any[] = [];
  arrays: any = [];
  today: Date = new Date();
  CMSLogin: any = [];
  arraycookie: any = [];
  CMSLatestVideo: any = [];
  imagess: any = [];
  linkss: any = [];
  ltstimages: any = [];
  otpFlag: boolean = false;
  unameempty: boolean = false;
  useNameError: boolean = false;
  obj: any;
  otpFlagShow: boolean = false;
  resendOTPFlag:boolean = false;
  resendOTPFlagCount :any = 0;
  otpreq: boolean = false;
  otpSentMessage: boolean = false;
  errorMessage: any;
  loginroleId: any;
  dataEvent: string;

  dorpdownVal: any = 1;
  userToken: any;

  @ViewChild("myCarousel") myCarousel: NgbCarousel;
  @ViewChild("loginCarousel") loginCarousel: NgbCarousel;

  readJSON = {
    imgArray: [
      {
        img: "../assets/img/login/NewsImage.png",
        heading: "New Base W3 Variant in XUV500!",
        date: "May 14,2019",
        description:
          "Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the",
      },
      {
        img: "../assets/img/login/NewsImage.png",
        heading: "New Base W3 Variant in XUV500!",
        date: "May 14,2019",
        description:
          "Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the",
      },
      {
        img: "../assets/img/login/NewsImage.png",
        heading: "New Base W3 Variant in XUV500!",
        date: "May 14,2019",
        description:
          "Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the",
      },
      {
        img: "../assets/img/login/NewsImage.png",
        heading: "New Base W3 Variant in XUV500!",
        date: "May 14,2019",
        description:
          "Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the",
      },
      {
        img: "../assets/img/login/NewsImage.png",
        heading: "New Base W3 Variant in XUV500!",
        date: "May 14,2019",
        description:
          "Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the",
      },
      {
        img: "../assets/img/login/NewsImage.png",
        heading: "New Base W3 Variant in XUV500!",
        date: "May 14,2019",
        description:
          "Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the",
      },
      {
        img: "../assets/img/login/NewsImage.png",
        heading: "New Base W3 Variant in XUV500!",
        date: "May 14,2019",
        description:
          "Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the",
      },
    ],
  };
  videoss: any;
  images = [
    "../assets/img/login/slider/1.png",
    "../assets/img/login/slider/1.png",
    "../assets/img/login/slider/1.png",
  ];
  showLogin: boolean = true;
  conversionOutput: any;
  showEventsNews: boolean;
  isLogOut: any;

  constructor(
    private route: ActivatedRoute,
    public toast: ToastrService,
    public cs: MySuggestionService,
    private _formBuilder: FormBuilder,
    public loginService: LoginServiceService,
    public datepipe: DatePipe,
    private router: Router,
    config: NgbCarouselConfig,
    private modalService: NgbModal,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private cookieService: CookieService
  ) {
    this.cookieService.deleteAll();

    config.keyboard = false;

    // config.showNavigationIndicators= true;
    //config.pauseOnHover = false;
    matIconRegistry.addSvgIcon(
      "pinkArrow",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myBusiness/pinkArrow.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "OE_Supplies",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myBusiness/OE_Supplies.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "Twitter",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/login/twitter.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "videoPause",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/login/VideoPlay.svg"
      )
    );
    this.getLoginCMS1();
    this.getLoginCMS2();
    this.getLoginVideoCMS();
    this.initLoginForm();

    // this.loginService.sso().subscribe(res=>{
    //   console.log(res)
    // })
    console.log(this.route.queryParams["emailId"]);
    if (this.route.queryParams["_value"].emailId) {
      this.route.queryParams.subscribe((params) => {
        if (params) {
          if (params["emailId"]) {
            console.log("params found");
            let body = {
              tokenId: params["tokenId"],
              emailId: params["emailId"],
              timeStamp: this.datepipe.transform(this.today, "yyyy-MM-dd"),
            };
            console.log(body);
            this.loginService.sso(body).subscribe((res: any) => {
              if (res.Id == 1) {
                this.ssoAuthenticate(res, btoa(params["tokenId"]));
              } else {
                this.toast.error("User Does Not Exists");
              }
              return res;
            });
          } else {
            this.toast.error("User Does Not Exists");
          }
        }
      });
    }
  }

  ssoLogin() {
    window.open("https://supplier.mahindra.com/SSO/", "_self");
  }

  openModalpdf(link) {
    // alert(link);
    window.open(link, "_blank");
  }
  initLoginForm() {
    this.loginForm = this._formBuilder.group({
      type: ["1", [Validators.required]],
      userName: ["", [Validators.required]],
      password: ["", [Validators.required]],
      reqOtp: ["", [Validators.required]],
    });
  }
  openModal1(data) {
    console.log("------" + data);

    const modalRef = this.modalService.open(WhatsNewModalComponent, {
      size: "lg",
    });
    modalRef.componentInstance.passedData = data;
    modalRef.componentInstance.pageName = "Login";
  }
  getLoginCMS1() {
    //  alert(this.today);
    let latest_date = this.datepipe.transform(this.today, "yyyy-MM-dd");
    // alert(latest_date);
    let postData = {
      // "TodaysDate":latest_date,
      PageNo: 1,
      ContentCategoryID: 5, //5
      Mode: "LOGIN",
    };
    this.loginService.getLoginCMS(postData).subscribe(
      (resp: any) => {
        console.log("get LoginCMS here-->", resp);

        // this.CMSLogin       = resp.ResponseData;
        // this.CMSLatestVideo = resp.ResponseData._GetCMSLatestVideo;
        for (var i = 0; i < resp.ResponseData.length; i++) {
          console.log("iii");
          let data ={
            img : resp.ResponseData[i]._Get_Img_Video[0].ImagePath,
            CMSID : resp.ResponseData[i].CMSID
          }
          this.CMSLogin.push(data);
          console.log("---", this.CMSLogin);
          // this.readJSON =
          // { "imgArray": [
          //   {"img": resp.ResponseData[i]._Get_Img_Video[0].ImagePath, "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
          //   // {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
          //   // {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
          //   // {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
          //   // {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
          //   // {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
          //   // {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"}
          //   ]
          //   };
        }

        // this.CMSLogin = this.readJSON.imgArray;
        console.log("cms login array" + JSON.stringify(this.CMSLogin));
      },
      (err) => {}
    );
  }
  getLoginCMS2() {
    let latest_date = this.datepipe.transform(this.today, "yyyy-MM-dd");

    let postData = {
      //  "TodaysDate":latest_date,
      PageNo: 1,
      ContentCategoryID: 6,
      Mode: "LOGIN",
    };
    this.loginService.getLoginCMS(postData).subscribe(
      (resp: any) => {
        console.log("get LoginCMS her e-->", resp);
        this.newsEvents = resp.ResponseData;

        // this.readJSON = { "imgArray": [
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"}
        //   ]
        //   };
        //  this.newsEvents = this.readJSON.imgArray;
      },
      (err) => {}
    );
  }

  getLoginVideoCMS() {
    let latest_date = this.datepipe.transform(this.today, "yyyy-MM-dd");

    let postData = {
      //   "TodaysDate":latest_date,
      PageNo: 1,
      ContentCategoryID: 7,
      Mode: "LOGIN",
    };
    this.loginService.getLoginCMS(postData).subscribe(
      (resp: any) => {
        this.CMSLatestVideo = resp.ResponseData;

        // this.videoss = resp.ResponseData[0].Link;
        // alert(this.CMSLatestVideo);

        // this.readJSON = { "imgArray": [
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"},
        //   {"img": "../assets/img/login/NewsImage.png", "heading" :"New Base W3 Variant in XUV500!", "date":"May 14,2019", "description":"Mahindra & Mahindra Ltd, a part of the US $20.7 billion Mahindra Group,today announced the"}
        //   ]
        //   };
        //  this.newsEvents = this.readJSON.imgArray;
      },
      (err) => {}
    );
  }

  getRouterArray() {
    let body = {
      RoleID: this.cs.roleID,
    };
    this.cs
      .postMySuggestionData("menuadmin/CheckDirectAccessMenu", body)
      .subscribe((res: any) => {
        if (!this.cs.isUndefinedORNull(res) && res.Message == "Success") {
          res.ResponseData.push("create-user-profile");
          res.ResponseData.push("Supplier-meet-landing");
          res.ResponseData.push("Supplier-meet-home");
          if (this.cs.roleID != 7) {
            res.ResponseData.push("my-library");
            res.ResponseData.push("GSTInfo");
          }
          localStorage.setItem("route_array", res.ResponseData);
        }
      });
  }

  getMainSideMenu() {
    let Request = {
      RoleID: this.cs.roleID,
    };
    this.cs
      .postMySuggestionData("menuadmin/GetLeftMenuByRole", Request)
      .subscribe((data: any) => {
        if (data.Message == "Success")
          localStorage.setItem("SideMenus", JSON.stringify(data.ResponseData));
        else
          this.cs.showError(
            "Error while fetching sidemenu please try again later"
          );
      });

    this.cs
      .postMySuggestionData("Analytics/GetFavouriteClicks", {
        UserName: this.userToken,
      })
      .subscribe((data: any) => {
        console.log(data)
        if (data.ID == 1)
          localStorage.setItem("favourites", JSON.stringify(data.ResponseData));
      });
  }

  datata: boolean;

  ngOnInit() {
    this.createBulkUploadForm();
    if (this.cs.isLogOut) {
      window.location.reload();
      this.cs.isLogOut = false;
    }
    localStorage.clear();
    setInterval(() => {
      this.myCarousel.next();
    }, 5000);
    setInterval(() => {
      this.loginCarousel.next();
    }, 5000);
  }

  openLg(content) {
    this.modalService.open(content, { size: "lg" });
  }

  getLoginDetailsByUserName(userName: any) {
    let postData = {
      UserName: userName,
    };

    this.cs
      .postMySuggestionData("VendorEmployee/VendorDetailsByUserName", postData)
      .subscribe(
        (res: any) => {
          if (!this.cs.isUndefinedORNull(res)) {
            this.cs.loginDetails = res.ResponseData;
            console.log("Login Details is :", this.cs.loginDetails);
          } else {
            alert("Something went wrong!");
          }
        },
        (error) => {
          console.log(error.message);
        }
      );
  }

  ssoAuthenticate(resp, tokenId) {
    console.log(resp.ResponseData);

    if (resp.ResponseData && resp.ResponseData[0].TokenId) {
      let userToken = resp.ResponseData[0].TokenId;
      this.userToken = resp.ResponseData[0].TokenId;
      this.cs.roleID = resp.ResponseData[0].RoleId;

      localStorage.setItem(
        "WkZoT2JHTnNVblpoTWxaMQ==",
        CryptoJS.AES.encrypt(userToken, "").toString()
      );
      localStorage.setItem(
        "WTIwNWMxcFZiR3M9",
        CryptoJS.AES.encrypt(resp.ResponseData[0].RoleId, "").toString()
      );
      localStorage.setItem(
        "WW0xR2RGcFJQVDA9",
        CryptoJS.AES.encrypt(resp.ResponseData[0].name, "").toString()
      );
      localStorage.setItem(
        "SM",
        CryptoJS.AES.encrypt(resp.ResponseData[0].SRMUSERNAME, "").toString()
      );
      localStorage.setItem(
        "WkcxV2RWcEhPWGxSTWpscldsRTlQUT09",
        CryptoJS.AES.encrypt(resp.ResponseData[0].VendorCode, "").toString()
      );
      localStorage.setItem(
        "VlROV2QyTkhlSEJhV0VwUFdWY3hiQT09",
        CryptoJS.AES.encrypt(resp.ResponseData[0].SupplierName, "").toString()
      );
      localStorage.setItem(
        "VkZjNWFXRlhlR3hVYmxaMFdXMVdlUT09",
        CryptoJS.AES.encrypt(resp.ResponseData[0].MobileNumber, "").toString()
      );
      localStorage.setItem(
        "V2xjeGFHRlhkejA9",
        CryptoJS.AES.encrypt(resp.ResponseData[0].EmailID, "").toString()
      );
      localStorage.setItem("JWT", resp.ResponseData[0].JWTAuthToken);
      if (!this.cs.isUndefinedORNull(resp.ResponseData[0].PrimaryRoleID)) {
        localStorage.setItem(
          "PR",
          CryptoJS.AES.encrypt(
            resp.ResponseData[0].PrimaryRoleID.toString(),
            ""
          ).toString()
        );
        localStorage.setItem(
          "DG",
          CryptoJS.AES.encrypt(
            resp.ResponseData[0].RoleName.toString(),
            ""
          ).toString()
        );
        localStorage.setItem(
          "MV",
          CryptoJS.AES.encrypt(
            resp.ResponseData[0].MasterVendorCode,
            ""
          ).toString()
        );
      }

      this.cs.userDetails = resp.ResponseData[0];
      // localStorage.setItem(btoa('userDetails'),JSON.stringify(btoa(resp.ResponseData[0])));
      // JSON.parse(localStorage.getItem('userDetails'));
      // localStorage.setItem('userToken',userToken);
      // localStorage.setItem('roleId',resp.ResponseData[0].RoleId);
      // localStorage.setItem('name',resp.ResponseData[0].name)
      // localStorage.setItem('vendorCode',resp.ResponseData[0].VendorCode);
      // localStorage.setItem('userDetails',JSON.stringify(resp.ResponseData[0]));
      // this.cs.userDetails = JSON.parse(localStorage.getItem('userDetails'));
      this.getRouterArray();
      this.getMainSideMenu();
      this.getBrowserName();

      let userClick = localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ==");
      userClick = CryptoJS.AES.decrypt(userClick, "").toString(
        CryptoJS.enc.Utf8
      );

      let analysticsRequest = {
        userClicked: userClick,
        device: "windows",
        browser: localStorage.getItem("browser"),
        moduleType: "LN",
        module: "Login",
      };
      this.cs
        .postMySuggestionData("Analytics/InsertAnalytics", analysticsRequest)
        .subscribe();
      this.showLogin = true;
      if (resp.ResponseData[0].RoleId != 7)
        this.router.navigateByUrl("dashboard");
      else this.router.navigateByUrl("adminDashboard");
    } else {
      this.toast.error("SSO Login Failed");
    }
  }
  loginModal() {
    // alert("---")
    this.modalService.dismissAll();
    // this.modal.dismiss();
  }

  authenticateUser() {
    this.cookieService.deleteAll();
    let yes = btoa("Yes");
    let isUserLogin = btoa("isUserLogin");
    if (
      !this.loginForm.get("userName").value &&
      !this.loginForm.get("password").value
    ) {
      this.unameempty = true;
    } else {
      this.unameempty = false;
      this.today = new Date();
      this.showLogin = false;
      let latest_date = this.datepipe.transform(
        this.today,
        "yyyy-MM-dd hh:mm:ss"
      );
      let postData = {
        TokenId: this.loginForm.get("userName").value,
        PassWord: this.loginForm.get("password").value,
        Timestamp: latest_date,
      };
      this.loginService.authenticate(postData).subscribe(
        (resp: any) => {
          if (resp.ResponseData) {
            this.cs.roleID = resp.ResponseData[0].RoleId;
            let flag = resp.ResponseData[0].IsOTPAllow;
            if(flag == 'Y') {
              this.otpFlagShow = true;
              this.resendOTPFlag =  true;
            }   
            else if(flag == 'N'){
              if (resp.ResponseData[0].RoleId == 1082)
              this.router.navigateByUrl("supplierDataUpload");
           else if (resp.ResponseData[0].RoleId != 7)
              this.router.navigateByUrl("dashboard");
             else this.router.navigateByUrl("adminDashboard");
            }
            localStorage.setItem(
              "WkZoT2JHTnNVblpoTWxaMQ==",
              CryptoJS.AES.encrypt(
                this.loginForm.get("userName").value,
                ""
              ).toString()
            );
            this.userToken = this.loginForm.get("userName").value;
            localStorage.setItem(
              "WTIwNWMxcFZiR3M9",
              CryptoJS.AES.encrypt(resp.ResponseData[0].RoleId, "").toString()
            );
            localStorage.setItem(
              "WW0xR2RGcFJQVDA9",
              CryptoJS.AES.encrypt(resp.ResponseData[0].name, "").toString()
            );
            localStorage.setItem(
              "WkcxV2RWcEhPWGxSTWpscldsRTlQUT09",
              CryptoJS.AES.encrypt(
                resp.ResponseData[0].VendorCode,
                ""
              ).toString()
            );
            localStorage.setItem(
              "VlROV2QyTkhlSEJhV0VwUFdWY3hiQT09",
              CryptoJS.AES.encrypt(
                resp.ResponseData[0].SupplierName,
                ""
              ).toString()
            );
            localStorage.setItem(
              "VkZjNWFXRlhlR3hVYmxaMFdXMVdlUT09",
              CryptoJS.AES.encrypt(
                resp.ResponseData[0].MobileNumber,
                ""
              ).toString()
            );
            localStorage.setItem(
              "V2xjeGFHRlhkejA9",
              CryptoJS.AES.encrypt(resp.ResponseData[0].EmailID, "").toString()
            );
            localStorage.setItem(
              "SM",
              CryptoJS.AES.encrypt(
                resp.ResponseData[0].SRMUSERNAME,
                ""
              ).toString()
            );
            localStorage.setItem("JWT", resp.ResponseData[0].JWTAuthToken);
            if (
              !this.cs.isUndefinedORNull(resp.ResponseData[0].PrimaryRoleID)
            ) {
              localStorage.setItem(
                "PR",
                CryptoJS.AES.encrypt(
                  resp.ResponseData[0].PrimaryRoleID.toString(),
                  ""
                ).toString()
              );
              localStorage.setItem(
                "DG",
                CryptoJS.AES.encrypt(
                  resp.ResponseData[0].RoleName.toString(),
                  ""
                ).toString()
              );
              localStorage.setItem(
                "MV",
                CryptoJS.AES.encrypt(
                  resp.ResponseData[0].MasterVendorCode,
                  ""
                ).toString()
              );
              localStorage.setItem("maintainanceFlag","false");
            }
            this.cs.userDetails = resp.ResponseData[0];
            this.getRouterArray();
            this.getMainSideMenu();
            this.getBrowserName();

            let userClick = localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ==");
            userClick = CryptoJS.AES.decrypt(userClick, "").toString(
              CryptoJS.enc.Utf8
            );

            let analysticsRequest = {
              userClicked: userClick,
              device: "",
              browser: localStorage.getItem("browser"),
              moduleType: "LN",
              module: "Login",
            };
            this.cs
              .postMySuggestionData(
                "Analytics/InsertAnalytics",
                analysticsRequest
              )
              .subscribe();
          //     if (resp.ResponseData[0].RoleId == 1082)
          //     this.router.navigateByUrl("supplierDataUpload");
          //  else if (resp.ResponseData[0].RoleId != 7)
          //     this.router.navigateByUrl("dashboard");
          //    else this.router.navigateByUrl("adminDashboard");
          } else {
            this.errorMessage = resp.Message;
          }
        },
        (err) => {
          console.log(err);
        }
      );
    }
  }

  getBrowserName() {
    const agent = window.navigator.userAgent.toLowerCase();
    switch (true) {
      case agent.indexOf("edge") > -1:
        return localStorage.setItem("browser", "edge");
      case agent.indexOf("opr") > -1 && !!(<any>window).opr:
        return localStorage.setItem("browser", "opera");
      case agent.indexOf("chrome") > -1 && !!(<any>window).chrome:
        return localStorage.setItem("browser", "chrome");
      case agent.indexOf("trident") > -1:
        return localStorage.setItem("browser", "ie");
      case agent.indexOf("firefox") > -1:
        return localStorage.setItem("browser", "firefox");
      case agent.indexOf("safari") > -1:
        return localStorage.setItem("browser", "safari");
      default:
        return localStorage.setItem("browser", "other");
    }
  }

  // myCarousel1:any

  ngAfterViewInit(): void {
    // @ts-ignore
    twttr.widgets.load();

    // this.myCarousel = false;
    // setTimeout(() => {
    //   this.myCarousel = true;
    // }, 1000);
  }

  opentwitter() {
    window.open("https://twitter.com/hsikka1?ref_src=twsrc%5Etfw", "_blank");
  }
  validateUser(byPassOtp?: any) {
    if (this.loginForm.get("reqOtp").value || byPassOtp) {
      this.otpreq = false;
      let postOtpData = {
        TokenId: this.userToken,
        OTP: this.loginForm.get("reqOtp").value || byPassOtp,
      };
      this.loginService.validateOTP(postOtpData).subscribe((respOtp: any) => {
        console.log(respOtp);
        if(respOtp.ResponseData[0].status == 1){
        let role = localStorage.getItem("WTIwNWMxcFZiR3M9");
        role = CryptoJS.AES.decrypt(role, "").toString(CryptoJS.enc.Utf8);
        let roleid: any = role;
        // if (roleid != 7) this.router.navigateByUrl("dashboard");
        // else this.router.navigateByUrl("adminDashboard");

        if (roleid == 1082)
              this.router.navigateByUrl("supplierDataUpload");
           else if (roleid != 7)
              this.router.navigateByUrl("dashboard");
             else this.router.navigateByUrl("adminDashboard");
         }else{
           this.toast.error(respOtp.Message);
           this.loginForm.get("reqOtp").setValue('');
          //  this.router.navigateByUrl("login");
          // this.router.navigateByUrl("adminDashboard");
         }
      });
    } else {
      this.otpreq = true;
    }
  }
  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  sentOTP() {
    if(this.resendOTPFlagCount == 2) {
      this.resendOTPFlag = false;
      this.otpFlagShow   = false;
      this.otpSentMessage= false;
      this.resendOTPFlagCount = 0;
     }
    else {
    let body = {
      TokenId: this.userToken,
    }; 
    this.loginService.sendOtp(body).subscribe((respOtp: any) => {
      console.log(respOtp);
      this.resendOTPFlagCount ++;
    //   if(this.resendOTPFlagCount == 2) {
    //   this.resendOTPFlag = false;
    //   this.otpFlagShow   = false;
    //   this.otpSentMessage= false;
    //   this.resendOTPFlagCount = 0;
    //  }
    //  else
    if(respOtp.Id == 0 || respOtp.Id == 2)
      this.toast.error(respOtp.Message);
      this.otpSentMessage = true;
    });
   }
  }

  openNewModal() {
    this.modalService.open(LoginModalComponent);
  }

  openSupplierData() {
    window.open(
      "https://cordys2.mahindra.com:8443/home/Mahindra/NVR_HTML/NVR_home_custom.htm",
      "_blank"
    );
  }
  openTrainingData() {
    window.open("http://msetulms.com/msetu_lms/login/index.php", "_blank");
  }

// code for uploading bulk data through modal
banner_name ="Stake Holder bulk upload";
excel_headers:any=[];
bulk_Upload_form:any;
Excel_File = [];
openBulkUploadModal(content) {
  this.modalService.open(content, {  size: 'xl' as 'lg',centered:true, windowClass : "formModal"});
}
closeModal(){
  this.modalService.dismissAll();
}
createBulkUploadForm(){
  this.bulk_Upload_form = this._formBuilder.group({
    Bulk_Upload: [{ value: '', disabled: true }, Validators.required]
  });
}
detectFiles(evt,form) {
  form.resetForm();
  this.excel_headers=["SupplierCode","Assessor","ScheduledAuditDate","SchID","Category","DbCommodity","Supplier_Name"];
  let target: any = <any>(evt.target);
  var headers = [];
  for (let file of target.files) {
    let ext = file.name.split(".").pop().toLowerCase();
    let reader = new FileReader();
    reader.onload = (e: any) => {
      if (ext != 'xlsx') {
        this.cs.showError("Kindly select file with extension '.xlsx");
        return false;
      }
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      var range = XLSX.utils.decode_range(ws['!ref']);
      var C, R = range.s.r;
      let excel_headers = this.excel_headers;
      excel_headers = excel_headers.map(v => v.toLowerCase().replace(/ /g, ""));
      for (C = range.s.c; C <= range.e.c; ++C) {
        var cell = ws[XLSX.utils.encode_cell({ c: C, r: R })]
        var hdr = "UNKNOWN " + C;
        if (cell && cell.t && excel_headers.includes(cell.v.toLowerCase().replace(/ /g, "")))
          hdr = XLSX.utils.format_cell(cell);
        else {
          this.cs.showError("Kindly refer/used sample template and try to upload")
          return false;
        }
        // headers.push(hdr.toLowerCase().replace(/ /g, ""));
        headers.push(hdr);
      }
      this.Upload(target.files[0], evt,headers,form)
    }
    reader.readAsBinaryString(target.files[0]);
  }
}

Upload(data, evt,headers,form) {
  let fileReader = new FileReader();
  fileReader.onload = (e) => {
    let arrayBuffer: any = fileReader.result;
    var data = new Uint8Array(arrayBuffer);
    var arr = new Array();
    for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
    var bstr = arr.join("");
    var workbook = XLSX.read(bstr, { type: "binary" });
    var first_sheet_name = workbook.SheetNames[0];
    var worksheet = workbook.Sheets[first_sheet_name];
    console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
    let array = [];
    array = XLSX.utils.sheet_to_json(worksheet, { raw: true });
    if (array.length == 0)
      return this.cs.showError("Kindly fill data in excel");
    else
      this.validateExcel(array, evt,headers,form);
  }
  fileReader.readAsArrayBuffer(data);
}

validateExcel(array, evt,headers,form) {
  for (let i = 0; i < this.excel_headers.length; i++) {
    for (let j = 0; j < array.length; j++) {
      if (this.cs.isUndefinedORNull(array[j][headers[i]])) {
        this.cs.showError("Please enter values under " + this.excel_headers[i] + " at line number " + (j + 2));
        return false;
      }
    }
  }
  this.Excel_File = [];
  this.Excel_File.push(evt.target.files[0]);
}
InsertStakeholderData(form: any,closeModalonclick) {
  let formData;
  let url;
 
    url='SRMSSO/BulkUploadScheduleAudit';   
    formData = new FormData;
    formData.append('ExcelFile', this.Excel_File[0]);
    // formData.append('CreatedBy', this.username);// need to add from login api

    this.cs.postMySuggestionData(url, formData).
      subscribe(
        (res: any) => {
          this.cs.showSwapLoader = false;
          this.cs.showSuccess(res.Message);
          if(closeModalonclick)
          closeModalonclick._elementRef.nativeElement.click();
          form.resetForm();

        },
        (error) => {
          this.cs.showSwapLoader = false;
          console.log(error.message);
        }
      )
}
DownloadSampleTemplate(filename:any){
  window.open(`${'../../assets/'}${filename}${'.xlsx'}`, '_blank');
  // window.open('../../assets/Navigation Sample.xlsx')
}
signInBaanner(){
  window.open("https://experiencenext.in/Furio7/MahindraLCVlaunch/login.html","_Blank")
}
}
