import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MSetuPageComponent } from './m-setu-page.component';

describe('MSetuPageComponent', () => {
  let component: MSetuPageComponent;
  let fixture: ComponentFixture<MSetuPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MSetuPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MSetuPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
