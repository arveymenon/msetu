import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { SurveyService } from '../services/survey/survey.service';
import { MySurveyModalComponent } from '../modal/my-survey-modal/my-survey-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-my-survey',
  templateUrl: './my-survey.component.html',
  styleUrls: ['./my-survey.component.scss']
})
export class MySurveyComponent implements OnInit {


  surveys = [];
  SurveyForm = new FormArray([]);

  constructor(
    public commonService: CommonUtilityService,
    public surveyService: SurveyService,
    public modal: NgbModal,
    public formBuilder: FormBuilder) {
        this.commonService.changeIsAuthenticate(true);
        let body = {
          "SurveyId":0
        }
        this.surveyService.getMySurveyQuestions('AdminCrudSurvey/GetMySurveyQuestionsList', body).subscribe(res => {
          console.log(res)
          this.surveys = res;
          
      });
  }

    // selfAssessment -> Risk
    // SSI -> My Business
    // my-survey -> my-survey

    username:any
  ngOnInit() {

this.username = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
this.username = CryptoJS.AES.decrypt(this.username,"").toString(CryptoJS.enc.Utf8);
    // this.surveyService.getSurveyQuestions('AdminCrudSurvey/Get_AllQuestionsforSupplier', null).subscribe(res => {
    //   console.log(res);
    //   if (res.Message == "Success") {
    //     this.questions = res.ResponseData;
    //     for (let data of res.ResponseData){
    //       this.SurveyForm.push(this.formBuilder.group({
    //         id: [data.Id],
    //         answer: ['', Validators.required]
    //       }));
    //     }
    //   }
    // });

  }

  openSurveyModal(surveyId) {
    console.log(surveyId);
    this.isSubmitted(surveyId).then(res=>{
      if(res){
        let modal = this.modal.open(MySurveyModalComponent, {  size:'xl' as 'lg' });
        modal.componentInstance.SurveyId = surveyId;
      }else {
        alert('Survey Already Responded')
      }
    })
  }

  
  isSubmitted(surveyId){
    return new Promise((resolve, reject)=>{
      let body = {
        SurveyTypeID: 1,
        SurveyId: surveyId,
        TokenId: this.username// localStorage.getItem('userToken')
     }
      this.surveyService.getMySurveyQuestions('AdminCrudSurvey/Msetu_Get_SurveyRespCheck', body).subscribe(res => {
          console.log(res)
          const response = res.ResponseData == 'Failed' ? true : false
          resolve(response);
        }); 
    })
  }

}
