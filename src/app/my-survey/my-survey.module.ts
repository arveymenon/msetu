import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MySurveyModalComponent } from '../modal/my-survey-modal/my-survey-modal.component';
import { MySurveyComponent } from './my-survey.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/material.module';
import { MatPaginatorModule, MatDialogModule, MatSortModule, MatFormFieldModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { MySurveyRoutingModule } from './my-survey-routing/my-survey-routing.module';

@NgModule({
  declarations: [
    MySurveyComponent,
    MySurveyModalComponent
  ],
  imports: [
    CommonModule,
    MySurveyRoutingModule,

    MaterialModule,
    FormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSortModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
  ],
  entryComponents: [
    MySurveyModalComponent
  ]
})

export class MySurveyModule { }