import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MySurveyComponent } from '../my-survey.component';
import { AuthGuard } from 'src/app/services/auth-guards/auth.guard';


const routes: Routes = [
  {path:'',component: MySurveyComponent,  canActivate: [AuthGuard],
  data: { roles: ['mySurvey'] }},
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MySurveyRoutingModule { }

