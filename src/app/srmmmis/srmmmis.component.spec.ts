import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SRMMMISComponent } from './srmmmis.component';

describe('SRMMMISComponent', () => {
  let component: SRMMMISComponent;
  let fixture: ComponentFixture<SRMMMISComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SRMMMISComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SRMMMISComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
