import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { NavigationExtras, Router } from '@angular/router';
import * as fs from "file-saver";
import { Workbook } from "exceljs";

import * as CryptoJS from "crypto-js";
import * as moment from 'moment';

@Component({
  selector: 'app-srmmmis',
  templateUrl: './srmmmis.component.html',
  styleUrls: ['./srmmmis.component.scss']
})
export class SRMMMISComponent implements OnInit {
  roleId = CryptoJS.AES.decrypt(
    localStorage.getItem("WTIwNWMxcFZiR3M9"),
    ""
  ).toString(CryptoJS.enc.Utf8);
  DataSource = [];
  displayedColumns: string[] = ['supplierCode', 'supplierName', 'Email', 'Rating', 'assessmentOn', 'Assessor', 'Status', 'AuditSubmitted', 'AuditCompleted', 'Commodity', 'ViewResponse'];
  assessor_data=[];

  constructor(public cs: MySuggestionService,public router:Router,private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer, public commonService: CommonUtilityService, private modalService: NgbModal) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "viewIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/View_Icon_Red.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      'edit',
      domSanitizer.bypassSecurityTrustResourceUrl(
        '../assets/Icons/innerPages/Edit.svg'));
    matIconRegistry.addSvgIcon('excelIcon', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/Excel_White_Icon.svg'));
    matIconRegistry.addSvgIcon('trash', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/delete.svg'));
  }

  master_details: any;

  ngOnInit() {
    this.getMasterDetails()
  }

  getMasterDetails(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.postMySuggestionData('SRMM/GetSRMM_MIS_MasterDetails', '').subscribe((res: any) => {
        if (!this.cs.isUndefinedORNull(res.ResponseData)) {
          this.master_details = res.ResponseData;
          resolve();
        }
        else {
          this.cs.showError("Failed to load data");
          return;
        }
      })
    })
  }

  getAssessor(year){
    let Request=
    {
    "Year":year,
    "Status":"ALL",
    "RatingValue":1.0,
    "Commodity":"ALL",
    "Accessor":"ALL"
    }
    this.searchMISReport();
    this.cs.postMySuggestionData('SRMM/GetCommodityWiseAssessor',Request).subscribe((res:any)=>{
      if (!this.cs.isUndefinedORNull(res.ResponseData)) {
        this.assessor_data = res.ResponseData;
        
      }
      else {
        this.cs.showError("Failed to load data");
        return;
      }
    })
  }

  year:any='';
  Assessor:any='';
  Commodity:any='';
  Status:any='ALL';
  Rating:any=0;

  searchMISReport(){
    let Request;
    // alert(this.Rating);
    let StatusFlag;
    if(this.Status =='ALL')
    StatusFlag = '';
    else
    StatusFlag = this.Status;
   
    // else if(this.Rating )
if(this.Rating =='0'){
   Request=
  {
  "Year":this.year,
  "Status":StatusFlag,
  "Rating1":'',
  "Rating2":'',
  "Commodity":this.Commodity,
  "Accessor":this.Assessor
}
}
  else{
     Request=
    {
    "Year":this.year,
    "Status":StatusFlag,
    "Rating1":this.Rating,
    "Rating2":this.Rating + 1,
    "Commodity":this.Commodity,
    "Accessor":this.Assessor
    }
  }
    this.cs.postMySuggestionData('SRMM/GetSRMM_MIS',Request).subscribe((res:any)=>{
      if (!this.cs.isUndefinedORNull(res.ResponseData)) {
        this.DataSource = res.ResponseData;
      }
      else {
        this.DataSource =[];
        this.cs.showError("Failed to load data");
        return;
      }
    })
  }

  exportExcel(){
  //  this.cs.exportJsonAsExcelFile(this.DataSource,'MIS Report');
    let dataNew = this.DataSource;
    let dataForExcel=[];
    for (const data of dataNew) {
        dataForExcel.push({
              supplierCode: data.SupplierCode,
              supplierName: data.SupplierName,
              Email: data.Email,
              Rating: data.Rating,
              assessmentOn: moment(data.CompletionDate).format('DD-MM-YYYY'),
              Assessor: data.Assessor,
              Status: data.Status,
              AuditSubmitted: data['Audit Submitted by Supplier'],
              AuditCompleted: data['Audit Submitted by Assessor'],
              Commodity: data.Commodity,
              // AssessmentType: data.AssessmentType,
   
              // Mode: data.Mode,
             
            });
          }
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('SRMM-MISReport');
     //Add Header Row
     let header: any 
    //  if(this.CategoryName == 'Retro Debit Note' || this.CategoryName == 'Retro Credit Note'){
    //    header = Object.keys(dataNew[0])
    //  } else {
    //    header = this.displayedColumns
    //  }
    header=['Supplier Code',	'Supplier Name',	'Email',	'Rating',	'Assessment On',	'Assessor',	'Status',	'Audit Submitted by Supplier', 	'Audit completed by Assessor',	'Commodity']
   let headerRow = worksheet.addRow(header);
      // Cell Style : Fill and Border
  headerRow.eachCell((cell, number) => {
   cell.fill = {
     type: 'pattern',
     pattern: 'solid',
     fgColor: { argb: '00FF0000' },
     bgColor: { argb: '00FF0000' }
   }
   cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
  })
  dataForExcel.forEach(d => {
   var values = Object.keys(d).map(function (key) { return d[key]; });
   console.log(values);
  //  if(this.CategoryName == 'LR & RA')
   // values.splice(12,1);
   // values.splice(13,1);
  //  if(this.CategoryName == 'VQI')
  //  [values[0], values[1]] = [values[1], values[0]];
    let row = worksheet.addRow(values);
  });
   workbook.xlsx.writeBuffer().then((dataForExcel) => {
     let blob = new Blob([dataForExcel], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
     fs.saveAs(blob,'SRMM-MIS.xlsx');
    
  });
  }

  navigate(data){
    let vcode = btoa(data.SupplierCode);
    let navigationExtras: NavigationExtras = {
      queryParams: {
          "VC": vcode,
          "FY": this.year
      }
  };
  this.router.navigate(["riskDashboard"], navigationExtras);
  }


  closeModal() {
    this.modalService.dismissAll();
  }
  openModal(content) {
    this.modalService.open(content, { centered: true, windowClass: "formModal" });
  }

}
