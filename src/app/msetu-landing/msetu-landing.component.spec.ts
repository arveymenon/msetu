import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MsetuLandingComponent } from './msetu-landing.component';

describe('MsetuLandingComponent', () => {
  let component: MsetuLandingComponent;
  let fixture: ComponentFixture<MsetuLandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MsetuLandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MsetuLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
