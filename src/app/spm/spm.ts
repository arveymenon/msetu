import { Component, OnInit } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { FormControl, Validators } from "@angular/forms";

import * as XLSX from "xlsx";
import * as moment from "moment";

import { NgxIndexedDBService } from "ngx-indexed-db";
import { ToastrService } from "ngx-toastr";
import { MyHelpDeskService } from "../services/myHelpDesk/my-help-desk.service";
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import * as CryptoJS from "crypto-js";

@Component({
  selector: "app-spm",
  templateUrl: "./spm.component.html",
  styleUrls: ["./spm.scss"],
})
export class SPMComponent implements OnInit {
  userToken = CryptoJS.AES.decrypt(
    localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),""
    ).toString(CryptoJS.enc.Utf8);

  pdfs = new FormControl("", Validators.required);
  errors = [];

  toBeSentExcel: any;

  Files = [];
  uploadedFileCount = 0;
  uploading: boolean = false;

  excel_headers = ["Vendor", "Month", "Sector", "Rating"];
  sbs_excel_headers = ["Vendor", "Month", "Zone", "Sector", "Rating"];

  constructor(
    public commonService: CommonUtilityService,
    private dbService: NgxIndexedDBService,
    public httpService: MyHelpDeskService,
    public toastr: ToastrService,
    public cs: MySuggestionService
  ) {
    this.commonService.changeIsAuthenticate(true);
    // this.dbService.clear('performace_file_queue')
    this.dbService.getAll("performace_file_queue").then(
      (files) => {
        console.log(files);
        if (files.length) {
          this.Files = files;
          this.getUploadedFilesCount();
          this.uploadedFileCount = this.getUploadedFilesCount();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  deleteAll() {
    this.uploadedFileCount = 0;
    this.Files = [];
    this.uploading = false;
    this.dbService.clear("performace_file_queue");
  }

  detectFiles($event) {
    console.log($event);
    this.getUploadedFilesCount();

    for (const file of $event.target.files) {
      let myReader = new FileReader();
      myReader.onloadend = (e) => {
        let stringfile = myReader.result.toString();
        console.log(stringfile);

        let fileObject = {
          file: file,
          uploaded: false,
        };

        this.dbService.add("performace_file_queue", fileObject).then(
          (response) => {
            this.dbService.getAll("performace_file_queue").then((files) => {
              this.Files = files;
            });
            console.log(response);
          },
          (error) => {
            console.log(error);
          }
        );
      };
      myReader.readAsDataURL(file);
    }

    console.log(this.Files);
  }

  uploadPdfs() {
    this.uploading = true;
    this.Files.forEach((pdf, index) => {
      setTimeout(() => {
        console.log(pdf);
        if (pdf.uploaded == false) {
          console.log(pdf);
          // call API here

          // let byteArray = new Uint8Array(atob(pdf.file).split('').map(char => char.charCodeAt(0)));
          // let file = new Blob([byteArray], {type: 'application/pdf'});

          let formData = new FormData();
          let user = this.userToken
          formData.append("User", user);
          formData.append("FileContent", pdf.file);

          this.httpService
            .call("PerformanceRating/SRMRatingFile", formData)
            .subscribe((res) => {
              console.log(res);

              // Check if uploading has completed;
              console.log(index);
              console.log(this.Files.length);
              if (this.Files.length - 1 == index) {
                this.uploading = false;
              }

              if (res.STATUS == "O") {
                this.dbService
                  .update("performace_file_queue", {
                    id: pdf.id,
                    file: pdf.file,
                    uploaded: true,
                  })
                  .then((res) => {
                    console.log(res);
                    // this.Files.findIndex(o=>o.id == pdf.id)
                    pdf.uploaded = true;

                    if (this.uploadedFileCount == this.Files.length) {
                      this.toastr.success("All Files Uploaded Successfully");
                      this.uploadedFileCount = 0;
                      this.deleteAll();
                    } else {
                      this.uploadedFileCount = this.getUploadedFilesCount();
                    }
                  });
              } else {
                if (res.REMARK == "Invalid File Name") {
                  this.errors.push(pdf);
                  this.dbService.delete("performace_file_queue", pdf.id);
                  this.Files.splice(index, 1);
                  console.log(this.errors);
                }
              }
            });
        }
      }, 2000);
    });
  }

  getUploadedFilesCount() {
    var count = 0;
    this.Files.forEach((file) => {
      count = file.uploaded == true ? count + 1 : count;
    });

    if (count == this.Files.length) {
      this.uploadedFileCount = 0;
      this.deleteAll();
    }

    return count;
  }

  uploadExcel(ev, type?: any) {
    var headers = [];
    for (let file of ev.target.files) {
      let ext = file.name.split(".").pop().toLowerCase();
      let reader = new FileReader();
      reader.onload = (e: any) => {
        if (ext != "xlsx") {
          alert("Kindly select excel file");
          return false;
        }
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: "binary" });
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        var range = XLSX.utils.decode_range(ws["!ref"]);
        var C,
          R = range.s.r;
        // let excel_headers = this.excel_headers;
        if(type == 'SBCS'){
          var excel_headers = this.sbs_excel_headers;
        }else{
          var excel_headers = this.excel_headers;
        }
        excel_headers = excel_headers.map((v) =>
          v.toLowerCase().replace(/ /g, "")
        );
        for (C = range.s.c; C <= range.e.c; ++C) {
          var cell = ws[XLSX.utils.encode_cell({ c: C, r: R })];
          var hdr = "UNKNOWN " + C;
          if (
            cell &&
            cell.t &&
            excel_headers.includes(cell.v.toLowerCase().replace(/ /g, ""))
          )
            hdr = XLSX.utils.format_cell(cell);
          else {
            alert("Kindly refer/used sample template and try to upload");
            return false;
          }
          // headers.push(hdr.toLowerCase().replace(/ /g, ""));
          headers.push(hdr);
        }
        this.Upload(ev.target.files[0], type, excel_headers);
      };
      reader.readAsBinaryString(ev.target.files[0]);
    }
  }

  Upload(data, type, excel_headers) {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      let arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i)
        arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });

      var all_sheet_names = workbook.SheetNames;
      var first_sheet_name = workbook.SheetNames[0];

      // var worksheet = workbook.Sheets[first_sheet_name];
      // var worksheet2 = workbook.Sheets[all_sheet_names[1]];

      // console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
      // console.log(XLSX.utils.sheet_to_json(worksheet2, { raw: true }));
      let array = [];

      for (let sheet of all_sheet_names) {
        XLSX.utils
          .sheet_to_json(workbook.Sheets[sheet], { raw: true })
          .forEach((element) => {
            array.push(element);
          });
      }

      // array = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      if (array.length == 0)
        return this.toastr.error("Kindly fill data in excel");
      else {
        this.validateExcel(array, type)
      }
    };
    fileReader.readAsArrayBuffer(data);
  }

  validateExcel(array, type) {
    if(type == 'SBCS'){
      var headers = this.sbs_excel_headers;
    }else{
      var headers = this.excel_headers;
    }

    for (let i = 0; i < headers.length; i++) {
      for (let j = 0; j < array.length; j++) {
        if (this.cs.isUndefinedORNull(array[j][headers[i]])) {
          console.log(array[j])
          console.log(headers[i])
          alert(
            "Please enter values under " +
            headers[i] +
              " at line number " +
              (j + 2)
          );
          return false;
        }

        // Date Validation
        console.log(array[j][headers[1]]);
        if (
          i == 1 &&
          !moment(array[j][headers[1]], "DD.YYYY", true).isValid()
          ) {
          console.log(array[j][headers[1]]);
          alert(
            "Invalid date format at line number " +
              (j + 2) +
              ".It should be in format of MM.YYYY. eg. 01.2020"
          );
          return false;
        }else{
          console.log('validated Date')
        }

        // Time Validation
      }
    }
    // array is the variable to be sent
  
    // this.toBeSentExcel = array;
    this.sendExcel(type, array);
  }

  sendExcel(type, array) {

    let user = this.userToken

    if (array && array.length > 0) {
      console.log(array, this.toBeSentExcel);
      let body: any = {
        User: user,
        SRMRATINGSROOT: [],
        RISKMETERTAINGSROOT: [],
        SBCBSCOREROOT: [],
      };

      let api = "";
      if (type == "Performance") {
        api = "PerformanceRating/UPLOADSRMRATINGS";
        for (let value of array) {
          body.SRMRATINGSROOT.push({
            Vendor: value.Vendor,
            MonthYear: value.Month,
            Sector: value.Sector,
            Rating: value.Rating,
          });
        }
      }
      if (type == "Risk") {
        api = "PerformanceRating/UploadRiskMeterRatings";
        for (let value of array) {
          body.RISKMETERTAINGSROOT.push({
            Vendor: value.Vendor,
            Period: value.Month,
            Sector: value.Sector,
            Rating: value.Rating,
          });
        }
      }
      if (type == "SBCS") {
        api = "PerformanceRating/UploadSBCScore";
        for (let value of array) {
          body.SBCBSCOREROOT.push({
            Vendor: value.Vendor,
            Period: value.Month,
            Zone: value.Zone,
            Sector: value.Sector,
            Rating: value.Rating,
          });
        }
      }

      this.httpService.call(api, body).subscribe((res) => {
        console.log(res);
        this.toastr.success("Excel Uploaded Successfully");
      });
    } else {
      this.toastr.error("Kindly Upload A Valid Excel File");
    }
  }

  downloadTemplate(flag? : any) {
    if(flag == 'sbcs'){
      window.open("../../assets/sbcSpmTemplate.xlsx", "_blank");
    }else{
      window.open("../../assets/spmTemplate.xlsx", "_blank");
    }
  }

  ngOnInit() {}
}
