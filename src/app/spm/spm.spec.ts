import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SPMComponent } from './spm';


describe('SPMComponent', () => {
  let component: SPMComponent;
  let fixture: ComponentFixture<SPMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SPMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SPMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
