import { Component, OnInit } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { FormControl } from "@angular/forms";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

import * as CryptoJS from "crypto-js";

@Component({
  selector: "app-user-details",
  templateUrl: "./user-details.component.html",
  styleUrls: ["./user-details.component.scss"],
})
export class UserDetailsComponent implements OnInit {
  public DDLValue = new FormControl();
  public reqRoleId = new FormControl();
  pageNo: any = 1;

  roleId = CryptoJS.AES.decrypt(
    localStorage.getItem("WTIwNWMxcFZiR3M9"),
    ""
  ).toString(CryptoJS.enc.Utf8);

  Vendor_Code_DDL = [];
  roleDetails_DDL = [];

  displayedColumns3 = [
    "SrNo",
    "VendorCode",
    "SupplierName",
    "FirstName",
    "LastName",
    "WorkPhone",
    "CellPhone",
    "WorkEmail",
    "USERNAME",
    "SuppliersToPlants",
    "SuppliersToSectors",
    "CordysUserID",
    "SRMUSERID",
    "ROLES",
    "PRIMARYROLE",
  ];
  updatedEmpDetails = [
  ];

  empDetailsCount = 10
  reqbody: any;

  constructor(
    public common: CommonUtilityService,
    public cs: MySuggestionService,
    public matIconRegistry: MatIconRegistry,
    public domSanitizer: DomSanitizer
  ) {
    this.common.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon('excelIcon', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/Excel_White_Icon.svg'));

    this.cs.postMySuggestionData("Vendor/GetVendorDDL", "").subscribe(
      (res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.Vendor_Code_DDL = res.ResponseData;
          // this.cs.showSuccess("Success");
        } else {
          this.cs.showSwapLoader = false;
          this.cs.showError("Something went wrong please try again later!");
        }
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    );

    this.cs.postMySuggestionData("CRUDMasterRole/GetRoleDDL", "").subscribe(
      (res: any) => {
        this.roleDetails_DDL = res.ResponseData;
      },
      (error) => {
        console.log(error.message);
      }
    );
  }

  ngOnInit() {
    this.getData(1)
  }

  exportAsExcelFile(updatedEmployeeTable,name){
    let body = this.reqbody;
    body.pageNo = 0
    this.cs
      .postMySuggestionData("admin/GetMsetuUserList", body)
      .subscribe((res) => {
        console.log(res.ResponseData);
        this.cs.exportJsonAsExcelFile(res.ResponseData, name)
      });
  }

  reset(){
    this.reqRoleId.reset();
    this.DDLValue.reset();
    this.reqbody = {}
    this.getData(1)
  }

  getData(pageNo) {
    this.reqbody =  {
    "pageNo": pageNo,
    "vendorCode": this.DDLValue.value || null,
    "roleId": this.reqRoleId.value || 0,
    "pageSize": 10
    }    
    let dataSource = [];
    this.cs
      .postMySuggestionData("admin/GetMsetuUserList", this.reqbody)
      .subscribe((res) => {
        console.log(res);
        this.empDetailsCount = res.TotalCount
        for (let data of res.ResponseData) {
          dataSource.push({
            SrNo: data.SrNo,
            VendorCode: data.VendorCode,
            SupplierName: data.SupplierName,
            FirstName: data.FirstName,
            LastName: data.LastName,
            WorkPhone: data.WorkPhone,
            CellPhone: data.CellPhone,
            WorkEmail: data.WorkEmail,
            USERNAME: data.USERNAME,
            SuppliersToPlants: data.SuppliersToPlants,
            SuppliersToSectors: data.SuppliersToSectors,
            CordysUserID: data.CordysUserID,
            SRMUSERID: data.SRMUSERID,
            ROLES: data.ROLES,
            PRIMARYROLE: data.PRIMARYROLE,
          });
        }
    
        this.updatedEmpDetails = [...dataSource];
      });
  }

  getUpdateEmpDetails() {
    console.log("filter");
  }
}
