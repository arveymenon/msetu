import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyHelpdeskComponent } from './my-helpdesk.component';

describe('MyHelpdeskComponent', () => {
  let component: MyHelpdeskComponent;
  let fixture: ComponentFixture<MyHelpdeskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyHelpdeskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyHelpdeskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
