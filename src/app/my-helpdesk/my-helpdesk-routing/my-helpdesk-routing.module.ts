import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MyHelpdeskComponent } from '../my-helpdesk.component';
import { AuthGuard } from 'src/app/services/auth-guards/auth.guard';

const routes: Routes = [
  {path:'',component: MyHelpdeskComponent, canActivate: [AuthGuard],
  data: { roles: ['myHelpdesk'] }}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyHelpdeskRoutingModule { }
