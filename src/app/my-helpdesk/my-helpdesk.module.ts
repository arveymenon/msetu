import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyHelpdeskComponent } from './my-helpdesk.component';

import { HelpdeskConcernComponent } from '../modal/helpdesk-concern/helpdesk-concern.component';
import { NotificationModalComponent } from '../services/myHelpDesk/notification-modal/notification-modal/notification-modal.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/material.module';
import { MatPaginatorModule, MatDialogModule, MatSortModule, MatFormFieldModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { MyHelpdeskRoutingModule } from './my-helpdesk-routing/my-helpdesk-routing.module';

@NgModule({
  declarations: [
    MyHelpdeskComponent,
    HelpdeskConcernComponent,
    NotificationModalComponent
  ],
  entryComponents: [
    HelpdeskConcernComponent,
    NotificationModalComponent
  ],
  imports: [
    CommonModule,

    // RouterModule,
    MyHelpdeskRoutingModule,
    MaterialModule,
    FormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSortModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
  ]
})
export class MyHelpdeskModule { }
