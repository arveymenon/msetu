import { Component, OnInit, ViewChild } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import {
  MatIconRegistry,
  DateAdapter,
  MAT_DATE_FORMATS,
  PageEvent,
  MatPaginator,
} from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { HelpdeskConcernComponent } from "../modal/helpdesk-concern/helpdesk-concern.component";
import { MyHelpDeskService } from "../services/myHelpDesk/my-help-desk.service";

import * as _ from "lodash";
import * as moment from "moment";
import * as XLSX from "xlsx";

import * as fs from "file-saver"

import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import { NotificationModalComponent } from "../services/myHelpDesk/notification-modal/notification-modal/notification-modal.component";
import {
  AppDateAdapter,
  APP_DATE_FORMATS,
} from "../myBusiness/oesupplies/date-format";

import * as CryptoJS from "crypto-js";
import { resolve } from "url";
import { Workbook } from 'exceljs';

@Component({
  selector: "app-my-helpdesk",
  templateUrl: "./my-helpdesk.component.html",
  styleUrls: ["./my-helpdesk.component.scss"],
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: APP_DATE_FORMATS,
    },
  ],
})
export class MyHelpdeskComponent implements OnInit {
  displayFileName = "";
  viewBussinessOptions = false;
  concernCount = {};

  allMasterResponse: any = {};
  createConcernOptions: any = {};
  pageEvent: PageEvent;

  createConcernForm: any = FormGroup;
  createConcernFile: any;
  Files: File[] = [];

  statusControl = new FormControl();
  searchValue = new FormControl();
  searchFromDate = new FormControl();
  searchToDate = new FormControl();

  // vendorcode = ;
  // roleId: any = ;
  // userToken = ;
  vendorcode = CryptoJS.AES.decrypt(
    localStorage.getItem("WkcxV2RWcEhPWGxSTWpscldsRTlQUT09"),
    ""
  ).toString(CryptoJS.enc.Utf8);
  roleId = CryptoJS.AES.decrypt(localStorage.getItem("PR"), "").toString(
    CryptoJS.enc.Utf8
  );
  userToken = CryptoJS.AES.decrypt(
    localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),
    ""
  ).toString(CryptoJS.enc.Utf8);

  vendors = [];

  @ViewChild("paginator") paginator: MatPaginator;
  pageSize = 1000;
  pageNo = 0;
  moment = moment;
  dataSource = [];

  displayedColumns: string[] = [
    "ticket_no",
    "vendor_code",
    "ticket_title",
    "ticket_req_date",
    "category",
    "ticket_username",
    "ticket_assigned_to",
    "ticket_status",
    "action",
  ];

  today = new Date();
  startDate = new FormControl(this.today, Validators.required);
  endDate = new FormControl("", Validators.required);
  endDateMax = new Date();

  excel_headers = ["Name", "RoasterDate", "StartTime", "EndTime", "IsActive"];

  // AGEING REPORT

  IType = new FormControl(1, Validators.required);
  Green = new FormControl(2, Validators.required);
  Orange = new FormControl(3, Validators.required);

  ageingDisplayedColumns: string[] = [
    // "id",
    "status",
    "green",
    "orange",
    "red",
    "total",
  ];

  ageingDataSource = [];

  // array mappings
  plants = [];
  sectors = [];
  bussinessFuntions = [];

  mapping_plant = new FormControl();
  mapping_sector = new FormControl();
  mapping_bussinessFunction = new FormControl();

  create_mapping_sector = new FormControl();

  create_mapping_plant = new FormControl();
  create_mapping_plant_sector = new FormControl();

  create_mapping_bussinessFunction = new FormControl();
  create_mapping_bussinessFunction_plant = new FormControl();

  constructor(
    public commonService: CommonUtilityService,
    public matIconRegistry: MatIconRegistry,
    public domSanitizer: DomSanitizer,
    public modal: NgbModal,
    public myHelpDeskService: MyHelpDeskService,
    public formBuilder: FormBuilder,
    public toastr: ToastrService,
    public cs: MySuggestionService
  ) {
    this.endDateMax.setDate(this.today.getDate() + 365);
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "arrowIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/cmsDashboard/Admin_Iconarrow.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "reopen",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myHelpDesk/reopen.svg"
      )
    );

    matIconRegistry.addSvgIcon(
      "help1",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myHelpDesk/Myhelpdesk_icon1.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "help2",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myHelpDesk/Myhelpdesk_icon2.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "help3",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myHelpDesk/Myhelpdesk_icon3.svg"
      )
    );

    this.myHelpDeskService
      .call("IncidentManagement/Get_Ticket_DashboardCount", {
        vendorCode: this.vendorcode, // localStorage.getItem("vendorCode"),
        roleId: this.roleId, // localStorage.getItem("roleId"),
        userID: this.userToken, // localStorage.getItem("userToken"),
      })
      .subscribe((response) => {
        console.log(response);
        this.concernCount = response.ResponseData;
      });

    this.myHelpDeskService
      .call("IncidentManagement/Get_IncidentMgmtMasterDetails", null)
      .subscribe((response) => {
        console.log(response);
        this.allMasterResponse = _.cloneDeep(response.ResponseData);
        this.createConcernOptions = _.cloneDeep(response.ResponseData);
        this.createConcernOptions._GetIncidentPlantMaster = [];
        this.createConcernOptions._GetIncidentBusinessFunctionMaster = [];
      });
    // this.createConcernForm = this.formBuilder.group({
    //   vendorId: [123, Validators.required],
    //   IssueType: ["", Validators.required],
    //   SectorId: [],
    //   PlantId: [],
    //   BussinessFunction: [],
    //   IssueCategory: ["", Validators.required],
    //   AssignedEmail: [
    //     "",
    //     Validators.compose([
    //       Validators.email,
    //       Validators.pattern(
    //         /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    //       ),
    //     ]),
    //   ],
    //   PhoneNo: [
    //     "",
    //     Validators.compose([
    //       Validators.maxLength(10),
    //       Validators.minLength(10),
    //     ]),
    //   ],
    //   TicketRequestDate: [new Date(), Validators.required],
    //   TicketTitle: ["", Validators.required],
    //   Description: [
    //     "",
    //     Validators.compose([Validators.required, Validators.maxLength(500)]),
    //   ],
    //   fileName: [],
    // });
    this.createConcernForm = this.formBuilder.group({
      vendorId: [123, Validators.required],
      IssueType: ["", Validators.required],
      supplierEmail :["", Validators.pattern(this.emailPattern)],
      supplierPhone : ["",Validators.compose([
        Validators.maxLength(10),
        Validators.minLength(10),
      ])],
      SectorId: [],
      PlantId: [],
      BussinessFunction: [],
      IssueCategory: ["", Validators.required],
      AssignedEmail: ["", Validators.required
                          ],
      PhoneNo: [
        "",
        Validators.compose([
          Validators.maxLength(10),
          Validators.minLength(10),
        ]),
      ],
      TicketRequestDate: [new Date(), Validators.required],
      TicketTitle: ["", Validators.required],
      Description: [
        "",
        Validators.compose([Validators.required, Validators.maxLength(500)]),
      ],
      fileName: [],
    });
    this.myHelpDeskService
      .call("IncidentManagement/GetVendorCodeList", null)
      .subscribe((res) => {
        if (res.Message == "Success") {
          this.vendors = res.ResponseData;
        }
      });
  }
 emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
  ngOnInit() {
    this.getTableValues();
    this.statusControl.valueChanges.subscribe(() => {
      this.getTableValues();
      console.log(this.pageEvent);
      this.paginator.pageIndex = 0;
    });
    this.searchFromDate.valueChanges.subscribe(() => {
      this.getTableValues();
    })
    this.searchToDate.valueChanges.subscribe(() => {
      this.getTableValues();
    })
    this.searchValue.valueChanges.subscribe(() => {
      this.getTableValues();
      console.log(this.pageEvent);
      this.paginator.pageIndex = 0;
    });

    this.startDate.valueChanges.subscribe((res) => {
      console.log(res);
      // this.today =
      let newDate = new Date(res);
      console.log(moment(newDate).add(1, "months"));
      this.endDateMax = new Date(moment(newDate).add(12, "months").toDate());
      console.log(this.endDateMax);
      if (moment(res).isAfter(this.endDate.value)) {
        this.endDate.reset();
      }
      // this.endDate.setValue(this.startDate.value.getDate() + 30)
    });
    this.getMappingSectors();
    this.getMappingPlants();
    this.getMappingBussinessFunction();
  }

  getMappingPlants() {
    this.myHelpDeskService
      .call("IncidentManagement/CRUDIncidentPlant", {
        action: "GETALL",
        pageNo: null,
      })
      .subscribe((res) => {
        this.plants = res.ResponseData;
      });
  }

  getMappingSectors() {
    this.myHelpDeskService
      .call("IncidentManagement/CRUDIncidentSector", {
        action: "GETALL",
        pageNo: null,
      })
      .subscribe((res) => {
        this.sectors = res.ResponseData;
      });
  }

  getMappingBussinessFunction() {
    this.myHelpDeskService
      .call("IncidentManagement/CRUDIncidentBusinessFunction", {
        action: "GETALL",
        pageNo: null,
      })
      .subscribe((res) => {
        this.bussinessFuntions = res.ResponseData;
      });
  }

  getTableValues(pageNo?: any) {
    var body = {
      PageNo: pageNo + 1 || 1,
      statusID: this.statusControl.value || 0,
      SearchValue: this.searchValue.value,
      userName: this.userToken, //  localStorage.getItem("userToken"),
      roleID: this.roleId, // localStorage.getItem("roleId"),
      startDate: this.searchFromDate.value ? moment(this.searchFromDate.value).format('DD/MM/YYYY') : null,
      toDate: this.searchToDate.value ? moment(this.searchToDate.value).format('DD/MM/YYYY') : null
    };
    // this.searchValue.value || this.statusControl.value ? false : (body = null);
    console.log(body);
    this.myHelpDeskService
      .call("IncidentManagement/Get_TicketDetailsList", body)
      .subscribe((response) => {
        console.log(response);
        this.pageSize = response.TotalCount;
        let dataSource = [];
        if(response.ResponseData){
        for (const ticketRequest of response.ResponseData) {
          dataSource.push({
            ticket_no: ticketRequest.TicketReq_RowID,
            unique_ticket_no: ticketRequest.TicketNo,
            vendor_code: ticketRequest.VendorCode,
            ticket_title: ticketRequest.TicketTitle,
            ticket_req_date: ticketRequest.TicketReqDate,
            category: ticketRequest.IncidentCategory_Name,
            ticket_username: ticketRequest.CreatedBy,
            ticket_assigned_to: ticketRequest.AssignedEamilID,
            ticket_status_id: ticketRequest.StatusID,
            ticket_status: ticketRequest.StatusName,
            ticket_allow_reopen: ticketRequest.AllowReopen,
          });
        }
      }
        this.dataSource = [...dataSource];
        console.log(this.dataSource);
      });
  }

  exportAsExcelFile(table: any, name: any) {

    this.getExportToExcelData().then((data: any[]) => {
      let dataNew = data;
      console.log(dataNew);
      //Create workbook and worksheet
      let workbook = new Workbook();
      let worksheet = workbook.addWorksheet("Helpdesk");
      let columns = Object.keys(data[0]);
      let headerRow = worksheet.addRow(columns);
      // Cell Style : Fill and Border
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: "00FF0000" },
          bgColor: { argb: "00FF0000" },
        };
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
      });
      dataNew.forEach((d) => {
        // var obj = JSON.parse(d);
        var values = Object.keys(d).map(function (key) {
          return d[key];
        });
        let row = worksheet.addRow(values);
      });
      workbook.xlsx.writeBuffer().then((dataNew) => {
        let blob = new Blob([dataNew], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        fs.saveAs(
          blob,
          "MyConcern.xlsx"
        );
      });

      // this.cs.exportJsonAsExcelFile(data, name);
      
    });
  }

  getExportToExcelData() {
    return new Promise((res, rej) => {
      var body = {
        PageNo: 0,
        statusID: this.statusControl.value,
        SearchValue: this.searchValue.value,
        userName: this.userToken, // localStorage.getItem("userToken"),
        roleID: this.roleId, //  localStorage.getItem("roleId"),
        startDate: this.searchFromDate.value ? moment(this.searchFromDate.value).format('DD/MM/YYYY') : null,
        toDate: this.searchToDate.value ? moment(this.searchToDate.value).format('DD/MM/YYYY') : null
      };
      // this.searchValue.value || this.statusControl.value ? false : (body = null);
      console.log(body);
      this.myHelpDeskService
        .call("IncidentManagement/Get_TicketDetailsList", body)
        .subscribe((response) => {
          console.log(response);
          this.pageSize = response.TotalCount;
          let tempDataSource = [];
          let dataSource = [];

          for (const ticketRequest of response.ResponseData) {
            dataSource.push({
              // ticket_no: ticketRequest.TicketReq_RowID,
              TicketNo: ticketRequest.TicketNo,
              VendorCode: ticketRequest.VendorCode,
              TicketTitle: ticketRequest.TicketTitle,
              TicketReqDate: ticketRequest.TicketReqDate,
              Category: ticketRequest.IncidentCategory_Name,
              TicketUsername: ticketRequest.CreatedBy,
              TicketAssignedTo: ticketRequest.AssignedEamilID,
              TicketStatus: ticketRequest.StatusName,

              // ticket_status_id: ticketRequest.StatusID,
              // ticket_allow_reopen: ticketRequest.AllowReopen,
            });
          }
          tempDataSource = [...dataSource];
          console.log(tempDataSource);
          res(tempDataSource);
        });
    });
  }

  async selectIssueType(ev) {
    console.log(ev);
    this.createConcernForm.get("SectorId").setValue(null);
    this.createConcernForm.get("PlantId").setValue(null);
    this.createConcernForm.get("BussinessFunction").setValue(null);
    this.viewBussinessOptions = ev.value == 1 ? true : false;
    if (ev.value == 1) {
      this.createConcernForm.get("SectorId").setValidators(Validators.required);
      this.createConcernForm.get("PlantId").setValidators(Validators.required);
      this.createConcernForm
        .get("BussinessFunction")
        .setValidators(Validators.required);
      // this.createConcernForm
      //   .get("AssignedEmail")
      //   .setValidators(Validators.required);
      // this.createConcernForm
      //   .get("PhoneNo")
      //   .setValidators(Validators.required);
      this.createConcernForm
      .get("AssignedEmail")
      .setValidators([Validators.pattern(this.emailPattern),Validators.required]);
    this.createConcernForm
      .get("PhoneNo")
      .setValidators(Validators.required);
    } else {
      this.createConcernForm.get("SectorId").setValidators([]);
      this.createConcernForm.get("PlantId").setValidators([]);
      this.createConcernForm.get("BussinessFunction").setValidators([]);
      this.createConcernForm.get("AssignedEmail").setValidators([]);
      this.createConcernForm.get("PhoneNo").setValidators([]);

    }
    await console.log('update validity')
    this.createConcernForm.get("SectorId").updateValueAndValidity();
    this.createConcernForm.get("PlantId").updateValueAndValidity();
    this.createConcernForm.get("BussinessFunction").updateValueAndValidity();
    this.createConcernForm.get("AssignedEmail").updateValueAndValidity();
    this.createConcernForm.get("PhoneNo").updateValueAndValidity();
    this.createConcernForm.updateValueAndValidity();
  }

  getPlants(sectorId) {
    console.log(sectorId);
    this.createConcernOptions._GetIncidentPlantMaster = this.allMasterResponse._GetIncidentPlantMaster.filter(
      (o) => o.SectorID == sectorId
    );
    this.createConcernForm.get("PlantId").setValue(null);
    console.log(this.createConcernOptions);
  }

  getBussinessFunction(plantId) {
    console.log(plantId);
    this.createConcernOptions._GetIncidentBusinessFunctionMaster = this.allMasterResponse._GetIncidentBusinessFunctionMaster.filter(
      (o) => o.PlantID == plantId
    );
    this.createConcernForm.get("BussinessFunction").setValue(null);
    console.log(this.createConcernOptions);
  }
  viewConcern(ticketRowId,action,statusID) {
    console.log("view concern");
    let modalRef = this.modal.open(HelpdeskConcernComponent, {
      size: "xl" as "lg",
      beforeDismiss: () => {
        this.getTableValues(0);
        return true;
      },
    });
    let data ={
      "ticketRowId" : ticketRowId,
      "action" :action,
      "statusID" : statusID
    }
    modalRef.componentInstance.ticketRowId = data;
  }


  // viewConcern(ticketRowId) {
  //   console.log("view concern");
  //   let modalRef = this.modal.open(HelpdeskConcernComponent, {
  //     size: "xl" as "lg",
  //     beforeDismiss: () => {
  //       this.getTableValues(0);
  //       return true;
  //     },
  //   });
  //   modalRef.componentInstance.ticketRowId = ticketRowId;
  // }

  detectFiles($event) {
    console.log($event);
    console.log($event.target.files[0].name);

    this.Files = $event.target.files;

    this.displayFileName = $event.target.files[0].name;
    this.createConcernFile = $event.target.files[0];
    return false;
  }

  validateFiles(files) {
    let valid = true;
    return new Promise(async (res, rej) => {
      await files.forEach((file) => {
        if (file.size > 5000000) {
          // this.toastr.error('Max File size: 5MB')
          res({ error: false, message: "Max File size: 5MB" });
          valid = false;
          // return false
        }
        const type = file.name.split(".").reverse()[0];
        console.log(type);
        if (
          type != "png" &&
          type != "jpg" &&
          type != "jpeg" &&
          type != "xlsx" &&
          type != "doc" &&
          type != "docx" &&
          type != "msg" &&
          type != "eml" &&
          type != "ppt" &&
          type != "pptx" &&
          type != "pdf"
        ) {
          // this.toastr.error('Kindly upload the mention file type')
          res({ error: false, message: "Kindly upload the mention file type" });
          valid = false;
          res(false);
          // return false
        }
      });
      if (valid == true) {
        await res(true);
      }
    });
  }

  createConcern() {
    console.log(this.createConcernForm);
    let IssueCategory = this.allMasterResponse._GetIncidentSubCategoryMaster.find(
      (o) =>
        o.IncidentSubCategory_Id == this.createConcernForm.value.IssueCategory
    );
    this.createConcernForm.markAsTouched();
    if (this.createConcernForm.valid) {
      const body = {
        statusID: this.createConcernForm.value.IssueType == 1 ? 2 : 1,
        issueTypeID: this.createConcernForm.value.IssueType,
        sectorID: this.createConcernForm.value.SectorId,
        plantID: this.createConcernForm.value.PlantId,
        businessFunID: this.createConcernForm.value.BussinessFunction,
        incidentCategory_Id: IssueCategory.IncidentCategory_Id,
        incidentSubCategory_Id: this.createConcernForm.value.IssueCategory,
        assignedEamilID: this.createConcernForm.value.AssignedEmail,
        contactNo: this.createConcernForm.value.PhoneNo,
        ticketReqDate: this.createConcernForm.get("TicketRequestDate").value,
        ticketTitle: this.createConcernForm.value.TicketTitle,
        ticketDescription: this.createConcernForm.value.Description,
        ConcernEmail :  this.createConcernForm.get("supplierEmail").value,
        ConcernPhone :  this.createConcernForm.get("supplierPhone").value,
        createdBy: this.userToken, // localStorage.getItem("userToken"),
        vendorCode:
          this.roleId == 7
            ? this.createConcernForm.value.vendorId
            : this.vendorcode, // localStorage.getItem("vendorCode"),
        // createdBy: this.roleId == 3 ? localStorage.getItem("userToken") : this.createConcernForm.value.vendorId,
      };
      console.log(body);
      let formData: FormData = new FormData();
      formData.append("reqjson", JSON.stringify(body));

      this.Files.forEach((file: File, index) => {
        formData.append("file" + index, file);
      });

      this.myHelpDeskService
        .call("IncidentManagement/Insert_TicketReqDetails", formData)
        .subscribe((res) => {
          console.log(res);
          if (res.Message == "Success") {
            this.toastr.success("Incident Reported Successfully");
            let modal = this.modal.open(NotificationModalComponent, {
              centered: true,
              size: "lg",
            });
            modal.componentInstance.incidentNumber = res.ResponseData;
            // this.toastr.
            this.displayFileName = "";
            this.createConcernForm.reset();
            if (this.roleId != 7) {
              this.createConcernForm.controls["vendorId"].setValue(123);
            }
            this.createConcernForm
              .get("TicketRequestDate")
              .setValue(new Date());
          }
        });
    } else {
      this.toastr.warning("Kindly Provide Valid Fields");
    }
  }

  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  downloadRoaster() {
    if (this.startDate.valid && this.endDate.valid) {
      this.myHelpDeskService
        .call("IncidentManagement/GetRosterEmployeeBetweenDates", {
          startDate: moment(this.startDate.value).format("DD/MM/YYYY"),
          endDate: moment(this.endDate.value).format("DD/MM/YYYY"),
        })
        .subscribe((res) => {
          if (res.Message == "Success") {
            // this.toastr.show()
            if (res.ResponseData.length > 0) {
              this.cs.exportJsonAsExcelFile(res.ResponseData, "RosterSchedule");
            } else {
              this.cs.exportJsonAsExcelFile(
                [
                  {
                    Name: null,
                    RoasterDate: null,
                    StartTime: null,
                    EndTime: null,
                    IsActive: null,
                  },
                ],
                "RosterSchedule"
              );
            }
          }
        });
    } else {
      this.toastr.error("Kindly Provide Valid Dates");
    }
  }

  uploadRoaster(ev) {
    var headers = [];
    const file = ev.target.files[0];

    let ext = file.name.split(".").pop().toLowerCase();
    let reader = new FileReader();
    reader.onload = (e: any) => {
      if (ext != "xlsx") {
        alert("Kindly select excel file");
        return false;
      }
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: "binary" });
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      var range = XLSX.utils.decode_range(ws["!ref"]);
      var C,
        R = range.s.r;
      let excel_headers = this.excel_headers;
      excel_headers = excel_headers.map((v) =>
        v.toLowerCase().replace(/ /g, "")
      );
      for (C = range.s.c; C <= range.e.c; ++C) {
        var cell = ws[XLSX.utils.encode_cell({ c: C, r: R })];
        var hdr = "UNKNOWN " + C;
        if (
          cell &&
          cell.t &&
          excel_headers.includes(cell.v.toLowerCase().replace(/ /g, ""))
        )
          hdr = XLSX.utils.format_cell(cell);
        else {
          alert("Kindly refer/used sample template and try to upload");
          return false;
        }
        // headers.push(hdr.toLowerCase().replace(/ /g, ""));
        headers.push(hdr);
      }
      this.Upload(ev.target.files[0], ev, headers);
    };
    reader.readAsBinaryString(ev.target.files[0]);
  }

  viewFile(file){
    var blob = new Blob([file], { type: file.type });
    var url = window.URL.createObjectURL(blob);
    window.open(url,"_blank")
    // var WinPrint = window.open(
    //   "",
    //   "",
    //   "letf=0,top=0,width=400,height=400,toolbar=0,scrollbars=0,status=0"
    // );
    // WinPrint.document.write('<img src='+url+'></img>')
  }

  async removeFile(i){
    // this.Files.splice(i,1)
    let files = []
    this.Files.forEach((file, index)=>{
      if(i != index){
        files.push(file)
      }
    })
    await console.log('setting new file list')
    this.Files = files
  }

  Upload(data, evt, headers) {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      let arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i)
        arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      console.log(XLSX.utils.sheet_to_json(worksheet, { raw: false }));
      let array = [];
      array = XLSX.utils.sheet_to_json(worksheet, { raw: false });
      if (array.length == 0)
        return this.cs.showError("Kindly fill data in excel");
      else this.validateExcel(array, evt, headers);
    };
    fileReader.readAsArrayBuffer(data);
  }

  validateExcel(array, ev, headers) {
    for (let i = 0; i < this.excel_headers.length; i++) {
      for (let j = 0; j < array.length; j++) {
        if (this.cs.isUndefinedORNull(array[j][headers[i]])) {
          alert(
            "Please enter values under " +
              this.excel_headers[i] +
              " at line number " +
              (j + 2)
          );
          return false;
        }

        // Date Validation
        if (
          i == 1 &&
          !moment(array[j][headers[1]], "DD/MM/YYYY", true).isValid()
        ) {
          alert(
            "Invalid date format at line number " +
              (j + 2) +
              ".It should be in format of DD/MM/YYYY. eg. 01/01/2020"
          );
          return false;
        }

        // Time Validation
        if (
          (i == 2 || i == 3) &&
          !array[j][headers[i]].match("^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")
        ) {
          alert(
            "Invalid date format on " +
              this.excel_headers[i] +
              " column at line number " +
              (j + 2) +
              ".  It should be in the format of HH:MM. eg. 16:15 "
          );
          return false;
        }
      }
    }
    if (!this.userToken) {
      this.toastr.error("User Token Not Found. Kindly Relogin");
    } else {
      this.myHelpDeskService
        .call("IncidentManagement/CURosterEmployee", {
          excel: array,
          createdBy: this.userToken,
        })
        .subscribe((res) => {
          if (res.Message == "Success") {
            console.log(res);
            this.toastr.success("Uploaded Successfully");
          }
        });
    }

    // array is the variable to be sent
  }

  reopen(ticketRowId) {
    this.myHelpDeskService
      .call("IncidentManagement/ReopenTicket", {
        TicketReq_RowID: ticketRowId,
      })
      .subscribe((res) => {
        if (res.Message == "Success") {
          this.getTableValues();
          this.toastr.success("Ticket Reopened");
        }
      });
  }

  checkOrangeValue() {
    if (this.Green.value == 0) {
      this.Green.patchValue(2);
      this.Orange.patchValue(3);
    }

    if (this.Orange.value <= this.Green.value) {
      this.Orange.patchValue(this.Green.value + 1);
    }
  }

  getAgeingReport() {
    if (
      this.startDate.valid &&
      this.endDate.valid &&
      this.Green.valid &&
      this.Orange.valid
    ) {
      this.myHelpDeskService
        .call("IncidentManagement/GetAgeingReport", {
          FromDate: moment(this.startDate.value).format("YYYY-MM-DD"),
          ToDate: moment(this.endDate.value).format("YYYY-MM-DD"),
          Green: this.Green.value,
          Orange: this.Orange.value,
          IType: this.IType.value,
        })
        .subscribe((response) => {
          console.log(response);
          if (response.Message == "Success") {
            let ageingDataSource = [];
            for (const data of response.ResponseData) {
              ageingDataSource.push({
                id: data.ID,
                status: data.StatusName,
                green: data.Green,
                orange: data.Orange,
                red: data.Red,
                total: data.Total,
              });
            }
            this.ageingDataSource = [...ageingDataSource];
            console.log(this.ageingDataSource);
          }
        });
    } else {
      this.toastr.error("Kindly provide valid inputs");
    }
  }

  manageMapping(content) {
    this.modal.open(content, {
      size: "xl" as "lg",
      centered: true,
      windowClass: "formModal",
    });
  }

  closeModal() {
    this.modal.dismissAll();
  }

  createSector() {
    if (this.create_mapping_sector.value) {
      this.myHelpDeskService
        .call("IncidentManagement/CRUDIncidentSector", {
          action: "INSERT",
          sectorName: this.create_mapping_sector.value,
        })
        .subscribe((res) => {
          console.log(res);
          if (res) {
            this.getMappingSectors();
            this.create_mapping_sector.reset();
            this.mapping_sector.reset();
            this.toastr.success("Sector Created Successfully");
          }
        });
    }
  }

  updateSector() {
    let update_sector = this.sectors.find(
      (o) => o.SectorName == this.mapping_sector.value
    );
    console.log(update_sector);
    this.myHelpDeskService
      .call("IncidentManagement/CRUDIncidentSector", {
        action: "UPDATE",
        sectorName: this.create_mapping_sector.value,
        isActive: true,
        sectorID: update_sector.SectorID,
      })
      .subscribe((res) => {
        console.log(res);
        if (res) {
          // get all sectors
          this.getMappingSectors();
          this.create_mapping_sector.reset();
          this.mapping_sector.reset();
          this.toastr.success("Sector Updated Successfully");
        }
      });
  }

  getPlantDetails(plantId) {
    console.log(plantId);
    let plant = this.plants.find((o) => o.PlantID == plantId);
    console.log(plant);
    this.create_mapping_plant.setValue(plant.PlantName);
    this.create_mapping_plant_sector.setValue(plant.SectorID);
  }

  createPlant() {
    console.log(
      this.create_mapping_plant.value,
      this.create_mapping_plant_sector.value
    );
    if (
      this.create_mapping_plant.value &&
      this.create_mapping_plant_sector.value
    ) {
      this.myHelpDeskService
        .call("IncidentManagement/CRUDIncidentPlant", {
          action: "INSERT",
          plantName: this.create_mapping_plant.value,
          sectorID: this.create_mapping_plant_sector.value,
        })
        .subscribe((res) => {
          console.log(res);
          if (res) {
            this.getMappingPlants();
            this.create_mapping_plant.reset();
            this.create_mapping_plant_sector.reset();
            this.mapping_plant.reset();
            this.toastr.success("Plant Created Successfully");
          }
        });
    }
  }

  updatePlant() {
    console.log(
      this.create_mapping_plant.value,
      this.create_mapping_plant_sector.value
    );
    if (
      this.create_mapping_plant.value &&
      this.create_mapping_plant_sector.value
    ) {
      this.myHelpDeskService
        .call("IncidentManagement/CRUDIncidentPlant", {
          action: "UPDATE",
          plantName: this.create_mapping_plant.value,
          sectorID: this.create_mapping_plant_sector.value,
          isActive: false,
          plantID: this.mapping_plant.value,
        })
        .subscribe((res) => {
          console.log(res);
          if (res) {
            this.getMappingPlants();
            this.create_mapping_plant.reset();
            this.create_mapping_plant_sector.reset();
            this.mapping_plant.reset();
            this.toastr.success("Plant Updated Successfully");
          }
        });
    }
  }

  getBusinessFunctionDetails(bfId) {
    console.log(bfId);
    let bf = this.bussinessFuntions.find((o) => o.BusinessFunID == bfId);
    console.log(bf);
    this.create_mapping_bussinessFunction.setValue(bf.BusniessFunctionName);
    this.create_mapping_bussinessFunction_plant.setValue(bf.PlantID);
  }

  createBussinessFunction() {
    if (
      this.create_mapping_bussinessFunction.value &&
      this.create_mapping_bussinessFunction_plant.value
    ) {
      this.myHelpDeskService
        .call("IncidentManagement/CRUDIncidentBusinessFunction", {
          action: "INSERT",
          busniessFunctionName: this.create_mapping_bussinessFunction.value,
          plantID: this.create_mapping_bussinessFunction_plant.value,
        })
        .subscribe((res) => {
          console.log(res);
          if (res) {
            this.getMappingBussinessFunction();
            this.create_mapping_bussinessFunction_plant.reset();
            this.create_mapping_bussinessFunction.reset();
            this.toastr.success("Bussiness Function Created Successfully");
          }
        });
    }
  }

  updateBussinessFunction() {
    if (
      this.create_mapping_bussinessFunction.value &&
      this.create_mapping_bussinessFunction_plant.value
    ) {
      this.myHelpDeskService
        .call("IncidentManagement/CRUDIncidentBusinessFunction", {
          action: "UPDATE",
          busniessFunctionName: this.create_mapping_bussinessFunction.value,
          plantID: this.create_mapping_bussinessFunction_plant.value,
          isActive: true,
          businessFunID: this.mapping_bussinessFunction.value,
        })
        .subscribe((res) => {
          console.log(res);
          if (res) {
            this.getMappingBussinessFunction();
            this.create_mapping_bussinessFunction_plant.reset();
            this.create_mapping_bussinessFunction.reset();
            this.toastr.success("Bussiness Function Created Successfully");
          }
        });
    }
  }
  //------------------New report added in mat tab--------------------------
  reportColumns1: string[] = [
    "no_of_incidents",
    "Business_concern",
    "IT_concern",
    "Resolved",
    "Pending_with_others",
    "Ageing"
  ];
  reportColumns2: string[] = [
    "No_of_calls",
    "Assigned_to",
    "resolved_by",
    "date",
    "call_resolution_time"
  ];
  reportColumns3: string[] = [
    "Consultant",
    "incident_no",
    "Ageing"
  ];
   fromSummary:Date = new Date();
   toSummary:Date = new Date();
   dataSourceSummary=[];
  onTabChang(e){
    console.log(e.index);
    if(e.index == 0){  
      let fdate = moment(this.fromSummary).format("YYYY-MM-DD");
      let tdate = moment(this.toSummary).format("YYYY-MM-DD");
      // this.fromSummary = moment(this.fromSummary).format("YYYY-MM-DD");
      // let fDate:Date = this.fromSummary
      
      // this.toSummary = moment(this.toSummary).format("YYYY-MM-DD");
      this.myHelpDeskService
      .call("IncidentManagement/Get_TicketSummaryDetails", {
        FromDate: fdate,
        ToDate  : tdate
      })
      .subscribe((res) => {
        console.log(res);
        if (res) {
          this.dataSourceSummary = res.ResponseData;
        }
      });
    }
    else if(e.index == 1){
      this.myHelpDeskService
      .call("IncidentManagement/CRUDIncidentBusinessFunction", {
        action: "UPDATE",
        busniessFunctionName: this.create_mapping_bussinessFunction.value,
        plantID: this.create_mapping_bussinessFunction_plant.value,
        isActive: true,
        businessFunID: this.mapping_bussinessFunction.value,
      })
      .subscribe((res) => {
        console.log(res);
        if (res) {
          
        }
      });
    }
    else if(e.index == 2){
      this.myHelpDeskService
      .call("IncidentManagement/CRUDIncidentBusinessFunction", {
        action: "UPDATE",
        busniessFunctionName: this.create_mapping_bussinessFunction.value,
        plantID: this.create_mapping_bussinessFunction_plant.value,
        isActive: true,
        businessFunID: this.mapping_bussinessFunction.value,
      })
      .subscribe((res) => {
        console.log(res);
        if (res) {
          
        }
      });
    }
  }
  SearchSummaryReport() {
      this.onTabChang({index : 0})
  }
  mainTabChange(e){
    if(e.index == 3)
    this.onTabChang({index : 0})
  }
  resetSummaryReport(){
    
    this.onTabChang({index : 0})
  }
}
