import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import * as XLSX from 'xlsx';
import { CommonUtilityService } from '../services/common/common-utility.service';


@Component({
  selector: 'app-stakeholder-upload',
  templateUrl: './stakeholder-upload.component.html',
  styleUrls: ['./stakeholder-upload.component.sass']
})
export class StakeholderUploadComponent implements OnInit {

  constructor(public cs: MySuggestionService, public commonService: CommonUtilityService,
    private _formBuilder: FormBuilder) { 
      this.commonService.showMenuForSatkeholder(true);
    }

  ngOnInit() {
    this.createBulkUploadForm();
  }
  // code for uploading bulk data through modal
banner_name ="Stake Holder bulk upload";
excel_headers:any=[];
bulk_Upload_form:any;
Excel_File = [];

createBulkUploadForm(){
  this.bulk_Upload_form = this._formBuilder.group({
    Bulk_Upload: [{ value: '', disabled: true }, Validators.required]
  });
}
detectFiles(evt,form) {
  form.resetForm();
  this.excel_headers=["VendorCode","SupplierName","Designation","FirstName","LastName","WorkEmail","ContactNumber","SupplierType","PlantCode","SuppliersToSectors","Commodity"];
  let target: any = <any>(evt.target);
  var headers = [];
  for (let file of target.files) {
    let ext = file.name.split(".").pop().toLowerCase();
    let reader = new FileReader();
    reader.onload = (e: any) => {
      if (ext != 'xlsx') {
        this.cs.showError("Kindly select file with extension '.xlsx");
        return false;
      }
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      var range = XLSX.utils.decode_range(ws['!ref']);
      var C, R = range.s.r;
      let excel_headers = this.excel_headers;
      excel_headers = excel_headers.map(v => v.toLowerCase().replace(/ /g, ""));
      for (C = range.s.c; C <= range.e.c; ++C) {
        var cell = ws[XLSX.utils.encode_cell({ c: C, r: R })]
        var hdr = "UNKNOWN " + C;
        if (cell && cell.t && excel_headers.includes(cell.v.toLowerCase().replace(/ /g, "")))
          hdr = XLSX.utils.format_cell(cell);
        else {
          this.cs.showError("Kindly refer/used sample template and try to upload")
          return false;
        }
        // headers.push(hdr.toLowerCase().replace(/ /g, ""));
        headers.push(hdr);
      }
      this.Upload(target.files[0], evt,headers,form)
    }
    reader.readAsBinaryString(target.files[0]);
  }
}

Upload(data, evt,headers,form) {
  let fileReader = new FileReader();
  fileReader.onload = (e) => {
    let arrayBuffer: any = fileReader.result;
    var data = new Uint8Array(arrayBuffer);
    var arr = new Array();
    for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
    var bstr = arr.join("");
    var workbook = XLSX.read(bstr, { type: "binary" });
    var first_sheet_name = workbook.SheetNames[0];
    var worksheet = workbook.Sheets[first_sheet_name];
    console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
    let array = [];
    array = XLSX.utils.sheet_to_json(worksheet, { raw: true });
    if (array.length == 0)
      return this.cs.showError("Kindly fill data in excel");
    else
      this.validateExcel(array, evt,headers,form);
  }
  fileReader.readAsArrayBuffer(data);
}

validateExcel(array, evt,headers,form) {
  for (let i = 0; i < this.excel_headers.length; i++) {
    for (let j = 0; j < array.length; j++) {
      if (this.cs.isUndefinedORNull(array[j][headers[i]])) {
        this.cs.showError("Please enter values under " + this.excel_headers[i] + " at line number " + (j + 2));
        return false;
      }

    }
  }
  this.Excel_File = [];
  form.resetForm();
   
  // form.get('Bulk_Upload').setValue(evt.target.files[0].name);
  form.control.get('Bulk_Upload').setValue(evt.target.files[0].name)
  this.Excel_File.push(evt.target.files[0]);
}
InsertStakeholderData(form: any) {

  let formData;
  let url;
 
    url='Vendor/BulkUploadTempVendorDetails';   
    formData = new FormData();
    formData.append('ExcelFile', this.Excel_File[0]);
    formData.append('CreatedBy', "test");// need to add from login api
    console.log("---",formData);
    this.cs.postMySuggestionData(url, formData).
      subscribe(
        (res: any) => {
          this.cs.showSwapLoader = false;
          this.cs.showSuccess(res.Message);
          // if(closeModalonclick)
          // closeModalonclick._elementRef.nativeElement.click();
          form.resetForm();

        },
        (error) => {
          this.cs.showSwapLoader = false;
          console.log(error.message);
        }
      )
}
DownloadSampleTemplate(filename:any){
  window.open(`${'../../assets/'}${filename}${'.xlsx'}`, '_blank');
  // window.open('../../assets/Navigation Sample.xlsx')
}

}
