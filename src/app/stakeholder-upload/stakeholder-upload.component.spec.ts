import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StakeholderUploadComponent } from './stakeholder-upload.component';

describe('StakeholderUploadComponent', () => {
  let component: StakeholderUploadComponent;
  let fixture: ComponentFixture<StakeholderUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StakeholderUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StakeholderUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
