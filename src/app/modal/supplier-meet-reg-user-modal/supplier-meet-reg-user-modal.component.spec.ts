import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierMeetRegUserModalComponent } from './supplier-meet-reg-user-modal.component';

describe('SupplierMeetRegUserModalComponent', () => {
  let component: SupplierMeetRegUserModalComponent;
  let fixture: ComponentFixture<SupplierMeetRegUserModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierMeetRegUserModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierMeetRegUserModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
