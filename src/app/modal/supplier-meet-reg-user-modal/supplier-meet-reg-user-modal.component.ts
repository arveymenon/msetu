import { Component, OnInit, Input } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MySuggestionService } from "src/app/services/MySuggestion/my-suggestion.service";
import * as moment from "moment";
import * as CryptoJS from "crypto-js";

@Component({
  selector: "app-supplier-meet-reg-user-modal",
  templateUrl: "./supplier-meet-reg-user-modal.component.html",
  styleUrls: ["./supplier-meet-reg-user-modal.component.scss"],
})
export class SupplierMeetRegUserModalComponent implements OnInit {
  @Input("details") details: any;
  room_data = [];
  Food_data = [];
  Liquore_data = [];
  age_data = [];
  ageId = 1;

  supplier_name: any;
  liquore_val: any;
  food_pref: any;
  room_req: any;

  PersonalDetailForm: FormGroup;
  FlightDetailForm: FormGroup;
  PassportDetailForm: FormGroup;

  supplier_value: any;
  username = CryptoJS.AES.decrypt(
    localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),
    ""
  ).toString(CryptoJS.enc.Utf8);

  constructor(
    private modalService: NgbModal,
    public fb: FormBuilder,
    public cs: MySuggestionService
  ) {
  }

  async ngOnInit() {
    console.log(this.details);
    this.PersonalDetailForm = this.fb.group({
      name: [this.details.Name],
      designation: [this.details.Designation],
      mobile: [this.details.MobileNo],
      emergencyNo: [this.details.EmergencyContactNo],
      health: [this.details.HealthIssues],
      hotel: [this.details.Hotel],
      organisation: [this.details.Organisation],
      phone: [this.details.PhoneNo],
      email: [this.details.Email],
      age: [this.details.Age],
    });

    
    await this.cs
      .getMySuggestionData("SMRegistration/GetRoomDetails")
      .subscribe((res: any) => {
        this.room_data = res.ResponseData;
        this.room_req = res.ResponseData.find(
          (o) => o.Room == this.details.Room
        ).RoomID;
      });
      this.supplier_value = this.details.SharingWith;

    this.cs
      .getMySuggestionData("SMRegistration/GetFoodPrefData")
      .subscribe((res: any) => {
        this.Food_data = res.ResponseData;
        this.food_pref = res.ResponseData.find(
          (o) => o.Food == this.details.Food
        ).FoodID;
      });

    this.cs
      .getMySuggestionData("SMRegistration/GetLiquorData")
      .subscribe((res: any) => {
        this.Liquore_data = res.ResponseData;
        this.liquore_val = res.ResponseData.find(
          (o) => o.Liquor == this.details.Liquor
        ).LiquorID;
      });
    this.cs
      .getMySuggestionData("SMRegistration/GetAgeGroupData")
      .subscribe((res: any) => {
        this.age_data = res.ResponseData;
        var ageId = this.age_data.find((o) => o.Age == this.details.Age).AgeID;
        this.PersonalDetailForm.controls["age"].setValue(ageId);
      });

    console.log(this.age_data);
    if (this.age_data.length > 0) {
    }

    this.FlightDetailForm = this.fb.group({
      Airline_name: [this.details.AirlineName],
      Date_Arrival: [this.details.DateofArrival],
      Time_Arrival: [this.details.TimeofArrival],
      Flight_No: [this.details.FlightNumber],
      Date_of_Departure: [this.details.DateofDeparture],
      Time_of_Departure: [this.details.TimeofDeparture],
    });

    this.PassportDetailForm = this.fb.group({
      Passport_no: [this.details.Passport],
      DOB: [new Date(this.details.DateofBirth)],
      Nationality: [this.details.Nationality],
      Visa: [this.details.VisaHolder],
    });

    this.cs
      .postMySuggestionData("SMRegistration/GetSupplierByVendor", {
        VendorID: this.details.VendorCode,
      })
      .subscribe((res: any) => {
        if (res) {
          this.supplier_name = res.ResponseData;
        }
      });
  }

  submit(form, stepper, ID) {
    if (form.valid) {
      if (ID == 2) {
        if (this.cs.isUndefinedORNull(this.room_req))
          this.cs.showError("Please select Room Requirement");
        else if (this.cs.isUndefinedORNull(this.food_pref))
          this.cs.showError("Please select Food Preference");
        else if (this.cs.isUndefinedORNull(this.liquore_val))
          this.cs.showError("Please select Liquore");
        else {
          stepper.selected.completed = true;
          stepper.next();
        }
      } else {
        stepper.selected.completed = true;
        stepper.next();
      }
    } else {
      if (ID == 1 && form.get("age").hasError("required")) {
        this.cs.showError("Kindly fill all mandatory details");
        return;
      }
      if (ID == 2) {
        if (this.cs.isUndefinedORNull(this.room_req))
          this.cs.showError("Kindly fill all mandatory details");
        else if (this.cs.isUndefinedORNull(this.food_pref))
          this.cs.showError("Please select Food Preference");
        else if (this.cs.isUndefinedORNull(this.liquore_val))
          this.cs.showError("Please select Liquore");
        return;
      }
    }
  }
  saveRegistrationDetails(form) {
    if (
      form.valid &&
      this.PersonalDetailForm.valid &&
      this.FlightDetailForm.valid &&
      !this.cs.isUndefinedORNull(this.liquore_val) &&
      !this.cs.isUndefinedORNull(this.food_pref) &&
      !this.cs.isUndefinedORNull(this.room_req)
    ) {
      let Request = {
        arrivalDate: moment(
          this.FlightDetailForm.controls.Date_Arrival.value
        ).format("DD-MMM-YYYY"),
        departureDate: moment(
          this.FlightDetailForm.controls.Date_of_Departure.value
        ).format("DD-MMM-YYYY"),
        wid: this.details.wid,
        ageGroup: this.PersonalDetailForm.controls.age.value,
        roomReq: this.room_req,
        foodPref: this.food_pref,
        visa: this.PassportDetailForm.controls.Visa.value,
        liquor: this.liquore_val,
        name: this.PersonalDetailForm.controls.name.value,
        designation: this.PersonalDetailForm.controls.designation.value,
        mobileNo: this.PersonalDetailForm.controls.mobile.value,
        emerContNo: this.PersonalDetailForm.controls.emergencyNo.value,
        healthIssues: this.PersonalDetailForm.controls.health.value,
        organization: this.PersonalDetailForm.controls.organisation.value,
        email: this.PersonalDetailForm.controls.email.value,
        airline: this.FlightDetailForm.controls.Airline_name.value,
        flightNo: this.FlightDetailForm.controls.Flight_No.value,
        arrivalTime: this.FlightDetailForm.controls.Time_Arrival.value,
        departureTime: this.FlightDetailForm.controls.Time_of_Departure.value,
        passportNo: this.PassportDetailForm.controls.Passport_no.value,
        nationality: this.PassportDetailForm.controls.Nationality.value,
        hotel: this.PersonalDetailForm.controls.hotel.value,
        phoneNo: this.PersonalDetailForm.controls.phone.value,
        dob: moment(this.PassportDetailForm.controls.DOB.value).format(
          "DD-MMM-YYYY"
        ),
        createdBy: this.username,
        VendorCode: this.details.VendorCode,
        sharingWith: this.supplier_value || '',
        isVisaHolder: this.PassportDetailForm.controls.Visa.value,
      };

      this.cs
        .postMySuggestionData("SMRegistration/UpdateRegisteredUser", Request)
        .subscribe((res: any) => {
          if (res) {
            console.log(res)
            this.cs.showSuccess('Details Updated Successfully');
            this.PersonalDetailForm.reset();
            this.FlightDetailForm.reset();
            this.PassportDetailForm.reset();
            this.room_req = null;
            this.food_pref = null;
            this.liquore_val = null;
            this.modalService.dismissAll();
          } else {
            this.cs.showError("Something went wrong please try again");
          }
        });
    } else {
      this.cs.showError("Kindly fill all mandatory details");
      return;
    }
  }

  closeModal() {
    this.modalService.dismissAll();
  }
}
