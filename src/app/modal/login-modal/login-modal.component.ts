import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-login-modal",
  templateUrl: "./login-modal.component.html",
  styleUrls: ["./login-modal.component.scss"],
})
export class LoginModalComponent implements OnInit {
  loginForm: FormGroup;
  passwordType = 'password'

  constructor(public fb: FormBuilder, public toast: ToastrService) {
    this.loginForm = this.fb.group({
      username: ["", Validators.required],
      password: ["", Validators.required],
    });
  }

  ngOnInit() {}

  submit() {
    console.log(this.loginForm);
    if (this.loginForm.valid) {
      if (
        this.loginForm.value.username.toLowerCase() == "vr001" &&
        this.loginForm.value.password.toLowerCase() == "vr001z"
      ) {
        window.open('https://cordys2.mahindra.com:8443/home/Mahindra/NVR_HTML/NVR_home_custom.htm', "_self");
      } else {
        this.toast.error("Invalid Credentials")
      }
    } else {
      this.toast.error("Kindly Fill Both The Fields");
    }
  }

  togglePasswordType(){
    this.passwordType = this.passwordType == 'password' ? 'text' : 'password';
  }
}
