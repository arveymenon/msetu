import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-liquidity-modal',
  templateUrl: './liquidity-modal.component.html',
  styleUrls: ['./liquidity-modal.component.scss']
})
export class LiquidityModalComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  @Input() LiquidityData=[];
  @Input() modalTitle='';
  radio_btn=[];
  
  ngOnInit() {
   this.radio_btn=[];
    this.LiquidityData.filter(x=>{
      let arr = x.Options.split(',');

      //  to remove last element
      arr.splice((arr.length-1), 1)
  

      x.Options = arr
    })


  }
  closeModal(){
    this.modalService.dismissAll();
  }
  openDoc(file){
    window.open(file,'_blank');
  }
}
