import { Component, OnInit, Input, PipeTransform, Pipe } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-plastic-waste-management',
  templateUrl: './plastic-waste-management.component.html',
  styleUrls: ['./plastic-waste-management.component.scss']
})

export class PlasticWasteManagementComponent implements OnInit  {
  @Input() public page_data;
  public window = window;

  constructor(
    public modal: NgbModal,
    public domSanitizer: DomSanitizer
  ) {
   }
   
   ngOnInit() {
     console.log(this.page_data);
    }

  wasteManagement(){
    window.open('https://supplier.mahindra.com/sites/supplierportal/SiteAssets/Plastic%20Waste%20Management/Plastic%20Waste%20Management%20(Amendment)%20Rules%202018_2.pdf','_blank')
  }

  rules(){
    window.open('https://supplier.mahindra.com/sites/supplierportal/SiteAssets/Plastic%20Waste%20Management/Plastic%20Waste%20Management%20(Amendment)%20Rules%202018.pdf','_blank')
  }
  
  amendmentRules(){
    window.open('https://supplier.mahindra.com/sites/supplierportal/SiteAssets/Plastic%20Waste%20Management/plasticwasteGazetteSearch_03072018.pdf','_blank')
  }

  closeModal(){
    this.modal.dismissAll();
  }

}
