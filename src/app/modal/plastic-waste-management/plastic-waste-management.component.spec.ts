import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlasticWasteManagementComponent } from './plastic-waste-management.component';

describe('PlasticWasteManagementComponent', () => {
  let component: PlasticWasteManagementComponent;
  let fixture: ComponentFixture<PlasticWasteManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlasticWasteManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlasticWasteManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
