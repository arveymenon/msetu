import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationdashboardmodalComponent } from './notificationdashboardmodal.component';

describe('NotificationdashboardmodalComponent', () => {
  let component: NotificationdashboardmodalComponent;
  let fixture: ComponentFixture<NotificationdashboardmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationdashboardmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationdashboardmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
