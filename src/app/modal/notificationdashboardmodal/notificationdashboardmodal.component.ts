import { Component, OnInit, ViewChild, forwardRef } from '@angular/core';
import { MatIconRegistry } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
//import { DashboardService } from '../services/S_dashboard/dashboard.service';
import { DashboardService } from 'src/app/services/S_dashboard/dashboard.service';
import { MainNavComponent } from 'src/app/main-nav/main-nav.component';
import * as CryptoJS from 'crypto-js';
@Component({
  selector: 'app-notificationdashboardmodal',
  templateUrl: './notificationdashboardmodal.component.html',
  styleUrls: ['./notificationdashboardmodal.component.scss']
})
export class NotificationdashboardmodalComponent implements OnInit {
  UnReadCount: any;
  pageNo: any =0;
  static getNotification() {
    throw new Error("Method not implemented.");
  }
  notificationData:any =[];
  updateNotificationData:any;
  notificationTo:any;
  pagedata:any=[];
  selectedarray:any=[];
  selected:any;
  seenArray: any=[];
  changeclr:boolean;
  clickedArray:any =[];
  vendorcode:any;
  emailid:any;
  RoleID:string;
  emailidReq:any;
  pageSize:any;
  // @ViewChild(forwardRef(()=>MainNavComponent))private navComponent:MainNavComponent
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public modalService: NgbModal,
    private router: Router,
    private toastr: ToastrService,
    private dashboardService: DashboardService
  ) { }

  ngOnInit() {
    // this.username = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
    // this.username = CryptoJS.AES.decrypt(this.username,"").toString(CryptoJS.enc.Utf8);
    this.RoleID = localStorage.getItem('PR');
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID,"").toString(CryptoJS.enc.Utf8);
    this.vendorcode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
    this.vendorcode = CryptoJS.AES.decrypt(this.vendorcode,"").toString(CryptoJS.enc.Utf8);
    this.emailidReq = localStorage.getItem('V2xjeGFHRlhkejA9');
    this.emailidReq = CryptoJS.AES.decrypt(this.emailidReq,"").toString(CryptoJS.enc.Utf8); 
  this.getNotification('get');
  }
  getNotification(flag){
    if(flag =='updated')
    {
      this.pageNo = 1;
      this.notificationData =[];
    }
    else
    this.pageNo = this.pageNo +1;
    this.notificationTo = localStorage.getItem('userToken');
    // this.RoleID = localStorage.getItem('roleId');
    // this.vendorcode= localStorage.getItem('vendorCode');
    // this.emailid= JSON.parse(localStorage.getItem('userDetails'));
    // this.emailidReq= this.emailid.EmailID;
    console.log(this.emailidReq);
    console.log(this.notificationTo);
    let reqData= {
      "NotificationTo":this.notificationTo,
      "VendorCode":this.vendorcode,
      "RoleID":this.RoleID,
      "EmailId":this.emailidReq,
      "PageNo" : this.pageNo
      }
      this.dashboardService.getNotificationdata(reqData).subscribe((resp:any) => {
        console.log(resp);
        if(resp.ID != 0){
          this.pagedata = resp.ResponseData;
          this.notificationData= this.notificationData.concat(this.pagedata);
          localStorage.setItem('noNotifi',this.notificationData[0].UnReadCount);
          this.UnReadCount = this.notificationData[0].UnReadCount;
          this.pageSize = resp.TotalCount;
        }
        
      });
  }
  viewedNotification(ids,cl){
     this.clickedArray.push(cl);
     if(ids.IsRead == true){
      let notID= ids.NotificationId;
       console.log("this Notification is already viewed");
       let chh= this.notificationData.findIndex(x => x.NotificationId == notID);
       console.log(chh);
     }
     else{
      let notID= ids.NotificationId;
      let postdata={
        "NotificationId":notID
      }
      this.dashboardService.updateNotification(postdata).subscribe((resp:any) => {
        console.log(resp);
        this.updateNotificationData= resp.ResponseData;
        this.getNotification('updated');
      });
     }

     if(ids.NotificationType =='s'){
      this.router.navigateByUrl('mySurvey');
       this.closeModal();
     }
     
     else if(ids.NotificationType =='i' || ids.NotificationType=='I'){
      this.router.navigateByUrl('myHelpdesk');
      this.closeModal();
     }

  }
  closeModal() {
    this.modalService.dismissAll();
  }

}
