import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonsoonUpdatesModalComponent } from './monsoon-updates-modal.component';

describe('MonsoonUpdatesModalComponent', () => {
  let component: MonsoonUpdatesModalComponent;
  let fixture: ComponentFixture<MonsoonUpdatesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonsoonUpdatesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonsoonUpdatesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
