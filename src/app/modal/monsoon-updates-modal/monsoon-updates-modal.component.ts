import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MySuggestionService } from 'src/app/services/MySuggestion/my-suggestion.service';
// import { MatIconRegistry } from '@angular/material';
 import { DomSanitizer,SafeResourceUrl } from '@angular/platform-browser';
@Component({
  selector: 'app-monsoon-updates-modal',
  templateUrl: './monsoon-updates-modal.component.html',
  styleUrls: ['./monsoon-updates-modal.component.sass']
})
export class MonsoonUpdatesModalComponent implements OnInit {
  monsoonUpdatePDF=[];
  PDFArrayLength:number;
  PDFArray:any;
  loading = true;
  constructor(private modalService: NgbModal, public cs1:MySuggestionService,public domSanitizer: DomSanitizer) {
   
   }

  ngOnInit() {
    // window.open(pdffile, '_blank');
    this.callMonsoonUpdates();
      }
      callMonsoonUpdates(){
        this.cs1.showSwapLoader=true;
        let postData = { 
          "BusinessCategoryID":8,
          "BusinessSubCategoryID":0 
        }
        this.cs1.postMySuggestionData('DocumentManagement/Get_CMSDocs', postData).
          subscribe(
            (res: any) => {
              this.cs1.showSwapLoader = false;
              this.monsoonUpdatePDF = res.ResponseData;
             this.PDFArrayLength= this.monsoonUpdatePDF.length;
             if(this.PDFArrayLength >0){
              if(this.monsoonUpdatePDF.length<2){
                this.PDFArray = this.domSanitizer.bypassSecurityTrustResourceUrl(this.monsoonUpdatePDF[0].Doc_Link);
                this.loading = false;
               // this.mArray= false;
               }
               else{
                this.PDFArray = this.domSanitizer.bypassSecurityTrustResourceUrl(this.monsoonUpdatePDF[this.PDFArrayLength - 1].Doc_Link);
                this.loading = false;
               }
             }
            
            },
            (error) => {
              this.cs1.showSwapLoader = false;
              console.log(error.message);
            });
      }
      closeModal(){
        this.modalService.dismissAll();
      }
}
