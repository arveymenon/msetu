import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-whats-new-modal',
  templateUrl: './whats-new-modal.component.html',
  styleUrls: ['./whats-new-modal.component.scss']
})
export class WhatsNewModalComponent implements OnInit {

  @Input() public passedData;
  @Input() public pageName;
  constructor( public modalService:NgbModal) { }

  ngOnInit() {
    console.log(this.passedData);
    console.log(this.pageName); 
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  viewContent(Link){
    window.open(Link, "_blank")
  }

}
