import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcellenceAwardModalComponent } from './excellence-award-modal.component';

describe('ExcellenceAwardModalComponent', () => {
  let component: ExcellenceAwardModalComponent;
  let fixture: ComponentFixture<ExcellenceAwardModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExcellenceAwardModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcellenceAwardModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
