import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-excellence-award-modal',
  templateUrl: './excellence-award-modal.component.html',
  styleUrls: ['./excellence-award-modal.component.scss']
})
export class ExcellenceAwardModalComponent implements OnInit {
  imageURL: any;

  constructor(private modalService: NgbModal,private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer) { 
    matIconRegistry.addSvgIcon('checkIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/checked.svg'));
    matIconRegistry.addSvgIcon('arrowRight',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/Arrowright.svg'));
  }
  @Input()imageArray=[]
  ngOnInit() {
this.imageArray
  }


  closeModal(){
    this.modalService.dismissAll();
  }

  showImage(data){
    this.imageURL=data.Link;
  }

  move(){
    alert("left")
  }
}
