import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OESuppliesComponent } from './oesupplies.component';

describe('OESuppliesComponent', () => {
  let component: OESuppliesComponent;
  let fixture: ComponentFixture<OESuppliesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OESuppliesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OESuppliesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
