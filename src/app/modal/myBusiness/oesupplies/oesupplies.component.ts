import { Component, OnInit, AfterViewInit, ViewChild, OnChanges, Input, ElementRef } from "@angular/core";
import { CommonUtilityService } from "src/app/services/common/common-utility.service";
import {
  MatIconRegistry,
  NativeDateAdapter,
  DateAdapter,
  MAT_DATE_FORMATS,
  MatTableDataSource,
  MatPaginator,
} from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { MyBusinessServiceService } from "src/app/services/MyBusinessService/my-business-service.service";
import * as CryptoJS from "crypto-js";
import { Subject, Subscription, Observable } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import {
  FormBuilder,
  Validators,
  FormControl,
  FormGroup,
  FormArray,
} from "@angular/forms";
import { DataServiceService } from "src/app/services/DataService/data-service.service";
import { CommonApiService } from "src/app/services/common-api/common-api.service";
import * as XLSX from "xlsx";
import * as moment from "moment";
import { AppDateAdapter, APP_DATE_FORMATS } from "././date-format";
import { ToastrService } from "ngx-toastr";
import { MySuggestionService } from "src/app/services/MySuggestion/my-suggestion.service";
import { DatePipe } from "@angular/common";
import { indexOf } from "@amcharts/amcharts4/.internal/core/utils/Array";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  Router,
  ActivatedRoute,
  Params,
  Event,
  NavigationEnd,
  NavigationStart,
} from "@angular/router";
import { resolve } from "url";
import { Element } from "@angular/compiler";
import { COPEmailModalComponent } from "src/app/modal/copemail-modal/copemail-modal.component";
import * as $ from "jquery";
import { Workbook } from "exceljs";

declare var require: any;
const FileSaver = require("file-saver");
const { read, write, utils } = XLSX;
type AOA = any[][];

import * as fs from "file-saver";
import { DashboardComponent } from 'src/app/dashboard/dashboard.component';

@Component({
  selector: "app-oesupplies",
  templateUrl: "./oesupplies.component.html",
  styleUrls: ["./oesupplies.component.scss"],
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: APP_DATE_FORMATS,
    },
  ],
})
export class OESuppliesComponent implements OnInit, AfterViewInit {
  @ViewChild('COPTabs') coptabs: any;
  @ViewChild('componentTabs') componentTabs: any;
  @ViewChild('stickers') stickers: any
  copUploadFile = new FormControl()
  COP_Approvers= ['23149939', '210066', '212618']
  data: AOA = [
    [1, 2],
    [3, 4],
  ];
  wopts: XLSX.WritingOptions = { bookType: "xlsx", type: "array" };
  quantity: any;
  PFS: any;
  WIP: any;
  TS: any;
  remarks: any;
  urls = [];
  fileUrls = [];
  bs4List = [];
  docDate: Date;
  fileName: any;
  checkedData = [];
  CMRDataSource: any = [];
  VQIDataSource: any = [];
  LRRADataSource: any = [];
  vendorCode: any;
  allChecked: boolean = false;
  pageType: any;
  sideMenuFlag: boolean = false;
  ReportNameList: any[];
  menuFlag: any = "MRQ";
  platformFilter: any = "";
  MRQDataSource: any[];
  MRQYearList: any = [];
  folderList: any = [];
  selectedYear: any;
  selectedMonth: any = "1";
  MRQVariantList: any = [];
  VQIVariantList: any = [];
  stickerSearch: any = "";
  ltpSearch: any = "";
  plantSearch: any = "";
  pageSize: number;
  selectedVariant: any;
  CDMMFormAdmin: FormGroup;
  CMForm: FormGroup;
  cmDataSource: any = [];
  MRQForm: FormGroup;
  ODSForm: FormGroup;
  ASNForm: FormGroup;
  VQIForm: FormGroup;
  LRnRAForm: FormGroup;
  subMenuList: String[];
  DCPUploadMaster: any = [];
  DCPUploadModel: any = [];
  DCPDocumentType: any = [];
  DCPUploadData: any = [];
  COPDataSource: any = [];
  CDMMDataSource: any = [];
  ChangesRelatedTo: any = [];
  BS4Form: FormGroup;

  BS4ExcelData: any = [];
  BSInvUploaddisplayedColumns: String[];
  BSInvUploaddisplayedColumns1: String[];
  bsInventoryUpload = [];
  printPartStickers: any;
  printPartDatasource: any = [];
  printPartdisplayedColumns: string[];
  vendorCodeDetailList: any = [];
  barCodeNumberList: any = [];
  subFolderList: any = [];
  LRnRAVariantList: any = [];
  CDMMForm: FormGroup;
  ChangeManagementForm: FormGroup;
  vendorList: any = [];
  subVendor: any;
  partNumber: any;
  COPPartList: any = [];
  supplierCodeList: any = [];
  supplierName: any;
  supplierLocation: any;
  COPUploadForm: FormGroup;
  DCPUploadForm: FormGroup;
  printStickerForm: FormGroup;
  term$1 = new Subject<string>();
  checkedArray: any = [];
  pageNo: number = 1;
  excelFlag: boolean = false;
  nomineeForm: FormGroup;
  mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
  variantID: any;
  bs4EditFlag: boolean;
  PlantNames = [
    { id: "1", value: "Nasik" },
    { id: "2", value: "Haridwar" },
    { id: "3", value: "Kandivli" },
    { id: "4", value: "Igatpuri" },
    { id: "5", value: "Zaheerabad" },
    { id: "6", value: "MVML" },
    { id: "7", value: "SEL1" },
  ];
  COPPlanFilter: any = "vendorCode";
  dataSource = [
    {
      Vendor: "DM181",
      Description: "Mungi Engineers Ltd",
      Plant: "A003",
      Auto: 15148,
      Manual: 10,
      autoSVN: 100,
    },
    {
      Vendor: "DM181",
      Description: "Mungi Engineers Ltd",
      Plant: "A003",
      Auto: 15148,
      Manual: 10,
      autoSVN: 100,
    },
    {
      Vendor: "DM181",
      Description: "Mungi Engineers Ltd",
      Plant: "A003",
      Auto: 15148,
      Manual: 10,
      autoSVN: 100,
    },
    {
      Vendor: "DM181",
      Description: "Mungi Engineers Ltd",
      Plant: "A003",
      Auto: 15148,
      Manual: 10,
      autoSVN: 100,
    },
    {
      Vendor: "DM181",
      Description: "Mungi Engineers Ltd",
      Plant: "A003",
      Auto: 15148,
      Manual: 10,
      autoSVN: 100,
    },
  ];
  ODSDataSource = [
    {
      Plant: "F003",
      vendorCode: "DM181",
      purchaseOrderNo: "3400320213",
      Item: "00020",
      material: "material",
      Description: "",
      documentChangeNo: "20238832831705",
      basicPrice: 35000.0,
      date1: 1.0,
      date2: 5524.0,
      date3: 6099.0,
      date4: 1.0,
      date5: 1.0,
      date6: 1.0,
      date7: 1.0,
      date8: 1.0,
      date9: 1.0,
      date10: 1.0,
      overallResult: 1.0,
    },
    {
      Plant: "F003",
      vendorCode: "DM181",
      purchaseOrderNo: "3400320213",
      Item: "00020",
      material: "material",
      Description: "",
      documentChangeNo: "20238832831705",
      basicPrice: 35000.0,
      date1: 1.0,
      date2: 5524.0,
      date3: 6099.0,
      date4: 1.0,
      date5: 1.0,
      date6: 1.0,
      date7: 1.0,
      date8: 1.0,
      date9: 1.0,
      date10: 1.0,
      overallResult: 1.0,
    },
    {
      Plant: "F003",
      vendorCode: "DM181",
      purchaseOrderNo: "3400320213",
      Item: "00020",
      material: "material",
      Description: "",
      documentChangeNo: "20238832831705",
      basicPrice: 35000.0,
      date1: 1.0,
      date2: 5524.0,
      date3: 6099.0,
      date4: 1.0,
      date5: 1.0,
      date6: 1.0,
      date7: 1.0,
      date8: 1.0,
      date9: 1.0,
      date10: 1.0,
      overallResult: 1.0,
    },
    {
      Plant: "F003",
      vendorCode: "DM181",
      purchaseOrderNo: "3400320213",
      Item: "00020",
      material: "material",
      Description: "",
      documentChangeNo: "20238832831705",
      basicPrice: 35000.0,
      date1: 1.0,
      date2: 5524.0,
      date3: 6099.0,
      date4: 1.0,
      date5: 1.0,
      date6: 1.0,
      date7: 1.0,
      date8: 1.0,
      date9: 1.0,
      date10: 1.0,
      overallResult: 1.0,
    },
    {
      Plant: "F003",
      vendorCode: "DM181",
      purchaseOrderNo: "3400320213",
      Item: "00020",
      material: "material",
      Description: "",
      documentChangeNo: "20238832831705",
      basicPrice: 35000.0,
      date1: 1.0,
      date2: 5524.0,
      date3: 6099.0,
      date4: 1.0,
      date5: 1.0,
      date6: 1.0,
      date7: 1.0,
      date8: 1.0,
      date9: 1.0,
      date10: 1.0,
      overallResult: 1.0,
    },
  ];
  ODSASNDataSource = [
    {
      Plant: "A005",
      vendor: "DM181D",
      vendorDescription: "MUNGI ENGINEERS PVT	",
      material: "0101BP608030N",
      materialDescription: "BRACKET WHEEL SPEED SENSOR",
      PONumber: "3101631467",
      POItem: "00010",
      TotalSchQty: 5000.0,
      qtyDelivered: 0.0,
      openPOQuantity: 5000.0,
      openASNQty: 0.0,
      actualOpenQuantity: 5000.0,
    },
    {
      Plant: "A005",
      vendor: "DM181D",
      vendorDescription: "MUNGI ENGINEERS PVT	",
      material: "0101BP608030N",
      materialDescription: "BRACKET WHEEL SPEED SENSOR",
      PONumber: "3101631467",
      POItem: "00010",
      TotalSchQty: 5000.0,
      qtyDelivered: 0.0,
      openPOQuantity: 5000.0,
      openASNQty: 0.0,
      actualOpenQuantity: 5000.0,
    },
    {
      Plant: "A005",
      vendor: "DM181D",
      vendorDescription: "MUNGI ENGINEERS PVT	",
      material: "0101BP608030N",
      materialDescription: "BRACKET WHEEL SPEED SENSOR",
      PONumber: "3101631467",
      POItem: "00010",
      TotalSchQty: 5000.0,
      qtyDelivered: 0.0,
      openPOQuantity: 5000.0,
      openASNQty: 0.0,
      actualOpenQuantity: 5000.0,
    },
    {
      Plant: "A005",
      vendor: "DM181D",
      vendorDescription: "MUNGI ENGINEERS PVT	",
      material: "0101BP608030N",
      materialDescription: "BRACKET WHEEL SPEED SENSOR",
      PONumber: "3101631467",
      POItem: "00010",
      TotalSchQty: 5000.0,
      qtyDelivered: 0.0,
      openPOQuantity: 5000.0,
      openASNQty: 0.0,
      actualOpenQuantity: 5000.0,
    },
    {
      Plant: "A005",
      vendor: "DM181D",
      vendorDescription: "MUNGI ENGINEERS PVT	",
      material: "0101BP608030N",
      materialDescription: "BRACKET WHEEL SPEED SENSOR",
      PONumber: "3101631467",
      POItem: "00010",
      TotalSchQty: 5000.0,
      qtyDelivered: 0.0,
      openPOQuantity: 5000.0,
      openASNQty: 0.0,
      actualOpenQuantity: 5000.0,
    },
  ];
  OMRDataSource = [
    {
      documentNo: "1172132749",
      Item: "000001",
      documentDate: "04-27-2017",
      Plant: "A003",
      plantDesc: "",
      supplierInvNo: "",
      Remarks: "REWORK & RETURN",
      materialNo: "0104AAF01850N",
      materialDescription: "NUT PROJECTION WELD HEX M6 X 1",
      vendorCode: "DM181",
      quantityEA: 200.0,
    },
    {
      documentNo: "1172132749",
      Item: "000001",
      documentDate: "04-27-2017",
      Plant: "A003",
      plantDesc: "",
      supplierInvNo: "",
      Remarks: "REWORK & RETURN",
      materialNo: "0104AAF01850N",
      materialDescription: "NUT PROJECTION WELD HEX M6 X 1",
      vendorCode: "DM181",
      quantityEA: 200.0,
    },
    {
      documentNo: "1172132749",
      Item: "000001",
      documentDate: "04-27-2017",
      Plant: "A003",
      plantDesc: "",
      supplierInvNo: "",
      Remarks: "REWORK & RETURN",
      materialNo: "0104AAF01850N",
      materialDescription: "NUT PROJECTION WELD HEX M6 X 1",
      vendorCode: "DM181",
      quantityEA: 200.0,
    },
    {
      documentNo: "1172132749",
      Item: "000001",
      documentDate: "04-27-2017",
      Plant: "A003",
      plantDesc: "",
      supplierInvNo: "",
      Remarks: "REWORK & RETURN",
      materialNo: "0104AAF01850N",
      materialDescription: "NUT PROJECTION WELD HEX M6 X 1",
      vendorCode: "DM181",
      quantityEA: 200.0,
    },
    {
      documentNo: "1172132749",
      Item: "000001",
      documentDate: "04-27-2017",
      Plant: "A003",
      plantDesc: "",
      supplierInvNo: "",
      Remarks: "REWORK & RETURN",
      materialNo: "0104AAF01850N",
      materialDescription: "NUT PROJECTION WELD HEX M6 X 1",
      vendorCode: "DM181",
      quantityEA: 200.0,
    },
  ];
  SRQDataSource = [
    {
      plant: "A002",
      vendor: "PM00094",
      SRQmaterial: "0092702",
      SRQdescription: "ASSY REAR AXLE TUBE LH",
      inboundDeliveryDoc: "6109990977",
      PCSNo: "2804711061",
      PCSDate: "4/21/2018 12:00:00 AM",
      invoiceDate: "4/21/2018 12:00:00 AM	",
      GRNO: "2804722844",
      PCSGRstatus: "-",
      storeRemark: "7 NOS SHORT",
      invoiceQuality: "280.0EA",
      receivedQuality: "266EA",
      shortReceivedQuantity: "14.000",
    },
    {
      plant: "A002",
      vendor: "PM00094",
      SRQmaterial: "0092702",
      SRQdescription: "ASSY REAR AXLE TUBE LH",
      inboundDeliveryDoc: "6109990977",
      PCSNo: "2804711061",
      PCSDate: "4/21/2018 12:00:00 AM",
      invoiceDate: "4/21/2018 12:00:00 AM	",
      GRNO: "2804722844",
      PCSGRstatus: "-",
      storeRemark: "7 NOS SHORT",
      invoiceQuality: "280.0EA",
      receivedQuality: "266EA",
      shortReceivedQuantity: "14.000",
    },
    {
      plant: "A002",
      vendor: "PM00094",
      SRQmaterial: "0092702",
      SRQdescription: "ASSY REAR AXLE TUBE LH",
      inboundDeliveryDoc: "6109990977",
      PCSNo: "2804711061",
      PCSDate: "4/21/2018 12:00:00 AM",
      invoiceDate: "4/21/2018 12:00:00 AM	",
      GRNO: "2804722844",
      PCSGRstatus: "-",
      storeRemark: "7 NOS SHORT",
      invoiceQuality: "280.0EA",
      receivedQuality: "266EA",
      shortReceivedQuantity: "14.000",
    },
    {
      plant: "A002",
      vendor: "PM00094",
      SRQmaterial: "0092702",
      SRQdescription: "ASSY REAR AXLE TUBE LH",
      inboundDeliveryDoc: "6109990977",
      PCSNo: "2804711061",
      PCSDate: "4/21/2018 12:00:00 AM",
      invoiceDate: "4/21/2018 12:00:00 AM	",
      GRNO: "2804722844",
      PCSGRstatus: "-",
      storeRemark: "7 NOS SHORT",
      invoiceQuality: "280.0EA",
      receivedQuality: "266EA",
      shortReceivedQuantity: "14.000",
    },
    {
      plant: "A002",
      vendor: "PM00094",
      SRQmaterial: "0092702",
      SRQdescription: "ASSY REAR AXLE TUBE LH",
      inboundDeliveryDoc: "6109990977",
      PCSNo: "2804711061",
      PCSDate: "4/21/2018 12:00:00 AM",
      invoiceDate: "4/21/2018 12:00:00 AM	",
      GRNO: "2804722844",
      PCSGRstatus: "-",
      storeRemark: "7 NOS SHORT",
      invoiceQuality: "280.0EA",
      receivedQuality: "266EA",
      shortReceivedQuantity: "14.000",
    },
  ];
  RIRDataSource = [
    {
      reAppNo: "",
      PCSNo: "2801218500",
      PCSdate: "03.02.2018",
      Plant: "A005",
      StorageLocation: "LC01",
      Vendor: "DM181D",
      SupplierInvNo: "24043688",
      InvoiceDate: "03.02.2018",
      ASNNumber: "8322712063",
      RIRMaterial: "8001MAG00450N",
      RIRDescription: "REAR BODY ASSY W O PAINT SXJ HSD	",
      PurchaseOrderNo: "6710062906",
      ItemNo: "10",
      CenvatStatus: "Not Availed",
      BillPassingDate: "04.02.2018",
      billPassingStatus: "Billed Passed",
      GRNo: "2801234874",
      GRDate: "03.02.2018",
      PCSGRStatus: "O",
      StoresRemarks: "",
      QMDate: "",
      AcceptanceStatus: "Fully Accpted",
      reasonforRejection: "",
      paymentstatus: "Payment Processed	",
      Paymentdate: "07.03.2018",
      Incoterms: "-",
      IncotermsPart1: "004",
      InvoiceQuantity: "1.000",
      ExciseAmount: "0.000	",
      TotalinvoiceAmount: "37611.140",
      RecivedQty: "1.000",
      AcceptedQty: "1.000",
      RejectedQty: "0.000",
      reAcceptanceQty: "0.000",
    },
    {
      reAppNo: "",
      PCSNo: "2801218500",
      PCSdate: "03.02.2018",
      Plant: "A005",
      StorageLocation: "LC01",
      Vendor: "DM181D",
      SupplierInvNo: "24043688",
      InvoiceDate: "03.02.2018",
      ASNNumber: "8322712063",
      RIRMaterial: "8001MAG00450N",
      RIRDescription: "REAR BODY ASSY W O PAINT SXJ HSD	",
      PurchaseOrderNo: "6710062906",
      ItemNo: "10",
      CenvatStatus: "Not Availed",
      BillPassingDate: "04.02.2018",
      billPassingStatus: "Billed Passed",
      GRNo: "2801234874",
      GRDate: "03.02.2018",
      PCSGRStatus: "O",
      StoresRemarks: "",
      QMDate: "",
      AcceptanceStatus: "Fully Accpted",
      reasonforRejection: "",
      paymentstatus: "Payment Processed	",
      Paymentdate: "07.03.2018",
      Incoterms: "-",
      IncotermsPart1: "004",
      InvoiceQuantity: "1.000",
      ExciseAmount: "0.000	",
      TotalinvoiceAmount: "37611.140",
      RecivedQty: "1.000",
      AcceptedQty: "1.000",
      RejectedQty: "0.000",
      reAcceptanceQty: "0.000",
    },
    {
      reAppNo: "",
      PCSNo: "2801218500",
      PCSdate: "03.02.2018",
      Plant: "A005",
      StorageLocation: "LC01",
      Vendor: "DM181D",
      SupplierInvNo: "24043688",
      InvoiceDate: "03.02.2018",
      ASNNumber: "8322712063",
      RIRMaterial: "8001MAG00450N",
      RIRDescription: "REAR BODY ASSY W O PAINT SXJ HSD	",
      PurchaseOrderNo: "6710062906",
      ItemNo: "10",
      CenvatStatus: "Not Availed",
      BillPassingDate: "04.02.2018",
      billPassingStatus: "Billed Passed",
      GRNo: "2801234874",
      GRDate: "03.02.2018",
      PCSGRStatus: "O",
      StoresRemarks: "",
      QMDate: "",
      AcceptanceStatus: "Fully Accpted",
      reasonforRejection: "",
      paymentstatus: "Payment Processed	",
      Paymentdate: "07.03.2018",
      Incoterms: "-",
      IncotermsPart1: "004",
      InvoiceQuantity: "1.000",
      ExciseAmount: "0.000	",
      TotalinvoiceAmount: "37611.140",
      RecivedQty: "1.000",
      AcceptedQty: "1.000",
      RejectedQty: "0.000",
      reAcceptanceQty: "0.000",
    },
    {
      reAppNo: "",
      PCSNo: "2801218500",
      PCSdate: "03.02.2018",
      Plant: "A005",
      StorageLocation: "LC01",
      Vendor: "DM181D",
      SupplierInvNo: "24043688",
      InvoiceDate: "03.02.2018",
      ASNNumber: "8322712063",
      RIRMaterial: "8001MAG00450N",
      RIRDescription: "REAR BODY ASSY W O PAINT SXJ HSD	",
      PurchaseOrderNo: "6710062906",
      ItemNo: "10",
      CenvatStatus: "Not Availed",
      BillPassingDate: "04.02.2018",
      billPassingStatus: "Billed Passed",
      GRNo: "2801234874",
      GRDate: "03.02.2018",
      PCSGRStatus: "O",
      StoresRemarks: "",
      QMDate: "",
      AcceptanceStatus: "Fully Accpted",
      reasonforRejection: "",
      paymentstatus: "Payment Processed	",
      Paymentdate: "07.03.2018",
      Incoterms: "-",
      IncotermsPart1: "004",
      InvoiceQuantity: "1.000",
      ExciseAmount: "0.000	",
      TotalinvoiceAmount: "37611.140",
      RecivedQty: "1.000",
      AcceptedQty: "1.000",
      RejectedQty: "0.000",
      reAcceptanceQty: "0.000",
    },
    {
      reAppNo: "",
      PCSNo: "2801218500",
      PCSdate: "03.02.2018",
      Plant: "A005",
      StorageLocation: "LC01",
      Vendor: "DM181D",
      SupplierInvNo: "24043688",
      InvoiceDate: "03.02.2018",
      ASNNumber: "8322712063",
      RIRMaterial: "8001MAG00450N",
      RIRDescription: "REAR BODY ASSY W O PAINT SXJ HSD	",
      PurchaseOrderNo: "6710062906",
      ItemNo: "10",
      CenvatStatus: "Not Availed",
      BillPassingDate: "04.02.2018",
      billPassingStatus: "Billed Passed",
      GRNo: "2801234874",
      GRDate: "03.02.2018",
      PCSGRStatus: "O",
      StoresRemarks: "",
      QMDate: "",
      AcceptanceStatus: "Fully Accpted",
      reasonforRejection: "",
      paymentstatus: "Payment Processed	",
      Paymentdate: "07.03.2018",
      Incoterms: "-",
      IncotermsPart1: "004",
      InvoiceQuantity: "1.000",
      ExciseAmount: "0.000	",
      TotalinvoiceAmount: "37611.140",
      RecivedQty: "1.000",
      AcceptedQty: "1.000",
      RejectedQty: "0.000",
      reAcceptanceQty: "0.000",
    },
  ];

  supplierTierDataSource = [
    {
      ID: "115",
      VendorName: "NHP DRIII",
      VendorSize: "Micro",
      VendorTier: "Tier 3",
      Address: "M&M, Samta Nagar Rd, Kandivali East",
      action: "",
    },
    {
      ID: "115",
      VendorName: "NHP DRIII",
      VendorSize: "Micro",
      VendorTier: "Tier 3",
      Address: "M&M, Samta Nagar Rd, Kandivali East",
      action: "",
    },
    {
      ID: "115",
      VendorName: "NHP DRIII",
      VendorSize: "Micro",
      VendorTier: "Tier 3",
      Address: "M&M, Samta Nagar Rd, Kandivali East",
      action: "",
    },
    {
      ID: "115",
      VendorName: "NHP DRIII",
      VendorSize: "Micro",
      VendorTier: "Tier 3",
      Address: "M&M, Samta Nagar Rd, Kandivali East",
      action: "",
    },
    {
      ID: "115",
      VendorName: "NHP DRIII",
      VendorSize: "Micro",
      VendorTier: "Tier 3",
      Address: "M&M, Samta Nagar Rd, Kandivali East",
      action: "",
    },
    {
      ID: "115",
      VendorName: "NHP DRIII",
      VendorSize: "Micro",
      VendorTier: "Tier 3",
      Address: "M&M, Samta Nagar Rd, Kandivali East",
      action: "",
    },
    {
      ID: "115",
      VendorName: "NHP DRIII",
      VendorSize: "Micro",
      VendorTier: "Tier 3",
      Address: "M&M, Samta Nagar Rd, Kandivali East",
      action: "",
    },
    {
      ID: "115",
      VendorName: "NHP DRIII",
      VendorSize: "Micro",
      VendorTier: "Tier 3",
      Address: "M&M, Samta Nagar Rd, Kandivali East",
      action: "",
    },
    {
      ID: "115",
      VendorName: "NHP DRIII",
      VendorSize: "Micro",
      VendorTier: "Tier 3",
      Address: "M&M, Samta Nagar Rd, Kandivali East",
      action: "",
    },
    {
      ID: "115",
      VendorName: "NHP DRIII",
      VendorSize: "Micro",
      VendorTier: "Tier 3",
      Address: "M&M, Samta Nagar Rd, Kandivali East",
      action: "",
    },
    {
      ID: "115",
      VendorName: "NHP DRIII",
      VendorSize: "Micro",
      VendorTier: "Tier 3",
      Address: "M&M, Samta Nagar Rd, Kandivali East",
      action: "",
    },
    {
      ID: "115",
      VendorName: "NHP DRIII",
      VendorSize: "Micro",
      VendorTier: "Tier 3",
      Address: "M&M, Samta Nagar Rd, Kandivali East",
      action: "",
    },
    {
      ID: "115",
      VendorName: "NHP DRIII",
      VendorSize: "Micro",
      VendorTier: "Tier 3",
      Address: "M&M, Samta Nagar Rd, Kandivali East",
      action: "",
    },
  ];
  supplierTierdisplayedColumns = [
    "ID",
    "VendorName",
    "VendorSize",
    "VendorTier",
    "Address",
    "action",
  ];
  QualityTierDataSource: MatTableDataSource<any>;
  supplierCode: string;
  country = [];
  state = [];
  city = [];
  dataSource123: MatTableDataSource<any>;
  today: Date = new Date();
  @ViewChild("copForm") formAward;
  COPUploadisplayedColumns = [];
  COPDownloadColumns = [
    "vendor",
    "partNo",
    "model",
    "documentType",
    "fileName",
    "status",
    "doc",
    "fromDate",
    "toDate",
    "Status",
  ];
  displayedColumns: string[] = [
    "Vendor",
    "Description",
    "Plant",
    "Auto",
    "Manual",
    "autoSVN",
  ];
  MRQdisplayedColumns: string[] = [
    "Plant",
    "plantDescription",
    "Component",
    "materialDescription",
    "Vendor",
    "vendorDescription",
    "reportingMonth",
    "LTPVersion",
    "netRequirementMay",
    "netRequirementJun",
    "netRequirementJul",
  ];
  ODSdisplayedColumns: string[] = [
    "Plant",
    "vendorCode",
    "purchaseOrderNo",
    "Item",
    "material",
    "Description",
    "documentChangeNo",
    "basicPrice",
    "date1",
    "date2",
    "date3",
    "date4",
    "date5",
    "date6",
    "date7",
    "date8",
    "date9",
    "date10",
    "overallResult",
  ];
  ODSASNdisplayedColumns: string[] = [
    "Plant",
    "vendor",
    "vendorDescription",
    "material",
    "materialDescription",
    "PONumber",
    "POItem",
    "TotalSchQty",
    "qtyDelivered",
    "openPOQuantity",
    "openASNQty",
    "actualOpenQuantity",
  ];
  OMRdisplayedColumns: string[] = [
    "documentNo",
    "Item",
    "documentDate",
    "Plant",
    "plantDesc",
    "supplierInvNo",
    "Remarks",
    "materialNo",
    "materialDescription",
    "vendorCode",
    "quantityEA",
  ];
  SRQdisplayedColumns: string[] = [
    "plant",
    "vendor",
    "SRQmaterial",
    "SRQdescription",
    "inboundDeliveryDoc",
    "PCSNo",
    "PCSDate",
    "invoiceDate",
    "GRNO",
    "PCSGRstatus",
    "storeRemark",
    "invoiceQuality",
    "receivedQuality",
    "shortReceivedQuantity",
  ];
  RIRdisplayedColumns: string[] = [
    "reAppNo",
    "PCSNo",
    "PCSdate",
    "Plant",
    "StorageLocation",
    "Vendor",
    "SupplierInvNo",
    "InvoiceDate",
    "ASNNumber",
    "RIRMaterial",
    "RIRDescription",
    "PurchaseOrderNo",
    "ItemNo",
    "CenvatStatus",
    "BillPassingDate",
    "billPassingStatus",
    "GRNo",
    "GRDate",
    "PCSGRStatus",
    "StoresRemarks",
    "QMDate",
    "AcceptanceStatus",
    "reasonforRejection",
    "paymentstatus",
    "Paymentdate",
    "Incoterms",
    "IncotermsPart1",
    "InvoiceQuantity",
    "ExciseAmount",
    "TotalinvoiceAmount",
    "TotalinvoiceAmount",
    "RecivedQty",
    "AcceptedQty",
    "RejectedQty",
    "reAcceptanceQty",
  ];
  DCPUploaddisplayedColumns: string[] = [
    "vendorCode",
    "partNo",
    "model",
    "documentType",
    "fileName",
    "uploadDate",
    "status",
  ];
  CDMMdisplayedColumns: string[] = [
    "vendor",
    "partNo",
    "model",
    "documentType",
    "fileName",
    "doc",
    "uploadDate",
  ];
  vendorDetails: any = [];
  vendorC: string;
  partFinishStock: any;
  newForm = new FormControl();
  newForm1 = new FormControl();
  WIPStock: any;
  tierStock: any;
  remarksBS4: any;
  tabIndex: any = 0;
  SideMenuForReports = [];
  RoleID: any;
  adminVendorCode: any = "";
  username: string;
  qualityTierForm: FormGroup;
  Tier_data_By_ID = [];
  SupID: any;
  fetchYear: any = "Previous";
  Json = [];

  pageNumbers: number[];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Input() showFirstLastButtons = true;
  perPageevent: boolean = false;
  browser: string;
  masterVendorCode = CryptoJS.AES.decrypt(localStorage.getItem('MV'), "").toString(CryptoJS.enc.Utf8);

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    public datepipe: DatePipe,
    public cs: MySuggestionService,
    private toastr: ToastrService,
    public commonAPIService: CommonApiService,
    public _dataService: DataServiceService,
    private _formBuilder: FormBuilder,
    public myBusinessService: MyBusinessServiceService,
    public commonService: CommonUtilityService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public dashboardComponent: DashboardComponent
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.getMRQVariant();
    this.pageType = this._dataService.getOption();
    this.getReportNameList();
    this.initNominees();

    this.initCDMMForm();
    this.initCDMMFormAdmin();
    this.initprintStickerForm();
    this.initCOPUploadForm();
    this.initDCPUploadForm();
    this.initMRQForm();
    this.initODSForm();
    this.initASNForm();
    this.initVQIForm();
    this.initLRnRAForm();
    // this.onTabChange({index :0})

    if (this.menuFlag == "MRQ") {
      this.getMRQYearList();
      this.term$1
        .pipe(debounceTime(100))
        .pipe(distinctUntilChanged())
        .subscribe((param) => {
          console.dir("search value--" + param);
          this.stickerSearch = param;
          this.getStickerData({
            pageIndex: 0,
            pageSize: 10,
            length: this.pageSize,
          });
        });
    }

    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelWhiteIcon ",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/excel.svg"
      )
    );
    //  matIconRegistry.addSvgIcon('downloadIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/excel.svg'));
    matIconRegistry.addSvgIcon(
      "folderIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "assets/Icons/myLibrary/Folder.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "excelNewIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "downloadIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/downloadicon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "edit",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Edit.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "delete",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/delete.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "pdfIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/pdfIcon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "imgIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/imageIcon.svg"
      )
    );
  }

  QualityTierDDLArray = [
    { value: "Certification to Tier II", Array: "Certification_to_Tier_II" },
    { value: "Commodity of Tier I", Array: "Commodity_of_Tier_I" },
    { value: "Type of Supplies", Array: "Type_of_Supplies" },
    { value: "Vehicle Model", Array: "Vehicle_Model" },
    { value: "Vendor size", Array: "Vendor_size" },
  ];

  Certification_to_Tier_II = [];
  Commodity_of_Tier_I = [];
  Type_of_Supplies = [];
  Vehicle_Model = [];
  Vendor_size = [];
  reference_heading: any;
  lastDay: Date;
  cdmmAdmin: boolean;

  @Input("length") set lengthChanged(value: number) {
    this.length = value;
    this.updateGoto();
  }
  @Input("pageSize") set pageSizeChanged(value: number) {
    this.pageSize = value;
    this.updateGoto();
  }

  ngOnInit() {
    $(document).ready(function () {
      //alert();
      //$(".subMenuList").removeClass("active");
      var test1 = $(".subMenuList");
      $(".subMenuList").click(function () {
        alert("");
        $(".subMenuList").removeClass("active");
        test1.addClass("active");
      });
    });

    let date = new Date();
    this.lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    this.RoleID = localStorage.getItem("WTIwNWMxcFZiR3M9");
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(
      CryptoJS.enc.Utf8
    );
    if (this.RoleID.includes("29")) this.cdmmAdmin = true;
    else this.cdmmAdmin = false;

    this.username = localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ==");
    this.username = CryptoJS.AES.decrypt(this.username, "").toString(
      CryptoJS.enc.Utf8
    );

    this.supplierCode = localStorage.getItem(
      "WkcxV2RWcEhPWGxSTWpscldsRTlQUT09"
    );
    this.supplierCode = CryptoJS.AES.decrypt(this.supplierCode, "").toString(
      CryptoJS.enc.Utf8
    );

    this.browser = localStorage.getItem("browser");
    this.createQualityTierForm();
    this.getqualityTierDDL();
    this.getCountry();
    this.pageType = this._dataService.getOption();

    if (this.RoleID == "7" || this.checkCOPApprover()) {
      this.COPUploadisplayedColumns = [
        "vendor",
        "partNo",
        "model",
        "documentType",
        "fileName",
        "status",
        "doc",
        "fromDate",
        "toDate",
        "Status",
        "action",
      ];
    } else {
      this.COPUploadisplayedColumns = [
        "vendor",
        "partNo",
        "model",
        "documentType",
        "fileName",
        "status",
        "doc",
        "fromDate",
        "toDate",
        "Status",
      ];
    }
    this.getVendorDetailsByUserName();
    this.getDCPUploadMaster();
    this.getSupplierCodeList();
    this.getVendorCode();
    this.SideMenuForReports = this.cs.SideMenuForReports;

    if (this.pageType.Page == "NPD") {
      this.onTabChange({ index: 0 }, 111, 112, 0);
    }
    this.getReportNameList();
    this.partFinishStock = new FormControl();
    this.WIPStock = new FormControl();
    this.tierStock = new FormControl();
    this.remarks = new FormControl();
    for (let i = 0; i < this.printPartDatasource.length; i++) {
      this.quantity[i] = new FormControl();
    }
    this.showReport = false;
    if (this.pageType.Page != "PF") {
      setTimeout(() => {
        this.showReport = true;
        this.tabIndex = 0;
      }, 100);
    }
    
  }

  ngAfterViewInit(){
    this.getSideMenuForReports(this.RoleID).then(res=>{
      console.log(res)
      if(this.route.queryParams['_value'].t > 0){
        this.route.queryParams.subscribe(params => {
          if(params){
            if(this.componentTabs){
              this.componentTabs.selectedIndex = (parseInt(params.t) - 1)
              this.tabIndex = parseInt(params.t) - 1
              console.log(this.SideMenuForReports)
              if(this.route.queryParams['_value'].m){
                setTimeout(() => {
                  let item = this.SideMenuForReports.find(o=>o.NavigationName == this.route.queryParams['_value'].m)
                  let i = this.SideMenuForReports.findIndex(o=>o.NavigationName == this.route.queryParams['_value'].m)
  
                  this.subMenuListClicked(item.NavigationName,item.IsSRMMenu,item.NavigationURL, i)
                  this.reportSubmenuClick(item.ParentName,item.NavigationName,item.IsSRMMenu,item.NavigationURL, i)
                }, 2000);
              }
              // this.onTabChange({index: parseInt(params.t) - 1},76,77,78)
              // this.reportSubmenuClick("Quality", "Upload COP",false,"https://supplier.mahindra.com/sites/supplierportal/_layouts/msetu.cop/copupload.aspx",8)
              // if(this.route.queryParams['_value'].m){
              //   this.route.queryParams.subscribe(params => {
              //     if(params){
              //       console.log(params)
              //       console.log(this.SideMenuForReports)
              //     }
              //   })
              // }
            }
          }
        })
      }
    })
  }

  btn_name = "Submit";
  downloadFonts() {
    // let link = document.createElement("a");
    // link.setAttribute("type", "hidden");
    // link.href = "assets/file";
    // link.download = window.location.origin+"/assets/fonts/Free_3_of_9/FREE3OF9.TTF";
    // document.body.appendChild(link);
    // link.click();
    // link.remove();
    window.open(
      window.location.origin + "/assets/fonts/Free_3_of_9/FREE3OF9.TTF",
      "_blank"
    );
  }
  getCountry(): Promise<any> {
    return new Promise((resolve: any) => {
      let Request = {
        GetCountryStateCity: "Country",
      };
      this.cs
        .postMySuggestionData("QualityTier2/GetCountryStateCity", Request)
        .subscribe((res: any) => {
          if (res.Message == "Success") {
            this.country = res.ResponseData;
            resolve();
          } else {
            this.cs.showError("Something went wrong please try again later");
            return;
          }
        });
    });
  }

  changeCOPDatasource() {
    this.getCOPList({ pageIndex: 0 }, false);
  }
  getState(event: any): Promise<any> {
    return new Promise((resolve: any) => {
      let Request = {
        GetCountryStateCity: "State",
        Country: event.value,
      };
      this.cs
        .postMySuggestionData("QualityTier2/GetCountryStateCity", Request)
        .subscribe((res: any) => {
          if (res.Message == "Success") {
            this.state = res.ResponseData;
            resolve();
          } else {
            this.cs.showError("Something went wrong please try again later");
            return;
          }
        });
    });
  }

  getCity(event: any): Promise<any> {
    return new Promise((resolve: any) => {
      let Request = {
        Country: "India",
        State: event.value,
        GetCountryStateCity: "City",
      };
      this.cs
        .postMySuggestionData("QualityTier2/GetCountryStateCity", Request)
        .subscribe((res: any) => {
          if (res.Message == "Success") {
            this.city = res.ResponseData;
            resolve();
          } else {
            this.cs.showError("Something went wrong please try again later");
            return;
          }
        });
    });
  }

  getqualityTierData(Year, SupId, event: any): Promise<any> {
    return new Promise((resolve: any) => {
      let Request = {
        SupCode: this.supplierCode,
        SchID: Year,
        SupId: SupId,
        GetAllData: "",
        PageNo: event.pageIndex + 1,
      };
      // this.fetchYear=='Previous'?this.fetchYear='Current':this.fetchYear='Previous';
      this.cs
        .postMySuggestionData("QualityTier2/GetSupplierTierInfo", Request)
        .subscribe((res: any) => {
          if (!this.cs.isUndefinedORNull(res) && res.Message == "Success") {
            this.fetchYear = "Current";
            if (res.ResponseData.length > 0)
              this.pageSize = res.ResponseData[0].TotalCount;
            this.QualityTierDataSource = new MatTableDataSource(
              res.ResponseData
            );
            this.QualityTierDataSource.filterPredicate = (
              d: any,
              filter: string
            ) => {
              const textToSearch =
                (d["VendorName"] && d["VendorName"].toLowerCase()) || "";
              return textToSearch.indexOf(filter) !== -1;
            };
            resolve();
          } else {
            this.pageSize = 0;
            resolve();
          }
        });
    });
  }

  createQualityTierForm() {
    this.qualityTierForm = this._formBuilder.group({
      vendor_name: ["", Validators.required],
      Commodityof_Tier: ["", Validators.required],
      vendor_size: ["", Validators.required],
      UAN: [""],
      Type_of_Supplies: ["", Validators.required],
      Vehicle_Model: ["", Validators.required],
      Certification_to_Tier_II: ["", Validators.required],
      Vendor_Tier: [""],
      Product_Family: [""],
      Address: [""],
      Country: ["", Validators.required],
      State: ["", Validators.required],
      City: ["", Validators.required],
      Zip_Code: ["", Validators.required],
      Name: ["", Validators.required],
      Cell_Number: ["", [Validators.required]],
      Email_ID: [
        "",
        [
          Validators.required,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$"),
        ],
      ],
    });
  }

  getqualityTierDataByID(Year, ID): Promise<any> {
    return new Promise((resolve: any) => {
      let Request = {
        SupCode: this.supplierCode,
        SchID: Year,
        SupId: ID,
        GetAllData: "",
      };
      this.cs
        .postMySuggestionData("QualityTier2/GetSupplierTierInfo", Request)
        .subscribe((res: any) => {
          if (!this.cs.isUndefinedORNull(res) && res.Message == "Success") {
            this.Tier_data_By_ID = res.ResponseData;
            resolve();
          }
        });
    });
  }

  openSupplierModal(content, element, btn_name) {
    this.btn_name = btn_name;
    this.SupID = element.ID;
    this.qualityTierForm.reset();
    if (!this.cs.isUndefinedORNull(element) && btn_name == "Update") {
      this.getqualityTierDataByID("FY 2020", element.ID).then((data: any) => {
        this.qualityTierForm
          .get("vendor_name")
          .setValue(this.Tier_data_By_ID[0]["VendorName"]);
        this.qualityTierForm
          .get("Commodityof_Tier")
          .setValue(this.Tier_data_By_ID[0]["CommodityOfTierI"]);
        this.qualityTierForm
          .get("vendor_size")
          .setValue(this.Tier_data_By_ID[0]["VendorSize"]);
        this.qualityTierForm
          .get("UAN")
          .setValue(this.Tier_data_By_ID[0]["UAN"]);
        this.qualityTierForm
          .get("Type_of_Supplies")
          .setValue(this.Tier_data_By_ID[0]["TypeOfSupplies"]);
        this.qualityTierForm
          .get("Vehicle_Model")
          .setValue(this.Tier_data_By_ID[0]["VehicleModel"]);
        this.qualityTierForm
          .get("Certification_to_Tier_II")
          .setValue(this.Tier_data_By_ID[0]["CertificationToTierII"]);
        this.qualityTierForm
          .get("Vendor_Tier")
          .setValue(this.Tier_data_By_ID[0]["VendorTier"]);
        this.qualityTierForm
          .get("Product_Family")
          .setValue(this.Tier_data_By_ID[0]["ProductFamily"]);
        this.qualityTierForm
          .get("Address")
          .setValue(this.Tier_data_By_ID[0]["FullAddress"]);
        this.qualityTierForm
          .get("Zip_Code")
          .setValue(this.Tier_data_By_ID[0]["PostalCode"]);
        this.qualityTierForm
          .get("Name")
          .setValue(this.Tier_data_By_ID[0]["Name"]);
        this.qualityTierForm
          .get("Cell_Number")
          .setValue(this.Tier_data_By_ID[0]["CellNumber"]);
        this.qualityTierForm
          .get("Email_ID")
          .setValue(this.Tier_data_By_ID[0]["EmailID"]);
      });
    }
    this.modalService.open(content, {
      size: "xl" as "lg",
      centered: true,
      windowClass: "formModal",
    });
  }

  applyFilter(filterValue: string) {
    this.QualityTierDataSource.filter = filterValue.trim().toLowerCase();
  }

  resetPartNumber() {
    this.stickerSearch = "";
    this.getPartSticker({ pageIndex: 0 });
  }

  getqualityTierDDL() {
    this.QualityTierDDLArray.forEach((data) => {
      let Request = {
        CategoryType: data.value,
      };
      this.cs
        .postMySuggestionData("QualityTier2/GetSupplierTierMaster", Request)
        .subscribe((res: any) => {
          if (!this.cs.isUndefinedORNull(res) && res.Message == "Success") {
            this[data["Array"]] = res.ResponseData;
          } else {
            this.cs.showError("unable to fetch drop down value");
            return;
          }
        });
    });
  }

  addqualityTierData() {
    if (this.qualityTierForm.valid) {
      let url;
      let Request = {
        VendorName: this.qualityTierForm.controls.vendor_name.value,
        VendorSize: this.qualityTierForm.controls.vendor_size.value,
        VendorTier: this.qualityTierForm.controls.Vendor_Tier.value,
        ParentCode: this.supplierCode,
        PostalCode: this.qualityTierForm.controls.Zip_Code.value,
        FullAddress: this.qualityTierForm.controls.Address.value,
        lat: "",
        lng: "",
        SchID: "FY 2020",
        City: this.qualityTierForm.controls.City.value,
        State: this.qualityTierForm.controls.State.value,
        Country: this.qualityTierForm.controls.Country.value,
        CommidityTierI: this.qualityTierForm.controls.Commodityof_Tier.value,
        TypeOFSupplies: this.qualityTierForm.controls.Type_of_Supplies.value,
        VModel: this.qualityTierForm.controls.Vehicle_Model.value,
        cerTierII: this.qualityTierForm.controls.Certification_to_Tier_II.value,
        cellNumber: this.qualityTierForm.controls.Cell_Number.value,
        Email: this.qualityTierForm.controls.Email_ID.value,
        ProductFamily: this.qualityTierForm.controls.Product_Family.value,
        UAN: this.qualityTierForm.controls.UAN.value,
        Name: this.qualityTierForm.controls.Name.value,
      };
      if (this.btn_name == "Submit") {
        url = "QualityTier2/AddSupplierTierInfo";
      } else {
        Request["SupID"] = this.SupID;
        url = "QualityTier2/UpdateSupplierTierInfo";
      }
      this.cs.postMySuggestionData(url, Request).subscribe((res: any) => {
        if (!this.cs.isUndefinedORNull(res) && res.Message == "Success") {
          this.getqualityTierData("FY 2020", null, { pageIndex: 0 });
          this.cs.showSuccess(
            "Data " +
              (this.btn_name == "Submit" ? "inserted" : "updated") +
              " successfully"
          );
          this.qualityTierForm.reset();
          this.closeModal();
        } else {
          this.cs.showError("Something went wrong please try again later");
          this.qualityTierForm.reset();
          return;
        }
      });
    } else {
      this.cs.showError("Please enter all mandatory fields");
      return;
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onTabChange(event, reportID, transactionID, referenceID) {
    this.selectedIndex = 0;
    this.selectedIndex1 = 0;

    this.expiryDate = "";
    this.reference_heading = "";
    this.getVendorDetailsByUserName();
    this.RoleID = this.RoleID; // localStorage.getItem('roleId');
    this.tabIndex = event.index;
    if (this.tabIndex == 0) {
      this._dataService.ParentId = reportID;
      this.getSideMenuForReports(this.RoleID);
    } else if (this.tabIndex == 1) {
      this._dataService.ParentId = transactionID;
      this.getSideMenuForReports(this.RoleID);
      if (this.pageType.Page == "NPD") {
        this.getFolderData();
      }
    } else if (this.tabIndex == 2) {
      this.getFolderData();
    }
    if (event.index == "0") {
      if (this.pageType.Page == "OS") {
        this.subMenuList = ["MRQ", "ODS", "ODS ASN"];
        this.menuFlag = "MRQ";
        this.getODSPageChangeData({
          pageIndex: 0,
          pageSize: 10,
          length: this.pageSize,
        });
      } else if (this.pageType.Page == "Q") {
        this.subMenuList = ["VQI", "Change Management", "LR & RA"];
        this.menuFlag = "VQI";
      } else if (this.pageType.Page == "NPD") {
        this.getVendorCode();
        this.getSideMenuForReports(this.RoleID);
        this.initCDMMForm();
        this.initCDMMFormAdmin();
      }
    } else if (event.index == "1") {
      if (this.pageType.Page == "OS") {
        this.BSInvUploaddisplayedColumns1 = [
          "vendorCode",
          "date",
          "partNo",
          "partDescription",
          "partStock",
          "partStockUnit",
          "WIPRMStock",
          "WIPRMStockUnit",
          "tierStock",
          "tierStockUnit",
          "remarks",
          "stockDate",
        ];
        this.BSInvUploaddisplayedColumns = [
          "vendorCode",
          "partNo",
          "partDescription",
          "partStock",
          "partStockUnit",
          "WIPRMStock",
          "WIPRMStockUnit",
          "tierStock",
          "tierStockUnit",
          "remarks",
        ];
        this.subMenuList = ["BS4 inventory upload", "BS4 inventory view"];
      } else if (this.pageType.Page == "SS") {
        this.subMenuList = ["Print Part Sticker"];
        this.printPartdisplayedColumns = [
          "selectCheck",
          "partNo",
          "partDescription",
          "MPQ",
          "quantity",
          "printedLbl",
        ];

        this.getStickerData({
          pageIndex: 0,
          pageSize: 10,
          length: this.pageSize,
        });
      } else if (this.pageType.Page == "Q") {
        this.subMenuList = [
          "Change Management",
          "Upload COP",
          "Upload DCP Document",
        ];
        if (this.RoleID == 7) this.menuFlag = "CM - Plant Approver Mapping";
        // else
        // this.menuFlag = 'Change Management';
        this.initChangeManagementForm();
        this.initCOPUploadForm();
        this.initCMForm();
        this.getCMDataSource({
          pageIndex: 0,
          pageSize: 10,
          length: this.pageSize,
        });
        this.getCOPListDataSource({
          pageIndex: 0,
          pageSize: 10,
          length: this.pageSize,
        });
        this.getChangesRelatedTo();
      } else if (this.pageType.Page == "P") {
      } else if (this.pageType.Page == "PF") {
      }
    }
  }

  resetCDMMFormAdmin() {
    this.CDMMFormAdmin.reset();
  }

  private subscription: Subscription = new Subscription();
  initCDMMFormAdmin() {
    this.CDMMFormAdmin = this._formBuilder.group({
      vendorCode: [""],
      platform: [""],
      folder: [""],
      subFolder: [""],
    });
  }

  getCDMMPageAdminChangeData(event) {
    // this.getCDMMData(event);
    this.getAdminCDMMData(event);
  }
  getCDMMAdmin() {
    this.getAdminCDMMData({
      pageIndex: 0,
      pageSize: 10,
      length: this.pageSize,
    });
  }

  getAdminCDMMData(event) {
    this.pageNo = event.pageIndex;
    let reqjson = {
      VendorCode: this.CDMMFormAdmin.get("vendorCode").value,
      PageNo: this.pageNo + 1,
      Platform: this.CDMMFormAdmin.get("platform").value,
      Folder: this.CDMMFormAdmin.get("folder").value,
      SubFolder: this.CDMMFormAdmin.get("subFolder").value,
    };
    this.myBusinessService.getAdminDataCDMM(reqjson).subscribe(
      (resp: any) => {
        this.CDMMAdminDataSource = resp.ResponseData;
        this.pageSize = resp.Count;
      },
      (err) => {}
    );
  }

  selectedPlant(e, pl) {
    console.log(e);
    console.log(pl);
    if (e.checked == true) {
      this.checkedArray.push({ PlantName: pl.value });
      console.log("----->>>", this.checkedArray);
    } else {
      for (let i = 0; i < this.checkedArray.length; i++) {
        if (this.checkedArray[i].PlantName == pl.value) {
          this.checkedArray.splice(i, 1);
        }
      }
    }
  }

  referenceDoc: any = [];

  downloadReferenceDoc(item) {
    FileSaver.saveAs(item.Doc_Link, item.Doc_Link);
  }

  callDocumentReference(folderId, name) {
    this.reference_heading = name;
    this.pageNo = 0;
    let subcategoryID;
    if (this.pageType.Page == "OS") subcategoryID = "1";
    else if (this.pageType.Page == "SS") subcategoryID = "2";
    else if (this.pageType.Page == "Q") subcategoryID = "3";
    else if (this.pageType.Page == "P") subcategoryID = "4";
    else if (this.pageType.Page == "NPD") subcategoryID = "5";
    else if (this.pageType.Page == "PF") subcategoryID = "6";

    let reqjson = {
      BusinessCategoryID: "1",
      BusinessSubCategoryID: subcategoryID,
      PageNo: this.pageNo + 1,
      folderId: folderId,
    };
    this.myBusinessService.getDocumentReference(reqjson).subscribe(
      (resp: any) => {
        this.pageSize = 9;
        this.referenceDoc = resp.ResponseData;
      },
      (err) => {}
    );
  }

  SideMenuForReference: any = [];

  getFolderData() {
    let subcategoryID;
    if (this.pageType.Page == "OS") subcategoryID = "1";
    else if (this.pageType.Page == "SS") subcategoryID = "2";
    else if (this.pageType.Page == "Q") subcategoryID = "3";
    else if (this.pageType.Page == "P") subcategoryID = "4";
    else if (this.pageType.Page == "NPD") subcategoryID = "5";
    else if (this.pageType.Page == "PF") subcategoryID = "6";
    let reqjson = {
      BusinessCategoryID: "1",
      BusinessSubCategoryID: subcategoryID,
    };
    this.myBusinessService.getFolderList(reqjson).subscribe(
      (resp: any) => {
        this.SideMenuForReference = resp.ResponseData;
        if (!this.cs.isUndefinedORNull(this.SideMenuForReference)) {
          this.callDocumentReference(
            this.SideMenuForReference[0].Doc_Id,
            this.SideMenuForReference[0].Doc_Name
          );
          // this.reference_heading=this.SideMenuForReference[0].Doc_Name;
        } else this.callDocumentReference(0, "");
      },
      (err) => {}
    );
  }

  @ViewChild("check") check;

  initCMForm() {
    this.CMForm = this._formBuilder.group({
      plantName: [
        "",
        [Validators.required, Validators.pattern("^[a-zA-Z -']+")],
      ],
      QualityChampEmail: [
        "",
        [Validators.required, Validators.pattern(this.emailPattern)],
      ],
      DesignChampEmail: [
        "",
        [Validators.required, Validators.pattern(this.emailPattern)],
      ],
    });
  }

  deleteApprover(data) {
    let reqjson = {
      TransChngMappingID: data.TransChngMappingID,
    };
    this.myBusinessService.deleteData(reqjson).subscribe(
      (resp: any) => {
        this.toastr.success("Deleted successfully");
        this.getCMDataSource({
          pageIndex: 0,
          pageSize: 10,
          length: this.pageSize,
        });
      },
      (err) => {
        this.toastr.error("Something went wrong");
      }
    );
  }

  addMapping() {
    if (this.CMForm.valid) {
      let reqjson = {
        PlantName: this.CMForm.get("plantName").value,
        EmailId: this.CMForm.get("QualityChampEmail").value,
        title: "",
        ChampEMAIL: this.CMForm.get("DesignChampEmail").value,
      };
      this.myBusinessService.submitCMData(reqjson).subscribe(
        (resp: any) => {
          this.toastr.success("Added successfully");
          this.getCMDataSource({
            pageIndex: 0,
            pageSize: 10,
            length: this.pageSize,
          });
          this.CMForm.reset();
          this.closeModal();
        },
        (err) => {
          this.CMForm.reset();
          this.toastr.error("Something went wrong");
        }
      );
    } else {
      this.validateAllFormFields(this.CMForm);
    }
  }
  getCMDataSource(event) {
    this.pageNo = event.pageIndex;
    let reqjson = {
      PlantName: "",
      PageNo: this.pageNo + 1,
    };
    this.myBusinessService.getCMDataSource(reqjson).subscribe(
      (resp: any) => {
        this.cmDataSource = resp.ResponseData;
        if (this.cs.isUndefinedORNull(this.cmDataSource)) {
          this.cmDataSource = [];
        }
        this.pageSize = resp.Count;
      },
      (err) => {}
    );
  }

  //--------------------------validation -number-alphabets-----------------------------------------------
  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  lettersOnly(event) {
    const charCode = event.which ? event.which : event.keyCode;
    if (
      !(charCode >= 65 && charCode <= 122) &&
      charCode != 32 &&
      charCode != 0
    ) {
      event.preventDefault();
    }
  }
  //---------------------------Open Delivery Schedule (ODS)-----------------------------------------------
  initODSForm() {
    this.ODSForm = this._formBuilder.group({
      variant: [""],
      fromDate: [""],
      toDate: [""],
      plantCode: [""],
      materialNo: [""],
      purchaseOrderNo: [""],
      ScheduleMonth: [""],
      VendorCode: [""],
    });
  }
  //getODSPageChangeData({ pageIndex: 0, pageSize: 10, length: this.pageSize })
  getODSPageChangeData(event) {
    this.getODSReportData(event);
  }
  saveODSDetails() {
    // need to check with amrut
  }
  getODSReportData(event) {
    this.pageNo = event.pageIndex;
    let reqjson = {
      Plant: this.ODSForm.get("plantCode").value,
      VendorCode: this.ODSForm.get("VendorCode").value,
      Material: this.ODSForm.get("materialNo").value,
      ReportingMonth: this.ODSForm.get("ScheduleMonth").value,
      PurchaseOrderNo: this.ODSForm.get("purchaseOrderNo").value,
      FromDate: this.ODSForm.get("fromDate").value,
      RoleID: this.RoleID,
      ToDate: this.ODSForm.get("toDate").value,
    };
    this.myBusinessService.getODSReport(reqjson).subscribe(
      (resp: any) => {
        this.ODSDataSource = resp.ResponseData;
        this.pageSize = resp.Count;
      },
      (err) => {}
    );
  }
  resetODSForm() {
    this.ODSForm.reset();
  }

  openCOPDoc(value) {
    window.open(value, "_blank");
  }
  COPFlag = false;
  copExportData: any = [];
  exportDataToExcel() {
    // let ReportName = "COPData" + moment().format("DD_MM_YYYY_h_mm_ss_a");
    // const tbl = document.getElementById("cop");
    // const wb = XLSX.utils.table_to_book(tbl);

    // /* save to file */
    // XLSX.writeFile(wb, `${ReportName}.xlsx`);
    // this.rprtName = ReportName;
    this.getCOPDetailsforAdmin(0,true, true)
  }
  getCOPDetailsforAdmin(event, flag, excel?: any) {
    this.pageNo = event.pageIndex;
    console.log("", this.COPUploadForm.get("subVendorCode").value);
    let reqjson = {
      SupCode: this.cs.isUndefinedORNull(
        this.COPUploadForm.get("subVendorCode").value
      )
        ? ""
        : this.COPUploadForm.get("subVendorCode").value,
      PageNo: flag ? this.pageNo : this.pageNo + 1,
      TacNo: this.cs.isUndefinedORNull(
        this.COPUploadForm.get("TACNumber").value
      )
        ? ""
        : this.COPUploadForm.get("TACNumber").value,
      CertificateNo: this.cs.isUndefinedORNull(
        this.COPUploadForm.get("certificateNumber").value
      )
        ? ""
        : this.COPUploadForm.get("certificateNumber").value,
      PartNo: this.cs.isUndefinedORNull(
        this.COPUploadForm.get("partNumber").value
      )
        ? ""
        : this.COPUploadForm.get("partNumber").value,
    };
    this.myBusinessService.getAdimCOPDetails(reqjson).subscribe(
      (resp: any) => {
        if(excel){
          // this.cs.exportAsExcelFile(resp.ResponseData, 'Report')
          this.exportAsExcel(resp.ResponseData)
        }
        if (flag) {
          this.copExportData = resp.ResponseData;
          if (this.cs.isUndefinedORNull(this.copExportData)) {
            this.copExportData = [];
          }
        } else {
          this.COPDataSource = resp.ResponseData;
          this.COPFlag = true;
          if (this.cs.isUndefinedORNull(this.COPDataSource)) {
            this.COPDataSource = [];
            this.toastr.error("No Data Found");
          }
          this.pageSize = resp.TotalCount;
        }
      },
      (err) => {}
    );
  }

  exportAsExcel(data){
    let dataNew = data;
    console.log(this.coptabs.selectedIndex)
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('Report Data');
     //Add Header Row
    // console.log(this.COPUploadisplayedColumns)
    
    let header=Object.keys(data[0]);

    // For COP Plan's First tab
    if(this.coptabs.selectedIndex == 0 && this.menuFlag == 'COP Plan'){
      header.splice(0,1)
      header.splice(4,1)
      // header.splice(7,1)
      header.splice(10,5)
      header.unshift(header.splice(7,1)[0])
      this.swapIndex(header, 3, 4)
      header.splice(5,0,header.splice(9,1)[0])
      header.splice(6,0,header.splice(8,1)[0])
      header[9] = "Status"
    }

    let headerRow = worksheet.addRow(header);
        // Cell Style : Fill and Border
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: '00FF0000' },
          bgColor: { argb: '00FF0000' }
        }
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      })
      dataNew.forEach(d => {
        var values = Object.keys(d).map(function (key) { return d[key]; });

        // For COP Plan's First tab
        if(this.coptabs.selectedIndex == 0 && this.menuFlag == 'COP Plan'){
          values.splice(0,1)
          values.splice(4,1)
          // values.splice(7,1)
          values.splice(10,5)
          values[4] = moment(new Date(values[4])).format('DD/MM/YYYY')
          values[5] = moment(new Date(values[5])).format('DD/MM/YYYY')
          values.unshift(values.splice(7,1)[0])
          this.swapIndex(values, 3, 4)
          values.splice(5,0,values.splice(9,1)[0])
          values.splice(6,0,values.splice(8,1)[0])
        }

        console.log(values);
          let row = worksheet.addRow(values);
      });
    workbook.xlsx.writeBuffer().then((dataNew) => {
      let blob = new Blob([dataNew], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, 'reportData.xlsx');
    });
  }

  swapIndex(arr, ind1, ind2){
    let temp= arr[ind1];
    arr[ind1] = arr[ind2];
    arr[ind2] = temp;
  }
  //----------------------------------end of ODS ---------------------------------------------------------
  //-------------------------- ASN Performance Report-------------------------------------------------------
  initASNForm() {
    this.ASNForm = this._formBuilder.group({
      variant: [""],
      fromDate: [""],
      toDate: [""],
      plant: [""],
      material: [""],
    });
  }
  resetASN() {
    this.ASNForm.reset();
  }
  updateASNDetails() {
    if (this.ASNForm.valid) {
      let reqjson = {
        VariantId: this.ASNForm.get("variant").value,
        Plant: this.ASNForm.get("plant").value,
        Material: this.ASNForm.get("material").value,
      };
      this.myBusinessService.updateODSASNReport(reqjson).subscribe(
        (resp: any) => {},
        (err) => {}
      );
    } else {
      this.validateAllFormFields(this.ASNForm);
    }
  }
  getASNVariantByUser() {
    let reqjson = {
      UserID: "",
    };
    this.myBusinessService.GetODSASNVariantsbyUser(reqjson).subscribe(
      (resp: any) => {},
      (err) => {}
    );
  }
  getASNVariantDetails() {
    let reqjson = {
      VariantId: this.ASNForm.get("variant").value,
    };
    this.myBusinessService.GetODSASNVariantsbyUser(reqjson).subscribe(
      (resp: any) => {},
      (err) => {}
    );
  }
  getASNPageChangeData(event) {
    this.getASNReportData(event);
  }
  getASNReportData(event) {
    if (this.ASNForm.valid) {
      this.pageNo = event.pageIndex;
      let reqjson = {
        Plant: this.ASNForm.get("plant").value,
        Material: this.ASNForm.get("material").value,
        PurchaseOrderNo: "",
        VendorCode: "",
        RoleID: this.RoleID,
        FromDate: this.ASNForm.get("fromDate").value,
        ToDate: this.ASNForm.get("toDate").value,
      };
      this.myBusinessService.getODSASNReport(reqjson).subscribe(
        (resp: any) => {},
        (err) => {}
      );
    } else {
      this.validateAllFormFields(this.ASNForm);
    }
  }
  //---------------------get print part stickers-----------------------------------------------------------
  getPartSticker(event) {
    this.pageNo = event.pageIndex;
    let reqjson = {
      //
      VendorCode: this.masterVendorCode,
      Search: this.stickerSearch,
      PageNo: this.pageNo + 1,
    };
    this.myBusinessService.getPrintPartSticker(reqjson).subscribe(
      (resp: any) => {
        this.printPartDatasource = resp.ResponseData;
        if (this.cs.isUndefinedORNull(this.printPartDatasource)) {
          this.printPartDatasource = [];
        }
        this.pageSize = resp.Count;

        for (let i = 0; i < this.printPartDatasource.length; i++) {
          this.quantity = new FormControl();
        }
      },
      (err) => {}
    );
  }
  //-------------------get variant details by variant id---------------------------------------------------
  initMRQForm() {
    this.MRQForm = this._formBuilder.group({
      month: ["", [Validators.required]],
      year: ["", [Validators.required]],
      variant: ["", [Validators.required]],
      plant: ["", [Validators.required]],
      ltp: ["", [Validators.required]],
    });
  }
  changeVariant() {
    let postData = {
      VariantId: this.MRQForm.get("variant").value,
    };
    this.myBusinessService.getMRQVariantDetails(postData).subscribe(
      (resp: any) => {
        console.log("------------" + resp);

        this.MRQForm = this._formBuilder.group({
          month: [resp.ResponseData[0].Plant, [Validators.required]],
          year: [resp.ResponseData[0].Plant, [Validators.required]],
          variant: [this.MRQForm.get("variant").value, [Validators.required]],
          plant: [resp.ResponseData[0].Plant, [Validators.required]],
          ltp: [resp.ResponseData[0].LTPVersion, [Validators.required]],
        });
      },
      (err) => {}
    );
  }
  updateVariant() {}
  resetForm() {
    this.MRQForm.reset();
  }
  InsertVariantDetails() {
    if (this.MRQForm.valid) {
      let reqjson = {
        UserID: this.username,
        VariantName: this.MRQForm.get("variant").value,
        Plant: this.MRQForm.get("plant").value,
        Calmonth: this.MRQForm.get("month").value,
        LTPVersion: this.MRQForm.get("ltp").value,
      };
      this.myBusinessService.updateVariantDetails(reqjson).subscribe(
        (resp: any) => {
          this.toastr.success("Updated successfully");
        },
        (err) => {
          this.toastr.error("Somwthing went wrong!");
        }
      );
    } else {
      this.validateAllFormFields(this.MRQForm);
    }
  }
  getPageChangeData(event) {
    this.getMRQReport(event);
  }
  getMRQReport(event) {
    this.pageNo = event.pageIndex;
    if (this.MRQForm.valid) {
      let reqjson = {
        Plant: this.MRQForm.get("plant").value,
        ReportingMonth: this.MRQForm.get("month").value,
        ReportingYear: this.MRQForm.get("year").value,
        LTPVersion: this.MRQForm.get("ltp").value,
        VendorCode: "DM181",
        RoleID: this.RoleID,
        PageNo: this.pageNo + 1,
      };
      console.log("-------", reqjson);
      this.myBusinessService.getMRQReport(reqjson).subscribe(
        (resp: any) => {
          for (var i = 0; i < resp.length; i++) {
            this.MRQDataSource = [
              {
                Plant: "A002",
                plantDescription: "AS Kandivili Plant	",
                Component: "0067540V",
                materialDescription: "FRT DOOR @ WELDED (RT) Mungi Bros.",
                Vendor: "DM181",
                vendorDescription: "MUNGI ENGINEERS PVT LTD",
                reportingMonth: "201705",
                LTPVersion: "	V01",
                netRequirementMay: 6775.0,
                netRequirementJun: 5524.0,
                netRequirementJul: 6099.0,
              },
            ];
          }
        },
        (err) => {}
      );
    } else {
      this.validateAllFormFields(this.MRQForm);
    }
  }
  //-------------------------end of MRQ report------------------------------------------------------------
  //---------------------------VQI Report----------------------------------------------------------------
  initVQIForm() {
    this.VQIForm = this._formBuilder.group({
      variant: [""],
      fromDate: [""],
      toDate: [""],
      plant: [""],
      vendor: ["", [Validators.required]],
      PONumber: [""],
      location: [""],
      material: [""],
    });
  }
  getVariantList() {
    let reqjson = {
      UserID: "",
    };
    this.myBusinessService.GetVQIVariantsbyUser(reqjson).subscribe(
      (resp: any) => {
        this.VQIVariantList = resp.ResponseData;
      },
      (err) => {}
    );
  }
  getVQIVariantDetails() {
    let reqjson = {
      VariantId: this.VQIForm.get("plant").value,
    };
    this.myBusinessService.GetVQIVariantDetailsbyVariantId(reqjson).subscribe(
      (resp: any) => {},
      (err) => {}
    );
  }
  resetVQIForm() {
    this.VQIForm.reset();
  }
  updateVQIDetails() {
    if (this.VQIForm.valid) {
      let reqjson = {
        // "VendorCode":"DM181",

        VariantId: "",
        VariantName: "",
        Plant: this.VQIForm.get("plant").value,
        Material: this.VQIForm.get("material").value,
        PONo: this.VQIForm.get("PONumber").value,
        StorageLocation: this.VQIForm.get("location").value,
        FromDate: this.VQIForm.get("fromDate").value,
        ToDate: this.VQIForm.get("toDate").value,
      };
      this.myBusinessService.updateVQIDetails(reqjson).subscribe(
        (resp: any) => {},
        (err) => {}
      );
    } else {
      this.validateAllFormFields(this.VQIForm);
    }
  }
  getVQIReport() {
    if (this.VQIForm.valid) {
      let reqjson = {
        VendorCode: "DM181",
        Plant: this.VQIForm.get("plant").value,
        Material: this.VQIForm.get("material").value,
        PONo: this.VQIForm.get("PONumber").value,
        StorageLocation: this.VQIForm.get("location").value,
        FromDate: this.VQIForm.get("fromDate").value,
        ToDate: this.VQIForm.get("toDate").value,
        RoleID: this.RoleID,
      };
      this.myBusinessService.getVQIReport(reqjson).subscribe(
        (resp: any) => {
          this.VQIDataSource = resp.ResponseData;
        },
        (err) => {}
      );
    } else {
      this.validateAllFormFields(this.VQIForm);
    }
  }
  //----------------------end of VQI report--------------------------------------------------------------
  //-----------------------Change Management------------------------------------------------------------
  // getChangeMAnagementReport(){
  //   let reqjson=
  //      {
  //        "RequestBy":"DM181-K"
  //       }
  //   this.myBusinessService.GetChangeManagementReport(reqjson).subscribe((resp:any) => {
  //         this.CMRDataSource = resp.ResponseData;

  //         }, (err) => {

  //         });

  // }
  viewChangeManagementDataById() {
    let reqjson = {
      Id: 101812,
    };
    this.myBusinessService.ViewChangeManagementDataById(reqjson).subscribe(
      (resp: any) => {},
      (err) => {}
    );
  }
  //----------------------------end of change management-----------------------------------------------
  //---------------------------LR & RA ---------------------------------------------------------------
  initLRnRAForm() {
    this.LRnRAForm = this._formBuilder.group({
      variant: [""],
      fromDate: [""],
      toDate: [""],
      plant: [""],
      material: [""],
    });
  }
  getLRnRAVariantList() {
    let reqjson = {
      UserID: "mahindra\\zakaya-cont",
    };
    this.myBusinessService.GetLRRAVariantsbyUser(reqjson).subscribe(
      (resp: any) => {
        this.LRnRAVariantList = resp.ResponseData;
      },
      (err) => {}
    );
  }
  getLRnRAVariantDetails() {
    let reqjson = {
      VariantId: this.LRnRAForm.get("variant").value,
    };
    this.myBusinessService.getLRRAVariantDetailsbyVariantId(reqjson).subscribe(
      (resp: any) => {},
      (err) => {}
    );
  }
  updateLRnRADetails() {
    let reqjson = {
      VariantId: this.LRnRAForm.get("variant").value,
      VariantName: "NEWTESTPlant",
      Plant: this.LRnRAForm.get("plant").value,
      DocumentDate: this.LRnRAForm.get("fromDate").value,
      DocumentFromDate: this.LRnRAForm.get("fromDate").value,
      DocumentToDate: this.LRnRAForm.get("toDate").value,
    };
    this.myBusinessService.updateLRRAVariantDetails(reqjson).subscribe(
      (resp: any) => {},
      (err) => {}
    );
  }
  getLRnRAReport() {
    let reqjson = {
      VendorCode: "DM181",
      Material: this.LRnRAForm.get("material").value,
      Plant: this.LRnRAForm.get("plant").value,
      FromDate: this.LRnRAForm.get("fromDate").value,
      ToDate: this.LRnRAForm.get("toDate").value,
    };
    this.myBusinessService.GetLRRAReport(reqjson).subscribe(
      (resp: any) => {
        this.LRRADataSource = resp.ResponseData;
      },
      (err) => {}
    );
  }
  //--------------------------end of LR & RA report----------------------------------------------------
  initprintStickerForm() {
    this.printStickerForm = this._formBuilder.group({
      nominees: [""],
    });
  }

  length: number;
  goTo: number;
  @Input() pageIndex: number;

  goToChange() {
    this.paginator.pageIndex = this.goTo - 1;
    this.getPartSticker({
      length: this.paginator.length,
      pageIndex: this.paginator.pageIndex,
      pageSize: this.paginator.pageSize,
    });
  }

  getStickerData(event) {
    this.allChecked = false;
    this.length = event.length;
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    if (this.length == undefined) {
      this.perPageevent = false;
    } else {
      this.perPageevent = true;
    }
    this.getPartSticker(event);
    this.updateGoto();
  }

  updateGoto() {
    this.goTo = (this.pageIndex || 0) + 1;
    this.pageNumbers = [];
    var totalLength = this.length / this.pageSize;
    var ceiledLength = Math.ceil(totalLength);
    console.log(ceiledLength);
    for (let i = 1; i <= ceiledLength; i++) {
      this.pageNumbers.push(i);
    }
  }

  modelFilter: any = "";

  filterDCP(value) {
    this.modelFilter = value;
    this.getDCPPageChangeData({
      pageIndex: 0,
      pageSize: 10,
      length: this.pageSize,
    });
  }
  filterCDMM(value) {
    this.platformFilter = value;
    this.getCDMMPageChangeData({
      pageIndex: 0,
      pageSize: 10,
      length: this.pageSize,
    });
  }
  getCDMMPageChangeData(event) {
    this.getCDMMData(event);
  }
  getDCPPageChangeData(event) {
    this.getDCPUploadedData(event);
  }

  getCOPPageChangeData(event) {
    if (
      this.RoleID == "7" ||
      this.RoleID == "17" ||
      this.RoleID == "9" ||
      this.menuFlag == "COP Plan"
    ) {
      this.getCOPDetailsforAdmin(event, false);
      this.getCOPDetailsforAdmin({ pageIndex: 0 }, true);
    } else this.getCOPDetails(event);
  }

  sideMenuVisible(flag) {
    this.sideMenuFlag = flag;
  }
  validateAllFormFields(formGroup) {
    Object.keys(formGroup.controls).forEach((controlName) => {
      const control = formGroup.get(controlName);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (
        control instanceof FormControl ||
        control instanceof FormArray
      ) {
        this.validateAllFormFields(control);
      }
    });
  }
  getSideMenuForReports(RoleID) {
    return new Promise(resolve=>{
      let postData = {
        RoleID: RoleID,
        ParentId: this._dataService.ParentId,
      };
      this.cs
        .postMySuggestionData("MenuAdmin/GetMenuByRole", postData)
        .subscribe((res: any) => {
          this.SideMenuForReports = res.ResponseData;
          // this.SideMenuForReports.push({
          //   ParentName: "Payments",
          //   NavigationName: "Retro Debit Note",
          //   IsSRMMenu: false,
          //   NavigationURL: ""
          // })
          console.log("Sidemenu", this.SideMenuForReports);
          resolve(this.SideMenuForReports)
        });
    })
  }

  bulkUpload() {
    if (this.docDate == undefined || this.fileUrls.length <= 0) {
      if (this.docDate == undefined) {
        this.toastr.error("Please select date");
        return;
      } else if (this.fileUrls.length <= 0) {
        this.toastr.error("Please select File");
        return;
      }
    }
    let date = moment(this.docDate).format("YYYY-MM-DD");
    let formData: FormData = new FormData();
    formData.append("Timestamp", date);
    formData.append("VendorCode", this.vendorDetails.VendorCode);
    for (var i = 0; i < this.fileUrls.length; i++) {
      formData.append("ExcelFile", this.fileUrls[i]);
    }
    this.myBusinessService.bulkUploadBS4(formData).subscribe(
      (resp: any) => {
        if (resp.Message == "Success") {
          this.fileUrls = [];
          this.fileName = "";
          this.docDate = null;
          this.toastr.success(resp.Message);
        }
        this.getBS4Inventory({
          pageIndex: 0,
          pageSize: 10,
          length: this.pageSize,
        });
      },
      (err) => {}
    );
  }

  getBS4Inputs(index, formvalue, data1) {
    this.bsInventoryUpload[index][data1] = formvalue;
    console.log("BS", this.bsInventoryUpload);
    console.log("index--", this.bsInventoryUpload[index]);
    this.addToList(index, this.bsInventoryUpload[index]);
  }

  addToList(index, data) {
    if (this.bs4List.length > 0) {
      let json;
      console.log(this.bs4List);
      for (let i = 0; i < this.bs4List.length; i++) {
        //  alert('inside for');
        if (this.bs4List[i].Partno == data["Partno"]) {
          this.bs4List.splice(i, 1);
          json = {
            Partno: data["Partno"],
            Partdescription: data.Partdescription,
            Plant_Finish_Stock: this.bsInventoryUpload[index][
              "partFinishStock"
            ],
            FS_Unit: data.FS_Unit,
            WIPorRM_Stock: data.WIPorRM_Stock,
            WIP_Unit: this.bsInventoryUpload[index]["WIPStock"],
            Tier_Stock: this.bsInventoryUpload[index]["tierStock"],
            Tier_Unit: data.Tier_Unit,
            Remark: this.bsInventoryUpload[index]["Remarks"],
          };
        } else {
          json = {
            Partno: data["Partno"],
            Partdescription: data.Partdescription,
            Plant_Finish_Stock: this.bsInventoryUpload[index][
              "partFinishStock"
            ],
            FS_Unit: data.FS_Unit,
            WIPorRM_Stock: data.WIPorRM_Stock,
            WIP_Unit: this.bsInventoryUpload[index]["WIPStock"],
            Tier_Stock: this.bsInventoryUpload[index]["tierStock"],
            Tier_Unit: data.Tier_Unit,
            Remark: this.bsInventoryUpload[index]["Remarks"],
            // "flag":"save"
          };
          // this.bs4List.push(json);
          console.log("--", this.bs4List);
        }
      }
      //  alert('pushing-->')
      this.bs4List.push(json);
    } else if (this.bs4List.length <= 0) {
      // if(this.bsInventoryUpload[i].Partno == part){
      //  if(this.bsInventoryUpload[index]['partFinishStock'] == undefined){
      //   this.bsInventoryUpload[index]['partFinishStock'] ='';
      //  }
      //   if(this.bsInventoryUpload[index]['WIPStock'] == undefined){
      //   this.bsInventoryUpload[index]['WIPStock'] = '';
      //  }
      //   if(this.bsInventoryUpload[index]['tierStock'] == undefined){
      //   this.bsInventoryUpload[index]['tierStock'] ='';
      // }
      //  if(this.bsInventoryUpload[index]['remarks'] == undefined){
      //   this.bsInventoryUpload[index]['remarks']=''
      // }
      let json = {
        // "Vcode":this.vendorDetails.VendorCode,
        Partno: data["Partno"],
        Partdescription: data.Partdescription,
        Plant_Finish_Stock: this.bsInventoryUpload[index]["partFinishStock"],
        FS_Unit: data.FS_Unit,
        WIPorRM_Stock: data.WIPorRM_Stock,
        WIP_Unit: this.bsInventoryUpload[index]["WIPStock"],
        Tier_Stock: this.bsInventoryUpload[index]["tierStock"],
        Tier_Unit: data.Tier_Unit,
        Remark: this.bsInventoryUpload[index]["Remarks"],
        // "flag":"save"
      };
      this.bs4List.push(json);
      console.log("--", this.bs4List);
    }
  }

  getquantity(check, quantity, index, MPQ, partNo, element, isGetquantity) {
    let qty;
    if (isGetquantity) {
      this.printPartDatasource[index]["quantity"] = quantity.value;
      qty = this.printPartDatasource[index]["quantity"];
    } else {
      this.printPartDatasource[index]["labels"] = 0;
      qty = this.printPartDatasource[index]["quantity"];
    }

    this.printPartDatasource[index]["labels"] = Math.floor(qty / MPQ);
    for (let i = this.barCodeNumberList.length - 1; i >= 0; --i) {
      if (this.barCodeNumberList[i].PartNumber == partNo) {
        this.barCodeNumberList.splice(i, 1);
      }
    }
    for (var j = 0; j < element.labels; j++) {
      let data = {
        PartNumber: partNo,
        MPQ: MPQ,
        Desc: element.PartDescription,
      };
      if (check) {
        this.barCodeNumberList.push(data);
      }
    }
    this.printPartDatasource = [...this.printPartDatasource];
  }
  selectData(check, quantity, index, MPQ, partNo, element, isGetquantity) {
    this.printPartDatasource[index]["checked"] = this.cs.isUndefinedORNull(
      check.checked
    )
      ? check
      : check.checked;
    if (
      !this.cs.isUndefinedORNull(this.printPartDatasource[index]["checked"]) &&
      this.printPartDatasource[index]["checked"]
    ) {
      this.getquantity(
        true,
        quantity,
        index,
        MPQ,
        partNo,
        element,
        isGetquantity
      );
    } else {
      this.getquantity(
        false,
        quantity,
        index,
        MPQ,
        partNo,
        element,
        isGetquantity
      );
    }
  }

  selectAll(e) {
    if (e.checked) {
      this.printPartDatasource.filter((element, i) => {
        element["checked"] = true;
        this.selectData(
          element["checked"],
          element["quantity"],
          i,
          element["MPQ"],
          element["PartNumber"],
          element,
          false
        );
      });
    } else {
      this.printPartDatasource.filter((element, i) => {
        element["checked"] = false;
        this.selectData(
          element["checked"],
          element["quantity"],
          i,
          element["MPQ"],
          element["PartNumber"],
          element,
          false
        );
      });
    }
  }

  openDCPDOC(data) {
    window.open(data, "_blank");
  }

  // @ViewChild('Report')Report;

  getReportNameList() {
    this.myBusinessService.getAllReportNameList().subscribe(
      (resp: any) => {
        this.ReportNameList = resp.ResponseData;
      },
      (err) => {}
    );
  }
  @ViewChild("picker") picker: any;
  saveDetails(flag) {
    let latest_date = this.datepipe.transform(this.today, "yyyy-MM-dd");
    let reqjson = {
      flag: flag,
      Timestamp: latest_date,
      Vcode: this.vendorDetails.VendorCode,
      BS4List: this.bs4List,
    };
    this.myBusinessService.saveAndUpdateBS4(reqjson).subscribe(
      (resp: any) => {
        if (flag == "save") {
          this.toastr.success("Saved Successfully");
        } else if (flag == "Update") {
          this.toastr.success("Updated Successfully");
        }
        this.getBS4Inventory({
          pageIndex: 0,
          pageSize: 10,
          length: this.pageSize,
        });
        this.bs4List = [];
      },
      (err) => {}
    );
  }

  @ViewChild("Report") Report;
  showReport: boolean = true;
  selectedIndex: number = 0;
  reportSubmenuClick(val, menu, isSRM, URL, i) {
    this.selectedIndex = i;
    this.selectedIndex1 = i;

    console.log(val, menu, isSRM, URL, i)
    if (isSRM) {
      let postData = {
        Url: URL,
        UserName: this.username,
        IsSRMMenu: true,
      };
      this.cs
        .postMySuggestionData("SRMSSO/GetSRMURL", postData)
        .subscribe((res) => {
          let data = res.ResponseData;
          window.open(data.NewURL, "_blank");
        });

      let analysticsRequest = {
        userClicked: this.username,
        device: "windows",
        browser: this.browser,
        moduleType: "SR",
        module: menu,
      };
      this.cs
        .postMySuggestionData("Analytics/InsertAnalytics", analysticsRequest)
        .subscribe((res: any) => {});
    } else {

    console.log(this.RoleID.split(',').includes('9'))
    if(this.RoleID.split(',').includes('9')) {
          if(menu == "MRQ"){
            window.open(
              "https://ep.mahindra.com/irj/servlet/prt/portal/prtroot/pcd!3aportal_content!2fcom.mahindra.ha_fld_harmony!2fcom.mahindra.analytics.ha_fld_analytics!2fcom.mahindra.analytics.ha_fld_iview!2fcom.mahindra.ha_fld_For_AFS_KND_SharePoint_Portal!2fcom.mahindra.ha_ivu_Material_wise_Requirement_Qty",
              "_blank"
            );
            return;
          }

          if(menu == "ODS"){
            window.open(
              "https://ep.mahindra.com/irj/servlet/prt/portal/prtroot/pcd!3aportal_content!2fcom.mahindra.ha_fld_harmony!2fcom.mahindra.analytics.ha_fld_analytics!2fcom.mahindra.analytics.ha_fld_iview!2fcom.mahindra.ha_fld_For_AFS_KND_SharePoint_Portal!2fcom.mahindra.ha_ivu_Open_Delivery_Schedule",
              "_blank"
            );
            return;
          }

          if(menu == "ODS With ASN"){
            window.open(
              "https://ep.mahindra.com/irj/servlet/prt/portal/prtroot/pcd!3aportal_content!2fcom.mahindra.ha_fld_harmony!2fcom.mahindra.analytics.ha_fld_analytics!2fcom.mahindra.analytics.ha_fld_iview!2fcom.mahindra.ha_fld_For_AFS_KND_SharePoint_Portal!2fcom.mahindra.ha_ivu_Open_Delivery_Schedule_ASN_NEW?sap-config-mode=true?97",
              "_blank"
            );
            return;
          }

          if(menu == "LTP (buyers’) report with Open ASN"){
            window.open(
              "https://ep.mahindra.com/irj/portal?NavigationTarget=navurl://a710945c82b72dd13c3dd20322258f25&NavMode=10",
              "_blank"
            );
            return;
          }
      }
        
      this._dataService.setOptionForReport(val, menu);
      this.showReport = false;
      setTimeout(() => {
        this.showReport = true;
      }, 100);
    }
  }
  //selectedIndex: number = 0;

  selectedIndex1: number = 0;
  subMenuListClicked(item, IsSRM, URL, i) {
    console.log(item, IsSRM, URL, i);
    this.selectedIndex1 = i;

    if (item == "QDM") {
      window.open(
        "https://mnmrvqdmapp.mahindra.com/DCS_Web/Services/Home/SignIn.aspx",
        "_blank"
      );
      return;
    }

    if (IsSRM) {
      let postData = {
        Url: URL,
        UserName: this.username,
        IsSRMMenu: true,
      };
      this.cs
        .postMySuggestionData("SRMSSO/GetSRMURL", postData)
        .subscribe((res) => {
          let data = res.ResponseData;
          window.open(data.NewURL, "_blank");
        });
    } else {
      this.menuFlag = item;
      if (this.menuFlag == "Upload COP"
      //  || this.menuFlag == "COP Plan"
       ) {
        this.getCOPPageChangeData({
          pageIndex: 0,
          pageSize: 10,
          length: this.pageSize,
        });
      } else if (
        this.menuFlag == "Upload DCP Document" ||
        this.menuFlag == "DCP Admin"
      ) {
        this.getDCPPageChangeData({
          pageIndex: 0,
          pageSize: 10,
          length: this.pageSize,
        });
      } else if (this.menuFlag == "BS4 Inventory - View") {
        this.getBS4Inventory({
          pageIndex: 0,
          pageSize: 10,
          length: this.pageSize,
        });
      } else if (this.menuFlag == "Tier II list") {
        this.getqualityTierData("FY 2020", null, { pageIndex: 0 });
      } else if (this.menuFlag == "Cop Dashboard") {
        this.getCOPList({ pageIndex: 0 }, false);
      }
    }
  }

  //--------------MRQ related API-------------------------------------------------------------------------
  getMRQYearList() {
    this.selectedYear = new Date().getFullYear();
    // alert("--"+this.currentYear);
    this.myBusinessService.getMRQYearDropDown().subscribe(
      (resp: any) => {
        this.MRQYearList = resp.ResponseData;
      },
      (err) => {}
    );
  }

  getMRQVariant() {
    let postData = {
      UserID: "mahindraext\\dm056-k",
    };
    this.myBusinessService.getMRQVariantList(postData).subscribe(
      (resp: any) => {
        this.MRQVariantList = resp.ResponseData;
      },
      (err) => {}
    );
  }
  changeYear(year) {
    this.selectedYear = year;
    //   this.getPageChangeData();
  }
  changeMonth(month) {
    //    alert(month);
    this.selectedMonth = month;
  }
  //-------------------------------------------Bar code-------------------------------------------------------------
  elementType = "svg";
  // values:any=['someValue12340987','tanvi'];
  format = "CODE39";
  lineColor = "#000000";
  width = 0.9;
  height = 25;
  displayValue = false;
  fontOptions = "";
  font = "monospace";
  textAlign = "center";
  textPosition = "bottom";
  textMargin = 2;
  fontSize = 12;
  background = "#ffffff";
  margin = 0;
  marginTop = 0;
  marginBottom = 0;
  marginLeft = 0;
  marginRight = 0;

  codeList: string[] = [
    "",
    "CODE128",
    "CODE128A",
    "CODE128B",
    "CODE128C",
    "UPC",
    "EAN8",
    "EAN5",
    "EAN2",
    "CODE39",
    "ITF14",
    "MSI",
    "MSI10",
    "MSI11",
    "MSI1010",
    "MSI1110",
    "pharmacode",
    "codabar",
  ];

  printComponent(): void {
    if (this.barCodeNumberList.length == 0) {
      this.cs.showError(
        "Kindly enter quantity/select atleast 1 print part number to print"
      );
      return;
    }
    console.log(this.barCodeNumberList)
    this.printPartStickers = ''

    this.postPrintPartSticket().then((res)=>{
        if(res == true){
          console.log(this.stickers)
          var WinPrint = window.open('', '', 'letf=0,top=0,width=400,height=400,toolbar=0,scrollbars=0,status=0');
          WinPrint.document.write(
              `
              <style type="text/css">
                img {
                  width: 105%;
                }
              </style>
              `
            );
          WinPrint.document.write(this.printPartStickers);
          WinPrint.document.close();
          WinPrint.focus();
          WinPrint.print();
        }
    })
  }

  postPrintPartSticket(){
    return new Promise((resolve,reject)=>{
      let body: any = [];
      this.printPartDatasource.forEach((barCodeNumber)=>{
        if(barCodeNumber.labels > 0){
          body.push({
            "PartNumber":barCodeNumber.PartNumber,
            "PartDescription": barCodeNumber.PartDescription,
            "MPQ": barCodeNumber.MPQ,
            "Quantity": barCodeNumber.labels
          })
        }
      })
      this.cs.postMySuggestionData('PrintSticker/GetPrintPartSticket',body).subscribe((res: any)=>{
        if(res.ID == 1){
          this.printPartStickers = res.ResponseData
          console.log(this.stickers)
          resolve(true)
        }else{
          resolve(false)
        }
      })
    })
  }
  // printComponent(): void {
  //   if (this.barCodeNumberList.length == 0) {
  //     this.cs.showError(
  //       "Kindly enter quantity/select atleast 1 print part number to print"
  //     );
  //     return;
  //   }
  //   let printContents, popupWin;
  //   printContents = document.getElementById("component1").innerHTML;
  //   //   var params = [
  //   //     'height='+screen.height,
  //   //     'width='+screen.width,
  //   //     'fullscreen=yes' // only works in IE, but here for completeness
  //   // ].join(',');
  //   //   // popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
  //   //   popupWin = window.open('', 'popup_window', params);
  //   //   popupWin.moveTo(0,0);
  //   //   popupWin.document.open();
  //   //   popupWin.document.write(`
  //   //     <html>
  //   //       <head>
  //   //         <title>Print tab</title>
  //   //         <style>
  //   //         //........Customized style.......
  //   //         </style>
  //   //       </head>
  //   //   <body onload="window.print();window.close()">${printContents}</body>
  //   //     </html>`
  //   //   );
  //   //   popupWin.document.close();
  //   let printWindow;
  //   printWindow = window.open(
  //     "",
  //     "",
  //     "location=1,status=1,scrollbars=1,width=650,height=600"
  //   );
  //   printWindow.document.write("<html><head>");
  //   printWindow.document.write(
  //     '<style type="text/css">@media print{.no-print, .no-print *{display: none !important;}</style>'
  //   );
  //   printWindow.document.write("</head><body>");
  //   printWindow.document.write('<div style="width:100%;text-align:right">');

  //   //Print and cancel button
  //   printWindow.document.write(
  //     '<input type="button" id="btnPrint" value="Print" class="no-print" style="width:100px" onclick="window.print()" />'
  //   );
  //   printWindow.document.write(
  //     '<input type="button" id="btnCancel" value="Cancel" class="no-print"  style="width:100px" onclick="window.close()" />'
  //   );

  //   printWindow.document.write("</div>");

  //   //You can include any data this way.
  //   printWindow.document.write(
  //     "<table><tr><td>" + printContents + "</td></tr></table>"
  //   );

  //   //  printWindow.document.write(document.getElementById('forPrintPreview').innerHTML);
  //   //here 'forPrintPreview' is the id of the 'div' in current page(aspx).
  //   printWindow.document.write("</body></html>");
  //   printWindow.focus();
  //   printWindow.document.close();
  //   //  printWindow.focus();
  // }
  //-------------------get token------------------------------------
  getVendorDetailsByUserName() {
    let postData = {
      UserName: this.username,
    };

    this.cs
      .postMySuggestionData("VendorEmployee/VendorDetailsByUserName", postData)
      .subscribe(
        (res: any) => {
          if (!this.cs.isUndefinedORNull(res)) {
            this.vendorDetails = res.ResponseData;
            this.getSupplierCodeList();
            this.getVendorList();
            if (this.pageType.Page == "OS") {
              this.getBS4Inventory({
                pageIndex: 0,
                pageSize: 10,
                length: this.pageSize,
              });
              // this.downloadExcel();
            }
          } else {
            alert("Something went wrong!");
          }
        },
        (error) => {
          this.cs.showSwapLoader = false;
          console.log(error.message);
          //  this.form.resetForm();
        }
      );
  }
  //---------------end of token---------------------------------------
  //----------------end of bar code generation---------------------------------------------------------
  //-------------------Download Excel format-BS4 inventory-----------------------------------------------------
  downloadUrl() {
    var myWindow = window.open(
      "https://supplier.mahindra.com:8443/MSetuDocuments/BS4_Template/Bs4supplierdetails.xlsx",
      "_blank"
    );
    //  this.utilityProvider.openUrl(this.commonAPIService.baseUrl+ '/RSUGrantBulk/RSU_Bulk_Template.xlsx', '_self', 'yes')
  }
  uploadBS4Document() {
    let reqjson = {
      //  tanvi
    };
    this.myBusinessService.uploadOEDocument(reqjson).subscribe(
      (resp: any) => {},
      (err) => {}
    );
  }
  getBS4Inventory(event) {
    this.callBS4Data(event);
  }
  callBS4Data(event) {
    let latest_date = this.datepipe.transform(this.today, "yyyy-MM-dd");
    this.pageNo = event.pageIndex;
    let reqjson = {
      VendorCode: this.vendorDetails.VendorCode,
      PageNo: this.pageNo + 1,
      Timestamp: latest_date,
    };
    this.myBusinessService.getBS4Data(reqjson).subscribe(
      (resp: any) => {
        this.bsInventoryUpload = resp.ResponseData;
        this.pageSize = resp.Count;

        if (resp.Message == "1") {
          this.bs4EditFlag = true;
        } else {
          this.bs4EditFlag = false;
        }
        console.log("=====", this.bsInventoryUpload);

        if (!this.cs.isUndefinedORNull(this.bsInventoryUpload)) {
          for (let i = 0; i < this.bsInventoryUpload.length; i++) {
            this.bsInventoryUpload[i][
              "partFinishStock"
            ] = this.bsInventoryUpload[
              i
            ].Plant_Finish_Stock_excluding_intransit_wh_qty;
            this.bsInventoryUpload[i]["WIPStock"] = this.bsInventoryUpload[
              i
            ].WIPorRM_Stock;
            this.bsInventoryUpload[i]["tierStock"] = this.bsInventoryUpload[
              i
            ].Tier_Stock;
            this.bsInventoryUpload[i]["remarks"] = this.bsInventoryUpload[
              i
            ].Remark;
            //   this.partFinishStock[i] = new FormControl();
            //   this.WIPStock[i]        = new FormControl();
            //   this.tierStock[i]       = new FormControl();
            //   this.remarks[i]         = new FormControl();
            //   alert(this.bsInventoryUpload[i]['partFinishStock']);
          }
        }
      },
      (err) => {}
    );
  }
  downloadExcel() {
    let latest_date = this.datepipe.transform(this.today, "yyyy-MM-dd");
    let reqjson = {
      // this.vendorDetails.VendorCode
      VendorCode: this.vendorDetails.VendorCode,
      PageNo: 0,
      Timestamp: latest_date,
    };
    this.myBusinessService.getBS4Data(reqjson).subscribe(
      (resp: any) => {
        this.BS4ExcelData = resp.ResponseData;
        // this.downloadBS4Excel();
        this.cs.exportJsonAsExcelFile(this.BS4ExcelData, "BS$InventoryData");
      },
      (err) => {}
    );
  }
  downloadBS4Excel() {
    // alert("===");tanuuuu

    // alert(this.BS4ExcelData.length);
    let ReportName = "BS4Inventory" + moment().format("DD_MM_YYYY_h_mm_ss_a");
    const tbl = document.getElementById("BS4ExcelDataID");
    const wb = XLSX.utils.table_to_book(tbl);

    /* save to file */
    XLSX.writeFile(wb, `${ReportName}.xlsx`);
    this.rprtName = ReportName;
  }
  //----------------------end of BS4 ----------------------------------------------------------------------
  //-----------------------------start of CDMM---------------------------------------------------------
  getCDMMData(event) {
    this.pageNo = event.pageIndex;
    let reqjson = {
      VendorCode: this.vendorDetails.VendorCode,
      PageNo: this.pageNo + 1,
      Search: "",
      Platform: this.platformFilter,
    };
    this.myBusinessService.getDataCDMM(reqjson).subscribe(
      (resp: any) => {
        this.CDMMDataSource = resp.ResponseData;
        this.pageSize = resp.Count;
      },
      (err) => {}
    );
  }
  getVendorCode() {
    this.myBusinessService.getVendorCode().subscribe(
      (resp: any) => {
        if (
          this.RoleID.split(",").includes("1") ||
          this.RoleID.split(",").includes("3")
        ) {
          this.myBusinessService
            .getVendorList({
              Vendor: this.vendorDetails.VendorCode,
            })
            .subscribe((subVendorCodesResponse) => {
              let vendors = [];
              subVendorCodesResponse.ResponseData.forEach((element) => {
                vendors.push({ MasterVendor: element.SubVendor });
              });
              this.vendorCodeDetailList = [...vendors];
            });
        } else {
          this.vendorCodeDetailList = resp.vendorCode;
        }
        this.folderList = resp.getFolder;
        if (this.pageType.Page == "NPD") {
          if (this.RoleID == "7" || this.cdmmAdmin) {
            this.getCDMMPageAdminChangeData({
              pageIndex: 0,
              pageSize: 10,
              length: this.pageSize,
            });
          } else {
            this.CDMMForm.controls["vendorCode"].setValue(
              this.vendorDetails.VendorCode
            );
            this.getCDMMPageChangeData({
              pageIndex: 0,
              pageSize: 10,
              length: this.pageSize,
            });
          }
        }
      },
      (err) => {}
    );
  }

  getSubFoldersList(val) {
    let reqjson;
    if (val == "admin") {
      reqjson = {
        Folder: this.CDMMFormAdmin.get("folder").value,
      };
    } else {
      reqjson = {
        Folder: this.CDMMForm.get("folder").value,
      };
    }
    this.myBusinessService.getSubFolders(reqjson).subscribe(
      (resp: any) => {
        this.subFolderList = resp.getSubFolders;
        console.log("----------->getSubFolders---->>", resp);
      },
      (err) => {}
    );
  }

  initNominees() {
    this.nomineeForm = this._formBuilder.group({
      nominees: this._formBuilder.array([]),
    });
  }

  initCDMMForm() {
    // this.vendorDetails.VendorCode ='DA001';
    this.CDMMForm = this._formBuilder.group({
      vendorCode: [
        { value: this.vendorDetails.VendorCode, disabled: false },
        [Validators.required],
      ],
      platform: ["", [Validators.required]],
      folder: ["", [Validators.required]],
      subFolder: ["", [Validators.required]],
      document: ["", [Validators.required]],
      date: ["", [Validators.required]],
      desc: ["", [Validators.required]],
    });
  }
  resetCDMMForm() {
    this.CDMMForm.reset();
    this.CDMMForm.controls["vendorCode"].setValue(
      this.vendorDetails.VendorCode
    );
  }

  uploadCDMM() {
    if (this.CDMMForm.valid) {
      let date = moment(this.CDMMForm.get("date").value).format("YYYY-MM-DD");
      let formData: FormData = new FormData();
      let reqjson = {
        VendorCode: this.CDMMForm.get("vendorCode").value,
        Platform: this.CDMMForm.get("platform").value,
        Folder: this.CDMMForm.get("folder").value,
        SubFolder: this.CDMMForm.get("subFolder").value,
        DocUploadDate: date,
        DocDesc: this.CDMMForm.get("desc").value,
      };
      formData.append("reqjson", JSON.stringify(reqjson));
      for (var i = 0; i < this.fileUrls.length; i++) {
        formData.append("file", this.fileUrls[i]);
      }
      this.myBusinessService.submitCDMM(formData).subscribe(
        (resp: any) => {
          this.toastr.success(resp.Message);
          this.CDMMForm.reset();
          this.CDMMForm.controls["vendorCode"].setValue(
            this.vendorDetails.VendorCode
          );
          this.getCDMMPageChangeData({
            pageIndex: 0,
            pageSize: 10,
            length: this.pageSize,
          });
          console.log("----------->submitCDMM---->>", resp);
        },
        (err) => {}
      );
    } else {
      this.validateAllFormFields(this.CDMMForm);
    }
  }
  //----------------------end of CDMM---------------------------------------------------------------------
  // selectAll(e) {
  //  // this.selectAction ='';
  //   console.log("--",e);
  //  // this.allChecked = !this.allChecked;
  //   if(e.checked){
  //     this.printPartDatasource.filter(element => {
  //       element["checked"] = true;
  //       // console.log(this.printPartDatasource.length);

  //     });
  //     for(let i=0;i<this.printPartDatasource.length;i++){
  //       if(this.printPartDatasource[i].labels+"--"+this.printPartDatasource[i].PartNumber){
  //      //   alert(this.printPartDatasource[i].labels)
  //         for(let j=0;j<this.printPartDatasource.length;j++){
  //             let data = {
  //               PartNumber: this.printPartDatasource[i].PartNumber
  //             };
  //             this.checkedData.push(data);
  //             //  alert("--"+data.PartNumber);
  //               this.barCodeNumberList.push(data);
  //             }
  //           }
  //    }
  //   }else{
  //     this.printPartDatasource.filter(element => {
  //       element["checked"] = false;
  //     });
  //   }

  // }

  rprtName: any;
  detectFiles(event) {
    this.fileUrls = [];
    this.fileName = "";
    console.log(event);
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        console.log(file);

        //   this.CDMMForm.controls['document'].setValue(fileName);

        // alert(this.fileName);
        let ext = file.name.split(".").pop().toLowerCase();
        const target: DataTransfer = <DataTransfer>event.target;
        const reader: FileReader = new FileReader();
        // alert(ext);

        if (this.pageType.Page == "OS") {
          var aaa = this.rprtName + ".xlsx";
          // if(this.fileName == aaa){
          if (ext == "xlsx") {
            reader.onload = (e: any) => {
              const bstr: string = e.target.result;
              const wb: XLSX.WorkBook = XLSX.read(bstr, { type: "binary" });

              /* grab first sheet */
              const wsname: string = wb.SheetNames[0];
              const ws: XLSX.WorkSheet = wb.Sheets[wsname];

              /* save data */
              this.data = <AOA>XLSX.utils.sheet_to_json(ws, { header: 1 });
              console.log(this.data);
              let dataTemp = [];
              dataTemp = this.data[0];
              // this.readAsJson();
              if (
                dataTemp[0] == "Vcode" &&
                dataTemp[1] == "Partno" &&
                dataTemp[2] == "Partdescription" &&
                dataTemp[3] ==
                  "Plant_Finish_Stock_excluding_intransit_wh_qty" &&
                dataTemp[4] == "FS_Unit" &&
                dataTemp[5] == "WIPorRM_Stock" &&
                dataTemp[6] == "WIP_Unit" &&
                dataTemp[7] == "Tier_Stock" &&
                dataTemp[8] == "Tier_Unit" &&
                dataTemp[9] == "Remarks"
              ) {
                // do nothing
                this.excelFlag = false;
                console.log("correct file" + dataTemp[0]);
                this.fileName = file.name;
                // reader.readAsBinaryString(target.files[0]);
                this.fileUrls.push(file);
                this.urls.push({ url: e.target.result, ext: ext });
                // reader.onload = (e: any) => {
                //   this.fileUrls.push(file);
                //   this.urls.push({url:e.target.result,ext:ext});
                // }
              } else {
                console.log("wrong");
                this.fileUrls = [];
                this.excelFlag = true;
                this.toastr.error("Please select proper format of file");
              }
            };
            reader.readAsBinaryString(target.files[0]);
          } else {
            this.fileUrls = [];
            this.excelFlag = true;
            // alert("");
            this.toastr.error("please select downloaded excel");
          }
          // reader.readAsBinaryString(target.files[0]);
        } else if (this.pageType.Page == "Q") {
          this.fileName = file.name;
          reader.onload = (e: any) => {
            this.fileUrls.push(file);
            this.urls.push({ url: e.target.result, ext: ext });
            if (this.menuFlag == "Upload COP") {
              this.COPUploadForm.controls["document"].setValue(this.fileName);
            } else if (this.menuFlag == "Change Management") {
              this.ChangeManagementForm.controls["document"].setValue(
                this.fileName
              );
            } else if (this.menuFlag == "Upload DCP Document") {
              this.DCPUploadForm.controls["document"].setValue(this.fileName);
            }
          };
        } else if (this.pageType.Page == "NPD") {
          this.fileName = file.name;
          reader.onload = (e: any) => {
            this.fileUrls.push(file);
            this.urls.push({ url: e.target.result, ext: ext });
          };
          this.CDMMForm.controls["document"].setValue(this.fileName);
        } else {
          reader.onload = (e: any) => {
            if (
              ext == "jpg" ||
              ext == "jpeg" ||
              ext == "png" ||
              ext == "pdf" ||
              ext == "xlsx"
            ) {
              this.fileUrls.push(file);
              this.urls.push({ url: e.target.result, ext: ext });
            } else {
              alert("please select proper format");
              //  this.utilityProvider.presentToast('Please select file in proper format','4000','top')
            }
          };
        }

        reader.readAsDataURL(file);
      }
      event.target.value = ''
    }
  }
  //----------------------------------- Quality -->Change Management--------------------------------------------------------
  initChangeManagementForm() {
    // this.vendorDetails.VendorCode ='DA001';
    this.ChangeManagementForm = this._formBuilder.group({
      supplierCode: [
        { value: this.vendorDetails.VendorCode, disabled: false },
        [Validators.required],
      ],
      supplierName: ["", Validators.required],
      supplierLocation: ["", Validators.required],
      changeMonth: ["No", [Validators.required]],
      name: ["", [Validators.required]],
      mobileNo: [
        "",
        [Validators.required, Validators.pattern(this.mobnumPattern)],
      ],
      emailID: [
        "",
        [Validators.required, Validators.pattern(this.emailPattern)],
      ],
      document: [""],
      changesRelatedTo: [""],
      detailsOfChange: [""],
      changesAffectedTo: [""],
      requiredPPAP: ["No"],
      donePPAP: ["No"],
      nameAndDept: [""],
      performanceField: ["No"],
      affectingPlant: [""],
      date: [""],
    });
    this.getSupplierDetails();
  }
  resetChangemgmtForm() {
    this.ChangeManagementForm.reset();
    this.ChangeManagementForm.controls["supplierCode"].setValue(
      this.vendorDetails.VendorCode
    );
    this.getSupplierDetails();
  }
  getSupplierCodeList() {
    this.myBusinessService
      .getsupplierCodeList({ SupplierCode: this.vendorDetails.VendorCode })
      .subscribe(
        (resp: any) => {
          if (resp.ResponseData.length > 0) {
            this.supplierCodeList = resp.ResponseData;
            this.ChangeManagementForm.get("supplierCode").setValue(
              resp.ResponseData[0].SUPPLIERNAME
            );
          }
          this.ChangeManagementForm.controls["supplierCode"].setValue(
            this.vendorDetails.VendorCode
          );
        },
        (err) => {}
      );
  }
  getSupplierDetails() {

    let supplier = this.supplierCodeList.find(
      (o) => o.VENDORCODE == this.ChangeManagementForm.get("supplierCode").value
    );
    if(supplier){
      this.ChangeManagementForm.controls["supplierName"].setValue(
        supplier.SUPPLIERNAME
      );
    }
    // let reqjson = {
    //   VendorCode: this.ChangeManagementForm.get("supplierCode").value,
    // };
    // this.myBusinessService.getSupplierDetails(reqjson).subscribe(
    //   (resp: any) => {
    //     //  this.supplierName = resp.VendorName;
    //     //  this.supplierLocation = resp;
    //     this.ChangeManagementForm.controls["supplierCode"].setValue(
    //       this.vendorDetails.VendorCode
    //     );
    //   },
    //   (err) => {}
    // );
  }
  submitChangeManagement() {
    if (this.ChangeManagementForm.valid) {
      // if(this.checkedArray.length <= 0){
      this.checkedArray.push({ PlantName: "ALL" });
      // }
      let formData: FormData = new FormData();
      let reqjson = {
        SupplierCode: this.ChangeManagementForm.get("supplierCode").value,
        SupplierName: this.ChangeManagementForm.get("supplierName").value,
        Location: this.ChangeManagementForm.get("supplierLocation").value,
        Changes_this_Month: this.ChangeManagementForm.get("changeMonth").value,
        ChangesRelatedTo: this.ChangeManagementForm.get("changesRelatedTo")
          .value,
        ChangeDetails: this.ChangeManagementForm.get("detailsOfChange").value || '',
        ChangeAffected_to: this.ChangeManagementForm.get("changesAffectedTo")
          .value || '',
        PPAP_Require: this.ChangeManagementForm.get("requiredPPAP").value,
        PPAP_Done: this.ChangeManagementForm.get("donePPAP").value,
        Communication: this.ChangeManagementForm.get("nameAndDept").value,
        Change_Affect_performance: this.ChangeManagementForm.get(
          "performanceField"
        ).value,
        PlantList: this.checkedArray,
        Implementation_Date: this.ChangeManagementForm.get("date").value,
        Name: this.ChangeManagementForm.get("name").value,
        MobileNo: this.ChangeManagementForm.get("mobileNo").value,
        EmailId: this.ChangeManagementForm.get("emailID").value,
        RequestBy: this.username,
        // file : "DBMestuDoc"
      };
      console.log("--change mgmt-", reqjson);

      formData.append("reqJson", JSON.stringify(reqjson));
      for (var i = 0; i < this.fileUrls.length; i++) {
        formData.append("file", this.fileUrls[i]);
      }
      console.log(formData);
      this.myBusinessService.submitChangeManagement(formData).subscribe(
        (resp: any) => {
          //  alert("data added successfully"+resp.Message);
          this.toastr.success(resp.Message);
          this.checkedArray.length = 0;
          this.ChangeManagementForm.reset();
          this.ChangeManagementForm.controls["supplierCode"].setValue(
            this.vendorDetails.VendorCode
          );
          this.getSupplierDetails();
        },
        (err) => {}
      );
    } else {
      this.validateAllFormFields(this.ChangeManagementForm);
    }
  }

  getChangesRelatedTo() {
    let reqjson = {
      // "SubVendor": this.COPUploadForm.get('subVendorCode').value
    };
    this.myBusinessService.getChangesRelatedToData().subscribe(
      (resp: any) => {
        this.ChangesRelatedTo = resp;
        //  alert(this.COPPartList.length);
      },
      (err) => {}
    );
  }
  //-------------------------------- quality ends here-----------------------------------------------------------
  //----------------------------------- Quality -->COP Upload--------------------------------------------------------
  initCOPUploadForm() {
    console.log(this.vendorDetails.VendorCode);
    this.COPUploadForm = this._formBuilder.group({
      subVendorCode: [this.supplierCode, [Validators.required]],
      partNumber: ["", [Validators.required]],
      partDescription: ["", [Validators.required]],
      certificateNumber: ["", [Validators.required]],
      TACNumber: ["", [Validators.required]],
      ValidFrom: ["", [Validators.required]],
      ValidUpTo: ["", [Validators.required]],
      document: ["", [Validators.required]],
    });
    this.GetCOPDetailsByID();
  }

  checkCOPApprover(){
    if(this.COP_Approvers.findIndex(o=> parseInt(o) == parseInt(this.username.split(`\\`).reverse()[0])) > -1){
      console.log('COP Approver')
      return true
    } else {
      console.log('Not COP Approver')
      return false
    }
  }

  getVendorList() {
    let reqjson = {
      Vendor: this.vendorDetails.VendorCode,
    };
    this.myBusinessService.getVendorList(reqjson).subscribe(
      (resp: any) => {
        this.vendorList = resp.ResponseData;
        //this.initCOPUploadForm();
      },
      (err) => {}
    );
  }

  rejectRemoveCOP(user, partNo, action, ID) {
    let reqjson = {
      COPId: ID,
      Flag: action,
      UserID: user,
      PartNumber: partNo,
      RejectedBy: this.supplierCode,
    };
    this.myBusinessService.updateCOP(reqjson).subscribe(
      (resp: any) => {
        this.toastr.success("Updated successfully");
        this.getCOPPageChangeData({ pageIndex: 0 });
      },
      (err) => {
        this.toastr.error("Failed to update");
      }
    );
  }

  GetCOPDetailsByID() {
    let reqjson;
    if (this.RoleID == "7" || this.menuFlag == "COP Plan") {
      this.COPFlag = false;
      reqjson = {
        SubVendor: this.COPUploadForm.get("subVendorCode").value,
      };
    } else {
      reqjson = {
        SubVendor: this.vendorDetails.VendorCode,
      };
    }

    this.myBusinessService.getPartsDDL(reqjson).subscribe(
      (resp: any) => {
        this.COPPartList = resp.ResponseData;
        //  alert(this.COPPartList.length);
      },
      (err) => {}
    );

    // get details of mail flag
    let req = {
      SupplierCode: this.COPUploadForm.get("subVendorCode").value,
    };
    this.myBusinessService.getMailFlag(req).subscribe(
      (resp: any) => {
        if (resp.ID == 1) this.initialMailFlag = true;
        else this.initialMailFlag = false;
      },
      (err) => {}
    );
  }
  getPartDetails() {
    let reqjson;
    if (this.RoleID == 7) {
      reqjson = {
        PartNumber: this.COPUploadForm.get("partNumber").value,
        //  "SupplierCode":"DM181"
        SupplierCode: this.COPUploadForm.get("subVendorCode").value,
      };
    } else {
      reqjson = {
        PartNumber: this.COPUploadForm.get("partNumber").value,
        //  "SupplierCode":"DM181"
        SupplierCode: this.vendorDetails.VendorCode,
      };
    }
    this.myBusinessService.getPartDetails(reqjson).subscribe(
      (resp: any) => {
        this.COPUploadForm.controls["partDescription"].setValue(
          resp.ResponseData.PartDescription
        );
        this.COPUploadForm.controls["TACNumber"].setValue(
          resp.ResponseData.TacNo
        );
        this.COPUploadForm.controls["certificateNumber"].setValue(
          resp.ResponseData.CertificateNo
        );
      },
      (err) => {}
    );
  }
  resetCOPUpload() {
    this.COPDataSource = []
    this.COPUploadForm.controls["subVendorCode"].setValue(this.supplierCode);
    this.COPUploadForm.reset();
    this.copPartsFilter = ""
  }

  navigate() {
    if (this.RoleID == 7 || this.RoleID == "7") {
      this.router.navigateByUrl("adminDashboard");
    } else {
      this.router.navigateByUrl("dashboard");
    }
  }

  addCOPDetails() {
    if (this.COPUploadForm.valid) {
      let fromDate = moment(this.COPUploadForm.get("ValidFrom").value).format(
        "YYYY-MM-DD"
      );
      let endDate = moment(this.COPUploadForm.get("ValidUpTo").value).format(
        "YYYY-MM-DD"
      );
      let formData: FormData = new FormData();
      let uploadReq = {
        subVendor: this.COPUploadForm.get("subVendorCode").value,
        // "supplierCode": "DM181",
        SupplierCode: this.vendorDetails.VendorCode,
        partNo: this.COPUploadForm.get("partNumber").value,
        validFrom: fromDate,
        validTo: endDate,
        certificateNo: this.COPUploadForm.get("certificateNumber").value,
        tacNo: this.COPUploadForm.get("TACNumber").value,
        partDescription: this.COPUploadForm.get("partDescription").value,
        createdBy: "amrut",
        modifiedBy: "amrut",
      };
      formData.append("uploadReq", JSON.stringify(uploadReq));
      for (var i = 0; i < this.fileUrls.length; i++) {
        formData.append("Certificate", this.fileUrls[i]);
      }
      this.myBusinessService.insertCOP(formData).subscribe(
        (resp: any) => {
          this.toastr.success(resp.Message);
          this.COPUploadForm.reset();
          this.COPUploadForm.controls["subVendorCode"].setValue(
            this.supplierCode
          );
          this.getCOPPageChangeData({
            pageIndex: 0,
            pageSize: 10,
            length: this.pageSize,
          });
        },
        (err) => {}
      );
    } else {
      this.validateAllFormFields(this.COPUploadForm);
    }
  }

  copPartsFilter: any = "";
  initialMailFlag = false;
  filterCOP(value) {
    this.copPartsFilter = value;
    this.getCOPPageChangeData({
      pageIndex: 0,
      pageSize: 10,
      length: this.pageSize,
    });
  }
  getCOPDetails(event) {
    this.pageNo = event.pageIndex;
    let reqjson = {
      // "ExpiryDate" : this.expiryDate,
      Vendor: this.vendorDetails.VendorCode,
      PageNo: this.pageNo + 1,
      SearchText: this.copPartsFilter,
    };
    this.myBusinessService.getCOPDetails(reqjson).subscribe(
      (resp: any) => {
        if (!this.cs.isUndefinedORNull(resp.ResponseData)) {
          this.COPDataSource = resp.ResponseData.CopDetails;
          this.pageSize = resp.ResponseData.Count;
        } else {
          this.cs.showError("Error while fetching data");
        }
      },
      (err) => {}
    );
  }
  //----------------------------- Quality -->COP Upload ends here --------------------------------------------
  //----------------------------- Quality --> DCPUploadForm---------------------------------------------------
  initDCPUploadForm() {
    // this.vendorDetails.VendorCode ='DA001';
    this.vendorC = this.vendorDetails.VendorCode;
    if (this.RoleID == "7" || this.menuFlag == "DCP Admin") {
      this.DCPUploadForm = this._formBuilder.group({
        vendorCode: ["", [Validators.required]],
        model: ["", [Validators.required]],
        partNumber: ["", [Validators.required]],
        documentType: ["", [Validators.required]],
        date: ["", [Validators.required]],
        document: ["", [Validators.required]],
      });
    } else {
      this.DCPUploadForm = this._formBuilder.group({
        vendorCode: [
          { value: this.supplierCode, disabled: true },
          [Validators.required],
        ],
        model: ["", [Validators.required]],
        partNumber: ["", [Validators.required]],
        documentType: ["", [Validators.required]],
        date: ["", [Validators.required]],
        document: ["", [Validators.required]],
      });
    }
  }

  getDCPUploadMaster() {
    this.myBusinessService.GetMasterUpload().subscribe(
      (resp: any) => {
        this.DCPUploadMaster = resp.vendorCode;
        this.DCPUploadModel = resp.getModel;
        this.DCPDocumentType = resp.getDocumentType;
        this.initDCPUploadForm();
        this.initCOPUploadForm();
        this.initChangeManagementForm();
      },
      (err) => {}
    );
  }
  resetDCPUploadForm() {
    if (this.RoleID != "7" && this.menuFlag == "Upload DCP Document") {
      this.DCPUploadForm.reset();
      this.DCPUploadForm.controls["vendorCode"].setValue(this.vendorC);
    } else if (this.RoleID == "7" || this.menuFlag == "DCP Admin") {
      this.adminVendorCode = "";
      this.DCPUploadData = [];
      this.pageSize = 0;
    }
  }
  uploadDCP() {
    if (this.DCPUploadForm.valid) {
      let date = moment(this.DCPUploadForm.get("date").value).format(
        "YYYY-MM-DD"
      );
      let formData: FormData = new FormData();
      let reqjson = {
        VendorCode: this.DCPUploadForm.get("vendorCode").value,
        PartNo: this.DCPUploadForm.get("partNumber").value,
        Model: this.DCPUploadForm.get("model").value,
        DocType: this.DCPUploadForm.get("documentType").value,
        DocDate: date,
      };
      formData.append("reqjson", JSON.stringify(reqjson));
      for (var i = 0; i < this.fileUrls.length; i++) {
        formData.append("file", this.fileUrls[i]);
      }
      this.myBusinessService.insertDCPUpload(formData).subscribe(
        (resp: any) => {
          this.toastr.success(resp.Message);
          this.DCPUploadForm.reset();
          this.DCPUploadForm.controls["vendorCode"].setValue(this.vendorC);
          this.getDCPPageChangeData({
            pageIndex: 0,
            pageSize: 10,
            length: this.pageSize,
          });
        },
        (err) => {}
      );
    } else {
      this.validateAllFormFields(this.DCPUploadForm);
    }
  }
  getDCPUploadedData(event) {
    // alert(this.RoleID);
    this.pageNo = event.pageIndex;
    let reqjson;
    if (this.RoleID != "7" && this.menuFlag == "Upload DCP Document") {
      reqjson = {
        // "VendorCode":"A0030H",
        VendorCode: this.vendorC,
        PageNo: this.pageNo + 1,
        Model: this.modelFilter,
      };
    } else if (this.RoleID == "7" || this.menuFlag == "DCP Admin") {
      reqjson = {
        // "VendorCode":"A0030H",
        VendorCode: this.adminVendorCode,
        PageNo: this.pageNo + 1,
        Model: this.modelFilter,
      };
    }
    //  alert(this.adminVendorCode);
    this.myBusinessService.getDCPUpload(reqjson).subscribe(
      (resp: any) => {
        this.DCPUploadData = resp.ResponseData;
        if (this.cs.isUndefinedORNull(this.DCPUploadData)) {
          this.DCPUploadData = [];
        }
        this.pageSize = resp.Count;
      },
      (err) => {}
    );
  }

  // admin transactions
  CDMMAdminDataSource = [];

  CMdisplayedColumns = [
    "srNo",
    "plantName",
    "qualityChampEmail",
    "designChampEmail",
    "delete",
  ];
  // CDMMAdmindisplayedColumns = ['selectCheck','type','name','modified','modifiedBy','documentDescription','dateofUpload','nameofPerson'];
  CDMMAdmindisplayedColumns = [
    "selectCheck",
    "type",
    "VendorCode",
    "Folder",
    "subFolder",
    "platform",
    "DocDescription",
    "dateofUpload",
  ];
  // CDMMAdminDataSource = [
  //   {selectCheck: false, type: '', name: '00 APQP status report', modified: '3/29/2020  4:33 PM',modifiedBy:'MSETU HELPDESK',documentDescription:'',dateofUpload:'',nameofPerson:'',},
  //   {selectCheck: false, type: '', name: '00 APQP status report', modified: '3/29/2020  4:33 PM',modifiedBy:'MSETU HELPDESK',documentDescription:'',dateofUpload:'',nameofPerson:'',},
  //   {selectCheck: false, type: '', name: '00 APQP status report', modified: '3/29/2020  4:33 PM',modifiedBy:'MSETU HELPDESK',documentDescription:'',dateofUpload:'',nameofPerson:'',},
  //   {selectCheck: false, type: '', name: '00 APQP status report', modified: '3/29/2020  4:33 PM',modifiedBy:'MSETU HELPDESK',documentDescription:'',dateofUpload:'',nameofPerson:'',},
  //   {selectCheck: false, type: '', name: '00 APQP status report', modified: '3/29/2020  4:33 PM',modifiedBy:'MSETU HELPDESK',documentDescription:'',dateofUpload:'',nameofPerson:'',},
  //   {selectCheck: false, type: '', name: '00 APQP status report', modified: '3/29/2020  4:33 PM',modifiedBy:'MSETU HELPDESK',documentDescription:'',dateofUpload:'',nameofPerson:'',},
  //  ];
  openModal(content) {
    this.CMForm.reset();
    this.modalService.open(content, {
      size: "lg",
      centered: true,
      windowClass: "formModal",
    });
  }
  closeModal() {
    this.modalService.dismissAll();
  }

  showDocumentsNPD(element) {
    // console.log(element.DocPath);
    window.open(element.DocPath, "_blank");
  }

  checkedCDMMAdmin = [];

  changeValue(e, checkedval, aa) {
    console.log(e);
    // window.open(link, '_blank');
    console.log(checkedval);
    if (e.checked == true || aa == true) {
      this.checkedCDMMAdmin.push(checkedval);
      console.log(this.checkedCDMMAdmin);
    } else {
      const idToRemove = checkedval;

      this.checkedCDMMAdmin = this.checkedCDMMAdmin.filter(
        (item) => item !== idToRemove
      );
      console.log(this.checkedCDMMAdmin);
    }
  }
  downloadMultiple() {
    if (this.checkedCDMMAdmin.length > 0) {
      for (var i = 0; i < this.checkedCDMMAdmin.length; i++) {
        FileSaver.saveAs(
          this.checkedCDMMAdmin[i].DocPath,
          this.checkedCDMMAdmin[i].UploadDocument
        );
      }
    }
  }
  changeAllValue(elem) {
    this.checkedCDMMAdmin.length = 0;
    if (elem.checked == true) {
      this.CDMMAdminDataSource.filter((element, i) => {
        element["checked"] = true;
        this.changeValue(element["checked"], element, true);
      });
    } else {
      this.CDMMAdminDataSource.filter((element, i) => {
        element["checked"] = false;
        this.changeValue(element["checked"], element, false);
      });
    }
  }

  COPList: any = [];
  expiryDate: any = "";
  COPVendorCode: any = "";
  COPDashboarddisplayedColumns = [
    "srNo",
    "plantName",
    "details",
    "partNo",
    "partDesc",
    "certificate",
    "TAC",
    "days",
    "qualityChampEmail",
    "designChampEmail",
  ];
  getCOPListDataSource(event) {
    this.getCOPList(event, false);
  }
  filterCOPDashboardList() {
    this.getCOPListDataSource({
      pageIndex: 0,
      pageSize: 10,
      length: this.pageSize,
    });
  }

  removeFile() {
    console.log("remove_file");
    this.fileUrls = [];
    this.urls = [];

    if (this.menuFlag == "Upload COP") {
      this.COPUploadForm.controls["document"].reset();
      console.log(document.getElementById('file-input'))
      console.log(document.getElementById('file-input').nodeValue)
      this.copUploadFile.reset()
      this.copUploadFile.markAsUntouched()
    }
  }
  getCOPList(event, isExcel): Promise<any> {
    return new Promise((resolve: any) => {
      this.pageNo = event.pageIndex;
      let reqjson = {
        Filter: this.COPPlanFilter == "vendorCode" ? 1 : 2,
        PageNo: isExcel ? this.pageNo : (this.pageNo + 1 || 1),
      };
      this.myBusinessService.getCOPListDataSource(reqjson).subscribe(
        (resp: any) => {
          // this.expiryDate=null;
          if (isExcel && this.COPPlanFilter == "vendorCode") {
            this.Json = [];
            let vendor = [
              "TotalCount",
              "TacNo",
              "PartNo",
              "PartDescription",
              "ExpireInDays",
              "CertificateNo",
            ];
            resp.ResponseData.forEach((data) => {
              vendor.forEach((vendor) => {
                delete data[vendor];
              });
              this.Json.push(data);
            });
            resolve();
          } else if (isExcel && this.COPPlanFilter == "expiryDate") {
            this.Json = [];
            let vendor = [
              "TotalCount",
              "Certificate_pending",
              "Certificate_received",
            ];
            resp.ResponseData.forEach((data) => {
              vendor.forEach((vendor) => {
                delete data[vendor];
              });
              this.Json.push(data);
            });
            resolve();
          } else if (!isExcel) {
            this.COPList = resp.ResponseData;
            console.log(this.COPList)
            this.pageSize = resp.ResponseData[0].TotalCount;
          } else {
            if (this.cs.isUndefinedORNull(this.COPList)) {
              this.COPList = [];
              resolve();
            }
            this.pageSize = resp.TotalCount;
            resolve();
          }
        },
        (err) => {}
      );
    });
  }

  resetCOPDashboardList() {
    this.COPVendorCode = "";
    this.expiryDate = "";
    this.getCOPList({ pageIndex: 0 }, false);
  }

  exportExcel() {
    this.getCOPList({ pageIndex: 0 }, true).then((data) => {
      // this.cs.exportJsonAsExcelFile(this.Json, "COPdashboardList");
      let dataNew = this.Json;
      console.log(dataNew);
      //Create workbook and worksheet
      let workbook = new Workbook();
      let worksheet = workbook.addWorksheet("ReportData");
      let columns = Object.keys(this.Json[0]);
      let headerRow = worksheet.addRow(columns);
      // Cell Style : Fill and Border
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: "00FF0000" },
          bgColor: { argb: "00FF0000" },
        };
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
      });
      dataNew.forEach((d) => {
        // var obj = JSON.parse(d);
        var values = Object.keys(d).map(function (key) {
          return d[key];
        });
        let row = worksheet.addRow(values);
      });
      workbook.xlsx.writeBuffer().then((dataNew) => {
        let blob = new Blob([dataNew], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        fs.saveAs(blob, "ReportData"+(this.COPPlanFilter? ' ('+ this.COPPlanFilter +')' : '')+".xlsx");
      });
    });
  }

  sendCOPEmail() {
    const modalRef = this.modalService.open(COPEmailModalComponent, {
      size: "lg",
    });
    modalRef.componentInstance.COPPartList = this.COPPartList;
    modalRef.componentInstance.supplierName = this.COPUploadForm.get(
      "subVendorCode"
    ).value;
  }

  sendEmail() {
    let reqjson = {
      VendorCode: this.COPUploadForm.get("subVendorCode").value,
      PartNoList: [],
    };
    this.myBusinessService.sendCOPEmail(reqjson).subscribe(
      (resp: any) => {
        this.toastr.success("Mail sent successfully");
        this.callUpdateMailFlag();
      },
      (err) => {
        this.toastr.error("Something went wrong");
      }
    );
  }
  callUpdateMailFlag() {
    let reqjson = {
      SupplierCode: this.COPUploadForm.get("subVendorCode").value,
      Status: "1",
    };
    this.myBusinessService.updateMailFlag(reqjson).subscribe(
      (resp: any) => {
        this.GetCOPDetailsByID();
      },
      (err) => {}
    );
  }
  onTabChanged(event) {
    this.COPUploadForm.reset();
    this.COPDataSource = []
    // this.initialMailFlag = false;
    if (event.index == 1) {
      this.getCOPList({ pageIndex: 0 }, false);
    }
  }
}
