import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpdeskConcernComponent } from './helpdesk-concern.component';

describe('HelpdeskConcernComponent', () => {
  let component: HelpdeskConcernComponent;
  let fixture: ComponentFixture<HelpdeskConcernComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpdeskConcernComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpdeskConcernComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
