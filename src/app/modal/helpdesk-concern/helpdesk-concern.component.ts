import { Component, OnInit, Input } from "@angular/core";
import { MyHelpDeskService } from "src/app/services/myHelpDesk/my-help-desk.service";
import * as CryptoJS from 'crypto-js';
import * as _ from "lodash";
import * as moment from "moment";

import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl
} from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/cmslanding/date-adapter';

@Component({
  selector: "app-helpdesk-concern",
  templateUrl: "./helpdesk-concern.component.html",
  styleUrls: ["./helpdesk-concern.component.scss"],
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: APP_DATE_FORMATS,
    },
  ],
})
export class HelpdeskConcernComponent implements OnInit {
  @Input() public ticketRowId;
  moment = moment;

  createConcernOptions: any = {};
  userRole: any;
  userToken: any;

  statusUpdateFile: any = [];
  message = new FormControl("", Validators.required);

  Messages = [];

  file: any;
  fileName: any;
  Files = []
  public createConcernForm: FormGroup;
  statusFlags = [
    { 1: "New" },
    { 2: "Work in progress" },
    { 3: "More Info Required" },
    { 4: "Pending with FI Team" },
    { 5: "Pending with BI team" },
    { 6: "Pending with GRC team" },
    { 7: "Pending with Others" },
    { 8: "Resolved" },
    { 9: "Closed" },
    { 10: "Reopen" }
  ];
  constructor(
    public helpDeskService: MyHelpDeskService,
    public formBuilder: FormBuilder,
    public toastr: ToastrService,
    public modal: NgbModal
  ) {
    this.createConcernForm = this.formBuilder.group({
      vendorId: [{ value: "", disabled: true }, Validators.required],
      IssueType: [{ value: "", disabled: true }, Validators.required],
      SectorId: [{ value: "", disabled: true }],
      PlantId: [{ value: "", disabled: true }],
      BussinessFunction: [{ value: "", disabled: true }],
      IssueCategory: [{ value: "", disabled: true }, Validators.required],
      AssignedEmail: [{ value: "", disabled: true }],
      PhoneNo: [{ value: "", disabled: true }, Validators.required],
      TicketRequestDate: [{ value: "", disabled: true }, Validators.required],
      TicketTitle: [{ value: "", disabled: true }],
      Description: [{ value: "", disabled: true }],
      file: [{ value: "", disabled: true }],
      fileName: [{ value: "", disabled: true }],
      status: [
        // { value: "", disabled: true }
      ]
    });
  }
  roleId:any;
  action:any;
  ifReopen:boolean= false;

  ngOnInit() {
    this.action = this.ticketRowId.action;
    if(this.ticketRowId.statusID == '8' || this.ticketRowId.statusID == '9' )
    this.ifReopen = true;
    this.userToken = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
    this.userToken = CryptoJS.AES.decrypt(this.userToken,"").toString(CryptoJS.enc.Utf8);
    this.userRole = localStorage.getItem('PR');
    this.userRole =  CryptoJS.AES.decrypt(this.userRole,"").toString(CryptoJS.enc.Utf8);
    this.roleId =    CryptoJS.AES.decrypt(localStorage.getItem("WTIwNWMxcFZiR3M9"), "").toString(
      CryptoJS.enc.Utf8
    );
    this.getHistory();

    this.helpDeskService
      .call("IncidentManagement/Get_IncidentMgmtMasterDetails", null)
      .subscribe(response => {
        console.log(response);
        this.createConcernOptions = _.cloneDeep(response.ResponseData);

        this.helpDeskService
          .call("IncidentManagement/Get_TicketDetailsById", {
            TicketReq_RowID: this.ticketRowId.ticketRowId
          })
          .subscribe(response => {
            console.log(response);
            this.Files = response.ResponseData
            let res = response.ResponseData[0]
            this.createConcernForm
              .get("vendorId")
              .patchValue(res.VendorCode);
            this.createConcernForm
              .get("IssueType")
              .patchValue(res.IssueTypeID);
            this.createConcernForm
              .get("SectorId")
              .patchValue(res.SectorId);
            this.createConcernForm
              .get("PlantId")
              .patchValue(res.PlantId);
            this.createConcernForm
              .get("BussinessFunction")
              .patchValue(res.BussinessFunction);
            this.createConcernForm
              .get("IssueCategory")
              .patchValue(res.IncidentSubCategory_Id);
            this.createConcernForm
              .get("AssignedEmail")
              .patchValue(res.AssignedEamilID);
            this.createConcernForm
              .get("PhoneNo")
              .patchValue(res.ContactNo);
            this.createConcernForm
              .get("TicketRequestDate")
              .patchValue(res.TicketReqDate);
            this.createConcernForm
              .get("TicketTitle")
              .patchValue(res.TicketTitle);
            this.createConcernForm
              .get("Description")
              .patchValue(res.TicketDescription);
            this.createConcernForm
              .get("status")
              .patchValue(res.StatusID);

              
            // let name = response.ResponseData.DocumentPath
            // name = name.split('/').reverse()[0]
            // this.file = response.ResponseData.DocumentPath
            // this.fileName = name
            
            this.createConcernForm
              .get("file")
              .patchValue(res.DocumentPath);
            this.createConcernForm
              .get("fileName")
              .patchValue(res);

            console.log(this.createConcernForm);
          });
      });
  }
  
  getFileName(filePath){
    if(filePath){
      let name = filePath.split('/').reverse()[0]
      return name
    }
  }

  getHistory() {
    this.helpDeskService
      .call("IncidentManagement/Get_TicketHistoryByTicketID", {
        TicketReq_RowID: this.ticketRowId.ticketRowId
      })
      .subscribe(response => {
        console.log(response);
        this.Messages = response.ResponseData
      });
  }

  detectFiles($event) {
    console.log($event.target.files);
    this.statusUpdateFile = $event.target.files;
  }
  submit(action) {
    if(this.message.valid){
      const Reqjson = {
        TicketReq_RowID: this.ticketRowId.ticketRowId,
        StatusID: this.createConcernForm.get("status").value,
        TicketMessage: this.message.value,
        UserName: this.userToken
      };
      let formData: FormData = new FormData();
      formData.append("reqjson", JSON.stringify(Reqjson));
      formData.append("file", this.statusUpdateFile);
  
      this.helpDeskService
        .call("IncidentManagement/SubmitTicketMessage", formData)
        .subscribe(response => {
          console.log(response);
          if (response.Message == "Success") {
            this.toastr.success("Message Sent Successfully");
            this.reopen(this.ticketRowId.ticketRowId);
            this.message.reset();
            this.statusUpdateFile = [];
            this.getHistory();
          }
        });
    }else{
      this.toastr.warning('Kindly add message to submit');
    }
  }
  reopen(ticketRowId) {
    this.helpDeskService
      .call("IncidentManagement/ReopenTicket", {
        TicketReq_RowID: ticketRowId.ticketRowId,
      })
      .subscribe((res) => {
        if (res.Message == "Success") {
          this.toastr.success("Ticket Reopened");
        }
      });
  }

  // submit() {
  //   if(this.message.valid){
  //     const Reqjson = {
  //       TicketReq_RowID: this.ticketRowId,
  //       StatusID: this.createConcernForm.get("status").value,
  //       TicketMessage: this.message.value,
  //       UserName: this.userToken
  //     };
  //     let formData: FormData = new FormData();
  //     formData.append("reqjson", JSON.stringify(Reqjson));
  //     // formData.append("file", this.statusUpdateFile);
  //     this.statusUpdateFile.forEach((file, index) => {
  //       formData.append("file" + index, file);
  //     });
  
  //     this.helpDeskService
  //       .call("IncidentManagement/SubmitTicketMessage", formData)
  //       .subscribe(response => {
  //         console.log(response);
  //         if (response.Message == "Success") {
  //           this.toastr.success("Message Sent Successfully");
  //           this.message.reset();
  //           this.statusUpdateFile = [];
  //           this.getHistory();
  //         }
  //       });
  //   }else{
  //     this.toastr.warning('Kindly add message to submit');
  //   }
  // }

  openFile(file){
    console.log(file)
    window.open(file,'_blank')
  }

  dismissModal(){
    this.modal.dismissAll()
  }
}
