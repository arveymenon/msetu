import { Component, OnInit, Input } from '@angular/core';
import { MySuggestionService } from 'src/app/services/MySuggestion/my-suggestion.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-vendor-employees',
  templateUrl: './vendor-employees.component.html',
  styleUrls: ['./vendor-employees.component.sass']
})
export class VendorEmployeesComponent implements OnInit {

  
  dataSource = [];
  displayedColumns : string[] =['SupplierCode','FirstName', 'LastName','Phone','Email'];
  @Input() UserName;

  constructor(public cs: MySuggestionService, public modalService:NgbModal) { }

  ngOnInit() {
    let postData ={
      "UserName":this.UserName
    }  
    
 
    this.cs.postMySuggestionData('Admin/GetVendorEmployeesByVendor', postData).subscribe(
      (res: any) => {
        this.dataSource = res.ResponseData;
        this.dataSource = [...this.dataSource];
  }
)
  }
  closeModal(){
    this.modalService.dismissAll();
  }
}
