import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-notification-on-plastic-usage',
  templateUrl: './notification-on-plastic-usage.component.html',
  styleUrls: ['./notification-on-plastic-usage.component.scss']
})
export class NotificationOnPlasticUsageComponent implements OnInit {

  constructor(public modal: NgbModal) { }

  ngOnInit() {
  }

  closeModal(){
    this.modal.dismissAll();
  }

}
