import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationOnPlasticUsageComponent } from './notification-on-plastic-usage.component';

describe('NotificationOnPlasticUsageComponent', () => {
  let component: NotificationOnPlasticUsageComponent;
  let fixture: ComponentFixture<NotificationOnPlasticUsageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationOnPlasticUsageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationOnPlasticUsageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
