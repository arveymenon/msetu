import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-contact-modal',
  templateUrl: './contact-modal.component.html',
  styleUrls: ['./contact-modal.component.scss']
})
export class ContactModalComponent implements OnInit {
  mailText:string = "";
  constructor(public modalService:NgbModal) { }

  ngOnInit() {
    this.mailText = "mailto:msetuhelpdesk@mahindra.com";
  }

  closeModal(){
    this.modalService.dismissAll();
  }

}
