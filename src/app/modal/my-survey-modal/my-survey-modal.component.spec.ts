import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MySurveyModalComponent } from './my-survey-modal.component';

describe('MySurveyModalComponent', () => {
  let component: MySurveyModalComponent;
  let fixture: ComponentFixture<MySurveyModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MySurveyModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MySurveyModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
