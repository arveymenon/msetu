import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { FormArray, FormControl, FormBuilder, Validators } from '@angular/forms';
import { NgbModal, NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { SurveyService } from 'src/app/services/survey/survey.service';
import { ToastrService } from 'ngx-toastr';
import * as CryptoJS from 'crypto-js';

import * as moment from 'moment';
 
@Component({
  selector: 'app-my-survey-modal',
  templateUrl: './my-survey-modal.component.html',
  styleUrls: ['./my-survey-modal.component.scss'],
  providers: [NgbRatingConfig]
})
export class MySurveyModalComponent implements OnInit {
  @Input() public SurveyId;
  public surveyForm = new FormArray([]);
  public surveyFormQuestions = [];
  formArray: FormBuilder;
  dynamicForm: any;
  moment = moment

  // rating = 2;
  constructor(
    private formBuilder: FormBuilder,
    public toastr: ToastrService,
    public modal: NgbModal,
    public rating: NgbRatingConfig,
    public surveyService: SurveyService
    ) {
    }

    userToken:any
    RoleID:any;
    ngOnInit() {
      this.userToken = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
      this.userToken = CryptoJS.AES.decrypt(this.userToken,"").toString(CryptoJS.enc.Utf8);
      this.RoleID = localStorage.getItem("WTIwNWMxcFZiR3M9");
      this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(
        CryptoJS.enc.Utf8
      );
      this.rating.max = 5;
      let body = {
        "SurveyId": this.SurveyId
      }
      this.surveyService.getMySurveyQuestions('AdminCrudSurvey/GetMySurveyQuestionsList', body).subscribe(res => {
        console.log(res)
        this.surveyFormQuestions = res;
        
        this.setQuestions();
        
      });
      console.log(this.surveyFormQuestions);
      // this.surveyFormQuestions.push({
      //   question_id: 1,
      //   question: ' Getting Timely Payment as per policy',
      //   question_type: 1,
      //   required: true,
      //   options: ['Very Poor','Poor','Average','Good','Very Good']
      // })
      // this.surveyFormQuestions.push({
      //   question_id: 2,
      //   question: 'Regularity in Reciept of Payment Details of Advices',
      //   question_type: 2,
      //   required: true,
      //   options: ['Very Poor','Poor','Average','Good','Very Good']
      // })
      // this.surveyFormQuestions.push({
      //   question_id: 3,
      //   question: 'Effectiveness in attending & resolving Payment related Issues',
      //   question_type: 3,
      //   required: true,
      //   options: ['Very Poor','Poor','Average','Good','Very Good']
      // })
    // this.createForm();
    // this.loadFormData();
  }
  
  setQuestions() {
    this.surveyFormQuestions.forEach(element => {
      console.log(element);
      this.surveyForm.push(this.formBuilder.group({
        question_id:  [element.QuestionID],
        answer:  ['', Validators.required]
      }));
      console.log(this.surveyForm);
    });
  }
 
  closeModal(){
    this.modal.dismissAll();
  }


 
  submitSurvey() {
    console.log(this.surveyForm);
    console.log(this.surveyForm.valid);
    // this.modal.dismissAll();
    if (this.surveyForm.valid) {
      console.log('Success');
      const questions = [];
      for (const question of this.surveyForm.value) {
        questions.push({
          Quetion_Id: question.question_id,
          Answer: question.answer
        });
      }
      const body = {
        TokenId: this.userToken,// localStorage.getItem('userToken'),
        SurveyId: this.SurveyId,
        SurveyQuetionAnsList: questions
      }
      console.log(body);
      this.surveyService.postMySurveyQuestions('AdminCrudSurvey/SurveyQuetionAnsResponce', body).subscribe(res => {
        console.log(res);
        this.modal.dismissAll();
        this.toastr.success('Survey Submitted Successfully');
      });
    } else {
      this.toastr.error('Kindly fill in all the required fields');
    }
  }
}