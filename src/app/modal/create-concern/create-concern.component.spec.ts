import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateConcernComponent } from './create-concern.component';

describe('CreateConcernComponent', () => {
  let component: CreateConcernComponent;
  let fixture: ComponentFixture<CreateConcernComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateConcernComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateConcernComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
