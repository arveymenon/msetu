import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MyHelpDeskService } from "src/app/services/myHelpDesk/my-help-desk.service";
import { NotificationModalComponent } from "src/app/services/myHelpDesk/notification-modal/notification-modal/notification-modal.component";
import { ToastrService } from "ngx-toastr";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import * as moment from 'moment';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: "app-create-concern",
  templateUrl: "./create-concern.component.html",
  styleUrls: ["./create-concern.component.scss"],
})
export class CreateConcernComponent implements OnInit {
  concernForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    public myHelpDeskService: MyHelpDeskService,
    public toastr: ToastrService,
    public modal: NgbModal
  ) {}

  username:any
  today = new Date()
  vendorcode:any

  ngOnInit() {
    this.username = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
    this.username = CryptoJS.AES.decrypt(this.username,"").toString(CryptoJS.enc.Utf8);
    this.vendorcode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
    this.vendorcode = CryptoJS.AES.decrypt(this.vendorcode,"").toString(CryptoJS.enc.Utf8);
    this.concernForm = this.formBuilder.group({
      title: ["", Validators.required],
      date: ["", Validators.required],
      time: ["", Validators.required],
      description: ['',Validators.required],
    });
  }

  submit() {
    console.log(this.concernForm);
    console.log(this.concernForm.value.time)
    this.concernForm.markAsTouched();
    if (this.concernForm.valid) {
      const body = {
        statusID: 1,
        issueTypeID: 2,
        sectorID: null,
        plantID: null,
        businessFunID: null,
        incidentCategory_Id: 10,
        incidentSubCategory_Id: 31,
        assignedEamilID: "",
        contactNo: "",
        ticketReqDate: new Date(),
        ticketTitle: this.concernForm.value.title,
        ticketDescription:
          "Date: " +
          moment(this.concernForm.value.date).format('DD/MM/YYYY') +
          " Time: " +
          this.concernForm.value.time.hour+':'+this.concernForm.value.time.minute +
          "- " +
          this.concernForm.value.description,
        createdBy:this.username,//    localStorage.getItem("userToken"),
        vendorCode:this.vendorcode,// localStorage.getItem("vendorCode"),
        file: null,
      };
      console.log(body);
      let formData: FormData = new FormData();
      formData.append("reqjson", JSON.stringify(body));
      // let formData: FormData = new FormData();
      formData.append("file", null);
      this.myHelpDeskService
        .call("IncidentManagement/Insert_TicketReqDetails", formData)
        .subscribe((res) => {
          console.log(res);
          if (res.Message == "Success") {
            this.toastr.success("Incident Reported Successfully");
            let modal = this.modal.open(NotificationModalComponent, {
              centered: true,
              size: "lg",
            });
            modal.componentInstance.incidentNumber = res.ResponseData;
            // this.toastr.
            this.concernForm.reset();
          }
        });
    } else {
      this.toastr.warning("Please fill all mandatory details");
    }
  }

  closeModal(){
    this.modal.dismissAll()
  }
}
