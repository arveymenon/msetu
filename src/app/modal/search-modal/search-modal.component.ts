import { Component, OnInit, ViewChild } from "@angular/core";
import { MatIconRegistry } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { SearchService } from "src/app/services/search/search.service";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import * as CryptoJS from "crypto-js";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MySuggestionService } from "src/app/services/MySuggestion/my-suggestion.service";

@Component({
  selector: "app-search-modal",
  templateUrl: "./search-modal.component.html",
  styleUrls: ["./search-modal.component.scss"],
})
export class SearchModalComponent implements OnInit {
  searchResults = [];
  editEmpFormdata: FormGroup;
  displayedColumns3: string[] = [
    "SearchField",
    "Link",
    "Description",
    "SearchName",
    "action"
  ];
  dataSource:any =[];
  @ViewChild("closeAddModal") closeAddModal;
  constructor(
    public modal: NgbModal,
    public formBuilder: FormBuilder,
    private matIconRegistry: MatIconRegistry,
    private searchService: SearchService,
    private domSanitizer: DomSanitizer,
    public modalService: NgbModal,
    private router: Router,
    public cs: MySuggestionService, 
    private toastr: ToastrService
  ) {
    matIconRegistry.addSvgIcon(
      "search",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/sidemenu/search.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "edit",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Edit.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "delete",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/delete.svg"
      )
    );
  }
  RoleID :any;
  pageSize:any;
  pageNo:any=0;
  searchID:any ='';

  showListFlag:boolean =false;

  ngOnInit() {
    this.RoleID = localStorage.getItem("WTIwNWMxcFZiR3M9");
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(
      CryptoJS.enc.Utf8
    );
    // alert(this.RoleID)
    this.editEmpForm();
    this.addSearchField('showContent', true , '');
  }
  getPageChangeData(event){
    this.getSearchData(event);
  }
  addSearchField(content, flag ,data) {
    this.showListFlag = flag;
    this.editEmpFormdata.reset();

    if(!this.showListFlag) {
        if(data) {
          this.searchID = data.Id;
          this.editEmpFormdata.controls["code"].setValue(data.SearchCode);
          this.editEmpFormdata.controls["name"].setValue(data.SearchName);
          this.editEmpFormdata.controls["link"].setValue(data.Link);
          this.editEmpFormdata.controls["desc"].setValue(data.Description);
        }
        this.modal.open(content, {
          size: "lg",
          centered: true,
          windowClass: "formModal",
        });
      }
     else {
       this.getSearchData({pageIndex: 0,pageSize: 10,length: this.pageSize});
        }
  }
  deleteSearchByID(data){
    let body ={
      Id : data.Id
    }
    this.cs
    .postMySuggestionData("MSetuSearch/DeleteSearchText",body)
    .subscribe(async (response) => {
      console.log(response);
      this.cs.showSuccess("Deleted successfully");
      this.addSearchField('showContent', true , ''); 
    }, (error) => {
      this.cs.showError(error);
      console.log(error.Message);
    });
  }
  
  getSearchData(event){
    this.pageNo = event.pageIndex;
    let body ={
      PageNo : this.pageNo +1
    }
    this.cs
    .postMySuggestionData("MSetuSearch/GetAllSearchTextList",body)
    .subscribe(async (response) => {
      console.log(response);
      this.dataSource = response.ResponseData; 
      this.pageSize = response.Count;  
    }, (error) => {
      this.cs.showError(error);
      console.log(error.Message);
    });
  }
  hideListTable(){
    this.showListFlag = false;
  }
  editEmpForm() {
    this.editEmpFormdata = this.formBuilder.group({
      // UserId: [0],
      // SupplierName: ["", Validators.required],
      code: ["", Validators.required],
      desc: ["", Validators.required],
      // Role: ["", Validators.required],
      // PrimaryRole: ["", Validators.required],
      // PhoneNumber: [""],
      name: ["",  Validators.required],
      link:["",Validators.required]

    });
  }
  closeModal() {
    this.modalService.dismissAll();
  }

  search(ev) {
    console.log("test");
    console.log(ev.srcElement.value),
      this.searchService
        .call("MSetuSearch/GetSearchResult", {
          SearchValue: ev.srcElement.value,
        })
        .subscribe((response: any) => {
          console.log(response)
          if(response.Message == 'Success'){
            this.searchResults = response.ResponseData
          }
        });
  }

  navigateTo(link){
    console.log(link)
    let newLink = link.split('/')
    if(newLink.length > 1){
      link = '/'+newLink.reverse()[0]
      // link == '/Profile' ? link = '/JIT' : false;
      console.log(link)
    } 

    this.router.navigate([link]).then((response=>{
      this.modalService.dismissAll()
    })).catch(err=>{
      this.toastr.error('Invalid Link'+link)
    })
  }

  onCloseAddModal(modal){
 
    modal();
    this.closeAddModal.nativeElement.click();
  }
  addSearch(){
    let body;
    if(this.editEmpFormdata.valid) {
      body ={
        SearchCode : this.editEmpFormdata.get('code').value,
        SearchName : this.editEmpFormdata.get('name').value,
        Description : this.editEmpFormdata.get('desc').value,
        Link : this.editEmpFormdata.get('link').value,
      }
    
   this.cs
    .postMySuggestionData("MSetuSearch/AddSearchText", body)
    .subscribe(async (response) => {
      console.log(response);
      this.cs.showSuccess("Added successfully");
      // this.onCloseAddModal('c');
      this.addSearchField('showContent', true , '');
    }, (error) => {
      this.cs.showError(error);
      console.log(error.Message);
    });
  }
}

updateSearchByID(){
  let body;
  if(this.editEmpFormdata.valid) {
    body ={
      Id          : this.searchID,
      SearchCode  : this.editEmpFormdata.get('code').value,
      SearchName  : this.editEmpFormdata.get('name').value,
      Description : this.editEmpFormdata.get('desc').value,
      Link        : this.editEmpFormdata.get('link').value,
    }
  
 this.cs
  .postMySuggestionData("MSetuSearch/UpdateSearchText", body)
  .subscribe(async (response) => {
    console.log(response);
    this.cs.showSuccess("Updated Successflly");
    // this.onCloseAddModal(t);
    this.addSearchField('showContent', true , '');
    // this.closeModal();
  }, (error) => {
    this.cs.showError(error);
    console.log(error.Message);
  });
}
}

}
