import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EscalationMatrixModalComponent } from './escalation-matrix-modal.component';

describe('ExcellenceAwardModalComponent', () => {
  let component: EscalationMatrixModalComponent;
  let fixture: ComponentFixture<EscalationMatrixModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EscalationMatrixModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EscalationMatrixModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
