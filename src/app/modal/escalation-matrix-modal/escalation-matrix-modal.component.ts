import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-escalation-matrix-modal',
  templateUrl: './escalation-matrix-modal.component.html',
  styleUrls: ['./escalation-matrix-modal.component.scss']
})
export class EscalationMatrixModalComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }
  closeModal(){
    this.modalService.dismissAll();
  }
}