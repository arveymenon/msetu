import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailoughBoxComponent } from './dailough-box.component';

describe('DailoughBoxComponent', () => {
  let component: DailoughBoxComponent;
  let fixture: ComponentFixture<DailoughBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailoughBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailoughBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
