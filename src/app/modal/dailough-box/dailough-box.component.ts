import { Component, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dailough-box',
  templateUrl: './dailough-box.component.html',
  styleUrls: ['./dailough-box.component.sass']
})
export class DailoughBoxComponent {
  flag:boolean = false;
  constructor(
    public dialogRef: MatDialogRef<DailoughBoxComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {

      if(data.messege == 'Are you sure want to approve all these record?' || data.messege =='Are you sure want to reject all these record?' || data.messege == 'Are you sure want to reject this record?' ||
          data.messege == 'Are you sure want to approve this record?')
      this.flag = true;
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
