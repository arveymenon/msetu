import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MySuggestionService } from 'src/app/services/MySuggestion/my-suggestion.service';
//import { MySuggestionService } from './services/MySuggestion/my-suggestion.service';
@Component({
  selector: 'app-escalate-matrix-modal',
  templateUrl: './escalate-matrix-modal.component.html',
  styleUrls: ['./escalate-matrix-modal.component.scss']
})
export class EscalateMatrixModalComponent implements OnInit {
  imageuploaded=[];
  imageArrayLength:number;
  singleArray:any;
  mArray:boolean=false;
  constructor(private modalService: NgbModal, public cs1:MySuggestionService) { }

  ngOnInit() {
this.callImageUpload();
  }
  callImageUpload(){
    this.cs1.showSwapLoader=true;
    let postData = { 
      "BusinessCategoryID":7,
      "BusinessSubCategoryID":0 
    }
    this.cs1.postMySuggestionData('DocumentManagement/Get_CMSDocs', postData).
      subscribe(
        (res: any) => {
          this.cs1.showSwapLoader = false;
          this.imageuploaded = res.ResponseData;
         console.log(this.imageuploaded);
         this.imageArrayLength= this.imageuploaded.length;
         if(this.imageArrayLength >0){
          if(this.imageuploaded.length<2){
            this.singleArray = this.imageuploaded[0].Doc_Link;
           // this.mArray= false;
           }
           else{
            this.singleArray = this.imageuploaded[this.imageArrayLength - 1].Doc_Link;
           }
         }
        
        },
        (error) => {
          this.cs1.showSwapLoader = false;
          console.log(error.message);
        });
  }
  closeModal(){
    this.modalService.dismissAll();
  }
}