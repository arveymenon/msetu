import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EscalateMatrixModalComponent } from './escalate-matrix-modal.component';

describe('EscalateMatrixModalComponent', () => {
  let component: EscalateMatrixModalComponent;
  let fixture: ComponentFixture<EscalateMatrixModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EscalateMatrixModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EscalateMatrixModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});