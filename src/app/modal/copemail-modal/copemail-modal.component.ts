import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MyBusinessServiceService } from '../../services/MyBusinessService/my-business-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-copemail-modal',
  templateUrl: './copemail-modal.component.html',
  styleUrls: ['./copemail-modal.component.scss']
})
export class COPEmailModalComponent implements OnInit {

  constructor(private toastr: ToastrService,public modalService:NgbModal,public myBusinessService:MyBusinessServiceService) { }

 
  @Input() public COPPartList;
  @Input() public supplierName;
  checkedData:any=[];
  COPdisplayedColumns = ['select','partNumber','partDesc']; 

  ngOnInit() {
    console.log(this.COPPartList);
    console.log(this.supplierName); 

  }
  sendEmail(){
    let reqjson={
      "VendorCode":this.supplierName,
      "PartNoList":this.checkedData
    }
    this.myBusinessService.sendCOPEmail(reqjson).subscribe((resp:any) => {
      this.toastr.success('Mail sent successfully');
      this.closeModal();
    }, (err) => {
      this.toastr.error('Something went wrong');
    });
  }
  closeModal(){
    this.modalService.dismissAll();
  }
  // selectAll(e) {
  //   this.selectAction ='';
  //   console.log("--"+e);
  //  // this.allChecked = !this.allChecked;
  //   if(e.detail.checked){
  //     this.dataSource.filter(element => {
  //       element["check"] = true;
  //     });
  //   }else{
  //     this.dataSource.filter(element => {
  //       element["check"] = false;
  //     });
  //   }
    
  // }

  selectData(e, Id) {
    console.log(Id, "..Id..");
    console.log(e.checked);
    if (e.checked) {
      if (this.checkedData.length > 0) {
        var result = this.checkedData.filter(x => x.PartNumber === Id);
        console.log(result);
        if (result.length > 0) {
        } else {
          let data = {
            PartNo: Id
          };
          this.checkedData.push(data);
        }
      } else {
        let data = {
          PartNo: Id
        };
        this.checkedData.push(data);
      }
    } else {
      if (this.checkedData.length > 0) {
        var result = this.checkedData.filter(x => x.PartNumber === Id);
        if (result.length > 0) {
          var index = this.checkedData.findIndex(x => x.PartNumber === Id);
          console.log(index, "index");
          this.checkedData.splice(index, 1);
        }
      } else {
      }
    }
   
    console.log(this.checkedData, "this.checkedData");
  }
 
}
