import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { COPEmailModalComponent } from './copemail-modal.component';

describe('COPEmailModalComponent', () => {
  let component: COPEmailModalComponent;
  let fixture: ComponentFixture<COPEmailModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ COPEmailModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(COPEmailModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
