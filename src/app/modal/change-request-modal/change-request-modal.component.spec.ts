import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeRequestModalComponent } from './change-request-modal.component';

describe('ChangeRequestModalComponent', () => {
  let component: ChangeRequestModalComponent;
  let fixture: ComponentFixture<ChangeRequestModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeRequestModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeRequestModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
