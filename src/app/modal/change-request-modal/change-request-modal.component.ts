import { Component, OnInit, Input } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MyBusinessServiceService } from "src/app/services/MyBusinessService/my-business-service.service";
import { ToastrService } from "ngx-toastr";
import * as CryptoJS from "crypto-js";
import { CommonApiService } from 'src/app/services/common-api/common-api.service';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/cmslanding/date-adapter';
import * as fs from "file-saver";
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: "app-change-request-modal",
  templateUrl: "./change-request-modal.component.html",
  styleUrls: ["./change-request-modal.component.scss"],
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: APP_DATE_FORMATS,
    },
  ],
})
export class ChangeRequestModalComponent implements OnInit {
  @Input() public passedData;
  @Input() public CategoryName;
  @Input() public flag;
  checkedArray: any = [];
  arr1: any = [];
  commonArray = [];
  CMRData;
  RoleID: any;
  username: any;
  supplierCode: any;
  remarks: any = "";
  changeManagementRole: any;
  fileUrl: any

  constructor(
    public modalService: NgbModal,
    public myBusinessService: MyBusinessServiceService,
    private toastr: ToastrService,
    public commonAPI: CommonApiService,
    private sanitizer: DomSanitizer
  ) {
    
  }
  viewChangeManagementDataById() {
  }
  PlantNames = [
    { id: "Nasik", value: "Nasik" },
    { id: "Haridwar", value: "Haridwar" },
    { id: "Kandivli", value: "Kandivli" },
    { id: "Igatpuri", value: "Igatpuri" },
    { id: "Zaheerabad", value: "Zaheerabad" },
    { id: "MVML", value: "MVML" },
    { id: "SEL1", value: "SEL1" },
  ];
  // Plants  = [ {"id": "3","value": "Kandivli"},
  // {"id": "4","value": "Igatpuri"}]
  ngOnInit() {
    console.log(this.CategoryName)
    this.RoleID = localStorage.getItem("WTIwNWMxcFZiR3M9");
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(
      CryptoJS.enc.Utf8
    );
    if (this.RoleID.includes("11")) this.changeManagementRole = 11;
    this.username = localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ==");
    this.username = CryptoJS.AES.decrypt(this.username, "").toString(
      CryptoJS.enc.Utf8
    );
    this.supplierCode = localStorage.getItem(
      "WkcxV2RWcEhPWGxSTWpscldsRTlQUT09"
    );
    this.supplierCode = CryptoJS.AES.decrypt(this.supplierCode, "").toString(
      CryptoJS.enc.Utf8
    );
    console.log(
      "----->>>" + JSON.stringify(this.passedData) + "---->",
      this.flag
    );
    let reqjson = {
      Id: this.passedData.id,
    };
    this.myBusinessService.ViewChangeManagementDataById(reqjson).subscribe(
      (resp: any) => {
        console.log(resp)
        this.CMRData = resp.ResponseData[0];
        if (this.CMRData.Remark == null) this.remarks = "";
        else this.remarks = this.CMRData.Remark;
      },
      (err) => {}
    );

    // this.PlantNames.forEach(a1Obj => {
    //   this.Plants.some(a2Obj => {
    //     if (a2Obj.id === a1Obj.id) {
    //       console.log(a1Obj);
    //       this.commonArray.push(a1Obj);
    //     }
    //   })
    // });
  }

  openAttachment() {
    if(this.CMRData.FileName){
      if(this.CMRData.FileData){
        const data = "data:"+this.CMRData.FileContentType+";base64,"+this.CMRData.FileData;
        fs.saveAs(data,this.CMRData.FileName+this.CMRData.FileExtension)
      } else {
        window.open(this.commonAPI.baseUrl1+"/MSetuDocuments/" + this.CMRData.FileName+this.CMRData.FileExtension, "_blank");
      }
    }
  }

  changeStatus(actionType) {
    let reqjson = {
      approve_Reject_By: this.supplierCode,
      ids: this.CMRData.id,
      actionType: actionType,
      remark: this.remarks,
    };
    this.myBusinessService.changeStatusAdminReport(reqjson).subscribe(
      (resp: any) => {
        this.toastr.success(resp.Message);
        this.closeModal();
      },
      (err) => {}
    );
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  // isCommonElt(id) {
  //   return this.commonArray.some(elt => elt.id === id);
  //   console.log(this.commonArray);
  // }
}
