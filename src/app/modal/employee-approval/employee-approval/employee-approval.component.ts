import {
  Component,
  OnInit,
  Input,
  forwardRef,
  ViewChild,
  ContentChild,
} from "@angular/core";
import { MySuggestionService } from "src/app/services/MySuggestion/my-suggestion.service";
import {
  NgbModal,
  NgbModalRef,
  NgbActiveModal,
} from "@ng-bootstrap/ng-bootstrap";
import { DailoughBoxComponent } from "../../../modal/dailough-box/dailough-box.component";
import { MatDialog } from "@angular/material";
import { VendorsComponent } from "../../../vendors/vendors.component";

@Component({
  selector: "app-employee-approval",
  templateUrl: "./employee-approval.component.html",
  styleUrls: ["./employee-approval.component.scss"],
})
export class EmployeeApprovalComponent {
  dataSource: any = [];

  displayedColumns: string[] = [
    "SupplierCode",
    "FirstName",
    "LastName",
    "Phone",
    "Email",
  ];
  @Input() UserName;

  constructor(
    public activeModal: NgbActiveModal,
    public cs: MySuggestionService,
    public dialog: MatDialog,
    public modal: NgbModal
  ) {}

  // @ViewChild(forwardRef(() =>VendorsComponent)) VendorsComponent: VendorsComponent;

  ngOnInit() {
    let postData = {
      UserName: this.UserName,
    };
    this.cs
      .postMySuggestionData("VendorEmployee/VendorDetailsByUserName", postData)
      .subscribe((res: any) => {
        this.dataSource = res.ResponseData;
      });
  }

  openDialog(element, value): void {
    element.status = value;
    const dialogRef = this.dialog.open(DailoughBoxComponent, {
      width: "300px",
      height: "130px",
      data: {
        messege:
          value == "R"
            ? "Are you sure want to reject this record?"
            : "Are you sure want to approve this record?",
        element: element,
      },
    });

    dialogRef.afterClosed().subscribe((data) => {
      console.log(data);
      let body = {
        VendorEmployeeIds: data.ID,
        Action: data.status,
      };
      this.cs.postMySuggestionData("Admin/ApproveOrRejectVendorEmployee", body).subscribe((res) => {
        console.log(res);
      });
      this.activeModal.close(data);
    });
  }

  close() {
    this.activeModal.close();
  }

  closeModal() {
    this.modal.dismissAll();
  }
}
