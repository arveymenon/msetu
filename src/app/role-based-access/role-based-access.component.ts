import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import * as XLSX from 'xlsx';
// import { IDropdownSettings } from 'ng-multiselect-dropdown';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-role-based-access',
  templateUrl: './role-based-access.component.html',
  styleUrls: ['./role-based-access.component.scss']
})
export class RoleBasedAccessComponent implements OnInit {
  Role_Master_Form:any;
  displayedColumns: string[] =['Role ID','Role Description','IsAdmin','CreatedBy','IsActive'];
  Inactive:boolean=true;
  rolebase_access_ddl='';
  banner_name:any="Role Master";
  Navigation_Form: any;
  updateMenuForm:any;
  mainMenuAccess_Form: any;
  isActiveNavigation:boolean=false;
  Roles_Navigation=[];
  excel_headers = [];
  uploaded_by_Excel: boolean = false;
  Excel_File: any = [];
  activeRoles=[];
  dropdownSettings: any= {};
  Navigation_Menu_Data=[];
  Menu_Navigation=[];
  Sub_Menu_Navigation=[];
  @ViewChild('form')form;
  @ViewChild('Navigationform')Navigationform;
  uploaded_by_Form: boolean = true;
  uploaded_by_Excel_Navigation: boolean=false;
  uploaded_by_Form_Navigation: boolean=true;
  RoleBased_Menu_Data=[];
  description_data=[];
  numbers_only: boolean;
  Report_Navigation=[];
  Select_Navigation=[];
  username: string;

  constructor(public cs: MySuggestionService,public modalService: NgbModal,private _formBuilder: FormBuilder, public commonService: CommonUtilityService,private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon('viewIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/View_Icon_Red.svg'));
    matIconRegistry.addSvgIcon('excelIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/Excel_White_Icon.svg'));
}

dropdownList = [];

  ngOnInit() {
    this.username = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
    this.username = CryptoJS.AES.decrypt(this.username,"").toString(CryptoJS.enc.Utf8);
    this.createRoleForm();
    this.createNavigationForm();
    this.createmainMenuAccessForm();
    this.getAllNavigationMenuData('');
    this.getAccessMenuData();
    this.getMenuForNavigation();
    this.getActiveRoleForNavigation();
    this.getAllActiveRolesMaster();
    
    this.dropdownSettings={
      singleSelection: false,
      idField: 'NavigationId',
      textField: 'NavigationName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      // allowSearchFilter: true
    }
  }

  createRoleForm(){
    this.Role_Master_Form = this._formBuilder.group({
      Add_Role: ['', Validators.required],
      Role_Description: ['', Validators.required],
      Bulk_Upload: [{ value: '', disabled: true }, Validators.required]
    });
  }
  
  createNavigationForm(){
    this.Navigation_Form = this._formBuilder.group({
      Menu: ['', Validators.required],
      Sub_Menu: [''],
      Select: [''],
      Report: [''],
      Role: ['', Validators.required],
      Bulk_Upload: [{ value: '', disabled: true }]
    });
  }
  createmainMenuAccessForm(){
    this.mainMenuAccess_Form = this._formBuilder.group({
      Menu: [''],
      Sub_Menu: [''],
      menuType:['Main Menu'],
      Select: [''],
      Report: [''],
      Role: [''],
      navigationName : ['',Validators.required],
      navigationLink : ['',Validators.required],
      navigationDesc :[''],
      navigationICon : [''],
      // Bulk_Upload: [{ value: '', disabled: true }]
    });
    this.menuType ='Main Menu';
    this.mainMenuAccess_Form.get("menuType").setValue(this.menuType);
  }
  
  selectedItems1=[];
  selectedItems=[];
  onItemSelect(item: any) {
    console.log(item);
    this.selectedItems1.push(item.NavigationId);
  }
  onSelectAll(items: any) {
    items.forEach((x)=>{this.selectedItems1.push(x.NavigationId)})
  }

  lettersOnly(event){
        const charCode = (event.which) ? event.which : event.keyCode;
        if(!(charCode >= 65 && charCode <= 122) && (charCode != 32 && charCode != 0)) { 
          this.cs.showError("Please enter alphabets only");
          event.preventDefault(); 
      }
 } 

 
 mainMenuFlag :any;
 mainNavFlag :boolean = true;
 menuType: any;
 typeList: string[] = ['Main Menu', 'Sub menu'];
 radioChange(flag) {
   this.menuType = flag;
  this.mainNavFlag = !flag;
 }
  open(content,flag) {
    this.Role_Master_Form.reset();
    this.Navigation_Form.reset();
    this.mainMenuAccess_Form.reset();
    this.selectedItems1=[];
    this.selectedItems=[];
    if(flag == 'mainMenuAccess'){
    //  this.mainNavFlag = true;
    this.menuType = '';
     this.mainMenuFlag = 'mainMenuAccess'; }
     else if(flag == 'Navigationcontent') {
     this.mainMenuFlag = 'Navigationcontent';
     }
    //  alert(this.mainMenuFlag);
    this.modalService.open(content, {  size: 'xl' as 'lg',centered:true, windowClass : "formModal"});
  }
  NavigationId:any;
  openUpdateModal(data,content) {
    this.NavigationId = data.NavigationId;
      this.updateMenuForm = this._formBuilder.group({
        Menu      : ['', Validators.required],
        Menu_desc : [''],
        path      : ['',Validators.required],
      });
      this.updateMenuForm.get("Menu").setValue(data.NavigationName);
      this.updateMenuForm.get("Menu_desc").setValue(data.NavigationDescription);
      this.updateMenuForm.get("path").setValue(data.NavigationURL);
    this.modalService.open(content, {  size: 'xl' as 'lg',centered:true, windowClass : "formModal"});
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  tabClick(ev:any){
    this.selectedItems1=[];
    this.selectedItems=[];
   this.banner_name=ev.tab.textLabel;
   this.getActiveRoleForNavigation();
   if(ev.index==0)
   this.getAllActiveRolesMaster();
   else if(ev.index==1){
    this.getAllNavigationMenuData('');
   }
   else{
     this.rolebase_access_ddl = '';
     this.getAllRoleBaseData('');
   }
  }

  getAllActiveRolesMaster(){
    this.cs.postMySuggestionData('CRUDMasterRole/GetMasterRole', '').
    subscribe(
      (res: any) => {
        this.cs.showSwapLoader = false;
        this.activeRoles = res.ResponseData;
        this.activeRoles = [...this.activeRoles];
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    )

  }

  ActiveInactiveRole(data,btn){
    let postData = {
      "RoleId": data.RoleId,
      "CreatedBy": data.CreatedBy
    }
    let url = btn.innerText=="Inactive"?'CRUDMasterRole/DeleteMasterRole':'CRUDMasterRole/ActivateMasterRole'
    this.cs.postMySuggestionData(url, postData).
    subscribe(
      (res: any) => {
        this.cs.showSwapLoader = false;
        this.cs.showSuccess('Role '+(btn.innerText=="Inactive"?'Inactivated ':'Activated ')+ 'Successfully');
        this.getAllActiveRolesMaster();
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    )

  }

  getAllNavigationMenuData(value){
    let Request={
      "RoleId":value
    }
    this.cs.postMySuggestionData('MenuAdmin/GetAllMenuMappingDetails', Request).
    subscribe(
      (res: any) => {
        this.cs.showSwapLoader = false;
        this.Navigation_Menu_Data = res.ResponseData;
        this.Navigation_Menu_Data = [...this.Navigation_Menu_Data];
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    )
  }
  Navigation_Menu_Data1:any=[];
  getAccessMenuData(){
    let Request={
      "PageNo":1
    }
    this.cs.postMySuggestionData('CRUDMasterNavigation/GetAllNavigationList', Request).
    subscribe(
      (res: any) => {
        this.cs.showSwapLoader = false;
        this.Navigation_Menu_Data1 = res.ResponseData;
        // this.Navigation_Menu_Data = [...this.Navigation_Menu_Data];
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    )
  }

  getActiveRoleForNavigation(){
    this.cs.postMySuggestionData('CRUDMasterRole/GetRoleDDL', '').
    subscribe(
      (res: any) => {
        this.Roles_Navigation = res.ResponseData;
      },
      (error) => {
        console.log(error.message);
      }
    )
  }

  getAllRoleBaseData(id){
    let postData={
      "RoleId":id
      }
    this.cs.postMySuggestionData('Admin/GetRoles', postData).
    subscribe(
      (res: any) => {
        this.cs.showSwapLoader = false;
        this.RoleBased_Menu_Data = res.ResponseData;
        this.RoleBased_Menu_Data = [...this.RoleBased_Menu_Data];
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    )
  }

  getMenuForNavigation(){
    this.cs.postMySuggestionData('MenuAdmin/GetParentMenu', '').
    subscribe(
      (res: any) => {
        this.Menu_Navigation = res.ResponseData;
      },
      (error) => {
        console.log(error.message);
      }
    )
  }

  getSubMenuForNavigation(){
    this.Report_Navigation=[];
    this.Select_Navigation = [];
    let postData = {
      "ParentId": this.Navigation_Form.controls.Menu.value
    }
    this.cs.postMySuggestionData('MenuAdmin/GetSubMenuByParentId', postData).
    subscribe(
      (res: any) => {
        this.Sub_Menu_Navigation = res.ResponseData;
        this.isNavigationFormvalid();
      },
      (error) => {
        console.log(error.message);
      }
    )
  }
  getSubMenuForNavigation1(){
    this.Report_Navigation=[];
    this.Select_Navigation = [];
    let postData = {
      "ParentId": this.mainMenuAccess_Form.controls.Menu.value
    }
    this.cs.postMySuggestionData('MenuAdmin/GetSubMenuByParentId', postData).
    subscribe(
      (res: any) => {
        this.Sub_Menu_Navigation = res.ResponseData;
        this.isNavigationFormvalid();
      },
      (error) => {
        console.log(error.message);
      }
    )
  }
  getReportForNavigation1(){
    this.Select_Navigation = [];
    this.Report_Navigation=[];
    let postData = {
      "ParentId": this.mainMenuAccess_Form.controls.Sub_Menu.value
    }
    this.cs.postMySuggestionData('MenuAdmin/GetSubMenuByParentId', postData).
    subscribe(
      (res: any) => {
        this.Report_Navigation = res.ResponseData;
        this.isNavigationFormvalid();
      },
      (error) => {
        console.log(error.message);
      }
    )
  }
  getReportForNavigation(){
    this.Select_Navigation = [];
    this.Report_Navigation=[];
    let postData = {
      "ParentId": this.Navigation_Form.controls.Sub_Menu.value
    }
    this.cs.postMySuggestionData('MenuAdmin/GetSubMenuByParentId', postData).
    subscribe(
      (res: any) => {
        this.Report_Navigation = res.ResponseData;
        this.isNavigationFormvalid();
      },
      (error) => {
        console.log(error.message);
      }
    )
  }

  getSelectMenuForNavigation(){
    this.Select_Navigation=[];
    let postData = {
      "ParentId": this.Navigation_Form.controls.Report.value
    }
    this.cs.postMySuggestionData('MenuAdmin/GetSubMenuByParentId', postData).
    subscribe(
      (res: any) => {
        this.Select_Navigation = res.ResponseData;
        this.isNavigationFormvalid();
      },
      (error) => {
        console.log(error.message);
      }
    )
  }
  
  ActiveInActiveNavigationRole(data,isActive){
    let postData = {
      "RoleNavigationId":data.RoleNavigationId,
      "IsActive": isActive,
      "CreatedBy":this.username
    }
    this.cs.postMySuggestionData('MenuAdmin/UpdateRoleMenuStatus', postData).
    subscribe(
      (res: any) => {
        this.cs.showSwapLoader = false;
        // this.isActiveNavigation=btn.innerText=="Inactive"?this.isActiveNavigation=true:this.isActiveNavigation=false;
        // btn.style.backgroundColor=btn.innerText=="Inactive"?"green":"#FF8181";
        // btn.innerText=btn.innerText=="Inactive"?"Active":"Inactive";
        // this.getAllActiveRolesMaster();
        this.cs.showSuccess('Navigation for role '+(isActive?'activated ':'inactivated ')+ 'successfully');
        this.getAllNavigationMenuData('');
    },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    )

  }

  isFormvalid() {
    if (this.Role_Master_Form.valid) {
      this.uploaded_by_Excel = false;
      this.Excel_File = [];
      this.Role_Master_Form.get('Bulk_Upload').setValue('');
      this.uploaded_by_Form = true;
    }
  }

  isNavigationFormvalid() {
    if (this.Navigation_Form.valid) {
      this.uploaded_by_Excel_Navigation = false;
      this.Excel_File = [];
      this.Navigation_Form.get('Bulk_Upload').setValue('');
      this.uploaded_by_Form_Navigation = true;
    }
  }

  detectFiles(evt,form,id) {
    form.resetForm();
    if(id==1)
    this.excel_headers=["RoleName","RoleDescription"];
    else
    this.excel_headers=["Role","Menu"];
    let target: any = <any>(evt.target);
    var headers = [];
    for (let file of target.files) {
      let ext = file.name.split(".").pop().toLowerCase();
      let reader = new FileReader();
      reader.onload = (e: any) => {
        if (ext != 'xlsx') {
          this.cs.showError("Kindly select file with extension '.xlsx");
          return false;
        }
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        var range = XLSX.utils.decode_range(ws['!ref']);
        var C, R = range.s.r;
        let excel_headers = this.excel_headers;
        excel_headers = excel_headers.map(v => v.toLowerCase().replace(/ /g, ""));
        for (C = range.s.c; C <= range.e.c; ++C) {
          var cell = ws[XLSX.utils.encode_cell({ c: C, r: R })]
          var hdr = "UNKNOWN " + C;
          if (cell && cell.t && excel_headers.includes(cell.v.toLowerCase().replace(/ /g, "")))
            hdr = XLSX.utils.format_cell(cell);
          else {
            this.cs.showError("Kindly refer/used sample template and try to upload")
            return false;
          }
          // headers.push(hdr.toLowerCase().replace(/ /g, ""));
          headers.push(hdr);
        }
        this.Upload(target.files[0], evt,headers,form,id)
      }
      reader.readAsBinaryString(target.files[0]);
    }
  }

  Upload(data, evt,headers,form,id) {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      let arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
      let array = [];
      array = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      if (array.length == 0)
        return this.cs.showError("Kindly fill data in excel");
      else
        this.validateExcel(array, evt,headers,form,id);
    }
    fileReader.readAsArrayBuffer(data);
  }

  validateExcel(array, evt,headers,form,id) {
    for (let i = 0; i < this.excel_headers.length; i++) {
      for (let j = 0; j < array.length; j++) {
        if (this.cs.isUndefinedORNull(array[j][headers[i]])) {
          this.cs.showError("Please enter values under " + this.excel_headers[i] + " at line number " + (j + 2));
          return false;
        }
        // if(!this.cs.isUndefinedORNull(array[j][headers[i]])&&headers[i]=="Mobile Number"){
        //   if(array[j][headers[i]].includes(/^[^a-zA-Z]$/)){
        //     alert("Please enter only numbers under " + this.excel_headers[i] + " at line number " + (j + 2));
        //     return false;
        //   }
        // }
      }
    }
    this.Excel_File = [];
    form.resetForm();
    if(id==1){
      this.uploaded_by_Excel = true;
      this.uploaded_by_Form = false;
      this.uploaded_by_Excel_Navigation=false;
      this.uploaded_by_Form_Navigation=true;
    }else{
      this.uploaded_by_Excel_Navigation = true;
      this.uploaded_by_Form_Navigation = false;
      this.uploaded_by_Excel = false;
      this.uploaded_by_Form = true;
    }
    
    // form.get('Bulk_Upload').setValue(evt.target.files[0].name);
    form.control.get('Bulk_Upload').setValue(evt.target.files[0].name)
    this.Excel_File.push(evt.target.files[0]);
  }

  getRoleByIdDescription(data,RoleDescription){
    this.open(RoleDescription,'');
    let postData = {
      "RoleId": data.RoleId
    }
    this.cs.postMySuggestionData('Admin/GetMenuByRoleId', postData).
      subscribe(
        (res: any) => {
          this.cs.showSwapLoader = false;
          this.description_data = res.ResponseData;
          this.description_data = [...this.description_data];
          // this.open(RoleDescription);
        },
        (error) => {
          this.cs.showSwapLoader = false;
          console.log(error.message);
        }
      )
  }

  DownloadSampleTemplate(filename:any){
    window.open(`${'../../assets/'}${filename}${'.xlsx'}`, '_blank');
    // window.open('../../assets/Navigation Sample.xlsx')
  }
  validateAllFormFields(formGroup){
          Object.keys(formGroup.controls).forEach(controlName =>{
            const control = formGroup.get(controlName);
            if(control instanceof FormControl){
              control.markAsTouched({onlySelf: true});
            }
            else if(control instanceof FormControl || control instanceof FormArray){
            this.validateAllFormFields(control);
            }
          });
        }  

  InsertRoleData(form: any,closeModalonclick) {
    let formData;
    let url;
    if (!this.Role_Master_Form.valid && !this.uploaded_by_Excel) {
      this.validateAllFormFields(this.Role_Master_Form);
      return;
    }

    else if (this.uploaded_by_Form && !this.uploaded_by_Excel){
      url='CRUDMasterRole/InsertMasterRole';
      // alert("Added by Form");
      formData = {
        "RoleName":this.Role_Master_Form.controls.Add_Role.value,
        "RoleDescription":this.Role_Master_Form.controls.Role_Description.value,
        "CreatedBy":this.username// data from login api
      }
    }
    else if (!this.uploaded_by_Form && this.uploaded_by_Excel) { 
      url='CRUDMasterRole/BulkUploadRoles';   
      // alert("Added by Excel");
      formData = new FormData;
      formData.append('ExcelFile', this.Excel_File[0]);
      formData.append('CreatedBy', this.username);// need to add from login api
      // formData.append('VendorCode', "DM181");
    }// need to add from login api
    else {
      return;
    }
      this.cs.postMySuggestionData(url, formData).
        subscribe(
          (res: any) => {
            this.cs.showSwapLoader = false;
            this.cs.showSuccess(res.Message);
            if(closeModalonclick)
            closeModalonclick._elementRef.nativeElement.click();
            form.resetForm();
            this.uploaded_by_Excel=false;
            this.getAllActiveRolesMaster();
          },
          (error) => {
            this.cs.showSwapLoader = false;
            console.log(error.message);
          }
        )
  }

  navigationDesc:any='';
  
deleteMenuByID(id) {
  let reqjson={
    "NavigationId":id,
    "CreatedBy":""
  }
  this.cs.postMySuggestionData('CRUDMasterNavigation/DeleteMasterNavigation', reqjson).
  subscribe(
    (res: any) => {
      this.cs.showSuccess("Successfully deleted");
      this.getAccessMenuData();
    },
    (error) => {
      this.cs.showSwapLoader = false;
      console.log(error.message);
    }
  )
}
updateMenu(form) {
if(this.updateMenuForm.valid){
  let reqjson ={
    "NavigationId"          : this.NavigationId,
    "NavigationName"        : this.updateMenuForm.get('Menu').value,
    "NavigationIcon"        : "",
    "NavigationURL"         : this.updateMenuForm.get('path').value,
    "NavigationDescription" : this.updateMenuForm.get('Menu_desc').value,
    "IsMainNavigation"      : "",
    "IsSubNavigation"       : "",
    "RoleId"                : "",
    "ParentId"              : "",
    "CreatedBy"             : "msetu",
    "IsSRMMenu"             : ""
    }
    this.cs.postMySuggestionData('CRUDMasterNavigation/UpdateMasterNavigation', reqjson).
    subscribe(
      (res: any) => {
        this.cs.showSuccess("Successfully updated");
        this.closeModal();
        this.getAccessMenuData();
      // alert("success")
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    )
}
else{
  this.validateAllFormFields(this.updateMenuForm);
  return;
}

}
  addNavigation(form:any,closeNavigation,name,url) {
    // let formData;
    // let navigationName;
    // let subNavigationName;
    let parentID;
    let reqjson;

    // for(let i = 0;i<this.Menu_Navigation.length;i++){
    //   if(this.mainMenuAccess_Form.get('Menu').value == this.Menu_Navigation[i].NavigationId){
    //     navigationName = this.Menu_Navigation[i].NavigationName;
    //   }
    // }
    // for(let i = 0;i<this.Sub_Menu_Navigation.length;i++){
    //   if(this.mainMenuAccess_Form.get('Sub_Menu').value == this.Menu_Navigation[i].NavigationId){
    //     subNavigationName = this.Menu_Navigation[i].NavigationName;
    //   }
    // }
    if (!this.mainMenuAccess_Form.valid) {
      this.validateAllFormFields(this.mainMenuAccess_Form);
      return;
    }
    else if (this.mainMenuAccess_Form.get('navigationName').value == '' || this.mainMenuAccess_Form.get('navigationLink').value == ''){
      this.cs.showError("Please enter all mandantory fields");
    }
    else {
      
      if(this.menuType =='Main Menu'){
       reqjson ={
        "NavigationId":0,
        "NavigationName"  :this.mainMenuAccess_Form.get('navigationName').value,
        "NavigationIcon"  :this.mainMenuAccess_Form.get('navigationICon').value,
        "NavigationURL"   :this.mainMenuAccess_Form.get('navigationLink').value,
        "NavigationDescription":this.mainMenuAccess_Form.get('navigationDesc').value,

        "IsMainNavigation":1,
        "IsSubNavigation":0,
        "RoleId":this.mainMenuAccess_Form.get('Role').value,
        "ParentId":0,
        "CreatedBy":"msetu",
        "IsSRMMenu":0
        }
        this.cs.postMySuggestionData('CRUDMasterNavigation/InsertMasterNavigation', reqjson).
        subscribe(
          (res: any) => {
            this.cs.showSuccess("Successfully added");
            this.closeModal();
            this.getAccessMenuData();
          // alert("success")
          },
          (error) => {
            this.cs.showSwapLoader = false;
            console.log(error.message);
          }
        )
      }
      else {
// alert(this.mainMenuAccess_Form.get('Menu').value+"--"+this.mainMenuAccess_Form.get('Sub_Menu').value)
        if(this.mainMenuAccess_Form.get('Menu').value == undefined || this.mainMenuAccess_Form.get('Menu').value == ''){
          parentID = 0;
        }
        else {
            if(this.mainMenuAccess_Form.get('Sub_Menu').value == undefined || this.mainMenuAccess_Form.get('Sub_Menu').value == ''){
              parentID =this.mainMenuAccess_Form.get('Menu').value;
            }
             else{
              if(this.mainMenuAccess_Form.get('Report').value == undefined || this.mainMenuAccess_Form.get('Report').value =='')
                parentID =this.mainMenuAccess_Form.get('Sub_Menu').value; 
              else
              parentID =this.mainMenuAccess_Form.get('Report').value; 
             } 
            }
         
      // alert(parentID);
         reqjson ={
          "NavigationId":0,
          "NavigationName"  :this.mainMenuAccess_Form.get('navigationName').value,
          "NavigationIcon"  :this.mainMenuAccess_Form.get('navigationICon').value,
          "NavigationURL"   :this.mainMenuAccess_Form.get('navigationLink').value,
          "NavigationDescription":this.mainMenuAccess_Form.get('navigationDesc').value,
  
          "IsMainNavigation":0,
          "IsSubNavigation":1,
          "RoleId":this.mainMenuAccess_Form.get('Role').value,
          "ParentId":parentID,
          "CreatedBy":"msetu",
          "IsSRMMenu":0
          }
      }
        this.cs.postMySuggestionData('CRUDMasterNavigation/InsertMasterNavigation', reqjson).
        subscribe(
          (res: any) => {
            this.cs.showSuccess("Successfully added");
            this.closeModal();
            this.getAccessMenuData();
          // alert("success")
          },
          (error) => {
            this.cs.showSwapLoader = false;
            console.log(error.message);
          }
        )
        }

    }

  AssigenMenuRoleWise(form:any,closeNavigation){
     let formData;
    let url;
    if (!this.Navigation_Form.valid && !this.uploaded_by_Excel_Navigation) {
      this.validateAllFormFields(this.Navigation_Form);
      return;
    }
    else if (this.uploaded_by_Form_Navigation && !this.uploaded_by_Excel_Navigation){
      url='MenuAdmin/AssignMenuRoleWise';
      let Navigation_ID=[];
      let join_ID=[];
      Navigation_ID.push(this.Navigation_Form.controls.Menu.value,this.Navigation_Form.controls.Sub_Menu.value
        ,this.Navigation_Form.controls.Select.value,this.Navigation_Form.controls.Report.value);
        Navigation_ID.filter(x => {
          if(!this.cs.isUndefinedORNull(x)){
            join_ID.push(x);
          }
        });
        join_ID.push(this.selectedItems1)
      formData={
        "RoleId":this.Navigation_Form.controls.Role.value,
        "NavigationId":join_ID.join(','),
        "CreatedBy":this.username
}
    }
    else if (!this.uploaded_by_Form_Navigation && this.uploaded_by_Excel_Navigation) { 
      url='MenuAdmin/BulkUploadRolesMenu';
      // alert("Excel");
      formData = new FormData;
      formData.append('ExcelFile', this.Excel_File[0]);
      formData.append('CreatedBy', this.username);
    }
    else {
      return;
    }
    this.cs.postMySuggestionData(url, formData).
        subscribe(
          (res: any) => {
            this.cs.showSwapLoader = false;
            if(!this.cs.isUndefinedORNull(res.ResponseData)){
              let assignMenu = res.ResponseData.split(',');
              if(assignMenu[assignMenu.length-1]=="NO"){
                this.cs.showError("Menu is already assigned");
                this.getAllNavigationMenuData('');
                form.resetForm();
                this.selectedItems=[];
                return;
              }
              else{
                if (closeNavigation)
                  closeNavigation._elementRef.nativeElement.click();
                form.resetForm();
                this.uploaded_by_Excel = false;
                if (!this.cs.isUndefinedORNull(res.ResponseData[0].Response)) {
                  this.cs.showSuccess(res.ResponseData[0].Response);
                  return;
                } else {
                  this.cs.showSuccess(res.Message);
                  this.getAllNavigationMenuData('');
                }
              }
            }else{
              form.resetForm();
              this.cs.showError("Please enter valid values in form/excel");
              return;
            }
          },
          (error) => {
            this.cs.showSwapLoader = false;
            console.log(error.message);
          }
        )
  }



}
