import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleBasedAccessComponent } from './role-based-access.component';

describe('RoleBasedAccessComponent', () => {
  let component: RoleBasedAccessComponent;
  let fixture: ComponentFixture<RoleBasedAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleBasedAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleBasedAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
