import { Component, OnInit, ViewChild,TemplateRef,ElementRef } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { Router } from '@angular/router';
import { CMSSurveyComponent } from '../cmssurvey/cmssurvey.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { CreateCMSSurveyComponent } from '../create-cmssurvey/create-cmssurvey.component';
import { DashboardService } from '../services/S_dashboard/dashboard.service';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {

  updateList: any;
  today: Date = new Date();
  underMaintainanceModuleList :any=[];

  constructor(private dashboardService: DashboardService, public datepipe: DatePipe, private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer, public commonService: CommonUtilityService, private router: Router,
     private modalService: NgbModal,
     public cs: MySuggestionService
) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon('arrowIcon', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Iconarrow.svg'));
    matIconRegistry.addSvgIcon('LandingPage', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Icon1.svg'));
    matIconRegistry.addSvgIcon('Dashboard', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Icon2.svg'));
    matIconRegistry.addSvgIcon('create', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Icon3.svg'));
    matIconRegistry.addSvgIcon('role', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Icon4.svg'));
    matIconRegistry.addSvgIcon('survey', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Icon5.svg'));
    matIconRegistry.addSvgIcon('update', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Icon6.svg'));
    matIconRegistry.addSvgIcon('document', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Icon7.svg'));
    matIconRegistry.addSvgIcon('fileTransfer', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Icon8.svg'));
    matIconRegistry.addSvgIcon('supplier', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Icon9.svg'));
    matIconRegistry.addSvgIcon('analytics', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Icon10.svg'));
    matIconRegistry.addSvgIcon('config', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Icon11.svg'));
    matIconRegistry.addSvgIcon('supplierMeet', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Supplier-Meet-CMS.svg'));
    matIconRegistry.addSvgIcon(
      "warningIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/suplierDashboard/warning.svg"
      )
    );
    this.getDashboardContent();
  }
  getDashboardContent() {
    let latest_date = this.datepipe.transform(this.today, 'yyyy-MM-dd');
    // let latest_date='2020-02-25';
    //  alert(latest_date);
    let postData = {
      "TodaysDate": latest_date
    }
    this.dashboardService.getNewUpdates(postData).subscribe((resp: any) => {
      console.log(resp);

      this.updateList = resp.ResponseData._GetCMSUpdates;
    }, (err) => {

    });
  }
  cardDetails = [
    { header: 'Landing Page', subHeader: 'Content management', triangle: 'dashboardTriangle1', Icon: 'LandingPage' },
    { header: 'Dashboard', subHeader: 'Content management', triangle: 'dashboardTriangle2', Icon: 'Dashboard' },
    { header: 'Create', subHeader: 'User Profile', triangle: 'dashboardTriangle3', Icon: 'create' },
    { header: 'Role', subHeader: 'Based Access', triangle: 'dashboardTriangle4', Icon: 'role' },
    { header: 'Survey', subHeader: 'Management', triangle: 'dashboardTriangle1', Icon: 'survey' },
    { header: 'Email', subHeader: 'Mass Mailer', triangle: 'dashboardTriangle2', Icon: 'update' },
    { header: 'Document', subHeader: 'Management', triangle: 'dashboardTriangle3', Icon: 'document' },
    // { header: 'File Transfer', subHeader: 'Access', triangle: 'dashboardTriangle4', Icon: 'fileTransfer' },
    // { header: 'Supplier', subHeader: 'Rating Management', triangle: 'dashboardTriangle1', Icon: 'supplier' },
    { header: 'Analytics', subHeader: 'Dashboard', triangle: 'dashboardTriangle2', Icon: 'analytics' },
    { header: 'Code Of Conduct List', subHeader: 'Management', triangle: 'dashboardTriangle3', Icon: 'config' },
    { header: 'Supplier Meet CMS', subHeader: 'Content Management', triangle: 'dashboardTriangle4', Icon: 'supplierMeet' },
    { header: 'Performance Rating', subHeader: 'Management', triangle: 'dashboardTriangle4', Icon: 'supplierMeet' },
    { header: 'User', subHeader: 'Details Management', triangle: 'dashboardTriangle4', Icon: 'supplierMeet' },
  ]
  ngOnInit() {
    let maintainanceFlag = localStorage.getItem("maintainanceFlag");
    if(maintainanceFlag == 'false'){
      this.checkUnderMaintainanceModule('');
    }
  }
  goToCMSDashboard(cardHeader) {
    if (cardHeader == 'Dashboard') {
      this.router.navigateByUrl('CMSDashboard');
    }
    else if (cardHeader == 'Landing Page')
      this.router.navigateByUrl('CMSLanding');
    else if (cardHeader == 'Document')
      this.router.navigateByUrl('CMSDocument');
    else if (cardHeader == 'Survey')
      this.router.navigateByUrl('CMSSurvey');
    else if (cardHeader == 'Role')
      this.router.navigateByUrl('RoleBasedAccess');
    else if (cardHeader == 'Create')
      this.router.navigateByUrl('vendors');
    else if (cardHeader == 'Supplier Meet CMS')
      this.router.navigateByUrl('SupplierMeet');
    else if (cardHeader == 'Email')
      this.router.navigateByUrl('emailpanel');
    else if (cardHeader == 'Code Of Conduct List')
      this.router.navigateByUrl('supplierCodeConductList');
    else if (cardHeader == 'Performance Rating')
      this.router.navigateByUrl('spm');
    else if (cardHeader == 'Analytics')
      this.router.navigateByUrl('usage-dashboard');
      else if (cardHeader == 'User')
      this.router.navigateByUrl('user-details');
  }
  @ViewChild('content') content: TemplateRef<any>;
  checkUnderMaintainanceModule(roldeID) { 
    let reqjson ={
      PageNo : 1,
      FromDate : "",
      ToDate: "",
      Type : 0
    }
    this.cs
    .postMySuggestionData("Admin/GetMaintenanceNotificationList",reqjson)
    .subscribe(async (response) => {
      // alert(response.ResponseData.length);
      this.underMaintainanceModuleList = response.ResponseData;
      // if(this.underMaintainanceModuleList.length > 0) {
        // this.openMaintainanceModal();
        if(this.underMaintainanceModuleList.length > 0) 
        this.modalService.open(this.content);
      // }
    }, (error) => {
      console.log(error.Message);
    });
  }
  closeMaintainanceModal() {
    localStorage.setItem("maintainanceFlag","true");
    this.modalService.dismissAll();
  }
  openMaintainanceModal() {
    this.modalService.open( {
      size: "lg",
      centered: true,
      windowClass: "formModal",
    });
  }
}
