import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatIconRegistry, MatStepper, DateAdapter,MAT_DATE_FORMATS } from '@angular/material';
// import { NativeDateAdapter, DateAdapter,  } from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS} from '../cmslanding/date-adapter'; 
import { DomSanitizer } from '@angular/platform-browser';
import { object } from '@amcharts/amcharts4/core';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { Observable, Observer } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import * as moment from 'moment';
import * as fs from "file-saver";
import { saveAs } from 'file-saver';
import * as CryptoJS from 'crypto-js';

import { CommonApiService } from '../services/common-api/common-api.service';
import { width } from '@amcharts/amcharts4/.internal/core/utils/Utils';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Workbook } from 'exceljs';
import { HttpClient } from '@angular/common/http';
// } from '@angular/http';

@Component({
  selector: 'app-my-suggestion',
  templateUrl: './my-suggestion.component.html',
  styleUrls: ['./my-suggestion.component.scss'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]
})
export class MySuggestionComponent implements OnInit {
  isLinear=true;
  isEditable=false;
  isCompleted=false;
  newDealForm:any;
  currentSituationForm:FormGroup;
  proposedSituationForm:FormGroup;
  submissionForm:FormGroup;
  roleId = CryptoJS.AES.decrypt(localStorage.getItem("PR"),"").toString(CryptoJS.enc.Utf8);
  // roleId = 7 user
  secondFormGroup:FormGroup;
  thirdFormGroup:FormGroup;
  forthFormGroup:FormGroup;
  nextStep:boolean=false;
  Add_File:any = '';
  Add_File_Current:any = '';
  Add_File_Proposed:any = '';
  @ViewChild('forthform')forthform:any;
  @ViewChild('proposalform')proposalform:any;
  @ViewChild('currentform')currentform:any;
  @ViewChild('newform')newform:any;
  stepper: MatStepper;
  modelAffected: any = [];
  workShopConductedBy: any = [];
  vehicleSystem: any = [];
  MMBenchMark: any = [];
  partToPlant: any = [];
  fileUrls: any = [];
  urls: any = [];
  sectorDivision: any = [];
  files_Of_Current: any = [];
  files_Of_Proposed: any = [];
  all_Files: any = [];
  new_Idea: boolean = false;
  navigation_From_Dashboard: string = "false";
  dash_Board_Data: any = [];
  isReadOnly: boolean = false;
  maxDate: any;
  divisionId: number;
  dash_Board_DataId: any;
  formStatus:any;
  ideano:any = 'ideano';
  isMatStepComplete:boolean=false;
  isAdmin: string='false';
  base64Image: string;
  attachemntChnage: boolean=false;
  CSCattachemntChnage: boolean=false;
  PSattachemntChnage: boolean=false;
  isDashboardFile:boolean=false;
  isDashboardPSIFile: boolean=false;
  isDashboardCSIFile: boolean=false;
  statusData=[];
  userDetails:any={};
  RoleID: any;

  constructor(private route: ActivatedRoute,public commonService: CommonUtilityService, private _formBuilder: FormBuilder,public cs1:CommonApiService
    ,private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer, private modalService: NgbModal, private http: HttpClient,
    public cs: MySuggestionService,public router:Router) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon('excelIcon', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/excel.svg'));
    matIconRegistry.addSvgIcon('arrowIcon', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Iconarrow.svg'));
  }

  vendorcode:any;
  username :any;
  supplierName :any;
  mobile:any;
  ngOnInit() {
    this.RoleID = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID,"").toString(CryptoJS.enc.Utf8);
    this.vendorcode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
    this.vendorcode = CryptoJS.AES.decrypt(this.vendorcode,"").toString(CryptoJS.enc.Utf8);
    // this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.username = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
    this.username = CryptoJS.AES.decrypt(this.username,"").toString(CryptoJS.enc.Utf8);
    // this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.supplierName = CryptoJS.AES.decrypt(localStorage.getItem('VlROV2QyTkhlSEJhV0VwUFdWY3hiQT09'),"").toString(CryptoJS.enc.Utf8);

    this.mobile = localStorage.getItem('VkZjNWFXRlhlR3hVYmxaMFdXMVdlUT09');
    this.mobile= CryptoJS.AES.decrypt(this.mobile,"").toString(CryptoJS.enc.Utf8); 


    this.maxDate = new Date();
    this.getStatusData();
    this.createNewDealForm();
    if(this.navigation_From_Dashboard == 'false'){
      let SupplierName = localStorage.getItem('VlROV2QyTkhlSEJhV0VwUFdWY3hiQT09');
      SupplierName = CryptoJS.AES.decrypt(SupplierName,"").toString(CryptoJS.enc.Utf8);
      let MobileNumber = localStorage.getItem('VkZjNWFXRlhlR3hVYmxaMFdXMVdlUT09');
      MobileNumber = CryptoJS.AES.decrypt(MobileNumber,"").toString(CryptoJS.enc.Utf8);
      let name = localStorage.getItem('WW0xR2RGcFJQVDA9');
      name = CryptoJS.AES.decrypt(name,"").toString(CryptoJS.enc.Utf8);
      let email = localStorage.getItem('V2xjeGFHRlhkejA9')
      email = CryptoJS.AES.decrypt(email,"").toString(CryptoJS.enc.Utf8);
      this.dash_Board_Data["SupplierName"]= SupplierName;//  localStorage.getItem("");
      this.dash_Board_Data["SupplierCode"]=this.vendorcode;
      this.dash_Board_Data["SupplierEmailId"]=email;
      this.dash_Board_Data["SupplierMobNo"]=MobileNumber;
      this.newDealForm.get('NameIdeaGenerator').setValue(name);
   //   this.newDealForm.controls['Admin_Status'].setValue();
    }
    this.route.queryParams.subscribe(params => {
      if(!this.cs.isUndefinedORNull(params["ID"])&&!this.cs.isUndefinedORNull(params["navigation_From_Dashboard"])&&!this.cs.isUndefinedORNull(params["Status"]) )
       {
        this.dash_Board_DataId = params["ID"];
        this.navigation_From_Dashboard = params["navigation_From_Dashboard"];
        this.formStatus = params["Status"]; 
        this.isAdmin = params['isAdmin'];
        this.ideano = params['IdeaNumber']
        if(this.navigation_From_Dashboard == 'true'){
          this.isDashboardFile=true;
          this.isDashboardPSIFile=true;
          this.isDashboardCSIFile=true;
          // this.dash_Board_Data["SupplierName"]= this.userDetails.SupplierName;//  localStorage.getItem("");
          // this.dash_Board_Data["SupplierCode"]=this.userDetails.VendorCode;
          // this.dash_Board_Data["SupplierEmailId"]=this.userDetails.EmailID;
          // this.dash_Board_Data["SupplierMobNo"]=this.userDetails.MobileNumber;
          // this.newDealForm.get('NameIdeaGenerator').setValue('this.userDetails.name')
        }
        if(this.formStatus==1 || this.isAdmin=='true')
        this.isReadOnly=true;
        else
        this.isReadOnly=false;
    }
  });
  
    this.createCurrentSituationForm();
    this.createProposedSituationForm();
    this.createFinalForm();
    this.getWorkShopConductedBy().subscribe(() => {
      this.getVehicleSystem().subscribe(() => {
        this.getSectorDivision().subscribe(() => {
          this.getPartSupplierToPlant().subscribe(() => {
            this.getBenchMark().subscribe(() => {
              if (!this.cs.isUndefinedORNull(this.dash_Board_DataId) && this.navigation_From_Dashboard == 'true') {
                this.getDashBoardDataById(this.dash_Board_DataId).then(()=>{
                  console.log(this.dash_Board_Data)
                  this.newDealForm.get('NameIdeaGenerator').setValue(this.dash_Board_Data.NameOfIdeaGenerator)
                  this.getModelAffected(this.dash_Board_Data.SectorOrMMDivision)
                  this.bindDataToForm(this.dash_Board_Data).then(() => {
                    this.isLinear=false;
                    this.cs.showSwapLoader = false;
                    this.isMatStepComplete=true;
                  });
                });
              }else{
                this.navigation_From_Dashboard='false';
              }
            })
          })
        })
      })
    })
  }


  getStatusData(){
    this.cs.postMySuggestionData('IMCRDashboard/GetStatusList','').subscribe((res:any)=>{
    this.statusData=res.ResponseData;
    })
  }

  getDashBoardDataById(data:any):Promise<any>{
    return new Promise((resolve:any)=>{
      let postData:any={
        "IdeaId":data
      }
       this.cs.postMySuggestionData('IMCRDashboard/GetIdeaDetails',postData).subscribe((res:any)=>{
         if(!this.cs.isUndefinedORNull(res)){
           this.dash_Board_Data = res.ResponseData[0];
           let attachments = res.ResponseData[0].AttachmentPath.split(',')
           console.log(attachments)
          if(attachments.length > 0){
           attachments.forEach(filePath=>{
             this.fileUrls.push({
               name: filePath.split('/').reverse()[0],
               filePath: filePath
             }) 
           })
          }else{
             this.fileUrls.push({
               name: attachments.split('/').reverse()[0],
               filePath: attachments
             }) 
          }
          console.log(this.fileUrls)
          //  this.dash_Board_Data[""]
          //  this.dash_Board_Data["SupplierName"]= res.ResponseData[0];//  localStorage.getItem("");
          //  this.dash_Board_Data["SupplierCode"]=this.vendorcode;
           this.dash_Board_Data["SupplierEmailId"]=res.ResponseData[0].SupplierEmailId;
           this.dash_Board_Data["SupplierMobNo"]=res.ResponseData[0].SupplierMobNo;
           resolve();
         }
       })
    })
    
  }

  bindDataToForm(data): Promise<any> {
    return new Promise((resolve: any, reject: any) => {
      var newDealAttachmentlast = data.AttachmentPath;

      var CurrentAttachmentlast=data.CSImageName;
      var ProposaltAttachmentlast=data.PSImageName;

      this.newDealForm.get('Supplier_Location').setValue(data.SupplierLocation);
      this.newDealForm.get('Conducted_By').setValue(data.WorkshopConductuedByMM);
      this.newDealForm.get('Date_of_Workshop').setValue(data.DateOfWorkshop);
      this.newDealForm.get('Date_of_IdeaPunching').setValue(data.DateOfIdeaPunching);
      this.newDealForm.get('Proposal_Description').setValue(data.ProposalDescription);
      this.newDealForm.get('Function_of_Component').setValue(data.BasicFunctionOfComponent);
      this.newDealForm.get('Vehicle_System').setValue(data.VehicleSystem);
      this.newDealForm.get('Add_File').setValue(newDealAttachmentlast);
      this.newDealForm.get('Sector').setValue(data.SectorOrMMDivision);
      this.newDealForm.get('Model_Affected').setValue(data.ModelAffected);
      this.newDealForm.get('Savings_As_Supplier').setValue(data.Saving_VehRs);
      this.newDealForm.get('Savings_As_MM').setValue(data.SV_Veh_MM);
      this.newDealForm.get('Investments_In_Lacks_As_Supplier').setValue(data.Investment_In_Lakh);
      this.newDealForm.get('Investments_In_Lacks_As_MM').setValue(data.Invest_MM);
      this.newDealForm.get('Monthly_Production').setValue(data.MonPrd);
      this.newDealForm.get('Parts_Suppliers_To_Plants').setValue(data.PSuppPlnt);
      this.newDealForm.get('Bench_Mark').setValue(data.MMBenchmark);
      this.newDealForm.get('Other_Bench_Mark').setValue(data.OtherThanBenchmark);
      //current situation form
      this.currentSituationForm.get('Part_Number').setValue(data.CSPartNo);
      this.currentSituationForm.get('Part_Description').setValue(data.CSPartDesc);
      this.currentSituationForm.get('Qty_Veh').setValue(data.CSOTY);
      this.currentSituationForm.get('Part_Cost').setValue(data.CSPartCost);
      this.currentSituationForm.get('Weight').setValue(data.WeightCS);
      this.currentSituationForm.get('RM_Grade').setValue(data.RMCS);
      this.currentSituationForm.get('Add_File').setValue(CurrentAttachmentlast);
      // proposed situation form
      this.proposedSituationForm.get('Part_Number').setValue(data.PSPartNo);
      this.proposedSituationForm.get('Part_Description').setValue(data.PSPartDesc);
      this.proposedSituationForm.get('Qty_Veh').setValue(data.PSOTY);
      this.proposedSituationForm.get('Part_Cost').setValue(data.PSPartCost);
      this.proposedSituationForm.get('Weight').setValue(data.WeightPS);
      this.proposedSituationForm.get('RM_Grade').setValue(data.RMPS);
      this.proposedSituationForm.get('Add_File').setValue(ProposaltAttachmentlast);
      // submission form
      this.submissionForm.get('Remark').setValue(data.SupllierRemark); 
      this.submissionForm.get('Admin_Status').setValue(data.AdminStatus);
      this.submissionForm.get('eVECP_Number').setValue(data.EVECP);
      this.submissionForm.get('Admin_Remark').setValue(data.Admin_Review);
      resolve();
    })

  }

  createNewDealForm() {
    this.newDealForm = this._formBuilder.group({
      Supplier_Location: ['', Validators.required],
      Conducted_By: [''],
      NameIdeaGenerator:['', Validators.required],
      Date_of_Workshop: ['', Validators.required],
      Date_of_IdeaPunching: [this.maxDate, Validators.required],
      Proposal_Description: ['', Validators.required],
      Function_of_Component: [''],
      Vehicle_System: [''],
      Add_File: [this.Add_File],
      Sector: ['', Validators.required],
      Model_Affected: ['', Validators.required],
      Savings_As_Supplier: ['', Validators.required],
      Savings_As_MM: [''],
      Investments_In_Lacks_As_Supplier: [''],
      Investments_In_Lacks_As_MM: [''],
      Monthly_Production: [''],
      Parts_Suppliers_To_Plants: [''],
      Bench_Mark: [''],
      Other_Bench_Mark: [''],
    });
  }

  createCurrentSituationForm() {
    this.currentSituationForm = this._formBuilder.group({
      Part_Number: [''],
      Part_Description: [''],
      Qty_Veh: [''],
      Part_Cost: ['', Validators.required],
      Weight: [''],
      RM_Grade: [''],
      Add_File: [this.Add_File_Current, Validators.required],
    });
  }

  createProposedSituationForm() {
    this.proposedSituationForm = this._formBuilder.group({
      Part_Number: [''],
      Part_Description: [''],
      Qty_Veh: [''],
      Part_Cost: ['', Validators.required],
      Weight: [''],
      RM_Grade: [''],
      Add_File: [this.Add_File_Proposed],
    });
  }

  createFinalForm() {
    this.submissionForm = this._formBuilder.group({
      Remark: ['',Validators.required],
      eVECP_Number: [''],
      Admin_Status: ['',Validators.required],
      Admin_Remark: ['',Validators.required],
    });
  }

  getWorkShopConductedBy(): Observable<any> {
    return new Observable((data: any) => {
      this.cs.showSwapLoader = true;
      this.cs.postMySuggestionData('IMCRSuggestion/GetWorkshopConductedBy', '').
        subscribe(
          (res: any) => {
            if (!this.cs.isUndefinedORNull(res)) {
              this.cs.showSwapLoader = false;
              this.workShopConductedBy = res.ResponseData;
              data.next();
            } else {
              this.cs.showSwapLoader = false;
              this.cs.showError('Something went wrong please try again later!')
            }
          },
          (error) => {
            this.cs.showSwapLoader = false;
            console.log(error.message);
            data.next();
          }
        )
    })
  }

  getVehicleSystem(): Observable<any> {
    return new Observable((data: any) => {
      this.cs.showSwapLoader = true;
      this.cs.postMySuggestionData('IMCRSuggestion/GetVehicleSystem', '').
        subscribe(
          (res: any) => {
            this.cs.showSwapLoader = false;
            this.vehicleSystem = res.ResponseData;
            data.next();
          },
          (error) => {
            this.cs.showSwapLoader = false;
            console.log(error.message);
            data.next();
          }
        )
    })

  }

  getSectorDivision(): Observable<any> {
    return new Observable((data: any) => {
      this.cs.showSwapLoader = true;
      this.cs.postMySuggestionData('IMCRSuggestion/GetMMDivision', '').
        subscribe(
          (res: any) => {
            this.cs.showSwapLoader = false;
            this.sectorDivision = res.ResponseData;
            data.next();
          },
          (error) => {
            this.cs.showSwapLoader = false;
            console.log(error.message);
            data.next();
          }
        )
    })
  }

  getPartSupplierToPlant(): Observable<any> {
    return new Observable((data: any) => {
      this.cs.showSwapLoader = true;
      this.cs.postMySuggestionData('IMCRSuggestion/GetPartSupptoPlant', '').
        subscribe(
          (res: any) => {
            this.cs.showSwapLoader = false;
            this.partToPlant = res.ResponseData;
            data.next();
          },
          (error) => {
            this.cs.showSwapLoader = false;
            console.log(error.message);
            data.next();
          }
        )
    })
  }

  getBenchMark(): Observable<any> {
    return new Observable((data: any) => {
      this.cs.showSwapLoader = true;
      this.cs.postMySuggestionData('IMCRSuggestion/GetMMBenchmark', '').
        subscribe(
          (res: any) => {
            this.cs.showSwapLoader = false;
            this.MMBenchMark = res.ResponseData;
            data.next();
          },
          (error) => {
            this.cs.showSwapLoader = false;
            console.log(error.message);
            data.next();
          }
        )
    })
  }

  getModelAffected(data) {
    this.cs.showSwapLoader=true;
    let divisionId = {
      "DivisionId": data
    }
    this.cs.postMySuggestionData('IMCRSuggestion/GetModelAffected', divisionId).
      subscribe(
        (res: any) => {
          this.cs.showSwapLoader = false;
          this.modelAffected = res.ResponseData;
        },
        (error) => {
          this.cs.showSwapLoader = false;
         this.cs.showError(error.message);
        }
      )
  }

  validateForm(Form: FormGroup,stepper) {
    return new Promise((resolve: any, reject: any) => {
      if(this.newDealForm.valid && this.cs.isUndefinedORNull(this.newDealForm.controls.Supplier_Location.value.trim())){
        this.cs.showError("Supplier location can not be blank");
        this.newDealForm.controls.Supplier_Location.reset();
        return;
        }
       if(this.newDealForm.valid && this.cs.isUndefinedORNull(this.newDealForm.controls.Proposal_Description.value.trim())){
         this.cs.showError("Proposal description can not be blank");
         this.newDealForm.controls.Proposal_Description.reset();
         return;
       }
      if(Form.valid){
         this.isLinear = false;
         stepper.selected.completed = true;
         stepper.next();
        resolve();
      }else{
        this.isLinear = true;
        stepper.selected.completed = false;
       return;
      }
    })

  }

  navigateToForm(id: number,stepper) {
    if (id == 4) {
      this.validateForm(this.newDealForm,stepper).then(
        data => {
          this.isLinear = true
          stepper.selected.completed = true;
          stepper.selectedIndex = 1;
          this.validateForm(this.currentSituationForm,stepper).then(
            data => {
              this.isLinear = true
              stepper.selected.completed = true;
              stepper.selectedIndex = 2;
              this.validateForm(this.proposedSituationForm,stepper).then(
              )
            },
            err => {
              console.log(err);
            }
          )
        }, err => {
          console.log(err);
        }
      )
    }
    else {
      this.validateForm(this.newDealForm,stepper).then(
        data => {
           this.isLinear = true
           stepper.selected.completed = true;
           stepper.selectedIndex = 1;
          this.validateForm(this.currentSituationForm,stepper).then(

          )
        }, err => {
          console.log(err);
        }
      )
    } 
  }

  detectFiles(event: any, count: any,form) {
    form.get('Add_File')
    .valueChanges
      // .pipe()
      .subscribe(([prev, next]: [any, any]) => {
        if (prev != next) {
          if(count==0){
            this.attachemntChnage = true;
          }
          else if(count==1)
           this.CSCattachemntChnage = true;
          else
           this.PSattachemntChnage = true
        } else {
          if(count==0){
            this.attachemntChnage = false;
          }
          else if(count==1)
          this.CSCattachemntChnage = false;
          else
          this.PSattachemntChnage = false
        }
      });
    let files = event.target.files;
    if(count==0)
    {
      this.fileUrls=[];
      this.all_Files=[]
      for(let file of files){
        let ext = file.name.split(".").pop().toLowerCase();
        if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext=='xlsx' || ext=='ppt' || ext == 'pptx' || ext == 'pdf' || ext == 'csv' || ext == 'doc' || ext == 'docx')
        this.fileUrls.push(file);
        else{
          alert("Please select 'doc', 'pdf', 'xlsx','ppt', 'jpg', jpeg' or 'png' format");
          this.newDealForm.get('Add_File').setValue('');
          return
        }
      }
    }
    if (files) {
      for (let file of files) {
        let ext = file.name.split(".").pop().toLowerCase();
        let reader = new FileReader();
        reader.onload = (e: any) => {
          let newFileNames: any = [];
          if (count == 0) {
            this.isDashboardFile=false;
            if (this.fileUrls.length > 1) {
              this.newDealForm.get('Add_File').setValue('');
              this.fileUrls.forEach(data => {
                newFileNames.push(data.name);
              })
              newFileNames.join(',')
              this.newDealForm.get('Add_File').setValue(newFileNames);
            } else {
              this.newDealForm.get('Add_File').setValue(file.name);
            }
          } else if (count == 1) {
            this.isDashboardCSIFile=false;
            this.files_Of_Current = [];
            this.currentSituationForm.get('Add_File').setValue("");
            if (ext == 'jpg' || ext == 'jpeg' || ext == 'png') {
              this.files_Of_Current.push(file);
              this.currentSituationForm.get('Add_File').setValue(file.name);
            } else {
              this.cs.showError("Please select 'jpg', jpeg' or 'png' format");
              return;
            }

          } else {
            this.isDashboardPSIFile=false;
            this.files_Of_Proposed = [];
            this.proposedSituationForm.get('Add_File').setValue("");
            if (ext == 'jpg' || ext == 'jpeg' || ext == 'png') {
              this.files_Of_Proposed.push(file);
              this.proposedSituationForm.get('Add_File').setValue(file.name);
            } else {
              this.cs.showError("Please select 'jpg', jpeg' or 'png' format");
              return;
            }
          }
        }
        reader.readAsDataURL(file);
      }
    }
  }

  async removeFile(i){
    // this.Files.splice(i,1)
    let files = []
    this.fileUrls.forEach((file, index)=>{
      if(i != index){
        files.push(file)
      }
    })
    await console.log('setting new file list')
    this.fileUrls = files
  }



//  View Image
  view(flag){
    // flag = 1 Current
    // flag = 2 Proposed

    if(flag == 1){
        var blob = new Blob(this.files_Of_Current, { type: this.files_Of_Current[0].type });
        var url = window.URL.createObjectURL(blob);
        var WinPrint = window.open(
          "",
          "",
          "letf=0,top=0,width=400,height=400,toolbar=0,scrollbars=0,status=0"
        );
        WinPrint.document.write('<img src='+url+'></img>')
        // reader.readAsArrayBuffer(this.files_Of_Current[0])
      }
      if(flag == 2){
        var blob = new Blob(this.files_Of_Proposed, { type: this.files_Of_Proposed[0].type });
        var url = window.URL.createObjectURL(blob);
        var WinPrint = window.open(
          "",
          "",
          "letf=0,top=0,width=400,height=400,toolbar=0,scrollbars=0,status=0"
        );
        WinPrint.document.write('<img src='+url+'></img>')
    }
  }

  getDivisionId(ev) {
    this.divisionId = ev.value;
    this.getModelAffected(ev.value);
  }

  numberOnly(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      // this.cs.bas
      return false;
    }  
  return true
  }

  viewFile(form:any){
    let ext = form.controls.Add_File.value.split('.').reverse()[0].toLowerCase()
    if (ext == 'jpeg' || ext == 'jpg' || ext == 'png') {
      var WinPrint = window.open(
        "",
        "",
        "left=0,top=0,width=400,height=400,toolbar=0,scrollbars=0,status=0"
        );
        WinPrint.document.write('<img src='+this.cs1.baseUrl1+ '/'+form.controls.Add_File.value+'></img>')
      } else {
        window.open(this.cs1.baseUrl1+ '/'+form.controls.Add_File.value,'_blank')
    }
  }
  viewAttachedFile(file){
    if(file.type){
      var blob = new Blob([file], { type: file.type });
      var url = window.URL.createObjectURL(blob);
      window.open(url,"_blank")
    }else if(file.name){
        let ext = file.name.split('.').reverse()[0].toLowerCase()
        if (ext == 'jpeg' || ext == 'jpg' || ext == 'png') {
          var WinPrint = window.open(
            "",
            "",
            "left=0,top=0,width=400,height=400,toolbar=0,scrollbars=0,status=0"
            );
            WinPrint.document.write('<img src='+this.cs1.baseUrl1+ '/'+file.filePath+'></img>')
          } else {
            window.open(this.cs1.baseUrl1+ '/'+file.filePath,'_blank')
        }
      return
    }
  }

  submitSuggestion(data:any) {
    if(this.isAdmin=='false'){
      this.submissionForm.controls["Admin_Status"].clearValidators();
      this.submissionForm.controls["Admin_Status"].updateValueAndValidity();
      this.submissionForm.controls["Admin_Remark"].clearValidators();
      this.submissionForm.controls["Admin_Remark"].updateValueAndValidity();
      
    }
    if(!this.dash_Board_Data.SupplierEmailId.toLowerCase().match("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$")){
      this.cs.showError("Kindly provide a valid Email ID!")
      return
    }
    if(this.isAdmin == 'true'){
      if(!this.newDealForm.valid || !this.currentSituationForm.valid|| !this.proposedSituationForm.valid|| !this.submissionForm.valid){
        return;
      }
      let postData=
      {
        "IdeaID":this.dash_Board_DataId,
        "sectorOrMMDivision": this.newDealForm.controls.Sector.value,
        "modelAffected": this.newDealForm.controls.Model_Affected.value,
        "mmBenchmark": this.newDealForm.controls.Bench_Mark.value,
        "SV_Veh_MM": this.newDealForm.controls.Savings_As_MM.value,
        "invest_MM": this.newDealForm.controls.Investments_In_Lacks_As_MM.value,
        "Status":this.submissionForm.controls["Admin_Status"].value,
        "evecp": this.submissionForm.controls.eVECP_Number.value,
        "Admin_Review": this.submissionForm.controls.Admin_Remark.value,
      }
      this.cs.postMySuggestionData('IMCRDashboard/UpdateIdeaStatus',postData).subscribe((data)=>{
        if(!this.cs.isUndefinedORNull(data)){
          this.cs.showSuccess("Status updated succesfully");
          this.router.navigateByUrl('suggestionDashboard/Admin');
          return;
        }
      })
    }
    else{
      let url;
      let status = ''
      if(!this.newDealForm.valid || !this.currentSituationForm.valid|| !this.proposedSituationForm.valid|| !this.submissionForm.valid){
        this.cs.showError("Kindly fill all the mandatory fields!")
        return;
      }
      if(data==1 && this.navigation_From_Dashboard=='false')
         url='IMCRSuggestion/InsertIMCRIdeaDetail';
      else if(data==2 && this.navigation_From_Dashboard=='false')
         url='IMCRSuggestion/SaveIMCRIdea';
       else
       {
        status = data == 1 ? 'Idea Submitted to M&M' : 'Draft';
        url='IMCRSuggestion/EditIMCRIdeaDetail';
       }
      this.all_Files=[];
      let formData: FormData = new FormData;
      this.cs.showSwapLoader=true;
      this.all_Files.push(this.fileUrls)
      let requestParams = {
        "ideaNumber": this.navigation_From_Dashboard == 'true'?this.dash_Board_Data.IdeaNumber:'',
        "supplierId": this.vendorcode,
        "supplierName": this.supplierName,
        "supplierLocation": this.newDealForm.controls.Supplier_Location.value,
        "nameOfIdeaGenerator": this.newDealForm.controls.NameIdeaGenerator.value,
        "sectorOrMMDivision": this.newDealForm.controls.Sector.value,
        "proposalDescription": this.newDealForm.controls.Proposal_Description.value,
        "vehicleSystem": this.newDealForm.controls.Vehicle_System.value,
        "mmBenchmark": this.newDealForm.controls.Bench_Mark.value,
        "saving_VehRs": this.newDealForm.controls.Savings_As_Supplier.value,
        "investment_In_Lakh": this.newDealForm.controls.Investments_In_Lacks_As_Supplier.value,
        "supplierCode": this.vendorcode,
        // this.dash_Board_Data["SupplierMobNo"]=MobileNumber;
        "supplierEmailId": this.userDetails.EmailID || this.dash_Board_Data["SupplierEmailId"],
        "supplierMobNo": this.mobile,
        "dateOfIdeaPunching":moment( this.newDealForm.controls.Date_of_IdeaPunching.value).format('DD-MMM-YYYY'),
        "basicFunctionOfComponent": this.newDealForm.controls.Function_of_Component.value,
        "modelAffected": this.newDealForm.controls.Model_Affected.value,
        "otherThanBenchmark": this.newDealForm.controls.Other_Bench_Mark.value,
        "workshopConductuedByMM": this.newDealForm.controls.Conducted_By.value,
        "dateOfWorkshop":moment(this.newDealForm.controls.Date_of_Workshop.value).format('DD-MMM-YYYY'),
        "teamMember": "123",
        "supplierDepartment": "123",  
        "supllierRemark": this.submissionForm.controls.Remark.value,
        "csPartNo": this.currentSituationForm.controls.Part_Number.value,
        "csPartDesc": this.currentSituationForm.controls.Part_Description.value,
        "csImageName": this.currentSituationForm.controls.Add_File.value,
        "csoty": this.currentSituationForm.controls.Qty_Veh.value,
        "psPartNo": this.proposedSituationForm.controls.Part_Number.value,
        "psPartDesc": this.proposedSituationForm.controls.Part_Description.value,
        "psoty": this.proposedSituationForm.controls.Qty_Veh.value,
        "psImageName": this.proposedSituationForm.controls.Add_File.value,
        "status": status,
        "StatusAfterEdit":status,
        "createdBy": this.username,
        "createdDate": moment(this.maxDate).format('DD-MMM-YYYY'),
        "attachmentPath": 'Files',
        "ideaID": this.navigation_From_Dashboard == 'true'?this.dash_Board_Data.IdeaID:'',
        "csPartcost": this.currentSituationForm.controls.Part_Cost.value,
        "psPartcost": this.proposedSituationForm.controls.Part_Cost.value,
        "sV_Veh_MM": this.newDealForm.controls.Savings_As_MM.value,
        "invest_MM": this.newDealForm.controls.Investments_In_Lacks_As_MM.value,
        "monPrd": this.newDealForm.controls.Monthly_Production.value,
        "pSuppPlnt": this.newDealForm.controls.Parts_Suppliers_To_Plants.value,
        "weightCS": this.currentSituationForm.controls.Weight.value,
        "rmcs": this.currentSituationForm.controls.RM_Grade.value,
        "weightPS": this.proposedSituationForm.controls.Weight.value,
        "rmps": this.proposedSituationForm.controls.RM_Grade.value,
        "evecp": this.submissionForm.controls.eVECP_Number.value,
        "OldCSImageFile":url.includes("EditIMCRIdeaDetail")&&!this.CSCattachemntChnage?this.currentSituationForm.controls.Add_File.value:'',
        "OldPSImageFile":url.includes("EditIMCRIdeaDetail")&&!this.PSattachemntChnage?this.proposedSituationForm.controls.Add_File.value:'',
        "OldAttachedFile":url.includes("EditIMCRIdeaDetail")&&(this.attachemntChnage==false)?this.newDealForm.controls.Add_File.value:''
      }
      formData.append('requestParams', JSON.stringify(requestParams));
      formData.append('CSImage', this.files_Of_Current[0]);
      formData.append('PSImage', this.files_Of_Proposed[0])
      // for (var i = 0; i < this.all_Files.length; i++) {
        for (var i = 0; i < this.fileUrls.length; i++) {
          formData.append(`file${i}`, this.fileUrls[i]);
        }
      this.cs.postMySuggestionData(url, formData).
        subscribe(
          (res: any) => {
            this.cs.showSwapLoader=false;
            if (res)
            {
              this.cs.showSuccess("Data Stored Successfully!");
              this.newform.resetForm();
              this.currentform.resetForm();
              this.proposalform.resetForm();
              this.forthform.resetForm();
              this.newDealForm.get('Date_of_IdeaPunching').setValue(this.maxDate);
              this.router.navigateByUrl('suggestionDashboard/User');
            }
            else
            this.cs.showError("Please try again");
          },
          (error) => {
            this.cs.showSwapLoader=false;
            this.cs.showError(error);
          }
        )
    }
   
  }

  navigate(){
    if(this.RoleID == 7 ||  this.RoleID == '7'){
      this.router.navigateByUrl('adminDashboard');
    }else{
      this.router.navigateByUrl('dashboard');
    }
  }

  // protected clarificationForm: FormGroup
  protected clarificationRemark = new FormControl()
  protected clarificationEmail = new FormControl()
  protected clarificationFile: File
  protected clarificationFileName = ''
  addClarificatation(content){
    this.modalService.open(content);
  }

  getFiles(event){
    this.clarificationFile = event.target.files[0];
    this.clarificationFileName = this.clarificationFile.name;
    console.log(this.clarificationFile)
  }
  
  submitClarification(){
    // if(this.clarificationRemark.valid && this.clarificationEmail.valid){
      let body = {
        ReviewText: this.clarificationRemark.value,
        ReviewAttch: "",
        Flag: "Admin",
        IdeaId: this.dash_Board_DataId
      }
      let formData = new FormData
      formData.append('reqjson',JSON.stringify(body))
      formData.append('Review_Image',this.clarificationFile)
      console.log(formData)
      
      this.cs.postMySuggestionData('IMCRSuggestion/UpdateIdeaReview',formData).subscribe(res=>{
        if(res.ResponseData[0].msg.toLowerCase() == 'updated'){
          this.cs.showSuccess('Seek Clarification has been sent')
        }
        this.closeModal()
      })
    // }else{
    //   this.cs.showError('Kindly Fill All Fields')
    // }
  }

  
  exportSingleExcel(){
    let json = [this.dash_Board_Data]
    let dataNew = json;
    console.log(dataNew);
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("ReportData");
    let columns = Object.keys(json[0]);

    columns.splice(14, 1);

    columns.splice(7, 1, 'Idea Uploaded By');
    columns.splice(9, 1, 'Supplier Department');
    columns.splice(22, 1, 'Current Situation Part Number');
    columns.splice(23, 1, 'Current Situation Part Description');
    columns.splice(24, 1, 'Current Situation Image Name');
    columns.splice(25, 1, 'Current Situation Quantity');
    columns.splice(26, 1, 'Proposed Situation Part Number');
    columns.splice(27, 1, 'Proposed Situation Part Description');
    columns.splice(28, 1, 'Proposed Situation Quantity');
    columns.splice(29, 1, 'Proposed Situation Image Name');
    columns.splice(34, 1, 'Current Situation Part Cost');
    columns.splice(35, 1, 'Proposed Situation Part Cost');
 
    let headerRow = worksheet.addRow(columns);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "00FF0000" },
        bgColor: { argb: "00FF0000" },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
    });
    dataNew.forEach((d) => {
      // var obj = JSON.parse(d);
      var values = Object.keys(d).map(function (key) {
        return d[key];
      });
      values.splice(14,1)
      let row = worksheet.addRow(values);
    });
    workbook.xlsx.writeBuffer().then((dataNew) => {
      let blob = new Blob([dataNew], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(
        blob,
        "SuggestionReport.xlsx"
      );
    });
    console.log(this.dash_Board_Data)
    // window.open(this.cs1.baseUrl1+ 'UploadedFiles/IMCR/'+this.newDealForm.value.Add_File,'_blank')
    // window.open(this.cs1.baseUrl1+ 'UploadedFiles/IMCR/'+this.proposedSituationForm.value.Add_File,'_blank')
    // window.open(this.cs1.baseUrl1+ 'UploadedFiles/IMCR/'+this.currentSituationForm.value.Add_File,'_blank')
    // console.log(this.cs1.baseUrl1+ 'UploadedFiles/IMCR/'+this.newDealForm.value.Add_File)

    // var blob = new Blob([this.cs1.baseUrl1+ 'UploadedFiles/IMCR/'+this.newDealForm.value.Add_File]);
    // if(this.newDealForm.value.Add_File){
    //   fs.saveAs(this.cs1.baseUrl1+ 'UploadedFiles/IMCR/'+this.newDealForm.value.Add_File, "img1.jpg");
    // }
    // // var blob = new Blob([this.cs1.baseUrl1+ 'UploadedFiles/IMCR/'+this.proposedSituationForm.value.Add_File]);
    // if(this.proposedSituationForm.value.Add_File){
    //   fs.saveAs(this.cs1.baseUrl1+ 'UploadedFiles/IMCR/'+this.proposedSituationForm.value.Add_File, "img2.jpg");
    // }
    // if(this.currentSituationForm.value.Add_File){
    //   fs.saveAs(this.cs1.baseUrl1+ 'UploadedFiles/IMCR/'+this.currentSituationForm.value.Add_File, "img3.jpg");
    // }
    // var blob = new Blob([this.cs1.baseUrl1+ 'UploadedFiles/IMCR/'+this.currentSituationForm.value.Add_File]);

      let blob = this.http.get(this.cs1.baseUrl1+ 'UploadedFiles/IMCR/'+this.currentSituationForm.value.Add_File, {
        responseType: 'blob'
      })
      // fs.saveAs(blob, this.currentSituationForm.value.Add_File)
      const a = document.createElement('a')
      // URL.createObjectURL
      const objectUrl = window.URL.createObjectURL(blob)
      a.href = objectUrl
      a.download = this.currentSituationForm.value.Add_File;
      a.click();
      URL.revokeObjectURL(objectUrl);
  }

  openClarificationFile(){
    if(this.dash_Board_Data.Admin_Review_Attch){
      window.open(this.dash_Board_Data.Admin_Review_Attch,'_blank')
    }
  }

  getAdmin_Review_Attch(path: string){
    let file_name = path.split('/').reverse()[0]
    return file_name
  }

  closeModal(){
    this.modalService.dismissAll()
  }
  
}
