import { Component, OnInit } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { MatIconRegistry } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { SurveyService } from "../services/survey/survey.service";
import { ToastrService } from 'ngx-toastr';
// import 'rxjs/operators';
import { map } from 'rxjs/operators'
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import * as CryptoJS from "crypto-js";
@Component({
  selector: "app-srmm-survey",
  templateUrl: "./srmm-survey.component.html",
  styleUrls: ["./srmm-survey.component.scss"],
})
export class SrmmSurveyComponent implements OnInit {
  categoryForm: FormGroup;
  questionForm: FormGroup;


  userToken = CryptoJS.AES.decrypt(
    localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),
    ""
  ).toString(CryptoJS.enc.Utf8);

  masters: any;
  roleId: string;
  // parseInt = parseInt

  toInteger(integer){
    return parseInt(integer)
  }

  displayedColumns = [
    "questionNumber",
    "category",
    "questions",
    "questionWeightage",
    "categoryWeightage",
    "measurement",
    "applicableTo",
    "attachment",
    "isActive",
    "isRequired",
    "isThreshold",
    "option0",
    "option0Rating",
    "requiredType",
    "action",
  ];

  DataSource = [];
  tempDataSource = [
    {
      questions: "Question1",
      questionNumber: "22",
      category: "Quality",
      questionWeightage: "1",
      categoryWeightage: "2",
      measurement: "123",
      applicableTo: "KAM",
      attachment: "Required",
      isActive: "Yes",
      isRequired: "No",
      isThreshold: "No",
      option0: "0",
      option0Rating: "Option 0",
      requiredType: "Required Type",
    },
  ];

  categories = [
    // { id: 1, name: "Liquidity", isActive: true },
    // { id: 2, name: "Quality", isActive: false },
  ];

  constructor(
    public commonService: CommonUtilityService,
    public formBuilder: FormBuilder,
    public modalService: NgbModal,
    public matIconRegistry: MatIconRegistry,
    public domSanitizer: DomSanitizer,
    public surveyService: SurveyService,
    public toastr: ToastrService,
    public cs: MySuggestionService
  ) {
    matIconRegistry.addSvgIcon(
      "edit",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Edit.svg"
      )
    );
    this.commonService.changeIsAuthenticate(true);
    this.categoryForm = this.formBuilder.group({
      categoryId: [0],
      isActive: [false],
      categoryName: ["", Validators.required],
    });

    this.questionForm = this.formBuilder.group({
      questionId: [0],
      question: ["", Validators.required],
      sourceOfInfo: ["", Validators.required],
      measurement: ["", Validators.required],
      isActive: [""],
      required: [""],
      attachment: ["", Validators.required],
      questionNumber:["", Validators.required],
      applicableTo: ["", Validators.required],
      frequency: ["", Validators.required],
      option0: [""],
      option1: ["", Validators.required],
      option2: ["", Validators.required],
      option3: [""],
      option4: [""],
      option5: [""],
      option0Rating: [""],
      option1Rating: ["", Validators.required],
      option2Rating: ["", Validators.required],
      option3Rating: [""],
      option4Rating: [""],
      option5Rating: [""],
      questionWeightage: ["", Validators.required],
      categoryWeightage: ["", Validators.required],
      category: ["", Validators.required],
      questions: ["", Validators.required],
      isThreshold: [""],
      requiredType: [""],
    });
    // this.questionForm.valueChanges
    // .pipe(map(data => data.filter((value) => this.questionForm.valid)))
    // .subscribe((value) => {
    //   console.log(value);
    //   console.log(this.questionForm.controls['{$value}'])
    //   if(this.cs.isUndefinedORNull(this.questionForm.controls.question.value.trim())){
    //     console.log(value,'is Blank')
    //   }else{
    //     console.log(value,'is not Blank')
    //   }
    //   return value;
    // })

    this.questionForm.controls.question.valueChanges.subscribe(res=>{if(res && this.cs.isUndefinedORNull(res.trim())){
        this.questionForm.controls.question.reset()
      }})

    this.questionForm.controls.option1.valueChanges.subscribe(res=>{if(res && this.cs.isUndefinedORNull(res.trim())){
        this.questionForm.controls.option1.reset()
      }})

    this.questionForm.controls.option2.valueChanges.subscribe(res=>{if(res && this.cs.isUndefinedORNull(res.trim())){
        this.questionForm.controls.option2.reset()
      }})

    this.questionForm.controls.option3.valueChanges.subscribe(res=>{if(res && this.cs.isUndefinedORNull(res.trim())){
        this.questionForm.controls.option3.reset()
      }})

    this.questionForm.controls.option4.valueChanges.subscribe(res=>{if(res && this.cs.isUndefinedORNull(res.trim())){
        this.questionForm.controls.option4.reset()
      }})

    this.questionForm.controls.option5.valueChanges.subscribe(res=>{if(res && this.cs.isUndefinedORNull(res.trim())){
        this.questionForm.controls.option5.reset()
      }})

    this.getCategories();
    this.getQuestions();

    this.surveyService
      .call("admincrudsurvey/Get_SRMMMasterDetails")
      .subscribe((res) => {
        console.log(res);
        this.masters = res.ResponseData;
      });
  }
  ngOnInit() {
    this.roleId = localStorage.getItem(btoa('roleId'));
    this.roleId = atob(this.roleId);
  }

  getCategories(){
    this.surveyService.call('admincrudsurvey/GetSRMMCategory').subscribe(res=>{
      console.log(res)
      this.categories = res.ResponseData;
    })
  }

  getQuestions(){

    this.surveyService.call("admincrudsurvey/GetSRMMQuestionByID", {
      QID: "0",
    }).subscribe(res=>{
      console.log(res)
      if(res.Message == 'Success'){
        let dataSource = [];
        for (const data of res.ResponseData) {
          dataSource.push({
            questionId: data.QID,
            questions: data.QUESTIONTEXT2,
            questionNumber: data.QUESTIONNUMBER,
            category: data.CATNAME,
            questionWeightage: data.QUESTIONWEIGHT,
            categoryWeightage: data.CATWEIGHT,
            measurement: data.MEASURENAME,
            applicableTo: data.APPLICABLETONAME,
            attachment: data.ATTACHMENTNAME,
            isActive: data.ISACTIVE,
            isRequired: data.ISREQUIRED,
            isThreshold: data.ISTHRESHOLD,
            option0: data.OPTEXT0,
            option0Rating: data.OPTRATING0,
            requiredType: data.requiredType || 'Temp',
          });
        }
        console.log(dataSource);
        this.DataSource = [...dataSource];
      }
    });

  }

  openCategoryModal(content) {
    const modalOps: NgbModalOptions = {
      size: "xl" as "lg",
      centered: true,
      backdrop: "static",
      windowClass: "formModal",
    };

    this.modalService.open(content, modalOps);
  }

  closeModal() {
    this.modalService.dismissAll();

    this.questionForm.reset();
    this.questionForm.controls.questionId.setValue(0);
  }

  edit(category) {
    console.log(category);
    this.categoryForm.controls["categoryName"].setValue(category.name);
    this.categoryForm.controls["categoryId"].setValue(category.ID);
    let active = (category.isactive == "1" || category.isactive == "t") ? true : false
    this.categoryForm.controls["isActive"].setValue(active);
  }

  submit() {

    let body: any = {};
    body.Name = this.categoryForm.value.categoryName
    body.ID = this.categoryForm.value.categoryId
    // body.IsActive= true,
    body.isActive = this.categoryForm.value.isActive,
    body.CreatedBy = this.userToken;

    if(this.categoryForm.valid){
      this.surveyService.call('admincrudsurvey/CRUDSRMMCategory', body).subscribe(res=>{
        console.log(res)
        if(res.Message == 'Success'){
          this.getCategories();
        }
      // this.categories.push({
      //   id: this.categories.length,
      //   name: this.categoryForm.value.categoryName,
      //   isActive: this.categoryForm.value.isActive,
      // });
      })
    }
  }

  resetForm() {
    this.categoryForm.reset();
    this.categoryForm.controls["categoryId"].setValue(0);
  }

  manageQuestions(content, question?: any) {
    const modalOps: NgbModalOptions = {
      size: "xl" as "lg",
      centered: true,
      backdrop: "static",
      windowClass: "formModal",
    };

    if (question) {
      this.setQuestionsForm(question);
    }

    this.modalService.open(content, modalOps);
  }

  setQuestionsForm(question) {
    console.log(question);
    this.surveyService.call('admincrudsurvey/GetSRMMQuestionByID',{QID: question.questionId}).subscribe(res=>{
      console.log(res)
      let response = res.ResponseData[0]
      if(res.Message == "Success"){
        console.log(response)
        this.questionForm.controls.questionId.setValue(response.QID);
        this.questionForm.controls.question.setValue(response.QUESTIONTEXT);
        this.questionForm.controls.sourceOfInfo.setValue(response.SOURCEID);
        this.questionForm.controls.measurement.setValue(response.MEASUREID);
        this.questionForm.controls.isActive.setValue(response.ISACTIVE);
        this.questionForm.controls.required.setValue(response.ISREQUIRED);
        this.questionForm.controls.attachment.setValue(response.ATTACHMENTID);
        this.questionForm.controls.questionNumber.setValue(response.QUESTIONNUMBER);
        this.questionForm.controls.applicableTo.setValue(response.APPLICABLEID);
        this.questionForm.controls.frequency.setValue(response.FREQUENCYID);
        this.questionForm.controls.option0.setValue(response.OPTEXT0);
        this.questionForm.controls.option1.setValue(response.OPTEXT1);
        this.questionForm.controls.option2.setValue(response.OPTEXT2);
        this.questionForm.controls.option3.setValue(response.OPTEXT3);
        this.questionForm.controls.option4.setValue(response.OPTEXT4);
        this.questionForm.controls.option5.setValue(response.OPTEXT5);
        this.questionForm.controls.option0Rating.setValue(response.OPTRATING0);
        this.questionForm.controls.option1Rating.setValue(response.OPTRATING1);
        this.questionForm.controls.option2Rating.setValue(response.OPTRATING2);
        this.questionForm.controls.option3Rating.setValue(response.OPTRATING3);
        this.questionForm.controls.option4Rating.setValue(response.OPTRATING4);
        this.questionForm.controls.option5Rating.setValue(response.OPTRATING5);
        this.questionForm.controls.questionWeightage.setValue(response.CATWEIGHT);
        this.questionForm.controls.categoryWeightage.setValue(response.QUESTIONWEIGHT);
        this.questionForm.controls.category.setValue(response.CATID);
        this.questionForm.controls.questions.setValue(response.QUESTIONTEXT2);
        this.questionForm.controls.isThreshold.setValue(response.ISTHRESHOLD);
        this.questionForm.controls.requiredType.setValue('');
      }
    })
  }

  submitQuestion() {
    console.log(this.questionForm);

    if(this.questionForm.valid){

      let body = new FormData;
      let Reqjson = {
          qid: this.questionForm.value.questionId,
          sourceinfo: this.questionForm.value.sourceOfInfo,
          catweight: this.questionForm.value.categoryWeightage,
          measurement: this.questionForm.value.measurement,
          questionnumber: this.questionForm.value.questionNumber,
          optratinG0: this.questionForm.value.option0Rating || 0,
          optratinG1: this.questionForm.value.option1Rating,
          optratinG2: this.questionForm.value.option2Rating,
          optratinG3: this.questionForm.value.option3Rating || 0,
          optratinG4: this.questionForm.value.option4Rating || 0,
          optratinG5: this.questionForm.value.option5Rating || 0,
          questionweight: this.questionForm.value.questionWeightage,
          category: this.questionForm.value.category,
          isactive: this.questionForm.value.isActive ? 1 : 0,
          isrequired: this.questionForm.value.required ? 1 : 0,
          isthreshold: this.questionForm.value.isThreshold ? 1 : 0,
          attachment: this.questionForm.value.attachment,
          applicableto: this.questionForm.value.applicableTo,
          frequency: this.questionForm.value.frequency,
          optexT0: this.questionForm.value.option0 || '',
          optexT1: this.questionForm.value.option1,
          optexT2: this.questionForm.value.option2,
          optexT3: this.questionForm.value.option3 || '',
          optexT4: this.questionForm.value.option4 || '',
          optexT5: this.questionForm.value.option5 || '',
          questiontext: this.questionForm.value.question,
          questiontexT2: this.questionForm.value.questions,
          requiredtype: this.questionForm.value.requiredType || '',
        };
        let user = localStorage.getItem(btoa('userToken'));
        user = atob(user);
      body.append('Reqjson', JSON.stringify(Reqjson))
      body.append('User', JSON.stringify(user))
      
      this.surveyService
        .call("admincrudsurvey/InsertSRMMQuestions", body)
        .subscribe((res) => {
          if(res.ResponseData.REMARK == 'Success'){
            this.questionForm.value.questionId == 0 ? this.toastr.success('Question Added Successfully') : this.toastr.success('Question Updated Successfully')
            console.log("Response To Insert SRM Question", res);
            this.getQuestions();
            this.closeModal();
          }
        });
    }else{
      this.toastr.error('Please Fill all mandatory questions');
    }
  }
}
