import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SrmmSurveyComponent } from './srmm-survey.component';

describe('SrmmSurveyComponent', () => {
  let component: SrmmSurveyComponent;
  let fixture: ComponentFixture<SrmmSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SrmmSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SrmmSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
