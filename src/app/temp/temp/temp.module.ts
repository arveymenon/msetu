import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/material.module';
import { MatPaginatorModule, MatDialogModule, MatSortModule, MatFormFieldModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { NgxBarcodeModule } from 'ngx-barcode';
import { TempRoutingModule } from './temp-routing.module';

import { CMSLandingComponent } from 'src/app/cmslanding/cmslanding.component';
import { CmsdashboardComponent } from 'src/app/cmsdashboard/cmsdashboard.component';
import { CmsdocumentComponent } from 'src/app/cmsdocument/cmsdocument.component';
import { SupplierMeetCMSComponent } from 'src/app/supplier-meet-cms/supplier-meet-cms.component';
import { MyExcellenceAwardComponent } from 'src/app/my-excellence-award/my-excellence-award.component';
import { AwardsModalComponent } from 'src/app/modal/awards-modal/awards-modal.component';
import { PlasticWasteManagementComponent } from 'src/app/modal/plastic-waste-management/plastic-waste-management.component';
import { NotificationOnPlasticUsageComponent } from 'src/app/modal/notification-on-plastic-usage/notification-on-plastic-usage.component';
import { MyPolicyGuidelinesComponent } from 'src/app/my-policy-guidelines/my-policy-guidelines.component';
import { SafeHtmlPipePipe } from 'src/app/pipe/safeHtmlPipe/safe-html-pipe.pipe';
import { EmailPanelComponent } from 'src/app/email-panel/email-panel.component';
import { CKEditorModule } from 'ng2-ckeditor';
import { ManagedUsersComponent } from 'src/app/supplier-meet-cms/managed-users/managed-users.component';

@NgModule({
  declarations: [
    CMSLandingComponent,
    CmsdashboardComponent,
    CmsdocumentComponent,
    MyExcellenceAwardComponent,
    AwardsModalComponent,
    ManagedUsersComponent,
    MyPolicyGuidelinesComponent,
    EmailPanelComponent,
    // SafeHtmlPipePipe,
    SupplierMeetCMSComponent,
    PlasticWasteManagementComponent,
    NotificationOnPlasticUsageComponent,
  ],
  entryComponents:[
    NotificationOnPlasticUsageComponent,
    PlasticWasteManagementComponent,
    EmailPanelComponent
  ],
  imports: [
    CKEditorModule,
    CommonModule,
    TempRoutingModule,

    MaterialModule,
    FormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSortModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    NgxBarcodeModule
  ]
})
export class TempModule { }
