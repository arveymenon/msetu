import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { CMSLandingComponent } from "src/app/cmslanding/cmslanding.component";
import { AuthGuard } from "src/app/services/auth-guards/auth.guard";

import { CmsdashboardComponent } from 'src/app/cmsdashboard/cmsdashboard.component';
import { CmsdocumentComponent } from 'src/app/cmsdocument/cmsdocument.component';
import { MyExcellenceAwardComponent } from 'src/app/my-excellence-award/my-excellence-award.component';
import { AwardsModalComponent } from 'src/app/modal/awards-modal/awards-modal.component';
import { SupplierMeetCMSComponent } from 'src/app/supplier-meet-cms/supplier-meet-cms.component';
import { MyPolicyGuidelinesComponent } from 'src/app/my-policy-guidelines/my-policy-guidelines.component';
import { EmailPanelComponent } from 'src/app/email-panel/email-panel.component';

const routes: Routes = [
  {
    path: "",
    component: CMSLandingComponent,
    canActivate: [AuthGuard],
    data: { roles: ['CMSLanding'] }
  },
  {
    path: "CMSLanding",
    component: CMSLandingComponent,
    canActivate: [AuthGuard],
    data: { roles: ['CMSLanding'] }
  },
  {
    path: "CMSDashboard",
    component: CmsdashboardComponent,
    canActivate: [AuthGuard],
    data: { roles: ['CMSDashboard'] }
  },
  {
    path: "CMSDocument",
    component: CmsdocumentComponent,
    canActivate: [AuthGuard],
    data: { roles: ['CMSDocument'] }
  },
  {
    path: "myExcellenceAward",
    component: MyExcellenceAwardComponent,
    canActivate: [AuthGuard],
    data: { roles: ['myExcellenceAward'] }
  },
  {
    path: "awardModal",
    component: AwardsModalComponent,
    canActivate: [AuthGuard],
    data: { roles: ['awardModal'] }
  },
  {
    path: "my-policy-guidelines",
    component: MyPolicyGuidelinesComponent,
    canActivate: [AuthGuard],
    data: { roles: ['my-policy-guidelines'] }
  },
  {
    path: "SupplierMeet",
    component: SupplierMeetCMSComponent,
    canActivate: [AuthGuard],
    data: { roles: ['SupplierMeet'] }
  },
  {
    path: "emailpanel",
    component: EmailPanelComponent,
    canActivate: [AuthGuard],
    data: { roles: [7] },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TempRoutingModule {}
