import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/material.module';
import { MatPaginatorModule, MatDialogModule, MatSortModule, MatFormFieldModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { NgxBarcodeModule } from 'ngx-barcode';
import { Temp2RoutingModule } from './temp2-routing.module';
import { IndexedDBModule } from '../IndexedDB/indexed-db/indexed-db.module';

import { NgxIndexedDBModule, DBConfig } from 'ngx-indexed-db';
import { SPMComponent } from 'src/app/spm/spm';
import { UserDetailsComponent } from 'src/app/user-details/user-details.component';

export function migrationFactory() {
  // The animal table was added with version 2 but none of the existing tables or data needed
  // to be modified so a migrator for that version is not included.
  // return {
  //   1: (db, transaction) => {
  //     const store = transaction.objectStore('performace_file_queue');
  //     store.createIndex('file', 'file', { unique: false });
  //   },
  //   3: (db, transaction) => {
  //     const store = transaction.objectStore('performace_file_queue');
  //     store.createIndex('uploaded', 'uploaded', { unique: false });
  //   }
  // };
}

const dbConfig: DBConfig  = {
  name: 'MyDb',
  version: 1,
  objectStoresMeta: [{
    store: 'performace_file_queue',
    storeConfig: { keyPath: 'id', autoIncrement: true },
    storeSchema: [
      { name: 'file', keypath: 'name', options: { unique: false } },
      { name: 'uploaded', keypath: 'name', options: { unique: false } }
    ]
  }]
  // migrationFactory
};

@NgModule({
  declarations: [
    SPMComponent,
    UserDetailsComponent
  ],
  imports: [
    CommonModule,
    Temp2RoutingModule,

    MaterialModule,
    FormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSortModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    NgxBarcodeModule,

    NgxIndexedDBModule.forRoot(dbConfig)
  ]
})
export class Temp2Module { }
