import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SPMComponent } from 'src/app/spm/spm'
import { UserDetailsComponent } from 'src/app/user-details/user-details.component';

const routes: Routes = [
  {
    path: "",
    component: SPMComponent
  },
  {
    path: "spm",
    component: SPMComponent
  },
  {
    path: "user-details",
    component: UserDetailsComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class Temp2RoutingModule { }
