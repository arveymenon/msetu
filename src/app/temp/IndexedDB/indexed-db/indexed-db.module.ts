import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { NgxIndexedDBModule, DBConfig } from 'ngx-indexed-db';

export function migrationFactory() {
  // The animal table was added with version 2 but none of the existing tables or data needed
  // to be modified so a migrator for that version is not included.
  return {
    1: (db, transaction) => {
      const store = transaction.objectStore('performace_file_queue');
      store.createIndex('file', 'uploaded', { unique: false });
    }
  };
}

const dbConfig: DBConfig  = {
  name: 'MyDb',
  version: 1,
  objectStoresMeta: [{
    store: 'performace_file_queue',
    storeConfig: { keyPath: 'id', autoIncrement: true },
    storeSchema: [
      { name: 'file', keypath: 'name', options: { unique: false } },
      { name: 'uploaded', keypath: 'name', options: { unique: false } }
    ]
  }],
  migrationFactory
};


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxIndexedDBModule.forRoot(dbConfig)
  ]
})
export class IndexedDBModule { }
