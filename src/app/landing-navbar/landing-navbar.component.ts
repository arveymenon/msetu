import { Component, OnInit } from '@angular/core';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-navbar',
  templateUrl: './landing-navbar.component.html',
  styleUrls: ['./landing-navbar.component.scss']
})
export class LandingNavbarComponent implements OnInit {

  constructor(
    public cs:MySuggestionService,
    public router: Router) { }

  ngOnInit() {
  }

}
