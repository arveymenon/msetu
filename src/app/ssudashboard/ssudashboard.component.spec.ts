import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SSUDashboardComponent } from './ssudashboard.component';

describe('SSUDashboardComponent', () => {
  let component: SSUDashboardComponent;
  let fixture: ComponentFixture<SSUDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SSUDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SSUDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
