import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-ssudashboard',
  templateUrl: './ssudashboard.component.html',
  styleUrls: ['./ssudashboard.component.scss']
})
export class SSUDashboardComponent implements OnInit {

  DataSource = [];
  displayedColumns: string[] =['FYYear','totalSuppliers', 'newAssessments','Reassessment','selfAssessment'];
  username:any;
  RoleID:any;
  
  roleId = CryptoJS.AES.decrypt(
    localStorage.getItem("WTIwNWMxcFZiR3M9"),
    ""
  ).toString(CryptoJS.enc.Utf8);
  supplierCode:any;
  report_data=[];
  year:any;
  YearHeader:any

  constructor(public cs: MySuggestionService,private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer,public commonService: CommonUtilityService,private modalService: NgbModal) { 
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "viewIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/View_Icon_Red.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      'edit',
      domSanitizer.bypassSecurityTrustResourceUrl(
        '../assets/Icons/innerPages/Edit.svg'));
        matIconRegistry.addSvgIcon('excelIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/Excel_White_Icon.svg'));
        matIconRegistry.addSvgIcon('trash',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/delete.svg'));
  }

  ngOnInit() {
    this.RoleID = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID,"").toString(CryptoJS.enc.Utf8)
    this.username = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==')
    this.username = CryptoJS.AES.decrypt(this.username,"").toString(CryptoJS.enc.Utf8)
    this.supplierCode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
    this.supplierCode = CryptoJS.AES.decrypt(this.supplierCode,"").toString(CryptoJS.enc.Utf8);
  }
 
  closeModal(){
    this.modalService.dismissAll();
  }
  openModal(content) {
    this.modalService.open(content, { centered:true, windowClass : "formModal"});
  }

  year_col=['Plan','Actual','Balance']

  getReport(){
    let Request = {
      "loginName": this.username,
      "schID": this.year,
      "supplierCode": this.supplierCode
      // "loginName": "MAHINDRAEXT\\DH024-k",
      // "schID": "FY 2017",
      // "supplierCode": "DM024"
    }
    this.YearHeader = this.year;
    this.cs.postMySuggestionData('SRMMSurvey/GetSSUDashboard',Request).subscribe((res:any)=>{
      if(!this.cs.isUndefinedORNull(res.ResponseData)){
        this.DataSource = res.ResponseData.PartOneData;
        // this.DataSource['year_col']=this.year_col
      }else{
        this.cs.showError('Something went wrong please try again later');
        return;
      }
    })
  }

}
