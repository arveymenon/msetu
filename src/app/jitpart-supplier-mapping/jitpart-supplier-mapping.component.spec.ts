import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JITPartSupplierMappingComponent } from './jitpart-supplier-mapping.component';

describe('JITPartSupplierMappingComponent', () => {
  let component: JITPartSupplierMappingComponent;
  let fixture: ComponentFixture<JITPartSupplierMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JITPartSupplierMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JITPartSupplierMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
