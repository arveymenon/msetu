import { Component, OnInit, ViewChild } from "@angular/core";
import { MatIconRegistry, MatPaginator } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import * as CryptoJS from 'crypto-js';
import { Workbook } from 'exceljs';
import * as fs from "file-saver";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";
import { JitService } from "../services/jit/jit.service";
import { ToastrService } from "ngx-toastr";
import { MyHelpDeskService } from "../services/myHelpDesk/my-help-desk.service";

@Component({
  selector: "app-jitpart-supplier-mapping",
  templateUrl: "./jitpart-supplier-mapping.component.html",
  styleUrls: ["./jitpart-supplier-mapping.component.scss"]
})
export class JITPartSupplierMappingComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator
  roleId = localStorage.getItem('WTIwNWMxcFZiR3M9');
  modelCode = new FormControl();
  partCode = new FormControl();
  plantCode = new FormControl();
  supplierCode = new FormControl();

  partSupplierId = 0;

  PartSupplierForm: FormGroup;
  goToControl = new FormControl();
  goTo: number;
  pageNumbers = [];

  pageSize = 0;
  pageNo = 0;

  DataSource = [];
  plantList = [];
  displayedColumns: string[] = [
    "modelCode",
    "partNumber",
    "partDescription",
    "aggregate",
    "plantCode",
    "supplierCode",
    "isActive",
    "action"
  ];

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public commonService: CommonUtilityService,
    private modalService: NgbModal,
    public jitService: JitService,
    public cs: MySuggestionService,
    public formBuilder: FormBuilder,
    public toastr: ToastrService,
    public http: MyHelpDeskService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "addIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/plus.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "trash",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/delete.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "edit",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Edit.svg"
      )
    );
    this.getTableValues(0);
    this.PartSupplierForm = this.formBuilder.group({
      ModelCode: ["", Validators.required],
      PartNumber: ["", Validators.required],
      PartDescription: [{ value: "", disabled: true }, Validators.required],
      Aggregates: [{ value: "", disabled: true }, Validators.required],
      PlantCode: ["", Validators.required],
      SupplierCode: ["", Validators.required]
    });

    this.jitService
      .call("PTLiveData/GetPlantCodeList", { SupplierCode: "DL016" })
      .subscribe(response => {
        console.log(response);

        this.plantList = response.ResponseData;
      });

    this.PartSupplierForm.get("PartNumber").valueChanges.subscribe(
      partNumber => {
        console.log(partNumber);
        if (partNumber && partNumber.length > 8) {
          this.jitService
            .call("PTLiveData/GetPartDetails", { PartNumber: partNumber })
            .subscribe(response => {
              console.log(response);
              if (response.ResponseData[0]) {
                this.PartSupplierForm.get("PartDescription").patchValue(
                  response.ResponseData[0].PartDescription
                );
                this.PartSupplierForm.get("Aggregates").patchValue(
                  response.ResponseData[0].Aggregates
                );
              }
            });
        }
      }
    );
  }

  getTableValues(pageNo?: any) {
    console.log(this.partCode.value);
    console.log(this.modelCode.value);
    const body = {
      PageStart: pageNo ? pageNo + 1 : 1,
      RowPerPage: 10,
      ModelCode: this.modelCode.value,
      PartNumber: this.partCode.value,
      PlantCode: this.plantCode.value,
      SupplierCode: this.supplierCode.value
    };
    this.jitService
      .call("PTLiveData/GetPartSupplierMapping", body)
      .subscribe(response => {
        console.log(response);
        let dataSource = [];
        this.pageSize = response ? response.DataCount : 200 || 0;
        for (const model of response.ResponseData) {
          dataSource.push({
            partSupplierId: model.PartSupplierId,
            modelCode: model.ModelCode,
            partNumber: model.PartNumber,
            partDescription: model.PartDescription,
            aggregate: model.Aggregates,
            plantCode: model.PlantCode,
            supplierCode: model.SupplierCode,
            isActive: model.IsActive,
            action: ""
          });
        }
        this.DataSource = [...dataSource];
        this.updateGoto();
        console.log(this.DataSource);
      });
  }


  goToChange(value?: any) {
    this.paginator.pageIndex = this.goTo - 1;
    console.log(value)
    console.log(this.paginator.pageIndex)
    this.goToControl.setValue(this.paginator.pageIndex)
    this.getTableValues(this.paginator.pageIndex)
  }

  updateGoto() {
    this.goTo = 1;
    this.pageNumbers = [];
    var totalLength = this.pageSize / 10;
    var ceiledLength = Math.ceil(totalLength);
    console.log(ceiledLength);
    for (let i = 1; i <= ceiledLength; i++) {
      this.pageNumbers.push(i);
    }
  }

  reset() {
    this.partCode.reset();
    this.modelCode.reset();
    this.plantCode.reset();
    this.supplierCode.reset();
    this.getTableValues(0);
  }

  // exportAsExcelFile(table: any, name: any) {
  //   this.displayedColumns.splice(7, 1);
  //   this.cs.exportAsExcelFile(table, name);
  //   this.displayedColumns.push("action");
  // }

  getPageChangeData($event) {
    console.log($event);
    this.getTableValues($event.pageIndex);
  }

  ngOnInit() {
    this.roleId = CryptoJS.AES.decrypt(this.roleId, "").toString(CryptoJS.enc.Utf8)
  }

  closeModal() {
    this.PartSupplierForm.reset();
    this.partSupplierId = 0;
    this.PartSupplierForm.get("ModelCode").enable({ onlySelf: true });
    this.PartSupplierForm.get("PartNumber").enable({ onlySelf: true });
    this.modalService.dismissAll();
  }

  open(content, partSupplier?: any) {
    console.log(partSupplier);
    this.modalService.open(content, {
      size: "xl" as "lg",
      centered: true,
      backdrop: "static",
      windowClass: "formModal"
    });
    if (partSupplier) {
      this.edit(partSupplier);
    }
  }

  submitForm() {
    const url =
      this.partSupplierId > 0
        ? "PTLiveData/UpdatePartSupplierDetails"
        : "PTLiveData/InsertPartSupplierMapping";

    const body = this.PartSupplierForm.value;

    this.partSupplierId > 0
      ? (body.PartSupplierId = this.partSupplierId)
      : false;

    console.log(body);
    if (this.PartSupplierForm.valid) {
      this.jitService.call(url, body).subscribe(response => {
        console.log(response);
        this.partSupplierId > 0
          ?
          this.toastr.success("Supplier Part Successfully Updated") :
          this.toastr.success("Supplier Part Successfully Added");
        this.closeModal();
        this.getTableValues()
      });
    } else {
      this.toastr.error("Kindly Enter Valid Inputs");
    }
  }

  resetForm() {
    this.PartSupplierForm.reset();
  }

  edit(PartSupplier) {
    console.log(PartSupplier);
    this.partSupplierId = PartSupplier.partSupplierId;
    this.PartSupplierForm.get("ModelCode").patchValue(PartSupplier.modelCode);
    this.PartSupplierForm.get("ModelCode").disable({ onlySelf: true });
    this.PartSupplierForm.get("PartNumber").patchValue(PartSupplier.partNumber);
    this.PartSupplierForm.get("PartNumber").disable({ onlySelf: true });
    this.PartSupplierForm.get("PartDescription").patchValue(
      PartSupplier.partDescription
    );
    this.PartSupplierForm.get("Aggregates").patchValue(PartSupplier.aggregate);
    this.PartSupplierForm.get("PlantCode").patchValue(PartSupplier.plantCode);
    this.PartSupplierForm.get("SupplierCode").patchValue(
      PartSupplier.supplierCode
    );
  }

  delete(PartSupplier) {
    console.log(PartSupplier);
    console.log(this.DataSource)
    this.jitService
      .call("PTLiveData/DeletePartSupplierMapping", {
        PartSupplierId: PartSupplier.partSupplierId
      })
      .subscribe(response => {
        console.log(response);
        this.getTableValues();

        this.toastr.show("Part Supplier Mapped Removed Successfully");
      });
  }

  exportExcel(JSON) {
    let body = {
      PageStart: 0,
      RowPerPage: 10,
      ModelCode: this.modelCode.value,
      PartNumber: this.partCode.value,
      PlantCode: this.plantCode.value,
      SupplierCode: this.supplierCode.value
    }
    this.http.call("PTLiveData/GetPartSupplierMapping", body).subscribe(async (res) => {
      if (res.Message == "Success") {
        let dataSource = []
        res.ResponseData.forEach((data, index) => {
          dataSource.push({
            partSupplierId: data.PartSupplierId,
            modelCode: data.ModelCode,
            partNumber: data.PartNumber,
            partDescription: data.PartDescription,
            aggregate: data.Aggregates,
            plantCode: data.PlantCode,
            supplierCode: data.SupplierCode,
            isActive: data.IsActive,
          });
          })

        let dataNew = dataSource
        // this.DataSource;
        await console.log(dataNew);
        //Create workbook and worksheet
        let workbook = new Workbook();
        let worksheet = workbook.addWorksheet("ReportData");
        let columns = Object.keys(this.DataSource[0]);
        let headerRow = worksheet.addRow(columns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: "00FF0000" },
            bgColor: { argb: "00FF0000" },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
        });
        dataNew.forEach((d) => {
          // var obj = JSON.parse(d);
          var values = Object.keys(d).map(function (key) {
            return d[key];
          });
          let row = worksheet.addRow(values);
        });
        workbook.xlsx.writeBuffer().then((dataNew) => {
          let blob = new Blob([dataNew], {
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          fs.saveAs(
            blob,
            "Part Supplier Mapping.xlsx"
          );
        });
      }
    })
  }
}
