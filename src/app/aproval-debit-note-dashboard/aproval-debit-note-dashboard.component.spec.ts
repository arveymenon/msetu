import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AprovalDebitNoteDashboardComponent } from './aproval-debit-note-dashboard.component';

describe('AprovalDebitNoteDashboardComponent', () => {
  let component: AprovalDebitNoteDashboardComponent;
  let fixture: ComponentFixture<AprovalDebitNoteDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AprovalDebitNoteDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AprovalDebitNoteDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
