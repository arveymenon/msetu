import { Component, OnInit } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { MatIconRegistry } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { JitService } from '../services/jit/jit.service';


import * as fs from "file-saver";
import * as _ from 'lodash'
import * as moment from 'moment'
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { ToastrService } from 'ngx-toastr';
import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS } from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS} from '../cmsdashboard/date-format';
import * as CryptoJS from 'crypto-js';
import { Workbook } from 'exceljs';
@Component({
  selector: "app-aproval-debit-note-dashboard",
  templateUrl: "./aproval-debit-note-dashboard.component.html",
  styleUrls: ["./aproval-debit-note-dashboard.component.scss"],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]
})
export class AprovalDebitNoteDashboardComponent implements OnInit {
  selectCheck: false;
  pageSize2 = 0
  tracker: any;
  debitNote: any = {
    debitNoteid: 0,
    debitNoteNo: 0
  };

  debiteNoteDataSource = [
  ];
  debiteNoteHistoryDataSource = [
  ];

  debitNotedisplayedColumns = [
    // "selectCheck",
    "debitNoteNo",
    "supplierName",
    "Commodity",
    "createdOn",
    "createdBy",
    "Amount",
    "Attachment",
    "Comments",
    "Status",
    "action",
  ];
  debitNoteHistorydisplayedColumns = [
    "ID",
    "UpdatedBy",
    "UserName",
    "Amount",
    "createdOn",
    "Status",
    "Remarks",
  ];

  openSupplierModal(content, debitNote) {
    console.log(debitNote)
    this.debitNote.debitNoteNo = debitNote.debitNoteNo;
    this.debitNote.debitNoteid = debitNote.DebitNoteID;
    this.getHistory()


    this.modalService.open(content, {
      size: "xl" as "lg",
      centered: true,
      windowClass: "formModal",
    });
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  masterOptions: any = {};
  pageSize = 0;
  filterForm: FormGroup;
  constructor(
    public commonService: CommonUtilityService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private modalService: NgbModal,
    public formBuilder: FormBuilder,
    public jit: JitService,
    public cs: MySuggestionService,
    public toast: ToastrService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "viewIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/View_Icon_Red.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "tick",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/tick.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "close",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/close.svg"
      )
    );

    this.filterForm = this.formBuilder.group({
      debitNoteNumber: [],
      supplierCode: [],
      commodity: [],
      sapDebitNoteNo: [],
      fromDate: [],
      toDate: [],
      status: [],
    });

    this.jit.call('DebitNote/GetDebitNoteAdminMaster',null).subscribe(res=>{
      console.log(res);
      this.masterOptions = res.ResponseData
    })

    // this.search(1);
  }
// dd(dda){
//   return moment(dda.value).format('DD/MM/YYYY');
// }
  resetFilter(){
    this.filterForm.reset()
    this.search()
  }

  openFile(path){
    path ? window.open(path) : null
  }
  pageNo:any;
  search(pageNo?: any) {
    this.pageNo = pageNo;
    console.log(this.filterForm);
    let body: any = this.filterForm.value;
    this.debiteNoteDataSource = [];

    body.pageNo = pageNo + 1 || 1;
    body.fromDate = moment(this.filterForm.value.fromDate).add(1,'day')
    body.toDate = moment(this.filterForm.value.toDate).add(1,'day')
    body.debitNoteID = this.filterForm.value.debitNoteNumber
    body.UserName = CryptoJS.AES.decrypt(this.username,"").toString(CryptoJS.enc.Utf8);
    
    console.log(body);
    this.jit.call("DebitNote/GetDebitNoteAdmin", body).subscribe((res) => {
      console.log(res);
      if (res.Message == "Success") {
        let dataSource = [];
        this.pageSize = res.TotalCount ? res.TotalCount : 200 || 0;
        for (const data of res.ResponseData) {
          // if(data.Status == 'rejected' || data.Status == 'Pending'){
            dataSource.push({
              debitNoteId: data.DebitNoteID,
              debitNoteNo: data.DebitNoteID,
              sapDebitNoteNumber: data.SAPDebitNoteNumber,
              supplierName: data.SupplierName,
              Commodity: data.Commodity,
              createdOn: data.CreatedOn ? data.CreatedOn : 'N/A',
              createdBy: data.CreatedBy,
              Attachment: data.FileName,
              path: data.AttachmentPath, 
              Amount: data.Amount,
              remarks: new FormControl(),
              Comments:data.Comments,
              Status: data.Status,
              action: "",
            });
          // }
        }
        this.debiteNoteDataSource = [...dataSource];
      }
    });
  }

  
  getHistory(pageNo?: any){
    this.debiteNoteHistoryDataSource = []
      let body = {
        PageNo: pageNo + 1 || 1,
        debitNoteNumber : this.debitNote.debitNoteNo,
        debitNoteID : this.debitNote.debitNoteNo,
        status:null
      }
    this.jit.call("DebitNote/GetDebitNoteHistory", body).subscribe((res) => {
      console.log(res);
      if (res.Message == "Success") {
        let dataSource = [];
        this.pageSize2 = res.TotalCount ? res.TotalCount : 200 || 0;
        for (const data of res.ResponseData) {
          dataSource.push({
            ID: data.ID,
            UpdatedBy: data.UpdatedBy,
            Amount: data.Amount,
            createdOn: data.CreatedOn ? data.CreatedOn : 'N/A',
            Status: data.status,
            Remarks: data.Remarks,
            UserName: data.UserName
          });
        }
        this.debiteNoteHistoryDataSource = [...dataSource];
      }
    })    
  }

  export(table: any, name: any) {
    // const tempDisplayedColumns: any = _.cloneDeep(this.debitNotedisplayedColumns);
    // this.debitNotedisplayedColumns.splice(8, 1);
    // let tempDataSource = this.debiteNoteDataSource;

    this.getExportToExcelData().then((data: []) => {
     this.exportExcel(data)
    });
  }
  
  trackDebitNote(content, element) {
    console.log(element);
    this.tracker = null;
    this.cs
      .postMySuggestionData("DebitNote/GetDebitNoteByID", {
        DebitNote_ID: element.debitNoteNo,
      })
      .subscribe((res) => {
        console.log(res);
        this.tracker = res.ResponseData;
      });

    this.modalService.open(content, {
      size: "xl" as "lg",
      centered: true,
      windowClass: "formModal",
    });
  }

  exportExcel(json){
    let dataNew = json;
    console.log(dataNew);
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("ReportData");
    let columns = Object.keys(json[0]);
    let headerRow = worksheet.addRow(columns);
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "00FF0000" },
        bgColor: { argb: "00FF0000" },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
    });
    dataNew.forEach((d) => {
      // var obj = JSON.parse(d);
      var values = Object.keys(d).map(function (key) {
        return d[key];
      });
      let row = worksheet.addRow(values);
    });
    workbook.xlsx.writeBuffer().then((dataNew) => {
      let blob = new Blob([dataNew], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(
        blob,
        "DashboardDetails.xlsx"
      );
    });
  }

  getExportToExcelData() {
    return new Promise((res, rej) => {
      let body: any = this.filterForm.value;
      body.pageNo = -1;
      // this.searchValue.value || this.statusControl.value ? false : (body = null);
      console.log(body);
      this.jit
        .call("DebitNote/GetDebitNoteAdmin", body)
        .subscribe((response) => {
          console.log(response);
          // this.pageSize = response.TotalCount;
          // let tempDataSource = [];
          // let dataSource = [];

          // for (const data of response.ResponseData) {
          //   dataSource.push({
          //     debitNoteNo: data.DebitNoteID,
          //     supplierName: data.SupplierName,
          //     Commodity: data.Commodity,
          //     createdOn: data.CreatedOn ? data.CreatedOn : '',
          //     Author: data.CreatedBy,
          //     Amount: data.Amount,
          //     SAPDebitNoteNumber: data.SAPDebitNoteNumber,
          //     Comments: data.Comments,
          //     DH_x002d_QAApprovalStatus: data.DHQAApprovalStatus,
          //     VP_x002d_QAApprovalStatus: data.VP_QAApprovalStatus,
          //     GM_x002d_CDMMApprovalStatus: data.GM_CDMMApprovalStatus,
          //     WorkflowStatus: data.WorkflowStatus,
          //   });
          // }
          // tempDataSource = [...dataSource];
          // console.log(tempDataSource);
          res(response.ResponseData);
        });
    });
  }

  username:any;
  ngOnInit() {
    this.username = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==')
    this.search()
  }

  updateStatus(status, debitNote){
    console.log(debitNote.remarks.value)
    if(status == 'Rejected' && !(debitNote.remarks.value)){
      this.cs.showError('Kindly provide your remarks in the comment box to reject');
      return
    }

    const body = {
      debitNoteNumber : debitNote.debitNoteNo,
      sapDebitNoteNo : debitNote.sapDebitNoteNumber,
      debitNoteID : debitNote.debitNoteId,
      status: status,
      UserName: CryptoJS.AES.decrypt(this.username,"").toString(CryptoJS.enc.Utf8),
      remarks : debitNote.remarks.value,
      DebitNoteAmount :debitNote.Amount
    }
    console.log(body)
    this.jit
        .call("DebitNote/UpdateDebitNote", body)
        .subscribe((response) => {
          if(response.Message == "Success"){
            this.search()
            if (status == 'Rejected' ) {
              this.toast.success('Debit note No.'+debitNote.debitNoteNo+' has been rejected');
            } else {
              this.toast.success('Debit note No.'+debitNote.debitNoteNo+' has been approved');
            }
          }else{
            this.toast.error('Some Error Occured');
          }
        })
      
  }
}
