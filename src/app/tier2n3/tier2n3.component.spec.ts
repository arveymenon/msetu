import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Tier2n3Component } from './tier2n3.component';

describe('Tier2n3Component', () => {
  let component: Tier2n3Component;
  let fixture: ComponentFixture<Tier2n3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Tier2n3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Tier2n3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
