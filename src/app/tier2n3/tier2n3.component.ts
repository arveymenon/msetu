import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatIconRegistry } from '@angular/material';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-tier2n3',
  templateUrl: './tier2n3.component.html',
  styleUrls: ['./tier2n3.component.sass']
})
export class Tier2n3Component implements OnInit {
  QualityTierDataSource: MatTableDataSource<any>;
  supplierTierdisplayedColumns= ['ID','VendorName','VendorSize','VendorTier','Address','action']; 

  public supplierCode = CryptoJS.AES.decrypt(localStorage.getItem("WkcxV2RWcEhPWGxSTWpscldsRTlQUT09"),'').toString(CryptoJS.enc.Utf8);
  pageSize = 0;
  pageNo = 1
  Vendor_size=['Small', 'Medium','Large'];
  fetchYear:any='Previous';
  Tier_data_By_ID: any;

  year = 'Current'
  country = []
  city = []
  state = []

  public qualityTierForm = this._formBuilder.group({
    vendor_name :['', Validators.required],
    // Commodityof_Tier: ['', Validators.required],
    vendor_size: ['', Validators.required],
    // UAN: [''],
    // Type_of_Supplies: ['', Validators.required],
    // Vehicle_Model: ['', Validators.required],
    // Certification_to_Tier_II: ['', Validators.required],
    Vendor_Tier: [''],
    // Product_Family: [''],
    Address: [''],
    Country: ['', Validators.required],
    State: ['', Validators.required],
    City: ['', Validators.required],
    Zip_Code: ['', Validators.required],
    // Name: ['', Validators.required],
    // Cell_Number: ['', [Validators.required]],
    // Email_ID: ['', [Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]]
  });

  constructor(
    public cs: MySuggestionService,
    public commonService: CommonUtilityService,
    public _formBuilder: FormBuilder,
    public matIconRegistry: MatIconRegistry,
    public domSanitizer: DomSanitizer,
    public modalService: NgbModal
  ) {
    this.commonService.changeIsAuthenticate(true)
    matIconRegistry.addSvgIcon("edit", domSanitizer.bypassSecurityTrustResourceUrl("../assets/Icons/innerPages/Edit.svg"));

    this.getCountry();
   }

  ngOnInit() {
    this.getqualityTierData("Current",null,{pageIndex:0}).then(res=>{
      console.log(this.QualityTierDataSource)
    });
  }

  getCountry():Promise<any> {
    return new Promise((resolve:any)=>{
      let Request = {
        "GetCountryStateCity": "Country"
      }
      this.cs.postMySuggestionData('QualityTier2/GetCountryStateCity', Request).subscribe((res: any) => {
        if (res.Message == "Success") {
          this.country = res.ResponseData;
          resolve();
        }
        else{
          this.cs.showError("Something went wrong please try again later");
          return;
        }
      })
    })
  }
  getState(event:any):Promise<any>{
    return new Promise((resolve:any)=>{
      let Request = {
        "GetCountryStateCity": "State",
        "Country": event.value
    }
      this.cs.postMySuggestionData('QualityTier2/GetCountryStateCity', Request).subscribe((res: any) => {
        if (res.Message == "Success") {
          this.state = res.ResponseData;
          resolve();
        }
        else{
          this.cs.showError("Something went wrong please try again later");
          return;
        }
      })
    })

  }

  getCity(event:any):Promise<any>{
    return new Promise((resolve:any)=>{
      let Request = {
        "Country":"India", 
        "State": event.value,
        "GetCountryStateCity": "City"
    }
      this.cs.postMySuggestionData('QualityTier2/GetCountryStateCity', Request).subscribe((res: any) => {
        if (res.Message == "Success") {
          this.city = res.ResponseData;
          resolve();
        }
        else{
          this.cs.showError("Something went wrong please try again later");
          return;
        }
      })
    })
  }


  getqualityTierData(year,SupId,event:any):Promise<any>{
    let Year = (year == 'Current') ? 'FY 2020' : 'FY 2019'; 
    return new Promise((resolve:any)=>{
      let Request={
        "SupCode": this.supplierCode,
        "SchID": Year,
        "SupId":SupId,
        "GetAllData":"",
        "PageNo":event.pageIndex+1
        }
        // this.fetchYear=='Previous'?this.fetchYear='Current':this.fetchYear='Previous';
        this.cs.postMySuggestionData('QualityTier2/GetSupplierTierInfo',Request).subscribe((res:any)=>{    
          this.year = (year == 'Current') ? 'Previous' : 'Current'; 

             if(!this.cs.isUndefinedORNull(res)&&res.Message=="Success"){
               this.fetchYear = 'Current';
              if(res.ResponseData.length >0)
               this.pageSize = res.ResponseData[0].TotalCount;
               this.QualityTierDataSource = new MatTableDataSource(res.ResponseData);
              //  this.QualityTierDataSource = res.ResponseData;
               this.QualityTierDataSource.filterPredicate = (d: any, filter: string) => {
                const textToSearch = d['VendorName'] && d['VendorName'].toLowerCase() || '';
                return textToSearch.indexOf(filter) !== -1;
              };
              resolve();
             }else{
              this.pageSize = 0;
              resolve();
             }
        })
    })
  }

  applyFilter(filterValue: string) {
    this.QualityTierDataSource.filter = filterValue.trim().toLowerCase();
  }

  
  btn_name: any;
  SupID: any;

  openSupplierModal(content, element, btn_name) {
    this.btn_name = btn_name;
    this.SupID = element.ID
    this.qualityTierForm.reset();
    if (!this.cs.isUndefinedORNull(element) && btn_name == 'Update') {
       this.getqualityTierDataByID("FY 2020",element.ID).then((data:any)=>{
        this.qualityTierForm.get('vendor_name').setValue(this.Tier_data_By_ID[0]['VendorName']);
        // this.qualityTierForm.get('Commodityof_Tier').setValue(this.Tier_data_By_ID[0]['CommodityOfTierI']);
        this.qualityTierForm.get('vendor_size').setValue(this.Tier_data_By_ID[0]['VendorSize']);
        // this.qualityTierForm.get('UAN').setValue(this.Tier_data_By_ID[0]['UAN']);
        // this.qualityTierForm.get('Type_of_Supplies').setValue(this.Tier_data_By_ID[0]['TypeOfSupplies']);
        // this.qualityTierForm.get('Vehicle_Model').setValue(this.Tier_data_By_ID[0]['VehicleModel']);
        // this.qualityTierForm.get('Certification_to_Tier_II').setValue(this.Tier_data_By_ID[0]['CertificationToTierII']);
        this.qualityTierForm.get('Vendor_Tier').setValue(this.Tier_data_By_ID[0]['VendorTier']);
        // this.qualityTierForm.get('Product_Family').setValue(this.Tier_data_By_ID[0]['ProductFamily']);
        this.qualityTierForm.get('Address').setValue(this.Tier_data_By_ID[0]['FullAddress']);
        this.qualityTierForm.get('Zip_Code').setValue(this.Tier_data_By_ID[0]['PostalCode']);
        // this.qualityTierForm.get('Name').setValue(this.Tier_data_By_ID[0]['Name']);
        // this.qualityTierForm.get('Cell_Number').setValue(this.Tier_data_By_ID[0]['CellNumber']);
        // this.qualityTierForm.get('Email_ID').setValue(this.Tier_data_By_ID[0]['EmailID']); 
      })
    }
    this.modalService.open(content, { size: 'xl' as 'lg', centered: true, windowClass: "formModal" });
    }

    getqualityTierDataByID(Year,ID):Promise<any>{
      return new Promise((resolve:any)=>{
        let Request={
          "SupCode": this.supplierCode,
          "SchID": Year,
          "SupId":ID,
          "GetAllData":""
          }
          this.cs.postMySuggestionData('QualityTier2/GetSupplierTierInfo',Request).subscribe((res:any)=>{
               if(!this.cs.isUndefinedORNull(res)&&res.Message=="Success"){
                 this.Tier_data_By_ID = res.ResponseData;
                 resolve();
               }
          })
      })
    }

    
addqualityTierData(){
  if(this.qualityTierForm.valid){
    let url;
    let Request={
      "VendorName":this.qualityTierForm.controls.vendor_name.value,
      "VendorSize":this.qualityTierForm.controls.vendor_size.value,
      "VendorTier": this.qualityTierForm.controls.Vendor_Tier.value,
      "ParentCode":this.supplierCode,
      "PostalCode":this.qualityTierForm.controls.Zip_Code.value,
      "FullAddress":this.qualityTierForm.controls.Address.value,
      "lat":"",
      "lng":"",
      "SchID":"FY 2020",
      "City":this.qualityTierForm.controls.City.value,
      "State":this.qualityTierForm.controls.State.value,
      "Country":this.qualityTierForm.controls.Country.value,

      "CommidityTierI": null,
      "TypeOFSupplies": null,
      "VModel": null,
      "cerTierII":null,
      "cellNumber": null,
      "Email": null,
      "ProductFamily": null,
      "UAN": null,
      "Name": null
    }
    if (this.btn_name == "Submit") {
      url = "QualityTier2/AddSupplierTierInfo"
    }
    else {
      Request['SupID']=this.SupID;
      url = "QualityTier2/UpdateSupplierTierInfo"
    }
    this.cs.postMySuggestionData(url, Request).subscribe((res: any) => {
      if (!this.cs.isUndefinedORNull(res) && res.Message == "Success") {
        this.getqualityTierData("FY 2020",null,{pageIndex:0});
        // this.cs.showSuccess("Data " + (this.btn_name == "Submit"?"inserted":"updated")+" successfully");
        this.qualityTierForm.reset();
        this.closeModal();
      }
      else{
        this.cs.showError("Something went wrong please try again later");
        this.qualityTierForm.reset();
        return;
      }
    })
  }
  else{
      this.cs.showError("Please enter all mandatory fields");
      return;
    }
  }

  numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
        }
        return true;
    
 }
 
  lettersOnly(event){
        const charCode = (event.which) ? event.which : event.keyCode;
        console.log(charCode)
        if(!(charCode >= 65 && charCode <= 122) && (charCode != 32 && charCode != 0)) { 
          event.preventDefault(); 
      }
 }

  closeModal() {
    this.modalService.dismissAll();
  }

}
