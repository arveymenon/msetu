import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtCodeOfConductionComponent } from './ext-code-of-conduction.component';

describe('ExtCodeOfConductionComponent', () => {
  let component: ExtCodeOfConductionComponent;
  let fixture: ComponentFixture<ExtCodeOfConductionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtCodeOfConductionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtCodeOfConductionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
