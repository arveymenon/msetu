import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import * as fs from "file-saver";
import { Workbook } from 'exceljs';

@Component({
  selector: 'app-supplier-list',
  templateUrl: './supplier-list.component.html',
  styleUrls: ['./supplier-list.component.sass']
})
export class SupplierListComponent implements OnInit {

  constructor(
    private matIconRegistry: MatIconRegistry,

    public cs: MySuggestionService
  ) { }
  RoleID:any;
  DataSource:any=[];
  displayedColumns: string[]
  ngOnInit() {
    // this.RoleID = localStorage.getItem("WTIwNWMxcFZiR3M9");
    // this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(
    //   CryptoJS.enc.Utf8
    // );

  
    this.displayedColumns = ["ModuleName", "Stakeholder", "EmailId", "Mobile","fname","Lname","email","number","type","code","sector","commidity"];
    this.getSupplierData();
  }

  async getSupplierData() {
    await this.cs
      .getMySuggestionData("Vendor/GetAllTempVendorDetails")
      .subscribe(async (response) => {
        console.log(response);
        await console.log("Loading Response");
        this.DataSource = [...response.ResponseData];
      });
    await console.log(this.DataSource);
  }

  Json =[];
  download() {
  
    this.cs
    .getMySuggestionData("Vendor/GetAllTempVendorDetails")
    .subscribe((res: any) => {
      console.log(res);
      if (res.Message == "Success") {
        this.Json = res.ResponseData;
        let dataNew = this.Json;
        console.log(dataNew);
        //Create workbook and worksheet
        let workbook = new Workbook();
        let worksheet = workbook.addWorksheet("ReportData");
        let columns = Object.keys(this.Json[0]);
        let headerRow = worksheet.addRow(columns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: "00FF0000" },
            bgColor: { argb: "00FF0000" },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
        });
        dataNew.forEach((d) => {
          // var obj = JSON.parse(d);
          var values = Object.keys(d).map(function (key) {
            return d[key];
          });
          let row = worksheet.addRow(values);
        });
        workbook.xlsx.writeBuffer().then((dataNew) => {
          let blob = new Blob([dataNew], {
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          fs.saveAs(
            blob,
            "CMSSurvey" +".xlsx"
          );
        });
      }
    });
  }
}
