import { Component, OnInit, Input, Inject, ViewChild } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';


import { SurveyService } from '../services/survey/survey.service';
import { Location } from '@angular/common';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';

@Component({
  selector: 'app-add-question-modal',
  templateUrl: './add-question-modal.component.html',
  styleUrls: ['./add-question-modal.component.scss']
})
export class AddQuestionModalComponent implements OnInit {
  // params from the CMSSurvey page
  @Input() public surveyId;
  @Input() public surveyTypeId;

  questionCreateForm: any = FormGroup;
  options = new FormArray([]);

  public question_id: 0;

  public option_type = '';

  public addedQuestions = [];
  public categories = [];
  public sections = [];

  public answerTypes = [
    // {value: 1, text: 'Input'},
    // {value: 2, text: 'Radio'},
    // {value: 3, text: 'Range'},
  ];

  pageSize: any;
  pageNo: any = '0';

  constructor(
    public modalService: NgbModal,
    public formBuilder: FormBuilder,
    public _location: Location,
    public surveyService: SurveyService,
    public cs: MySuggestionService
    ) {
   }

  ngOnInit() {
    console.log(this.surveyId);
    console.log(this.surveyTypeId);

    this.surveyService.getSurveySectionsAndCategories('AdminCrudSurvey/Get_Section_CategoryMasterDetails', null).subscribe((res: any) => {
        console.log(res);
        if (res.Message == 'Success') {
          this.categories = res.ResponseData._GetCategoryMaster;
          this.sections = res.ResponseData._GetSectionMaster;
          this.answerTypes = res.ResponseData._GetAnswerTypesMaster;
        }
      });

    this.getStickerData({ pageIndex: 0, pageSize: 10, length: this.pageSize });
    this.questionCreateForm = this.formBuilder.group({
      question: ['', Validators.required],
      answertype: ['', Validators.required],
      section: [],
      category: [],
      number_of_options: [],
    });

    this.questionCreateForm.controls.answertype.valueChanges.subscribe(res => {
      console.log(res);
      if (res == 1) {
          this.questionCreateForm.controls['number_of_options']
            .setValidators(Validators.required);
          this.questionCreateForm.get('number_of_options').updateValueAndValidity();
        } else {
          this.questionCreateForm.controls['number_of_options']
          .clearValidators();
          this.questionCreateForm.get('number_of_options').updateValueAndValidity();
        }
      });
    }

  setNumberOfOption(event, type?: any){
      console.log(event);
      console.log(type);
      console.log(this.questionCreateForm);
      this.emptyOptions().then(res => {
        console.log(res);
      });
    }

  setOptionFields(number_of_options, placeholder) {
    // console.log(number_of_options);
    this.emptyOptions().then((res: any) => {
      console.log(res);
      if (res) {
        for (let i = 0; i < number_of_options; i++) {
          // this.setOptionFields('Option '+(i+1));
          this.options.push(this.formBuilder.group({
            name: ['', Validators.required],
            placeholder: [placeholder + ' ' + (i + 1)]
          }));
          console.log(this.options);
          console.log('eg', this.questionCreateForm);
          }
      }
    });

    }

  emptyOptions() {
    return new Promise((resolve, reject) => {
      // }
      console.log(this.questionCreateForm);

      // this.questionCreateForm.value.options = [];
      while(this.options.length !== 0){
        this.options.removeAt(0);
      }
      resolve(this.questionCreateForm);
     });
    }

    @ViewChild('newform')newform:any;

  addQuestion(){
      console.log(this.questionCreateForm);
      console.log(this.questionCreateForm.valid);
      console.log(this.options.valid);
      // this.modalService.dismissAll
      // var modal: NgbModalOptions;
      // modal.
      if (this.questionCreateForm.valid && this.options.valid) {
        console.log(this.questionCreateForm.value);
        console.log(this.options.value);
        let body: any = {
          Question: this.questionCreateForm.value.question,
          AnswerTypeId: this.questionCreateForm.value.answertype,
          SurveyID: this.surveyId,
          SurveyTypeID: this.surveyTypeId,
          Sec_id: this.questionCreateForm.value.section,
          Cat_id: this.questionCreateForm.value.category,
          Question_OptionsList: []
          };
        const optionsParam: any = {};
        if (this.questionCreateForm.value.number_of_options > 1) {
          //  body.Question_OptionsList = 1;

          optionsParam.NoOfOptions = this.questionCreateForm.value.number_of_options;

          if (this.options.value[0]) {
            optionsParam.Option_One = this.options.value[0].name;
          }

          if (this.options.value[1]) {
            optionsParam.Option_Two = this.options.value[1].name;
          }

          if (this.options.value[2]) {
            optionsParam.Option_Three = this.options.value[2].name;
          } else {
            optionsParam.Option_Three = null;
          }

          if (this.options.value[3]) {
            optionsParam.Option_Four = this.options.value[3].name;
          } else {
            optionsParam.Option_Four = null;
          }

          if (this.options.value[4]) {
            optionsParam.Option_Five = this.options.value[4].name;
          } else {
            optionsParam.Option_Five = null;
          }

          body.Question_OptionsList.push(optionsParam);
        } else {
          body.Question_OptionsList = null;
        }
        console.log(body);
        body.questionId = this.question_id;
        this.surveyService.postCreateSurveyForm('AdminCrudSurvey/Insert_SurveyQuestions', body)
          .subscribe(res => {
            console.log(res);
            if(res.Message = 'Success') {
              this.questionCreateForm.reset();
              this.emptyOptions();
              this.getPartSticker({pageIndex: this.pageNo})
              this.newform.resetForm();
            }
          });
      } else {
        console.log('error');
      }
    }

  getStickerData(event) {
    this.getPartSticker(event);
  }

  getPartSticker(event) {
    console.log(event);
    console.log(event.pageIndex);
    this.pageNo = event.pageIndex;
    const reqjson = {
        SurveyID: this.surveyId,
        PageNo: this.pageNo + 1
      }
    console.log(reqjson);
    this.surveyService.getQuestions('AdminCrudSurvey/Get_Questions', reqjson).subscribe((resp: any) => {
    // this.printPartDatasource = resp.ResponseData;
      console.log(resp);
      if  (resp.Message == 'Success') {
        this.pageSize = resp.TotalCount;
        this.addedQuestions = resp.ResponseData;
        this.addedQuestions.reverse();
        }
      }, (err) => {

      });
    }

  closeModal() {
      this.modalService.dismissAll();
      // this._location.reload();
    }

  deleteQuestion(questionId) {
      console.log(questionId);
      let body = {
        questionId: questionId
      }
      this.surveyService.deleteSurveyQuestion('AdminCrudSurvey/Delete_SurveyQuestion', body).subscribe((res: any) => {
        console.log(res);
        if (res.Message == 'Success') {
          console.log(this.addedQuestions);
          console.log(this.addedQuestions.findIndex(o => o.QuestionID == questionId));
          this.addedQuestions.splice(this.addedQuestions.findIndex(o => o.QuestionID == questionId), 1);
        }
      });
    }
    
    publishQuestion(){
      console.log('Survey_ID', this.surveyId);
      let body = {
        SurveyID: this.surveyId
      }
      this.surveyService.publishSurvey('AdminCrudSurvey/PublishSurvey', body).subscribe((res: any) => {
        console.log(res);
        if (res.Message == 'Success') {
          this.cs.showSuccess("Question published successfully")
          this.modalService.dismissAll();
          // this.addedQuestions.splice(this.addedQuestions.findIndex(o => o.QuestionId == questionId), 1)
        }
      })
  }

}
