import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonUtilityService {
  navchange: EventEmitter<any> = new EventEmitter(); 
  public IsAuthenticate:boolean=false;
  // public IsLoginFlag:boolean;
  constructor() { }
  changeIsAuthenticate(val:boolean){
    this.IsAuthenticate=val;
 //   this.IsLoginFlag=val1;
    this.navchange.emit(this.IsAuthenticate);
  }
  // changeIsLoginFlag(val:boolean){
  //   this.IsLoginFlag=val;
  //   this.navchange.emit(this.IsLoginFlag);
  // }

}
