import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CocDashboardComponent } from './coc-dashboard.component';

describe('CocDashboardComponent', () => {
  let component: CocDashboardComponent;
  let fixture: ComponentFixture<CocDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CocDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CocDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
