import { Component, OnInit } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { MyHelpDeskService } from "../services/myHelpDesk/my-help-desk.service";
import * as _ from "lodash";
import { DomSanitizer } from "@angular/platform-browser";
import { MatIconRegistry, DateAdapter, MAT_DATE_FORMATS } from "@angular/material";
import { Router } from "@angular/router";

import * as CryptoJS from 'crypto-js';
import * as moment from 'moment';
import { NgModel, FormControl } from '@angular/forms';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { AppDateAdapter, APP_DATE_FORMATS } from '../cmslanding/date-adapter';

@Component({
  selector: 'app-coc-dashboard',
  templateUrl: './coc-dashboard.component.html',
  styleUrls: ['./coc-dashboard.component.scss'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]
})
export class CocDashboardComponent implements OnInit {
  roleId = localStorage.getItem('WTIwNWMxcFZiR3M9');
  pageNo = 1;
  pageSize = 100;
  userToken = CryptoJS.AES.decrypt(localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),"").toString(CryptoJS.enc.Utf8);
  DataSource: any = [];
  yearDDL: NgModel
  status: NgModel
  displayedColumns: string[] = []

  fromDate = new FormControl('')
  toDate = new FormControl('')

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public commonService: CommonUtilityService,
    public router: Router,
    public http: MyHelpDeskService,
    public cs: MySuggestionService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );

    this.fromDate.valueChanges.subscribe(res=>{
      console.log(res)
      this.getData(1)
    })
    this.toDate.valueChanges.subscribe(res=>{
      console.log(res)
      this.getData(1)
    })
  }

  ngOnInit() {
    this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8);
    this.getData(1);
  }

  getData(pageNo) {
    let body = {
      "code": null,
      "fromDate": this.fromDate.value ? moment(new Date(this.fromDate.value)).format('YYYY-MM-DD') : null,
      "toDate": this.toDate.value ? moment(new Date(this.toDate.value)).format('YYYY-MM-DD') : null,
      "pageNo": pageNo
    }
    this.http.call('SupplierDealerCOC/GetAll_SupplierDealerCOC', body).subscribe(res=>{
      console.log(res)
      if(res.ID == 1){
        this.displayedColumns = Object.keys(res.ResponseData[0])
        this.DataSource = res.ResponseData
        this.pageSize = res.TotalCount
      }
    })
  }

  download(){
    let body = {
      "code": null,
      "fromDate": null,
      "toDate": null,
      "pageNo": 0
    }
    this.http.call('SupplierDealerCOC/GetAll_SupplierDealerCOC', body).subscribe(res=>{
      console.log(res)
      if(res.ID == 1){
        // this.displayedColumns = Object.keys(res.ResponseData[0])
        // this.DataSource = res.ResponseData
        this.cs.exportJsonAsExcelFile(res.ResponseData,'Coc_Responses')
      }
    })
  }

  view(link){
    window.open(link,'_blank')
  }

}
