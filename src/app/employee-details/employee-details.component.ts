import { Component, OnInit } from '@angular/core';
import { Workbook } from 'exceljs';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';

import * as fs from "file-saver";
@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.scss']
})
export class EmployeeDetailsComponent implements OnInit {
  DDLValue = ''
  Vendor_Code_DDL = []

  reqRoleId = ''
  roleDetails_DDL = []

  filterText = ''

  displayedColumns3: string[] = [
    "EmployeeName",
    "VendorCode",
    "Role",
    "UserName",
    "Email",
    "primarRole",
    "commodity",
    "location",
    "modifiedBy",
    "modifiedDate"
  ];

  updatedEmpDetails = []
  empDetailsCount: number = 100
  pageNo: number = 0

  constructor(public cs: MySuggestionService) {
    while(this.Vendor_Code_DDL.length == 0){
      setTimeout(()=>{
        this.getVendorDDL();
        this.getRolesDDL();
      },1000)
    }
  }
  
  ngOnInit() {
    this.getUpdateEmpDetails(1, "", "", 0);
  }

  tempbody: any
  getUpdateEmpDetails(
    pageNo: any,
    VendorCode: any,
    roleID: any,
    filternbr?: any
  ) {
    let postData = {
      PageNo: pageNo,
      filterText: this.filterText,
      VendorCode: this.cs.isUndefinedORNull(VendorCode) ? "" : VendorCode,
      RoleId: this.cs.isUndefinedORNull(roleID) ? 0 : roleID,
    };
    this.tempbody = postData;
    this.cs.postMySuggestionData("AD/GetUserList", postData).subscribe(
      (res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.updatedEmpDetails = res.ResponseData;
          // this.updatedEmpDetails = [...this.updatedEmpDetails];
          this.empDetailsCount = res.TotalCount;
          // this.cs.showSuccess("Success");
        } else {
          this.cs.showSwapLoader = false;
          this.cs.showError("Something went wrong please try again later!");
        }
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    );
  }

  getVendorDDL() {
    this.cs.postMySuggestionData("Vendor/GetVendorDDL", "").subscribe(
      (res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.Vendor_Code_DDL = res.ResponseData;
          // this.cs.showSuccess("Success");
        } else {
          this.cs.showSwapLoader = false;
          this.cs.showError("Something went wrong please try again later!");
        }
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    );
  }

  getRolesDDL() {
    this.cs.postMySuggestionData("CRUDMasterRole/GetRoleDDL", "").subscribe(
      (res: any) => {
        this.roleDetails_DDL = res.ResponseData;
      },
      (error) => {
        console.log(error.message);
      }
    );
  }

  resetgetUpdateEmpDetails() {
    this.DDLValue = "";
    this.reqRoleId = "";
    this.getUpdateEmpDetails(1, "", 0, 1);
  }

  exportAsExcelFile(table: any, name: any) {
    this.getExportToExcelData2();
  }

  getExportToExcelData2() {
    let postData = {
      PageNo: 0,
      VendorCode: "",
      RoleId: 0,
    };
    this.cs
      .postMySuggestionData("AD/GetUserList", postData)
      .subscribe(async (data) => {
        let json = data.ResponseData.map((e) => {
          return {
            'Employee Name': e.FirstName+ e.LastName,
            'Vendor Code': e.VendorCode,
            'Role Name': e.RoleName,
            'Username': e.UserName,
            'Email': e.Email,
            'Primary Role Name': e.PrimaryRoleName,
            'Commodity': e.Commodity,
            'Location': e.OfficeLocation
          }
        });
        await this.exportExcel(json);
      });
  }

  exportExcel(json){
    let dataNew = json;
    console.log(dataNew);
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("ReportData");
    let columns = Object.keys(json[0]);
    let headerRow = worksheet.addRow(columns);
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "00FF0000" },
        bgColor: { argb: "00FF0000" },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
    });
    dataNew.forEach((d) => {
      // var obj = JSON.parse(d);
      var values = Object.keys(d).map(function (key) {
        return d[key];
      });
      let row = worksheet.addRow(values);
    });
    workbook.xlsx.writeBuffer().then((dataNew) => {
      let blob = new Blob([dataNew], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(
        blob,
        "DashboardDetails.xlsx"
      );
    });
  }

 
  getPageChangeData(ev, id) {
    this.getUpdateEmpDetails(
      ev.pageIndex + 1,
      this.DDLValue,
      this.reqRoleId,
      0
    );
  }

}
