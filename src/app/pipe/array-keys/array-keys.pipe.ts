import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayKeys'
})
export class ArrayKeysPipe implements PipeTransform {

  transform(value, args:string[]) : any {
    return Object.keys(value);
  }

}
