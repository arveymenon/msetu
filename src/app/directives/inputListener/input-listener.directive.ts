import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appInputListener]'
})
export class InputListenerDirective {

  specialCharacters = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;

  constructor(public el: ElementRef) { 
    // this.validate()
  }
  
  @HostListener('keypress', ['$event']) onKeyDown(event: KeyboardEvent) {
    return this.validate(event.key)
  }
  
  @HostListener('paste', ['$event']) onPaste(event: any) {
    console.log(event)
    let paste = event.clipboardData.getData('text');
    paste = paste.toUpperCase();
    return this.validate(paste)
  }
  
  // @HostListener('paste', ['$event']) onPaste(event: ClipboardEvent) {
  //   // return this.validate(event.clipboardData.getData('text'))
  // }
  
  validate(string){
    if(this.specialCharacters.test(string)){
      return false;
    }
  }

}
