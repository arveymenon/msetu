import { Component, OnInit } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";

import * as CryptoJS from "crypto-js";
import { Router } from "@angular/router";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from "@angular/forms";
import { Validatable } from "@amcharts/amcharts4/core";
import { MyHelpDeskService } from "../services/myHelpDesk/my-help-desk.service";
import { Location } from "@angular/common";

@Component({
  selector: "app-tcs-survey",
  templateUrl: "./tcs-survey.component.html",
  styleUrls: ["./tcs-survey.component.scss"],
})
export class TcsSurveyComponent implements OnInit {
  ParentVendorCode: String = CryptoJS.AES.decrypt(
    localStorage.getItem("MV"),
    ""
  ).toString(CryptoJS.enc.Utf8);
  SubVendorCode: String = CryptoJS.AES.decrypt(
    localStorage.getItem("WkcxV2RWcEhPWGxSTWpscldsRTlQUT09"),
    ""
  ).toString(CryptoJS.enc.Utf8);
  VendorName: String = CryptoJS.AES.decrypt(
    localStorage.getItem("VlROV2QyTkhlSEJhV0VwUFdWY3hiQT09"),
    ""
  ).toString(CryptoJS.enc.Utf8);
  UserToken: String = CryptoJS.AES.decrypt(
    localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),
    ""
  ).toString(CryptoJS.enc.Utf8);

  show = false;
  tcsForm: FormGroup;
  constructor(
    private commonUtility: CommonUtilityService,
    private router: Router,
    private cs: MySuggestionService,
    private formBuilder: FormBuilder,
    private http: MyHelpDeskService,
    public _location: Location
  ) {
    this.commonUtility.changeIsAuthenticate(true);
    console.log(this.ParentVendorCode, this.SubVendorCode, this.VendorName);
    if (!this.SubVendorCode) {
      this.cs.showError("Kindly Login To Access This Page");
      this.router.navigateByUrl("/login");
    }
    let body = {
      actionType: "ISEXIST",
      subVendorCode: this.SubVendorCode,
    };
    this.http.call("TCSSurvey/TCSSurvey", body).subscribe((res) => {
      console.log(res);
      if(res.ID == 1){
        this.tcsForm = this.formBuilder.group({
          PAN: ["", Validators.required],
          TAN: ["", Validators.required],
          q1: ["", Validators.required],
          q2: ["", Validators.required],
          remark: [""]
        });
        this.show = true
      }else{
        this.show = false
        this.cs.showSuccess('Survey Has Already Been Submitted')
      }
    });
  }

  submit() {
    console.log(this.tcsForm.valid);
    console.log(this.tcsForm.value);
    if (this.tcsForm.valid) {
      this.cs.showSwapLoader = true;
      let body = {
        actionType: "INSERT",
        parentVendorCode: this.ParentVendorCode,
        subVendorCode: this.SubVendorCode,
        vendorName: this.VendorName,
        pan: this.tcsForm.value.PAN,
        taN_Number: this.tcsForm.value.TAN,
        isTurnoverMoreThan10CR: this.tcsForm.value.q1,
        isTCSApplicable: this.tcsForm.value.q2,
        remarks: this.tcsForm.value.remark,
        createdBy: this.UserToken,
      };
      this.http.call("TCSSurvey/TCSSurvey", body).subscribe((res) => {
        console.log(res);
        if (res.ID == 1) {
          this.cs.showSwapLoader = false;
          this.cs.showSuccess("Submitted Successfully");
          this._location.back();
        } else {
          // this.cs.showSuccess('Submitted Successfully');
        }
      });
    } else {
      this.cs.showError("Kindly Fill All Questions In The Form");
    }
  }

  ngOnInit() {}
}
