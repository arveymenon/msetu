import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TcsSurveyComponent } from './tcs-survey.component';

describe('TcsSurveyComponent', () => {
  let component: TcsSurveyComponent;
  let fixture: ComponentFixture<TcsSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TcsSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TcsSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
