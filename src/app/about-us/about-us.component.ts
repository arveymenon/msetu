import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  mailText:string = "";
  constructor(private modalService: NgbModal,private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer) { 
    matIconRegistry.addSvgIcon('checkIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/checked.svg'));
     matIconRegistry.addSvgIcon('arrowIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Iconarrow.svg'));
  }
  onTabChange(event){
    if(event.index == '1'){
     window.open('https://www.mahindrarise.com/', "_blank");
    }
  }
  ngOnInit() {
    this.mailText = "mailto:whistleblower.mahindra@ethicshelpline.in";
  }

}
