import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCMSSurveyComponent } from './create-cmssurvey.component';

describe('CreateCMSSurveyComponent', () => {
  let component: CreateCMSSurveyComponent;
  let fixture: ComponentFixture<CreateCMSSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCMSSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCMSSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
