import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SurveyService } from '../services/survey/survey.service';
import { AppDateAdapter, APP_DATE_FORMATS} from '../cmslanding/date-adapter'; 
import { MatIconRegistry,DateAdapter,MAT_DATE_FORMATS } from '@angular/material';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-cmssurvey',
  templateUrl: './create-cmssurvey.component.html',
  styleUrls: ['./create-cmssurvey.component.scss'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]
})
export class CreateCMSSurveyComponent implements OnInit {
  @Input() survey;

  date = Date;
  maxDate: any;
  minDate = new Date();
  toDateMinDate = new Date();
  createSurvey: FormGroup;

  assignTo = [];
  surveyType = [];
  plants = [];
  constructor(
    private  router: Router,
    public modalService: NgbModal,
    public formBuilder: FormBuilder,
    public surveyService: SurveyService,
    public toastr: ToastrService
    ) { }

  ngOnInit() {
    console.log(this.survey);
    // this.minDate = new Date();
    if(this.survey){
      this.createSurvey = this.formBuilder.group({
        SurveyAssignTo: [this.survey.SurveyAssignTo],
        SurveyTitle: [this.survey.SurveyTitle, Validators.required],
        SurveyTypeID: [this.survey.type, Validators.required],
        From_date: [this.minDate, Validators.required],
        To_date : [this.survey.Todate,Validators.required],
        SurveyDescription: [this.survey.SurveyDescription]
      })
      this.createSurvey.updateValueAndValidity()
    }else{
      this.createSurvey = this.formBuilder.group({
        SurveyAssignTo: ['',Validators.required],
        SurveyTitle: ['',Validators.required],
        SurveyTypeID: ['',Validators.required],
        From_date: ['',Validators.required],
        To_date : ['',Validators.required],
        SurveyDescription: [''],
        PlantName :['']
      })
    }
    this.createSurvey.get('SurveyTypeID').valueChanges.subscribe(res=>{
      console.log(res)
      this.createSurvey.get("PlantName").setValue('');
      this.createSurvey.get("PlantName").setValidators([]);
      this.createSurvey.get("PlantName").updateValueAndValidity();
    })

    this.surveyService.createFormOptions('AdminCrudSurvey/Get_SurveyMasterDetails').subscribe((res: any)=>{
      console.log(res);
      if (res.Message == 'Success') {
        this.assignTo = res.ResponseData._GetRoleMaster;
        this.surveyType = res.ResponseData._GetServeyTypeMaster;
        // this.plants = res.ResponseData._GetPlantMaster;

        // this.createSurvey.controls.From_date.valueChanges.subscribe((res: any) => {
        //   console.log(res);
        //   this.createSurvey.controls.From_date.setValue(moment(res).format('DD-MM-YYYY'));
        // });
      }
    });
    this.createSurvey.get('From_date').valueChanges.subscribe(fromDate=>{
      console.log(fromDate)
      if(fromDate){
        console.log(this.createSurvey.get('To_date').value)
        this.toDateMinDate = new Date(fromDate)
        if(this.createSurvey.get('To_date').value){
          console.log('compare from and to date')
          this.maxDate = new Date(this.createSurvey.get('To_date').value)
        }
      }
    })
    this.createSurvey.get('To_date').valueChanges.subscribe(toDate=>{
      console.log(toDate)
      if(toDate){
        console.log(this.createSurvey.get('From_date').value)
        this.maxDate = new Date(toDate)
        if(!this.createSurvey.get('From_date').value){
          this.toDateMinDate = new Date()
        }
      }
    })
  }

  openServeyList() {
    console.log(this.createSurvey)
    // console.log(this.createSurvey.controls.title.valid)
    if(this.createSurvey.valid){
      this.createSurvey.value.From_date = moment(this.createSurvey.value.From_date).format('YYYY-MM-DD HH:mm:ss.SSS')
      this.createSurvey.value.To_date = moment(this.createSurvey.value.To_date).format('YYYY-MM-DD HH:mm:ss.SSS')

      let body: any = this.createSurvey.value;
      body.SurveyId = 0
      if(this.survey){
        body.SurveyId = this.survey.SurveyId || 0
        body.SurveyAssignTo = this.createSurvey.value.SurveyAssignTo || this.survey.SurveyAssignTo
      }
      this.surveyService.postCreateSurveyForm('AdminCrudSurvey/InsertSurvey', body).subscribe((res: any)=>{
        console.log(res)
        if(res.Message == 'Success' && res.ResponseData != -1){
          this.modalService.dismissAll();
          this.toastr.success('Survey Created Successfully')
          // location.reload();
          this.router.navigateByUrl('CMSSurvey');
        } else {
          if(res.ResponseData == -1){
            this.toastr.error('Kindly Select A Different Date Range. Survey Already Exists Between The Provided Dates')
          }else{
            console.log('Some Error Occured');
          }
        }
      })
    } else {
      this.toastr.error('Kindly fill in all the required fields');
    }
  }
  closeModal(){
    this.modalService.dismissAll();
  }
 
}
