import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AprovalDebitNoteDashboardComponent } from '../aproval-debit-note-dashboard/aproval-debit-note-dashboard.component';
import { CreateDebitNoteComponent } from '../create-debit-note/create-debit-note.component';
import { DebitNoteDashboardComponent } from './debit-note-dashboard.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/material.module';
import { MatPaginatorModule, MatDialogModule, MatSortModule, MatFormFieldModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { NgxBarcodeModule } from 'ngx-barcode';
import { DebitNoteRoutingModule } from './debit-note-routing/debit-note-routing.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { UtilityComponentsModule } from '../utility-components/utility-components.module';
import { ConfirmationAlertComponent } from '../utility-components/confirmation-alert/confirmation-alert.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    
    DebitNoteDashboardComponent,
    CreateDebitNoteComponent,
    AprovalDebitNoteDashboardComponent,
    
  ],
  imports: [
    CommonModule,
    DebitNoteRoutingModule,
    NgxMatSelectSearchModule,
    UtilityComponentsModule,

    MaterialModule,
    FormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSortModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    NgxBarcodeModule
  ],
  providers:[
    NgbActiveModal
  ]
})
export class DebitNoteDashboardModule { }
