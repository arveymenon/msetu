import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DebitNoteDashboardComponent } from '../debit-note-dashboard.component';
import { AprovalDebitNoteDashboardComponent } from 'src/app/aproval-debit-note-dashboard/aproval-debit-note-dashboard.component';
import { CreateDebitNoteComponent } from 'src/app/create-debit-note/create-debit-note.component';
import { AuthGuard } from 'src/app/services/auth-guards/auth.guard';

const routes: Routes = [
  {
    path: "",
    component: DebitNoteDashboardComponent,
    canActivate: [AuthGuard],
    data: { roles: ['debitNoteDashboard'] }
  },
  {
    path: "aprovalDebitNoteDashboard",
    component: AprovalDebitNoteDashboardComponent,
    canActivate: [AuthGuard],
  data: { roles: ['aprovalDebitNoteDashboard'] }
  },
  {
    path: "createdebitNote",
    component: CreateDebitNoteComponent,
    canActivate: [AuthGuard],
    data: { roles: ['createdebitNote'] }
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DebitNoteRoutingModule { }
