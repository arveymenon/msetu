import { Component, OnInit } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { MatIconRegistry, MatDialog } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { NgbModal, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, NgModel, FormControl } from "@angular/forms";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";

import * as fs from "file-saver";
import * as CryptoJS from "crypto-js";
import * as moment from "moment";
import * as _ from "lodash";
import { JitService } from "../services/jit/jit.service";
import {
  NativeDateAdapter,
  DateAdapter,
  MAT_DATE_FORMATS,
} from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS } from "../cmsdashboard/date-format";
import { Router } from "@angular/router";
import { DailoughBoxComponent } from "../modal/dailough-box/dailough-box.component";
import { formArrayNameProvider } from "@angular/forms/src/directives/reactive_directives/form_group_name";
import { Workbook } from 'exceljs';

@Component({
  selector: "app-debit-note-dashboard",
  templateUrl: "./debit-note-dashboard.component.html",
  styleUrls: ["./debit-note-dashboard.component.scss"],
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: APP_DATE_FORMATS,
    },
  ],
})
export class DebitNoteDashboardComponent implements OnInit {
  debiteNoteDataSource = [];
  tracker: any;
  ApproverGroupDDL: NgModel;
  newApprover: NgModel;
  debiteNoteHistoryDataSource = [];
  debitNotedisplayedColumns = [
    "debitNoteNo",
    "supplierName",
    "Commodity",
    "createdOn",
    "createdBy",
    "Amount",
    "Attachment",
    "Comments",
    "Status",
    "action",
  ];
  debitNoteHistorydisplayedColumns = [
    "ID",
    "UpdatedBy",
    "UserName",
    "Amount",
    "createdOn",
    "Status",
    "Remarks",
  ];
  username = CryptoJS.AES.decrypt(
    localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),
    ""
  ).toString(CryptoJS.enc.Utf8);
  RoleID = CryptoJS.AES.decrypt(localStorage.getItem("WTIwNWMxcFZiR3M9"), "").toString(
    CryptoJS.enc.Utf8
  );
  masterOptions: any = {};
  debitNote: any = {
    debitNoteid: 0,
    debitNoteNo: 0,
  };

  openSupplierModal(content, debitNote) {
    console.log(debitNote);
    this.debitNote.debitNoteNo = debitNote.debitNoteNo;
    this.debitNote.debitNoteid = debitNote.DebitNoteID;
    this.getHistory();

    this.modalService.open(content, {
      size: "xl" as "lg",
      centered: true,
      windowClass: "formModal",
    });
  }

  getHistory(pageNo?: any) {
    this.debiteNoteHistoryDataSource = [];
    let body = {
      PageNo: pageNo + 1 || 1,
      debitNoteNumber: this.debitNote.debitNoteNo,
      debitNoteID: this.debitNote.debitNoteid,
      status: null,
    };
    this.jit.call("DebitNote/GetDebitNoteHistory", body).subscribe((res) => {
      console.log(res);
      if (res.Message == "Success") {
        let dataSource = [];
        this.pageSize2 = res.TotalCount ? res.TotalCount : 200 || 0;
        for (const data of res.ResponseData) {
          dataSource.push({
            ID: data.ID,
            UpdatedBy: data.UpdatedBy,
            Amount: data.Amount,
            createdOn: data.CreatedOn ? data.CreatedOn : "N/A",
            Status: data.status,
            Remarks: data.Remarks,
            UserName: data.UserName,
          });
        }
        this.debiteNoteHistoryDataSource = [...dataSource];
      }
    });
  }

  deleteDebitNote(debitNote): void {
    const dialogRef = this.dialog.open(DailoughBoxComponent, {
      width: "300px",
      height: "130px",
      data: {
        messege: "Are You Sure You Want To Delete This Record",
        element: debitNote,
      },
    });

    dialogRef.afterClosed().subscribe((data) => {
      console.log(data);
      if (data) {
        let body = {
          debitNoteID: debitNote.DebitNoteID,
          debitNoteNumber: debitNote.debitNoteNo,
          UserName: this.username,
          remarks: "",
        };
        this.jit.call("debitnote/DeleteDebitNote", body).subscribe((res) => {
          if (res) {
            this.cs.showSuccess("Debit Note Has been deleted")
            this.search();
          }
        });
      }
    });
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  filterForm: FormGroup;

  pageSize = 0;
  pageSize2 = 0;
  approverGroups = [
    { id: 1, name: "QA DH" },
    { id: 2, name: "CDMM GM" },
    { id: 3, name: "VPSCPC" },
    { id: 4, name: "VPQA" },
    { id: 5, name: "VPCDMM" },
    { id: 6, name: "Accounts" },
  ];
  AllApprovers: any;
  approvers = [];

  constructor(
    public commonService: CommonUtilityService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private modalService: NgbModal,
    public formBuilder: FormBuilder,
    public jit: JitService,
    public cs: MySuggestionService,
    public router: Router,
    public activeModal: NgbActiveModal,
    public dialog: MatDialog
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "viewIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/View_Icon_Red.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "trash",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/delete.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "edit",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Edit.svg"
      )
    );
    this.filterForm = this.formBuilder.group({
      debitNoteNumber: [],
      supplierCode: [],
      commodity: [],
      sapDebitNoteNo: [],
      fromDate: [],
      toDate: [],
      status: [],
    });

    this.jit
      .call("DebitNote/GetDebitNoteAdminMaster", null)
      .subscribe((res) => {
        console.log(res);
        this.masterOptions = res.ResponseData;
      });
    this.jit.call("debitNote/GetApproverGroupList", null).subscribe((res) => {
      console.log(res);
      this.approverGroups = res.ResponseData;
      // this.masterOptions = res.ResponseData;
    });

    this.search();
  }

  resetFilter(){
    this.filterForm.reset()
    this.search()
  }

  search(pageNo?: any) {
    console.log(this.filterForm);
    let body: any = this.filterForm.value;
    this.debiteNoteDataSource = [];

    body.fromDate = moment(this.filterForm.value.fromDate).add(1,'day')
    body.toDate = moment(this.filterForm.value.toDate).add(1,'day')
    body.pageNo = pageNo + 1 || 1;
    body.debitNoteID = this.filterForm.value.debitNoteNumber;
    // body.status = body.status ? body.status.toLowerCase() : null
    
    if(this.RoleID == '7'){
      var url = "DebitNote/GetDebitNoteAdmin"
      body.UserName = null;
    }else{
      var url = 'DebitNote/GetDebitNoteDashboard'
      body.UserName = this.username;
    }
    console.log(body);
    this.jit.call(url, body).subscribe((res) => {
      console.log(res);
      if (res.Message == "Success") {
        let dataSource = [];
        this.pageSize = res.TotalCount || 0;
        for (const data of res.ResponseData) {
          dataSource.push({
            debitNoteNo: data.DebitNoteID,
            DebitNoteID: data.DebitNoteID,
            supplierName: data.SupplierName,
            Commodity: data.Commodity,
            createdOn: data.CreatedOn ? data.CreatedOn : "N/A",
            createdBy: data.CreatedBy,
            Amount: data.Amount,
            Attachment: data.FileName,
            path: data.AttachmentPath, 
            Comments: data.Comments,
            Status: data.Status,
            action: "",
            isActive: data.isActive,
            WorkflowStatus: data.WorkflowStatus,
          });
        }
        this.debiteNoteDataSource = [...dataSource];
      }
    });
  }

  openFile(path){
        path ? window.open(path) : null
  }

  export(table: any, name: any) {
    this.getExportToExcelData().then((data: []) => {
      this.exportExcel(data)
     });
  }

  getExportToExcelData() {
    return new Promise((res, rej) => {
      let body: any = this.filterForm.value;
      body.pageNo = -1;
      // this.searchValue.value || this.statusControl.value ? false : (body = null);
      console.log(body);
      if(this.RoleID == '7'){
        var url = "DebitNote/GetDebitNoteAdmin"
        body.UserName = null;
      }else{
        var url = 'DebitNote/GetDebitNoteDashboard'
        body.UserName = this.username;
      }
      this.jit
        .call(url, body)
        .subscribe((response) => {
          console.log(response);
          this.pageSize = response.TotalCount;
          let dataSource = [];

          // for (const data of response.ResponseData) {
          //   dataSource.push({
          //     debitNoteNo: data.DebitNoteID,
          //     supplierName: data.SupplierName,
          //     Commodity: data.Commodity,
          //     createdOn: data.CreatedOn ? data.CreatedOn : '',
          //     Author: data.CreatedBy,
          //     Amount: data.Amount,
          //     SAPDebitNoteNumber: data.SAPDebitNoteNumber,
          //     Comments: data.Comments,
          //     DH_x002d_QAApprovalStatus: data.DHQAApprovalStatus,
          //     VP_x002d_QAApprovalStatus: data.VP_QAApprovalStatus,
          //     GM_x002d_CDMMApprovalStatus: data.GM_CDMMApprovalStatus,
          //     WorkflowStatus: data.WorkflowStatus,
          //   });
          // }
          res(response.ResponseData);
        });
    });
  }

  
  exportExcel(json){
    let dataNew = json;
    console.log(dataNew);
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("ReportData");
    let columns = Object.keys(json[0]);
    let headerRow = worksheet.addRow(columns);
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "00FF0000" },
        bgColor: { argb: "00FF0000" },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
    });
    dataNew.forEach((d) => {
      // var obj = JSON.parse(d);
      var values = Object.keys(d).map(function (key) {
        return d[key];
      });
      let row = worksheet.addRow(values);
    });
    workbook.xlsx.writeBuffer().then((dataNew) => {
      let blob = new Blob([dataNew], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(
        blob,
        "DashboardDetails.xlsx"
      );
    });
  }

  editDebitNote(element) {
    console.log(element);
    this.router.navigateByUrl(
      "debitNoteDashboard/createdebitNote?did=" + element.debitNoteNo
    );
  }

  trackDebitNote(content, element) {
    console.log(element);
    this.tracker = null;
    this.cs
      .postMySuggestionData("DebitNote/GetDebitNoteByID", {
        DebitNote_ID: element.debitNoteNo,
      })
      .subscribe((res) => {
        console.log(res);
        this.tracker = res.ResponseData;
      });

    this.modalService.open(content, {
      size: "xl" as "lg",
      centered: true,
      windowClass: "formModal",
    });
  }

  ngOnInit() {
    this.search();
    this.cs
      .postMySuggestionData("debitnote/GetAllApproverDDL", "")
      .subscribe((res: any) => {
        this.AllApprovers = res.ResponseData;
      });
  }

  manageApproverGroup(content) {
    this.modalService.open(content, {
      size: "xl" as "lg",
      centered: true,
      windowClass: "formModal",
    });
  }

  selectApproverGroup() {
    console.log(this.ApproverGroupDDL);
    // get approvers of the ApproverGroup

    this.approvers = [];
    this.jit
      .call("debitNote/CRUDGroupApprover", {
        actionType: "GETBYGROUP",
        GroupID: this.ApproverGroupDDL,
      })
      .subscribe((res) => {
        console.log(res);
        res.ResponseData.forEach((element) => {
          if (element.ApproverUserName) {
            this.approvers.push({
              id: element.GroupMemberID,
              UserName: element.ApproverUserName,
              Name: element.ApproverName,
              cname: new FormControl(element.ApproverUserName),
            });
          }
        });
        // Empty push for the input
        this.approvers.push({
          UserName: "",
          Name: "",
          cname: new FormControl(""),
        });
      });
  }

  addApprover(approver) {
    console.log("New Approver", approver);
    console.log(this.AllApprovers.listInternalUser[3].UserName);
    if (approver.cname.value) {
      let array = this.AllApprovers.listInternalUser;
      let newApprover = array.find((o) => o.UserName == approver.cname.value);

      this.approvers[this.approvers.length - 1].UserName = newApprover.UserName;
      this.approvers[this.approvers.length - 1].Name = newApprover.Name;

      this.jit
        .call("debitNote/CRUDGroupApprover", {
          actionType: "INSERT",
          approverName: newApprover.Name,
          approverUserName: newApprover.UserName,
          groupID: this.ApproverGroupDDL,
          createdModifiedBy: this.username,
        })
        .subscribe((res) => {
          if (res) {
            if (res.ID == 1 && res.Message != "Already available in group") {
              this.cs.showSuccess("Approver Added Successfully");
              this.selectApproverGroup()
            } else {
              this.cs.showError(res.Message);
            }
          }
        });
    } else {
      this.cs.showError("Kindly Select an approver");
    }
  }

  updateApprover(approver) {
    console.log("Update Approver", approver);
    if (approver.UserName != approver.cname.value) {
      this.cs.showSuccess("Approver Updated");
    }
  }

  deleteApprover(approver, itt) {
    console.log(approver,itt)
    this.jit
      .call("debitNote/CRUDGroupApprover", {
        actionType: "DELETE",
        groupMemberID: approver.id,
        createdModifiedBy: this.username,
      })
      .subscribe((res) => {
        if(res.ID == 1){
          this.cs.showSuccess('Approver Deleted')
          this.selectApproverGroup()
        }else{
          this.cs.showError(res.Message)
        }
      });
  }
}
