import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SRMMAssessmentComponent } from './srmmassessment.component';

describe('SRMMAssessmentComponent', () => {
  let component: SRMMAssessmentComponent;
  let fixture: ComponentFixture<SRMMAssessmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SRMMAssessmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SRMMAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
