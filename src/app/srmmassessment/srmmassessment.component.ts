import { Component, OnInit } from "@angular/core";
import { MatIconRegistry } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormControl, FormBuilder, FormGroup } from "@angular/forms";
import { MyHelpDeskService } from "../services/myHelpDesk/my-help-desk.service";
import * as CryptoJS from "crypto-js";
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { NavigationExtras, Router } from '@angular/router';
import { Workbook } from "exceljs";
import * as fs from "file-saver";

@Component({
  selector: "app-srmmassessment",
  templateUrl: "./srmmassessment.component.html",
  styleUrls: ["./srmmassessment.component.scss"],
})
export class SRMMAssessmentComponent implements OnInit {
  FY = [];
  selectedYear: any;
  FinancialYear = new FormControl();
  DataSource = [];
  excelData = [];

  // Form
  assessmentType = new FormControl()
  status = new FormControl()
  mode = new FormControl()
  commodity = new FormControl()
  searchBy = new FormControl()
  searchByValue = new FormControl()

  // for (let i = 0; i < this.printPartDatasource.length; i++) {
  //   this.quantity = new FormControl();
  // }
  assessmentTypes = [
    {id: 1, name: "New Assessment"},
    {id: 2, name: "Re-Assessment"},
    {id: 3, name: "Self Assessment"}
  ]
  
  statuses = [
    {id: 1, name: "Completed"},
    {id: 2, name: "Pending with Assessor"},
    {id: 3, name: "Pending with Supplier"},
    {id: 4, name: "Not Completed"}
  ]
  
  modes = [
    {id: 1, name: "Draft"},
    {id: 2, name: "Submit"},
    {id: 3, name: "Not Started"}
  ]

  commodities = []

  displayedColumns: string[] = [
    "supplierCode",
    "supplierName",
    "Email",
    "Rating",
    "assessmentOn",
    "Assessor",
    "AuditSubmitted",
    "AuditCompleted",
    "Commodity",
    "AssessmentType",
    "Status",
    "Mode",
    "ViewResponse",
    "Action",
  ];
  pageSize = 0;
  filter: FormGroup

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public commonService: CommonUtilityService,
    private modalService: NgbModal,
    public http: MyHelpDeskService,
    public formBuilder: FormBuilder,
    public toast: ToastrService,
    public router: Router
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "viewIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/View_Icon_Red.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "edit",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Edit.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "trash",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/delete.svg"
      )
    );

    this.filter = this.formBuilder.group({

    })
    
    this.http.call('SRMMAssesment/GetAllYears').subscribe(res=>{
      console.log(res)
      this.FY = res
    })

    this.http.call('SRMMAssesment/GetCommodity').subscribe(res=>{
      console.log(res)
      this.commodities = res
    })
    
    this.FinancialYear.valueChanges.subscribe((year) => {
      console.log(year);
      if (year) {
        this.selectedYear = year
        this.search()
      }
    });


  }
RoleID:any;
  ngOnInit() {
    this.RoleID = localStorage.getItem("WTIwNWMxcFZiR3M9");
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(CryptoJS.enc.Utf8);
   
    
  }
c:any;
  search(pageNo?: any){
    let value1='';
    let value2='';
    let assessmentType;
    let status,mode,cmd;
    if(this.searchBy.value == 'Supplier Name')
      {
        value1= this.searchByValue.value;
        value2 ='';
      }
      else if(this.searchBy.value =='Supplier Code'){
        value1='';
        value2 = this.searchByValue.value;
      }
if(this.assessmentType.value ==null)
  assessmentType ="";
  else
  assessmentType = this.assessmentType.value;
  if(this.status.value ==null)
  status ="";
  else
  status = this.status.value;
  if(this.mode.value ==null)
  mode ="";
  else
  mode = this.mode.value;
  if(this.commodity.value ==null)
  cmd ="";
  else 
  cmd = this.commodity.value;
  let body;
      if(this.downloadFlag == true){
         body = {
          Year: this.FinancialYear.value,
          AssesmentType: assessmentType,
          Status: status,
          Mode: mode,
          Commodity: cmd,
          SupplierCode:value2,
          SupplierName:value1,
          // SearchBy: this.searchBy.value,
          // SearchByValue: this.searchByValue.value,
          PageNo: 0
        }
      }
     else{
         body = {
          Year: this.FinancialYear.value,
          AssesmentType: assessmentType,
          Status: status,
          Mode: mode,
          Commodity: cmd,
          SupplierCode:value2,
          SupplierName:value1,
          // SearchBy: this.searchBy.value,
          // SearchByValue: this.searchByValue.value,
          PageNo: pageNo || pageNo == 0 ? 0 : 1
        }
      }
 
 
    
    
    // {"Year":"FY 2019","AssessmentType":null,"Status":null,"Mode":null,"Commodity":null,"SupplierCode":"","SupplierName":"","PageNo":1}
// {"Year":"FY 2018","AssesmentType":"","Commodity":"","Status":"","Mode":"","SupplierCode":"","SupplierName":""}


    console.log(body)
    this.http.call("SRMMAssesment/GetReportSRMM", body).subscribe(res=>{
      console.log(res)
if(this.downloadFlag == false){
      if(res.ResponseData.length >0){
        this.pageSize = res.ResponseData[0].TotalCount
        let dataSource = [];
            // this.pageSize = res.DataCount ? res.DataCount : 200 || 0;
            for (const data of res.ResponseData) {
              dataSource.push({
                supplierCode: data.SupplierCode,
                supplierName: data.SupplierName,
                Email: data.Email,
                Rating: data.Rating,
                assessmentOn: moment(data.CompletionDate).format('DD-MM-YYYY'),
                Assessor: data.Assessor,
                AuditSubmitted: data.AuditSubmittedSupplier,
                AuditCompleted: data.AuditSubmittedAssessor,
                Commodity: data.Commodity,
                AssessmentType: data.AssessmentType,
                Status: data.Status,
                Mode: data.Mode,
                ViewResponse: data.ID,
                Action: data.ID,
              });
             
            }
            this.DataSource = [...dataSource];
            
  for (let i = 0; i < this.DataSource.length; i++) {
    this.c = new FormControl(this.DataSource[i].commodity);
  }
  
      } else {
        this.DataSource = [];
        this.pageSize = 0;
        this.toast.error('No data Available');
      }
    }
    else{
      this.excelData = res.ResponseData;
      let dataSourceTemp=[];
       // tanvi -new expoer to excel
     // let dataNew = ['Supplier Code',	'Supplier Name',	'Email',	'Rating',	'Assessment On',	'Assessor',	'Audit Submitted by Supplier', 	'Audit completed by Assessor',	'Commodity',	'Assessment Type',	'Status',	'Mode']
     let dataNew = this.excelData;
      for (const data of dataNew) {
              dataSourceTemp.push({
                supplierCode: data.SupplierCode,
                supplierName: data.SupplierName,
                Email: data.Email,
                Rating: data.Rating,
                assessmentOn: moment(data.CompletionDate).format('DD-MM-YYYY'),
                Assessor: data.Assessor,
                AuditSubmitted: data.AuditSubmittedSupplier,
                AuditCompleted: data.AuditSubmittedAssessor,
                Commodity: data.Commodity,
                AssessmentType: data.AssessmentType,
                Status: data.Status,
                Mode: data.Mode,
               
              });
            }
     //Create workbook and worksheet
     let workbook = new Workbook();
     let worksheet = workbook.addWorksheet('SRMMAssessmentReport');
      //Add Header Row
      let header: any 
     //  if(this.CategoryName == 'Retro Debit Note' || this.CategoryName == 'Retro Credit Note'){
     //    header = Object.keys(dataNew[0])
     //  } else {
     //    header = this.displayedColumns
     //  }
     header=['Supplier Code',	'Supplier Name',	'Email',	'Rating',	'Assessment On',	'Assessor',	'Audit Submitted by Supplier', 	'Audit completed by Assessor',	'Commodity',	'Assessment Type',	'Status',	'Mode']
    let headerRow = worksheet.addRow(header);
       // Cell Style : Fill and Border
  headerRow.eachCell((cell, number) => {
    cell.fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: '00FF0000' },
      bgColor: { argb: '00FF0000' }
    }
    cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
  })
  dataSourceTemp.forEach(d => {
    var values = Object.keys(d).map(function (key) { return d[key]; });
    console.log(values);
   //  if(this.CategoryName == 'LR & RA')
    // values.splice(12,1);
    // values.splice(13,1);
   //  if(this.CategoryName == 'VQI')
   //  [values[0], values[1]] = [values[1], values[0]];
     let row = worksheet.addRow(values);
 });
    workbook.xlsx.writeBuffer().then((dataSourceTemp) => {
      let blob = new Blob([dataSourceTemp], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob,'reportData.xlsx');
      this.downloadFlag = false;
});
    }
    });


  }

  updateCommidity1()
  {
    let req=
    {
      "VendorCode":this.SupplierC,
    "Commidity":this.newCom,
    "Year":this.selectedYear,
    "Assessor":this.assessorC}
    this.http.call('SRMMAssesment/UpdateCommidity',req).subscribe(res=>{
      console.log(res)
      this.toast.success("Commodity updated");
      this.closeModal(true);
    })
  }
  closeModal(flag) {
    this.modalService.dismissAll();
    if(flag== true){
      this.search();
    }
  }

  navigate(data){
    let vcode = btoa(data.supplierCode);
    let navigationExtras: NavigationExtras = {
      queryParams: {
          "VC": vcode,
          "FY": this.selectedYear
      }
  };
    this.router.navigate(["riskDashboard"], navigationExtras);
  }
currentCom:any;
SupplierC:any;
newCom:any ='';
assessorC:any
  openModal(content,data) {
    this.supplierCode1='';
    this.currentAssessor='';
    this.newAssessor ='';
    if(data != ''){
      this.currentCom = data.Commodity;
      this.SupplierC  = data.supplierCode; 
      this.assessorC = data.Assessor;
    }
    this.modalService.open(content, {
      centered: true,
      windowClass: "formModal",
    });
  }

  supplierCode1:any;
  currentAssessor:any;
  newAssessorList:any;
  newAssessor:any;

  getassessorDetails(code){
    this.supplierCode1 =code;
    
   
    let req={"VendorCode":this.supplierCode1}
    this.http.call('SRMMAssesment/GetAssessor',req).subscribe(res=>{
      console.log(res)
      this.currentAssessor = res.ResponseData[0].Assessor;
    })
    this.getList();
  }
  getList(){
    let req1={
      "RoleId":this.RoleID
    }
    this.http.call('SRMMAssesment/GetAssessorList',req1).subscribe(res=>{
      console.log(res)
      this.newAssessorList = res.ResponseData;
    })
  }
  updateAssessorCall(){

if(this.supplierCode1 !='' && this.currentAssessor !='' && this.newAssessor !='')
{
  let req=
  {"VendorCode": this.supplierCode1,
  "Assessor":this.newAssessor
}
  this.http.call('SRMMAssesment/UpdateAssessor',req).subscribe(res=>{
    console.log(res)
   this.toast.success("Assessor updated successfully !");
   this.closeModal(true);
  })
}
else{
  if(this.supplierCode1 =='' || this.supplierCode1 ==null)
  this.toast.error("please enter Supplier Code");
  if(this.currentAssessor =='' || this.currentAssessor ==null)
  this.toast.error("please enter current Assessor");
  if(this.newAssessor =='' || this.newAssessor ==null)
  this.toast.error("please enter new Assessor");
}
 
  }
  downloadFlag:boolean=false;
  exportToExcel(){
    this.downloadFlag = true;
    this.search();
     
  }
}

