import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JITPrintHistoryComponent } from './jitprint-history.component';

describe('JITPrintHistoryComponent', () => {
  let component: JITPrintHistoryComponent;
  let fixture: ComponentFixture<JITPrintHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JITPrintHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JITPrintHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
