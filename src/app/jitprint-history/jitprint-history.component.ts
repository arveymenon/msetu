import { Component, OnInit, ViewChild } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { JitService } from "../services/jit/jit.service";
import * as CryptoJS from 'crypto-js';
import * as moment from "moment";
import { MatPaginator } from '@angular/material';
import { FormControl } from '@angular/forms';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { Workbook } from 'exceljs';
import * as fs from "file-saver";
import { MyHelpDeskService } from "../services/myHelpDesk/my-help-desk.service";


@Component({
  selector: "app-jitprint-history",
  templateUrl: "./jitprint-history.component.html",
  styleUrls: ["./jitprint-history.component.scss"]
})
export class JITPrintHistoryComponent implements OnInit {
  userid = CryptoJS.AES.decrypt(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ=='),"").toString(CryptoJS.enc.Utf8);
  @ViewChild(MatPaginator) paginator: MatPaginator
  roleId = localStorage.getItem('WTIwNWMxcFZiR3M9');
  DataSource = [];
  isDisabled: boolean = true
  displayedColumns: string[] = [
    "shopName",
    "DSN",
    "SPN",
    "BIN",
    "modelCode",
    "dateTime",
    "colour",
    "remarks",
    "country",
    "paintOutScan",
    "majorVariantt",
    "VIN",
    "partNo",
    "partDescription",
    "dataFrom"
  ];

  pageSize: any;
  pageNo = 0;

  supplierCode = "DI061X";
  constructor(
    public commonService: CommonUtilityService,
    public jitService: JitService,
    public cs: MySuggestionService,
    public http: MyHelpDeskService,
  ) {
    this.commonService.changeIsAuthenticate(true);
    this.getTableValues(0)
  }
  
  getTableValues(pageNo? : any) {
    const tempBody = { PageStart: pageNo || 1 };
    this.commonService.changeIsAuthenticate(true);
    this.jitService
      .call("PTLiveData/GetPTTransactionHistory", tempBody
      )
      .subscribe(res => {
        console.log(res);
        if (res.Message == "Success") {
         // let dataSource = [];
         this.DataSource = res.ResponseData;
          // for (const data of res.ResponseData) {
          //   this.DataSource.push({
          //     selectCheck: false,
          //     Barcode: data.BarcodeData,
          //     productionLine: data.ProductionLine,
          //     shopName: data.ShopName,
          //     DSN: data.DSN,
          //     SPN: data.SPN,
          //     BIN: data.BIN,
          //     modelCode: data.ModelCode,
          //     dateTime: data.Dateandtime ? moment(data.Dateandtime).format("DD-MM-YYYY hh:mm:s") : 'N/A',
          //     colour: data.Colour,
          //     remarks: data.Remarks,
          //     country: data.Country,
          //     paintOutScan: data.PaintoutScan,
          //     majorVariant: data.MajorVariant,
          //     VIN: data.VIN,
          //     partNo: data.PartNumber,
          //     partDescription: data.PartDescription,
          //     dataFrom: data.DataFrom
          //   });
          //   console.log(this.DataSource);
            
          // }
          // this.DataSource = [...dataSource];
          if (this.DataSource.length > 0)
            this.isDisabled = false;
          
          this.pageSize = res.DataCount;
        }
      });

  }

  
  goToChange(value? : any) {
    this.paginator.pageIndex = this.goTo - 1;
    console.log(value)
    console.log(this.paginator.pageIndex)
    this.goToControl.setValue(this.paginator.pageIndex)
    this.getTableValues(this.paginator.pageIndex)
  }

  goToControl: FormControl
goTo: number;
  pageNumbers = [];

  updateGoto() {
    this.goTo = 1;
    this.pageNumbers = [];
    var totalLength= this.pageSize / 10;
    var ceiledLength =Math.ceil(totalLength);
    console.log(ceiledLength);
    for (let i = 1; i <= ceiledLength; i++) {
      this.pageNumbers.push(i);
    }
  }

  getPageChangeData($event) {
    console.log($event);
    this.getTableValues($event.pageIndex);
  }

  ngOnInit() {
    this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8);
    let analysticsRequest={
      "userClicked":this.userid,//  localStorage.getItem('userToken'),
      "device": "windows",
      "browser": localStorage.getItem('browser'),
      "moduleType": "Document",
      "module": "MyLibary"
    }
   this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})
  }

  exportExcel(JSON) { 
    let body = {
      PageStart: 0
    }
    this.http.call("PTLiveData/GetPTTransactionHistory", body).subscribe(async (res) => {
      if (res.Message == "Success") {
        let dataSource = []
        res.ResponseData.forEach((data, index) => {
          dataSource.push({
            selectCheck: false,
              Barcode: data.BarcodeData,
              productionLine: data.ProductionLine,
              shopName: data.ShopName,
              DSN: data.DSN,
              SPN: data.SPN,
              BIN: data.BIN,
              modelCode: data.ModelCode,
              dateTime: data.Dateandtime ? moment(data.Dateandtime).format("DD-MM-YYYY hh:mm:s") : 'N/A',
              colour: data.Colour,
              remarks: data.Remarks,
              country: data.Country,
              paintOutScan: data.PaintoutScan,
              majorVariant: data.MajorVariant,
              VIN: data.VIN,
              partNo: data.PartNumber,
              partDescription: data.PartDescription,
              dataFrom: data.DataFrom
          });
          })

        let dataNew = dataSource
        // this.DataSource;
        await console.log(dataNew);
        //Create workbook and worksheet
        let workbook = new Workbook();
        let worksheet = workbook.addWorksheet("ReportData");
        let columns = Object.keys(this.DataSource[0]);
        let headerRow = worksheet.addRow(columns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: "00FF0000" },
            bgColor: { argb: "00FF0000" },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
        });
        dataNew.forEach((d) => {
          // var obj = JSON.parse(d);
          var values = Object.keys(d).map(function (key) {
            return d[key];
          });
          let row = worksheet.addRow(values);
        });
        workbook.xlsx.writeBuffer().then((dataNew) => {
          let blob = new Blob([dataNew], {
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          fs.saveAs(
            blob,
            "PT Live Data Print History.xlsx"
          );
        });
      }
    })
  }
  
}
