import { Component, OnInit, ViewChild } from "@angular/core";
import { MatIconRegistry, MatPaginator } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  FormControl,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { JitService } from "../services/jit/jit.service";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import { ToastrService } from "ngx-toastr";
import * as CryptoJS from 'crypto-js';
import { Workbook } from 'exceljs';
import * as fs from "file-saver";
import { MyHelpDeskService } from "../services/myHelpDesk/my-help-desk.service";
@Component({
  selector: "app-jitsupplier-details",
  templateUrl: "./jitsupplier-details.component.html",
  styleUrls: ["./jitsupplier-details.component.scss"]
})
export class JITSupplierDetailsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator 
  roleId = localStorage.getItem('WTIwNWMxcFZiR3M9');
  plantCode = new FormControl();
  supplierCode = new FormControl();
  // DataSource = [];
  pageSize = 0;
  pageNo = 0;
  supplierId = 0;

  supplierDetailsForm: FormGroup;

  DataSource = [];
  displayedColumns: string[] = [
    "plantcode",
    "supplierCode",
    "supplierEmail",
    "recipient",
    "isActive",
    "action"
  ];

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public commonService: CommonUtilityService,
    private modalService: NgbModal,
    public jitService: JitService,
    public cs: MySuggestionService,
    public formBuilder: FormBuilder,
    public toastr: ToastrService,
    public http: MyHelpDeskService,
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "addIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/plus.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "trash",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/delete.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "edit",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Edit.svg"
      )
    );

    this.getTableValues(0);

    this.supplierDetailsForm = this.formBuilder.group({
      PlantCode: ["", Validators.required],
      SupplierCode: ["", Validators.required],
      SupplierName: [{ value: "", disabled: true }],
      SupplierEmail: ["", [Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      Recipient: ["", Validators.required],
      CreatedBy: []
    });

    this.supplierDetailsForm
      .get("SupplierCode")
      .valueChanges.subscribe(supplierCode => {
        console.log(supplierCode);
        if (supplierCode && supplierCode.length > 3) {
          this.jitService
            .call("PTLiveData/GetSupplierDetails", {
              PageStart: 1,
              RowPerPage: 100,
              SupplierCode: supplierCode
            })
            .subscribe(response => {
              console.log(response);
              if (response.ResponseData[0]) {
                for(let supplier of response.ResponseData){
                  if(supplier.SupplierEmail){
                    this.supplierDetailsForm
                      .get("SupplierEmail")
                      .patchValue(supplier.SupplierEmail);
                  }
                }
              }
            });
        }
      });
  }

  getTableValues(pageNo?: any) {
    console.log(this.plantCode.value);
    console.log(this.supplierCode.value);
    console.log(this.pageNo);
    this.pageNo = pageNo + 1 || 1
    const body = {
      PageStart: this.pageNo,
      RowPerPage: 10,
      PlantCode: this.plantCode.value,
      SupplierCode: this.supplierCode.value
    };
    this.jitService
      .call("PTLiveData/GetSupplierDetails", body)
      .subscribe(response => {
        console.log(response);
        this.pageSize = response.DataCount || 200
        let dataSource = [];
        for (const model of response.ResponseData) {
          dataSource.push({
            supplierId: model.SupplierDetailsId,
            plantcode: model.PlantCode,
            supplierCode: model.SupplierCode,
            supplierEmail: model.SupplierEmail,
            recipient: model.Recipient,
            isActive: model.IsActive,
            action: ""
          });
        }
        this.DataSource = [...dataSource];
        console.log(this.DataSource);
        this.updateGoto()
      });
  }

  reset() {
    this.plantCode.reset();
    this.supplierCode.reset();
    this.getTableValues(0);
  }

  submitForm() {
    const body = this.supplierDetailsForm.value;
    this.supplierId > 0 ? (body.SupplierDetailsId = this.supplierId) : false;

    const url =
      this.supplierId > 0
        ? "PTLiveData/UpdateSupplierDetails"
        : "PTLiveData/InsertSupplierDetails";
    console.log(body);
    if (this.supplierDetailsForm.valid) {
      this.jitService.call(url, body).subscribe(response => {
        console.log(response);
        this.supplierId > 0 ? 
        this.toastr.success("Supplier Successfully Updated") :
        this.toastr.success("Supplier Successfully Added");
        this.closeModal();
        this.resetForm();
        this.getTableValues()
      });
    } else {
      this.toastr.error("Kindly Enter Valid Inputs");
    }
  }
  

  goToControl= new FormControl(1)
  goTo: number;
  pageNumbers = [];
  

  goToChange(value? : any) {
    this.paginator.pageIndex = this.goToControl.value;
    console.log(value)
    console.log(this.paginator.pageIndex)
    // this.goTo = this.paginator.pageIndex;
    this.getTableValues(this.paginator.pageIndex)
    this.goToControl.setValue(this.paginator.pageIndex);
  }

  updateGoto() {
    this.goTo = 1;
    this.pageNumbers = [];
    var totalLength= this.pageSize / 10;
    var ceiledLength =Math.ceil(totalLength);
    console.log(ceiledLength);
    for (let i = 1; i <= ceiledLength; i++) {
      this.pageNumbers.push(i);
    }
  }

  resetForm() {
    this.supplierDetailsForm.reset();
  }

  // exportAsExcelFile(table: any, name: any) {
  //   this.cs.exportAsExcelFile(table, name);
  // }

  getPageChangeData($event) {
    console.log($event);
    this.getTableValues($event.pageIndex);
  }

  ngOnInit() {
    this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8);
  }
  closeModal() {
    this.supplierDetailsForm.reset();
    this.supplierId = 0;
    this.supplierDetailsForm.get("PlantCode").enable({onlySelf: true});
    this.supplierDetailsForm.get("SupplierCode").enable({onlySelf: true});
    this.modalService.dismissAll();
  }

  open(content, supplier?: any) {
    console.log(supplier);

    this.modalService.open(content, {
      size: "xl" as "lg",
      centered: true,
      backdrop: "static",
      windowClass: "formModal"
    });
    if (supplier) {
      this.edit(supplier);
    }
  }

  edit(supplier) {
    console.log(supplier);
    this.supplierId = supplier.supplierId;
    this.supplierDetailsForm.get("PlantCode").patchValue(supplier.plantcode);
    this.supplierDetailsForm.get("PlantCode").disable({onlySelf: true});
    this.supplierDetailsForm
    .get("SupplierCode")
    .patchValue(supplier.supplierCode);
    this.supplierDetailsForm.get("SupplierCode").disable({onlySelf: true});
    
    this.supplierDetailsForm
      .get("SupplierName")
      .patchValue(supplier.SupplierName);
    this.supplierDetailsForm
      .get("SupplierEmail")
      .patchValue(supplier.supplierEmail);
    this.supplierDetailsForm
      .get("Recipient")
      .patchValue(supplier.recipient);
    this.supplierDetailsForm.get("CreatedBy").patchValue(supplier.CreatedBy);
  }

  delete(supplier) {
    console.log(supplier);
    console.log(this.DataSource);
    this.jitService
      .call("PTLiveData/DeleteSupplierDetails", {
        SupplierDetailsId: supplier.supplierId
      })
      .subscribe(response => {
        console.log(response);
        this.getTableValues();

        this.toastr.show("Part Supplier Removed Successfully");
      });
  }
  exportExcel(JSON) {
    let body = {
      PageStart: 0,
      RowPerPage: 10,
      PlantCode: this.plantCode.value,
      SupplierCode: this.supplierCode.value
    }
    this.http.call("PTLiveData/GetSupplierDetails", body).subscribe(async (res) => {
      if (res.Message == "Success") {
        let dataSource = []
        res.ResponseData.forEach((data, index) => {
          dataSource.push({
            supplierId: data.SupplierDetailsId,
            plantcode: data.PlantCode,
            supplierCode: data.SupplierCode,
            supplierEmail: data.SupplierEmail,
            recipient: data.Recipient,
            isActive: data.IsActive,
          });
          })

        let dataNew = dataSource
        // this.DataSource;
        await console.log(dataNew);
        //Create workbook and worksheet
        let workbook = new Workbook();
        let worksheet = workbook.addWorksheet("ReportData");
        let columns = Object.keys(this.DataSource[0]);
        let headerRow = worksheet.addRow(columns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: "00FF0000" },
            bgColor: { argb: "00FF0000" },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
        });
        dataNew.forEach((d) => {
          // var obj = JSON.parse(d);
          var values = Object.keys(d).map(function (key) {
            return d[key];
          });
          let row = worksheet.addRow(values);
        });
        workbook.xlsx.writeBuffer().then((dataNew) => {
          let blob = new Blob([dataNew], {
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          fs.saveAs(
            blob,
            "Supplier Details.xlsx"
          );
        });
      }
    })
  }
}
