import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JITSupplierDetailsComponent } from './jitsupplier-details.component';

describe('JITSupplierDetailsComponent', () => {
  let component: JITSupplierDetailsComponent;
  let fixture: ComponentFixture<JITSupplierDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JITSupplierDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JITSupplierDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
