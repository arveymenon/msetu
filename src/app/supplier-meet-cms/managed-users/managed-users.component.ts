import { Component, OnInit } from "@angular/core";
import { MyHelpDeskService } from "src/app/services/myHelpDesk/my-help-desk.service";
import { ToastrService } from "ngx-toastr";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import * as XLSX from "xlsx";
import { MySuggestionService } from "src/app/services/MySuggestion/my-suggestion.service";

@Component({
  selector: "app-managed-users",
  templateUrl: "./managed-users.component.html",
  styleUrls: ["./managed-users.component.sass"],
})
export class ManagedUsersComponent implements OnInit {
  pageNo = 1;
  pageSize = 0;
  manageUserForm: FormGroup;

  dataSource = [
    // {
    //   ManageUserID: 1,
    //   VendorCode: "Dl016",
    //   Vendor: "Test",
    //   Salutation: "Mr.",
    //   ParticipantName: "SABLE",
    //   EmailID: "ab@gmai.com",
    //   PhoneNo: "231231231",
    //   Hotel: "Taj",
    // },
  ];
  sending_array = [];
  displayedColumns = [
    "ManageUserID",
    "VendorCode",
    "Vendor",
    "Salutation",
    "ParticipantName",
    "EmailID",
    "PhoneNo",
    "Hotel",
    "action",
  ];
  excel_headers = [
    "vendorCode",
    "vendor",
    "salutation",
    "participantName",
    "emailID",
    "phoneNo",
    "hotel"
  ];
  constructor(
    public http: MyHelpDeskService,
    public toast: ToastrService,
    public modalService: NgbModal,
    public fb: FormBuilder,
    public toastr: ToastrService,
    public cs: MySuggestionService
  ) {
    this.manageUserForm = this.fb.group({
      ManageUserID: [0],
      VendorCode: ["", Validators.required],
      Vendor: ["", Validators.required],
      Salutation: [],
      Name: ["", Validators.required],
      Email: ["", Validators.required],
      PhoneNumber: ["", Validators.required],
      Hotel: ["", Validators.required],
    });
    this.getData(1);
  }

  getData(pageNo?: any) {
    this.dataSource = [];
    this.http
      .call("SMRegistration/ManageUser", {
        pageNo: pageNo + 1 || 1,
        actionType: "GETALL",
      })
      .subscribe((response) => {
        console.log(response);
        let dataSource = [];
        if (response.Message == "success") {
          // for (let data of response.ResponseData) {
          //   dataSource.push({
          //     ManageUserID: data.ManageUserID,
          //     VendorCode: data.VendorCode,
          //     Vendor: "Test",
          //     Salutation: "Mr.",
          //     ParticipantName: "SABLE",
          //     EmailID: "ab@gmai.com",
          //     PhoneNo: "231231231",
          //     Hotel: "Taj",
          //   });
          // }
          this.dataSource = [...response.ResponseData];
        } else {
          this.toastr.error("No Managed Users");
        }
      });
  }

  submit() {
    console.log(this.manageUserForm.value);
    let body: any = {
      ManageUserID: this.manageUserForm.value.ManageUserID,
      vendorCode: this.manageUserForm.value.VendorCode,
      vendor: this.manageUserForm.value.Vendor,
      salutation: this.manageUserForm.value.Salutation,
      participantName: this.manageUserForm.value.Name,
      emailID: this.manageUserForm.value.Email,
      phoneNo: this.manageUserForm.value.PhoneNumber,
      hotel: this.manageUserForm.value.Hotel,
      actionType:
        this.manageUserForm.value.ManageUserID > 0 ? "UPDATE" : "INSERT",
    };
    if (this.manageUserForm.value.ManageUserID > 0) {
      body.ModifiedBy = "admin";
      body.IsActive = true;
    } else {
      body.createdBy = "admin";
    }
    this.http.call("SMRegistration/ManageUser", body).subscribe((response) => {
      console.log(response);
      this.getData(1);
      this.closeModal();
      if (this.manageUserForm.value.ManageUserID > 0) {
        this.toast.success("Data Updated Successfully");
      } else {
        this.toast.success("Data Added Successfully");
      }
    });
  }

  delete(id) {
    this.http
      .call("SMRegistration/ManageUser", {
        ManageUserID: id,
        actionType: "DELETE",
      })
      .subscribe((response) => {
        console.log(response);
        this.getData(1);
        this.toast.success("Data Deleted Successfully");
      });
  }

  manageUser(content, element) {
    console.log(element);
    if (element) {
      this.manageUserForm.controls["ManageUserID"].setValue(
        element.ManageUserID
      );
      this.manageUserForm.controls["VendorCode"].setValue(element.VendorCode);
      this.manageUserForm.controls["Vendor"].setValue(element.Vendor);
      this.manageUserForm.controls["Salutation"].setValue(element.Salutation);
      this.manageUserForm.controls["Name"].setValue(element.ParticipantName);
      this.manageUserForm.controls["Email"].setValue(element.EmailID);
      this.manageUserForm.controls["PhoneNumber"].setValue(element.PhoneNo);
      this.manageUserForm.controls["Hotel"].setValue(element.Hotel);
    } else {
      this.manageUserForm.reset();
    }
    this.modalService.open(content, {
      size: "lg",
      centered: true,
      windowClass: "formModal",
    });
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  ngOnInit() {}

  uploadSendingExcel(ev) {
    var headers = [];
    for (let file of ev.target.files) {
      let ext = file.name.split(".").pop().toLowerCase();
      let reader = new FileReader();
      reader.onload = (e: any) => {
        if (ext != "xlsx") {
          alert("Kindly select excel file");
          return false;
        }
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: "binary" });
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        var range = XLSX.utils.decode_range(ws["!ref"]);
        var C,
          R = range.s.r;
        let excel_headers = this.excel_headers;
        excel_headers = excel_headers.map((v) =>
          v.toLowerCase().replace(/ /g, "")
        );
        for (C = range.s.c; C <= range.e.c; ++C) {
          var cell = ws[XLSX.utils.encode_cell({ c: C, r: R })];
          var hdr = "UNKNOWN " + C;
          if (
            cell &&
            cell.t &&
            excel_headers.includes(cell.v.toLowerCase().replace(/ /g, ""))
          )
            hdr = XLSX.utils.format_cell(cell);
          else {
            alert("Kindly refer/used sample template and try to upload");
            return false;
          }
          // headers.push(hdr.toLowerCase().replace(/ /g, ""));
          headers.push(hdr);
        }
        this.Upload(ev.target.files[0], ev, headers);
      };
      reader.readAsBinaryString(ev.target.files[0]);
    }
    return false;
  }
  Upload(data, evt, headers) {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      let arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i)
        arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
      let array = [];
      array = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      if (array.length == 0)
        return this.cs.showError("Kindly fill data in excel");
      else this.validateExcel(array, evt, headers);
    };
    fileReader.readAsArrayBuffer(data);
  }

  async validateExcel(array, ev, headers) {
    // for (let i = 0; i < this.excel_headers.length; i++) {
    let len = array.length
    await console.log(len)
    for (let j = 0; j < len; j++) {
      await console.log(len)
      await console.log(j)
      console.log(j<len)
      if(j < len){
        for (let i = 0; i < headers.length; i++) {
  
          if (
            this.cs.isUndefinedORNull(array[j][headers[i]]) && (i !=2)
          ) {
            console.log(array[j]);
            this.cs.showError("Kindly Fill The Mandatory Fields")
            this.cs.showError("Error in Column: "+headers[i]+" Row no."+ j)
            return false;
          }
        }
      }else{
        break;
      }
    }

    await ('Validated')
    this.sending_array = array;
    this.toast.success("File has been validated kindly click on 'Send Uploaded File'")
  }
  downloadTemplate() {
    this.cs.exportJsonAsExcelFile(
      [{
        vendorCode: null,
        vendor: null,
        salutation: null,
        participantName: null,
        emailID: null,
        phoneNo: null,
        hotel: null
      }],
      "template.xls"
    );
  }

  async bulkUpload() {
    console.log(this.sending_array);
    this.sending_array.forEach(element=>{
      let body: any = {
        ManageUserID: 0,
        vendorCode: element.vendorCode,
        vendor: element.vendor,
        salutation: element.salutation,
        participantName: element.participantName,
        emailID: element.emailID,
        phoneNo: element.phoneNo,
        hotel: element.hotel,
        actionType: "INSERT",
        createdBy: "admin"
      };
      this.http.call("SMRegistration/ManageUser", body).subscribe((response) => {
        console.log(response)
      })
    })

    await console.log('All Vendors Uploaded')

    this.cs.showSuccess('File Uploaded Successfully')
  }
}
