import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierMeetCMSComponent } from './supplier-meet-cms.component';

describe('SupplierMeetCMSComponent', () => {
  let component: SupplierMeetCMSComponent;
  let fixture: ComponentFixture<SupplierMeetCMSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierMeetCMSComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierMeetCMSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
