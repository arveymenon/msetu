import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { NavItem } from './nav-item';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SupplierMeetRegUserModalComponent } from '../modal/supplier-meet-reg-user-modal/supplier-meet-reg-user-modal.component';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import * as _ from "lodash";
import { DateAdapter, MAT_DATE_FORMATS } from "@angular/material";
import { AppDateAdapter,APP_DATE_FORMATS } from '../myBusiness/oesupplies/date-format';
// import { AppDateAdapter, APP_DATE_FORMATS} from '././date-format';

import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-supplier-meet-cms',
  templateUrl: './supplier-meet-cms.component.html',
  styleUrls: ['./supplier-meet-cms.component.scss'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]
})
export class SupplierMeetCMSComponent implements OnInit {
  content : any;
  
  roleId = CryptoJS.AES.decrypt(
    localStorage.getItem("WTIwNWMxcFZiR3M9"),
    ""
  ).toString(CryptoJS.enc.Utf8);
  displyMenu: any;
  parentMenu: any;
  selectCheck: boolean;
  ContentTextArea:any;
  filterPageNo: any = 1;
  maxDate:any;
  pageSize;

  
  // Edit Configuration
  config = {
    uiColor: '#ffffff',
    toolbarGroups: [
    // { name: 'clipboard', groups: ['clipboard', 'undo'] },
    // { name: 'editing', groups: ['find', 'selection', 'spellchecker'] },
    // { name: 'links' },
    // { name: 'insert' },
    // { name: 'document', groups: ['mode', 'document', 'doctools'] },
    { name: 'basicstyles', groups: ['basicstyles'] },
    { name: 'paragraph', groups: ['list', 'align'] },
    { name: 'styles' },
    { name: 'colors' }],
    skin: 'kama',
    // { name: 'basicstyles', groups: ['basicstyles'] },
    // { name: 'paragraph', groups: ['list', 'align'] },
    // { name: 'colors' }],

    resize_enabled: true,
    removePlugins: 'elementspath,save,magicline',
    extraPlugins: 'divarea,smiley,justify,indentblock,colordialog',
    colorButton_foreStyle: {
       element: 'font',
       attributes: { 'color': '#(color)' }
    },
    height: 30,
    removeDialogTabs: 'image:advanced;link:advanced',
    removeButtons: 'Subscript,Superscript,Anchor,Source,Table',
    format_tags: 'p;h1;h2;h3;pre;div'
 }
 
  menu: NavItem [] = [
    {
      displayName: 'Home Page',
      iconName: '',
      route: '',
      parentID:0
    },        
    {
      displayName: 'Location',
      iconName: '',
      route: '',
      parentID:1,
      children: [
        {
          displayName: 'About',
          iconName: '',
          route: '',
          childID:1
        },
        {
          displayName: 'Night Life',
          iconName: '',
          route: '',
          childID:2
        },
        {
          displayName: 'Place To Eat',
          iconName: '',
          route: '',
          childID:3
        },
        {
          displayName: 'Place To Shop',
          iconName: '',
          route: '',
          childID:4
        },
        {
          displayName: 'Place To Visit',
          iconName: '',
          route: '',
          childID:5
        },
        {
          displayName: 'Tips During Stay',
          iconName: '',
          route: '',
          childID:6
        },
        {
          displayName: 'Dos & Donts',
          iconName: '',
          route: '',
          childID:7
        }
      ]
    },
    {
      displayName: 'Conference',
      iconName: '',
      route: '',
      parentID:2,
      children: [
        {
          displayName: 'Itinerary',
          iconName: '',
          route: '',
          childID:8
        },
        {
          displayName: 'Hotel',
          iconName: '',
          route: '',
          childID:9
        },
        {
          displayName: 'How To Reach',
          iconName: '',
          route: '',
          childID:10
        },
      ]
   
    },
    {
      displayName: 'Registration',
      iconName: '',
      route: '',
      children: [
        {
          displayName: 'Registration',
          iconName: 'how_to_reg',
          route: ''
        },
        {
          displayName: 'Registered Users',
          iconName: 'how_to_reg',
          route: ''
        },
        {
          displayName: 'Managed Users',
          iconName: 'how_to_reg',
          route: ''
        }
       ]
    },
    {
      displayName: 'History',
      iconName: '',
      route: '',
    },    
    {
      displayName: 'Contact Us',
      iconName: '',
      route: '',
    },     
   
  ];
  locationDataSource=[]
  dragRowData = [ '1002','1002','1002','1002','1002','1002'];
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  HomePageForm:any;
  historyDataSource = []
  DataSource = [    
  ];
  displayedColumns: string[] =['name', 'hotel','Organisation','Designation','PhoneNo','MobileNo','emergencyContactNo','Email','ageGroup','healthIssues','FoodPreference','SmokingPreferences','Liquor','RoomRequirement','AirelineName','FlightNumber','DateofArrival','DateofDeparture','TimeofArrival','TimeOfDeparture','SharingWith','PreBooking','Passport','Created','CreatedBy','Modified','ModifiedBy','RegisterWF','DateofBirth','action'];
  historydisplayedColumns: string[] = ['Location','Year','HeaderImageName','backgroundImageName']
  current_year=[];
  Backgrd_Images=[];
  backgrdImageName=[];
  urls=[];
  headerImgURL: any;
  headr_images=[];
  isUpdate: boolean=false;
  LocationForm: FormGroup;
  parentID: any;
  childID: any;
  locationImages=[];
  TipsStayForm: any;
  stayID: any;
  Do: any='';
  Dos: any='';
  howToReachForm: any;
  howToReachFile=[];
  DosID: any;
  Itinerary=[];
  ItineraryFile=[];
  ItineraryFileName: any='';
  ContactForm: any;
  contactID: any;
  howToReachImageURL: any;
  headr_images_ID: any;
  new_imgs=[];
  historybkgImg=[];
  isChild: boolean=true;
  SMID: any;
  reach_id: any;
  trimmedAddress:any;
  @ViewChild('form')form:any;
  register_users=[];
  pageSize1;
  constructor(public cs:MySuggestionService,public commonService: CommonUtilityService,private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer,private _formBuilder: FormBuilder,private modalService: NgbModal) { 
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon('trash',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/delete.svg'));
    matIconRegistry.addSvgIcon('dragIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/moveicon.svg'));
    matIconRegistry.addSvgIcon('edit',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/Edit.svg'));
    matIconRegistry.addSvgIcon('excelIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/Excel_White_Icon.svg'));
  }

  ngOnInit() {
    this.createHomePageForm();
    this.createLocationForm();
    this.createTeapsForm();
    this.getCurrentYear();
    this.createHowToReachForm();
    this.createContactForm();
    this.getHistoryData(1);
    this.getRegisterUsers(1);
  }
  createHomePageForm() {
    this.HomePageForm = this._formBuilder.group({
      Supplier_Location: [''],
      Supplier_Year: [''],
      Supplier_bckImage: [''],
      Supplier_HdrImage: [''],
    });
  }

  createLocationForm() {
    this.LocationForm = this._formBuilder.group({
      Image: [''],
      Title: ['',Validators.required],
      Contents: ['',Validators.required],
    });
  }

  createTeapsForm() {
    this.TipsStayForm = this._formBuilder.group({
      Documents: [''],
      Smoking: [''],
      Precautions: [''],
      TheftHotel: [''],
    });
  }

  createHowToReachForm() {
    this.howToReachForm = this._formBuilder.group({
      how_to_reach: ['',Validators.required],
      Address: ['',Validators.required],
      file: ['',Validators.required],
    });
  }

  createContactForm() {
    // ^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$
    this.ContactForm = this._formBuilder.group({
      Email: ['',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      Contact_No: ['',Validators.required],
      Address: ['',Validators.required]
      // ,Validators.pattern("^[^\s].+[^\s]$")]
    });
  }

  getCurrentYear(){
    this.cs.postMySuggestionData('SupplierMeetAdmin/GetCurrentYear','').subscribe((res:any)=>{
      this.current_year=res.ResponseData;
    })
  }
  checkAll(e){
    if(e.checked == true){
      this.register_users.filter((element,i) => {
        element["checked"] = true;       
      }); 
    }
    else{
      this.register_users.filter((element,i) => {
      element["checked"] = false;       
    });
  }
  }
  getRegisterUsers(pageNo: any){
    let Request={
      "PageNo": pageNo
    }
    this.cs.postMySuggestionData('SMRegistration/GetRegisteredUserDetails',Request).subscribe((res:any)=>{
     if(!this.cs.isUndefinedORNull(res.ResponseData)){
      this.register_users=res.ResponseData;
      this.pageSize = res.TotalCount;
     }
      else
      this.cs.showError("Something went wrong please try again");
    })
  }
  
  deleteUserDetails(wid){
    let Request={
      "wid": wid
    }
    this.cs.postMySuggestionData('SMRegistration/DeleteRegisterVendor',Request).subscribe((res:any)=>{
     if(res.ID == "1"){
       this.getRegisterUsers(1)
     }
      else
      this.cs.showError("Something went wrong please try again");
    })
  }

  exportExcel(){
    let postData={
      "PageNo":0
    }
     this.cs.postMySuggestionData('SMRegistration/GetRegisteredUserDetails',postData).subscribe((data)=>{
       let json = data.ResponseData;
       this.cs.exportJsonAsExcelFile(json,'RegisterUsers');
     })
  }

  detectFiles(event: any,ID) {
    // this.headerImgURL='';
    if (ID==1 && this.urls.length >= 3) {
      this.cs.showError("you can add only 3 images");
      return false;
    }
    if (ID==2 && this.headr_images.length >= 1) {
      this.cs.showError("you can add only 1 images");
      return false;
    }
    if(ID==4 && this.Itinerary.length > 0){
      this.cs.showError("To add new image kindly, delete current file and try again");
      return false;
    }
    let files = event.target.files;
    if (files) {
      this.locationImages=[];
      let filename = ''
      files.forEach(file => filename = filename+file.name+', ');
      for (let file of files) {
        let ext = file.name.split(".").pop().toLowerCase();
        let reader = new FileReader();
        reader.onload = (e: any) => {
          if (ext != 'jpg' && ext != 'jpeg' && ext != 'png' && ID != 4){
            this.cs.showError("Please select 'jpg', jpeg' or 'png' format");
            return false;
          }
         else if(ID==1)
          {
            this.backgrdImageName.push(file.name);
            this.urls.push(file);
            this.HomePageForm.get('Supplier_bckImage').setValue(this.backgrdImageName.join(','));
          }
          else if(ID==2)
          {
            this.headr_images=[];
            this.headr_images.push(file);
            this.HomePageForm.get('Supplier_HdrImage').setValue(file.name);
          }
          else if(ID==3)
          {
            this.locationImages.push(file);
            this.LocationForm.get('Image').setValue(filename);
          }
          else if(ID==4)
          {
            this.ItineraryFile=[];
            this.ItineraryFile.push(file);
            this.locationImages=[];
            this.locationImages.push(file);
            this.ItineraryFileName = file.name;
          }
          else if(ID==5)
          {
            this.howToReachFile=[];
            this.howToReachFile.push(file);
            this.howToReachForm.get('file').setValue(file.name);
          }
          return false
        }
        reader.readAsDataURL(file);
      }
    }
    return false
  }

  DeleteHomeHeaderImg(ID:any){
    this.cs.postMySuggestionData('SupplierMeetAdmin/DeleteSMImagesByID', {
      "SM_MainImageID": ID
    })
      .subscribe((res: any) => {
        if (res) {
          this.getYearWiseData();
          this.headr_images=[];
          this.headerImgURL='';
        }
      })

  }

  DeleteLocYear(data: any,val) {
    data.value = '';
    if(val=='Supplier_Year'){
      this.urls=[];
      this.headerImgURL='';
      this.headr_images=[];
      this.Backgrd_Images=[];
      this.backgrdImageName=[];
      this.HomePageForm.get('Supplier_Location').setValue('');
    }
    this.HomePageForm.get(val).setValue('');
  }

  getYearWiseData(){
    this.cs.postMySuggestionData('SupplierMeetAdmin/GetSupplierMeetDetails',{
      "Year": this.HomePageForm.controls.Supplier_Year.value
    }).subscribe((res: any) => {
      this.urls = _.cloneDeep(res.ResponseData.BackgroundIamges);
      this.new_imgs = res.ResponseData.BackgroundIamges;
      this.HomePageForm.get("Supplier_Location").setValue(res.ResponseData.Location);
      if(this.cs.isUndefinedORNull(this.urls)){
        this.urls=[];
      }
      if(this.urls.length>=1){
        this.isUpdate = true;
      }
      this.isUpdate = true;
      this.SMID = res.ResponseData.SMID;
      if(!this.cs.isUndefinedORNull(res.ResponseData.HeaderIamges)){
        this.headerImgURL = res.ResponseData.HeaderIamges.ImagePath;
        this.headr_images.push(res.ResponseData.HeaderIamges.ImagePath);
      }
      if(this.cs.isUndefinedORNull(this.headr_images)){
        this.headr_images=[];
      }
      this.headr_images_ID = res.ResponseData.HeaderIamges.SM_MainImageID;
      
    })
  }

  DeleteImage(ID: any) {
    this.cs.postMySuggestionData('SupplierMeetAdmin/DeleteSMImagesByID', {
      "SM_MainImageID": ID
    })
      .subscribe((res: any) => {
        if (res) {
          this.getYearWiseData();
        }
      })
  }

   saveHomepageData(){
     let Reqjson ={
       "location": this.HomePageForm.controls.Supplier_Location.value,
       "year": this.HomePageForm.controls.Supplier_Year.value,
       "createdBy": "",
       "actionType": !this.isUpdate?"INSERT":"UPDATE",
       "smid": this.SMID?this.SMID:null
      }
      this.Backgrd_Images = this.urls;
      for(let i = this.Backgrd_Images.length - 1; i >= 0; --i){
        if(!this.cs.isUndefinedORNull(this.Backgrd_Images[i].ImagePath)){
          this.Backgrd_Images.splice(i,1)
        }
      }
      let formData: FormData = new FormData;
      formData.append('Reqjson', JSON.stringify(Reqjson));
      for (var j = 0; j < this.Backgrd_Images.length; j++) {
      formData.append(`BkgImg${j+1}`, this.Backgrd_Images[j]);
    }
    formData.append('HeaderImg', this.headr_images[0]);
    this.cs.postMySuggestionData('SupplierMeetAdmin/IUSupplierMeet', formData).
        subscribe(
          (res: any) => {
            this.cs.showSwapLoader=false;
            if (res)
            {
              this.cs.showSuccess("Data Stored Successfully");
              this.HomePageForm.reset();
              this.Backgrd_Images=[];
              this.urls=[];
              this.new_imgs=[];
              this.headr_images=[];
              this.headerImgURL='';
              this.backgrdImageName=[];
            }
            else
            this.cs.showError("Please try again");
          },
          (error) => {
            this.cs.showSwapLoader=false;
            this.cs.showError(error);
          }
        )
   }

    
  getParentMenuData(menuName,parentID){
    this.parentMenu = menuName;
    this.parentID = parentID
    if(this.parentMenu == 'Contact Us'){
      this.getContactDetails();
      return;
    }
  }

 

  getChildMenuData(menuName,parentMenu,parentID,childID){
    this.LocationForm.reset();
    this.parentMenu = parentMenu;
    this.displyMenu = menuName;
    this.parentID = parentID
    this.childID = childID;
    if(this.displyMenu == 'Tips During Stay'){
      this.getTipsStatyData();
      return;
    }
    else if(this.displyMenu == 'Dos & Donts'){
      this.getDoDontsData();
      return;
    }
    else if(this.displyMenu == 'How To Reach'){
      this.getHowToReachData();
      return;
    }
    else if(this.displyMenu == 'Registration'||this.parentMenu=='Registration'){
      this.getRegisterUsers(1);
    }
    else{
      let request = {
        "SMCategoryID":this.parentID,
        "SMSubCategoryID":this.childID,
        "PageNo":"1"
      }
      this.cs.postMySuggestionData('SupplierMeetAdmin/GetSMContentByCateAndSubCate',request)
        .subscribe((res: any) => {
          if (res.Message== "Success") {
           // this.form.resetForm();
            this.locationDataSource=res.ResponseData;
            if(this.displyMenu == 'Itinerary'){
              this.Itinerary = res.ResponseData
              if(this.cs.isUndefinedORNull(res.ResponseData))
                   this.isUpdate=false;
              else
                  this.isUpdate=true;
            }
            return;
          }else{
            this.cs.showSuccess("Some error occured please try again later");
            return;
          }
        })
    }
  }

  addData(){
    let isItinerary: boolean;
    // if(this.LocationForm.controls.Title.value == '' || this.LocationForm.controls.Contents.value == '' || this.LocationForm.controls.Image.value == ''){
    //   this.cs.showError('Please fill all mandatory details');
    //   return;
    // }
    if(this.LocationForm.valid || this.displyMenu == 'Itinerary'){
      if (this.displyMenu == 'Itinerary'){
        if(this.cs.isUndefinedORNull(this.ItineraryFileName)){
          this.cs.showError("Please add file");
          return;
        }
        isItinerary = true;
      }
    else
      isItinerary=false;
    let Reqjson= {
      "smContentID":0,
      "smContentTitle": !isItinerary?this.LocationForm.controls.Title.value:'',
      "smContentDescription":!isItinerary?this.ContentTextArea:'',
      "smCategoryID":this.parentID,
      "smSubCategoryID":this.childID,
      "isActive":true,
      "createdBy":"sgsWg@g",
      "actionType": "INSERT"
    }
    let formData: FormData = new FormData;
    formData.append('Reqjson', JSON.stringify(Reqjson));
    if(this.locationImages[0]){
      this.locationImages.forEach((file, index)=>{
        if(index == 0){
          formData.append(`Image`,file)
        }else{
          formData.append(`Image${index}`,file)
        }
      })
    }
    this.cs.postMySuggestionData('SupplierMeetAdmin/IUSupplierMeetContent',formData)
      .subscribe((res: any) => {
        if (res.Message == "Success") {
          this.cs.showSuccess("Data Stored Successfully");
          this.getChildMenuData(this.displyMenu,this.parentMenu,this.parentID,this.childID);
          this.form.resetForm();
          this.ContentTextArea='';
          return;
        }else{
          this.cs.showError("Some error occured please try again later");
          return;
        }
      })
    }
    else{
      this.cs.showError('Please fill all mandatory details');
      return;
    }
  
  }

  DeleteTableData(ID:any){
    this.cs.postMySuggestionData('SupplierMeetAdmin/DeleteSMContentByID',{"SMContentID":ID})
    .subscribe((res: any) => {
      if (res.Message == "Success") {
        this.cs.showSuccess("Data deleted successfully");
        if(this.displyMenu=='Itinerary'){
          this.ItineraryFileName='';
        }
        this.getChildMenuData(this.displyMenu,this.parentMenu,this.parentID,this.childID) 
        return;
      }else{
        this.cs.showError("Some error occured please try again later");
        return;
      }
    })
  }

  getTipsStatyData(){
    this.cs.getMySuggestionData('SMRegistration/GetDuringStayDetails')
      .subscribe((res: any) => {
        if (res.Message == "Data not found"){
          this.isUpdate=false;
        }
        else{
          this.isUpdate=true;
          this.TipsStayForm.get('Documents').setValue(res.ResponseData[0].Documents);
          this.TipsStayForm.get('Smoking').setValue(res.ResponseData[0].Smoking);
          this.TipsStayForm.get('Precautions').setValue(res.ResponseData[0].Precautions);
          this.TipsStayForm.get('TheftHotel').setValue(res.ResponseData[0].HotelDetails);
          this.stayID = res.ResponseData[0].StayID;
          }
        })
  }

  getDoDontsData(){
    this.cs.getMySuggestionData('SMRegistration/GetDoDontDetails')
      .subscribe((res: any) => {
        if (res.Message == "Data not found") {
          this.isUpdate=false;
        }
        else{
          this.isUpdate=true;
          this.Do = res.ResponseData[0].Do;
          this.Dos = res.ResponseData[0].Dont;
          this.DosID = res.ResponseData[0].ID;
          }
        })
  }


  saveDoandDos(){
    let Requset =
    {
      "Do": this.Do,
      "Donts": this.Dos,
      "Parameter": this.isUpdate?"U":"I",
      "ID": this.isUpdate?this.DosID:null
    }
    this.cs.postMySuggestionData('SMRegistration/InsertDoDonts',Requset)
      .subscribe((res: any) => {
        if (res.Message == "Data"+this.isUpdate?"updated":"inserted"+ "successfully") {
          this.cs.showSuccess(res.Message);
          // Form.reset();
          return;
        }else{
          this.cs.showError("Some error occured please try again later");
          return;
        }
      })
    
  }

  saveTipsStay(Form:any){
    let Requset=
    {
      "documents": Form.controls.Documents.value,
      "smoking": Form.controls.Smoking.value,
      "precautions": Form.controls.Precautions.value,
      "hotelDetails": Form.controls.TheftHotel.value,
      "Parameter":this.isUpdate?"U":"I",
      "StayID":this.stayID
    }
    this.cs.postMySuggestionData('SMRegistration/InsertDuringStay',Requset)
      .subscribe((res: any) => {
        if (res.Message == "Data"+this.isUpdate?"updated":"inserted"+ "successfully") {
          this.cs.showSuccess(res.Message);
          // Form.reset();
          return;
        }else{
          this.cs.showError("Some error occured please try again later");
          return;
        }
      })
    
  }


  getHowToReachData(){
    this.cs.getMySuggestionData('SMRegistration/GetHowToReachDetails')
    .subscribe((res: any) => {
      if (res.Message == "Data not found") {
        this.isUpdate=false;
      }
      else{
        this.isUpdate=true;
        this.howToReachForm.get('how_to_reach').setValue(res.ResponseData[0].HowToReach);
        this.howToReachForm.get('Address').setValue(res.ResponseData[0].Address);
        this.howToReachForm.get('file').setValue(res.ResponseData[0].ImageName);
        this.howToReachImageURL = res.ResponseData[0].ImagePath;
        this.reach_id = res.ResponseData[0].ReachID;
        }
      })
  }

  getContactDetails(){
    this.cs.getMySuggestionData('SMRegistration/GetContactUsDetails')
    .subscribe((res: any) => {
      if (res.Message == "Data not found") {
        this.isUpdate=false;
      }
      else{
        this.isUpdate=true;
        this.ContactForm.get('Email').setValue(res.ResponseData[0].Email);
        this.ContactForm.get('Contact_No').setValue(res.ResponseData[0].ContactNo);
        this.ContactForm.get('Address').setValue(res.ResponseData[0].Address);
        this.contactID = res.ResponseData[0].ID;
        }
      })
  }
  getPageChangeData(ev, id) {
    if (id == 1) {
      this.filterPageNo = ev.pageIndex + 1;
      this.getRegisterUsers(ev.pageIndex + 1);
      // this.getRegisterUsers();
    }
    else {
      this.filterPageNo = ev.pageIndex + 1;
      this.getHistoryData(ev.pageIndex + 1);
    //  this.getEmployeeApprovalData(ev.pageIndex + 1, this.DDLValue);
    }
  }
 
  saveContactDetails(){
    this.trimmedAddress = this.ContactForm.controls.Address.value.trim();
    if(this.ContactForm.controls.Contact_No.value =='' ||  this.ContactForm.controls.Email.value=='' || this.ContactForm.controls.Address.value ==''){
      this.cs.showError('Please fill all mandatory details');
      return;
    }
    if (!(this.ContactForm.controls.Address.value).replace(/\s/g, '').length) {
      this.cs.showError('Address Should Not Be Blank');
      return;
    }
    else if(this.ContactForm.valid){
      let Request={
        "parameter": this.isUpdate?"U":"I",
        "ContactNo": this.ContactForm.controls.Contact_No.value,
        "Email": this.ContactForm.controls.Email.value,
        "Address": this.ContactForm.controls.Address.value,
        "contactID": this.contactID
      }


     // else{
      this.cs.postMySuggestionData('SMRegistration/InsertUpdateContactUS',Request)
        .subscribe((res: any) => {
          if(res.Message == "Data"+this.isUpdate?"updated":"inserted"+ "successfully") {
            this.cs.showSuccess(res.Message); 
            return;
          }else{
            this.cs.showError("Some error occured please try again later");
            return;
          }
        })   
  //  }
    }
   else{
    this.cs.showError('Please fill valid details');
   }
  }
  numberOnly(event): boolean {
   
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      this.cs.showError("Contact No Only Accepts Numeric Values");
      return false;
     
    }
    
    return true;

}

  AddLocationData(){
    this.LocationForm.get('Contents').setValue(this.ContentTextArea);
    this.closeModal();
  }



  saveHowToReachData(Form){
    if(!this.cs.isUndefinedORNull(Form.controls.how_to_reach.value) && !this.cs.isUndefinedORNull(Form.controls.Address.value)  && !this.cs.isUndefinedORNull(Form.controls.Address.value)){
      let Requset = {
        "parameter": this.isUpdate?"U":"I",
        "howToReach": Form.controls.how_to_reach.value,
        "address": Form.controls.Address.value,
        "reachID": this.isUpdate?this.reach_id:0
      }
          let formData: FormData = new FormData;
          formData.append('Reqjson', JSON.stringify(Requset));
          if(this.howToReachFile[0]){
            formData.append('Image', this.howToReachFile[0]);
          }
          this.cs.postMySuggestionData('SMRegistration/InsertUpdateHowToReach',formData)
          .subscribe((res: any) => {
            if (res.ID != 0) {
              this.cs.showSuccess(res.Message);
              this.getHowToReachData();
              return;
            }else{
              this.cs.showError(res.Message);
              return;
            }
          })
    }
   else{
    this.cs.showError("Please fill all mandatory fields");
    return;
   }
    
  }



  getHistoryData(pageNo){
    let reqData={
      "PageNo": pageNo
    }
    this.cs.postMySuggestionData('SupplierMeetAdmin/GetSupplierMeetHistory',reqData).subscribe((res:any)=>{
      this.historyDataSource=res.ResponseData;
      this.pageSize1= res.TotalCount;
      this.historybkgImg = res.ResponseData.BackgroundIamges;
      // this.historybkgImg = res.ResponseData.HeaderIamges;
    })
  }





  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.dragRowData, event.previousIndex, event.currentIndex);
  }
  
  closeModal(){
    this.modalService.dismissAll();
  }
  open(content) {
    this.modalService.open(content, {  size: 'lg',centered:true, windowClass : "formModal"});
  }
  openModal(element){  
    let modal = this.modalService.open(SupplierMeetRegUserModalComponent, {  size: 'xl' as 'lg'} );
    modal.componentInstance.details = element
  }

  getExt(path){
    return path.split('.').reverse()[0].toLowerCase()
  }

  viewFile(filePath){
    window.open(filePath, '_blank')
  }


}
