export interface NavItem {
  displayName: string;
  disabled?: boolean;
  iconName: string;
  route?: string;
  parentID?:number;
  childID?:number;
  children?: NavItem[];
}