import { Component, OnInit } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { SurveyService } from "../services/survey/survey.service";
import {
  FormArray,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";

import * as _ from "lodash";
import { NgbRatingConfig } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import * as CryptoJS from 'crypto-js';
@Component({
  selector: "app-ssisurvey",
  templateUrl: "./ssisurvey.component.html",
  styleUrls: ["./ssisurvey.component.scss"],
  providers: [NgbRatingConfig],
})
export class SSISurveyComponent implements OnInit {
  roleId = localStorage.getItem('WTIwNWMxcFZiR3M9');
  panelOpenState = false;
  tabIndex = 0;
  tabCount = 0;
  SupplierCode: any;
  public plants = [];
  public surveyForm = new FormArray([]);
  errors = []
  responseQuestions: any = [
    // {
    //   plantId: 1,
    //   plantName: 'Shivri',
    //   sections: [
    //     {
    //       sectionId: 1,
    //       sectionName: 'A',
    //       questions: [
    //         {
    //           questionId: 100,
    //           Question: 'Testing Question',
    //           answerType: 1,
    //           options: [
    //             { optionId: 1, optionName: 'Option1'},
    //             { optionId: 2, optionName: 'Option2'},
    //             { optionId: 3, optionName: 'Option3'},
    //           ]
    //         },
    //         {
    //           questionId: 102,
    //           Question: 'Testing Question 2',
    //           answerType: 2,
    //           options: null
    //         },
    //         {
    //           questionId: 103,
    //           Question: 'Testing Question 3',
    //           answerType: 3,
    //           options: null
    //         },
    //       ]
    //     },
    //     {
    //       sectionId: 12,
    //       sectionName: 'B',
    //       questions: [
    //         {
    //           questionId: 104,
    //           Question: 'Testing Question',
    //           answerType: 1,
    //           options: [
    //             { optionId: 1, optionName: 'Option1'},
    //             { optionId: 2, optionName: 'Option2'},
    //             { optionId: 3, optionName: 'Option3'},
    //           ]
    //         },
    //         {
    //           questionId: 105,
    //           Question: 'Testing Question 2',
    //           answerType: 2,
    //           options: null
    //         },
    //         {
    //           questionId: 106,
    //           Question: 'Testing Question 3',
    //           answerType: 2,
    //           options: null
    //         },
    //       ]
    //     },
    //     {
    //       sectionId: 13,
    //       sectionName: 'C',
    //       questions: [
    //         {
    //           questionId: 107,
    //           Question: 'Testing Question',
    //           answerType: 1,
    //           options: [
    //             { optionId: 1, optionName: 'Option1'},
    //             { optionId: 2, optionName: 'Option2'},
    //             { optionId: 3, optionName: 'Option3'},
    //           ]
    //         },
    //         {
    //           questionId: 108,
    //           Question: 'Testing Question 2',
    //           answerType: 2,
    //           options: null
    //         },
    //         {
    //           questionId: 109,
    //           Question: 'Testing Question 3',
    //           answerType: 2,
    //           options: null
    //         },
    //       ]
    //     }
    //   ]
    // },
    // {
    //   plantId: 2,
    //   plantName: 'Kandivali',
    //   sections: [
    //     {
    //       sectionId: 2,
    //       sectionName: 'A',
    //       questions: [
    //         {
    //           questionId: 10,
    //           Question: 'Testing Question',
    //           answerType: 1,
    //           options: [
    //             { optionId: 1, optionName: 'Option1'},
    //             { optionId: 2, optionName: 'Option2'},
    //             { optionId: 3, optionName: 'Option3'},
    //           ]
    //         },
    //         {
    //           questionId: 11,
    //           Question: 'Testing Question 2',
    //           answerType: 2,
    //           options: null
    //         },
    //         {
    //           questionId: 12,
    //           Question: 'Testing Question 3',
    //           answerType: 2,
    //           options: null
    //         },
    //       ]
    //     }
    //   ]
    // }
  ];

  constructor(
    public commonService: CommonUtilityService,
    public surveyService: SurveyService,
    public rating: NgbRatingConfig,
    public formBuilder: FormBuilder,
    public toast: ToastrService,
    public router: Router
  ) {
    this.commonService.changeIsAuthenticate(true);
    this.rating.max = 5;
  }

  username:any;

  ngOnInit() {
    this.roleId = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8);
    this.username = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
    this.username = CryptoJS.AES.decrypt(this.username,"").toString(CryptoJS.enc.Utf8)
    this.SupplierCode = localStorage.getItem("vendorCode") != "null" ? localStorage.getItem("vendorCode") : "DM089Y";
    this.SupplierCode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
    this.SupplierCode = CryptoJS.AES.decrypt(this.SupplierCode,"").toString(CryptoJS.enc.Utf8);
    this.getQuestions();
  }

  getQuestions(SupplierCode?: any) {
    this.isSubmitted().then(allow=>{
      if (allow) {
        let body = {
          SupplierCode: SupplierCode || this.SupplierCode,
          // "SupplierCode": "DM089Y"
        };
        this.surveyService
          .getSSISurveyQuestions("AdminCrudSurvey/GetSuveryMyBusiness", body)
          .subscribe((res: any) => {
            console.log(res);
            if (res && res.length != 0) {
              this.plants = res;
              this.setQuestionsForm();
            } else {
              // this.router.navigate(["/dashboard"]);
              this.errors.push("No Survey for this user")
              // this.toast.show("No Survey for this user");
              // this.getQuestions("DM089Y")
            }
            // if(res.Message == 'Success') {
              // this.allQuestions = res.ResponseData.filter(o => o.SurveyTypeId == 2);
              // }
            });
          } else {
            this.errors.push("Survey Already Submitted for this user")
        }
    })
  }

  isSubmitted() {
    return new Promise((resolve, reject) => {
      let body = {
        SurveyTypeID: 2,
        TokenId:this.username// localStorage.getItem("userToken"),
      };
      this.surveyService
        .getMySurveyQuestions("AdminCrudSurvey/Msetu_Get_SurveyRespCheck", body)
        .subscribe((res) => {
          console.log(res);
          const response = res.ResponseData == "Failed" ? true : false;
          resolve(response);
        });
    });
  }

  setQuestionsForm() {
    console.log(this.plants);
    if (this.plants) {
      // console.log(this.responseQuestions);
      // this.plants = this.responseQuestions;
      this.tabCount = this.plants.length;

      this.plants.forEach((plant) => {
        plant.Sections.forEach((section) => {
          section.panelOpenState = false
          section.questions.forEach((question) => {
            console.log(question);

            console.log(question.questionId);
            question.answer = new FormControl("", Validators.required);
            console.log(this.surveyForm);
          });
        });
      });
    }
  }

  next() {
    console.log(this.tabIndex);
    console.log(this.tabCount);
    this.tabIndex = (this.tabIndex + 1) % this.tabCount;
  }

  submit() {
    this.validatateForm().then(valid=>{
      if(valid){
        console.log(this.plants);
        // this.surveyForm.
        this.plants.forEach((plant, plantInd) => {
          plant.Sections.forEach((section, sectionInd) => {
            section.questions.forEach((question, questionInd) => {

              const body = {
                TokenId:this.username,// localStorage.getItem('userToken'),
                SurveyId: question.SurveyId,
                VendorCode: this.SupplierCode,
                SurveyQuetionAnsList: [{
                  PlantID: plant.PlantId,
                  Quetion_Id: question.questionId,
                  Answer: question.answer.value
                }]
              }
              console.log(body);
              this.surveyService.postMySurveyQuestions('AdminCrudSurvey/SurveyQuetionAnsResponce', body).subscribe(res => {
                console.log(res);

                if (
                  this.plants.length - 1 == plantInd &&
                  plant.Sections.length - 1 == sectionInd &&
                  section.questions.length - 1 == questionInd
                ) {
                  this.toast.success('Survey Submitted Successfully');
                  history.go(-1);
                }
                // this.modal.dismissAll();
              });



            })
          })
        })




      }
    });
    console.log(this.plants);
    console.log(this.surveyForm);
    // location.reload();

    // if(this.surveyForm.valid){
    //   this.surveyForm.reset();
    // }
  }

  validatateForm() {
    return new Promise((resolve, reject) => {
      this.plants.forEach((plant, plantInd) => {
        plant.Sections.forEach((section, sectionInd) => {
          section.questions.forEach((question, questionInd) => {
            this.surveyForm.push(question.answer);
            console.log(this.surveyForm.valid);
            if (
              this.plants.length - 1 == plantInd &&
              plant.Sections.length - 1 == sectionInd &&
              section.questions.length - 1 == questionInd
            ) {
              console.log("last question");
              if (this.surveyForm.valid) {
                resolve(true)
              }else{
                resolve(false)
                this.toast.error("Kindly Answer All Questions");
              }
            }
          });
        });
      });
      // if (!this.surveyForm.valid) {
      //   this.toast.error("Kindly fill all required fields");
      //   return;
      // }
    });
  }
}
