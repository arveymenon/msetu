import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SSISurveyComponent } from './ssisurvey.component';

describe('SSISurveyComponent', () => {
  let component: SSISurveyComponent;
  let fixture: ComponentFixture<SSISurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SSISurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SSISurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
