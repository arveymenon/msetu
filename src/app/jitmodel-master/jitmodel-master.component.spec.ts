import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JITModelMasterComponent } from './jitmodel-master.component';

describe('JITModelMasterComponent', () => {
  let component: JITModelMasterComponent;
  let fixture: ComponentFixture<JITModelMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JITModelMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JITModelMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
