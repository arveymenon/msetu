import { Component, OnInit, ViewChild } from "@angular/core";
import { MatIconRegistry, MatPaginator } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { JitService } from "../services/jit/jit.service";
import { FormControl } from "@angular/forms";
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import * as CryptoJS from 'crypto-js';
import { MyHelpDeskService } from 'src/app/services/myHelpDesk/my-help-desk.service';
import { Workbook } from 'exceljs';
import * as fs from "file-saver";

@Component({
  selector: "app-jitmodel-master",
  templateUrl: "./jitmodel-master.component.html",
  styleUrls: ["./jitmodel-master.component.scss"]
})
export class JITModelMasterComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator
  roleId = localStorage.getItem('WTIwNWMxcFZiR3M9')
  modelCode = new FormControl();
  partCode = new FormControl();
  DataSource = [];
  displayedColumns: string[] = [
    "modelCode",
    "partNumber",
    "partDescription",
    "aggregate",
    "isActive"
  ];
  pageSize = 0;
  pageNo = 0;

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public commonService: CommonUtilityService,
    public jitService: JitService,
    public cs: MySuggestionService,
    public http: MyHelpDeskService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );

    this.getTableValues(0);
  }

  getTableValues(pageNo) {
    console.log(this.partCode.value);
    console.log(this.modelCode.value);
    const body = {
      PageStart: pageNo + 1,
      RowPerPage: 10,
      ModelCode: this.modelCode.value,
      PartNumber: this.partCode.value
    };
    this.jitService
      .call("PTLiveData/GetModelMaster", body)
      .subscribe(response => {
        console.log(response);
        this.pageSize = response.DataCount
        let dataSource = [];
        for (const model of response.ResponseData) {
          dataSource.push({
            modelCode: model.ModelCode,
            partNumber: model.PartNumber,
            partDescription: model.PartDescription,
            aggregate: model.Aggregates,
            isActive: model.IsActive
          });
        }
        this.DataSource = [...dataSource];
        console.log(this.DataSource);
        this.updateGoto()
      });
  }

  goToControl = new FormControl();
  goTo: number;
    pageNumbers = [];
  
  goToChange(value? : any) {
    this.paginator.pageIndex = this.goTo - 1;
    console.log(value)
    console.log(this.paginator.pageIndex)
    this.goToControl.setValue(this.paginator.pageIndex)
    this.getTableValues(this.paginator.pageIndex)
  }


  updateGoto() {
    this.goTo = 1;
    this.pageNumbers = [];
    var totalLength= this.pageSize / 10;
    var ceiledLength = Math.ceil(totalLength);
    console.log(ceiledLength);
    for (let i = 1; i <= ceiledLength; i++) {
      this.pageNumbers.push(i);
    }
  }

  reset() {
    this.partCode.reset();
    this.modelCode.reset();
    this.getTableValues(0);
  }

  // exportAsExcelFile(table, name: any) {
  //   this.cs.exportAsExcelFile(table, name);
  // }

  getPageChangeData($event){
    console.log($event)
    this.getTableValues($event.pageIndex)
  }

  ngOnInit() {
    this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8);
  }
  exportExcel(JSON) {
    let body = {
      PageStart: 0,
      RowPerPage: 10,
      ModelCode: this.modelCode.value,
      PartNumber: this.partCode.value
    }
    this.http.call("PTLiveData/GetModelMaster", body).subscribe(async (res) => {
      if (res.Message == "Success") {
        let dataSource = []
        res.ResponseData.forEach((data, index) => {
          dataSource.push({
            modelCode: data.ModelCode,
            partNumber: data.PartNumber,
            partDescription: data.PartDescription,
            aggregate: data.Aggregates,
            isActive: data.IsActive
          });
          })

        let dataNew = dataSource
        // this.DataSource;
        await console.log(dataNew);
        //Create workbook and worksheet
        let workbook = new Workbook();
        let worksheet = workbook.addWorksheet("ReportData");
        let columns = Object.keys(this.DataSource[0]);
        let headerRow = worksheet.addRow(columns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: "00FF0000" },
            bgColor: { argb: "00FF0000" },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
        });
        dataNew.forEach((d) => {
          // var obj = JSON.parse(d);
          var values = Object.keys(d).map(function (key) {
            return d[key];
          });
          let row = worksheet.addRow(values);
        });
        workbook.xlsx.writeBuffer().then((dataNew) => {
          let blob = new Blob([dataNew], {
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          fs.saveAs(
            blob,
            "Model Master Details.xlsx"
          );
        });
      }
    })
  }
}
