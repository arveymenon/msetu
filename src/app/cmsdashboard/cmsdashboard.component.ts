import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CommonUtilityService } from '../services/common/common-utility.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { AdminDashboardService } from '../services/admin_Dashboard/admin-dashboard.service';
import { FormBuilder, Validators, FormControl, FormGroup, FormArray  } from '@angular/forms';
import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS } from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS} from '././date-format';
import * as moment from "moment";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
// import { identity } from 'lodash';
import * as CryptoJS from "crypto-js";
import { Workbook } from 'exceljs';
import * as fs from "file-saver";
// import { MySuggestionService } from 'MYBACKUP 15-05-2020/src/app/services/MySuggestion/my-suggestion.service';

@Component({
  selector: 'app-cmsdashboard',
  templateUrl: './cmsdashboard.component.html',
  styleUrls: ['./cmsdashboard.component.scss'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]
})
export class CmsdashboardComponent implements OnInit {
  displayedColumns3: string[] = [
    "SearchField",
    "Link",
    "Likes"
    // "Description",
    // "SearchName",
    // "action"
  ];
  maintainanceDisplayedColumns: string[] = [
    "SearchField",
    "Link",
    "Link1"
    // "Description",
    // "SearchName",
    // "action"
  ];
  categoryID:any;
  pageNo: number = 1;
  pageSize: number;
  historyDataSource:any=[];
  fromDate: Date;
  toDate: Date;
  urls=[];
  years:number;
  noOfYears=[];
  fileUrls:any=[];
  fileAwardUrls:any=[];
  fileName:any=[];
  filesName:any=[];
  totalArray:any=[];
  acc:any=[];
  ext:any=[];
  pdffileUrls= [];
  pdfFileName:any;
  marqueeContent:any;
  NewUpdatesForm:FormGroup;
  whatsNewForm  :FormGroup;
  @ViewChild('form')form;
  @ViewChild('formNewUpdate')formNewUpdate;
  @ViewChild('formWhatsNew')formWhatsNew;
  @ViewChild('formAward')formAward;
  awardForm   :FormGroup;
  marqueeForm :FormGroup;
  MaintainanceForm: FormGroup;
  getMultiImages:any=[];
  dragRowData :any;
  dragRowData1:any;
  dragRowData2:any;
  dragRowData3:any;
  tabFlag     :any='0';
  today       :Date = new Date();
  alphabetPattern = /^[a-zA-Z]+$/;
  actionFlag :any;
  passedData :any;
  fileSize:any;
 
  dataSource = [
    {title: 'DM18139497347437',category: 'category Name', startDate: '12/2/2020', endDate: '16/2/2020', status: 'Inactive'},
    {title: 'DM18139497347437',category: 'category Name', startDate: '12/2/2020', endDate: '16/2/2020', status: 'Inactive'},
    {title: 'DM18139497347437',category: 'category Name', startDate: '12/2/2020', endDate: '16/2/2020', status: 'Inactive'},
    {title: 'DM18139497347437',category: 'category Name', startDate: '12/2/2020', endDate: '16/2/2020', status: 'Inactive'},
    {title: 'DM18139497347437',category: 'category Name', startDate: '12/2/2020', endDate: '16/2/2020', status: 'Inactive'},
    {title: 'DM18139497347437',category: 'category Name', startDate: '12/2/2020', endDate: '16/2/2020', status: 'Inactive'},
   ];
   displayedColumns: string[] =['title','category','startDate','endDate','status'];
   displayedHistoryColumns: string[] =['title','NewUpdatesLink','WhatsNewLink','startDate','endDate','eventyear','eventlocation','status'];
  multiimg: boolean;
   sequenceList :any=[];
   data:any;
   historyFlag:any;

   config = {
    uiColor: '#ffffff',
    toolbarGroups: [
    // { name: 'clipboard', groups: ['clipboard', 'undo'] },
    // { name: 'editing', groups: ['find', 'selection', 'spellchecker'] },
    // { name: 'links' },
    // { name: 'insert' },
    // { name: 'document', groups: ['mode', 'document', 'doctools'] },
    { name: 'basicstyles', groups: ['basicstyles'] },
    { name: 'paragraph', groups: ['list', 'align'] },
    { name: 'styles' },
    { name: 'colors' }],
    skin: 'kama',
    // { name: 'basicstyles', groups: ['basicstyles'] },
    // { name: 'paragraph', groups: ['list', 'align'] },
    // { name: 'colors' }],

    resize_enabled: true,
    removePlugins: 'elementspath,save,magicline',
    extraPlugins: 'divarea,smiley,justify,indentblock,colordialog',
    colorButton_foreStyle: {
       element: 'font',
       attributes: { 'color': '#(color)' }
    },
    height: 30,
    removeDialogTabs: 'image:advanced;link:advanced',
    removeButtons: 'Subscript,Superscript,Anchor,Source,Table',
    format_tags: 'p;h1;h2;h3;pre;div'
 }
 
  constructor(public cs:MySuggestionService,private toastr: ToastrService,private modalService: NgbModal,private _formBuilder: FormBuilder,private adminDashboard:AdminDashboardService,private  router:Router,public commonService: CommonUtilityService,private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer) { 
    this.commonService.changeIsAuthenticate(true);
    this.fileSize = this.commonService.fileSize;
    this.initNewupdatesForm();
    this.initWhatsNewForm();
    this.initAwardForm();
    this.initmarqueeForm();
    this.initMaintainanceForm();
    this.getContentList("4","0"); 
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    matIconRegistry.addSvgIcon('dragIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/moveicon.svg'));
   }
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      console.log("---",event.container.data);
      this.data = event.container.data;
      this.sequenceList =[];
      for(let i= 0;i<this.data.length;i++){
        // let add={
        //   CMSID :this.data[i].CMSID
        // }
        this.sequenceList.push(this.data[i].CMSID);
      }
      console.log(this.sequenceList);
      let add={
        CMSID :this.sequenceList,
        "Seq":0
      }
      // {"CMSID":[493,484,510,509,513],"Seq":0}
      this.adminDashboard.addSequence(add).subscribe((resp:any) => {
        console.log("addSequence here-->",resp);
        this.toastr.success('Success');       
        
        }, (err) => {
         this.toastr.error('Error');
        })
      
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
                        console.log(event.container.data);
                        
    }
   }
   username:any;
  ngOnInit() {
    this.username = localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ==");
    this.username = CryptoJS.AES.decrypt(this.username, "").toString(
      CryptoJS.enc.Utf8
    );
   }
   onPaste(e){
     console.log("----paste--",e);
     
   }
   onDrop(e){
     console.log("--------drop",e);
     
   }
  open(content) {
    this.actionFlag = 'add';
    this.modalService.open(content, {  size: 'xl' as 'lg',centered:true, windowClass : "formModal"});
   }
   filterMaintainanceTo(toDate) {
     this.toDate = toDate;
     this.viewHistory({ pageIndex: 0,
      pageSize: 10,
      length: this.pageSize,
    },'');
   }
   filterMaintainanceFrom(fromDate) {
    this.fromDate = fromDate;
    this.viewHistory({ pageIndex: 0,
     pageSize: 10,
     length: this.pageSize,
   },'');
  }
   openAndDownload(content,category) {
    this.historyFlag = category;
    this.viewHistory({ pageIndex: 0,
      pageSize: 10,
      length: this.pageSize,
    },category);
    this.modalService.open(content, {  size: 'xl' as 'lg',centered:true, windowClass : "formModal"});
   }

   historyDataSource1:any=[];
   viewHistory(event,flag) {
     if(flag == 'messageBoard'){
     let reqjson = {
      PageNo : event.pageIndex + 1
     }
     
     this.adminDashboard.viewHistoryData(reqjson).subscribe((resp:any) => {
     
      this.historyDataSource1 = resp.ResponseData;
      if(flag =='Close'){
          this.closeModal();   
      }
     
    }, (err) => {
      this.toastr.error('Failed');
    })
  }
  else {
      let sfromDate: any='';
      let stoDate: any='';
      if (this.fromDate !== undefined && this.fromDate !== null) {  
      sfromDate = moment(this.fromDate).format('DD-MM-YYYY');
        console.dir("from date filter--"+sfromDate+"_--"+this.fromDate);
      }
      if (this.toDate !== undefined && this.toDate !== null) {  
        stoDate = moment(this.toDate).format('DD-MM-YYYY');
        console.dir("from date filter--"+stoDate+"_--"+this.toDate);     
      }
    let reqjson ={
      PageNo   : event.pageIndex + 1,
      FromDate : sfromDate,
      ToDate   : stoDate,
      Type     : 1
    }
     this.adminDashboard.getMaintainanceList(reqjson).subscribe((resp:any) => {
      this.historyDataSource1 = resp.ResponseData;
      this.pageSize = resp.TotalCount;
     }, (err) => {
   
     })
  }
   }
   update(content,data){
    this.actionFlag = 'update';
    this.passedData = data;
    console.log("data",data);
    if(this.tabFlag == '0'){
      this.marqueeForm.controls['marqueeContent'].setValue(data.ContentName);
      this.marqueeForm.controls['fromDate'].setValue(data.StartDate);
      this.marqueeForm.controls['toDate'].setValue(data.EndDate);
    }
    else if(this.tabFlag == '1'){
    this.NewUpdatesForm.controls['title'].setValue(data.ContentName);
    this.NewUpdatesForm.controls['link'].setValue(data._Get_Img_Video[0].ImagePath);
    this.NewUpdatesForm.controls['fromDate'].setValue(data.StartDate);
    this.NewUpdatesForm.controls['toDate'].setValue(data.EndDate);
    }
    else if(this.tabFlag == '2'){
      this.whatsNewForm.controls['title'].setValue(data.ContentName);
      this.whatsNewForm.controls['content'].setValue(data.SubContentName);
      this.whatsNewForm.controls['fromDate'].setValue(data.StartDate);
      this.whatsNewForm.controls['toDate'].setValue(data.EndDate);
      this.whatsNewForm.controls['img'].setValue(data._Get_Img_Video[0].ImagePath);
      if(data._Get_Img_Video[1])
      this.whatsNewForm.controls['link'].setValue(data._Get_Img_Video[1].ImagePath);
      else
      this.whatsNewForm.controls['link'].setValue('');
      }
    else if(this.tabFlag == '4'){
      this.MaintainanceForm.controls['content'].setValue(data.Message);
      this.MaintainanceForm.controls['fromDate'].setValue(data.FromDate);
      this.MaintainanceForm.controls['toDate'].setValue(data.ToDate);
    }

    this.modalService.open(content, {  size: 'xl' as 'lg',centered:true, windowClass : "formModal"});
    
  }
  closeModal(){
     this.formNewUpdate.resetForm();
     this.form.resetForm();
     this.formWhatsNew.resetForm();
     this.formAward.resetForm();
     this.MaintainanceForm.reset();
     this.modalService.dismissAll();
   }
  initmarqueeForm(){
    
    this.marqueeForm = this._formBuilder.group({
      marqueeContent :['',[Validators.required,Validators.maxLength(500)]],
      fromDate       :['',[Validators.required]],
      toDate        :['',[Validators.required]]
    })
   }
   initMaintainanceForm(){
    this.MaintainanceForm = this._formBuilder.group({
      content       :['',[Validators.required,Validators.maxLength(500)]],
      fromDate      :['',[Validators.required]],
      toDate        :['',[Validators.required]]
    })
   }
  initNewupdatesForm(){
    this.NewUpdatesForm = this._formBuilder.group({
        title   : ['', [Validators.required]],
        fromDate: ['', [Validators.required]],
        toDate  : ['',[Validators.required]],
        link    : ['', [Validators.required]],
      });
   }
  initWhatsNewForm(){
    this.whatsNewForm = this._formBuilder.group({
        title   : ['', [Validators.required]],
        content : [''],
        fromDate: ['', [Validators.required]],
        toDate  : ['',[Validators.required]],
        link    : [''],
        img     : ['',[Validators.required]]
      });
   }
  initAwardForm(){
    this.awardForm = this._formBuilder.group({
      
        title          : ['', [Validators.required]],
        content        : ['',[Validators.required]], 
        eventYear      : ['',[Validators.required]],
        // eventYear      : ['',[Validators.required,Validators.pattern("^[0-9]*$"),Validators.maxLength(4),Validators.minLength(4)]],
        // [Validators.pattern("^\d{4}$")]],
        eventLocation  : ['',[Validators.required,Validators.pattern(this.alphabetPattern)]],
        file           : ['',Validators.required]
      });
      this.callYears();
   }
   callYears(){
    var year = new Date().getFullYear();
     for(var i= year; i>= 1980; i--){
      this.years= i;
      this.noOfYears.push(this.years);
     }

   }
  validateAllFormFields(formGroup){
      Object.keys(formGroup.controls).forEach(controlName =>{
        const control = formGroup.get(controlName);
        if(control instanceof FormControl){
          control.markAsTouched({onlySelf: true});
        }
        else if(control instanceof FormControl || control instanceof FormArray){
        this.validateAllFormFields(control);
        }
      });
   } 
  numberOnly(event): boolean {
          const charCode = (event.which) ? event.which : event.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
          }
          return true;
      
   }
  lettersOnly(event){
          const charCode = (event.which) ? event.which : event.keyCode;
           if(!(charCode >= 65 && charCode <=90) && !(charCode >=97&& charCode <=122) && (charCode != 32 && charCode != 0 ) ) { 
              event.preventDefault(); 
        }
   } 
   Json:any=[];
downloadExcel() {
  let reqjson = {
    PageNo : 0 
   }
   
   this.adminDashboard.viewHistoryData(reqjson).subscribe((resp:any) => {
   
    // this.historyDataSource1 = resp.ResponseData;
    this.Json = resp.ResponseData;
    let dataNew = this.Json;
    console.log(dataNew);
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("ReportData");
    let columns = Object.keys(this.Json[0]);
    let headerRow = worksheet.addRow(columns);
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "00FF0000" },
        bgColor: { argb: "00FF0000" },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
    });
    dataNew.forEach((d) => {
      // var obj = JSON.parse(d);
      var values = Object.keys(d).map(function (key) {
        return d[key];
      });
      let row = worksheet.addRow(values);
    });
    workbook.xlsx.writeBuffer().then((dataNew) => {
      let blob = new Blob([dataNew], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(
        blob,
        "MessageBoardData" +".xlsx"
      );
    });
  });
}

  addMarquee(flag){
   // alert(marquee);
 
   if(this.marqueeForm.valid){
    let startDate = moment(this.marqueeForm.get('fromDate').value).format('YYYY-MM-DD');
    let endDate   = moment(this.marqueeForm.get('toDate').value).format('YYYY-MM-DD');
    let data= {
    //   "testParm":"Testttttt"
      "CMSId":this.actionFlag == 'add' ? 0 : this.passedData.CMSID,
      "ContentCategoryId":4,
      "ContentName":  this.marqueeForm.get('marqueeContent').value,
      "SubContentName": "",
      "Link": "",  
      "StatementType":this.actionFlag == 'add' ? "INSERT" : "UPDATE",  // "Update" // "DELETE"
      "CreatedBy":  "SupplierEMPID",
      "StartDate": startDate,
      "EndDate":endDate,
     
}
let formData:FormData = new FormData();
formData.append('reqjson',JSON.stringify(data));
console.log("--------",formData);

this.adminDashboard.addMarquee(formData).subscribe((resp:any) => {
  console.log("getNewUpdates here-->",resp);
  this.marqueeContent ='';
 // this.toastr.success('----');
  this.toastr.success('Success');
  this.getContentList("4","0");
  this.form.resetForm();
  if(flag =='Close'){
      this.closeModal();   
  }
 
}, (err) => {
  this.toastr.error('Failed');
})
   }
   else{
     this.validateAllFormFields(this.marqueeForm);
   }
   }
  addNewUpdates(flag){
    if(this.NewUpdatesForm.valid && this.cs.isUndefinedORNull(this.NewUpdatesForm.controls.title.value.trim())){
      this.cs.showError("Title can not be blank");
      return;
    }
    if(this.NewUpdatesForm.valid && this.cs.isUndefinedORNull(this.NewUpdatesForm.controls.link.value.trim())){
      this.cs.showError("Link can not be blank");
      return;
    }
  if(this.NewUpdatesForm.valid){
    let startDate = moment(this.NewUpdatesForm.get('fromDate').value).format('YYYY-MM-DD');
    let endDate   = moment(this.NewUpdatesForm.get('toDate').value).format('YYYY-MM-DD');
    let formData: FormData = new FormData();
    let reqjson= {
       "CMSId":this.actionFlag == 'add' ? 0 : this.passedData.CMSID,
       "ContentCategoryId":1,
       "ContentName": this.NewUpdatesForm.get('title').value,
       "SubContentName": "",
       "Link": "",  
       "StatementType":this.actionFlag == 'add' ? "INSERT" : "UPDATE",  // "Update" // "DELETE"
       "CreatedBy":  "SupplierEMPID",
       "StartDate": startDate,
       "EndDate":endDate,
       "Event_Year":"",
       "Event_Location":"" 
   
   }
   formData.append('reqjson', JSON.stringify(reqjson));
   for (var i = 0; i < this.pdffileUrls.length; i++) {
    // formData.append('IMG', this.fileUrls[i]);
    formData.append('PDF',this.pdffileUrls[i]);
  }
   this.adminDashboard.addNewUpdates(formData).subscribe((resp:any) => {
   console.log("getNewUpdates here-->",resp);
  
   this.toastr.success('Success');
   this.getContentList("1","1");
   this.formNewUpdate.resetForm();
   if(flag=='Close'){
     this.closeModal();
   }
   
   }, (err) => {
    this.toastr.error('Error');
   })
  }
  else{
     this.validateAllFormFields(this.NewUpdatesForm);
   }
 
   }
  addWhatsNew(flag){
    if (this.whatsNewForm.valid && this.cs.isUndefinedORNull(this.whatsNewForm.controls.title.value.trim())) {
      this.cs.showError("Title can not be blank");
      return;
    }
    // if (this.whatsNewForm.valid && this.cs.isUndefinedORNull(this.whatsNewForm.controls.link.value.trim())) {
    //   this.cs.showError("Link can not be blank");
    //   return;
    // }  
  if(this.whatsNewForm.valid){
    let startDate = moment(this.whatsNewForm.get('fromDate').value).format('YYYY-MM-DD');
    let endDate   = moment(this.whatsNewForm.get('toDate').value).format('YYYY-MM-DD');
    let formData: FormData = new FormData;   
    let reqjson= {
      "CMSId":this.actionFlag == 'add' ? 0 : this.passedData.CMSID,
      "ContentCategoryId":2,
      "ContentName": this.whatsNewForm.get('title').value.slice(0 , this.whatsNewForm.get('title').value.length - 1),
      "SubContentName": this.whatsNewForm.get('content').value.slice(0 , this.whatsNewForm.get('content').value.length - 1) || '',
      "Link":"",  
      "StatementType":this.actionFlag == 'add' ? "INSERT" : "UPDATE",  // "Update" // "DELETE"
      "CreatedBy":  "SupplierEMPID",
      "StartDate":startDate,
      "EndDate":endDate,
      "Event_Year":"",
      "Event_Location":"" 
    }
  formData.append('reqjson', JSON.stringify(reqjson));
  for (var i = 0; i < this.fileUrls.length; i++) {
    formData.append('IMG', this.fileUrls[i]);
  }
  for (var i = 0; i < this.pdffileUrls.length; i++) {
    formData.append(`PDF${i}`,this.pdffileUrls[i]);
  }
  console.log(reqjson);
  console.log(formData,"..formData..");
  this.adminDashboard.addWhatsNew(formData).subscribe((resp:any) => {
  console.log("getNewUpdates here-->",resp);
  this.toastr.success('Success');
  this.getContentList("2","2");
  this.formWhatsNew.resetForm();
  // this.whatsNewForm .clearValidators();
  if(flag =='Close'){
    this.closeModal();
  }
 
  
  }, (err) => {
  
  })
  } else{
     this.validateAllFormFields(this.whatsNewForm);
   }
  
   }
  addAward(flag){
  if(this.awardForm.valid){
    
    let formData: FormData = new FormData;   
   // let startDate = moment(this.awardForm.get('fromDate').value).format('YYYY-MM-DD');
   // let endDate   = moment(this.awardForm.get('toDate').value).format('YYYY-MM-DD');
    // this.loginFormGroup.get('userName').value
    let reqjson= {
      "CMSId":0,
      "ContentCategoryId":3,
      "ContentName": this.awardForm.get('title').value,
      "SubContentName": this.awardForm.get('content').value,
      "Link": "",  
      "StatementType":"INSERT",  // "Update" // "DELETE"
      "CreatedBy":  "SupplierEMPID",
      "StartDate":"",
      "EndDate":"",
     // "EventYear": this.awardForm.get('eventYear').value,
      //"EventLocation": this.awardForm.get('eventLocation').value,
       "Event_Year": this.awardForm.get('eventYear').value,
      "Event_Location": this.awardForm.get('eventLocation').value, 
     
      // eventYear      : ['', [Validators.required]],
      // eventLocation  : ['',[Validators.required]],
     
  }
  console.log(reqjson);
  formData.append('reqjson', JSON.stringify(reqjson));
  var fileLength= this.fileAwardUrls[0].length;
  for (var i = 0; i < fileLength; i++) {
   // this.totalArray.push(this.fileUrls[0][i])
   console.log(this.fileAwardUrls[0][i]);
    formData.append('IMG',this.fileAwardUrls[0][i]);
  }
  // console.log(formData);
  // this.acc.push(this.totalArray);
  // console.log(this.acc);
  // formData.append('IMG',this.acc[0]);
  this.adminDashboard.addAwards(formData).subscribe((resp:any) => {
  console.log("addAwards here-->",resp);
  this.getContentList("3","3");
  //console.log(this.fileAwardUrls.length);
  this.toastr.success('Success');
 // this.fileAwardUrls.length= 0;
  this.formAward.resetForm();
  if(flag =='Close'){
    this.closeModal();
  }
  this.totalArray=[];
  }, (err) => {
  alert("failed");
  })
  } else{
     this.validateAllFormFields(this.awardForm);
   }
  
   }
  //-----------------------------upload whatsnew file-----------------------------------------------------------
  detectFiles(event) {
 
    console.log(event);
    let files = event.target.files; 
    if (files) {
      for (let file of files) {
        console.log(file);
        this.fileName = file.name;
      
        let ext=file.name.split(".").pop().toLowerCase();
        console.log(ext);
        let reader = new FileReader();
        reader.onload = (e: any) => {
         
                if(ext =='jpg'|| ext=='jpeg' || ext=='png'){
                  this.fileUrls=[];
                  this.fileUrls.push(file);
                  this.urls.push({url:e.target.result,ext:ext});  
                  if(this.tabFlag == '2'){
                     
                      this.whatsNewForm.controls['img'].setValue(this.fileName);
                  }    
                  else if(this.tabFlag == '3'){
                    
                    this.awardForm.controls['file'].setValue(this.fileName);
                  }  
                }
                else{
                  alert("please select proper format");
                //  this.utilityProvider.presentToast('Please select file in proper format','4000','top')
                }

                if(this.tabFlag == '1'){   
                  this.fileUrls=[];
                  this.fileUrls.push(file);
                  this.urls.push({url:e.target.result,ext:ext});               
                  this.NewUpdatesForm.controls['link'].setValue(this.fileName);  
                }
        }
        reader.readAsDataURL(file);
      }
    }
  }
  detectFilespdf(events){
    console.log(events);
    this.pdfFileName = '';
    this.pdffileUrls = [];
    let pdffiles = events.target.files;
    if (pdffiles) {
      for (let file of pdffiles) {
        console.log(file);
        this.pdfFileName += file.name + ",";
        let ext=file.name.split(".").pop().toLowerCase();
        console.log(ext);
        let reader = new FileReader();
        reader.onload = (e: any) => {
                // if(ext =='pdf'){
                    // this.pdffileUrls = [];
                    this.pdffileUrls.push(file);
                    this.urls.push({url:e.target.result,ext:ext}); 
                    
                   if(this.tabFlag =='2'){
                      this.whatsNewForm.controls['link'].setValue(this.pdfFileName);
                   }  
                   else if(this.tabFlag == '1'){
                    this.NewUpdatesForm.controls['link'].setValue(this.pdfFileName);
                   } 
        
                // }
                // else{
                //   alert("please select proper format");
               
                // }
        }
        reader.readAsDataURL(file);
      }
    }
  }
  detectAwardFiles(event) {
 
  console.log(event);
  var files = event.target.files; 
  if (event.target.files && event.target.files[0]) {
    var filesAmount = event.target.files.length;
    this.filesName=[];
      for (let i = 0; i < filesAmount; i++) {
        this.fileName[i]=event.target.files[i].name;
        this.ext[i] = this.fileName[i].split(".").pop().toLowerCase();
      let reader = new FileReader();
      reader.onload = (e: any) => {
              if(this.ext[i] =='jpg'|| this.ext[i]=='jpeg' || this.ext[i]=='png'){
                if(event.target.files[i].size <= this.fileSize){
                    this.fileAwardUrls=[];
                    this.fileAwardUrls.push(event.target.files);
                    this.urls.push({url:e.target.result,ext:this.ext[i]});  
                    // if(this.tabFlag == '2'){   
                    //     this.whatsNewForm.controls['img'].setValue(this.fileName[i]);
                    // }    
                    // else if(this.tabFlag == '3'){
                      console.log(this.fileName[i]);
                      this.filesName.push(this.fileName[i]);
                      this.awardForm.controls['file'].setValue(this.filesName);
                  //  } 
              }
              else{
                alert("Cannot exceed file size more than 20MB");
                if(this.filesName.length>0){
                  this.filesName.length=0;
                }
                this.awardForm.controls['file'].setValue('');
              } 
              }
              else{
                alert("please select proper format");
              //  this.utilityProvider.presentToast('Please select file in proper format','4000','top')
              }
      }
      reader.readAsDataURL(event.target.files[i]);
    }
   // }
  }
   }
  //------------------------------get Content list by ID -------------------------------------------------------
  getContentList(id,tabID){
          let postData= {
            "PageNo":1,
            "ContentCategoryID":id,  
            "Mode":"CMS"
            }
        this.adminDashboard.getAdminDashboardCMS(postData).subscribe((resp:any) => {
        console.log("getContentList here-->",resp);
       
          if(tabID == 0){
            this.dragRowData= resp.ResponseData;
            //alert("calling data")
            for(var i=0;i<resp.ResponseData.length;i++) 
            {
           //   this.dragRowData= resp.ResponseData
            // this.dragRowData = [
            //   { description: resp.ResponseData[i].ContentName, startDate: resp.ResponseData[i].StartDate, endDate:resp.ResponseData[i].EndDate},
            //  ];
            }
            console.log("--->"+JSON.stringify(this.dragRowData));
            
          }
          else if(tabID == 1){
          //  alert("calling data1")
          this.dragRowData1= resp.ResponseData;
          //   for(var i=0;i<resp.ResponseData.length;i++)
          //   {
          //   this.dragRowData1 = [
          //     { description: resp.ResponseData[i].ContentName, startDate: resp.ResponseData[i].StartDate, endDate:resp.ResponseData[i].EndDate},
          //    ];
          // }
        }
        else if(tabID == 2){
        //  alert("calling data2")
        this.dragRowData2 = resp.ResponseData;
        //   for(var i=0;i<resp.ResponseData.length;i++)
        //   {
        //   this.dragRowData2 = [
        //     { description: resp.ResponseData[i].ContentName, startDate: resp.ResponseData[i].StartDate, endDate:resp.ResponseData[i].EndDate},
        //    ];
        // }
      }
      else if(tabID == 3){
        //alert("calling data3")
        this.dragRowData3 = resp.ResponseData;
        
        // console.log(this.dragRowData3);
       
        // for(let i=0;i<this.dragRowData3.length;i++){
        //   this.getMultiImages[i] = this.dragRowData3[i]._Get_Img_Video;
        //   console.log(this.getMultiImages[i]);
        // if(this.getMultiImages[i].length>1){
        //   this.multiimg = true;
        // }
        // else{
        //   this.multiimg = false;
        // }
        // }
        
      //   for(var i=0;i<resp.ResponseData.length;i++)
      //   {
      //   this.dragRowData3 = [
      //     { description: resp.ResponseData[i].ContentName, startDate: resp.ResponseData[i].StartDate, endDate:resp.ResponseData[i].EndDate},
      //    ];
      // }
    }
        
     
        }, (err) => {

})
  }
  //------------------------------------------------------------------------------------------------------------
  tabClick(event){
  //alert(event.index);
  if(event.index == 0){
      this.tabFlag = '0';
      this.NewUpdatesForm.reset();
      this.whatsNewForm.reset();
      this.awardForm.reset();
      this.getContentList("4","0");
  }
  else if(event.index == 1){
    this.tabFlag = '1';
    this.whatsNewForm.reset();
    this.awardForm.reset();
    this.getContentList("1","1");
  }
  else if(event.index == 2){
    this.tabFlag = '2';
      this.NewUpdatesForm.reset();
      this.awardForm.reset();
      this.getContentList("2","2");
  }
  else if(event.index == 3){
    this.tabFlag = '3';
    this.NewUpdatesForm.reset();
    this.whatsNewForm.reset();
    this.getContentList("3","3");
  }
  if(event.index == 4){
    this.tabFlag = '4';
    this.NewUpdatesForm.reset();
    this.whatsNewForm.reset();
    this.awardForm.reset();
    this.marqueeForm.reset();
    this.getMaintainanceList();
}
  else if(event.index ='5'){
    this.categoryID = '1234';
    this.getHistoryData({ pageIndex: 0, pageSize: 10, length: this.pageSize });
  }
  }
  //------------------------------end of tab click----------------------------------------------------------------
  //------------------------------getMaintainance List method-----------------------------------------------------
  getMaintainanceList(){
    let reqjson ={
      PageNo : 1,
      FromDate : "",
      ToDate: "",
      Type : 0
    }
     this.adminDashboard.getMaintainanceList(reqjson).subscribe((resp:any) => {
     this.dragRowData = resp.ResponseData;
    //  this.pageSize = resp.ResponseData[0].Serial;

     }, (err) => {
   
     })
  }
  addMaintainanceText(flag){
    // alert(this.username)
    if(this.MaintainanceForm.valid){
      if(this.actionFlag == 'add'){
        let reqJson = {
          "Message"  : this.MaintainanceForm.get('content').value,
          "FromDate" : this.MaintainanceForm.get('fromDate').value,
          "ToDate"   : this.MaintainanceForm.get('toDate').value,
          "CreatedBy": this.username
        }
        this.adminDashboard.addMaintainanceToList(reqJson).subscribe((resp:any) => {
          this.MaintainanceForm.reset();
          // if(flag =='Close'){
              this.closeModal(); 
              this.getMaintainanceList();  
          // }
          }, (err) => {
        
          })
      }
    else {
      let reqJson = {
        "id"       : this.passedData.Id,
        "Message"  : this.MaintainanceForm.get('content').value,
        "FromDate" : this.MaintainanceForm.get('fromDate').value,
        "ToDate"   : this.MaintainanceForm.get('toDate').value,
        "CreatedBy": this.username
      }
      this.adminDashboard.updateMaintainance(reqJson).subscribe((resp:any) => {
        this.closeModal(); 
        this.getMaintainanceList();
        }, (err) => {
      
        })
    }
    }
    else{
       this.validateAllFormFields(this.MaintainanceForm);
    }
  }
  
  //------------------------------update inactive---------------------------------------------------------------
  updateInactive(id,CMSId){
 // alert(CMSId);
  let reqjson=

  { "CMSID": CMSId }
  this.adminDashboard.updateInactive(reqjson).subscribe((resp:any) => {
    console.log("getNewUpdates here-->",resp);
    if(id == '4'){
      this.getContentList("4","0");
    }
    else if(id=='1'){
     this.getContentList("1","1");
    }
    else if(id=='2'){
     this.getContentList("2","2");
    }
    else if(id=='3'){
      this.getContentList("3","3");
     }
     
  }, (err) => {
   
  })
  }
  //----------------------------end of inactive-----------------------------------------------------------------  
//-------------------history------------------------------------
getChangedData(){
  this.fromDate =null;
  this.toDate = null;
  this.getHistoryData({ pageIndex: 0, pageSize: 10, length: this.pageSize });
  }
  search(date){
    this.fromDate = date;
   this.getHistoryData({ pageIndex: 0, pageSize: 10, length: this.pageSize });
   }
 
  ResetDate(){
   this.fromDate =null;
   this.toDate = null;
   this.getHistoryData({ pageIndex: 0, pageSize: 10, length: this.pageSize });
  }
 
  search1(date){
   this.toDate = date;
   this.getHistoryData({ pageIndex: 0, pageSize: 10, length: this.pageSize });
  }
getHistoryData(event){
  this.pageNo = event.pageIndex;
 // alert(this.pageNo);
  let sfromDate: any='';
  let stoDate: any='';
  if (this.fromDate !== undefined && this.fromDate !== null) {  
   sfromDate = moment(this.fromDate).format('DD-MM-YYYY');
    console.dir("from date filter--"+sfromDate+"_--"+this.fromDate);
  }
  if (this.toDate !== undefined && this.toDate !== null) {  
    stoDate = moment(this.toDate).format('DD-MM-YYYY');
     console.dir("from date filter--"+stoDate+"_--"+this.toDate);     
   }

  let reqjson= {
   // PageNo: 1,
    PageNo: this.pageNo+1,
    ContentCategoryID: this.categoryID,
    Mode: "CMS",
    FromDate : sfromDate,
    toDate : stoDate
    }
  this.adminDashboard.getHistoryDataAPI(reqjson).subscribe((resp:any) => {
  this.historyDataSource = resp.ResponseData;
  this.pageSize = resp.ResponseData[0].Serial;
  //alert(this.pageSize);
  }, (err) => {

  })
}
checkError = (marqueeContent: string, errorName: string) => {
  return this.marqueeForm.controls[marqueeContent].hasError(errorName);
}

deleteMaintainance(data) {
  let reqjson={
    Id : data.Id,
    ModifiedBy : ""
  }
  this.adminDashboard.deleteMaintainance(reqjson).subscribe((resp:any) => {
    this.getMaintainanceList(); 
   this.cs.showSuccess("Deleted Successfully");
    }, (err) => {
  this.cs.showError(err);
    })
}
}
