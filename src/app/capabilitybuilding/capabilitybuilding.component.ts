import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { Router } from '@angular/router';
import * as CryptoJS from 'crypto-js';
import { CommonApiService } from '../services/common-api/common-api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';

@Component({
  selector: 'app-capabilitybuilding',
  templateUrl: './capabilitybuilding.component.html',
  styleUrls: ['./capabilitybuilding.component.scss']
})
export class CapabilitybuildingComponent implements OnInit {

  url: SafeResourceUrl;
  RoleID: any;
  form: FormGroup;
  showForm = true;
  supplierName = CryptoJS.AES.decrypt(localStorage.getItem('VlROV2QyTkhlSEJhV0VwUFdWY3hiQT09'),"").toString(CryptoJS.enc.Utf8)
  usertoken = CryptoJS.AES.decrypt(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ=='),"").toString(CryptoJS.enc.Utf8)
  vendorCode = CryptoJS.AES.decrypt(localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09'),"").toString(CryptoJS.enc.Utf8);
  name = CryptoJS.AES.decrypt(localStorage.getItem('WW0xR2RGcFJQVDA9'),"").toString(CryptoJS.enc.Utf8);

  constructor(private router:Router,private domSanitizer: DomSanitizer,public commonService: CommonUtilityService,
    private commonApi: CommonApiService, public fb: FormBuilder, private cs: MySuggestionService) { 
    this.commonService.changeIsAuthenticate(true);
    this.form = this.fb.group({
      name: ['', Validators.required],
      designation: ['', Validators.required],
      location: ['', Validators.required],
    })
  }

  ngOnInit() {
    this.RoleID = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID,"").toString(CryptoJS.enc.Utf8)
    this.url = 'http://172.32.1.199:8001/MSETU_Sustainability_Elearning/FirstModule.aspx?supCode='+this.vendorCode+'&ParticipantName='+this.name;
  }
  
  submit(){
    if(this.form.valid){
      // this.showForm = false;;
      window.open('http://172.32.1.199:8001/MSETU_Sustainability_Elearning/FirstModule.aspx?supCode='+this.vendorCode+"&SupplierName="+this.supplierName
      +'&ParticipantName='+this.form.value.name+'&Designation='+this.form.value.designation+'&Location='+this.form.value.location
      +'&Token='+this.usertoken, '_blank')
    }else{
      this.cs.showError('Kindly Fill All Fields')
    }
  }

//   get(url: string): Observable<any> {
//     let options = new RequestOption();
//     options.headers = new Headers();
//     options.headers.append('AUTH-TOKEN', 'SomeToken123');
//     options.responseType = ResponseContentType.Blob;

//     return new Observable((observer: Subscriber<any>) => {
//         let objectUrl: string = null;

//         this.http
//             .get(url, options)
//             .subscribe(m => {
//                 objectUrl = URL.createObjectURL(m.blob());
//                 observer.next(objectUrl);
//             });

//         return () => {
//             if (objectUrl) {
//                 URL.revokeObjectURL(objectUrl);
//                 objectUrl = null;
//             }
//         };
//     });
// }
// }

  navigate(){
    if(this.RoleID == 7 ||  this.RoleID == '7'){
      this.router.navigateByUrl('adminDashboard');
    }else{
      this.router.navigateByUrl('dashboard');
    }
  }
  

}
