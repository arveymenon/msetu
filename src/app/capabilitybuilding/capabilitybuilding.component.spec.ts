import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapabilitybuildingComponent } from './capabilitybuilding.component';

describe('CapabilitybuildingComponent', () => {
  let component: CapabilitybuildingComponent;
  let fixture: ComponentFixture<CapabilitybuildingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapabilitybuildingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapabilitybuildingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
