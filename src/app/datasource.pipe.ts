import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'datasource'
})
export class DatasourcePipe implements PipeTransform {

  DatasourceResponseColumns:any;


  transform(value: any,index:any): any {
    this.DatasourceResponseColumns = localStorage.getItem('DatasourceResponseColumns')
    this.DatasourceResponseColumns = this.DatasourceResponseColumns.split(',')
    return this.DatasourceResponseColumns[index];
  }

}
