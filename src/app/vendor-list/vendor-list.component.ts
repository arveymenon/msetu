import { Component, OnInit } from '@angular/core';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import * as CryptoJS from "crypto-js";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-vendor-list',
  templateUrl: './vendor-list.component.html',
  styleUrls: ['./vendor-list.component.sass']
})
export class VendorListComponent implements OnInit {
  displayedColumns3: string[] = [
    "EmployeeName",
    "VendorCode",
    "Role",
    "UserName",
    "PhoneNumber",
    "Email",
    "SupplierName",
    "primarRole",
    "commodity",
    "location",
    "modifiedBy",
    "modifiedDate",
    "action",
  ];
  filterPageNo: any = 1;
  editEmpFormdata: FormGroup;

  dataSource:any=[];
  vendorCode:any;
  pageSize:any;
  RoleID:any;
  constructor( public cs: MySuggestionService, public formBuilder: FormBuilder,private domSanitizer: DomSanitizer, 
    public modal: NgbModal, private matIconRegistry: MatIconRegistry,) {
      matIconRegistry.addSvgIcon(
        "edit",
        domSanitizer.bypassSecurityTrustResourceUrl(
          "../assets/Icons/innerPages/Edit.svg"
        )
      );
     }

  ngOnInit() {
    this.editEmpForm();
    this.vendorCode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
    this.vendorCode = CryptoJS.AES.decrypt(this.vendorCode, "").toString(CryptoJS.enc.Utf8);
    this.RoleID = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(CryptoJS.enc.Utf8);
    this.getVendorList({pageIndex: 1,pageSize: 10,length: this.pageSize});
  }
  async getVendorList(ev){
    //  let body = {
    //   "VendorCode":this.vendorCode,
    //   "PageNo":ev.pageIndex,
    //   "RoleId":this.RoleID,
    //   "filterText":""
    //   }
    let body = {
      "PageNo":0,
      "VendorCode":"DM181",
      "RoleId":0,
      "filterText":""
       
      }
  await this.cs
       .postMySuggestionData("Vendor/GetSupplierList", body)
       .subscribe(async (response) => {
         console.log(response);
       
         this.dataSource = [...response.ResponseData];
         this.pageSize = response.TotalCount;
       });
  }
  
  getPageChangeData(ev) {
  
      this.filterPageNo = ev.pageIndex + 1;
      this.getVendorList(
        ev.pageIndex + 1
      );
    
  }
  editEmpForm() {
    this.editEmpFormdata = this.formBuilder.group({
      // UserId: [0],
      // SupplierName: ["", Validators.required],
      FirstName: ["", Validators.required],
      LastName: ["", Validators.required],
      // Role: ["", Validators.required],
      // PrimaryRole: ["", Validators.required],
      PhoneNumber: ["", Validators.required],
      Email: ["",  Validators.required],
      // Commodity: [""],
      // Location: [""],
      SupplierName: ["", Validators.required]
    });
  }
  passedData:any;
  openModal(content, id?: any, userName?: any) {
    this.editEmpFormdata.reset();
    let data = userName;
    this.passedData = userName;
    console.log(data);
    
    //  this.updateEmployeeUserName = userName;
    this.editEmpFormdata.controls["FirstName"].setValue(data.FirstName);
    this.editEmpFormdata.controls["LastName"].setValue(data.LastName);
    this.editEmpFormdata.controls["PhoneNumber"].setValue(data.PhoneNumber);
    this.editEmpFormdata.controls["Email"].setValue(data.Email);
    this.editEmpFormdata.controls["SupplierName"].setValue(data.SupplierName);
    this.modal.open(content, {
      size: "lg",
      centered: true,
      windowClass: "formModal",
    });
    // this.getVendorUser(id);
  }
  closeModal() {
    this.modal.dismissAll();
  }
  updateDetails() {
    if (this.editEmpFormdata.valid) {
      let formData: FormData = new FormData();
      let postData = {
        ID : this.passedData.ID,
        VendorCode : this.passedData.VendorCode,
        // userNAme: this.updateEmployeeUserName,
        // roleId: this.editEmpFormdata.controls.Role.value.join(),
        firstName: this.editEmpFormdata.controls.FirstName.value,
        lastName: this.editEmpFormdata.controls.LastName.value,
        // supplierName: this.editEmpFormdata.controls.SupplierName.value,
        // userID: this.editEmpFormdata.controls.UserId.value,
        // updatedBy: "system",
        // primaryRole: this.editEmpFormdata.controls.PrimaryRole.value,
        // phoneNo: this.editEmpFormdata.controls.PhoneNumber.value,
        Email: this.editEmpFormdata.controls.Email.value,
        PhoneNumber : this.editEmpFormdata.controls.PhoneNumber.value,
        SupplierName: this.editEmpFormdata.controls.SupplierName.value,
        ModifiedBy : this.vendorCode
        // officeLocation: this.editEmpFormdata.controls.Location.value,
        // commodity: this.editEmpFormdata.controls.Commodity.value,
        // MasterVendor: this.editEmpFormdata.controls.masterVendor.value
      };
      this.cs
        .postMySuggestionData("Vendor/UpdateSupplierDetails", postData)
        .subscribe(
          (res: any) => {
            // this.updatedVendorUser = res.ResponseData;
            this.getVendorList({pageIndex: 1,pageSize: 10,length: this.pageSize});
            console.log("--")
            this.cs.showSuccess("Updated Successfully");
            this.editEmpFormdata.reset();
            // this.DDLValue = ''
            // this.reqRoleId = ''
            this.closeModal();
          },
          (error) => {
            this.cs.showError(error.message);
            console.log(error.message);
          }
        );
    } else {
      this.cs.showError("Please fill all mandatory fields");
      return;
    }
  }
}
