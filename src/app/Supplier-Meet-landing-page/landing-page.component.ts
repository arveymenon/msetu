import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import {
  NgbCarouselConfig,
  NgbModal,
  NgbCarousel,
} from "@ng-bootstrap/ng-bootstrap";
import { AnimationService } from "../Supplier-Meet-common/animation.service";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import * as CryptoJS from "crypto-js";
import { FormControl, NgModel } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { CommonApiService } from '../services/common-api/common-api.service';
declare var particlesJS: any;
@Component({
  selector: "app-landing-page",
  templateUrl: "./landing-page.component.html",
  styleUrls: ["./landing-page.component.scss"],
})
export class LandingPageComponent implements OnInit {
  current_year: any;
  Year = new Date()
  fullyear = this.Year.getFullYear();
  back_grd_imgs = [];
  @ViewChild("carousel") carousel: NgbCarousel;
  @ViewChild("loginCarousel") loginCarousel: NgbCarousel;
  supplier_location: any;
  supplier_Year: any;
  RoleID: string;
  temp_route = "";
  vendorCode: NgModel;
  constructor(
    private commonAPIService: CommonApiService,
    private http: HttpClient,
    private modalService: NgbModal,
    public cs: MySuggestionService,
    private animSRVC: AnimationService,
    private router: Router,
    private ro: Router,
    public commonService: CommonUtilityService,
    private toast: ToastrService
  ) {
    this.commonService.changeIsAuthenticate(false);
  }
  URL: any;

  ngOnInit() {
    this.RoleID = localStorage.getItem("WTIwNWMxcFZiR3M9");
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(
      CryptoJS.enc.Utf8
    );
    particlesJS.load("particles-js", "assets/js/particles.json", function () {
      console.log("callback - particles.js config loaded");
    });
    this.getCurrentYear();
    this.GetActiveSMDetails();
    setInterval(() => {
      this.loginCarousel.next();
    }, 6000);
  }
  ngAfterViewInit() {
    this.URL = this.router.url;
  }
  GetActiveSMDetails() {
    this.cs
      .supplierMeetPost("SupplierMeetAdmin/GetActiveSMDetails", "")
      .subscribe((data: any) => {
        if (data.Message == "Success") {
          localStorage.setItem(
            "SupplierMeetLocation",
            data.ResponseData[0].Location
          );
          localStorage.setItem("SupplierMeetYear", data.ResponseData[0].Year);
          this.supplier_location = data.ResponseData[0].Location;
          this.supplier_Year = data.ResponseData[0].Year;
        } else {
          this.cs.showError("Error while fetching location details");
        }
      });
  }

  navigate() {
    if (this.RoleID != "3") this.ro.navigateByUrl("dashboard");
    else this.ro.navigateByUrl("adminDashboard");
  }

  gotoRoute(rou, modal) {
    this.temp_route = rou;
    if (this.RoleID) {
      this.redirect(rou);
    } else {
      console.log("Display Modal");
      this.modalService.open(modal);
    }
  }

  closeModal(){
    this.modalService.dismissAll()
  }

  redirect(rou) {
    if (rou == "sydney") {
      this.animSRVC.slideToLEFT();
      this.ro.navigateByUrl(rou);
    } else if (rou == "conference") {
      this.animSRVC.slideToLEFT();
      this.ro.navigateByUrl(rou);
    } else if (rou == "registration") {
      this.animSRVC.slideToLEFT();
      this.ro.navigateByUrl(rou);
    } else if (rou == "contact") {
      this.animSRVC.slideToLEFT();
      this.ro.navigateByUrl(rou);
    }
  }

  submitVendorCode() {
    if(this.vendorCode){
      let vendorCode: string = this.vendorCode.value || this.vendorCode  
      console.log(this.vendorCode);
      let Request = {
        VendorCode: vendorCode.toUpperCase(),
      };
      this.cs
        .supplierMeetPost("SMRegistration/GetUserDetailsByVendorCode", Request)
        .subscribe((res: any) => {
          console.log(res)
          if (
            !this.cs.isUndefinedORNull(res) &&
            res.Message == "success"
          ) {
            localStorage.setItem(
              "WkcxV2RWcEhPWGxSTWpscldsRTlQUT09",
              CryptoJS.AES.encrypt(this.vendorCode, "").toString()
            );
            this.redirect(this.temp_route)
            this.modalService.dismissAll()
          } else {
            this.toast.error('Vendor Not Registered')
          }
            },(err)=>{
          console.log(err)
        });
    }else{
      this.toast.error('Kindly Enter A Vendor Code to Proceed')
    }
  }

  getCurrentYear() {
    this.cs
      .supplierMeetPost("SupplierMeetAdmin/GetCurrentYear", "")
      .subscribe((res: any) => {
        this.current_year = res.ResponseData;
        this.getBackgroundImg();
      });
  }

  getBackgroundImg() {
    let postData = {
      Year: this.current_year,
    };
    this.cs
      .supplierMeetPost(
        "SupplierMeetAdmin/GetSupplierMeetDetails",
        postData
      )
      .subscribe((res: any) => {
        this.back_grd_imgs = res.ResponseData.BackgroundIamges;
        this.cs.supplier_headr_img = res.ResponseData.HeaderIamges.ImagePath;
      });
  }
}
