import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GSTInfoComponent } from './gst-info.component';

describe('GSTInfoComponent', () => {
  let component: GSTInfoComponent;
  let fixture: ComponentFixture<GSTInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GSTInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GSTInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
