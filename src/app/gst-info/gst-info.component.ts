import { Component, OnInit } from "@angular/core";
import { MatIconRegistry } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { MyBusinessServiceService } from "../services/MyBusinessService/my-business-service.service";
declare var require: any
const FileSaver = require('file-saver');
import * as moment from 'moment';
import { CommonUtilityService } from '../services/common/common-utility.service';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: "app-gst-info",
  templateUrl: "./gst-info.component.html",
  styleUrls: ["./gst-info.component.scss"]
})
export class GSTInfoComponent implements OnInit {
  userid = CryptoJS.AES.decrypt(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ=='),"").toString(CryptoJS.enc.Utf8);
  documents = [];
  roleId:any;
  moment = moment
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public commonService:CommonUtilityService,
    private cs: MyBusinessServiceService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon('imgIcon',domSanitizer.bypassSecurityTrustResourceUrl('assets/Icons/innerPages/picture.svg'));
    matIconRegistry.addSvgIcon('downIcon',domSanitizer.bypassSecurityTrustResourceUrl('assets/Icons/innerPages/down-arrow_red.svg'));

    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/excel.svg"
      ),
      
    );
    matIconRegistry.addSvgIcon('pdfIcon',domSanitizer.bypassSecurityTrustResourceUrl('assets/Icons/innerPages/Pdf_Icon.svg'));
    matIconRegistry.addSvgIcon('docIcon',domSanitizer.bypassSecurityTrustResourceUrl('assets/Icons/innerPages/word_Icon.svg'));
    matIconRegistry.addSvgIcon('dwnloadicon',domSanitizer.bypassSecurityTrustResourceUrl('assets/Icons/innerPages/downloadicon.svg'));
    const body = { BusinessCategoryID: 4, BusinessSubCategoryID: 0 };
    this.cs
      .postDataToMyBussiness("DocumentManagement/Get_CMSDocs", body)
      .subscribe(response => {
        console.log(response);
        this.documents = response.ResponseData;
      });
  }
  downloadFile(pdfUrl: string, pdfName: string){
    FileSaver.saveAs(pdfUrl, pdfName);
  }

  ngOnInit() {
    this.roleId = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8)

    let analysticsRequest={
      "userClicked":this.userid,//  localStorage.getItem('userToken'),
      "device": "windows",
      "browser": localStorage.getItem('browser'),
      "moduleType": "Document",
      "module": "GSTinfo"
    }
   this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})
  }
  view(doc){
    window.open(doc,'_blank')
  }
}
