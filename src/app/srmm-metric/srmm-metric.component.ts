import { Component, OnInit } from "@angular/core";
import { SurveyService } from "../services/survey/survey.service";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { FormControl } from "@angular/forms";
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { resolve } from 'url';
import { Workbook } from 'exceljs';
import * as fs from "file-saver";

@Component({
  selector: "app-srmm-metric",
  templateUrl: "./srmm-metric.component.html",
  styleUrls: ["./srmm-metric.component.scss"],
})
export class SrmmMetricComponent implements OnInit {
  DataSource = [];
  category = new FormControl();
  year = new FormControl();
  currentYear:any;
  categoryNew:any='1';
  constructor(
    public http: SurveyService,
    public commonService: CommonUtilityService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public cs: MySuggestionService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/excel.svg"
      )
    );
 
    this.currentYear= new Date().getFullYear()
    this.currentYear = "FY "+this.currentYear;
    this.getTable(this.currentYear);
  }

  exportToExcel(content?: any) {
    console.log(content)
    if(this.DataSource){
      this.export()
      // this.cs.exportAsExcelFile(content, 'SRMM - Metric Excel Data')
    }else{
      this.getTable(this.currentYear).then(()=>this.export())
      // this.cs.exportJsonAsExcelFile(this.DataSource, 'SRMM - Metric Excel Data')
    }
  }

  export(){

    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("Risk Data");

    let headers = Object.keys(this.DataSource[0])

    let headerRow = worksheet.addRow(headers);
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "00FF0000" },
        bgColor: { argb: "00FF0000" },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };


    });

    let dataNew = this.DataSource
    dataNew.forEach((d, row_itt) => {
      var values = Object.keys(d).map(function (key) {
        return d[key];
      });
      console.log(values);
      let row = worksheet.addRow(values);

      headers.forEach((header,col_itt)=>{
        let color = 'FF99FF99';
        
        if(row.getCell(col_itt+1)){
          let cell = row.getCell(col_itt+1);
          if(row_itt <= 1){
            color = "e33045"
          } else if(header != 'Que Id' && header != 'Category' && header != 'Vendor code') {
            if(cell.value <= 1.9) {
               color = 'e33045'
              } else
            if (cell.value <= 2.9) {
              color = 'ffc0cb'
            } else 
            if(cell.value <= 3.9) {
              color = 'ffff00'
            } else
            if(cell.value <= 4.9){
              color = '95C883'
            } else {
              color = '317824'
            }
          }
          cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: color }
          }
          cell.border = {
            top: { style: "thin"},
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
        }

      })
    });
    workbook.xlsx.writeBuffer().then((dataNew) => {
      let blob = new Blob([dataNew], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(blob, "Risk_reportData.xlsx");
    });
  }

  getTable(year) {
    return new Promise((resolve,reject)=>{
      let body;
      let qids = '1'
      
      // if year 2021
      if(year == "FY 2021"){
        for(let i=2; i<=90; i++){
          qids += ','+i 
        }
        var category = '11,14,1,8,3,7,2,5,9,12,6,10,13,15'
      } else {
        for(let i=2; i<=90; i++){
          qids += ','+i 
        }
        var category = 
        "Power,Safety & Sustainability,Compliance,Machinery,Funding,Liquidity,Dependence on Tier 2/3 suppliers,Geography,Management,Profitability,Labour,Performance,Quality,Technology"
      }
      if(this.categoryNew == '1'){
      body = {
        QuestionIds: qids,
        Categorys: category,
        Year: year 
          // Year: this.year.value || "FY 2017",
      };
    }
    else{
      body=
      {
        QuestionIds: qids,
        Categorys:category,
        Year:year,
        ddlYearWise:"2"
      }
    }
      this.http
        .call("SRMM_Metric/GetReportSRMM_Metric", body)
        .subscribe((res) => {
          console.log(res);
       //   console.log(JSON.parse(res.ResponseData));
          this.DataSource = JSON.parse(res.ResponseData);
          resolve()
        });

    })
  }

  trim(string: String){
    // console.log(string)
    // if(string != 'Que Id' && string != 'Category' && string != 'Vendor code' && this.categoryNew == 2){
    //   return string.slice(0, 5)
    // }else{
    //   return string
    // }
    return string
  }

  ngOnInit() {}
}
