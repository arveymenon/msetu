import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SrmmMetricComponent } from './srmm-metric.component';

describe('SrmmMetricComponent', () => {
  let component: SrmmMetricComponent;
  let fixture: ComponentFixture<SrmmMetricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SrmmMetricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SrmmMetricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
