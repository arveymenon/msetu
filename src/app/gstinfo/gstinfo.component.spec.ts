import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GSTINFOComponent } from './gstinfo.component';

describe('GSTINFOComponent', () => {
  let component: GSTINFOComponent;
  let fixture: ComponentFixture<GSTINFOComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GSTINFOComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GSTINFOComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
