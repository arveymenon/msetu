import { Component, OnInit, ViewChild, ElementRef, Input } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { NgbRatingConfig } from "@ng-bootstrap/ng-bootstrap";
import { FormArray, Validators, FormControl } from "@angular/forms";
import { SurveyService } from "../services/survey/survey.service";
import { Observable } from "rxjs";
import { resolve } from "url";
import { ToastrService } from "ngx-toastr";
import { MatIconRegistry, MatCard } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

import * as CryptoJS from "crypto-js";
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { ToastServiceService } from "../services/toaster/toast-service.service";

@Component({
  selector: "app-self-assessment",
  templateUrl: "./self-assessment.component.html",
  styleUrls: ["./self-assessment.component.scss"],
  providers: [NgbRatingConfig],
})
export class SelfAssessmentComponent implements OnInit {
  userid = CryptoJS.AES.decrypt(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ=='),"").toString(CryptoJS.enc.Utf8);
  @ViewChild("questions") questions: any;

  roleId = CryptoJS.AES.decrypt(
    localStorage.getItem("WTIwNWMxcFZiR3M9"),
    ""
  ).toString(CryptoJS.enc.Utf8);

  primaryRoleId = CryptoJS.AES.decrypt(localStorage.getItem("PR"), "").toString(
    CryptoJS.enc.Utf8
  );
  primaryRoleName = CryptoJS.AES.decrypt(localStorage.getItem("DG"), "").toString(
    CryptoJS.enc.Utf8
  );

  username = CryptoJS.AES.decrypt(
    localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),
    ""
  ).toString(CryptoJS.enc.Utf8);
  vendorCode = CryptoJS.AES.decrypt(
    localStorage.getItem("WkcxV2RWcEhPWGxSTWpscldsRTlQUT09"),
    ""
  ).toString(CryptoJS.enc.Utf8);

  scheduleId: any;

  activeMenuItem = "Funding";
  surveyForm = new FormArray([]);
  showSave = false;
  menu: any = [];
  files = [];

  error = "";
  errorQuestions = [];
  errorFiles = [];
  navigatedFromSrmmAssesor = false;
  isAssessor: any;

  constructor(
    public _location: Location,
    public commonService: CommonUtilityService,
    public rating: NgbRatingConfig,
    public surveyService: SurveyService,
    public toastr: ToastrService,
    public tost: ToastServiceService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public router: Router,
    public route: ActivatedRoute,
    public cs: MySuggestionService
  ) {
    this.commonService.changeIsAuthenticate(true);
    this.rating.max = 5;
    matIconRegistry.addSvgIcon(
      "arrowIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Rightarrow.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "checkIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/righttik.svg"
      )
    );
  }
  yearDDL:any;
  ngOnInit() {
    // this.roleId = atob(this.roleId);
    // this.isSubmitted().then((submited) => {
    // if (submited) {
    // currentLoginUserName: "mahindraext\\DM181-k",
    this.route.queryParams.subscribe((qParams) => {
      console.log(qParams.si);
      var body: any;
      // if (qParams.si) {
      //   this.vendorCode = qParams.si.toLowerCase();
   
      // }
      
        if(qParams["si"] && qParams["FY"]){
          this.vendorCode = qParams["si"];
          this.yearDDL = qParams["FY"];
         
          body = {
              currentLoginUserName: this.username,
              supplierCode: this.vendorCode,
              roleId: this.primaryRoleId,
              roleName: this.primaryRoleId,
            };
        } 
        
       else {
        body = {
          currentLoginUserName: this.username,
          supplierCode: this.vendorCode,
          roleId: this.primaryRoleId,
          roleName: this.primaryRoleId,
        };
      }
      alert(this.username);
      this.surveyService
        .getSSISurveyQuestions("SRMMSurvey/GetSupplierSurveyQuestion", body)
        .subscribe((res: any) => {
          if (res.ResponseData) {
            console.log(res.ResponseData);
            this.scheduleId = res.SchID;
            this.menu = res.ResponseData;
            this.isAssessor = res.IsAssessor;
            this.setQuestionsForm();
          } else {
            // alert(res.Message)
            this.toastr.error(res.Message);
            // this.error = res.Message
          }
        });
    });
    // } else {
    //   alert("Survey Already Responded");
    // }
    // });
    let analysticsRequest={
      "userClicked":this.userid,//  localStorage.getItem('userToken'),
      "device": "windows",
      "browser": localStorage.getItem('browser'),
      "moduleType": "Survey",
      "module": "SelfAssessment"
    }
   this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})
  }

  // isSubmitted() {
  //   return new Promise((resolve, reject) => {
  //     let body = {
  //       SurveyTypeID: 3,
  //       TokenId: this.username, // localStorage.getItem("userToken"),
  //     };
  //     this.surveyService
  //       .getMySurveyQuestions("AdminCrudSurvey/Msetu_Get_SurveyRespCheck", body)
  //       .subscribe((res) => {
  //         console.log(res);
  //         const response = res.ResponseData == "Failed" ? true : false;
  //         resolve(response);
  //       });
  //   });
  // }
  updateList:any=[];
  radioChange(event,id,ans,applicable,q){
    console.log(ans.value);
    let answer =ans.value;
    let rating;
    // currentLoginUserName: this.username,
    answer = answer.slice(4);
    rating = ans.value.slice(0, 1);
    let passedYear = this.yearDDL.slice(3);
// alert(answer)

    if(applicable == '1'){
      if(this.updateList.length>0){
      for(let i =0;i <this.updateList.length;i++){
        if(this.updateList[i].QId == id){
          this.updateList.splice(i,1)
          this.updateList.push({
            QId         : id,
            Answer      : answer,
            rating      : rating,
            Year        : passedYear,
            SupplierCode: this.vendorCode,
            Assesor     : this.username
          })
        }
        else{
          // {"QId":"40","Answer":"1","Year":"2020","SupplierCode":"a1704","Assesor":"MAHINDRA\\25003760","LoginId":"mahindraext\\A1704-k"}
          this.updateList.push({
            QId         : id,
            Answer      : answer,
            rating      : rating,
            Year        : passedYear,
            SupplierCode: this.vendorCode,
            Assesor     : this.username
          })
        }
      } 
    }
    else{
      this.updateList.push({
        QId         : id,
        Answer      : answer,
        rating      : rating,
        Year        : passedYear,
        SupplierCode: this.vendorCode,
        Assesor     : this.username
      })
    }
  }

    
  }
  EditData(){
   // alert(this.updateList.length);
    this.surveyService.getSSISurveyQuestions("SupplierAnswers/UpdateSupplierResponce", this.updateList)
    .subscribe((res: any) => {
      if (res.ResponseData) {
        console.log(res.ResponseData);
       this.toastr.success(res.Message);
       this.updateList =[];
      } else {
        // alert(res.Message)
        this.toastr.error(res.Message);
        // this.error = res.Message
      }
    });
  }

  setQuestionsForm() {
    console.log(this.menu);
    let questionNo = 1;
    if (this.menu) {
      this.menu.forEach((category, index) => {
        if (index == 0) {
          this.activeMenuItem = category.CategoryName;
        }

        category.error = null;

        if (category.listQuest) {
          category.listQuest.forEach((question) => {
            console.log(question);
            question.questionNo = questionNo;

            let answer = "";
            let number = "";
            let remarks = "";

            if (question.Answers) {
              let rating = question.Answers.Rating;
              if (question.OPTRATING0 == rating) {
                number = "0";
              }
              if (question.OPTRATING1 == rating) {
                number = "1";
              }
              if (question.OPTRATING2 == rating) {
                number = "2";
              }
              if (question.OPTRATING3 == rating) {
                number = "3";
              }
              if (question.OPTRATING4 == rating) {
                number = "4";
              }
              if (question.OPTRATING5 == rating) {
                number = "5";
              }

              if (question["OPTEXT" + number] != "") {
                let ratingValue = question["OPTRATING" + number];
                let ratingText = question["OPTEXT" + number];

                answer = question.Answers
                  ? ratingValue + "@-@" + ratingText
                  : "";
              }
              
              if(question.Answers.Remarks) {
                remarks = question.Answers.Remarks;
              }
              else{
                remarks = '';
              }
            }

            if (question.ISREQUIRED == true) {
              question.answer = new FormControl(answer, Validators.required);
            } else {
              question.answer = new FormControl(answer);
            }
            question.file_name = "";
            // if(question.Answers.Remarks)
            // question.remarks = new FormControl(question.Answers.Remarks);
            // else
            question.remarks = new FormControl(remarks);
         

            questionNo = questionNo + 1;
          });
        }
      });
    }
    console.log(this.menu);
  }
  selectedIndex: number = 0;

  changeViewTo(viewableSurvey, i) {
    console.log(viewableSurvey);
    this.selectedIndex = i;
    this.activeMenuItem = viewableSurvey;
    this.menu[this.menu.length - 1].CategoryName == viewableSurvey
      ? (this.showSave = true)
      : (this.showSave = false);
  }

  next() {
    console.log(this.menu);
    this.questions.nativeElement.scrollTop = 0;
    let index = this.menu.findIndex(
      (o) => o.CategoryName == this.activeMenuItem
    );

    this.selectedIndex = index + 1;
    console.log(index);
    if (this.menu[index + 1]) {
      this.showSave = false;
      this.activeMenuItem = this.menu[index + 1].CategoryName;
    } else {
      this.showSave = true;
    }
  }

  detectFiles(event: any, menuItemItt, questionItt) {
    console.log(menuItemItt);
    console.log(questionItt);
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let ext = file.name.split(".").pop().toLowerCase();
        let reader = new FileReader();
        reader.onload = (e: any) => {
          console.log(file);
          console.log(file.size);

          if (file.size < 5000000) {
            this.menu[menuItemItt].listQuest[questionItt].file_name = file.name;
            this.menu[menuItemItt].listQuest[questionItt].doc_Link =
              file.webkitRelativePath;

            console.log(
              this.menu[menuItemItt].listQuest[questionItt].QuestionID
            );
            let index = this.files.findIndex(
              (o) =>
                o.qid ==
                this.menu[menuItemItt].listQuest[questionItt].QuestionID
            );
            if (index > -1) {
              this.files.splice(index, 1);
            }
            this.files.push({
              file: file,
              qid: this.menu[menuItemItt].listQuest[questionItt].QuestionID,
              qNo: this.menu[menuItemItt].listQuest[questionItt].questionNo,
            });
            this.toastr.success(file.name + " Added Successfully");
          } else {
            this.tost.error("Kindly add a file smaller than 5 MB");
          }
        };
        reader.readAsDataURL(file);
      }
    }
  }

  submit(submit) {
    console.log(this.menu);
    console.log(this.files);
    // location.reload();
    // [{"scheduleID": 2356,"questionID": 21,"selectedOption": 2,"selectedRating": 3,
    // "remark": "test","createdModifiedBy": "system","supplierCode": "DS029","mode": "submit"}]

    let formData = new FormData();
    var reqjson = [];
    this.menu.forEach((category) => {
      console.log(category);
      category.listQuest.forEach((question) => {
        if (question.answer.value) {
          // if (
          //   (this.isAssessor == "True" && question.APPLICABLETO == "2") ||
          //   this.isAssessor == "False"
          // ) {      
            reqjson.push({
              scheduleID: this.scheduleId,
              questionID: question.QuestionID,
              selectedRating: question.answer.value.split("@-@")[0],
              selectedOption: question.answer.value.split("@-@")[1],
              remark: question.remarks.value,
              createdModifiedBy: this.username, // localStorage.getItem('userToken'),
              supplierCode: this.vendorCode, // localStorage.getItem('vendorCode'),
              mode: submit == 1 ? "Submit" : "Draft",
            });
          // }
        }
      });
    });
    console.log(reqjson);
    formData.append("reqjson", JSON.stringify(reqjson));
    formData.append("IsAssessor", this.isAssessor);

    this.files.forEach((file, index) => {
      formData.append("File" + file.qid, file.file);
    });

    let byPassValidation = submit == 0 ? true : false;

    this.checkFormValidity().then((valid) => {
      console.log(valid);
      if (valid || byPassValidation) {
        this.surveyService
          .call("SRMMSurvey/InsertSupplierResponse", formData)
          .subscribe((res) => {
            this.toastr.success(res.Message);
            console.log(res);
            if (submit == 1) {
              this._location.back();
            }
          });
      } else {
        this.tost.error(
          "Kindly fill in all the required fields to submit the form"
        );
      }
    });
  }

  goToTier2N3() {
    this.router.navigateByUrl("/CMSSurvey/tier2N3");
  }

  checkFormValidity(onlyCheck?: any) {
    this.errorQuestions = [];
    this.errorFiles = [];
    return new Promise((res, rej) => {
      this.menu.forEach((category, catInt) => {
        let categoryFormArray = new FormArray([]);
        category.listQuest.forEach((question, qInt) => {
          this.surveyForm.push(question.answer);

          // For Answers in Category
          categoryFormArray.push(
            new FormControl(question.answer.value, Validators.required)
          );

          if (!question.answer.value) {
            this.errorQuestions.push(question.questionNo);
            console.log(this.errorQuestions);
          }

          // For Files in Category
          if (question.ATTACHMENT == 2 && this.isAssessor == "False") {
            let file = this.files.find((o) => o.qid == question.QuestionID);
            if (!file) {
              this.errorFiles.push(question.questionNo);
            }
          }

          console.log(this.surveyForm.valid);

          // last question of the category
          if (category.listQuest.length - 1 == qInt) {
            if (!categoryFormArray.valid) {
              console.log("Error", category);
              category.error = true;
            } else {
              category.error = false;
            }

            // last category of the all categories ie. menu
            this.surveyForm.updateValueAndValidity();
            if (this.menu.length - 1 == catInt) {
              // Validating Answers
              if (this.errorQuestions.length > 0 && onlyCheck == true) {
                let error = "";
                this.errorQuestions.forEach((element) => {
                  error = error + element + ", ";
                });

                this.toastr.error(
                  "The following questions are pending: " + error
                );
              }

              // Validating Files
              if (this.errorFiles.length > 0) {
                let error = "";
                this.errorFiles.forEach((element) => {
                  error = error + element + ", ";
                });

                this.toastr.error(
                  "The following questions Files are pending: " + error
                );
              }

              if (this.surveyForm.valid && this.errorFiles.length == 0) {
                console.log(this.menu);
                res(true);
              } else {
                res(false);
              }
            }
          }
        });
      });
    });
  }
}
