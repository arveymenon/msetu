import { Component, ViewChild, HostListener, AfterViewInit, OnDestroy } from "@angular/core";
import { MySuggestionService } from "./services/MySuggestion/my-suggestion.service";
import { MainNavComponent } from "./main-nav/main-nav.component";
import { AnimationService } from "./Supplier-Meet-common/animation.service";
import { trigger, transition } from "@angular/animations";
import { slideToLeft, slideToRight } from "./Supplier-Meet-common/animations";
import { IdleSessionService } from './services/IdleSession/idle-session.service';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from './services/api/api.service';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  animations: [
    trigger("routeTransition", [
      transition("* => slideToLeft", slideToLeft),
      transition("* => slideToRight", slideToRight),
      transition("* => slideToLeftDuplicate", slideToLeft),
      transition("* => slideToRightDuplicate", slideToRight),
    ]),
  ],
})
export class AppComponent implements OnDestroy {
  title = "Msetu";
  @ViewChild(MainNavComponent) MainNavComponent;
  loading;
  username:any
 config = {
    uiColor: '#ffffff',
    toolbarGroups: [{ name: 'clipboard', groups: ['clipboard', 'undo'] },
    { name: 'editing', groups: ['find', 'selection', 'spellchecker'] },
    { name: 'links' }, { name: 'insert' },
    { name: 'document', groups: ['mode', 'document', 'doctools'] },
    { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align'] },
    { name: 'styles' },
    { name: 'colors' }],
    skin: 'kama',
    resize_enabled: false,
    removePlugins: 'elementspath,save,magicline',
    extraPlugins: 'divarea,smiley,justify,indentblock,colordialog',
    colorButton_foreStyle: {
       element: 'font',
       attributes: { 'color': '#(color)' }
    },
    height: 188,
    removeDialogTabs: 'image:advanced;link:advanced',
    removeButtons: 'Subscript,Superscript,Anchor,Source,Table',
    format_tags: 'p;h1;h2;h3;pre;div'
 }

  constructor(public api:ApiService,
    public cs: MySuggestionService,private http: HttpClient,
    private animSRVC: AnimationService,
    private idleService: IdleSessionService,
    public router: Router
  ) {
    this.idleService;
    this.loading = true;
    if(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==')){
      this.username = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
      this.username = CryptoJS.AES.decrypt(this.username,"").toString(CryptoJS.enc.Utf8);
    }
  }

  ngOnDestroy() {
    
}


  @HostListener('window:beforeunload', ['$event'])
   unloadHandler(event: Event) {
    let isUserLogin = btoa("isUserLogin");
    let Request =
    {
      "TokenId": this.username
    }
    this.cs.postMySuggestionData('Login/Logout',Request).subscribe()
    // this.cookieService.set(isUserLogin,btoa("No") ,null , '/msetu');
  }


  @HostListener("mouseover")
  myClick() {
    let data = this;
    window.addEventListener("popstate", function (event) {
      data.MainNavComponent.closeMenu._elementRef.nativeElement.click();
    });
    if (!this.MainNavComponent.sideMenuFlag) {
      if (
        this.MainNavComponent.divElement.nativeElement.contains(event.target) ||
        this.MainNavComponent.sideNavecontainer._elementRef.nativeElement.contains(
          event.target
        )
      )
        this.MainNavComponent.click_outside_Menu = false;
      else {
        if (!this.MainNavComponent.addRemovButtonClick)
          this.MainNavComponent.click_outside_Menu = true;
        else this.MainNavComponent.click_outside_Menu = false;
      }

      if (this.MainNavComponent.click_outside_Menu)
        this.MainNavComponent.sideMenuFlag = true;
      else this.MainNavComponent.sideMenuFlag = false;
      this.MainNavComponent.addRemovButtonClick = false;
    }
  }

  getAnimation() {
    return this.animSRVC.getCurrentAnimation();
  }
}
