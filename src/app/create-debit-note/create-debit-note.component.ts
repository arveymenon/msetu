import { Component, OnInit, ViewChild, HostListener } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { MatIconRegistry, MatSelect } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormControl, FormBuilder, Validators } from "@angular/forms";
import { Observable } from "rxjs";
import { startWith, map } from "rxjs/operators";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import * as CryptoJS from "crypto-js";
import { Scroll, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-create-debit-note",
  templateUrl: "./create-debit-note.component.html",
  styleUrls: ["./create-debit-note.component.scss"],
})
export class CreateDebitNoteComponent implements OnInit {
  control = new FormControl();
  streets: string[] = [
    "Champs-Élysées",
    "Lombard Street",
    "Abbey Road",
    "Fifth Avenue",
    "Abbey Road",
    "Fifth Avenue",
    "Abbey Road",
    "Fifth Avenue",
    "Abbey Road",
    "Fifth Avenue",
    "Abbey Road",
    "Fifth Avenue",
    "Abbey Road",
    "Fifth Avenue",
    "Abbey Road",
    "Fifth Avenue",
    "Abbey Road",
    "Fifth Avenue",
  ];
  filteredStreets: Observable<any[]>;
  debitNoteForm: any;
  approveDetailsForm: any;
  vendor_details = [];
  commodity_details = [];
  RoleID: any;
  username: any;
  attachment = [];
  editableData: any = {};
  @ViewChild("stepper") stepper: any;
  @ViewChild("selectBox") matSelect: any;
  @HostListener("scroll", ["$event"]) scroll(e: Scroll) {
    console.log(e);
  }
  searchBox = new FormControl();
  @ViewChild("newapproverform") newapproverform: any;
  @ViewChild("newform") newform: any;
  debitNoteMaster_details: any = [];
  allApprovers: any = [];
  searchedApprovers = [];

  constructor(
    public cs: MySuggestionService,
    private _formBuilder: FormBuilder,
    public commonService: CommonUtilityService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private modalService: NgbModal,
    public route: ActivatedRoute
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "arrowIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/cmsDashboard/Admin_Iconarrow.svg"
      )
    );
    this.getVendorDetails();
    this.getCommodityDetails();
    this.getDebitNotemasterDetails();
  }
  ngOnInit() {
    this.RoleID = localStorage.getItem("WTIwNWMxcFZiR3M9");
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(
      CryptoJS.enc.Utf8
    );
    this.username = localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ==");
    this.username = CryptoJS.AES.decrypt(this.username, "").toString(
      CryptoJS.enc.Utf8
    );

    this.route.queryParams.subscribe((params) => {
      console.log(params);
      if (params.did) {
        console.log("Params", params);
        this.cs
          .postMySuggestionData("DebitNote/GetDebitNoteByID", {
            DebitNote_ID: params.did,
          })
          .subscribe((res) => {
            console.log(res);
            this.editableData = res.ResponseData;
            this.createApproveDetailsForm();
            this.createDebitNoteForm();
          });
      } else {
        this.createApproveDetailsForm();
        this.createDebitNoteForm();
      }
    });
    // while(true){
    //   setTimeout(()=>{
    //     this.debitNoteForm.value
    //   },3000)
    // }
    this.filteredStreets = this.approveDetailsForm.controls.QA_DH.valueChanges.pipe(
      startWith(""),
      map((value: any) => this._filter(value))
    );
    // this.searchBox.valueChanges.subscribe(res=>{
    //   console.log(res)
    //   if(res.length > 2){
    //     this.searchedApprovers = []
    //     for(let approver of this.allApprovers.listInternalUser) {
    //       var itt = 0
    //       while(itt < 10){
    //         if(approver.UserName.includes(res) || approver.Name.includes(res)){
    //           this.searchedApprovers.push(approver)
    //           itt++
    //         }
    //       }
    //       if(itt == 10){
    //         break;
    //       }
    //     }
    //     console.log(this.searchedApprovers)
    //   }else{
    //     var top = 0
    //       while(top < 10){
    //         this.searchedApprovers.push(this.allApprovers.listInternalUser[top])
    //         top++
    //       }
    //   }
    // })
  }
  private _filter(value: string): string[] {
    const filterValue = this._normalizeValue(value);
    return this.streets.filter((street) =>
      this._normalizeValue(street).includes(filterValue)
    );
  }

  private _normalizeValue(value: string): string {
    return value.toLowerCase().replace(/\s/g, "");
  }

  getVendorDetails(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs
        .postMySuggestionData("Vendor/GetVendorDDL", "")
        .subscribe((res: any) => {
          if (!this.cs.isUndefinedORNull(res) && res.Message == "Success") {
            this.vendor_details = res.ResponseData;
            resolve();
          }
        });
    });
  }

  Reset(Form: any) {
    Form.reset();
  }

  isLinear = true;
  isMatStepComplete: boolean = false;

  validateForm(Form: any, stepper) {
    if (Form.valid) {
      this.isLinear = false;
      stepper.selected.completed = true;
      stepper.next();
    } else {
      this.cs.showError("Please fill all mandatory fields");
      this.isLinear = true;
      stepper.selected.completed = false;
      return;
    }
  }

  getVcodeWiseName(ev) {
    this.vendor_details.filter((x) => {
      if (x.VendorCode == ev.value) {
        this.debitNoteForm.get("supplier_name").setValue(x.VendorCodeName);
      }
    });
  }

  getVNameWiseVCode(ev) {
    this.vendor_details.filter((x) => {
      if (x.VendorCodeName == ev.value) {
        this.debitNoteForm.get("supplier_code").setValue(x.VendorCode);
      }
    });
  }

  getCommodityDetails(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs
        .postMySuggestionData("Vendor/GetCommodityDropdown", "")
        .subscribe((res: any) => {
          if (!this.cs.isUndefinedORNull(res) && res.Message == "Success") {
            this.commodity_details = res.ResponseData;
            resolve();
          }
        });
    });
  }

  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    console.log(event.value);
    console.log(charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
      return false;
    }
    return true;
  }

  getDebitNotemasterDetails(): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs
        .postMySuggestionData("debitnote/GetAllApproverDDL", "")
        .subscribe((res: any) => {
          if (!this.cs.isUndefinedORNull(res) && res.ID == 1) {
            this.allApprovers = res.ResponseData;
            var top = 0;
            while (top < 10) {
              this.searchedApprovers.push(
                res.ResponseData.listInternalUser[top]
              );
              top++;
            }
            // listInternalUser
            this.cs
              .postMySuggestionData("DebitNote/Get_DebitNoteMasterDetails", "")
              .subscribe((res: any) => {
                if (
                  !this.cs.isUndefinedORNull(res) &&
                  res.Message == "Success"
                ) {
                  this.debitNoteMaster_details = res.ResponseData;
                }
                resolve();
              });
          }
        });
    });
  }
  getCustomMasters(id) {
    return this.allApprovers.listApproverGroupUser.filter(
      (o) => o.GroupID == id
    );
  }

  createDebitNoteForm() {
    this.debitNoteForm = this._formBuilder.group({
      supplier_code: [
        this.editableData.SupplierCode || "",
        Validators.required,
      ],
      commodity: [this.editableData.Commodity || "", Validators.required],
      debit_amount: [
        this.editableData.DebitNoteAmount || "",
        Validators.required,
      ],
      supplier_name: [
        this.editableData.SupplierName || "",
        Validators.required,
      ],
      comments: [this.editableData.Comments || "", Validators.required],
      attachment: [
        this.editableData.FileName || "",
        this.editableData.FileName ? null : Validators.required,
      ],
    });
  }

  createApproveDetailsForm() {
    this.approveDetailsForm = this._formBuilder.group({
      QA_DH: [this.editableData.QADHApproverName || "", Validators.required],
      CDDMM_DH: [this.editableData.CDMMDH || ""],
      CDMM_GM: [
        this.editableData.CDMMGMApproverName || "",
        Validators.required,
      ],
      VP_QA: [this.editableData.VPQAApproverName || "", Validators.required],
      Accounts: [this.editableData.GMAccounts || ""],
      QA_manager: [this.editableData.QAManager || ""],
      CDMM_manager: [this.editableData.CDMMManager || ""],
      VP_SCPC: [
        this.editableData.VPSCPCApproverName || "",
        Validators.required,
      ],
      VP_CDMM: [
        this.editableData.VPCDMMApproverName || "",
        Validators.required,
      ],
      GM_SCPC: [this.editableData.GMSCPC || ""],
      Account_approver: [
        this.editableData.AccountsApproverName || "",
        Validators.required,
      ],
    });
  }

  detectFiles(event: any) {
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let ext = file.name.split(".").pop().toLowerCase();
        let reader = new FileReader();
        reader.onload = (e: any) => {
          if (
            ext != "jpg" &&
            ext != "jpeg" &&
            ext != "png" &&
            ext != "pdf" &&
            ext != "xlsx" && 
            ext != "xls" &&
            ext != "pptx" &&
            ext != "ppt"
          ) {
            this.cs.showError(
              "Please select 'jpg', jpeg','xls','xlsx','pdf', 'ppt','pptx' or 'png' format"
            );
            this.debitNoteForm.get("attachment").setValue("");
            return;
          } else {
            this.attachment = [];
            this.attachment.push(file);
            this.debitNoteForm.get("attachment").setValue(file.name);
          }
        };
        reader.readAsDataURL(file);
      }
    }
  }

  submitCreateDebitNote() {
    if (this.debitNoteForm.valid && this.approveDetailsForm.valid) {
      let Request = {
        supplierCode: this.debitNoteForm.controls.supplier_code.value,
        supplierName: this.debitNoteForm.controls.supplier_name.value,
        commodity: this.debitNoteForm.controls.commodity.value,
        comments: this.debitNoteForm.controls.comments.value,
        debitNoteAmount: this.debitNoteForm.controls.debit_amount.value,
        qadhName: this.approveDetailsForm.controls.QA_DH.value,
        qaManager: this.approveDetailsForm.controls.QA_manager.value,
        cdmM_DH: this.approveDetailsForm.controls.CDDMM_DH.value,
        cdmM_Manager: this.approveDetailsForm.controls.CDMM_manager.value,
        cdmM_GM: this.approveDetailsForm.controls.CDMM_GM.value,
        vP_SCPC: this.approveDetailsForm.controls.VP_SCPC.value,
        vP_QA: this.approveDetailsForm.controls.VP_QA.value,
        vP_CDMM: this.approveDetailsForm.controls.VP_CDMM.value,
        accounts: this.approveDetailsForm.controls.Accounts.value,
        gM_SCPC: this.approveDetailsForm.controls.GM_SCPC.value,
        accountApprover: this.approveDetailsForm.controls.Account_approver
          .value,
        createdBy: this.username,
        debitNoteID: this.editableData.DebitNote_ID || 0,
        DebitNoteAttachment_ID: this.editableData.DebitNoteAttachment_ID || 0,
        DebitNoteNo: 0,
      };
      let formData: FormData = new FormData();
      formData.append("DebitNoteCreationReq", JSON.stringify(Request));
      formData.append("Attachment", this.attachment[0]);

      this.cs
        .postMySuggestionData("DebitNote/CreateDebitNote", formData)
        .subscribe((res: any) => {
          if (res.Message == "Success") {
            if (this.editableData.DebitNote_ID) {
              this.cs.showSuccess(
                "Debit note No " +
                  this.editableData.DebitNote_ID +
                  " has been updated"
              );
            } else {
              this.cs.showSuccess(
                "Debit Note No" +
                  res.ResponseData +
                  "has been created successfully"
              );
            }
            this.newapproverform.resetForm();
            this.newform.resetForm();
            this.stepper.selectedIndex = 0;
            return;
          } else {
            this.cs.showError("Something went wrong please try again");
            return;
          }
        });
    } else {
      this.cs.showError("Please fill all mandatory fields");
      return;
    }
  }

  openFile() {
    console.log(this.attachment);
    if (this.attachment.length > 0) {
      var blob = new Blob(this.attachment, { type: this.attachment[0].type });
      var url = window.URL.createObjectURL(blob);
      window.open(url, "_blank");
    } else if (this.editableData) {
      window.open(this.editableData.AttachmentPath, "_blank");
    }
  }

  getSupplierName() {
    this.cs
      .postMySuggestionData("DebitNote/GetDetailsByVendorCode", {
        VendorCode: this.debitNoteForm.value.supplier_code,
      })
      .subscribe((res) => {
        console.log(res);
        if(res.ID == 1){
          this.debitNoteForm.get('supplier_name').setValue(res.ResponseData.VendorCodeName)
        }else{
          this.debitNoteForm.get('supplier_name').reset()
        }
      });
  }
}
