import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CommonUtilityService } from '../services/common/common-utility.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import {MatTooltipModule} from '@angular/material/tooltip';
import { FormBuilder, Validators, FormControl, FormGroup, FormArray  } from '@angular/forms';
import { AdminDashboardService } from '../services/admin_Dashboard/admin-dashboard.service';
import * as moment from "moment"; 
import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS } from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS} from '././date-adapter'; 
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';

@Component({
  selector: 'app-cmslanding',
  templateUrl: './cmslanding.component.html',
  styleUrls: ['./cmslanding.component.scss'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]
})
export class CMSLandingComponent implements OnInit {
  urls=[];
  fileUrls=[];
  pdffileUrls= [];
  fileName:any;
  pdfFileName:any;
  fromDate: Date;
  toDate: Date;
  pageSize: number;
  today: Date = new Date();
  NewsAndEventsForm:FormGroup;
  videoForm:FormGroup;
  bannerForm:FormGroup;
  dragRowData1:any=[];
  dragRowData2:any=[];
  dragRowData:any=[];
  categoryID:any;
  pageNo: number = 1;
  formID: any='0';
  historyDataSource:any=[];
  displayedColumns: string[] =['title','Link','Content','PDFfile','startDate','endDate','status'];
  actionFlag :any;
  passedData :any;
  @ViewChild('formbanner')formbanner;
  @ViewChild('formVideo')formVideo;
  @ViewChild('formNewsEvents')formNewsEvents; 
  sequenceList :any=[];
  data:any;
  fileSize:any;

  config = {
    uiColor: '#000000',
    toolbarGroups: [
    // { name: 'clipboard', groups: ['clipboard', 'undo'] },
    // { name: 'editing', groups: ['find', 'selection', 'spellchecker'] },
    // { name: 'links' }, { name: 'insert' },
    // { name: 'document', groups: ['mode', 'document', 'doctools'] },
    { name: 'basicstyles', groups: ['basicstyles'] },
    { name: 'paragraph', groups: ['list', 'align'] },
    { name: 'styles' },
    { name: 'colors' }],
    skin: 'kama',
    // { name: 'basicstyles', groups: ['basicstyles'] },
    // { name: 'paragraph', groups: ['list', 'align'] },
    // { name: 'colors' }],

    resize_enabled: true,
    removePlugins: 'elementspath,save,magicline',
    extraPlugins: 'divarea,smiley,justify,indentblock,colordialog',
    colorButton_foreStyle: {
       element: 'font',
       attributes: { 'color': '#(color)' }
    },
    height: 30,
    removeDialogTabs: 'image:advanced;link:advanced',
    removeButtons: 'Subscript,Superscript,Anchor,Source,Table',
    format_tags: 'p;h1;h2;h3;pre;div'
 }
 
  constructor(private toastr: ToastrService,private modalService: NgbModal,public cs1:MySuggestionService,private adminDashboard:AdminDashboardService,private _formBuilder: FormBuilder,private  router:Router,public commonService: CommonUtilityService,private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer) { 
    this.initNewsAndEvents();
    this.initBannerForm();
    this.initvideoForm();cs1.isUndefinedORNull
    this.getBannerList("5","0");  
    this.commonService.changeIsAuthenticate(true);
    this.fileSize = this.commonService.fileSize;
    matIconRegistry.addSvgIcon('dragIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/moveicon.svg'));
  }
  open(content) {
    this.actionFlag = 'add';
    this.modalService.open(content, {  size: 'xl' as 'lg',centered:true, windowClass : "formModal"});
  }
  update(content,data){
    this.actionFlag = 'update';
    this.passedData = data;
    console.log("data",data);
    if(this.formID == '0'){
      this.bannerForm.controls['file'].setValue(data._Get_Img_Video[0].ImagePath);
      this.bannerForm.controls['fromDate'].setValue(data.StartDate);
      this.bannerForm.controls['toDate'].setValue(data.EndDate);
    }
    else if(this.formID == '1'){

    this.videoForm.controls['title'].setValue(data.ContentName);
    this.videoForm.controls['videoLink'].setValue(data.Link);
    this.videoForm.controls['fromDate'].setValue(data.StartDate);
    this.videoForm.controls['toDate'].setValue(data.EndDate);
    }
    else if(this.formID == '2'){
      // this.NewsAndEventsForm = this._formBuilder.group({
      //   title   : ['', [Validators.required]],
      //   fromDate: ['', [Validators.required]],
      //   toDate  : ['',[Validators.required]],
      //   content : ['', [Validators.required]],
      //   document :['',[Validators.required]],
      //   pdfFile: ['',[Validators.required]]
      // });
      this.NewsAndEventsForm.controls['title'].setValue(data.ContentName);
      this.NewsAndEventsForm.controls['content'].setValue(data.SubContentName);
      this.NewsAndEventsForm.controls['fromDate'].setValue(data.StartDate);
      this.NewsAndEventsForm.controls['toDate'].setValue(data.EndDate);
      this.NewsAndEventsForm.controls['document'].setValue(data._Get_Img_Video[0].ImagePath);
      this.NewsAndEventsForm.controls['pdfFile'].setValue(data._Get_Img_Video[1].ImagePath);
      }
    
    this.modalService.open(content, {  size: 'xl' as 'lg',centered:true, windowClass : "formModal"});
    
  }
  validateAllFormFields(formGroup){
      Object.keys(formGroup.controls).forEach(controlName =>{
        const control = formGroup.get(controlName);
        if(control instanceof FormControl){
          control.markAsTouched({onlySelf: true});
        }
        else if(control instanceof FormControl || control instanceof FormArray){
        this.validateAllFormFields(control);
        }
      });
  } 
  closeModal(){
    this.formbanner.resetForm();
    this.formVideo.resetForm();
    this.formNewsEvents.resetForm();
    this.modalService.dismissAll();
  }
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      this.data = event.container.data;
      this.sequenceList =[];
      for(let i= 0;i<this.data.length;i++){
        // let add={
        //   CMSID :this.data[i].CMSID
        // }
        this.sequenceList.push(this.data[i].CMSID);
      }
      console.log(this.sequenceList);
      let add={
        CMSID :this.sequenceList,
        "Seq":0
      }
      // {"CMSID":[493,484,510,509,513],"Seq":0}
      this.adminDashboard.addSequence(add).subscribe((resp:any) => {
        console.log("addSequence here-->",resp);
        this.toastr.success('Success');       
        
        }, (err) => {
         this.toastr.error('Error');
        })
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }
  ngOnInit() {
  }

  openLink(url){
    window.open(url,'_blank');
  }
  detectFiles(event) {
    let files = event.target.files; 
    if (files) {
      for (let file of files) {
        console.log(file);
        this.fileName = file.name;
        let ext=file.name.split(".").pop().toLowerCase();
        console.log(ext);
        let reader = new FileReader();
        reader.onload = (e: any) => {
                if(ext =='jpg'|| ext=='jpeg' || ext=='png'){
                  if(event.target.files[0].size <= this.fileSize){
                    this.fileUrls = [];
                    this.fileUrls.push(file);
                    this.urls.push({url:e.target.result,ext:ext}); 
                    if(this.formID == '0'){
                      this.bannerForm.controls['file'].setValue(this.fileName);
                    }
                    else if(this.formID =='1'){
                      this.videoForm.controls['file'].setValue(this.fileName);
                    }
                    else if(this.formID =='2'){
                      this.NewsAndEventsForm.controls['document'].setValue(this.fileName);
                    }   
                  }
                  else{
                    alert("Cannot exceed file size more than 20MB");
                  }       
                }
                else{
                  alert("please select proper format");
                //  this.utilityProvider.presentToast('Please select file in proper format','4000','top')
                }
        }
        reader.readAsDataURL(file);
      }
    }
  }
  detectFilespdf(events){
    console.log(events);
    let pdffiles = events.target.files;
    if (pdffiles) {
      for (let file of pdffiles) {
        console.log(file);
        this.pdfFileName += ", "+ file.name;
        let ext=file.name.split(".").pop().toLowerCase();
        console.log(ext);
        let reader = new FileReader();
        reader.onload = (e: any) => {
                if(ext =='pdf'){
                    // this.pdffileUrls = [];
                    this.pdffileUrls.push(file);
                    this.urls.push({url:e.target.result,ext:ext}); 
                    
                   // if(this.formID =='2'){
                      this.NewsAndEventsForm.controls['pdfFile'].setValue(this.pdfFileName);
                   // }   
        
                }
                else{
                  alert("please select proper format");
                //  this.utilityProvider.presentToast('Please select file in proper format','4000','top')
                }
        }
        reader.readAsDataURL(file);
      }
    }
  }
  initvideoForm(){
    this.videoForm = this._formBuilder.group({
        title     : ['', [Validators.required]],
        fromDate  : ['', [Validators.required]],
        toDate    : ['',[Validators.required]],
        videoLink : ['', [Validators.required]],
        
      });
  }
  initBannerForm(){
    this.bannerForm = this._formBuilder.group({
        file    : ['', [Validators.required]],
        fromDate: ['', [Validators.required]],
        toDate  : ['',[Validators.required]],
        // content : ['', [Validators.required]],
      });
  }
  initNewsAndEvents(){
    this.NewsAndEventsForm = this._formBuilder.group({
        title   : ['', [Validators.required]],
        fromDate: ['', [Validators.required]],
        toDate  : ['', [Validators.required]],
        content : ['', [Validators.required]],
        document: ['', [Validators.required]],
        pdfFile : ['', [Validators.required]]
      });
  }
  addVideo(flag){
    if(this.videoForm.valid && this.cs1.isUndefinedORNull(this.videoForm.controls.title.value.trim())){
      this.cs1.showError("Title can not be blank");
      return;
    }
    if(this.videoForm.valid && this.cs1.isUndefinedORNull(this.videoForm.controls.videoLink.value.trim())){
      this.cs1.showError("video Link can not be blank");
      return;
    }
    if(this.videoForm.valid){
    let formData: FormData = new FormData;   
    let startDate = moment(this.videoForm.get('fromDate').value).format('YYYY-MM-DD');
    let endDate   = moment(this.videoForm.get('toDate').value).format('YYYY-MM-DD');
    let reqjson= {  
      "CMSId":this.actionFlag == 'add' ? 0 : this.passedData.CMSID,
      "ContentCategoryId":7,
      "ContentName": this.videoForm.get('title').value,
      "SubContentName": "",
      "Link": this.videoForm.get('videoLink').value,
      "StatementType":this.actionFlag == 'add' ? "INSERT" : "UPDATE",  // "Update" // "DELETE"
      "CreatedBy":  "SupplierEMPID",
      "StartDate":startDate,
      "EndDate":endDate, 
      "Event_Year":"",
      "Event_Location": "" 
      }
    formData.append('reqjson', JSON.stringify(reqjson));
    // for (var i = 0; i < this.fileUrls.length; i++) {
    //   formData.append('IMG', this.fileUrls[i]);
    // }
   // console.log(postData);
  
    console.log(formData,"..formData..");
    this.adminDashboard.addLandingPAgeVideo(formData).subscribe((resp:any) => {
    console.log("getNewUpdates here-->",resp);
    
    this.cs1.showSuccess("Data store successfully");
     this.formVideo.resetForm(); 
    if(flag =='Close'){
      this.closeModal();
    }
    this.getBannerList("7","1");  
    }, (err) => {

    })
  }else{
        this.validateAllFormFields(this.videoForm);
  }
  }
  removeValidation(){
     this.formNewsEvents.resetForm(); 
     this.formbanner.resetForm(); 
     this.formVideo.resetForm(); 
  }
  addNewsAndEvents(flag){
    if(this.NewsAndEventsForm.valid && this.cs1.isUndefinedORNull(this.NewsAndEventsForm.controls.title.value.trim())) {
      this.cs1.showError("Title can not be blank");
      return;
    }
    if(this.NewsAndEventsForm.valid){
    let formData: FormData = new FormData; 
    let startDate = moment(this.NewsAndEventsForm.get('fromDate').value).format('YYYY-MM-DD');
    let endDate   = moment(this.NewsAndEventsForm.get('toDate').value).format('YYYY-MM-DD');  
    let reqjson= {
      "CMSId":0,
      "ContentCategoryId":6,
      "ContentName": this.NewsAndEventsForm.get('title').value,
      "SubContentName": this.NewsAndEventsForm.get('content').value.slice(0 , this.NewsAndEventsForm.get('content').value.length - 1),
      "Link":"",  
      "StatementType":"INSERT",  // "Update" // "DELETE"
      "CreatedBy":  "SupplierEMPID",
      "StartDate":startDate,
      "EndDate":endDate,
      "Event_Year":"",
      "Event_Location": "" 
      }
    formData.append('reqjson', JSON.stringify(reqjson));
    for (var i = 0; i < this.fileUrls.length; i++) {
      console.log(this.pdffileUrls[i]);
      formData.append('IMG', this.fileUrls[i]);
    }
    for (var i = 0; i < this.pdffileUrls.length; i++) {
      formData.append(`PDF${i}`,this.pdffileUrls[i]);
    }
    console.log(formData,"..formData..");
    this.adminDashboard.addNewsAndEvents(formData).subscribe((resp:any) => {
    console.log("getNewUpdates here-->",resp);
    this.toastr.success("Added successfully");
    this.getBannerList("6","2");
    this.formNewsEvents.resetForm(); 
    if(flag =='Close'){
      this.closeModal();
    }
    
     
    }, (err) => {

    })
  }
  else{
        this.validateAllFormFields(this.NewsAndEventsForm);
  }
  }
  addLandingPageImages(flag){
  // alert(this.bannerForm.get('fromDate').value)
  if(this.bannerForm.valid)
  {
    let formData: FormData = new FormData;   
    let startDate = moment(this.bannerForm.get('fromDate').value).format('YYYY-MM-DD');
    let endDate   = moment(this.bannerForm.get('toDate').value).format('YYYY-MM-DD');
    let reqjson= {
      "CMSId":this.actionFlag == 'add' ? 0 : this.passedData.CMSID,
      "ContentCategoryId":5,
      "ContentName": "",
      "SubContentName": "",
      "Link":"",  
      "StatementType":this.actionFlag == 'add' ? "INSERT" : "UPDATE",  // "Update" // "DELETE"
      "CreatedBy":  "SupplierEMPID",
      "StartDate":startDate,
      "EndDate":endDate, 
      "Event_Year":"",
      "Event_Location": "" 
      }
      console.log(this.bannerForm.get('fromDate').value);   
      console.log("bannerForm req"+JSON.stringify(reqjson));
      
    formData.append('reqjson', JSON.stringify(reqjson));
    for (var i = 0; i < this.fileUrls.length; i++) {
      formData.append('IMG', this.fileUrls[i]);
    }
   
    this.adminDashboard.addLandingPageImages(formData).subscribe((resp:any) => {
    console.log("getNewUpdates here-->",resp);
    //this.bannerForm.reset();
    this.toastr.success("Added successfully");
    this.getBannerList("5","0");
     this.formbanner.resetForm();
      if(flag=='Close'){
        this.closeModal();
      } 
    }, (err) => {

    })
  }
  else{
        this.validateAllFormFields(this.bannerForm);
  }
  }  
  getBannerList(id,tabID){
    let postData= {
      "PageNo":1,
      "ContentCategoryID":id,  
      "Mode":"CMS"   
  }
  this.adminDashboard.getAdminDashboardCMS(postData).subscribe((resp:any) => {
    console.log("resp here-->",resp);
      if(tabID == 0){
         this.dragRowData= resp.ResponseData;
        }  
      else if(tabID == 1){
        this.dragRowData1= resp.ResponseData;
      }
      else if(tabID == 2){
        this.dragRowData2 = resp.ResponseData;
      }  
    }, (err) => {

})
  }
  bannerClicked(event){
  this.removeValidation()
    if(event.index == 0){
       this.formID = '0';
       this.videoForm.reset();
       this.NewsAndEventsForm.reset();
       this.getBannerList("5","0");  
    }
    else if(event.index == 1){
      this.formID = '1';
      this.NewsAndEventsForm.reset();
      this.bannerForm.reset();
      this.getBannerList("7","1");
    }
    else if(event.index == 2){
      this.formID = '2';
      this.bannerForm.reset();
      this.videoForm.reset();  
      this.getBannerList("6","2");
    }
    else if(event.index == 3){
      this.categoryID = '567';
      this.getHistoryData({ pageIndex: 0, pageSize: 10, length: this.pageSize });
    } 
  }
  getChangedData(){
  this.fromDate =null;
  this.toDate = null;
  this.getHistoryData({ pageIndex: 0, pageSize: 10, length: this.pageSize });
  }
  search(date){
   this.fromDate = date;
  this.getHistoryData({ pageIndex: 0, pageSize: 10, length: this.pageSize });
  }

 ResetDate(){
  this.fromDate =null;
  this.toDate = null;
  this.getHistoryData({ pageIndex: 0, pageSize: 10, length: this.pageSize });
 }

 search1(date){
  this.toDate = date;
  this.getHistoryData({ pageIndex: 0, pageSize: 10, length: this.pageSize });
 }
 getHistoryData(event){
  this.pageNo = event.pageIndex;
 // alert(this.pageNo);
  let sfromDate: any='';
  let stoDate: any='';
  if (this.fromDate !== undefined && this.fromDate !== null) {  
   sfromDate = moment(this.fromDate).format('DD-MM-YYYY');
    console.dir("from date filter--"+sfromDate+"_--"+this.fromDate);
  }
  if (this.toDate !== undefined && this.toDate !== null) {  
    stoDate = moment(this.toDate).format('DD-MM-YYYY');
     console.dir("from date filter--"+stoDate+"_--"+this.toDate);     
   }

  let reqjson= {
   // PageNo: 1,
    PageNo: this.pageNo+1,
    ContentCategoryID: this.categoryID,
    Mode: "CMS",
    FromDate : sfromDate,
    toDate : stoDate
    }
  this.adminDashboard.getHistoryDataAPI(reqjson).subscribe((resp:any) => {
  this.historyDataSource = resp.ResponseData;
  console.log(this.historyDataSource);
  this.pageSize = resp.ResponseData[0].Serial;
 // alert(this.pageSize);
  }, (err) => {

  })
}
//--------------------------------------------update inactive---------------------------------------------
 updateInactive(id,CMSId){
  //alert(CMSId);
  let reqjson=

  { "CMSID": CMSId }
  this.adminDashboard.updateInactive(reqjson).subscribe((resp:any) => {
    console.log("getNewUpdates here-->",resp);
    if(id == '5'){
      this.getBannerList("5","0");
    }
    else if(id=='6'){
      this.getBannerList("6","2");
    }
    else if(id=='7'){
      this.getBannerList("7","1");
    }
     
  }, (err) => {
   
  })
  }
  openPDF(link){
    // alert(link);
     window.open(link, '_blank');
  }
  // editor
  onChange($event: any): void {
    console.log("onChange",$event);
    //this.log += new Date() + "<br />";
  }
}
