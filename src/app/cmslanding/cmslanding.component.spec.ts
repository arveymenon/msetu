import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CMSLandingComponent } from './cmslanding.component';

describe('CMSLandingComponent', () => {
  let component: CMSLandingComponent;
  let fixture: ComponentFixture<CMSLandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CMSLandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CMSLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
