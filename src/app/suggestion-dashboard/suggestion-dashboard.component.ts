import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { MatIconRegistry, MatTableDataSource, MatPaginator } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { FormBuilder } from '@angular/forms';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';

import * as fs from "file-saver";
import * as CryptoJS from 'crypto-js';
import * as moment from 'moment';
import { Workbook } from 'exceljs';

@Component({
  selector: 'app-suggestion-dashboard',
  templateUrl: './suggestion-dashboard.component.html',
  styleUrls: ['./suggestion-dashboard.component.scss']
})
export class SuggestionDashboardComponent implements OnInit {
  displayedColumns: string[] =['IdeaNo', 'proposalDescription','platform','supplierName','ideaReceived','CA','PA','status','pendingClarification'];
  dashBoardData:any=[];
  statusData:any=[];
  filterForm: any;
  platFormData:any=[];
  isDisabled:boolean=true
  @ViewChild('table')table:any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageSize:any;
  pageNo:any;
  filterPageNo:any;
  isAdmin: boolean;
  RoleID: any;
  vendorCode: string;
  usename: string;

    constructor(public activatedRoute:ActivatedRoute, public router:Router, private _formBuilder: FormBuilder,public commonService: CommonUtilityService,
    public cs:MySuggestionService,private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon('excelIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/Excel_White_Icon.svg'));
  
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
      };
  }
    
  ngOnInit() {
    this.vendorCode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
    this.vendorCode = CryptoJS.AES.decrypt(this.vendorCode,"").toString(CryptoJS.enc.Utf8);
    this.RoleID = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID,"").toString(CryptoJS.enc.Utf8);
    this.usename = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
    this.usename = CryptoJS.AES.decrypt(this.usename,"").toString(CryptoJS.enc.Utf8);
    this.createFilterForm();
    this.getDashBoardData(0,0,false);
    this.getStatusData();
    this.getPlatFormList();
  }
  reset(){
    this.filterForm.reset();
    this.getDashBoardData(0, 0, false)
  }

  getDashBoardData(id:any,PageNo:any,isExel:boolean){
    let url;
    let postData;
    if (this.router.url.includes('Admin')){
      url = 'IMCRDashboard/GetDashboardDataAdmin';
      this.isAdmin=true;
      let pageNo1 = PageNo + 1;
      this.pageNo =PageNo;
      postData=
      {
        "IdeaNumber":id==0?"":this.filterForm.controls.Idea_Number.value,
        "SupplierName":id==0?"":this.filterForm.controls.Supplier_Name.value,
        "Platform":"",
        "Status":id==0?"":this.filterForm.controls.Status.value,
        "PageNo":isExel== true?'0':pageNo1
      }
      
    }
    else{
      url = 'IMCRDashboard/GetIMCRDashboardData';
      this.isAdmin=false;
     
    postData={
      // "UserId":"mahindraext\dm181-K",
      "UserId" : this.usename,
      "RoleId":this.RoleID,
      "IdeaNumber":id==0?"":this.filterForm.controls.Idea_Number.value,
      "SupplierName":id==0?"":this.filterForm.controls.Supplier_Name.value,
      "Platform":'',
      "Status":id==0?"":this.filterForm.controls.Status.value,
      "PageNo":PageNo
    }
    }
                 
      this.cs.postMySuggestionData(url,postData).subscribe((res:any)=>{
      if((this.isAdmin && !this.cs.isUndefinedORNull(res.ResponseData)) || (!this.isAdmin && !this.cs.isUndefinedORNull(res.ResponseData.DashBoardDetails))){
        this.isAdmin?this.pageSize=res.TotalCount: this.pageSize=res.ResponseData.Count;
        this.dashBoardData=!this.isAdmin?res.ResponseData.DashBoardDetails:res.ResponseData;
        this.dashBoardData = [...this.dashBoardData];
        if(isExel){
          var json = [];
          this.dashBoardData.forEach(element => {
            json.push({
              "IdeaID" : element.IdeaID,
              "Idea Number" : element.IdeaNumber,
              "Proposal Description": element.ProposalDescription,
              "Platform": element.ModelAffected,
              "Supplier Name": element.SupplierName,
              "Idea Received On Date": moment(element.CreatedDate).format('DD/MM/YYYY'),
              "CA": element.CurrentAgeing,
              "PA": element.ProcessingAgeing,
              "Status": element.StatusOfTranscation,
              "Pending Seek Clarification": element.SeelClarification
            })
          });          

          this.exportExcel(json)
          // this.cs.exportJsonAsExcelFile(json,'IMCR_Dashboard');

          this.getDashBoardData(0,0,false);
        }
      }

     else{
       this.dashBoardData = [];
     }
      // this.cs.exportAsExcelFile(this.dashBoardData,'IMCR_Dashboard');
      if(this.dashBoardData.length>0)
      this.isDisabled=false;
      })
  }

  // exportData(element){
  //   let postData:any={
  //     "IdeaId":element.IdeaID
  //   }
  //    this.cs.postMySuggestionData('IMCRDashboard/GetIdeaDetails',postData).subscribe((res:any)=>{
  //      console.log(res.ResponseData)
  //      this.exportSingleExcel(res.ResponseData)
  //    })
  // }

  exportExcel(json){
    let dataNew = json;
    console.log(dataNew);
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("ReportData");
    let columns = Object.keys(json[0]);
    let headerRow = worksheet.addRow(columns);
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "00FF0000" },
        bgColor: { argb: "00FF0000" },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
    });
    dataNew.forEach((d) => {
      // var obj = JSON.parse(d);
      var values = Object.keys(d).map(function (key) {
        return d[key];
      });
      let row = worksheet.addRow(values);
    });
    workbook.xlsx.writeBuffer().then((dataNew) => {
      let blob = new Blob([dataNew], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(
        blob,
        "SuggestionReport.xlsx"
      );
    });
  }

  getStatusData(){
    this.cs.postMySuggestionData('IMCRDashboard/GetStatusList','').subscribe((res:any)=>{
    this.statusData=res.ResponseData;
    })
  }

  getPlatFormList(){
    let data={"DivisionId":1}
    this.cs.postMySuggestionData('IMCRSuggestion/GetModelAffected',data).subscribe((res:any)=>{
      this.platFormData=res.ResponseData;
    })
  }

  createFilterForm(){
    this.filterForm = this._formBuilder.group({
      Idea_Number: [''],
      Supplier_Name: [''],
      Platform_Name: [''],
      Status: [''],
      });
  }

  navigate(){
    if(this.RoleID == 7 ||  this.RoleID == '7'){
      this.router.navigateByUrl('adminDashboard');
    }else{
      this.router.navigateByUrl('dashboard');
    }
  }

  getDashBoardDataById(data:any){
      let navigationExtras: NavigationExtras = {
        queryParams: {
            "ID": data.IdeaID,
            "navigation_From_Dashboard":"true",
            "Status":data.StatusOfTranscation !="Draft"?1:0,
            "isAdmin":this.isAdmin
        }
    };
    this.router.navigate(['mySuggestion'], navigationExtras);
  }

  exportAsExcelFile(){
    this.getDashBoardData(0,0,true)
  }


  getExportToExcelData(){
    let postData={
      "PageNo":0,
      "VendorCode": ""
    }
     this.cs.postMySuggestionData('Admin/GetVendorList',postData).subscribe((data)=>{
       let json = data.ResponseData['VendorList'];
       this.cs.exportJsonAsExcelFile(json,'VendorList');
     })
   }

  getPageChangeData(ev){
    this.filterPageNo = ev.pageIndex + 1;
    this.getDashBoardData(0,ev.pageIndex,false);
  }

}
