import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuggestionDashboardComponent } from './suggestion-dashboard.component';

describe('SuggestionDashboardComponent', () => {
  let component: SuggestionDashboardComponent;
  let fixture: ComponentFixture<SuggestionDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuggestionDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestionDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
