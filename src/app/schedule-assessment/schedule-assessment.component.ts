import { Component, OnInit } from "@angular/core";
import {
  MatIconRegistry,
  DateAdapter,
  MAT_DATE_FORMATS,
} from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { CommonUtilityService } from "../services/common/common-utility.service";
import {
  FormControl,
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Observable } from "rxjs";
import { startWith, map } from "rxjs/operators";
import { MyHelpDeskService } from "../services/myHelpDesk/my-help-desk.service";
import { ToastrService } from "ngx-toastr";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";

import * as moment from "moment";
import * as _ from "lodash";
import { APP_DATE_FORMATS, AppDateAdapter } from "../cmslanding/date-adapter";

import * as CryptoJS from 'crypto-js';
import { Workbook } from "exceljs";
import * as XLSX from 'xlsx';
import * as fs from "file-saver";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastServiceService } from "../services/toaster/toast-service.service";

@Component({
  selector: "app-schedule-assessment",
  templateUrl: "./schedule-assessment.component.html",
  styleUrls: ["./schedule-assessment.component.scss"],
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: APP_DATE_FORMATS,
    },
  ],
})
export class ScheduleAssessmentComponent implements OnInit {
  selectCheck: boolean;
  today = new Date();
  selectAll = new FormControl(false);
  
  username = CryptoJS.AES.decrypt(
    localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),
    ""
  ).toString(CryptoJS.enc.Utf8);
  DataSource = [
    // {
    //   selectCheck: false,
    //   supplierCode: "DV001",
    //   supplierName: "Visteon Automotive System India Pvt Ltd",
    //   mailTo: "ppravin@hanorsystem.com",
    //   assessmentOn: "",
    //   Assessor: "",
    //   Category: "",
    //   newAssessments: "",
    //   Reassessments: "",
    //   onlineSelfAssessment: "",
    // },
  ];

  primaryColumns: string[] = [
    "selectCheck",
    "supplierCode",
    "supplierName",
    "mailTo",
    "assessmentOn",
    "Assessor",
    "Category",
    "newAssessments",
    "Reassessments",
    "onlineSelfAssessment",
  ];
  secondaryColumns: string[] = [
    "selectCheck",
    "supplierCode",
    "supplierName",
    "mailTo",
    "assessmentOn",
    "Assessor",
    "action",
  ];

  pageSize = 0;
  displayedColumns: string[] = this.primaryColumns;
  commodities = [];
  supplierLocation = [];
  public filterForm: FormGroup;
  Role_Master_Form:any;

  constructor(
    private _formBuilder: FormBuilder,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public httpService: MyHelpDeskService,
    public formBuilder: FormBuilder,
    public commonService: CommonUtilityService,
    public toast: ToastrService,
    public tost : ToastServiceService,
    public modalService: NgbModal,
    public cs: MySuggestionService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );

    this.httpService
      .call("admincrudsurvey/GetSRRMCommodityList")
      .subscribe((res) => {
        console.log(res);
        if (res.Message == "Success") {
          this.commodities = res.ResponseData;
        }
      });

    this.httpService
      .call("admincrudsurvey/GetSRRMSupplierLocation")
      .subscribe((res) => {
        console.log(res);
        if (res.Message == "Success") {
          this.supplierLocation = res.ResponseData;
        }
      });

    this.filterForm = this.formBuilder.group({
      supplierCode: [""],
      supplierName: [""],
      location: [""],
      commodity: [""],
      status: ["UnPlanned"],
    });

    this.filterForm.controls["status"].valueChanges.subscribe((res) => {
      this.filterForm.updateValueAndValidity()
      console.log(res);
      this.search(1);
      this.DataSource = [];
      this.pageSize = 0;
    });

  }
  createRoleForm(){
    this.Role_Master_Form = this._formBuilder.group({
      Bulk_Upload: [{ value: '', disabled: true }, Validators.required]
    });
  }
  ngOnInit() {
    this.search(1);
    this.createRoleForm();
    this.selectAll.valueChanges.subscribe((res) => {
      console.log(res);
      this.DataSource.forEach((element) => {
        if (element.selectCheck.value != res) {
          element.selectCheck.setValue(res);
        }
      });
    });
  }

  lettersOnly(event) {
    console.log(event.which, event.keyCode);
    const charCode = event.which ? event.which : event.keyCode;
    if (
      !(charCode >= 65 && charCode <= 122) &&
      charCode != 32 &&
      charCode != 0
    ) {
      event.preventDefault();
    }
  }

  search(pageNo?: any) {
    console.log(pageNo);
    return new Promise((resolve, reject) => {
      if (this.filterForm.value.status == "UnPlanned") {
        this.displayedColumns = this.primaryColumns;
      } else {
        this.displayedColumns = this.secondaryColumns;
      }
      console.log(this.filterForm);
      let body = {
        status: this.filterForm.value.status || null,
        supplierCode: this.filterForm.value.supplierCode || null,
        supplierName: this.filterForm.value.supplierName || null,
        supplierLocation: this.filterForm.value.location || null,
        commodity: this.filterForm.value.commodity || null,
        IsExcelDownload: false,
        PageNo: pageNo > -1 ? pageNo : 0,
      };
      this.DataSource = [];
      // this.pageSize = 0;
      console.log(body);
      this.httpService
        .call("admincrudsurvey/GetSRRMSupplierAudit", body)
        .subscribe((res) => {
          console.log(res);
          if (res.Message == "Success") {
            this.pageSize = res.TotalCount;
            let dataSource = [];
            res.ResponseData.forEach((data, index) => {
              dataSource.push({
                SchID: null,
                AuditID: null,
                selectCheck: new FormControl(false),
                supplierCode: data.SupplierCode,
                supplierName: data.SupplierName,
                mailTo: data.Email,
                commodity: data.Commodity,
                assessmentOn: new FormControl(data.CompletionDate, Validators.required),
                Assessor: new FormControl(data.Assessor, Validators.required),
                Category_funding: new FormControl(),
                Category_liquidity: new FormControl(),
                Category_profitablity: new FormControl(),
                newAssessments: new FormControl((data.NewAssessment == "True" || data.NewAssessment == "true") ? true : false),
                Reassessments: new FormControl((data.ReAssessment == "True" || data.ReAssessment == "true") ? true : false),
                onlineSelfAssessment: new FormControl((data.SelfAssessment == "True" || data.SelfAssessment == "true") ? true : false),
              });

              dataSource[index].selectCheck.valueChanges.subscribe((res) => {
                console.log(res);
                console.log(dataSource[index]);
                if (res == true) {
                  if (
                    !dataSource[index].assessmentOn.valid 
                    
                  ) {
                    dataSource[index].selectCheck.setValue(false);
                    this.tost.errorFromOtherPage("Kindly Set An Assessment Date");
                    // this.toast.error(
                    //   "Kindly Set An Assessment Date "
                    // );
                  }
                  else if(!dataSource[index].Assessor.valid){
                    dataSource[index].selectCheck.setValue(false);
                    this.tost.error(
                      "Kindly set Assessor"
                    );
                  }
                  //  if(!dataSource[index].Reassessments.value ){
                  //   alert("99")
                  // }
                }
              });
            });
            this.DataSource = [...dataSource];
            resolve(res.ResponseData);
          } else {
            this.toast.error(res.Message);
          }
        });
    });
  }

  exportAsExcelFile(table: any, name: any) {
    let body = {
      status: this.filterForm.value.status || null,
      supplierCode: this.filterForm.value.supplierCode || null,
      supplierName: this.filterForm.value.supplierName || null,
      supplierLocation: this.filterForm.value.location || null,
      commodity: this.filterForm.value.commodity || null,
      PageNo: 0,
      IsExcelDownload: true,
    };
    this.httpService
    .call("admincrudsurvey/GetSRRMSupplierAudit", body)
    .subscribe((res) => {
      console.log(res)
     // this.cs.exportJsonAsExcelFile(res.ResponseData, this.filterForm.value.status)
     let dataNew = res.ResponseData;
     let dataSourceTemp=[];
     for (const data of dataNew) {
      dataSourceTemp.push({
        supplierCode: data.SupplierCode,
        supplierName: data.SupplierName,
        Email: data.Email,
        Commodity: data.Commodity,
             
      });
    }
     //Create workbook and worksheet
     let workbook = new Workbook();
     let worksheet = workbook.addWorksheet('SheduleAssessment');
      //Add Header Row
      let header: any 
     //  if(this.CategoryName == 'Retro Debit Note' || this.CategoryName == 'Retro Credit Note'){
     //    header = Object.keys(dataNew[0])
     //  } else {
     //    header = this.displayedColumns
     //  }
     header=['SupplierCode',	'SupplierName',	'MailTo',	'Commodity'];
     let headerRow = worksheet.addRow(header);
       // Cell Style : Fill and Border
   headerRow.eachCell((cell, number) => {
    cell.fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: '00FF0000' },
      bgColor: { argb: '00FF0000' }
    }
    cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
   })
   dataSourceTemp.forEach(d => {
    var values = Object.keys(d).map(function (key) { return d[key]; });
    console.log(values);
   //  if(this.CategoryName == 'LR & RA')
    // values.splice(12,1);
    // values.splice(13,1);
   //  if(this.CategoryName == 'VQI')
   //  [values[0], values[1]] = [values[1], values[0]];
     let row = worksheet.addRow(values);
   });
    workbook.xlsx.writeBuffer().then((dataSourceTemp) => {
      let blob = new Blob([dataSourceTemp], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob,'SRMM-YOYAnalysis.xlsx');
     
   });
    });

    // const temp_displayedColumns = _.cloneDeep(this.displayedColumns);
    // this.displayedColumns.splice(0, 1);
    // this.search(0).then(
    //   (data: any[]) => {
    //     console.log("Got Response");
    //     // this.cs.exportAsExcelFile(table, name);

    //     this.cs.exportJsonAsExcelFile(data, name);
    //     setTimeout(() => {
    //       this.search(1);
    //     }, 5000);
    //     this.displayedColumns = temp_displayedColumns;
    //   },
    //   (err) => {
    //     this.toast.error("Some Error Occoured");
    //   }
    // );
  }

  submit() {
    console.log(this.DataSource);
    let body = [];
    // if(this.filterForm.value.commodity){

    // }else{
    //   this.cs.showError('')
    // }
    for (let mapping of this.DataSource) {
      if (mapping.selectCheck.value == true) {
        var category = "";
        if (mapping.Category_funding.value == true) {
          category = category.concat("Funding,");
        }
        if (mapping.Category_liquidity.value == true) {
          category = category.concat(" Liquidity,");
        }
        if (mapping.Category_profitablity.value == true) {
          category = category.concat(" Profitablity");
        }
        let Date   = moment(mapping.assessmentOn.value).format('YYYY-MM-DD');
        body.push({
          supplierCode: mapping.supplierCode,
          assessor: mapping.Assessor.value,
          scheduledAuditDate: Date,
          createdBy: this.username, // localStorage.getItem("userToken"),
          oldSchID: mapping.SchID,
          mode: "Planned",
          newAssessment: mapping.newAssessments.value,
          reAssessment: mapping.Reassessments.value,
          selfAssessment: mapping.onlineSelfAssessment.value,
          category: category,
          hdfAuditId: mapping.AuditId || 0,
        });
      }
    }

    console.log(body);
    this.httpService
      .call("admincrudsurvey/AddSRMMSupplier", body)
      .subscribe((res) => {
        console.log(res);
        if (res.Message == "Succcess") {
          this.search(1);
          this.DataSource = [];
          this.toast.success("Scheduled Successfully");
        }
      });
  }

  update(element) {
    console.log(element);
    // admincrudsurvey/UpdateSRMMSupplier

    let body = {
      supplierCode: element.supplierCode,
      assessor: element.Assessor.value,
      scheduledAuditDate: moment(element.assessmentOn.value),
      mode: "Planned",
      createdBy: this.username, // localStorage.getItem("userToken"),
      Commodity: element.commodity,
    };
    this.httpService
      .call("admincrudsurvey/UpdateSRMMSupplier", body)
      .subscribe((res) => {
        console.log(res);
        if (res.Message == "Succcess") {
          this.search(1);
          this.toast.success("Schedule Updated Successfully");
        }
      });
  }
   // schedule mass assessment
   banner_name = "Schedule mass assessment";
   excel_headers = [];
   Excel_File: any = [];
   open(content) {
    this.modalService.open(content, {  size: 'xl' as 'lg',centered:true, windowClass : "formModal"});
  }
  closeModal(){
    this.modalService.dismissAll();
  }
  detectFiles(evt,form) {
    form.resetForm();
    // if(id==1)
    this.excel_headers=["SupplierCode","Assessor","ScheduledAuditDate","SchID","Category","DbCommodity","Supplier_Name"];
    // else
    // this.excel_headers=["Role","Menu"];
    let target: any = <any>(evt.target);
    var headers = [];
    for (let file of target.files) {
      let ext = file.name.split(".").pop().toLowerCase();
      let reader = new FileReader();
      reader.onload = (e: any) => {
        if (ext != 'xlsx') {
          this.cs.showError("Kindly select file with extension '.xlsx");
          return false;
        }
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        var range = XLSX.utils.decode_range(ws['!ref']);
        var C, R = range.s.r;
        let excel_headers = this.excel_headers;
        excel_headers = excel_headers.map(v => v.toLowerCase().replace(/ /g, ""));
        for (C = range.s.c; C <= range.e.c; ++C) {
          var cell = ws[XLSX.utils.encode_cell({ c: C, r: R })]
          var hdr = "UNKNOWN " + C;
          if (cell && cell.t && excel_headers.includes(cell.v.toLowerCase().replace(/ /g, "")))
            hdr = XLSX.utils.format_cell(cell);
          else {
            this.cs.showError("Kindly refer/used sample template and try to upload")
            return false;
          }
          // headers.push(hdr.toLowerCase().replace(/ /g, ""));
          headers.push(hdr);
        }
        this.Upload(target.files[0], evt,headers,form)
      }
      reader.readAsBinaryString(target.files[0]);
    }
  }

  Upload(data, evt,headers,form) {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      let arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
      let array = [];
      array = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      if (array.length == 0)
        return this.cs.showError("Kindly fill data in excel");
      else
        this.validateExcel(array, evt,headers,form);
    }
    fileReader.readAsArrayBuffer(data);
  }

  validateExcel(array, evt,headers,form) {
    for (let i = 0; i < this.excel_headers.length; i++) {
      for (let j = 0; j < array.length; j++) {
        if (this.cs.isUndefinedORNull(array[j][headers[i]])) {
          this.cs.showError("Please enter values under " + this.excel_headers[i] + " at line number " + (j + 2));
          return false;
        }
        // if(!this.cs.isUndefinedORNull(array[j][headers[i]])&&headers[i]=="Mobile Number"){
        //   if(array[j][headers[i]].includes(/^[^a-zA-Z]$/)){
        //     alert("Please enter only numbers under " + this.excel_headers[i] + " at line number " + (j + 2));
        //     return false;
        //   }
        // }
      }
    }
    this.Excel_File = [];
    form.resetForm();
     
    // form.get('Bulk_Upload').setValue(evt.target.files[0].name);
    form.control.get('Bulk_Upload').setValue(evt.target.files[0].name)
    this.Excel_File.push(evt.target.files[0]);
  }

  InsertRoleData(form: any,closeModalonclick) {
    let formData;
    let url;
    // if (!this.Role_Master_Form.valid && !this.uploaded_by_Excel) {
    //   this.validateAllFormFields(this.Role_Master_Form);
    //   return;
    // }

    // else if (this.uploaded_by_Form && !this.uploaded_by_Excel){
    //   url='CRUDMasterRole/InsertMasterRole';
    //   // alert("Added by Form");
    //   formData = {
    //     "RoleName":this.Role_Master_Form.controls.Add_Role.value,
    //     "RoleDescription":this.Role_Master_Form.controls.Role_Description.value,
    //     "CreatedBy":this.username// data from login api
    //   }
    // }
   // else if (!this.uploaded_by_Form && this.uploaded_by_Excel) { 
      url='SRMSSO/BulkUploadScheduleAudit';   
      // alert("Added by Excel");
      formData = new FormData;
      formData.append('ExcelFile', this.Excel_File[0]);
      formData.append('CreatedBy', this.username);// need to add from login api
      // formData.append('VendorCode', "DM181");
    // }// need to add from login api
    // else {
    //   return;
    // }
      this.cs.postMySuggestionData(url, formData).
        subscribe(
          (res: any) => {
            this.cs.showSwapLoader = false;
            this.cs.showSuccess(res.Message);
            if(closeModalonclick)
            closeModalonclick._elementRef.nativeElement.click();
            form.resetForm();
            // this.uploaded_by_Excel=false;
            // this.getAllActiveRolesMaster();
          },
          (error) => {
            this.cs.showSwapLoader = false;
            console.log(error.message);
          }
        )
  }
  DownloadSampleTemplate(filename:any){
    window.open(`${'../../assets/'}${filename}${'.xlsx'}`, '_blank');
    // window.open('../../assets/Navigation Sample.xlsx')
  }
}
