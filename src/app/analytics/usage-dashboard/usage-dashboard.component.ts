import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { CommonUtilityService } from 'src/app/services/common/common-utility.service';
/* Imports */
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { Router } from '@angular/router';
import { MySuggestionService } from 'src/app/services/MySuggestion/my-suggestion.service';


@Component({
  selector: 'app-usage-dashboard',
  templateUrl: './usage-dashboard.component.html',
  styleUrls: ['./usage-dashboard.component.scss']
})
export class UsageDashboardComponent implements OnInit {
  statusData: any;
  columnChart: any;
  chart: any;
  chart1: any;
  chart2: any;
  chart3: any;
  testArr = [];
  testArr1 = [];
  testArr2 = [];
  testArr3 = [];
  MySyCompleted = [];
  SSISYCompleted = [];
  SFASCompleted = [];
  BSCRPCompleted = [];
  T5Transactions = []
  T5Reports = []

  constructor(public commonService: CommonUtilityService,
    public cs: MySuggestionService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private router: Router) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon('arrowIcon', domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/cmsDashboard/Admin_Iconarrow.svg'));
    matIconRegistry.addSvgIcon('boxarrowright', domSanitizer.bypassSecurityTrustResourceUrl('../assets/img/usageAnalytics/boxarrowright.svg'));
  }
  ngOnInit() {
    this.getStatusData();
    this.getTopFiveReports();
    this.getTopFiveTransactions();

    // Create chart instance
    // this.chart1 = am4core.create("chartdiv1", am4charts.PieChart);
    // // Add and configure Series
    // this.chart1.logo.disabled = true;
    // let pieSeries1 = this.chart1.series.push(new am4charts.PieSeries());
    // pieSeries1.dataFields.value = "litres";
    // pieSeries1.dataFields.category = "country";
    // pieSeries1.labels.template.disabled = true;

    // let label1 = pieSeries1.createChild(am4core.Label);
    // label1.text = "{values.value.sum}%";
    // label1.horizontalCenter = "middle";
    // label1.verticalCenter = "middle";
    // label1.fontSize = 15;
    // label1.fill = am4core.color("#87959F");
    // // pieSeries.slices.template.tooltip.fontSize= 15;

    // // Let's cut a hole in our Pie chart the size of 30% the radius
    // this.chart1.innerRadius = am4core.percent(70);

    // // Put a thick white border around each Slice
    // pieSeries1.slices.template.stroke = am4core.color("#fff");
    // pieSeries1.slices.template.strokeWidth = 2;
    // pieSeries1.slices.template.strokeOpacity = 1;
    // pieSeries1.slices.template
    //   // change the cursor on hover to make it apparent the object can be interacted with
    //   .cursorOverStyle = [
    //     {
    //       "property": "cursor",
    //       "value": "pointer"
    //     }
    //   ];

    // pieSeries1.alignLabels = false;
    // pieSeries1.labels.template.bent = true;
    // pieSeries1.labels.template.radius = 3;
    // pieSeries1.labels.template.padding(0, 0, 0, 0);

    // pieSeries1.ticks.template.disabled = true;

    // // Create a base filter effect (as if it's not there) for the hover to return to
    // let shadow1 = pieSeries1.slices.template.filters.push(new am4core.DropShadowFilter);
    // shadow1.opacity = 0;

    // // Create hover state
    // let hoverState1 = pieSeries1.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

    // // Slightly shift the shadow and make it more prominent on hover
    // let hoverShadow1 = hoverState1.filters.push(new am4core.DropShadowFilter);
    // hoverShadow1.opacity = 0.7;
    // hoverShadow1.blur = 5;

    // // Add a legend
    // this.chart1.legend = new am4charts.Legend();
    // this.chart1.legend.useDefaultMarker = true;
    // let marker1 = this.chart1.legend.markers.template.children.getIndex(0);
    // //  marker.cornerRadius(12, 12, 12, 12);
    // marker1.strokeWidth = 2;
    // marker1.strokeOpacity = 1;
    // let markerTemplate1 = this.chart1.legend.markers.template;
    // markerTemplate1.width = 13;
    // markerTemplate1.height = 13;
    // marker1.stroke = am4core.color("#ccc");
    // this.chart1.legend.position = "left";
    // this.chart1.legend.maxHeight = 150;
    // this.chart1.legend.maxWidth = 150;
    // this.chart1.legend.fontSize = 13;
    // this.chart1.legend.labels.template.fill = am4core.color("#AFAFAF");
    // pieSeries1.colors.list = [
    //   am4core.color("#0EB1D8"),
    //   am4core.color("#EDBD01"),
    //   am4core.color("#DFE6E8"),
    // ];
    // //-------------------------------------------------------------------------------------
    // // Create chart instance
    // this.chart2 = am4core.create("chartdiv2", am4charts.PieChart);
    // // Add and configure Series
    // this.chart2.logo.disabled = true;
    // let pieSeries2 = this.chart2.series.push(new am4charts.PieSeries());
    // pieSeries2.dataFields.value = "litres";
    // pieSeries2.dataFields.category = "country";
    // pieSeries2.labels.template.disabled = true;

    // let label2 = pieSeries1.createChild(am4core.Label);
    // label2.text = "{values.value.sum}%";
    // label2.horizontalCenter = "middle";
    // label2.verticalCenter = "middle";
    // label2.fontSize = 15;
    // label2.fill = am4core.color("#87959F");
    // // pieSeries.slices.template.tooltip.fontSize= 15;

    // // Let's cut a hole in our Pie chart the size of 30% the radius
    // this.chart2.innerRadius = am4core.percent(70);

    // // Put a thick white border around each Slice
    // pieSeries2.slices.template.stroke = am4core.color("#fff");
    // pieSeries2.slices.template.strokeWidth = 2;
    // pieSeries2.slices.template.strokeOpacity = 1;
    // pieSeries2.slices.template
    //   // change the cursor on hover to make it apparent the object can be interacted with
    //   .cursorOverStyle = [
    //     {
    //       "property": "cursor",
    //       "value": "pointer"
    //     }
    //   ];

    // pieSeries2.alignLabels = false;
    // pieSeries2.labels.template.bent = true;
    // pieSeries2.labels.template.radius = 3;
    // pieSeries2.labels.template.padding(0, 0, 0, 0);
    // pieSeries2.ticks.template.disabled = true;

    // // Create a base filter effect (as if it's not there) for the hover to return to
    // let shadow2 = pieSeries2.slices.template.filters.push(new am4core.DropShadowFilter);
    // shadow1.opacity = 0;

    // // Create hover state
    // let hoverState2 = pieSeries2.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

    // // Slightly shift the shadow and make it more prominent on hover
    // let hoverShadow2 = hoverState2.filters.push(new am4core.DropShadowFilter);
    // hoverShadow2.opacity = 0.7;
    // hoverShadow2.blur = 5;

    // // Add a legend
    // this.chart2.legend = new am4charts.Legend();
    // this.chart2.legend.useDefaultMarker = true;
    // let marker2 = this.chart2.legend.markers.template.children.getIndex(0);
    // //  marker.cornerRadius(12, 12, 12, 12);
    // marker2.strokeWidth = 2;
    // marker2.strokeOpacity = 1;
    // let markerTemplate2 = this.chart1.legend.markers.template;
    // markerTemplate2.width = 13;
    // markerTemplate2.height = 13;
    // marker2.stroke = am4core.color("#ccc");
    // this.chart2.legend.position = "left";
    // this.chart2.legend.maxHeight = 150;
    // this.chart2.legend.maxWidth = 150;
    // this.chart2.legend.fontSize = 13;
    // this.chart2.legend.labels.template.fill = am4core.color("#AFAFAF");
    // pieSeries2.colors.list = [
    //   am4core.color("#0EB1D8"),
    //   am4core.color("#EDBD01"),
    //   am4core.color("#DFE6E8"),
    // ];

    // //-------------------------------------------------------------------------------------
    // // Create chart instance
    // this.chart3 = am4core.create("chartdiv3", am4charts.PieChart);
    // // Add and configure Series
    // this.chart3.logo.disabled = true;
    // let pieSeries3 = this.chart3.series.push(new am4charts.PieSeries());
    // pieSeries3.dataFields.value = "litres";
    // pieSeries3.dataFields.category = "country";
    // pieSeries3.labels.template.disabled = true;

    // let label3 = pieSeries3.createChild(am4core.Label);
    // label3.text = "{values.value.sum}%";
    // label3.horizontalCenter = "middle";
    // label3.verticalCenter = "middle";
    // label3.fontSize = 15;
    // label3.fill = am4core.color("#87959F");
    // // pieSeries.slices.template.tooltip.fontSize= 15;

    // // Let's cut a hole in our Pie chart the size of 30% the radius
    // this.chart3.innerRadius = am4core.percent(70);

    // // Put a thick white border around each Slice
    // pieSeries3.slices.template.stroke = am4core.color("#fff");
    // pieSeries3.slices.template.strokeWidth = 2;
    // pieSeries3.slices.template.strokeOpacity = 1;
    // pieSeries3.slices.template
    //   // change the cursor on hover to make it apparent the object can be interacted with
    //   .cursorOverStyle = [
    //     {
    //       "property": "cursor",
    //       "value": "pointer"
    //     }
    //   ];

    // pieSeries3.alignLabels = false;
    // pieSeries3.labels.template.bent = true;
    // pieSeries3.labels.template.radius = 3;
    // pieSeries3.labels.template.padding(0, 0, 0, 0);
    // pieSeries3.ticks.template.disabled = true;

    // // Create a base filter effect (as if it's not there) for the hover to return to
    // let shadow3 = pieSeries3.slices.template.filters.push(new am4core.DropShadowFilter);
    // shadow3.opacity = 0;

    // // Create hover state
    // let hoverState3 = pieSeries3.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

    // // Slightly shift the shadow and make it more prominent on hover
    // let hoverShadow3 = hoverState3.filters.push(new am4core.DropShadowFilter);
    // hoverShadow3.opacity = 0.7;
    // hoverShadow3.blur = 5;

    // // Add a legend
    // this.chart3.legend = new am4charts.Legend();
    // this.chart3.legend.useDefaultMarker = true;
    // let marker3 = this.chart3.legend.markers.template.children.getIndex(0);
    // //  marker.cornerRadius(12, 12, 12, 12);
    // marker3.strokeWidth = 2;
    // marker3.strokeOpacity = 1;
    // let markerTemplate3 = this.chart1.legend.markers.template;
    // markerTemplate3.width = 13;
    // markerTemplate3.height = 13;
    // marker3.stroke = am4core.color("#ccc");
    // this.chart3.legend.position = "left";
    // this.chart3.legend.maxHeight = 150;
    // this.chart3.legend.maxWidth = 150;
    // this.chart3.legend.fontSize = 13;
    // this.chart3.legend.labels.template.fill = am4core.color("#AFAFAF");
    // pieSeries3.colors.list = [
    //   am4core.color("#0EB1D8"),
    //   am4core.color("#EDBD01"),
    //   am4core.color("#DFE6E8"),
    // ];






    //column chart
    // Create chart instance
    this.columnChart = am4core.create("columnChartdiv", am4charts.XYChart);
    this.columnChart.logo.disabled = true;
    // Create axes

    let categoryAxis = this.columnChart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "HRS";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;



    let valueAxis = this.columnChart.yAxes.push(new am4charts.ValueAxis());

    // Create series
    let series = this.columnChart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "HRSCNT";
    series.dataFields.categoryX = "HRS";
    series.name = "Visits";
    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
    series.columns.template.fillOpacity = 1;
    series.stroke = am4core.color("#9CD2AB"); // red
    series.columns.template.fill = am4core.color("#9CD2AB");
    series.strokeWidth = 3; // 3px
    series.columns.template.width = 4;

    let columnTemplate = series.columns.template;
    columnTemplate.strokeWidth = 1;
    columnTemplate.strokeOpacity = 1;

    // this.columnChart.scrollbarY = new am4core.Scrollbar();
    // this.columnChart.scrollbarX = new am4core.Scrollbar();

    //chart.scrollbarY = new am4core.Scrollbar();
    //this.columnChart.scrollbarX = new am4core.Scrollbar();
  }

  openTotalUsers() {
    this.router.navigateByUrl('total-users');
  }
  noUsersLogged() {
    this.router.navigateByUrl('not-logged-in-users');
  }
  openReports() {
    this.router.navigateByUrl('top-five-reports');
  }
  openTransaction() {
    this.router.navigateByUrl('top-five-transaction');
  }
  openUniqueVisitors() {
    this.router.navigateByUrl('unique-visitors');
  }
  openanalyticsreports(){
    this.router.navigateByUrl('analytics-reports');
  }

  BrowserDetails=[];

  getStatusData() {
    this.cs.postMySuggestionData('PerformanceRating/GetClickDashboard', '').subscribe((res: any) => {
      this.statusData = res.ResponseData;
      console.log(JSON.stringify(this.statusData));
      this.columnChart.data = this.statusData.TimeDetails;
      this.MySyCompleted = res.ResponseData.MySyCompletedDetails;
      this.SSISYCompleted = this.statusData.SSISYCompletedDetails;
      this.SFASCompleted = this.statusData.SFASCompletedDetails;
      this.BSCRPCompleted = this.statusData.BSCRPCompletedDetails;
      this.BrowserDetails = this.statusData.BrowserDetails

      // this.createMySurveyCharts(res.ResponseData.MySyCompletedDetails)
      // console.log(this.chart.data);

      for (let i = 0; i < this.SSISYCompleted.length; i++) {
        for (let j = 0; j <= this.SSISYCompleted.length; j++) {
          this.testArr1.push({
            "country": Object.keys(this.SSISYCompleted[i])[j],
            "litres": Object.values(this.SSISYCompleted[i])[j]
          })
        }
      }
      this.chart1.data = this.testArr1;

      for (let i = 0; i < this.SFASCompleted.length; i++) {
        for (let j = 0; j <= this.SFASCompleted.length; j++) {
          this.testArr2.push({
            "country": Object.keys(this.SFASCompleted[i])[j],
            "litres": Object.values(this.SFASCompleted[i])[j]
          })
        }
      }
      this.chart2.data = this.testArr2;

      for (let i = 0; i < this.BSCRPCompleted.length; i++) {
        for (let j = 0; j <= this.BSCRPCompleted.length; j++) {
          this.testArr3.push({
            "country": Object.keys(this.BSCRPCompleted[i])[j],
            "litres": Object.values(this.BSCRPCompleted[i])[j]
          })
        }
      }
      this.chart3.data = this.testArr3;
    })
  }
  getTopFiveTransactions(){
    this.cs.postMySuggestionData('Analytics/GetTopFiveClicks', {Type: "T"}).subscribe(res=>{
      this.T5Transactions = res.ResponseData
    })
  }
  getTopFiveReports(){
    this.cs.postMySuggestionData('Analytics/GetTopFiveClicks', {Type: "R"}).subscribe(res=>{
      this.T5Reports = res.ResponseData
    })
  }

  // createMySurveyCharts(mySurveyCharts){
  //   // Create chart instance
  //   this.chart = am4core.create("chartdiv", am4charts.PieChart);
  //   this.chart.logo.disabled = true;

  //   // Add and configure Series
  //   let pieSeries = this.chart.series.push(new am4charts.PieSeries());
  //   pieSeries.dataFields.value = "litres";
  //   pieSeries.dataFields.category = "country";
  //   pieSeries.labels.template.disabled = true;

  //   let label = pieSeries.createChild(am4core.Label);
  //   label.text = "{values.value.sum}%";
  //   label.horizontalCenter = "middle";
  //   label.verticalCenter = "middle";
  //   label.fontSize = 15;
  //   label.fill = am4core.color("#87959F");
  //   // pieSeries.slices.template.tooltip.fontSize= 15;

  //   // Let's cut a hole in our Pie chart the size of 30% the radius
  //   this.chart.innerRadius = am4core.percent(70);

  //   // Put a thick white border around each Slice
  //   pieSeries.slices.template.stroke = am4core.color("#fff");
  //   pieSeries.slices.template.strokeWidth = 2;
  //   pieSeries.slices.template.strokeOpacity = 1;
  //   pieSeries.slices.template
  //     // change the cursor on hover to make it apparent the object can be interacted with
  //     .cursorOverStyle = [
  //       {
  //         "property": "cursor",
  //         "value": "pointer"
  //       }
  //     ];

  //   pieSeries.alignLabels = false;
  //   pieSeries.labels.template.bent = true;
  //   pieSeries.labels.template.radius = 3;
  //   pieSeries.labels.template.padding(0, 0, 0, 0);

  //   pieSeries.ticks.template.disabled = true;

  //   // Create a base filter effect (as if it's not there) for the hover to return to
  //   let shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
  //   shadow.opacity = 0;

  //   // Create hover state
  //   let hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

  //   // Slightly shift the shadow and make it more prominent on hover
  //   let hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
  //   hoverShadow.opacity = 0.7;
  //   hoverShadow.blur = 5;

  //   // Add a legend
  //   this.chart.legend = new am4charts.Legend();
  //   this.chart.legend.useDefaultMarker = true;
  //   let marker = this.chart.legend.markers.template.children.getIndex(0);
  //   //  marker.cornerRadius(12, 12, 12, 12);
  //   marker.strokeWidth = 2;
  //   marker.strokeOpacity = 1;
  //   let markerTemplate = this.chart.legend.markers.template;
  //   markerTemplate.width = 13;
  //   markerTemplate.height = 13;
  //   marker.stroke = am4core.color("#ccc");
  //   this.chart.legend.position = "left";
  //   // this.chart.legend.maxHeight = 150;
  //   // this.chart.legend.maxWidth = 220;
  //   this.chart.legend.maxHeight = 100;
  //   this.chart.legend.maxWidth = 150;
  //   this.chart.legend.fontSize = 13;
  //   this.chart.legend.labels.template.fill = am4core.color("#AFAFAF");
  //   //  chart.legend.labels.template.text.fill = am4core.color("#AFAFAF");

  //   pieSeries.colors.list = [
  //     am4core.color("#0EB1D8"),
  //     am4core.color("#EDBD01"),
  //     am4core.color("#DFE6E8"),
  //   ];

  //   for (let i = 0; i < mySurveyCharts.length; i++) {
  //     for (let j = 1; j <= Object.keys(mySurveyCharts[i]).length; j++) {
  //       this.testArr.push({
  //         "country": Object.keys(mySurveyCharts[i])[j],
  //         "litres": Object.values(mySurveyCharts[i])[j]
  //       })
  //     }
  //   }
  //   this.chart.data = this.testArr;
  // }
}
