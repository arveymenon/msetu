import { Component, OnInit, Input } from '@angular/core';
import { MySuggestionService } from 'src/app/services/MySuggestion/my-suggestion.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-report-details-modal',
  templateUrl: './report-details-modal.component.html',
  styleUrls: ['./report-details-modal.component.sass']
})
export class ReportDetailsModalComponent implements OnInit {
  @Input('data') data: any
  @Input('type') type: any

  DataSource = []
  displayedColumns = ['user','count']
  constructor(private cs: MySuggestionService, private modal: NgbModal) { }

  ngOnInit() {
    console.log("DATA", this.data,"TYPE", this.type)
    if(this.data && this.type){
      this.cs.postMySuggestionData('Analytics/GetTopClicksByModule',{
        "Type": this.type,
        "MODULE": this.data.module
        }).subscribe(async res=>{
          await console.log(res)
          this.DataSource = res.ResponseData
        })
    }else{
      // this.modal.dismissAll()
    }
  }

  closeModal(){
    this.modal.dismissAll()
  }

}
