import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotLoggedInUsersComponent } from './not-logged-in-users.component';

describe('NotLoggedInUsersComponent', () => {
  let component: NotLoggedInUsersComponent;
  let fixture: ComponentFixture<NotLoggedInUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotLoggedInUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotLoggedInUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
