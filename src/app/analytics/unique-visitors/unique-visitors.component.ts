import { Component, OnInit } from "@angular/core";
import { MatIconRegistry, PageEvent } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { CommonUtilityService } from "src/app/services/common/common-utility.service";
import { MyHelpDeskService } from 'src/app/services/myHelpDesk/my-help-desk.service';
import { FormControl } from '@angular/forms';
import { Workbook } from 'exceljs';
import * as fs from "file-saver";

@Component({
  selector: "app-unique-visitors",
  templateUrl: "./unique-visitors.component.html",
  styleUrls: ["./unique-visitors.component.scss"],
})
export class UniqueVisitorsComponent implements OnInit {

  filter = new FormControl('30')
  pageNo = 1
  pageSize = 100

  DataSource = [
  ];
  displayedColumns: string[] = [
    "srNo",
    "userName",
    "userID",
    "vendorCode",
    "location"
  ];

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public commonService: CommonUtilityService,
    public http: MyHelpDeskService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    this.getData(1)
  }

  getData(pageNo){
    let body = { 
      Days: this.filter.value || 30, 
      PageNo: pageNo || 0  
    }
    this.http.call('Analytics/GetUniqueUsers', body).subscribe(res=>{
      if(res.Message == "Success"){
        let dataSource = []
        this.pageSize = res.TotalCount
        res.ResponseData.forEach((data, index)=>{
          dataSource.push({
              srNo: index+1,
              userName: data.FirstName+" "+data.LastName,
              userID: data.USERNAME,
              vendorCode: data.VENDORCODE,
              location: data.OFFICELOCATION
            })
        })
        this.DataSource = [...dataSource]
      }
    })
  }

  ngOnInit() {}
  exportExcel(){
    let body = { 
      Days: this.filter.value || 30, 
      PageNo: 0  
    }
    this.http.call('Analytics/GetUniqueUsers', body).subscribe(async res=>{
      if(res.Message == "Success"){
        let dataSource = []
        res.ResponseData.forEach((data, index)=>{
          dataSource.push({
              srNo: index+1,
              userName: data.FirstName+" "+data.LastName,
              userID: data.USERNAME,
              vendorCode: data.VENDORCODE,
              location: data.OFFICELOCATION
            })
        })

        let dataNew = dataSource
        // this.DataSource;
        await console.log(dataNew);
        //Create workbook and worksheet
        let workbook = new Workbook();
        let worksheet = workbook.addWorksheet("ReportData");
        let columns = Object.keys(this.DataSource[0]);
        let headerRow = worksheet.addRow(columns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: "00FF0000" },
            bgColor: { argb: "00FF0000" },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
        });
        dataNew.forEach((d) => {
          // var obj = JSON.parse(d);
          var values = Object.keys(d).map(function (key) {
            return d[key];
          });
          let row = worksheet.addRow(values);
        });
        workbook.xlsx.writeBuffer().then((dataNew) => {
          let blob = new Blob([dataNew], {
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          fs.saveAs(
            blob,
            "Unique Visitors.xlsx"
          );
        });
      }
    })
  }
}
