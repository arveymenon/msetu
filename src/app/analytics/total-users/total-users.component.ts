import { Component, OnInit } from "@angular/core";
import { MatIconRegistry, PageEvent } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { CommonUtilityService } from "src/app/services/common/common-utility.service";
import { FormControl } from "@angular/forms";
import { MyHelpDeskService } from 'src/app/services/myHelpDesk/my-help-desk.service';

@Component({
  selector: "app-total-users",
  templateUrl: "./total-users.component.html",
  styleUrls: ["./total-users.component.scss"],
})
export class TotalUsersComponent implements OnInit {
  filter = new FormControl('30');

  pageNo = 1
  pageSize = 100
  // roleId = CryptoJS.AES.decrypt(
  //   localStorage.getItem("WTIwNWMxcFZiR3M9"),
  //   ""
  // ).toString(CryptoJS.enc.Utf8);

  DataSource = [];
  displayedColumns: string[] = [
    "srNo",
    "userName",
    "userID",
    "vendorCode",
    "location"
  ];

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public commonService: CommonUtilityService,
    public http: MyHelpDeskService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    this.getData(1)
  }

  getData(pageNo) {
    let body = { 
      Days: this.filter.value || 30,
      PageNo: pageNo || 0 
    };
    this.http.call("Analytics/GetTotalUsers", body).subscribe((res) => {
      if (res.Message == "Success") {
        let dataSource = [];
        res.ResponseData.forEach((data, index) => {
          dataSource.push({
            srNo: index+1,
            userName: data.FirstName + " " + data.LastName,
            userID: data.USERNAME,
            vendorCode: data.VENDORCODE,
            location: data.OFFICELOCATION,
            action: index,
          });
        });
        this.DataSource = [...dataSource];
      }
    });
  }

  ngOnInit() {}
}
