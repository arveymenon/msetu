import { Component, OnInit } from "@angular/core";
import { MatIconRegistry, PageEvent } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { NgbModal, NgbModalRef, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { CommonUtilityService } from "src/app/services/common/common-utility.service";
import { FormControl } from "@angular/forms";
import { MyHelpDeskService } from 'src/app/services/myHelpDesk/my-help-desk.service';
import { ReportDetailsModalComponent } from './../modals/report-details-modal/report-details-modal.component';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';


@Component({
  selector: "app-top-five-reports",
  templateUrl: "./top-five-reports.component.html",
  styleUrls: ["./top-five-reports.component.scss"],
})
export class TopFiveReportsComponent implements OnInit {
  filter = new FormControl("R");

  pageNo = 0
  pageSize = 100
  DataSource = [];
  displayedColumns: string[] = ["srNo", "reportName", "action"];

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public commonService: CommonUtilityService,
    public http: MyHelpDeskService,
    public modal: NgbModal
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );

    this.getData();
  }

  ngOnInit() {}

  viewReportsdataModal(data) {
    var modalRef = this.modal.open(ReportDetailsModalComponent, { size: 'xl' as "lg" });
    modalRef.componentInstance.data = data
    modalRef.componentInstance.type = 'R'
    // modalRef.componentInstance = data.VendorCode;
  }

  getData(){
    let body = { Type: this.filter.value || "R"}
    this.http.call('Analytics/GetTopFiveClicks', body).subscribe(res=>{
      let dataSource = []
      this.DataSource = []

      if(res.Message == 'Success'){
        res.ResponseData.forEach((data, index)=>{
          dataSource.push({
            srNo: index+1,
            reportName: data.MODULE,
            module: data.MODULE
          })
        })

        this.DataSource = [...dataSource]
      }
    })
  }

  exportExcel(){
    let json = this.DataSource
    let dataNew = json;
    console.log(dataNew);
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet("ReportData");
    let columns = Object.keys(json[0]);
    columns.splice(2,1)
    let headerRow = worksheet.addRow(columns);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "00FF0000" },
        bgColor: { argb: "00FF0000" },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
    });
    dataNew.forEach((d) => {
      // var obj = JSON.parse(d);
      var values = Object.keys(d).map(function (key) {
        return d[key];
      });
      values.splice(2,1)
      let row = worksheet.addRow(values);
    });
    workbook.xlsx.writeBuffer().then((dataNew) => {
      let blob = new Blob([dataNew], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(
        blob,
        "TopFiveReports.xlsx"
      );
    });
  }
}
