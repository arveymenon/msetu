import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopFiveReportsComponent } from './top-five-reports.component';

describe('TopFiveReportsComponent', () => {
  let component: TopFiveReportsComponent;
  let fixture: ComponentFixture<TopFiveReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopFiveReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopFiveReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
