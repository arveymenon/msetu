import { Component, OnInit } from "@angular/core";
import { MatIconRegistry, PageEvent } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { CommonUtilityService } from "src/app/services/common/common-utility.service";
import { FormControl } from "@angular/forms";
import { MyHelpDeskService } from 'src/app/services/myHelpDesk/my-help-desk.service';
import { FormBuilder } from '@angular/forms';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import * as fs from "file-saver";
import * as CryptoJS from 'crypto-js';
import * as moment from 'moment';
import { Workbook } from 'exceljs';
import { MySuggestionService } from '../../services/MySuggestion/my-suggestion.service';

@Component({
  selector: 'app-analytics-reports',
  templateUrl: './analytics-reports.component.html',
  styleUrls: ['./analytics-reports.component.scss']
})
export class AnalyticsReportsComponent implements OnInit {
  displayedColumns: string[] = ['srNo', 'UserName', 'UserID', 'VendorCode', 'MODULE', 'DEVICE', 'BROWSER', 'CLICKTIME'];
  filter = new FormControl('30');
  filterForm: any;
  platFormData: any = [];
  isDisabled: boolean = true
  filterPageNo: any;
  isAdmin: boolean;
  RoleID: any;
  vendorCode: string;
  usename: string;
  pageNo = 1
  pageSize = 100
  DataSource = [];

  constructor(public activatedRoute: ActivatedRoute, public router: Router, private _formBuilder: FormBuilder, public commonService: CommonUtilityService,
    public cs: MySuggestionService, private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer, public http: MyHelpDeskService) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Excel_White_Icon.svg"
      )
    );
    // this.getData(1)
  }

  //  getData(pageNo) {
  //   let body = { PageNo: pageNo };
  //   this.http.call("Analytics/GetAnalyticsReport", body).subscribe(async (res) => {
  //     if (res.Message == "Success") {
  //       let dataSource = [];
  //       res.ResponseData.forEach((data, index) => {
  //         dataSource.push({
  //           srNo: index+1,
  //           UserName: data.UserName,
  //           UserID: data.UserID,
  //           MODULE: data.MODULE,
  //           DEVICE: data.DEVICE,
  //           BROWSER: data.BROWSER,
  //           CLICKTIME: data.CLICKTIME, 
  //            action: index,
  //         });
  //       });
  //       await console.log(dataSource)
  //       this.DataSource = [...dataSource];
  //       this.pageSize = res.TotalCount;
  //     }
  //   });
  // }


  ngOnInit() {
    this.vendorCode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
    this.vendorCode = CryptoJS.AES.decrypt(this.vendorCode, "").toString(CryptoJS.enc.Utf8);
    this.RoleID = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(CryptoJS.enc.Utf8);
    this.usename = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
    this.usename = CryptoJS.AES.decrypt(this.usename, "").toString(CryptoJS.enc.Utf8);
    this.createFilterForm();
    this.getreportsData(1)
    // this.getData(1)
    // this.getreportsData(0,0,reportsDatafalse);
  }
  reset() {
    this.filterForm.reset();
    this.getreportsData(1)
    // this.getData(1)
  }


  async getreportsData(PageNo: any) {
    // async getreportsData(PageNo:any){
    let url = 'Analytics/GetAnalyticsReport';
    let postData =
      {
        "PageNo": PageNo,
        "vendorCode": this.filterForm.controls.vendor_code.value,
        "userID": this.filterForm.controls.user_id.value,
        "module": this.filterForm.controls.module.value,
        "application": this.filterForm.controls.application.value,
        "fromDate": this.filterForm.controls.from_date.value ? moment(this.filterForm.controls.from_date.value).format('YYYY-MM-DD HH:mm:ss') : "",
        "toDate": this.filterForm.controls.to_date.value ? moment(this.filterForm.controls.to_date.value).format('YYYY-MM-DD HH:mm:ss') : "",
      }
    await this.cs.postMySuggestionData(url, postData).subscribe((res: any) => {
      this.DataSource = res.ResponseData;
      this.pageSize = res.TotalCount;
      // this.DataSource = [...this.reportsData];


      if (this.DataSource.length > 0)
        this.isDisabled = false;
    })
  }

  createFilterForm() {
    this.filterForm = this._formBuilder.group({
      vendor_code: [''],
      user_id: [''],
      module: [''],
      application: [''],
      from_date: [''],
      to_date: [''],
    });
  }

  exportExcel(JSON) {
    let body = 
    // {
    //   PageNo: 0
    // }
    {
      "PageNo": 0,
      "vendorCode": this.filterForm.controls.vendor_code.value,
      "userID": this.filterForm.controls.user_id.value,
      "module": this.filterForm.controls.module.value,
      "application": this.filterForm.controls.application.value,
      "fromDate": this.filterForm.controls.from_date.value ? moment(this.filterForm.controls.from_date.value).format('YYYY-MM-DD HH:mm:ss') : "",
      "toDate": this.filterForm.controls.to_date.value ? moment(this.filterForm.controls.to_date.value).format('YYYY-MM-DD HH:mm:ss') : "",
    }
    this.http.call("Analytics/GetAnalyticsReport", body).subscribe(async (res) => {
      if (res.Message == "Success") {
        let dataSource = []
        res.ResponseData.forEach((data, index) => {
          dataSource.push({
            srNo: index + 1,
            UserName: data.UserName,
            UserID: data.UserID,
            MODULE: data.MODULE,
            DEVICE: data.DEVICE,
            BROWSER: data.BROWSER,
            CLICKTIME: data.CLICKTIME
          })
        })

        let dataNew = dataSource
        // this.DataSource;
        await console.log(dataNew);
        //Create workbook and worksheet
        let workbook = new Workbook();
        let worksheet = workbook.addWorksheet("ReportData");
        let columns = Object.keys(this.DataSource[0]);
        let headerRow = worksheet.addRow(columns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: "00FF0000" },
            bgColor: { argb: "00FF0000" },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
        });
        dataNew.forEach((d) => {
          // var obj = JSON.parse(d);
          var values = Object.keys(d).map(function (key) {
            return d[key];
          });
          let row = worksheet.addRow(values);
        });
        workbook.xlsx.writeBuffer().then((dataNew) => {
          let blob = new Blob([dataNew], {
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          fs.saveAs(
            blob,
            "AnalyticsReport.xlsx"
          );
        });
      }
    })
  }
}
