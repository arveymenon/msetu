import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UsageDashboardComponent } from './usage-dashboard/usage-dashboard.component';
import { NotLoggedInUsersComponent } from './not-logged-in-users/not-logged-in-users.component';
import { TopFiveReportsComponent } from './top-five-reports/top-five-reports.component';
import { TopFiveTransactionComponent } from './top-five-transaction/top-five-transaction.component';
import { TotalUsersComponent } from './total-users/total-users.component';
import { UniqueVisitorsComponent } from './unique-visitors/unique-visitors.component';
import { AnalyticsReportsComponent } from './analytics-reports/analytics-reports.component';




// TESGIN
const routes: Routes = [
  {
    path: '',
    redirectTo: 'usage-dashboard'
  },
  {
    path: 'usage-dashboard',
    component: UsageDashboardComponent
  },
  {
    path: 'not-logged-in-users',
    component: NotLoggedInUsersComponent
  },
  {
    path: 'top-five-reports',
    component: TopFiveReportsComponent
  },
  {
    path: 'top-five-transaction',
    component: TopFiveTransactionComponent
  },
  {
    path: 'total-users',
    component: TotalUsersComponent
  },
  {
    path: 'unique-visitors',
    component: UniqueVisitorsComponent
  },
  {
    path: 'analytics-reports',
    component: AnalyticsReportsComponent
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class AnalyticsRoutingModule { }
