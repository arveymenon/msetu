import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsageDashboardComponent } from './usage-dashboard/usage-dashboard.component';
import { NotLoggedInUsersComponent } from './not-logged-in-users/not-logged-in-users.component';
import { TopFiveReportsComponent } from './top-five-reports/top-five-reports.component';
import { TopFiveTransactionComponent } from './top-five-transaction/top-five-transaction.component';
import { TotalUsersComponent } from './total-users/total-users.component';
import { UniqueVisitorsComponent } from './unique-visitors/unique-visitors.component';
import { AnalyticsRoutingModule } from './analytics-routing.module';
import { MaterialModule } from 'src/material.module';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatFormFieldModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { AnalyticsReportsComponent } from './analytics-reports/analytics-reports.component';
import { ReportDetailsModalComponent } from './modals/report-details-modal/report-details-modal.component';


// testing save
@NgModule({
  declarations: [
    UsageDashboardComponent,
    NotLoggedInUsersComponent,
    TopFiveReportsComponent,
    TopFiveTransactionComponent,
    TotalUsersComponent,
    UniqueVisitorsComponent,
    AnalyticsReportsComponent,

    // Modals
    ReportDetailsModalComponent
  ],
  entryComponents:[
    ReportDetailsModalComponent
  ],
  imports: [
    CommonModule,

    AnalyticsRoutingModule,
    MaterialModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    ReactiveFormsModule,
    MatFormFieldModule,
  ]
})
export class AnalyticsModule { }
