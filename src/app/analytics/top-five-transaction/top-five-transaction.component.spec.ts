import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopFiveTransactionComponent } from './top-five-transaction.component';

describe('TopFiveTransactionComponent', () => {
  let component: TopFiveTransactionComponent;
  let fixture: ComponentFixture<TopFiveTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopFiveTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopFiveTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
