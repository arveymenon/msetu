import { HomeComponent } from "./home.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from '../services/auth-guards/auth.guard';

const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
    children: [
      { path: "", redirectTo: "sydney", pathMatch: "full",
      data: { roles: ['Supplier-meet-landing'] }},
      { path: "sydney", loadChildren: "./sydney/sydney.module#SydneyModule" },
      { path: "conference", loadChildren: "./conference/conference.module#ConferenceModule",
      data: { roles: ['Supplier-meet-landing'] }},
      { path: "registration", loadChildren: "./registration/registration.module#RegistrationModule", 
      data: { roles: ['Supplier-meet-landing'] }, },
      { path: "contact", loadChildren: "./contact/contact.module#ContactModule", 
      data: { roles: ['Supplier-meet-landing'] } },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
