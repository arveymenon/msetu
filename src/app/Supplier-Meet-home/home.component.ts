import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { trigger,  transition } from '@angular/animations';
// import { AnimationService } from '../common/animation.service';

// import { slideToLeft, slideToRight,  } from '../common/animations';
import { Subscription } from 'rxjs';
import { slideToLeft, slideToRight } from '../Supplier-Meet-common/animations';
import { AnimationService } from '../Supplier-Meet-common/animation.service';
declare var $: any;
import * as $ from "jquery";
declare var particlesJS: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('routeTransition', [
      transition("* => slideToLeft", slideToLeft),
      transition("* => slideToRight", slideToRight),
      transition("* => slideToLeftDuplicate", slideToLeft),
      transition("* => slideToRightDuplicate", slideToRight),
    ])
  ],
})
export class HomeComponent implements OnInit {
  subscription: Subscription;
  constructor(private ro: Router, private animSRVC: AnimationService) { }

  ngOnInit(): void {
    // $("#menu-toggle").click(function (e) {
    //   e.preventDefault();
    //   $("#wrapper").toggleClass("toggled");
    // });
    // particlesJS.load('particles-js', 'assets/js/particles.json', function() {
    //   console.log('callback - particles.js config loaded');
    // });
    console.log('sm route to home')
  }

  gotoRoute(rou){
    if(rou=='sydney'){
      this.animSRVC.slideToRIGHT();
      this.ro.navigateByUrl(rou);
    }
    else if(rou=='conference'){
      this.animSRVC.slideToLEFT();
        this.ro.navigateByUrl(rou);
    } 
    else if(rou=='registration'){
      this.animSRVC.slideToRIGHT();
         this.ro.navigateByUrl(rou);
    } 
  }
  getAnimation() {
    console.log(this.animSRVC.getCurrentAnimation());
    return this.animSRVC.getCurrentAnimation();
  }
}
