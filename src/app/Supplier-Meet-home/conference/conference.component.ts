import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AnimationService } from 'src/app/Supplier-Meet-common/animation.service';
import { MySuggestionService } from 'src/app/services/MySuggestion/my-suggestion.service';
declare var $: any;
@Component({
  selector: 'app-conference',
  templateUrl: './conference.component.html',
  styleUrls: ['./conference.component.scss']
})
export class ConferenceComponent implements OnInit {
  howToReachData=[];
  isHowToReach: boolean=false;
  isHotel:boolean=false;
  isItenary:boolean=true; 
  Itinerary=[];
  flag: any='Itninerary';
  supplier_location = localStorage.getItem('SupplierMeetLocation');

  constructor(public cs:MySuggestionService,private animSRVC: AnimationService,private ro: Router) { }

  ngOnInit(){
    this.getData(2,8);
    // $("#menu-toggle").click(function (e) {
    //   e.preventDefault();
    //   $("#wrapper").toggleClass("toggled");
    // });
  }


  getHowToReachData(){
    this.cs.supplierMeetGet('SMRegistration/GetHowToReachDetails')
    .subscribe((res: any) => {
      if (res.Message == "Data not found") {
        return;
      }
      else{
        this.howToReachData = res.ResponseData[0];
        }
      })
  }
  gotoRoute(rou){

    if(rou=='sydney'){
      this.animSRVC.slideToRIGHT();
      this.ro.navigateByUrl(rou);
    }
    else if(rou=='conference'){
      this.animSRVC.slideToLEFT();
         this.ro.navigateByUrl(rou);
    }
    else if(rou=='registration'){
      this.animSRVC.slideToLEFT();
         this.ro.navigateByUrl(rou);
    }
    else if(rou=='contact'){
      this.animSRVC.slideToLEFT();
         this.ro.navigateByUrl(rou);
      }   
      else if(rou=='Supplier-meet-home'){
        this.animSRVC.slideToRIGHT();
           this.ro.navigateByUrl(rou);
      }
      else
      this.ro.navigate(['../dashboard']);
 
  }

  navigateTo(){
    this.ro.navigateByUrl("Supplier-meet-home");
  }

  navigate(url) {
    this.flag=url;
    if (url == 'How To Reach') {
      this.isHowToReach = true;
      this.isItenary=false;
      this.isHotel=false;
      this.getHowToReachData();
    }
    else if(url=='hotel'){
      this.isHowToReach = false;
      this.isItenary=false;
      this.isHotel=true;
      this.getData(2,9);
    }else{
      this.isHowToReach = false;
      this.isItenary=true;
      this.isHotel=false;
      this.getData(2,8);
    }
  }

  getData(parentID, childID) {
    let request = {
      "SMCategoryID": parentID,
      "SMSubCategoryID": childID,
      "PageNo": "1"
    }
    this.cs.postMySuggestionData('SupplierMeetAdmin/GetSMContentByCateAndSubCate', request)
      .subscribe((res: any) => {
        if (res.Message == "Success") {
          this.Itinerary = res.ResponseData
          return;
        } else {
          this.cs.showSuccess("Some error occured please try again later");
          return;
        }
      })
  }

  evenOdd(index){
    if ( index % 2 == 0) {
      return true;
    }else{
      return false
    }
  }

  
  getExt(path){
    return path.split('.').reverse()[0].toLowerCase()
  }

  viewFile(filePath){
    window.open(filePath, '_blank')
  }
  

}
