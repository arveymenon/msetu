import { ConferenceComponent } from "./conference.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ItineraryComponent } from "./itinerary/itinerary.component";
import { HotelComponent } from "./hotel/hotel.component";
import { HowToReachComponent } from "./how-to-reach/how-to-reach.component";
import { AuthGuard } from 'src/app/services/auth-guards/auth.guard';

const routes: Routes = [
  {
    path: "",
    component: ConferenceComponent,
    children: [
      { path: "", redirectTo: "howToReach", pathMatch: "full", 
      data: { roles: ['Supplier-meet-landing'] } },
      { path: "itinerary", component: ItineraryComponent, 
      data: { roles: ['Supplier-meet-landing'] }  },
      { path: "hotel", component: HotelComponent },
      { path: "howToReach", component: HowToReachComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConferenceRoutingModule {}
