import { Component, OnInit } from '@angular/core';
import { MySuggestionService } from 'src/app/services/MySuggestion/my-suggestion.service';

@Component({
  selector: 'app-how-to-reach',
  templateUrl: './how-to-reach.component.html',
  styleUrls: ['./how-to-reach.component.css']
})
export class HowToReachComponent implements OnInit {
  howToReachData=[];

  constructor(public cs:MySuggestionService) { }

  ngOnInit(){
    // this.getHowToReachData();
  }

  getHowToReachData(){
    this.cs.supplierMeetGet('SMRegistration/GetHowToReachDetails')
    .subscribe((res: any) => {
      if (res.Message == "Data not found") {
        return;
      }
      else{
        // this.isUpdate=true;
        // this.howToReachForm.get('how_to_reach').setValue(res.ResponseData[0].HowToReach);
        // this.howToReachForm.get('Address').setValue(res.ResponseData[0].Address);
        // this.howToReachForm.get('file').setValue(res.ResponseData[0].ImagePath);
        // this.howToReachImageURL = res.ResponseData[0].ImagePath

        this.howToReachData = res.ResponseData[0];
        }
      })
  }

}
