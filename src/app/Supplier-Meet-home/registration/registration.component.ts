import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from "@angular/core";
import { AnimationService } from "src/app/Supplier-Meet-common/animation.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { MySuggestionService } from "src/app/services/MySuggestion/my-suggestion.service";
import { DateAdapter, MAT_DATE_FORMATS, MatDialog } from "@angular/material";
import {
  AppDateAdapter,
  APP_DATE_FORMATS,
} from "src/app/myBusiness/oesupplies/date-format";
import * as moment from "moment";
import * as CryptoJS from "crypto-js";
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { MyHelpDeskService } from "src/app/services/myHelpDesk/my-help-desk.service";
@Component({
  selector: "app-registration",
  templateUrl: "./registration.component.html",
  styleUrls: ["./registration.component.scss"],
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: APP_DATE_FORMATS,
    },
  ],
})
export class RegistrationComponent implements OnInit {
  supplier_Year: any;
  @ViewChild('alert') AlertBox: ElementRef

  modalService: any;
  room_data = [];
  Liquore_data: any;
  age_data = [];
  Food_data = [];
  liquore_val: any;
  food_pref: any;
  room_req: any;
  supplier_location = localStorage.getItem('SupplierMeetLocation');
  
  details: any;
  FlightDetailForm: FormGroup;
  PassportDetailForm: FormGroup;
  supplier_name = [];
  supplier_value: any;
  vendorcode: string;
  date: Date;
  isRegister: boolean;
  msg: any;
  username: any;
  PersonalDetailForm: FormGroup;
  showdate: any;
  enddate: any;
  constructor(
    public http: MyHelpDeskService,
    public cs: MySuggestionService,
    private animSRVC: AnimationService,
    private ro: Router,
    private fb: FormBuilder,
    public dialog: MatDialog,
    public router: Router
  ) {
    
    this.vendorcode = localStorage.getItem("WkcxV2RWcEhPWGxSTWpscldsRTlQUT09");
    this.vendorcode = CryptoJS.AES.decrypt(this.vendorcode, "").toString(
      CryptoJS.enc.Utf8
    );
    this.username = localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ==");
    
    if(this.username){
      this.username = CryptoJS.AES.decrypt(this.username, "").toString(
        CryptoJS.enc.Utf8
      );
    }
    this.date = new Date();
    this.isUserRegister().then((data: any) => {
      if (this.isRegister) {
        this.msg = "User is already register";
      }
      this.cs
        .supplierMeetPost("SMRegistration/GetUserDetailsByVendorCode", {
          VendorCode: this.vendorcode,
        })
        .subscribe((res: any) => {
		  console.log(res);
		  if(res.Message == "success"){
			  this.details = res.ResponseData[0]
        this.PersonalDetailForm = this.fb.group({
          name: [this.details.ParticipantName || "", Validators.required],
          designation: [this.details.Salutation, Validators.required],
          mobile: [this.details.PhoneNo|| "", Validators.required],
          emergencyNo: ["", Validators.required],
          health: ["", Validators.required],
          hotel: [this.details.Hotel || "", Validators.required],
          organisation: [this.details.Vendor, Validators.required],
          phone: [this.details.PhoneNo || "", Validators.required],
          email: [this.details.EmailID || "", Validators.required],
          age: ["", Validators.required],
        });
		  }
    });
    })
  }

  ngOnInit(): void {

    this.getupdateddate()
    this.GetActiveSMDetails();
    this.getRoomDetails();
    this.getliquorDetails();
    this.getFoodDetails();
    this.getAgeDetails();
    this.createFlightForm();
    this.createPassportForm();
    this.getSupplierByVendor();

    // const dialogRef = this.dialog.open(DisclaimerComponent, {
    //   width: "600px",
    //   height: "400px"
    // });

    // dialogRef.afterClosed().subscribe((data) => {
    //   console.log(data);
    //   if (!data) {
    //     this.router.navigateByUrl('/sydney/sydney/aboutSydney')
    //   }
    // });
  }

  GetActiveSMDetails() {
    this.cs
      .supplierMeetPost("SupplierMeetAdmin/GetActiveSMDetails", "")
      .subscribe((data: any) => {
        if (data.Message == "Success") {
          localStorage.setItem(
            "SupplierMeetLocation",
            data.ResponseData[0].Location
          );
          localStorage.setItem("SupplierMeetYear", data.ResponseData[0].Year);
          this.supplier_location = data.ResponseData[0].Location;
          this.supplier_Year = data.ResponseData[0].Year;
        } else {
          this.cs.showError("Error while fetching location details");
        }
      });
  }


  createPassportForm() {
    this.PassportDetailForm = this.fb.group({
      Passport_no: ["", Validators.required],
      DOB: ["", Validators.required],
      Nationality: ["", Validators.required],
      Visa: ["", Validators.required],
    });
  }

  getSupplierByVendor() {
    this.cs
      .supplierMeetPost("SMRegistration/GetUserDetailsByVendorCode", {
        VendorCode: this.vendorcode,
      })
      .subscribe((res: any) => {
        if (res) {
          this.supplier_name = res.ResponseData;
        }
      });
  }

  isUserRegister(): Promise<any> { 
    return new Promise((resolve: any) => {
      let Request = {
        VendorCode: this.vendorcode,
      };
      this.cs
        .supplierMeetPost("SMRegistration/IsVendorRegister", Request)
        .subscribe((res: any) => { 
          // console.log("=====dates======",this.enddate)
          // console.log("=====dates======",new Date())
          // this.enddate = new Date(this.enddate);
          // if(new Date > this.enddate){
          //     alert('Given date is greater than the current date.');
          // }           
         if (
            !this.cs.isUndefinedORNull(res) && res.Message == "User not registered") 
            {
            this.isRegister = false;
            resolve();
            const dialogRef = this.dialog.open(DisclaimerComponent, {
              width: "600px",
              height: "400px"
            });
            dialogRef.afterClosed().subscribe((data) => {
              console.log(data);
              if (!data) {
                this.router.navigateByUrl('/sydney/sydney/aboutSydney')
              }
            });
          } 
        else {
            this.isRegister = true;
            resolve();
          }
        });
    });
  }

  submit(form, stepper, ID) {
    if (form.valid) {
      if (ID == 2) {
        if (this.cs.isUndefinedORNull(this.room_req))
          this.cs.showError("Please select Room Requirement");
        else if (this.cs.isUndefinedORNull(this.food_pref))
          this.cs.showError("Please select Food Preference");
        else if (this.cs.isUndefinedORNull(this.liquore_val))
          this.cs.showError("Please select Liquore");
        else {
          stepper.selected.completed = true;
          stepper.next();
        }
      } else {
        stepper.selected.completed = true;
        stepper.next();
      }
    } else {
      if (ID == 1 && form.get("age").hasError("required")) {
        this.cs.showError("Kindly fill all mandatory details");
        return;
      }
      if (ID == 2) {
        if (this.cs.isUndefinedORNull(this.room_req))
          this.cs.showError("Kindly fill all mandatory details");
        else if (this.cs.isUndefinedORNull(this.food_pref))
          this.cs.showError("Please select Food Preference");
        else if (this.cs.isUndefinedORNull(this.liquore_val))
          this.cs.showError("Please select Liquore");
        return;
      }
    }
  }

  add() {
    alert("Employee Details Save successfully");
  }

  gotoRoute(rou) {
    if (rou == "sydney") {
      this.animSRVC.slideToRIGHT();
      this.ro.navigateByUrl(rou);
    } else if (rou == "conference") {
      this.animSRVC.slideToLEFT();
      this.ro.navigateByUrl(rou);
    } else if (rou == "registration") {
      this.animSRVC.slideToRIGHT();
      this.ro.navigateByUrl(rou);
    } else if (rou == "contact") {
      this.animSRVC.slideToLEFT();
      this.ro.navigateByUrl(rou);
    } else if (rou == "Supplier-meet-home") {
      this.animSRVC.slideToRIGHT();
      this.ro.navigateByUrl(rou);
    } else this.ro.navigate(["../dashboard"]);
  }

  getAgeDetails() {
    this.cs
      .supplierMeetGet("SMRegistration/GetAgeGroupData")
      .subscribe((res: any) => {
        this.age_data = res.ResponseData;
      });
  }

  getRoomDetails() {
    this.cs
      .supplierMeetGet("SMRegistration/GetRoomDetails")
      .subscribe((res: any) => {
        this.room_data = res.ResponseData;
      });
  }

  getFoodDetails() {
    this.cs
      .supplierMeetGet("SMRegistration/GetFoodPrefData")
      .subscribe((res: any) => {
        this.Food_data = res.ResponseData;
      });
  }

  getliquorDetails() {
    this.cs
      .supplierMeetGet("SMRegistration/GetLiquorData")
      .subscribe((res: any) => {
        this.Liquore_data = res.ResponseData;
      });
  }

  createFlightForm() {
    this.FlightDetailForm = this.fb.group({
      Airline_name: ["", Validators.required],
      Date_Arrival: ["", Validators.required],
      Time_Arrival: ["", Validators.required],
      Flight_No: ["", Validators.required],
      Date_of_Departure: ["", Validators.required],
      Time_of_Departure: ["", Validators.required],
    });
  }

  saveRegistrationDetails(form) {
    if (
      form.valid &&
      this.PersonalDetailForm.valid &&
      this.FlightDetailForm.valid &&
      !this.cs.isUndefinedORNull(this.liquore_val) &&
      !this.cs.isUndefinedORNull(this.food_pref) &&
      !this.cs.isUndefinedORNull(this.room_req)
    ) {
      let Request = {
        arrivalDate: moment(
          this.FlightDetailForm.controls.Date_Arrival.value
        ).format("DD-MMM-YYYY"),
        departureDate: moment(
          this.FlightDetailForm.controls.Date_of_Departure.value
        ).format("DD-MMM-YYYY"),
        ageGroup: this.PersonalDetailForm.controls.age.value,
        roomReq: this.room_req,
        foodPref: this.food_pref,
        visa: this.PassportDetailForm.controls.Visa.value,
        liquor: this.liquore_val,
        name: this.PersonalDetailForm.controls.name.value,
        designation: this.PersonalDetailForm.controls.designation.value,
        mobileNo: this.PersonalDetailForm.controls.mobile.value,
        emerContNo: this.PersonalDetailForm.controls.emergencyNo.value,
        healthIssues: this.PersonalDetailForm.controls.health.value,
        organization: this.PersonalDetailForm.controls.organisation.value,
        email: this.PersonalDetailForm.controls.email.value,
        airline: this.FlightDetailForm.controls.Airline_name.value,
        flightNo: this.FlightDetailForm.controls.Flight_No.value,
        arrivalTime: this.FlightDetailForm.controls.Time_Arrival.value,
        departureTime: this.FlightDetailForm.controls.Time_of_Departure.value,
        passportNo: this.PassportDetailForm.controls.Passport_no.value,
        nationality: this.PassportDetailForm.controls.Nationality.value,
        hotel: this.PersonalDetailForm.controls.hotel.value,
        phoneNo: this.PersonalDetailForm.controls.phone.value,
        dob: moment(this.PassportDetailForm.controls.DOB.value).format(
          "DD-MMM-YYYY"
        ),
        createdBy: this.username || this.vendorcode,
        VendorCode: this.vendorcode,
        sharingWith: this.supplier_value,
        isVisaHolder: this.PassportDetailForm.controls.Visa.value,
      };

      this.cs
        .supplierMeetPost("SMRegistration/InsertRegisterUser", Request)
        .subscribe((res: any) => {
          if (res) {
            this.cs.showSuccess(res.Message);
            this.ro.navigate(["/sydney"]);
            this.PersonalDetailForm.reset();
            this.FlightDetailForm.reset();
            this.PassportDetailForm.reset();
            this.room_req = null;
            this.food_pref = null;
            this.liquore_val = null;
          } else {
            this.cs.showError("Something went wrong please try again");
          }
        });
    } else {
      this.cs.showError("Kindly fill all mandatory details");
      return;
    }
  }

  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  

  getupdateddate() {
    let body = {
      "type": "R",
      "smid": 1
    };
    this.http.call("SMRegistration/SMRegistrationDate", body).subscribe(showdate => {
      this.showdate=showdate;
      this.enddate = showdate.ResponseData.EndDate;
      console.log("shoenddate",this.enddate)
    });
  }
}
