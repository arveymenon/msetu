import { RegistrationComponent } from './registration.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { DemoMaterialModule } from 'src/app/material-module';



const routes: Routes = [
  {
    path: '', component: RegistrationComponent,
    children: [{ path: '', redirectTo: 'registration', pathMatch: 'full' },
      
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class RegistrationRoutingModule { }
