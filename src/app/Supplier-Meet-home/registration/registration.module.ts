import { RegistrationRoutingModule } from './registration-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { DemoMaterialModule } from 'src/app/material-module';
import { RegistrationComponent } from './registration.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


import { MaterialModule } from 'src/material.module';
import { MatPaginatorModule, MatDialogModule, MatSortModule, MatFormFieldModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';

@NgModule({
  declarations: [RegistrationComponent, DisclaimerComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RegistrationRoutingModule,

    
    MaterialModule,
    FormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSortModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    // DemoMaterialModule
  ],
  entryComponents:[DisclaimerComponent]
})
export class RegistrationModule { }
