import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MyHelpDeskService } from 'src/app/services/myHelpDesk/my-help-desk.service';
import { MySuggestionService } from 'src/app/services/MySuggestion/my-suggestion.service';

@Component({
  selector: 'app-disclaimer',
  templateUrl: './disclaimer.component.html',
  styleUrls: ['./disclaimer.component.sass']
})
export class DisclaimerComponent{
  showdate: any;
  supplierid:any;

  constructor(
    public http: MyHelpDeskService,
    public cs: MySuggestionService,
    public dialogRef: MatDialogRef<DisclaimerComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.getupdateddate()

  }



  getupdateddate() {
    let body = {
      "type": "R",
      "smid": 1
    };
    this.http.call("SMRegistration/SMRegistrationDate", body).subscribe(showdate => {
      this.showdate=showdate;
    });
  }

}
