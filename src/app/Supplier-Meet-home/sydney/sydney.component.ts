import { Component, OnInit } from "@angular/core";
// import { AnimationService } from 'src/app/common/animation.service';
import { Router } from "@angular/router";
import { AnimationService } from "src/app/Supplier-Meet-common/animation.service";
import { MySuggestionService } from "src/app/services/MySuggestion/my-suggestion.service";
import { ApiService } from 'src/app/services/api/api.service';
declare var $: any;
@Component({
  selector: "app-sydney",
  templateUrl: "./sydney.component.html",
  styleUrls: ["./sydney.component.scss"],
})
export class SydneyComponent implements OnInit {
  SydneyData: any = [];
  DosAndDontData: any = [];
  tipsStayData: any = [];
  supplier_headr_img: any;
  flag: any = 1;
  supplier_location: string;
  supplier_Year: string;
  constructor(
    private animSRVC: AnimationService,
    private ro: Router,
    public cs: MySuggestionService
  ) {}
  ngOnInit(): void {
    this.supplier_headr_img = this.cs.supplier_headr_img;
    this.supplier_location = localStorage.getItem("SupplierMeetLocation");
    this.supplier_Year = localStorage.getItem("SupplierMeetYear");
    this.getSydneyData(1, 1);
    // $("#menu-toggle").click(function (e) {
    //   e.preventDefault();
    //   $("#wrapper").toggleClass("toggled");
    // });
  }

  gotoRoute(rou) {
    if (rou == "sydney") {
      this.animSRVC.slideToRIGHT();
      this.ro.navigateByUrl(rou);
    } else if (rou == "conference") {
      this.animSRVC.slideToLEFT();
      this.ro.navigateByUrl(rou);
    } else if (rou == "registration") {
      this.animSRVC.slideToLEFT();
      this.ro.navigateByUrl(rou);
    } else if (rou == "contact") {
      this.animSRVC.slideToLEFT();
      this.ro.navigateByUrl(rou);
    } else if (rou == "Supplier-meet-home") {
      this.animSRVC.slideToRIGHT();
      this.ro.navigateByUrl(rou);
    } else this.ro.navigate(["../dashboard"]);
  }

  getSydneyData(categoryID, subCategoryID) {
    this.flag = subCategoryID;
    let Request = {
      SMCategoryID: categoryID,
      SMSubCategoryID: subCategoryID,
      PageNo: "1",
    };
    this.cs
      .supplierMeetPost(
        "SupplierMeetAdmin/GetSMContentByCateAndSubCate",
        Request
      )
      .subscribe((res: any) => {
        if (res.Message == "Success") {
          // this.SydneyData=res.ResponseData;

          this.SydneyData = res.ResponseData.map((data) => {
            if(data.SMDocPath){
              data.SMDocPath = data.SMDocPath.split(",");
              if(!data.SMDocName){
                data.SMDocPath = [];
              }
            }
            return data;
          });
          console.log(this.SydneyData);
          localStorage.setItem("locationData", JSON.stringify(this.SydneyData));
          // this.ro.navigate(['/aboutSydney'])
          // this.ro.navigateByUrl('/sydney/sydney/aboutSydney')
        } else {
          this.cs.showSuccess("Some error occured please try again later");
          return;
        }
      });
  }

  getDoDontsData(data) {
    this.cs
      .supplierMeetGet("SMRegistration/GetDoDontDetails")
      .subscribe((res: any) => {
        if (res.Message == "Data not found") {
          this.flag = data;
          return;
        } else {
          this.DosAndDontData = res.ResponseData[0];
          this.flag = data;
        }
      });
  }

  getTipsStatyData(data) {
    this.cs
      .supplierMeetGet("SMRegistration/GetDuringStayDetails")
      .subscribe((res: any) => {
        if (res.Message == "Data not found") {
          this.flag = data;
          return;
        } else {
          this.tipsStayData = res.ResponseData[0];
          this.flag = data;
        }
      });
  }
}
