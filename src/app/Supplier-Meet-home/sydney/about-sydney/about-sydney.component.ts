import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MySuggestionService } from 'src/app/services/MySuggestion/my-suggestion.service';
import { SydneyComponent } from '../sydney.component';
import { path } from '@amcharts/amcharts4/core';
import { ApiService } from 'src/app/services/api/api.service';
import { HttpClient } from '@angular/common/http';
declare var $: any;
@Component({
  selector: 'app-about-sydney',
  templateUrl: './about-sydney.component.html',
  styleUrls: ['./about-sydney.component.scss']
})
export class AboutSydneyComponent implements OnInit {
  locationData=[];
  // @ViewChild(SydneyComponent)sydney;
  constructor(private ro: Router,public cs:MySuggestionService,public sydney:SydneyComponent, private http: HttpClient) { }

  ngOnInit(): void {
    this.getSydneyData(1,1);
    $("#menu-toggle").click(function (e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });

    this.locationData = JSON.parse(localStorage.getItem('locationData'));
  }

  getSydneyData(categoryID, subCategoryID) {
    let Request = {
      "SMCategoryID": categoryID,
      "SMSubCategoryID": subCategoryID,
      "PageNo": "1"
    }
    this.cs.supplierMeetPost('SupplierMeetAdmin/GetSMContentByCateAndSubCate', Request) .subscribe((res: any) => {
      if (res.Message== "Success") {
        this.locationData=res.ResponseData;
        localStorage.setItem('locationData',JSON.stringify(this.locationData))
        }
      else{
        this.cs.showSuccess("Some error occured please try again later");
        return;
      }
    })
  }

  commaSeparate(string){
    // let docPaths = string.split(',')
    const paths = string.split(',')
    var docPaths = [];
    paths.forEach(path => {
      this.http
      .get(path,{
        observe: "response",
      }).subscribe(resPath=>{
        docPaths.push(resPath)
      })
    });
    return paths

    // console.log(paths)


    // console.log(docPaths)

    // return docPaths
  }

  evenOdd(index){
    if ( index % 2 == 0) {
      return true;
    }else{
      return false
    }
  }
  removeNbsp(string){
    return string.replace(/\&nbsp;/g, ' ');
  }
}
