import { Component, OnInit } from '@angular/core';
import { SydneyComponent } from '../sydney.component';

@Component({
  selector: 'app-tips-during-stay',
  templateUrl: './tips-during-stay.component.html',
  styleUrls: ['./tips-during-stay.component.css']
})
export class TipsDuringStayComponent implements OnInit {

  constructor(public sydney:SydneyComponent) { }

  ngOnInit(): void {
    this.sydney.getTipsStatyData(6);
  }

}
