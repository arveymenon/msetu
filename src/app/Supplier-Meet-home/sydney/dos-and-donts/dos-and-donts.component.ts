import { Component, OnInit } from '@angular/core';
import { SydneyComponent } from '../sydney.component';

@Component({
  selector: 'app-dos-and-donts',
  templateUrl: './dos-and-donts.component.html',
  styleUrls: ['./dos-and-donts.component.css']
})
export class DosAndDontsComponent implements OnInit {

  constructor(public sydney:SydneyComponent) { }

  ngOnInit(): void {
    this.sydney.getDoDontsData(7);
  }

}
