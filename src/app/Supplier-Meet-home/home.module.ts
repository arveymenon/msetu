import { RegistrationModule } from "./registration/registration.module";
import { ConferenceModule } from "./conference/conference.module";
import { SydneyModule } from "./sydney/sydney.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./home.component";
import { ContactComponent } from "./contact/contact.component";
// import { DemoMaterialModule } from '../material-module';

import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MaterialModule } from "src/material.module";
import {
  MatPaginatorModule,
  MatDialogModule,
  MatSortModule,
  MatFormFieldModule,
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
} from "@angular/material";
import { AppRoutingModule } from "../app-routing.module";
import { LayoutModule } from "@angular/cdk/layout";
import { ContactModule } from './contact/contact.module';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HomeRoutingModule,
    SydneyModule,
    ConferenceModule,
    RegistrationModule,
    ContactModule,

    MaterialModule,
    FormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSortModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
  ]
})
export class HomeModule {}
