import { Component, OnInit } from '@angular/core';
import { AnimationService } from 'src/app/Supplier-Meet-common/animation.service';
import { Router } from '@angular/router';
import { MySuggestionService } from 'src/app/services/MySuggestion/my-suggestion.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contactDetails:any=[];
  supplier_location = localStorage.getItem('SupplierMeetLocation');

  constructor(public cs:MySuggestionService,private animSRVC: AnimationService, private ro: Router) { }

  ngOnInit(): void {
    this.getContactDetails();
  }
  getContactDetails(){
    this.cs.supplierMeetGet('SMRegistration/GetContactUsDetails')
    .subscribe((res: any) => {
      if (res.Message == "Data not found") {
      return;
      }
      else{
        this.contactDetails = res.ResponseData[0]
        }
      })
  }


  gotoRoute(rou) {
    if (rou == 'sydney') {
      this.animSRVC.slideToRIGHT();
      this.ro.navigateByUrl(rou);
    }
    else if (rou == 'conference') {
      this.animSRVC.slideToLEFT();
      this.ro.navigateByUrl(rou);
    }
    else if (rou == 'registration') {
      this.animSRVC.slideToLEFT();
      this.ro.navigateByUrl(rou);
    }
    else if (rou == 'contact') {
      this.animSRVC.slideToLEFT();
      this.ro.navigateByUrl(rou);
    }
    else if(rou==''){
      this.animSRVC.slideToRIGHT();
      this.ro.navigateByUrl(rou);
    }
    else
     this.ro.navigate(['Supplier-meet-home']);
  }

}
