import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JitprintComponent } from './jitprint.component';

describe('JitprintComponent', () => {
  let component: JitprintComponent;
  let fixture: ComponentFixture<JitprintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JitprintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JitprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
