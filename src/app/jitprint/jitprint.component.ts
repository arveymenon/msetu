import { Component, OnInit, ViewChild } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { MatIconRegistry, MatPaginator } from "@angular/material";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { SurveyService } from "../services/survey/survey.service";
import { JitService } from "../services/jit/jit.service";
import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";
import * as CryptoJS from 'crypto-js';
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";

import * as _ from "lodash";
import * as moment from "moment";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-jitprint',
  templateUrl: './jitprint.component.html',
  styleUrls: ['./jitprint.component.scss']
})
export class JitprintComponent implements OnInit {
  userid = CryptoJS.AES.decrypt(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ=='),"").toString(CryptoJS.enc.Utf8);
  @ViewChild(MatPaginator) paginator: MatPaginator
  roleId = localStorage.getItem('WTIwNWMxcFZiR3M9')
  // supplierCode = "DI061X";
  selectAllData = new FormControl(false)
  public supplierCode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
  date = Date;
  moment = moment
  viewFilter = true;
  FilterForm: FormGroup;
  FilterAttributeForm: FormGroup;
  selectAllAttribute = new FormControl(false);

  plantCodes = [];
  shopList = [];
  productionLineList = [];
  dataFromList = [];
  Aggregates = [];
  
  barCodeNumberTransaction = [];
  DataSource = [];
  displayedColumns: string[] = [
    "selectCheck",
    "productionLine",
    "shopName",
    "DSN",
    "SPN",
    "BIN",
    "modelCode",
    "dateTime",
    "colour",
    "remarks",
    "country",
    "paintOutScan",
    "majorVariant",
    "VIN",
    "partNo",
    "partDescription",
    "dataFrom"
  ];

  public allDisplayedColumns: string[] = [
    "selectCheck",
    "productionLine",
    "shopName",
    "DSN",
    "SPN",
    "BIN",
    "modelCode",
    "dateTime",
    "colour",
    "remarks",
    "country",
    "paintOutScan",
    "majorVariant",
    "VIN",
    "partNo",
    "partDescription",
    "dataFrom"
  ];
  
  pageSize = 200;

  public searchColOptions = _.cloneDeep(this.displayedColumns);

  barCodeNumberList: any = [];
  Json=[];

  constructor(
    public commonService: CommonUtilityService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private modalService: NgbModal,
    public jitService: JitService,
    public formBuilder: FormBuilder,
    public cs: MySuggestionService,
    public toastr: ToastrService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/excel.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "wordIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/word_Icon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "printIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/print.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "filter_white",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/filter_white.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "filter",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/filter.svg"
      )
    );
    this.searchColOptions.splice(0, 1);
    this.FilterForm = this.formBuilder.group({
      PlantCode: ['', Validators.required],
      ShopName: [],
      ProductionLine: [],
      DataFrom: [],
      Aggregates: [],
      SearchBy: [],
      SearchByValue: [],
      SearchByCutOff: []
    });

    this.FilterAttributeForm = this.formBuilder.group({
      productionLine: [true],
      shopName: [true],
      modelCode: [true],
      dateTime: [true],
      colour: [true],
      remarks: [true],
      country: [true],
      paintOutScan: [true],
      majorVariant: [true],
      VIN: [true],
      partNo: [true],
      partDescription: [true]
    });

    let body = { SupplierCode: CryptoJS.AES.decrypt(this.supplierCode,"").toString(CryptoJS.enc.Utf8) };
    this.jitService.call("PTLiveData/GetPlantCodeList", body).subscribe(res => {
      console.log(res);
      this.plantCodes = res.ResponseData;
    });

    this.jitService
      .call("PTLiveData/GetAggregatesList", body)
      .subscribe(res => {
        console.log(res);
        this.Aggregates = res.ResponseData;
      });

    this.FilterAttributeForm.valueChanges.subscribe(res => {
      console.log("Filter Attribute Form Changed");
      Object.keys(this.FilterAttributeForm.controls).forEach(attr => {
        console.log(this.FilterAttributeForm.get(attr).value);
        if (this.FilterAttributeForm.get(attr).value == false) {
          this.selectAllAttribute.patchValue(false);
        }
      });
    });

    console.log(this.FilterForm);
  }

  ngOnInit() {
    this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8);
    this.supplierCode = CryptoJS.AES.decrypt(this.supplierCode,"").toString(CryptoJS.enc.Utf8);
    this.selectAllData.valueChanges.subscribe(res=>{
      console.log(res)
      this.DataSource.forEach(element => {
        if(element.selectCheck.value != res){
          this.selectToPrintBarcode(null, element)
          element.selectCheck.setValue(res)
        }
      });
    })
    let analysticsRequest={
      "userClicked":this.userid,//  localStorage.getItem('userToken'),
      "device": "windows",
      "browser": localStorage.getItem('browser'),
      "moduleType": "PT Data",
      "module": "jitprint"
    }
   this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})
  }

  closeModal(content?: any) {
    console.log(this.FilterAttributeForm);
    this.modalService.dismissAll();

    const newDisplayColumns = _.cloneDeep(this.allDisplayedColumns);
    console.log(newDisplayColumns);

    Object.keys(this.FilterAttributeForm.controls).forEach(attribute => {
      console.log(attribute);
      if (this.FilterAttributeForm.value[attribute] == false) {
        console.log(this.FilterAttributeForm.value[attribute]);
        console.log(newDisplayColumns.findIndex(o => o == attribute));
        newDisplayColumns.splice(
          newDisplayColumns.findIndex(o => o == attribute),
          1
        );
      }
    });
    console.log(this.displayedColumns);
    console.log(this.allDisplayedColumns);
    this.displayedColumns = newDisplayColumns;
  }

  getProductionLineAndDataFrom(plantCode) {
    console.log(plantCode);
    let body = { PlantCode: plantCode };
    this.jitService
      .call("PTLiveData/GetProductionLineList", body)
      .subscribe(res => {
        console.log(res);
        this.productionLineList = res.ResponseData;
      });

    this.jitService.call("PTLiveData/GetDataFromList", body).subscribe(res => {
      console.log(res);
      this.dataFromList = res.ResponseData;
    });

    this.jitService.call("PTLiveData/GetShopList", body).subscribe(res => {
      console.log(res);
      this.shopList = res.ResponseData;
    });
  }

  open(content) {
    const modalOps: NgbModalOptions = {
      size: "xl" as "lg",
      centered: true,
      backdrop: "static",
      windowClass: "formModal"
    };

    this.modalService.open(content, modalOps);
  }

  exportAsExcelFile(table: any, name: any) {
    const temp_displayedColumns = _.cloneDeep(this.displayedColumns);
    this.displayedColumns.splice(0, 1);
    this.search(0).then(()=>{
      console.log('Got Response');
      // this.cs.exportAsExcelFile(table, name);
      this.cs.exportJsonAsExcelFile(this.DataSource, name);
      this.displayedColumns = temp_displayedColumns;
    },
    err=>{
      this.toastr.error('Some Error Occoured');
    })
  //   let body = {
  //     PageNo:0,
  //     PlantCode: this.FilterForm.value.PlantCode,
  //     SupplierCode: this.supplierCode,
  //     ProductionLine: this.FilterForm.value.ProductionLine,
  //     ShopName: this.FilterForm.value.ShopName,
  //     DataFrom: this.FilterForm.value.DataFrom,
  //     Aggregates1: this.FilterForm.value.Aggregates
  //   };
  //   console.log(body);
  //   this.jitService.call("PTLiveData/GetPTLiveData", body).subscribe(res => {
  //     console.log(res);
  //     if (res.Message == "Success") {
        
  //         this.Json = res.ResponseData;
  //         this.cs.exportJsonAsExcelFile(this.Json,"JITReport")
  //       }

  // })

  }

  clearSearchForm(){
    this.FilterForm.reset();
  }

  search(pageNo? : any) {
    console.log(pageNo)
    return new Promise((resolve, reject)=>{
      this.DataSource = [];
      let body = {
        PageNo: pageNo ? pageNo : pageNo == 0 ? 0 : 1,
        PlantCode: this.FilterForm.value.PlantCode,
        SupplierCode: this.supplierCode,
        ProductionLine: this.FilterForm.value.ProductionLine,
        ShopName: this.FilterForm.value.ShopName,
        DataFrom: this.FilterForm.value.DataFrom,
        Aggregates1: this.FilterForm.value.Aggregates
      };
      console.log(body);
      this.jitService.call("PTLiveData/GetPTLiveData", body).subscribe(res => {
        console.log(res);
        if (res.Message == "Success") {
          let dataSource = [];
          this.pageSize = res.DataCount ? res.DataCount : 200 || 0;
          for (const data of res.ResponseData) {
            dataSource.push({
              id: data.PKID,
              selectCheck: new FormControl(false),
              Barcode: data.BarcodeData,
              productionLine: data.ProductionLine,
              shopName: data.ShopName,
              DSN: data.DSN,
              SPN: data.SPN,
              BIN: data.BIN,
              modelCode: data.ModelCode,
              dateTime: moment(data.Dateandtime).format("DD-MM-YYYY hh:mm:s"),
              colour: data.Colour,
              remarks: data.Remarks,
              country: data.Country,
              paintOutScan: data.PaintoutScan,
              majorVariant: data.MajorVariant,
              VIN: data.VIN,
              partNo: data.PartNumber,
              partDescription: data.PartDescription,
              dataFrom: data.DataFrom,
              txnId: data.TransactionId
            });
          }
          this.DataSource = [...dataSource];
          this.updateGoto()
          if(res.ResponseData.length == dataSource.length){
            resolve('');
          }

          if (
            this.FilterForm.get("SearchBy").value &&
            this.FilterForm.get("SearchByValue").value
          ) {
            this.DataSource = this.DataSource.filter(o =>
              o[this.FilterForm.get("SearchBy").value]
                .toLowerCase()
                .includes(
                  this.FilterForm.get("SearchByValue").value.toLowerCase()
                )
            );
          }
          this.modalService.dismissAll();
          if (res.ResponseData.length > 0) {
            this.viewFilter = false;
          }else{
            this.toastr.error('Provided Data does not have sufficient data to be shown');
          }
        } else {
          reject('Some Error Occoured');
        }
      });

    })
  }

  goToControl= new FormControl(1)
  goTo: number;
  pageNumbers = [];
  

  goToChange(value? : any) {
    this.paginator.pageIndex = this.goToControl.value;
    console.log(value)
    console.log(this.paginator.pageIndex)
    // this.goTo = this.paginator.pageIndex;
    this.search(this.paginator.pageIndex)
    // this.goToControl.setValue(this.paginator.pageIndex);
  }


  updateGoto() {
    this.goTo = 1;
    this.pageNumbers = [];
    var totalLength= this.pageSize / 10;
    var ceiledLength =Math.ceil(totalLength);
    console.log(ceiledLength);
    for (let i = 1; i <= ceiledLength; i++) {
      this.pageNumbers.push(i);
    }
  }

  // ---------------------------------Filter Modal------------------------------------------------------------

  selectAll() {
    console.log(this.selectAllAttribute);
    console.log(this.FilterAttributeForm);
    const setValue = this.selectAllAttribute.value == true ? false : true;

    this.selectAllAttribute.patchValue(setValue);
    Object.keys(this.FilterAttributeForm.controls).forEach(attr => {
      console.log(this.FilterAttributeForm.get(attr).value);
      this.FilterAttributeForm.get(attr).setValue(setValue);
    });
  }

  //-------------------------------------------Bar code-------------------------------------------------------------
  elementType = 'svg';
  value = '0110AAG04711A';
  format = 'CODE128';
  lineColor = '#000000';
  width = 2;
  height = 30;
  displayValue = true;
  fontOptions = '';
  font = 'monospace';
  textAlign = 'center';
  textPosition = 'bottom';
  textMargin = 2;
  fontSize = 20;
  background = '#ffffff';
  margin = 10;
  marginTop = 10;
  marginBottom = 10;
  marginLeft = 10;
  marginRight = 10;

  selectToPrintBarcode(event, element) {
    console.log(event);
    console.log(element);

    let index = this.barCodeNumberTransaction.findIndex(o => o.id == element.id);
    if (index > -1) {
      this.barCodeNumberList.splice(index, 1);
      this.barCodeNumberTransaction.splice(index, 1);
    } else {
      this.barCodeNumberList.push(element.partNo);
      var currentdate = new Date(element.dateTime);
      element.date =  currentdate.getDate() + "-"+ (currentdate.getMonth()+1)  + "-" + currentdate.getFullYear();
      element.time = currentdate.getHours() + ":" + currentdate.getMinutes() + ":"+ currentdate.getSeconds();
      this.barCodeNumberTransaction.push(element);
    }
    
    console.log(this.barCodeNumberList);
    console.log(this.barCodeNumberTransaction);
  }

  get values(): string[] {
    console.log(this.value.split('\n'))
    return this.value.split('\n');
  }

  printComponent(component): void {

    const modalOps: NgbModalOptions = {
      size: "xl" as "lg",
      centered: true,
      windowClass: "formModal"
    };

    this.modalService.open(component, modalOps)

    for(const txn of this.barCodeNumberTransaction){
      this.jitService.call('PTLiveData/UpdateManualPrintStatus',{SrNo: txn.txnId}).subscribe(res=>{
        console.log('GetPTLiveData/UpdateManualPrintStatus', 'Updated');
        console.log(res)
      })
    }
  }

  print(component){
    let printContents, popupWin;

    printContents = document.getElementById("component1").innerHTML;
    console.log(printContents)
    // printContents = document.getElementById("component1").innerHTML;
    // popupWin = window.open("", "_blank", "top=0,left=0,height=100%,width=auto");
    // popupWin = window.open("", "", "location=1,status=1,scrollbars=1,width=auto,height=auto");
    // popupWin.document.open();
    // popupWin.document.write(`
    //   <html>
    //     <head>
    //       <title>Print tab</title>
    //       <style>
    //       //........Customized style.......
    //       </style>
    //     </head>
    //        <body onload="window.print();window.close()">${printContents}</body>
    //   </html>`);
    // popupWin.document.close();

    let printWindow;
    printWindow = window.open("", "", "location=1,status=1,scrollbars=1,width=650,height=600");
    printWindow.document.write('<html><head>');
    printWindow.document.write('<style type="text/css">@media print{.no-print, .no-print *{display: none !important;}</style>');
    printWindow.document.write('</head><body>');
    printWindow.document.write('<div style="width:100%;text-align:right">');

      //Print and cancel button
    printWindow.document.write('<input type="button" id="btnPrint" value="Print" class="no-print" style="width:100px" onclick="window.print()" />');
    printWindow.document.write('<input type="button" id="btnCancel" value="Cancel" class="no-print"  style="width:100px" onclick="window.close()" />');

    printWindow.document.write('</div>');

    //You can include any data this way.
    printWindow.document.write('<table><tr><td>'+ printContents +'</td></tr></table');

    //  printWindow.document.write(document.getElementById('forPrintPreview').innerHTML);
    //here 'forPrintPreview' is the id of the 'div' in current page(aspx).
    printWindow.document.write('</body></html>');
    printWindow.focus();
    printWindow.document.close();
  }
}