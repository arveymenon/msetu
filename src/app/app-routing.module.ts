import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { AboutUsComponent } from "./about-us/about-us.component";
import { ContactUsComponent } from "./contact-us/contact-us.component";
// import { QuickLinksComponent } from './quick-links/quick-links.component';
// import { MSetuPageComponent } from './m-setu-page/m-setu-page.component';
import { AdminDashboardComponent } from "./admin-dashboard/admin-dashboard.component";
import { MySuggestionComponent } from "./my-suggestion/my-suggestion.component";
import { RiskDashboardComponent } from "./risk-dashboard/risk-dashboard.component";
import { LiquidityModalComponent } from "./modal/liquidity-modal/liquidity-modal.component";
import { SuggestionDashboardComponent } from "./suggestion-dashboard/suggestion-dashboard.component";
// import { MySurveyComponent } from './my-survey/my-survey.component';

import { SearchModalComponent } from "./modal/search-modal/search-modal.component";
import { ContactModalComponent } from "./modal/contact-modal/contact-modal.component";
import { WhatsNewModalComponent } from "./modal/whats-new-modal/whats-new-modal.component";
import { ExcellenceAwardModalComponent } from "./modal/excellence-award-modal/excellence-award-modal.component";
// import { MSetuPageComponent } from "./m-setu-page/m-setu-page.component";
import { MsetuLandingComponent } from "./msetu-landing/msetu-landing.component";
import { QuickLinksLandingComponent } from "./quick-links-landing/quick-links-landing.component";
import { EscalateMatrixModalComponent } from "./modal/escalate-matrix-modal/escalate-matrix-modal.component";
import { SupplierMeetRegUserModalComponent } from "./modal/supplier-meet-reg-user-modal/supplier-meet-reg-user-modal.component";
import { LandingPageComponent } from "./Supplier-Meet-landing-page/landing-page.component";
import { CapabilitybuildingComponent } from "./capabilitybuilding/capabilitybuilding.component";
// import { SupplierAccessorMappingComponent } from "./supplier-accessor-mapping/supplier-accessor-mapping.component";
import { AuthGuard } from "./services/auth-guards/auth.guard";
import { SupplierCodeConductComponent } from "./supplier-code-conduct/supplier-code-conduct.component";
import { SourceCodeConductListComponent } from "./source-code-conduct-list/source-code-conduct-list.component";
import { ExtCodeOfConductionComponent } from './ext-code-of-conduction/ext-code-of-conduction.component';
import { EmployeeDetailsComponent } from "./employee-details/employee-details.component";
import { StakeholderComponent } from "./stakeholder/stakeholder.component";
// import { StakeholderUploadComponent } from "./stakeholder-upload/stakeholder-upload.component";
// import { VendorListComponent } from "./vendor-list/vendor-list.component";
import { SupplierListComponent } from "./supplier-list/supplier-list.component";
import { StakeholderUploadComponent } from "./stakeholder-upload/stakeholder-upload.component";
import { VendorListComponent } from "./vendor-list/vendor-list.component";
import { CapabilityScoreboardComponent } from "./capability-scoreboard/capability-scoreboard.component";

// import { SupplierListComponent } from "./supplier-list/supplier-list.component";

const routes: Routes = [
  { path: "", redirectTo:"login",pathMatch:"full" },
  { path: "login", component: LoginComponent },
  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [AuthGuard],
    data: { roles: ['dashboard'] }
  },
  {
    path: "my-library",
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
  {
    path: "GSTInfo",
    redirectTo: 'my-library/GSTInfo'
    
  },
  {
    path: "myHelpdesk",
    loadChildren: "./my-helpdesk/my-helpdesk.module#MyHelpdeskModule"
  },
  {
    path: "CMSSurvey",
    loadChildren: "./cmssurvey/cmssurvey.module#CmssurveyModule",
  },
  { 
  path: 'srmmSurvey' , 
  redirectTo: 'CMSSurvey/srmmSurvey'
  },
  { 
    path: 'SRMMAssessment' , 
    redirectTo: 'CMSSurvey/SRMMAssessment'
  },  
  { 
    path: 'srmm-assessors' , 
    redirectTo: 'CMSSurvey/srmm-assessors'
  },  
  { 
    path: 'tcs-survey', 
    redirectTo: 'CMSSurvey/tcs-survey'
  },  
  { 
  path: 'scheduleAssessment' , 
  redirectTo: 'CMSSurvey/scheduleAssessment'
  },
  {
    path: "createSurvey",
    loadChildren: "./cmssurvey/cmssurvey.module#CmssurveyModule",
    canActivate: [AuthGuard],
    data: { roles: ['createSurvey'] }
  },
  // {
  //   path: "SRMMMISYOYAnalysis",
  //   component: SrmmMISYOYAnalysisComponent
  //  // canActivate: [AuthGuard],
  //   //data: { roles: ['Supplier-meet-landing'] }
  // },
  {
    path: "SRMMMIS",
    redirectTo: 'CMSSurvey/SRMMMIS'
   // canActivate: [AuthGuard],
    //data: { roles: ['Supplier-meet-landing'] }
  },
  {
    path: "SSUDashboard",
    redirectTo: 'CMSSurvey/SSUDashboard'
    //canActivate: [AuthGuard],
   // data: { roles: ['Supplier-meet-landing'] }
  },
  {
    path: 'SrmmMetric',
    redirectTo: 'CMSSurvey/SrmmMetric'
    },
  {
    path: "SRMMMISYOYAnalysis",
    redirectTo: 'CMSSurvey/SRMMMISYOYAnalysis'
  }, 
  {
    path: "SSISurvey",
    redirectTo: "CMSSurvey/SSISurvey",
    canActivate: [AuthGuard],
    data: { roles: ['SSISurvey'] }
  },
  {
    path: "ssi-dashboard",
    redirectTo: "CMSSurvey/ssi-dashboard",
  },
  {
    path: "srmmSurvey",
    redirectTo: "CMSSurvey/srmmSurvey",
    // canActivate: [AuthGuard],
    // data: { roles: ['SSISurvey'] }
  },
  {
    path: "selfAssessment",
    redirectTo: "CMSSurvey/selfAssessment",
    canActivate: [AuthGuard],
    data: { roles: ['selfAssessment'] }
  },
  {
    path: "BSCSurvey",
    redirectTo: "CMSSurvey/BSCSurvey",
    canActivate: [AuthGuard],
    data: { roles: ['BSCSurvey'] }
  },
  {
    path: "mySurvey",
    loadChildren: "./my-survey/my-survey.module#MySurveyModule"
  },

  {
    path: "JIT",
    loadChildren: "./jit/jit.module#JitModule"
  },
  {
    path: "jitprint",
    redirectTo: "JIT/jitprint"
  },
  {
    path: "JITPrintHistory",
    redirectTo: "JIT/JITPrintHistory"
  },
  {
    path: "JITModelMaster",
    redirectTo: "JIT/JITModelMaster",
  },
  {
    path: "JITPartSupplierMapping",
    redirectTo: "JIT/JITPartSupplierMapping"
  },
  {
    path: "JITSupplierDetails",
    redirectTo: "JIT/JITSupplierDetails"
  },
  {
    path: "aboutus",
    component: AboutUsComponent,
    // canActivate: [AuthGuard],
    // data: { roles: [3, 7] },
  },
  {
    path: "contactUs",
    component: ContactUsComponent,
    // canActivate: [AuthGuard],
    // data: { roles: [3, 7] },
  },
  {
    path: "aboutus",
    component: AboutUsComponent,
    // canActivate: [AuthGuard],
    // data: { roles: [3, 7] },
  },
  {
    path: "adminDashboard",
    component: AdminDashboardComponent,
    canActivate: [AuthGuard],
    data: { roles: ['adminDashboard'] }
  },

  {
    path: "temp",
    loadChildren: './temp/temp/temp.module#TempModule'
  },
  {
    path: "CMSLanding",
    redirectTo: 'temp/CMSLanding'
  },
  {
    path: "CMSDashboard",
    redirectTo: 'temp/CMSDashboard'
  },
  {
    path: "CMSDocument",
    redirectTo: 'temp/CMSDocument'
  },
  {
    path: "myExcellenceAward",
    redirectTo: 'temp/myExcellenceAward'
  },
  {
    path: "awardModal",
    redirectTo: 'temp/awardModal'
  },
  {
    path: "mySuggestion",
    component: MySuggestionComponent,
    canActivate: [AuthGuard],
    data: { roles: ['mySuggestion'] }
  },
  {
    path: "my-policy-guidelines",
    redirectTo: 'temp/my-policy-guidelines'
  },
  {
    path: "SupplierMeet",
    redirectTo: 'temp/SupplierMeet'
  },

  {
    path: "riskDashboard",
    component: RiskDashboardComponent,
    // canActivate: [AuthGuard],
    // data: { roles: ['riskDashboard'] }
  },
  {
    path: "liquidityModal",
    component: LiquidityModalComponent,
    canActivate: [AuthGuard],
    data: { roles: ['liquidityModal'] }
  },
  {
    path: "suggestionDashboard/User",
    component: SuggestionDashboardComponent,
    canActivate: [AuthGuard],
    data: { roles: ['suggestionDashboard/User'] }
  },
  {
    path: "suggestionDashboard/Admin",
    component: SuggestionDashboardComponent,
    canActivate: [AuthGuard],
    data: { roles: ['suggestionDashboard/Admin'] }
  },
  {
    path: "searchModal",
    component: SearchModalComponent,
    canActivate: [AuthGuard],
    data: { roles: ['searchModal'] }
  },
  {
    path: "contactModal",
    component: ContactModalComponent,
    canActivate: [AuthGuard],
    data: { roles: ['contactModal'] }
  },
  {
    path: "whatsNewModal",
    component: WhatsNewModalComponent,
    canActivate: [AuthGuard],
    data: { roles: ['whatsNewModal'] }
  },
  {
    path: "vendors",
    loadChildren: './vendors/vendors.module#VendorsModule'
  },
  {
    path: "create-user-profile",
    redirectTo: 'vendors/create-user-profile'
  },
  {
    path: "RoleBasedAccess",
    redirectTo: 'vendors/RoleBasedAccess'
  },
  {
    path: "OESupplies",
    redirectTo: "CMSSurvey/OESupplies",
    canActivate: [AuthGuard],
    data: { roles: ['OESupplies'] }
  },
  {
    path: "reportNew",
    redirectTo: "CMSSurvey/reportNew",
    canActivate: [AuthGuard],
    data: { roles: ['reportNew'] }
  },
  {
    path: "ReportFilter",
    redirectTo: "CMSSurvey/ReportFilter",
    canActivate: [AuthGuard],
    data: { roles: ['ReportFilter'] }
  },
  {
    path: "excellenceModal",
    component: ExcellenceAwardModalComponent,
    canActivate: [AuthGuard],
    data: { roles: ['excellenceModal'] }
  },
  {
    path: "msetuLanding",
    component: MsetuLandingComponent,
    // canActivate: [AuthGuard],
    // data: { roles: [3, 7] },
  },
  {
    path: "QuickLinks",
    component: QuickLinksLandingComponent,
    // canActivate: [AuthGuard],
    // data: { roles: [3, 7] },
  },
  {
    path: "escalatematrixModal",
    component: EscalateMatrixModalComponent,
    canActivate: [AuthGuard],
    data: { roles: ['escalatematrixModal'] }
  },

  {
    path: "SupplierMeetRegUser",
    component: SupplierMeetRegUserModalComponent,
    canActivate: [AuthGuard],
    data: { roles: ['SupplierMeetRegUser'] }
  },
  {
    path: "Supplier-meet-landing",
    component: LandingPageComponent,
    // canActivate: [AuthGuard],
    data: { roles: ['Supplier-meet-landing'] }
  },
  {
    path: "Supplier-meet-home",
    component: LandingPageComponent,
    // canActivate: [AuthGuard],
    data: { roles: ['Supplier-meet-home'] }
  },
  {
    path: "capability",
    component: CapabilitybuildingComponent,
    canActivate: [AuthGuard],
    data: { roles: ['capability'] }
  },
  {
    path : "CapabilityScoreboard",
    component :CapabilityScoreboardComponent
  },
  {
    path: "sydney",
    loadChildren: "./Supplier-Meet-home/home.module#HomeModule"
  },
  {
    path: "conference",
    redirectTo: "sydney/conference"
  },
  {
    path: "registration",
    redirectTo: "sydney/registration"
  },
  {
    path: "contact",
    redirectTo: "sydney/contact"
  },
  {
    path: "emailpanel",
    redirectTo: 'temp/emailpanel',
    canActivate: [AuthGuard],
    data: { roles: ['emailpanel'] }
  },
  {
    path: "debitNoteDashboard",
    loadChildren: './debit-note-dashboard/debit-note-dashboard.module#DebitNoteDashboardModule'
  },
  {
    path: "aprovalDebitNoteDashboard",
    redirectTo: 'debitNoteDashboard/aprovalDebitNoteDashboard'
  },
  {
    path: "temp2",
    loadChildren: './temp/temp2/temp2.module#Temp2Module'
  },
  {
    path: "spm",
    redirectTo: 'temp2/spm'
  },
  {
    path: "user-details",
    redirectTo: 'temp2/user-details'
  },
  {
    path: 'scheduleAssessment',
    redirectTo: 'CMSSurvey/scheduleAssessment'
  },

  {
    path: "createdebitNote",
    redirectTo: 'debitNoteDashboard/createdebitNote'
  },
  {
    path: "extSupplierCodeConduct",
    component: ExtCodeOfConductionComponent
  },
  {
    path: "supplierCodeConduct",
    component: SupplierCodeConductComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Supplier-meet-landing'] }
  },
  {
    path: "supplierCodeConductList",
    component: SourceCodeConductListComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Supplier-meet-landing'] }
  },
  {
    path: "employee-details",
    component: EmployeeDetailsComponent
  },
  {
    path: "analytics",
    loadChildren: './analytics/analytics.module#AnalyticsModule'
  },
  {
    path: 'usage-dashboard',
    redirectTo: 'analytics/usage-dashboard'
  },
  {
    path: 'not-logged-in-users',
    redirectTo: 'analytics/not-logged-in-users'
  },
  {
    path: 'top-five-reports',
    redirectTo: 'analytics/top-five-reports'
  },
  {
    path: 'top-five-transaction',
    redirectTo: 'analytics/top-five-transaction'
  },
  {
    path: 'total-users',
    redirectTo: 'analytics/total-users'
  },
  {
    path: 'unique-visitors',
    redirectTo: 'analytics/unique-visitors'
  },
  {
    path: 'analytics-reports',
    redirectTo: 'analytics/analytics-reports'
  },
  {
    path: 'stakeholder',
    component: StakeholderComponent 
  },
  {
    path: 'supplierDataUpload',
    component: StakeholderUploadComponent 
  },
  {
    path: 'supplierList',
    component: SupplierListComponent 
  },
  {
    path: 'vendorList',
    component: VendorListComponent 
  },
  // {
  //   path: 'RPAComponent',
  //   component: RPAComponentComponent 
  // }
  
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
      onSameUrlNavigation: "reload",
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
