import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CMSSurveyComponent } from '../cmssurvey.component';
import { AuthGuard } from 'src/app/services/auth-guards/auth.guard';
import { BSCSurveyComponent } from 'src/app/bscsurvey/bscsurvey.component';
import { SSISurveyComponent } from 'src/app/ssisurvey/ssisurvey.component';
import { SelfAssessmentComponent } from 'src/app/self-assessment/self-assessment.component';
import { OESuppliesComponent } from 'src/app/myBusiness/oesupplies/oesupplies.component';
import { ReportNewComponent } from 'src/app/report-new/report-new.component';
import { ReportFilterComponent } from 'src/app/report-filter/report-filter.component';
import { SrmmSurveyComponent } from 'src/app/srmm-survey/srmm-survey.component';
import { ScheduleAssessmentComponent } from 'src/app/schedule-assessment/schedule-assessment.component';
import { SRMMAssessmentComponent } from 'src/app/srmmassessment/srmmassessment.component';
import { SRMMMISComponent } from 'src/app/srmmmis/srmmmis.component';
import { SrmmMISYOYAnalysisComponent } from 'src/app/srmm-misyoyanalysis/srmm-misyoyanalysis.component';
import { SSUDashboardComponent } from 'src/app/ssudashboard/ssudashboard.component';
import { SrmmMetricComponent } from 'src/app/srmm-metric/srmm-metric.component';
import { Tier2n3Component } from 'src/app/tier2n3/tier2n3.component';
import { SrmmAssessorsComponent } from 'src/app/srmm-assessors/srmm-assessors.component';
import { TcsSurveyComponent } from 'src/app/tcs-survey/tcs-survey.component';
import { SsiDashboardComponent } from 'src/app/ssi-dashboard/ssi-dashboard.component';
import { CocDashboardComponent } from 'src/app/coc-dashboard/coc-dashboard.component';


const routes: Routes = [
  {
    path:'',
    component: CMSSurveyComponent,
    canActivate: [AuthGuard],
    data: { roles: ['CMSSurvey'] }
  },
  {
    path: "SSISurvey",
    component: SSISurveyComponent,
    // canActivate: [AuthGuard],
    // data: { roles: ['SSISurvey'] }
  },
  {
    path: "selfAssessment",
    component: SelfAssessmentComponent,
    canActivate: [AuthGuard],
    data: { roles: ['selfAssessment'] }
  },
  {
    path: "BSCSurvey",
    component: BSCSurveyComponent,
    canActivate: [AuthGuard],
    data: { roles: ['BSCSurvey'] }
  },    
  {
    path: "OESupplies",
    component: OESuppliesComponent,
    canActivate: [AuthGuard],
    data: { roles: ['OESupplies'] }
  },
  {
    path: "reportNew",
    component: ReportNewComponent,
    canActivate: [AuthGuard],
    data: { roles: ['reportNew'] }
  },
  {
    path: "ReportFilter",
    component: ReportFilterComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ReportFilter'] }
  },

  {
    path: "srmmSurvey",
    component: SrmmSurveyComponent,
    // canActivate: [AuthGuard],
    // data: { roles: ['selfAssessment'] }
  },
  {
    path: "SRMMAssessment",
    component: SRMMAssessmentComponent,
    // canActivate: [AuthGuard],
    // data: { roles: ['SRMMAssessment'] }
  },
  {
    path: "SRMMMIS",
    component: SRMMMISComponent,
    // canActivate: [AuthGuard],
    // data: { roles: ['SRMMMIS'] }
  },

  {
    path: "scheduleAssessment",
    component: ScheduleAssessmentComponent,
    // canActivate: [AuthGuard],
    // data: { roles: ['scheduleAssessment'] }
  },
  {
    path: "SRMMMISYOYAnalysis",
    component: SrmmMISYOYAnalysisComponent,
   // canActivate: [AuthGuard],
    //data: { roles: ['Supplier-meet-landing'] }
  },
  {
    path: "SSUDashboard",
    component: SSUDashboardComponent,
    //canActivate: [AuthGuard],
   // data: { roles: ['Supplier-meet-landing'] }
  },
  {
    path: "SrmmMetric",
    component: SrmmMetricComponent,
    //canActivate: [AuthGuard],
   // data: { roles: ['Supplier-meet-landing'] }
  },
  {
    path: "tier2N3",
    component: Tier2n3Component,
    //canActivate: [AuthGuard],
   // data: { roles: ['Supplier-meet-landing'] }
  },
  {
    path: "srmm-assessors",
    component: SrmmAssessorsComponent,
    //canActivate: [AuthGuard],
   // data: { roles: ['Supplier-meet-landing'] }
  },
  {
    path: "ssi-dashboard",
    component: SsiDashboardComponent
  },
  {
    path: "tcs-survey",
    component: TcsSurveyComponent
  },
  {
    path: "coc-dashboard",
    component: CocDashboardComponent
  }

]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CmssurveyRoutingModule { }
