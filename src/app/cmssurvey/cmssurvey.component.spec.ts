import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CMSSurveyComponent } from './cmssurvey.component';

describe('CMSSurveyComponent', () => {
  let component: CMSSurveyComponent;
  let fixture: ComponentFixture<CMSSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CMSSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CMSSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
