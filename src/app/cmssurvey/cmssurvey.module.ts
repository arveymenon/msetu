import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CMSSurveyComponent } from './cmssurvey.component';
import { CreateCMSSurveyComponent } from '../create-cmssurvey/create-cmssurvey.component';
import { AddQuestionModalComponent } from '../add-question-modal/add-question-modal.component';
import { ViewQuestionListModalComponent } from '../view-question-list-modal/view-question-list-modal.component';
import { BSCSurveyComponent } from '../bscsurvey/bscsurvey.component';
import { SSISurveyComponent } from '../ssisurvey/ssisurvey.component';
import { SelfAssessmentComponent } from '../self-assessment/self-assessment.component';
import { OESuppliesComponent } from '../myBusiness/oesupplies/oesupplies.component';
import { ReportNewComponent } from '../report-new/report-new.component';
import { ReportFilterComponent } from '../report-filter/report-filter.component';
import { MySurveyModalComponent } from "../modal/my-survey-modal/my-survey-modal.component";

// import { StylePaginatorDirective } from "./../myBusiness/oesupplies/style-paginator.directive";
import { DatasourcePipe } from "./../datasource.pipe";

import { CmssurveyRoutingModule } from './cmssurvey-routing/cmssurvey-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/material.module';
import { MatPaginatorModule, MatDialogModule, MatSortModule, MatFormFieldModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { NgxBarcodeModule } from 'ngx-barcode';
import { SrmmSurveyComponent } from '../srmm-survey/srmm-survey.component';
import { ScheduleAssessmentComponent } from '../schedule-assessment/schedule-assessment.component';
import { SRMMAssessmentComponent } from '../srmmassessment/srmmassessment.component';
import { SRMMMISComponent } from '../srmmmis/srmmmis.component';
import { SrmmMISYOYAnalysisComponent } from '../srmm-misyoyanalysis/srmm-misyoyanalysis.component';
import { SSUDashboardComponent } from '../ssudashboard/ssudashboard.component';
import { SrmmMetricComponent } from '../srmm-metric/srmm-metric.component';
import { ArrayKeysPipe } from '../pipe/array-keys/array-keys.pipe';
import { Tier2n3Component } from '../tier2n3/tier2n3.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { SrmmAssessorsComponent } from '../srmm-assessors/srmm-assessors.component';
import { TcsSurveyComponent } from '../tcs-survey/tcs-survey.component';
import { SsiDashboardComponent } from '../ssi-dashboard/ssi-dashboard.component';
import { CocDashboardComponent } from '../coc-dashboard/coc-dashboard.component';


@NgModule({
  declarations: [
    MySurveyModalComponent,
    CMSSurveyComponent,
    CreateCMSSurveyComponent,
    AddQuestionModalComponent,
    ViewQuestionListModalComponent,

    BSCSurveyComponent,
    SSISurveyComponent,
    SelfAssessmentComponent,
    TcsSurveyComponent,

    SrmmSurveyComponent,
    SRMMAssessmentComponent,
    SRMMMISComponent,
    ScheduleAssessmentComponent,
    SrmmMISYOYAnalysisComponent,
    SSUDashboardComponent,
    SrmmMetricComponent,
    SrmmAssessorsComponent,
    Tier2n3Component,
    
    OESuppliesComponent,
    ReportNewComponent,
    ReportFilterComponent,
    SsiDashboardComponent,
    
    CocDashboardComponent,

    DatasourcePipe,
    ArrayKeysPipe,
  ],
  entryComponents: [
    CreateCMSSurveyComponent,
    AddQuestionModalComponent,
    ViewQuestionListModalComponent,
    MySurveyModalComponent,
  ],
  providers: [
    DashboardComponent
  ],
  imports: [
    CommonModule,
    CmssurveyRoutingModule,

    MaterialModule,
    FormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSortModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    NgxBarcodeModule,
  ]
})
export class CmssurveyModule { }
