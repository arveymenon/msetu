import { Component, OnInit } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { Router } from "@angular/router";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from "@angular/cdk/drag-drop";
import {
  MatIconRegistry,
  DateAdapter,
  MAT_DATE_FORMATS,
  PageEvent,
} from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { AddQuestionModalComponent } from "../add-question-modal/add-question-modal.component";
import { ViewQuestionListModalComponent } from "../view-question-list-modal/view-question-list-modal.component";
import { CreateCMSSurveyComponent } from "../create-cmssurvey/create-cmssurvey.component";
import { SurveyService } from "../services/survey/survey.service";
import { AppDateAdapter, APP_DATE_FORMATS } from "../cmslanding/date-adapter";
import * as moment from "moment";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import { ToastrService } from "ngx-toastr";
import * as fs from "file-saver";
import { Workbook } from "exceljs";
import { MySurveyModalComponent } from "../modal/my-survey-modal/my-survey-modal.component";
// import { AppDateAdapter } from '../myBusiness/oesupplies/date-format';

@Component({
  selector: "app-cmssurvey",
  templateUrl: "./cmssurvey.component.html",
  styleUrls: ["./cmssurvey.component.scss"],
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: APP_DATE_FORMATS,
    },
  ],
})
export class CMSSurveyComponent implements OnInit {
  pageEvent: PageEvent;
  pageSize = 0;
  pageNo = 1;

  surveys = [];
  moment = moment;
  surveyTypeId: any;

  dragRowData = [
    {
      description: "Mahindra AlturasG4",
      startDate: "30-01-2020",
      endDate: "25-03-2020",
    },
    {
      description: "Mahindra Marrazzo",
      startDate: "30-01-2020",
      endDate: "25-03-2020",
    },
    {
      description: "Mahindra XUV300",
      startDate: "30-01-2020",
      endDate: "25-03-2020",
    },
    {
      description: "Mahindra thar",
      startDate: "30-01-2020",
      endDate: "25-03-2020",
    },
    {
      description: "Mahindra XUV500",
      startDate: "30-01-2020",
      endDate: "25-03-2020",
    },
  ];

  constructor(
    private router: Router,
    public commonService: CommonUtilityService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private modalService: NgbModal,
    public surveyService: SurveyService,
    public cs: MySuggestionService,
    public toast: ToastrService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "trash",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/delete.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "edit",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Edit.svg"
      )
    );

    matIconRegistry.addSvgIcon(
      "addQuestion",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/addQuestion.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "viewQuestion",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/viewQuestion.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "viewResponce",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/portable-document-format.svg"
      )
    );
  }
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  roleId: any;
  ngOnInit() {
    this.roleId = localStorage.getItem(btoa("roleId"));
    this.roleId = atob(this.roleId);
    this.getSurveys(1);
  }
  Json =[];
  download(surveyID) {
    let body ={SurveyTypeID : surveyID}
    this.surveyService
    .getSurveysOfType("AdminCrudSurvey/GetAllSurveyListExcel", body)
    .subscribe((res: any) => {
      console.log(res);
      if (res.Message == "Success") {
        this.Json = res.ResponseData;
        let dataNew = this.Json;
        console.log(dataNew);
        //Create workbook and worksheet
        let workbook = new Workbook();
        let worksheet = workbook.addWorksheet("ReportData");
        let columns = Object.keys(this.Json[0]);
        let headerRow = worksheet.addRow(columns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: "00FF0000" },
            bgColor: { argb: "00FF0000" },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
        });
        dataNew.forEach((d) => {
          // var obj = JSON.parse(d);
          var values = Object.keys(d).map(function (key) {
            return d[key];
          });
          let row = worksheet.addRow(values);
        });
        workbook.xlsx.writeBuffer().then((dataNew) => {
          let blob = new Blob([dataNew], {
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          });
          fs.saveAs(
            blob,
            "CMSSurvey" +".xlsx"
          );
        });
      }
    });
  }
  getSurveys(surveyTypeID, pageNo?: any) {
    this.surveys = [];
    console.log(surveyTypeID);
    console.log(pageNo);
    this.surveyTypeId = surveyTypeID;
    var body: any;
    if(surveyTypeID == 3)
      body = { SurveyTypeID: 0 };
    else if (surveyTypeID != 4) {
      body = { SurveyTypeID: surveyTypeID };
    } else {
      body = { SurveyTypeID: 0 };
    }
    // this.pageNo =
    body.PageNo = pageNo + 1 || 1;
    console.log(body);
    this.surveyService
      .getSurveysOfType("AdminCrudSurvey/GetAllSurveyList", body)
      .subscribe((res: any) => {
        console.log(res);
        if (res.Message == "Success") {
          this.pageSize = res.TotalCount;
          this.surveys = res.ResponseData;
        }
      });
  }

  downloadTcsSurvey() {
    let body = {
      actionType: "GETALL",
      pageNo: 0,
    };
    this.surveyService
      .getSurveysOfType("TCSSurvey/TCSSurvey", body)
      .subscribe((res) => {
        console.log(res);
        if (res.ID == 1) {
          this.cs.exportJsonAsExcelFile(res.ResponseData, "TCS_Survey");
        }
      });
  }

  // downloadExtCocSurvey() {
  //   let body = {
  //     code: null,
  //     fromDate: null,
  //     toDate: null,
  //     pageNo: 0,
  //   };
  //   this.surveyService
  //     .getSurveysOfType("SupplierDealerCOC/GetAll_SupplierDealerCOC", body)
  //     .subscribe((res) => {
  //       console.log(res);
  //       if (res.ID == 1) {
  //         this.cs.exportJsonAsExcelFile(res.ResponseData, "Ext_COC_Survey");
  //       }
  //     });
  // }

  downloadCocSurvey() {
    this.surveyService
      .getSurveysOfType("supplierCodeofConduct/GetAll_COCServey", '')
      .subscribe((res) => {
        console.log(res);
        if (res.Id == 1) {
          this.cs.exportJsonAsExcelFile(res.ResponseData, "COC_Survey");
        }
      });
  }

  getNextPage(currentPage) {
    console.log(currentPage.pageIndex);
    this.getSurveys(this.surveyTypeId, currentPage.pageIndex);
  }

  editSurvey(survey, type) {
    console.log(survey);
    survey.type = type;
    this.createSurveyModal(survey);
  }

  addQuestionsModal(SurveyId) {
    const config: NgbModalOptions = {};
    config.size = "xl" as "lg";
    console.log(SurveyId);

    const modal = this.modalService.open(AddQuestionModalComponent, config);
    modal.componentInstance.surveyId = SurveyId;
    modal.componentInstance.surveyTypeId = this.surveyTypeId;
    modal.result.then((res) => {
      console.log(res);
      this.getSurveys(this.surveyTypeId);
    });
  }

  viewQuestionList(SurveyId) {
    const modal = this.modalService.open(ViewQuestionListModalComponent, {
      size: "xl" as "lg",
    });
    modal.componentInstance.surveyId = SurveyId;
  }

  viewResponses(survey) {
    this.surveyService
      .call("AdminCrudSurvey/Get_Survey_Question_Response", {
        SurveyID: survey.SurveyId,
        PageNo: -1,
      })
      .subscribe((response) => {
        if (response.Message == "Success") {
          if (response.ResponseData.length > 0) {
            this.cs.exportJsonAsExcelFile(
              response.ResponseData,
              survey.SurveyTitle
            );
          } else {
            this.toast.warning("No response for this survey yet");
          }
        } else {
          this.toast.warning("No response for this survey yet");
        }
      });
  }

  deleteSurvey(surveyId) {
    console.log(surveyId);
    console.log(this.surveys);
    let index = this.surveys.findIndex((o) => o.SurveyId == surveyId);
    console.log(index);
    this.surveyService
      .call("AdminCrudSurvey/DeleteSurvey", {
        SurveyID: surveyId,
      })
      .subscribe((response) => {
        console.log(response);
        if (response.Message == "Success") {
          this.surveys.splice(index, 1);
          this.toast.success("Deleted Successfully");
        }
      });
  }

  createSurveyModal(survey?: any) {
    // this.modalService.open(CreateCMSSurveyComponent, { size: 'lg'});
    let modal = this.modalService.open(CreateCMSSurveyComponent, {
      size: "xl" as "lg",
      windowClass: "formModal",
    });
    modal.result.then((res) => {
      console.log("modal dismissed");
      this.getSurveys(this.surveyTypeId);
    });
    modal.componentInstance.survey = survey;
    // modal.result.then(res=>{
    //   console.log('modal dismissed');
    //   this.getSurveys(this.surveyTypeId);
    // })
  }

  closeModal() {
    this.modalService.dismissAll();
  }
  openSurveyModal(surveyId) {
        let modal = this.modalService.open(MySurveyModalComponent, {  size:'xl' as 'lg' });
        modal.componentInstance.SurveyId = surveyId;
     
  }
}
