import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatIconRegistry } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from "ngx-toastr";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { JitService } from "../services/jit/jit.service";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import * as CryptoJS from "crypto-js";

@Component({
  selector: "app-stakeholder",
  templateUrl: "./stakeholder.component.html",
  styleUrls: ["./stakeholder.component.sass"],
})
export class StakeholderComponent implements OnInit {
  
  DataSource = [];
  editableStakeHolder: any

  StackholderForm: FormGroup;
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public commonService: CommonUtilityService,
    private modalService: NgbModal,
    public jitService: JitService,
    public cs: MySuggestionService,
    public formBuilder: FormBuilder,
    public toastr: ToastrService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "addIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/plus.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "trash",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/delete.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "edit",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Edit.svg"
      )
    );
  }
  RoleID:any;
  displayedColumns: string[]
  ngOnInit() {
    this.RoleID = localStorage.getItem("WTIwNWMxcFZiR3M9");
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID, "").toString(
      CryptoJS.enc.Utf8
    );

    if(this.RoleID == '7') {
      this.displayedColumns = ["ModuleName", "Stakeholder", "EmailId", "Mobile", "action"];
    }
    else {
      this.displayedColumns = ["ModuleName", "Stakeholder", "EmailId", "Mobile"];
    }
    this.StackholderForm = this.formBuilder.group({
      ModuleName: ["", Validators.required],
      StackeHolder: ["", Validators.required],
      EmailId: ["", Validators.required],
      Mobile: ["", Validators.required]
    });
    this.StackholderForm.get("ModuleName").valueChanges.subscribe((value)=>{
      debugger;
      console.log(value)
    })
    this.getStakeholders();
  }

  async getStakeholders() {
    await this.cs
      .getMySuggestionData("StackeHolder/GetAllStackeHolder")
      .subscribe(async (response) => {
        console.log(response);
        await console.log("Loading Response");
        this.DataSource = [...response.ResponseData];
      });
    await console.log(this.DataSource);
  }

  closeModal() {
    this.editableStakeHolder = null
    this.StackholderForm.reset();
    this.modalService.dismissAll();
  }

  open(content, stakeholder?: any) {
    console.log(stakeholder);
    this.modalService.open(content, {
      size: "xl" as "lg",
      centered: true,
      backdrop: "static",
      windowClass: "formModal",
    });
    if (stakeholder) {
      this.edit(stakeholder);
    }
  }

  edit(stakeholder) {
    console.log(stakeholder);
    this.editableStakeHolder = stakeholder
    this.StackholderForm = this.formBuilder.group({
      ModuleName: [stakeholder.ModuleName, Validators.required],
      StackeHolder: [stakeholder.StackeHolder, Validators.required],
      EmailId: [stakeholder.EmailId, Validators.required],
      Mobile: [stakeholder.Mobile, Validators.required]
    });
    // this.partSupplierId = PartSupplier.partSupplierId;
    // this.StackholderForm.get("StackeHolder").patchValue(stakeholder.modelCode);
  }

  async updateStakeHolder(){
    
    this.StackholderForm.updateValueAndValidity()

    await console.log(this.StackholderForm.valid) 
    if(this.StackholderForm.valid){
      let body = this.StackholderForm.value
      body.Id = this.editableStakeHolder.Id
      body.UpdatedBy = 100
      
      await this.cs
      .postMySuggestionData("StackeHolder/UpdateStackeHolderById", body)
      .subscribe(async (response) => {
        console.log(response);
        this.toastr.success('Stake Holder Updated')
        this.getStakeholders()
        this.closeModal()
      });
    } else{
      this.toastr.error('Kindly Fill All Mandatory Fields')
    }
  }

  async addStakeHolder(){
    this.StackholderForm.updateValueAndValidity({onlySelf: false})
    if(this.StackholderForm.valid){
      let body = this.StackholderForm.value
      body.UpdatedBy = 100
  
      await this.cs
        .postMySuggestionData("StackeHolder/AddStackeHolder", body)
        .subscribe(async (response) => {
          console.log(response);
          this.toastr.success('Stake Holder Added')
          this.getStakeholders()
          this.closeModal()
        });
    } else{
      this.toastr.error('Kindly Fill All Mandatory Fields')
    }
  }

  async delete(stakeholder) {
    let body = {
      Id: stakeholder.Id,
      UpdatedBy: 100,
    };

    await this.cs
      .postMySuggestionData("StackeHolder/DeleteStackeHolderById", body)
      .subscribe(async (response) => {
        console.log(response);
        this.toastr.success('Stake Holder Removed')
        this.getStakeholders()
      });
  }
    //--------------------------validation -number-alphabets-----------------------------------------------
    numberOnly(event): boolean {
      const charCode = event.which ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
}
