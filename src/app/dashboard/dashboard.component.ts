import {
  Component,
  OnInit,
  ViewEncapsulation,
  NgZone,
  ViewChild,
  ElementRef,
  TemplateRef,
} from "@angular/core";

import { MatIconRegistry } from "@angular/material";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { NgbCarousel, NgbCarouselConfig } from "@ng-bootstrap/ng-bootstrap";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { DatePipe } from "@angular/common";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from "@angular/animations";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { DashboardService } from "../services/S_dashboard/dashboard.service";
import { DataServiceService } from "../services/DataService/data-service.service";
import { WhatsNewModalComponent } from "../modal/whats-new-modal/whats-new-modal.component";
import { EscalateMatrixModalComponent } from "../modal/escalate-matrix-modal/escalate-matrix-modal.component";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import { MonsoonUpdatesModalComponent } from "../modal/monsoon-updates-modal/monsoon-updates-modal.component";
import { MyHelpDeskService } from "../services/myHelpDesk/my-help-desk.service";
import { FormControl } from '@angular/forms';
import * as CryptoJS from 'crypto-js';
import { ToastrService } from 'ngx-toastr';
import { ToastServiceService } from "../services/toaster/toast-service.service";
@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
      .carousel-item {
        display: block;
      }
      .carousel-caption {
        display: block;
      }

      .carousel-control-next-icon {
        background-image: url("/assets/img/Nextarrow.png");
      }
      .carousel-control-prev-icon {
        background-image: url("/assets/img/Nextarrow.png");
        transform: rotate(180deg);
      }
    `,
  ],

  animations: [
    trigger("simpleFadeAnimation", [
      state("false", style({ opacity: 0 })),
      transition("*=>false", [style({ opacity: 1 }), animate(600)]),
      transition("false=>*", animate(600, style({ opacity: 1 }))),
    ]),
  ],
})
export class DashboardComponent implements OnInit {
  @ViewChild("iframe") iframe: ElementRef;
  chatbotvisibility: boolean;
  private chart: am4charts.XYChart;
  url: SafeResourceUrl;
  newUpdates: any[] = [];
  whatsNewList: any = [];
  awardTitle = 'Excellence Awards';
  updateList: any = [];
  today: Date = new Date();
  seasons: string[] = ["Risk Meter", "SBCB Score"];
  @ViewChild("carousel") carousel: NgbCarousel;
  images = [
    "https://picsum.photos/900/500?random&t=1",
    "https://picsum.photos/900/500?random&t=2",
    "https://picsum.photos/900/500?random&t=3",
  ];
  userId: string;
  report_access:any;
  report_access_name=[];
  RoleID: any;
  userDetails: any;
  performanceData: any;
  cordysLink:any;
  favoriteSeason = new FormControl('Risk Meter');

  chartView = true
  userName: string;
  constructor(
    public cs: MySuggestionService,
    private modalService: NgbModal,
    public dataService: DataServiceService,
    public datepipe: DatePipe,
    private dashboardService: DashboardService,
    public commonService: CommonUtilityService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private AmCharts: AmChartsService,
    private zone: NgZone,
    private router: Router,
    private config: NgbCarouselConfig,
    public helpDeskService: MyHelpDeskService,
    public toast: ToastrService,
    public route: ActivatedRoute,
    public toastService: ToastServiceService
  ) {
    // this.cs.postMySuggestionData('Login/SetCookie', {}).subscribe(res=>{
      
    // })
    this.commonService.changeIsAuthenticate(true);
    this.config.interval = 5000;
    matIconRegistry.addSvgIcon(
      "OE_Supplies",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myBusiness/OE_Supplies.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "spareSupplies",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myBusiness/SpareSupplies.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "quality",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myBusiness/Quality.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "payment",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myBusiness/Payment.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "newPartDevelopment",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myBusiness/New_Part_Development.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "performance",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myBusiness/Performance.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "pinkArrow",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myBusiness/pinkArrow.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "brownArrow",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/myBusiness/brownArrow.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "analytics",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/suplierDashboard/analyticsicon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "spreadsheet",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/suplierDashboard/spreadsheeticon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "chatbotCloseIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/suplierDashboard/signs.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "warningIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/suplierDashboard/warning.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "arrowIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/suplierDashboard/Iconarrow.svg"
      )
    );
    // this.getNewUpdates();
    // this.helpDeskService.call('admin/SendMailFun').subscribe(res=>{
    //   console.log(res)
    // })
  }
  openNewUpdate(link) {
    // alert(link);
    window.open(link, "_blank");
  }
  openRPA() {
    this.router.navigateByUrl("RPAComponent");
  }
  getNewUpdates() {
    let latest_date = this.datepipe.transform(this.today, "yyyy-MM-dd");
    let postData = {
      TodaysDate: latest_date,
      Userid: this.userId,
    };
    this.dashboardService.getNewUpdates(postData).subscribe(
      (resp: any) => {
        console.log(resp);
        this.newUpdates = resp.ResponseData._GetCMSNewUpdate;
        this.whatsNewList = resp.ResponseData._GetCMSWhatsNew;
        //  this.images = resp.ResponseData._GetCMSWhatsNew.ImagePath;
        if (resp.ResponseData._GetCMSExcellenceAwards[0]) {
          this.awardTitle =
            resp.ResponseData._GetCMSExcellenceAwards[0].ContentCategoryName;
        }
        this.updateList = resp.ResponseData._GetCMSUpdates;
      },
      (err) => { }
    );
  }

  vendorCode:any
  
  ngOnInit() {
    this.RoleID = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID,"").toString(CryptoJS.enc.Utf8)
    this.chatbotvisibility = false
    this.userId = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
    this.userId = CryptoJS.AES.decrypt(this.userId,"").toString(CryptoJS.enc.Utf8);
    // this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    
    this.userName = localStorage.getItem('WW0xR2RGcFJQVDA9');
    this.userName = CryptoJS.AES.decrypt(this.userName,"").toString(CryptoJS.enc.Utf8);
    
    this.vendorCode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
    this.vendorCode = CryptoJS.AES.decrypt(this.vendorCode,"").toString(CryptoJS.enc.Utf8);

    // let srm='';
    let srm = localStorage.getItem('SM');
    srm = CryptoJS.AES.decrypt(srm,"").toString(CryptoJS.enc.Utf8) || 'sharda';
    this.userId = this.userId.split('-')[0]
    let userId = this.userId.split('\\')[0] +','+ this.userId.split('\\')[1]
    console.log(userId)
    // "https://mlu.mahindra.com/MSETU_UAT/MSETU.html?username=Kim%20&uname=DS063A&password=Password1&srmname=sharda&token=mahindraextDM181-K&usersource=web"
    "https://supplier.mahindra.com/CARA_WEB/CARA.html?username=Kim%20&uname=DS063A&password=Password1&srmname=sharda&token=mahindraextDM181-K&usersource=web&embedded=true"
    "https://supplier.mahindra.com/CARA_WEB/CARA.html?username=Girish%20Patel&uname=DL016&password=Password1&srmname=sharda&token=MAHINDRAEXT\DL016-K&usersource=web&embedded=true"
    this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(
    "https://supplier.mahindra.com/CARA_WEB/CARA.html?username="+this.userName+"&uname="+this.vendorCode+"&password=Password1&srmname="+srm+"&token="+userId+"&usersource=web&error=&embedded=true"
    );
    console.log(this.url);
    this.getMainSideMenu().then(()=>{
      this.getNewUpdates();
      this.getPerformanceRatingData().then(()=>{

        this.zone.runOutsideAngular(() => {
          let chart = am4core.create("chartdiv", am4charts.PieChart);
          chart.logo.disabled = true;
          // Add and configure Series
          let pieSeries = chart.series.push(new am4charts.PieSeries());
          pieSeries.dataFields.value = "count";
          pieSeries.dataFields.category = "status";
    
          // Let's cut a hole in our Pie chart the size of 30% the radius
          chart.innerRadius = am4core.percent(45);
          // Put a thick white border around each Slice
          // pieSeries.slices.template.stroke = am4core.color("#fff");
          pieSeries.slices.template.strokeWidth = 2;
          pieSeries.slices.template.strokeOpacity = 1;
          // change the cursor on hover to make it apparent the object can be interacted with
          pieSeries.slices.template.cursorOverStyle = [
            {
              property: "cursor",
              value: "pointer",
            },
          ];
    
          pieSeries.alignLabels = false;
          pieSeries.labels.template.bent = true;
          pieSeries.labels.template.radius = 3;
          pieSeries.labels.template.padding(0, 0, 0, 0);
          pieSeries.ticks.template.disabled = false;
          pieSeries.labels.template.fill = am4core.color("white");
          pieSeries.labels.template.fontSize = 14;
          pieSeries.labels.template.fontWeight = "300";
          pieSeries.labels.template.text = "{category}";
          pieSeries.slices.template.tooltipText = "{category}: {value.value}";
    
          // Create a base filter effect (as if it's not there) for the hover to return to
          let shadow = pieSeries.slices.template.filters.push(
            new am4core.DropShadowFilter()
          );
          shadow.opacity = 0;
    
          // Create hover state
          let hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists
    
          // Slightly shift the shadow and make it more prominent on hover
          let hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter());
          hoverShadow.opacity = 0.7;
          hoverShadow.blur = 5;
    
          // Add a legend
          // chart.legend = new am4charts.Legend();
          // chart.legend.position = "right";
          this.helpDeskService
            .call("IncidentManagement/Get_Ticket_DashboardCount", {
              vendorCode:this.vendorCode,//  localStorage.getItem("vendorCode"),
              roleId:this.RoleID,// localStorage.getItem("roleId"),
              userID:this.userId,// localStorage.getItem("userToken"),
            })
            .subscribe((response) => {
              if (response.Message == "Success") {
                console.log("Helpdesk count", response);
                var statuses: any = [];
    
                Object.keys(response.ResponseData).forEach((status) => {
                  if (response.ResponseData[status] != 0 && status != 'Closed') {
                    statuses.push({status: status=='Approved'?'Assigned':status=='Closed'?'Resolved':status, count: response.ResponseData[status] })
    
                    if (status == "Approved") {
                      pieSeries.colors.list.push(am4core.color("#ffc107"))
                    }
                    if (status == "NewConcern") {
                      pieSeries.colors.list.push(am4core.color("#e33045"))
                    }
                    if (status == "Closed") {
                      pieSeries.colors.list.push(am4core.color("#009688"))
                    }
                    //console.log("statuses",statuses);
    
                  }
                })
    
                chart.data = statuses;
              }
            });
    
          // pieSeries.colors.list = [
          //   am4core.color("#E31837"),
          //   am4core.color("#FFB100"),
          //   am4core.color("#FFE200"),
          // ];
          let label = pieSeries.createChild(am4core.Label);
          label.text = "{values.value.sum}";
          // {value.percent.formatNumber('#.0')}%
          label.horizontalCenter = "middle";
          label.verticalCenter = "middle";
          label.fontSize = 20;
          label.fill = am4core.color("white");
          // label.fill = "white";
          // pieSeries.labels.template.fill = am4core.color("white");
    
          let chart2 = am4core.create("barchartdiv", am4charts.XYChart);
          // chart2.hiddenState.properties.opacity = 0; // this creates initial fade-in
    
          chart2.data = this.setChart('Risk Meter');
          
          this.favoriteSeason.valueChanges.subscribe(res=>{
            console.log(res)
            chart2.data = this.setChart(res);
          })
  
          chart2.colors.list = [am4core.color("#FFCE00"), am4core.color("#D6B466")];
          let newcategoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
          newcategoryAxis.renderer.grid.template.location = 0;
          newcategoryAxis.dataFields.category = "country";
          newcategoryAxis.renderer.minGridDistance = 40;
          newcategoryAxis.fontSize = 11;
    
          let newvalueAxis = chart2.yAxes.push(new am4charts.ValueAxis());
          newvalueAxis.min = 0;
          newvalueAxis.max = 5;
          newvalueAxis.strictMinMax = true;
          newvalueAxis.renderer.minGridDistance = 15;
    
    
          //  let newvalueAxis = chart2.yAxes.push(new am4charts.CategoryAxis());
          //  newvalueAxis.dataFields.category = "visits";
          //  newvalueAxis.renderer.grid.template.location = 0;
           
          // axis break
          // let axisBreak = newvalueAxis.axisBreaks.create();
          // axisBreak.startValue = 2100;
          // axisBreak.endValue = 22900;
          //axisBreak.breakSize = 0.005;
          // fixed axis break
          // let d = (axisBreak.endValue - axisBreak.startValue) / (valueAxis.max - valueAxis.min);
          // axisBreak.breakSize = 0.05 * (1 - d) / d; // 0.05 means that the break will take 5% of the total value axis height
    
          // // make break expand on hover
          // let hoverState = axisBreak.states.create("hover");
          // hoverState.properties.breakSize = 1;
          // hoverState.properties.opacity = 0.1;
          // hoverState.transitionDuration = 1500;
    
          // axisBreak.defaultState.transitionDuration = 1000;
    
          let series = chart2.series.push(new am4charts.ColumnSeries());
          series.dataFields.categoryX = "country";
          series.dataFields.valueY = "visits";
    
          // series.dataFields.openCategoryY = "country";
          // series.dataFields.categoryY = "country"
          // series.dataFields.openValueY = "country"
          // series.dataFields.Y = "country"
          // series.columns.template.Y
    
          series.columns.template.tooltipText = "{valueY.value}";
          series.columns.template.scale = 1
          series.columns.template.tooltipY = 0;
          series.columns.template.strokeOpacity = 0;
    
          // as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
          series.columns.template.adapter.add("fill", function (fill, target) {
            return chart2.colors.getIndex(target.dataItem.index);
          });
        });
      });
    })
    let maintainanceFlag = localStorage.getItem("maintainanceFlag");
    if(maintainanceFlag == 'false'){
      this.checkUnderMaintainanceModule(this.RoleID);
    }

    setInterval(() => {
      this.carousel.next();
    }, 6000);

    if(this.route.queryParams['_value'].pId > -1 && this.route.queryParams['_value'].p){
      this.route.queryParams.subscribe(params => {
        console.log(params)
        if(this.route.queryParams['_value'].pId && this.route.queryParams['_value'].p){
          this.openPaymentReport(this.route.queryParams['_value'].pId, this.route.queryParams['_value'].p)
        }
      })
    }
  }

  setChart(res){
    if(this.performanceData){
      var data: any = [] 
      // = am4core.create("barchartdiv", am4charts.XYChart)

      if(res == 'SBCB Score'){
          data = [{
              country: "AD",
              visits: this.performanceData.SADSCORE,
            },
            {
              country: "FD",
              visits: this.performanceData.SFDSCORE,
            }
          ]
        }else{
          data = [{
              country: "AD",
              visits: this.performanceData.RADSCORE,
            },
            {
              country: "FD",
              visits: this.performanceData.RFDSCORE,
            }
          ]
        }
        return data;
    }
  }

  toggleView(val){
    this.chartView = val;
    console.log(this.favoriteSeason)
  }

  openModal(data) {
    const modalRef = this.modalService.open(WhatsNewModalComponent, {
      size: "lg",
    });
    modalRef.componentInstance.passedData = data;
    if (data.Views != 1)
      //  this.likeContent(data.cmsid,'0','1',0);
      modalRef.componentInstance.passEntry.subscribe((receivedEntry) => {
        console.log(receivedEntry);
        // this.getNewUpdates();
      });
  }

  likeContent(cmsid, DocumentID, like, view, dislike, Flag) {
    if (like == 1 && Flag == 1) {
      like = 0;
    }
    if (like == 2 && Flag == 2) {
      like = 0;
    }
    let reqjson = {
      Like: like,
      View: view,
      Userid: this.userId,
      CMSID: cmsid,
      dislike: dislike,
      DocumentID:DocumentID
    };
    this.dashboardService.updateLikesAndView(reqjson).subscribe(
      (resp: any) => {
        console.log(resp);
        this.getNewUpdates();
      },
      (err) => {
      }
    );
  }

  navigateToadminDashboard() {
    this.router.navigateByUrl("adminDashboard");
  }

  getPDF(val){
    let Request = {
      "Vendor": this.vendorCode, 
      "Sector": val
    }
    this.cs.postMySuggestionData('PerformanceRating/GetMySPMRatingFile',Request).subscribe((res:any)=>{
           if(!this.cs.isUndefinedORNull(res.FileName)){
            const linkSource = 'data:application/pdf;base64,' + res.FileContent;
            const downloadLink = document.createElement("a");
            const fileName = res.FileName;
            downloadLink.href = linkSource;
            downloadLink.download = fileName;
            downloadLink.click();
           }
           else{
             this.cs.showError("Failed to load file");
             return;
           }
    })

  }

  getPerformanceRatingData(){
    return new Promise((resolve, rej)=>{
      let Request={
        "Vendor":this.vendorCode
      }
      this.cs.postMySuggestionData('PerformanceRating/GetMyDashboardRatings',Request).subscribe((res:any)=>{
        if(!this.cs.isUndefinedORNull(res) && res[0].PADPERIOD){
          this.performanceData = res[0];
          console.log(this.performanceData)
          resolve(this.performanceData)
        }
        else{
          this.performanceData=[];
          resolve(this.performanceData)
        }
      })
    })

  }

  openMyBusinessPage(Page) {
    // alert("call");
    this.dataService.setOption("Page", Page);
    this.router.navigateByUrl("OESupplies");
  }

  getMainSideMenu():Promise<any>{
    return new Promise((resolve:any)=>{
      let Request = {
        "RoleID": this.RoleID
      }
      this.cs.postMySuggestionData('menuadmin/GetLeftMenuByRole', Request).subscribe((data: any) => {
        if (data.Message == "Success"){
          localStorage.setItem('SideMenus', JSON.stringify(data.ResponseData));
          this.report_access = JSON.parse(localStorage.getItem("SideMenus"));
          if(!this.cs.isUndefinedORNull(this.report_access)){
            if(this.report_access[0].DisplayName == "My Business" && this.report_access[0].listSubMenu.length > 0){
              this.report_access[0].listSubMenu.forEach((data,i) => {
                if(data.DisplayName == 'CordysPDMS'){
                  this.cordysLink = data.Route
                }
                  this.report_access_name.push(data.DisplayName);
              })
              console.log("Report",this.report_access_name)
            }
            resolve();
          } 
        }
        else
          this.cs.showError("Error while fetching sidemenu please try again later");
          resolve();
      })
    })

  }
  
  ngAfterViewInit() {
    // @ts-ignore
    twttr.widgets.load();
    this.zone.runOutsideAngular(() => {
      let chart = am4core.create("chartdiv", am4charts.PieChart);

      // Add and configure Series
      let pieSeries = chart.series.push(new am4charts.PieSeries());
      pieSeries.dataFields.value = "count";
      pieSeries.dataFields.category = "status";

      // Let's cut a hole in our Pie chart the size of 30% the radius
      chart.innerRadius = am4core.percent(45);
      // Put a thick white border around each Slice
      // pieSeries.slices.template.stroke = am4core.color("#fff");
      pieSeries.slices.template.strokeWidth = 2;
      pieSeries.slices.template.strokeOpacity = 1;
      // change the cursor on hover to make it apparent the object can be interacted with
      pieSeries.slices.template.cursorOverStyle = [
        {
          property: "cursor",
          value: "pointer",
        },
      ];

      pieSeries.alignLabels = false;
      pieSeries.labels.template.bent = true;
      pieSeries.labels.template.radius = 3;
      pieSeries.labels.template.padding(0, 0, 0, 0);
      pieSeries.ticks.template.disabled = false;
      pieSeries.labels.template.fill = am4core.color("white");
      pieSeries.labels.template.fontSize = 14;
      pieSeries.labels.template.fontWeight = "300";
      pieSeries.labels.template.text = "{category}";
      pieSeries.slices.template.tooltipText = "{category}: {value.value}";

      // Create a base filter effect (as if it's not there) for the hover to return to
      let shadow = pieSeries.slices.template.filters.push(
        new am4core.DropShadowFilter()
      );
      shadow.opacity = 0;

      // Create hover state
      let hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

      // Slightly shift the shadow and make it more prominent on hover
      let hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter());
      hoverShadow.opacity = 0.7;
      hoverShadow.blur = 5;

      // Add a legend
      // chart.legend = new am4charts.Legend();
      // chart.legend.position = "right";
      this.helpDeskService
        .call("IncidentManagement/Get_Ticket_DashboardCount", {
          vendorCode: this.vendorCode,// localStorage.getItem("vendorCode"),
          roleId:this.RoleID,// localStorage.getItem("roleId"),
          userID:this.userId,// localStorage.getItem("userToken"),
        })
        .subscribe((response) => {
          if (response.Message == "Success") {
            console.log("Helpdesk count", response);
            var statuses: any = [];

            Object.keys(response.ResponseData).forEach((status) => {
              if (response.ResponseData[status] != 0) {
                statuses.push({ status: status=='Approved'?'Assigned':status=='Closed'?'Resolved':status, count: response.ResponseData[status] })

                if (status == "Approved") {
                  pieSeries.colors.list.push(am4core.color("#ffc107"))
                }
                if (status == "NewConcern") {
                  pieSeries.colors.list.push(am4core.color("#e33045"))
                }
                if (status == "Closed") {
                  pieSeries.colors.list.push(am4core.color("#009688"))
                }
                //console.log("statuses",statuses);

              }
            })

            chart.data = statuses;
          }
        });

      // pieSeries.colors.list = [
      //   am4core.color("#E31837"),
      //   am4core.color("#FFB100"),
      //   am4core.color("#FFE200"),
      // ];
      let label = pieSeries.createChild(am4core.Label);
      label.text = "{values.value.sum}";
      // {value.percent.formatNumber('#.0')}%
      label.horizontalCenter = "middle";
      label.verticalCenter = "middle";
      label.fontSize = 20;
      label.fill = am4core.color("white");
      // label.fill = "white";
      // pieSeries.labels.template.fill = am4core.color("white");

      // let chart2 = am4core.create("barchartdiv", am4charts.XYChart);
      // chart2.hiddenState.properties.opacity = 0; // this creates initial fade-in

     

      // chart2.data = [
      //   {
      //     country: "AD",
      //     visits: 1
      //   },
      //   {
      //     country: "FD",
      //     visits: 2,
      //   }
       
      // ];
      // chart2.colors.list = [am4core.color("#FFCE00"), am4core.color("#D6B466")];
      // let newcategoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
      // newcategoryAxis.renderer.grid.template.location = 0;
      // newcategoryAxis.dataFields.category = "country";
      // newcategoryAxis.renderer.minGridDistance = 40;
      // newcategoryAxis.fontSize = 11;

      // let newvalueAxis = chart2.yAxes.push(new am4charts.ValueAxis());
      // newvalueAxis.min = 0;
      // newvalueAxis.max = 3;
      // newvalueAxis.strictMinMax = true;
      // newvalueAxis.renderer.minGridDistance = 30;

      //  let newvalueAxis = chart2.yAxes.push(new am4charts.CategoryAxis());
      //  newvalueAxis.dataFields.category = "visits";
      //  newvalueAxis.renderer.grid.template.location = 0;
       
      // axis break
      // let axisBreak = newvalueAxis.axisBreaks.create();
      // axisBreak.startValue = 2100;
      // axisBreak.endValue = 22900;
      //axisBreak.breakSize = 0.005;
      // fixed axis break
      // let d = (axisBreak.endValue - axisBreak.startValue) / (valueAxis.max - valueAxis.min);
      // axisBreak.breakSize = 0.05 * (1 - d) / d; // 0.05 means that the break will take 5% of the total value axis height

      // // make break expand on hover
     // let hoverState = axisBreak.states.create("hover");
      // hoverState.properties.breakSize = 1;
      // hoverState.properties.opacity = 0.1;
      // hoverState.transitionDuration = 1500;

      // axisBreak.defaultState.transitionDuration = 1000;

      // let series = chart2.series.push(new am4charts.ColumnSeries());
      // series.dataFields.categoryX = "country";
      // series.dataFields.valueY  = "visits";

      // series.dataFields.openCategoryY = "country";
      // series.dataFields.categoryY = "country"
      // series.dataFields.openValueY = "country"
      // series.dataFields.Y = "country"
      // series.columns.template.Y

      // series.columns.template.tooltipText = "{valueY.value}";
      // series.columns.template.tooltipY = 0;
      // series.columns.template.strokeOpacity = 0;

      // // as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
      // series.columns.template.adapter.add("fill", function (fill, target) {
      //   return chart2.colors.getIndex(target.dataItem.index);
      // });
    });

    // this.onLoad();
  }


  navigateToAward() {
    this.router.navigateByUrl("myExcellenceAward");
  }
  navigateToMySuggestion() {
    this.router.navigateByUrl("mySuggestion");
  }

  myLibrary() {
    this.router.navigate(["my-library"]);
  }
  openSSISurvey() {
    this.router.navigateByUrl("SSISurvey");
  }
  openSelfAssessment() {
    this.router.navigateByUrl("selfAssessment");
  }
  gstInfo() {
    this.router.navigateByUrl("GSTInfo");
  }
  openEscalationMatrix() {
    this.modalService.open(EscalateMatrixModalComponent, {
      size: "xl" as "lg",
    });
  }

  getSideMenuForReports(ParentId, RoleID, holdRedirect?: any) {
    let postData = {
      RoleID: RoleID,
      ParentId: ParentId,
    };
    this.cs
      .postMySuggestionData("MenuAdmin/GetMenuByRole", postData)
      .subscribe((res: any) => {
        let SideMenuForReports = [];
        SideMenuForReports = res.ResponseData;
        this.cs.SideMenuForReports = SideMenuForReports;
        this.dataService.ParentId = ParentId;
        if (SideMenuForReports.length > 0) {
          this.dataService.setOptionForReport(
            SideMenuForReports[0].ParentName,
            SideMenuForReports[0].NavigationName
          );
        }
        if(this.route.queryParams['_value'].t > 0){
          this.route.queryParams.subscribe(params => {
            let t = this.route.queryParams['_value'].t
            let m = encodeURIComponent(this.route.queryParams['_value'].m)
            this.router.navigateByUrl("OESupplies?"+"t="+t+"&m="+m);
          })
        } else {
          this.router.navigateByUrl("OESupplies");
        }
      });
  }

  opentwitter() {
    window.open("https://twitter.com/Msetu_EB?ref_src=twsrc%5Etfw", "_blank");
  }

  openPaymentReport(ParentId, page) {
    if(ParentId==0){
      window.open(this.cordysLink,'_blank');
      return
    }
    if(ParentId==1){
     this.router.navigateByUrl('BSCSurvey');
      return
    }
    let RoleID =this.RoleID;
    this.dataService.setOption("Page", page);
    this.getSideMenuForReports(ParentId, RoleID);
  }

  closeTrasition: boolean = false;
  closeModal() {
    this.chatbotvisibility = false;
    // this.closeTrasition = true;
  }
  // openChatbot() {
   
  //   let analysticsRequest={
  //     "userClicked":this.userId,//  localStorage.getItem('userToken'),
  //     "device": "",
  //     "browser": localStorage.getItem('browser'),
  //     "moduleType": "CA",
  //     "module": "CARA"
  //   }
  //  this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})
  // //  this.toast.warning('Coming Soon')
  //   this.chatbotvisibility = true;
  //   this.access();
  //   this.toastService.openChatbot()
  //   this.toastService.error('TEST ERROR')
  // }

  // access() {
  //   // alert("call");
  //   var iframe = document.getElementById("iframe") as HTMLIFrameElement;
  //   var style = document.createElement("style");
  //   style.textContent =
  //     "body {" + "  background-color: red;" + "  overflow: hidden;" + "}";
  //   iframe.contentDocument.head.appendChild(style);
  // }

  openMonsoonUpdates() {
    this.modalService.open(MonsoonUpdatesModalComponent, {
      size: "xl" as "lg",
    });
  }
  openMyPolicyGuidelines(){
    this.router.navigateByUrl('/my-policy-guidelines')
  }

  doc: any;
  onLoad() {
    // this.doc = this.iframe.nativeElement.contentDocument ||
    //            this.iframe.nativeElement.contentWindow;
    //            console.log("this.doc.body",this.doc.body)
    // this.doc.body.style.background = 'red';
    var iframe = document.getElementById("iframeId") as HTMLIFrameElement;
    var iWindow = iframe.contentWindow;
    var doc = iframe.contentDocument || iframe.contentWindow.document;
    console.debug("doc", doc);
    // console.log(doc.getElementById('foo').innerText);
  }

  openMyHelpdesk() {
    this.router.navigateByUrl("myHelpdesk");
  }
  //------------------- under maintainance modal code-------------
  @ViewChild('content') content: TemplateRef<any>;
  underMaintainanceModuleList :any=[];
  checkUnderMaintainanceModule(roldeID) {
    let reqjson ={
      PageNo : 1,
      FromDate : "",
      ToDate: "",
      Type : 0
    }
    
    this.cs
    .postMySuggestionData("Admin/GetMaintenanceNotificationList",reqjson)
    .subscribe(async (response) => {
      // alert(response.ResponseData.length);
      this.underMaintainanceModuleList = response.ResponseData;
      // alert(this.underMaintainanceModuleList.length)
       if(this.underMaintainanceModuleList.length > 0) 
        this.modalService.open(this.content);
      // }
    }, (error) => {
      console.log(error.Message);
    });
  }
  closeMaintainanceModal() {
    localStorage.setItem("maintainanceFlag","true");
    this.modalService.dismissAll();
  }
  openMaintainanceModal() {
    this.modalService.open( {
      size: "lg",
      centered: true,
      windowClass: "formModal",
    });
  }
  
}
