import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyLibraryComponent } from '../my-library/my-library.component';
import { GSTInfoComponent } from '../gst-info/gst-info.component';
import { DashboardRoutingModule } from './dashboard-routing/dashboard-routing.module';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/material.module';
import { SafeHtmlPipePipe } from 'src/app/pipe/safeHtmlPipe/safe-html-pipe.pipe';
import { MatPaginatorModule, MatDialogModule, MatSortModule, MatFormFieldModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';

@NgModule({
  declarations: [
    MyLibraryComponent,
    GSTInfoComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    
    MaterialModule,
    FormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSortModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule
  ]
})
export class DashboardModule { }
