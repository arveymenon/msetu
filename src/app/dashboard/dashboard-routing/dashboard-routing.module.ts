import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MyLibraryComponent } from 'src/app/my-library/my-library.component';
import { GSTInfoComponent } from 'src/app/gst-info/gst-info.component';
import { AuthGuard } from 'src/app/services/auth-guards/auth.guard';

const routes: Routes = [
  { path: '', component: MyLibraryComponent,canActivate: [AuthGuard],
  data: { roles: ['my-library'] } },
  { path: 'GSTInfo', component: GSTInfoComponent ,canActivate: [AuthGuard],
  data: { roles: ['GSTInfo'] } }
] 

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DashboardRoutingModule { }
