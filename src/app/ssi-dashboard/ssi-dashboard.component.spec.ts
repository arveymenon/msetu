import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SsiDashboardComponent } from './ssi-dashboard.component';

describe('SsiDashboardComponent', () => {
  let component: SsiDashboardComponent;
  let fixture: ComponentFixture<SsiDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SsiDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SsiDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
