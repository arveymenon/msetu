import { Component, OnInit } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";

import * as CryptoJS from "crypto-js";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as pdfMake from 'pdfmake';
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { JitService } from "../services/jit/jit.service";
import { NULL_EXPR } from "@angular/compiler/src/output/output_ast";
am4core.useTheme(am4themes_animated);

@Component({
  selector: "app-ssi-dashboard",
  templateUrl: "./ssi-dashboard.component.html",
  styleUrls: ["./ssi-dashboard.component.scss"],
})
export class SsiDashboardComponent implements OnInit {
  private chart: any = [];
  charts: any;
  chartData:any =[];

  vendorCode = CryptoJS.AES.decrypt(
    localStorage.getItem("WkcxV2RWcEhPWGxSTWpscldsRTlQUT09"),
    ""
  ).toString(CryptoJS.enc.Utf8);

  usertoken = CryptoJS.AES.decrypt(
    localStorage.getItem("WkZoT2JHTnNVblpoTWxaMQ=="),
    ""
  ).toString(CryptoJS.enc.Utf8);

  primaryRoleId = CryptoJS.AES.decrypt(localStorage.getItem("PR"), "").toString(
    CryptoJS.enc.Utf8
  );

  constructor(
    public commonUtility: CommonUtilityService,
    public http: JitService
  ) {
    this.commonUtility.changeIsAuthenticate(true);
  }

  ngOnInit() {

    let body = {
      chartType: null,
      vendorCode: this.primaryRoleId != 7 ? this.vendorCode : '',
      userName: this.primaryRoleId != 7 ? this.usertoken : '',
      roleID: this.primaryRoleId,
    };
    this.http.call("SSI/GetSSIScores", body).subscribe(res => {
      console.log(res)
      if (res.ResponseData) {
        this.charts = res.ResponseData
        this.avg_data(res.ResponseData.AverageScoreData);
        this.ssi_param_data(res.ResponseData.ParameterWiseData);
        this.plantwise_data(res.ResponseData.PlantWiseDataAD);
        this.plantwisesuppiler_data(res.ResponseData.PlantWiseSuppilerAD);
        this.Plantwisefd_data(res.ResponseData.PlantWiseDataFD);
        this.Plantwisesuppilerfd_data(res.ResponseData.PlantWiseSuppilerFD);
        this.OverallParticipationAD_data(res.ResponseData.OverallSupplierParticipationAD);
        this.OverallParticipationFD_data(res.ResponseData.OverallSupplierParticipationFD);
        this.OverallSupplier(res.ResponseData.OverallSupplierRating);
        this.chartData.push(this.chart);
        // this.myfun();
      }
      // this.ssi_param_data();
    })
  }

  avg_data(avg_data) {
    let chart2 = am4core.create("barchartdiv", am4charts.XYChart);
    chart2.data = avg_data;
    chart2.numberFormatter.numberFormat = "#.#'%'";
    var legend = new am4charts.Legend();
    legend.parent = chart2.chartContainer;
    chart2.colors.list = [am4core.color("#0000FF"), am4core.color("#0000FF"),
    am4core.color("#0000FF"),am4core.color("#0000FF"),am4core.color("#0000FF"),am4core.color("#0000FF"),am4core.color("#0000FF")];
    let newcategoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
    newcategoryAxis.renderer.grid.template.location = 0;
    newcategoryAxis.dataFields.category = "FinYear";
    newcategoryAxis.renderer.minGridDistance = 40;
    newcategoryAxis.fontSize = 11;
    newcategoryAxis.title.text = "Year";
    newcategoryAxis.renderer.grid.template.strokeWidth = 0;
    this.createGrid(20, chart2, 100, "SSI Average Score");
    let series = chart2.series.push(new am4charts.ColumnSeries());
    series.events.on("ready", function (ev) {
      let legenddata = [];

      legenddata.push({
        name: "Score",
      })

      legend.data = legenddata;
    });
    series.columns.template.maxWidth = 1;
    series.dataFields.categoryX = "FinYear";
    series.dataFields.valueY = "Per";
    series.columns.template.width = 50;
    series.columns.template.tooltipText = "{valueY.value}";
    series.calculatePercent = true;
    series.columns.template.scale = 1;
    series.columns.template.strokeOpacity = 0;
    series.columns.template.tooltipText = "{categoryX}\n[bold]Score:{valueY}[/]";
    series.columns.template.tooltipY = 0;
    series.tooltip.dy = -5;
    series.tooltip.pointerOrientation = "down";
    series.columns.template.height = am4core.percent(10);
    series.columns.template.adapter.add("fill", function (fill, target) {
      return chart2.colors.getIndex(target.dataItem.index);
    });
    let valueLabel = series.bullets.push(new am4charts.LabelBullet());
    valueLabel.label.verticalCenter = "top";
    valueLabel.label.dy = 10;
    valueLabel.label.fill = am4core.color("#fff");
    valueLabel.label.text = "{valueY.value}";
    valueLabel.label.fontSize = 15;
    valueLabel.label.horizontalCenter = "middle";
    valueLabel.label.dx = 0;
    valueLabel.label.rotation = 0;
    valueLabel.label.truncate = false;
    valueLabel.label.hideOversized = false;
    this.chart.push(chart2);
    //this.chartData.push(this.chart);
  }
  createGrid(value, chart2, maxlim, title) {
    let newvalueAxis = chart2.yAxes.push(new am4charts.ValueAxis());
    newvalueAxis.min = 0;
    newvalueAxis.max = maxlim;
    newvalueAxis.title.text = title;
    newvalueAxis.strictMinMax = true;
    var range = newvalueAxis.axisRanges.create();
    range.value = value;
    // range.label.text = title;
  }
  ssi_param_data(chartData) {
    let chart2 = am4core.create("barchart2div", am4charts.XYChart);

    chart2.data = chartData;
    chart2.legend = new am4charts.Legend();
    chart2.legend.position = 'top'
    chart2.colors.list = [am4core.color("#0000FF"), am4core.color("#FF0000"), am4core.color("#FFCE00"), am4core.color("#00FF00"), am4core.color("#800080")];
    let newcategoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
    newcategoryAxis.renderer.cellStartLocation = 0.2;
    let label = newcategoryAxis.renderer.labels.template;
    label.truncate = true;
    label.maxWidth = 250;
    newcategoryAxis.renderer.grid.template.strokeWidth = 0;
    newcategoryAxis.renderer.grid.template.location = 0;
    newcategoryAxis.dataFields.category = "Question";
    newcategoryAxis.renderer.labels.template.rotation = 60
    newcategoryAxis.renderer.labels.template.horizontalCenter = "left";
    newcategoryAxis.renderer.labels.template.dx = -50;
    newcategoryAxis.renderer.labels.template.verticalCenter = "middle";
    newcategoryAxis.renderer.validatePosition()
    newcategoryAxis.renderer.minGridDistance = 100;
    newcategoryAxis.fontSize = 11;
    newcategoryAxis.title.text = "Question";
    newcategoryAxis.title
    this.createGrid(1, chart2, 4, "Parameter Average Score");
    let itts = Object.keys(chartData[0]);
    let array = [];
    itts.forEach((year, itt) => {
      if (itt != 0 && chartData[itt][year] != 0) {
        let series = chart2.series.push(new am4charts.ColumnSeries());
        series.dataFields.categoryX = "Question";
        series.dataFields.valueY = year;
        series.name = year;
        series.columns.template.width = 20;
        series.columns.template.tooltipText = "{valueY.value}";
        series.columns.template.scale = 1;
        series.columns.template.tooltipY = 0;
        series.tooltip.dy = -5;
        series.tooltip.pointerOrientation = "down";
        series.columns.template.tooltipText = "{categoryX}\n[bold]{year}{valueY}[/]";
        series.columns.template.strokeOpacity = 0;
        let valueLabel = series.bullets.push(new am4charts.LabelBullet());
        valueLabel.label.verticalCenter = "top";
        valueLabel.label.dy = 10;
        valueLabel.label.fill = am4core.color("#fff");
        valueLabel.label.text = "{valueY.value}";
        valueLabel.label.fontSize = 13;
        valueLabel.label.rotation = -90;
        valueLabel.label.truncate = false;
        valueLabel.label.hideOversized = false;
        valueLabel.label.horizontalCenter = "middle";
        valueLabel.label.dx = 0;
        array.push(series);
      }
    });

    this.chart.push(chart2);
  }

  //added by shakir plant wise average score
  plantwise_data(chartData) {
    let chart2 = am4core.create("barchart2divs", am4charts.XYChart);
    let itemList1 = { PlantName: 'AS_Kandivali', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    //let itemList2 = { PlantName: 'AS_Nashik', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList3 = { PlantName: 'AS_Igatpuri', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList4 = { PlantName: 'AS_CHAKAN', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList5 = { PlantName: 'AS_Zaheerabad', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList6 = { PlantName: 'AS_Haridwar', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    chartData.push(itemList1);
    //chartData.push(itemList2);
    chartData.push(itemList3);
    chartData.push(itemList4);
    chartData.push(itemList5);
    chartData.push(itemList6);
    chart2.data = chartData;
    chart2.legend = new am4charts.Legend();
    chart2.legend.position = 'top'
    chart2.colors.list = [am4core.color("#0000FF"), am4core.color("#FF0000"), am4core.color("#FFCE00"), am4core.color("#00FF00"), am4core.color("#800080")];
    chart2.numberFormatter.numberFormat = "#.#'%'";
    let newcategoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
    newcategoryAxis.renderer.cellStartLocation = 0.2;
    newcategoryAxis.renderer.grid.template.location = 0;
    newcategoryAxis.renderer.grid.template.strokeWidth = 0;
    newcategoryAxis.dataFields.category = "PlantName";
    newcategoryAxis.renderer.labels.template.rotation = 60
    newcategoryAxis.renderer.labels.template.horizontalCenter = "left";
    newcategoryAxis.renderer.labels.template.dx = -50;
    newcategoryAxis.renderer.labels.template.verticalCenter = "middle";
    newcategoryAxis.renderer.validatePosition()
    newcategoryAxis.renderer.minGridDistance = 40;
    newcategoryAxis.fontSize = 11;
    newcategoryAxis.title.text = "Plant Name";
    newcategoryAxis.title
    this.createGrid(20, chart2, 100, "Plant Average Score");
    var itts = Object.keys(chartData[0]);
    let array = [];
    itts.forEach((year, itt) => {
      if (itt != 0 && chartData[year] != 0) {
        let series = chart2.series.push(new am4charts.ColumnSeries());
        series.dataFields.categoryX = "PlantName";
        series.dataFields.valueY = year;
        series.name = year;
        series.columns.template.width = 20;
        series.columns.template.tooltipText = "{valueY.value}";
        series.columns.template.scale = 1;
        series.columns.template.tooltipY = 0;
        series.tooltip.dy = -5;
        series.tooltip.pointerOrientation = "down";
        series.columns.template.tooltipText = "{categoryX}\n[bold]{year}{valueY}[/]";
        series.columns.template.strokeOpacity = 0;
        let valueLabel = series.bullets.push(new am4charts.LabelBullet());
        valueLabel.label.verticalCenter = "bottom";
        valueLabel.label.dy = 10;
        valueLabel.label.fill = am4core.color("#fff");
        valueLabel.label.text = "{valueY.value}";
        valueLabel.label.fontSize = 10;
        valueLabel.label.rotation = 0; 
        valueLabel.label.truncate = false;
        valueLabel.label.hideOversized = false;
        valueLabel.label.horizontalCenter = "left";
        valueLabel.label.horizontalCenter = "middle";
        valueLabel.label.dx = 0;
        array.push(series);
      }
    });
    this.chart.push(chart2);
  }

  // plant wise suppiler 
  plantwisesuppiler_data(chartData) {
    let chart2 = am4core.create("barchart2divsuppiler", am4charts.XYChart);
    let itemList1 = { PlantName: 'AS_Kandivali', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    //let itemList2 = { PlantName: 'AS_Nashik', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList3 = { PlantName: 'AS_Igatpuri', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList4 = { PlantName: 'AS_CHAKAN', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList5 = { PlantName: 'AS_Zaheerabad', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList6 = { PlantName: 'AS_Haridwar', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    chartData.push(itemList1);
    //chartData.push(itemList2);
    chartData.push(itemList3);
    chartData.push(itemList4);
    chartData.push(itemList5);
    chartData.push(itemList6);
    chart2.data = chartData;
    chart2.legend = new am4charts.Legend();
    chart2.legend.position = 'top'
    chart2.colors.list = [am4core.color("#0000FF"), am4core.color("#FF0000"), am4core.color("#FFCE00"), am4core.color("#00FF00"), am4core.color("#800080")];
    let newcategoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
    newcategoryAxis.renderer.cellStartLocation = 0.2;
    newcategoryAxis.renderer.grid.template.location = 0;
    newcategoryAxis.renderer.grid.template.strokeWidth = 0;
    newcategoryAxis.dataFields.category = "PlantName";
    newcategoryAxis.renderer.labels.template.rotation = 60;
    newcategoryAxis.renderer.labels.template.horizontalCenter = "left";
    newcategoryAxis.renderer.labels.template.dx = -50;
    newcategoryAxis.renderer.labels.template.verticalCenter = "middle";
    newcategoryAxis.renderer.validatePosition()
    newcategoryAxis.renderer.minGridDistance = 40;
    newcategoryAxis.fontSize = 11;
    newcategoryAxis.title.text = "Plant Name";
    newcategoryAxis.title
    this.createGrid(20, chart2, 500, "Suppiler Participated");
    var itts = Object.keys(chartData[0]);
    let array = [];
    itts.forEach((year, itt) => {
      if (itt != 0 && chartData[year] != 0) {
        let series = chart2.series.push(new am4charts.ColumnSeries());
        series.dataFields.categoryX = "PlantName";
        series.dataFields.valueY = year;
        series.name = year;
        series.columns.template.width = 20;
        series.columns.template.tooltipText = "{valueY.value}";
        series.columns.template.scale = 1;
        series.columns.template.tooltipY = 0;
        series.tooltip.dy = -5;
        series.tooltip.pointerOrientation = "down";
        series.columns.template.tooltipText = "{categoryX}\n[bold]{year}{valueY}[/]";
        series.columns.template.strokeOpacity = 0;
        let valueLabel = series.bullets.push(new am4charts.LabelBullet());
        valueLabel.label.verticalCenter = "bottom";
        valueLabel.label.dy = 10;
        valueLabel.label.fill = am4core.color("#fff");
        valueLabel.label.text = "{valueY.value}";
        valueLabel.label.fontSize = 10;
        valueLabel.label.rotation = 0;
        valueLabel.label.truncate = false;
        valueLabel.label.hideOversized = false;
        valueLabel.label.horizontalCenter = "left";
        valueLabel.label.horizontalCenter = "middle";
        valueLabel.label.dx = 0;
        array.push(series);
      }
    });
    this.chart.push(chart2);
  }

  //plant average score fd
  Plantwisefd_data(chartData) {
    let chart2 = am4core.create("barchart2divplantfd", am4charts.XYChart);
    let itemList1 = { PlantName: 'FD_Hub_Rajkot', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList2 = { PlantName: 'FD_Hub_Pune', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList3 = { PlantName: 'FD_Hub_Nasik', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList4 = { PlantName: 'FD_Hub_South', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList5 = { PlantName: 'FD_Hub_North', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    //let itemList6 = { PlantName: 'FD_Hub_Nagpur', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList7 = { PlantName: 'FD_Hub_Kolhapur', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList8 = { PlantName: 'FD_Hub_Mumbai', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };

    chartData.push(itemList1);
    chartData.push(itemList2);
    chartData.push(itemList3);
    chartData.push(itemList4);
    chartData.push(itemList5);
   // chartData.push(itemList6);
    chartData.push(itemList7);
    chartData.push(itemList8);

    chart2.data = chartData;
    chart2.legend = new am4charts.Legend();
    chart2.legend.position = 'top'
    chart2.colors.list = [am4core.color("#0000FF"), am4core.color("#FF0000"), am4core.color("#FFCE00"), am4core.color("#00FF00"), am4core.color("#800080")];
    chart2.numberFormatter.numberFormat = "#.#'%'";
    let newcategoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
    newcategoryAxis.renderer.cellStartLocation = 0.2;
    newcategoryAxis.renderer.grid.template.location = 0;
    newcategoryAxis.renderer.grid.template.strokeWidth = 0;
    newcategoryAxis.dataFields.category = "PlantName";
    newcategoryAxis.renderer.labels.template.rotation = 60
    newcategoryAxis.renderer.labels.template.horizontalCenter = "left";
    newcategoryAxis.renderer.labels.template.dx = -50;
    newcategoryAxis.renderer.labels.template.verticalCenter = "middle";
    newcategoryAxis.renderer.validatePosition()
    newcategoryAxis.renderer.minGridDistance = 40;
    newcategoryAxis.fontSize = 11;
    newcategoryAxis.title.text = "Plant Name";
    newcategoryAxis.title
    this.createGrid(20, chart2, 100, "Plant Average Score");
    var itts = Object.keys(chartData[0]);
    let array = [];
    itts.forEach((year, itt) => {
      if (itt != 0 && chartData[year] != 0) {
        let series = chart2.series.push(new am4charts.ColumnSeries());
        series.dataFields.categoryX = "PlantName";
        series.dataFields.valueY = year;
        series.name = year;
        series.columns.template.width = 20;
        series.columns.template.tooltipText = "{valueY.value}";
        series.columns.template.scale = 1;
        series.columns.template.tooltipY = 0;
        series.tooltip.dy = -5;
        series.tooltip.pointerOrientation = "down";
        series.columns.template.tooltipText = "{categoryX}\n[bold]{year}{valueY}[/]";
        series.columns.template.strokeOpacity = 0;
        let valueLabel = series.bullets.push(new am4charts.LabelBullet());
        valueLabel.label.verticalCenter = "bottom";
        valueLabel.label.dy = 10;
        valueLabel.label.fill = am4core.color("#fff");
        valueLabel.label.text = "{valueY.value}";
        valueLabel.label.fontSize = 10;
        valueLabel.label.rotation = 0;
        valueLabel.label.truncate = false;
        valueLabel.label.hideOversized = false;
        valueLabel.label.horizontalCenter = "left";
        valueLabel.label.horizontalCenter = "middle";
        valueLabel.label.dx = 0;
        array.push(series);
      }
    });
    this.chart.push(chart2);
  }
  ////plant wise participation fd
  Plantwisesuppilerfd_data(chartData) {
    let chart2 = am4core.create("barchart2divplantsuppilerfd", am4charts.XYChart);

    let itemList1 = { PlantName: 'FD_Hub_Rajkot', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList2 = { PlantName: 'FD_Hub_Pune', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList3 = { PlantName: 'FD_Hub_Nasik', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList4 = { PlantName: 'FD_Hub_South', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList5 = { PlantName: 'FD_Hub_North', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
   // let itemList6 = { PlantName: 'FD_Hub_Nagpur', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList7 = { PlantName: 'FD_Hub_Kolhapur', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList8 = { PlantName: 'FD_Hub_Mumbai', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };

    chartData.push(itemList1);
    chartData.push(itemList2);
    chartData.push(itemList3);
    chartData.push(itemList4);
    chartData.push(itemList5);
   // chartData.push(itemList6);
    chartData.push(itemList7);
    chartData.push(itemList8);

    chart2.data = chartData;
    chart2.legend = new am4charts.Legend();
    chart2.legend.position = 'top'
    chart2.colors.list = [am4core.color("#0000FF"), am4core.color("#FF0000"), am4core.color("#FFCE00"), am4core.color("#00FF00"), am4core.color("#800080")];

    let newcategoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
    newcategoryAxis.renderer.cellStartLocation = 0.2;
    newcategoryAxis.renderer.grid.template.location = 0;
    newcategoryAxis.renderer.grid.template.strokeWidth = 0;
    newcategoryAxis.dataFields.category = "PlantName";
    newcategoryAxis.renderer.labels.template.rotation = 60
    newcategoryAxis.renderer.labels.template.horizontalCenter = "left";
    newcategoryAxis.renderer.labels.template.dx = -50;
    newcategoryAxis.renderer.labels.template.verticalCenter = "middle";
    newcategoryAxis.renderer.validatePosition()
    newcategoryAxis.renderer.minGridDistance = 40;
    newcategoryAxis.fontSize = 11;
    newcategoryAxis.title.text = "Plant Name";
    newcategoryAxis.title
    this.createGrid(20, chart2, 160, "Plant Average Score");
    var itts = Object.keys(chartData[0]);
    let array = [];
    itts.forEach((year, itt) => {
      if (itt != 0 && chartData[year] != 0) {
        let series = chart2.series.push(new am4charts.ColumnSeries());
        series.dataFields.categoryX = "PlantName";
        series.dataFields.valueY = year;
        series.name = year;
        series.columns.template.width = 20;
        series.columns.template.tooltipText = "{valueY.value}";
        series.columns.template.scale = 1;
        series.columns.template.tooltipY = 0;
        series.tooltip.dy = -5;
        series.tooltip.pointerOrientation = "down";
        series.columns.template.tooltipText = "{categoryX}\n[bold]{year}{valueY}[/]";
        series.columns.template.strokeOpacity = 0;
        let valueLabel = series.bullets.push(new am4charts.LabelBullet());
        valueLabel.label.verticalCenter = "bottom";
        valueLabel.label.dy = 10;
        valueLabel.label.fill = am4core.color("#fff");
        valueLabel.label.text = "{valueY.value}";
        valueLabel.label.fontSize = 10;
        valueLabel.label.rotation = 0;
        valueLabel.label.truncate = false;
        valueLabel.label.hideOversized = false;
        valueLabel.label.horizontalCenter = "left";
        valueLabel.label.horizontalCenter = "middle";
        valueLabel.label.dx = 0;
        array.push(series);
      }
    });
    this.chart.push(chart2);
  }

  OverallParticipationAD_data(chartData) {
    let chart2 = am4core.create("barchart2divOverallSupplierpartica", am4charts.XYChart);
    let itemList1 = { SectorName: 'AD', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    chartData.push(itemList1);
    chart2.data = chartData;
    chart2.legend = new am4charts.Legend();
    chart2.legend.position = 'top'
    chart2.colors.list = [am4core.color("#0000FF"), am4core.color("#FF0000"), am4core.color("#FFCE00"), am4core.color("#00FF00"), am4core.color("#800080")];
    let newcategoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
    newcategoryAxis.renderer.grid.template.location = 0;
    newcategoryAxis.renderer.grid.template.strokeWidth = 0;
    newcategoryAxis.dataFields.category = "SectorName";
    newcategoryAxis.renderer.labels.template.rotation = 60
    newcategoryAxis.renderer.labels.template.horizontalCenter = "left";
    newcategoryAxis.renderer.labels.template.dx = -50;
    newcategoryAxis.renderer.labels.template.verticalCenter = "middle";
    newcategoryAxis.renderer.validatePosition()
    newcategoryAxis.renderer.minGridDistance = 40;
    newcategoryAxis.fontSize = 11;
    newcategoryAxis.title.text = "Sector";
    newcategoryAxis.title

    this.createGrid(20, chart2, 100, "Supplier Participated");
    var itts = Object.keys(chartData[0]);
    let array = [];
    itts.forEach((year, itt) => {
      if (itt != 0 && chartData[year] != 0) {
        let series = chart2.series.push(new am4charts.ColumnSeries());
        series.dataFields.categoryX = "SectorName";
        series.dataFields.valueY = year;
        series.name = year;
        series.columns.template.width = 20;
        series.columns.template.tooltipText = "{valueY.value}";
        series.columns.template.scale = 1;
        series.columns.template.tooltipY = 0;
        series.tooltip.dy = -5;
        series.tooltip.pointerOrientation = "down";
        series.columns.template.tooltipText = "{categoryX}\n[bold]{year}{valueY}[/]";
        series.columns.template.strokeOpacity = 0;
        let valueLabel = series.bullets.push(new am4charts.LabelBullet());
        valueLabel.label.verticalCenter = "bottom";
        valueLabel.label.dy = 10;
        valueLabel.label.fill = am4core.color("#fff");
        valueLabel.label.text = "{valueY.value}";
        valueLabel.label.fontSize = 10;
        valueLabel.label.rotation = 0;
        valueLabel.label.truncate = false;
        valueLabel.label.hideOversized = false;
        valueLabel.label.horizontalCenter = "left";
        valueLabel.label.horizontalCenter = "middle";
        valueLabel.label.dx = 0;
        array.push(series);
      }
    });
    this.chart.push(chart2);
  }

  OverallParticipationFD_data(chartData) {
    let chart2 = am4core.create("barchart2divOverallSupplierparticafd", am4charts.XYChart);
    //let itemList1 = { SectorName: 'AD', '2021-22': 'Null', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    let itemList2 = { SectorName: 'FD', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
  //  chartData.push(itemList1);
    chartData.push(itemList2);
    chart2.data = chartData;
    chart2.legend = new am4charts.Legend();
    chart2.legend.position = 'top'
    chart2.colors.list = [am4core.color("#0000FF"), am4core.color("#FF0000"), am4core.color("#FFCE00"), am4core.color("#00FF00"), am4core.color("#800080")];
    let newcategoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
    newcategoryAxis.renderer.grid.template.location = 0;
    newcategoryAxis.renderer.grid.template.strokeWidth = 0;
    newcategoryAxis.dataFields.category = "SectorName";
    newcategoryAxis.renderer.labels.template.rotation = 60
    newcategoryAxis.renderer.labels.template.horizontalCenter = "left";
    newcategoryAxis.renderer.labels.template.dx = -50;
    newcategoryAxis.renderer.labels.template.verticalCenter = "middle";
    newcategoryAxis.renderer.validatePosition()
    newcategoryAxis.renderer.minGridDistance = 30;
    newcategoryAxis.fontSize = 11;
    newcategoryAxis.title.text = "Sector";
    newcategoryAxis.title
    this.createGrid(20, chart2, 600, "Supplier Participated");
    var itts = Object.keys(chartData[0]);
    let array = [];
    itts.forEach((year, itt) => {
      if (itt != 0 && chartData[year] != 0) {
        let series = chart2.series.push(new am4charts.ColumnSeries());
        series.dataFields.categoryX = "SectorName";
        series.dataFields.valueY = year;
        series.name = year;
        series.columns.template.width = 20;
        series.columns.template.tooltipText = "{valueY.value}";
        series.columns.template.scale = 1;
        series.columns.template.tooltipY = 0;
        series.tooltip.dy = -5;
        series.tooltip.pointerOrientation = "down";
        series.columns.template.tooltipText = "{categoryX}\n[bold]{year}{valueY}[/]";
        series.columns.template.strokeOpacity = 0;
        let valueLabel = series.bullets.push(new am4charts.LabelBullet());
        valueLabel.label.verticalCenter = "bottom";
        valueLabel.label.dy = 10;
        valueLabel.label.fill = am4core.color("#fff");
        valueLabel.label.text = "{valueY.value}";
        valueLabel.label.fontSize = 1;
        valueLabel.label.rotation = 0;
        valueLabel.label.truncate = false;
        valueLabel.label.hideOversized = false;
        valueLabel.label.horizontalCenter = "left";
        valueLabel.label.horizontalCenter = "middle";
        valueLabel.label.dx = 0;
        array.push(series);
      }
    });
    this.chart.push(chart2);
  }

  OverallSupplier(chartData) {
    let chart2 = am4core.create("barchart2divOverallSupplier", am4charts.XYChart);
    let itemList1 = { SectorName: 'FD', '2021-22': 'NULL', '2020-21': 'NULL', '2019-20': 'NULL', '2018-19': 'NULL', '2017-18': 'NULL' };
    chartData.push(itemList1);
    chart2.data = chartData;
    chart2.legend = new am4charts.Legend();
    chart2.legend.position = 'top'
    chart2.colors.list = [am4core.color("#0000FF"), am4core.color("#FF0000"), am4core.color("#FFCE00"), am4core.color("#00FF00"), am4core.color("#800080")];
    chart2.numberFormatter.numberFormat = "#.#'%'";
    let newcategoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
    newcategoryAxis.renderer.grid.template.location = 0;
    newcategoryAxis.renderer.grid.template.strokeWidth = 0;
    newcategoryAxis.dataFields.category = "SectorName";
    newcategoryAxis.renderer.labels.template.rotation = 60
    newcategoryAxis.renderer.labels.template.horizontalCenter = "left";
    newcategoryAxis.renderer.labels.template.dx = -50;
    newcategoryAxis.renderer.labels.template.verticalCenter = "middle";
    newcategoryAxis.renderer.validatePosition()
    newcategoryAxis.renderer.minGridDistance = 30;
    newcategoryAxis.fontSize = 11;
    newcategoryAxis.title.text = "Sector";
    newcategoryAxis.title
    this.createGrid(20, chart2, 100, "Average Score");
    var itts = Object.keys(chartData[0]);
    let array = [];
    itts.forEach((year, itt) => {
      if (itt != 0 && chartData[year] != 0) {
        let series = chart2.series.push(new am4charts.ColumnSeries());
        series.dataFields.categoryX = "SectorName";
        series.dataFields.valueY = year;
        series.name = year;
        series.columns.template.width = 20;
        series.columns.template.tooltipText = "{valueY.value}";
        series.columns.template.scale = 1;
        series.columns.template.tooltipY = 0;
        series.tooltip.dy = -5;
        series.tooltip.pointerOrientation = "down";
        series.columns.template.tooltipText = "{categoryX}\n[bold]{year}{valueY}[/]";
        series.columns.template.strokeOpacity = 0;
        let valueLabel = series.bullets.push(new am4charts.LabelBullet());
        valueLabel.label.verticalCenter = "bottom";
        valueLabel.label.dy = 10;
        valueLabel.label.fill = am4core.color("#fff");
        valueLabel.label.text = "{valueY.value}";
        valueLabel.label.fontSize = 10;
        valueLabel.label.rotation = 0;
        valueLabel.label.truncate = false;
        valueLabel.label.hideOversized = false;
        valueLabel.label.horizontalCenter = "left";
        valueLabel.label.horizontalCenter = "middle";
        valueLabel.label.dx = 0;
        array.push(series);
      }
    });
    this.chart.push(chart2);
  }

  download(id) {
    let chart = this.chart[id];
    //let chart = this.chartData;
    let options = chart.exporting.getFormatOptions("pdf");
    options.addURL = false;
    // chart.exporting.filePrefix = id == 1 ? 'SSI_Parameter_Wise_Scores' : 'SSI_Average_Scores';
    if (chart.exporting.filePrefix = id == 1) {
      chart.exporting.filePrefix = 'SSI_Parameter_Wise_Scores';
    }
    else if (chart.exporting.filePrefix = id == 0) {
      chart.exporting.filePrefix = 'SSI_Average_Scores';
    }
    else if (chart.exporting.filePrefix = id == 2) {
      chart.exporting.filePrefix = 'SSI_PlantWise_Scores';
    }
    else if (chart.exporting.filePrefix = id == 3) {
      chart.exporting.filePrefix = 'SSI_PlantWise_Suppiler_AD';
    }
    else if (chart.exporting.filePrefix = id == 4) {
      chart.exporting.filePrefix = 'SSI_PlantWise_Average_FD';
    }
    else if (chart.exporting.filePrefix = id == 5) {
      chart.exporting.filePrefix = 'SSI_PlantWise_Supplier_Participation_FD';
    }
    else if (chart.exporting.filePrefix = id == 6) {
      chart.exporting.filePrefix = 'SSI_OverAll_Supplier_AD';
    }
    else if (chart.exporting.filePrefix = id == 7) {
      chart.exporting.filePrefix = 'SSI_OverAll_Supplier_FD';
    }
    else if (chart.exporting.filePrefix = id == 8) {
      chart.exporting.filePrefix = 'SSI_OverAll_Supplier';
    }
    chart.exporting.setFormatOptions("pdf", options);
    chart.exporting.export('pdf')
    // Promise.all([
    //   this.chart[0].exporting.pdfmake,
    //   this.chart[0].getImage("png"),
    //   this.chart[1].exporting.getImage("png")
    // ]).then(res=>{
    //   console.log(res)
    //   let pdfMake = res[0];
    //   pdfMake.download("report.pdf");
    // }); 
  }

  myfun() {
    let chart = am4core.create('chartdiv', am4charts.XYChart)
    chart.colors.step = 2;

    chart.legend = new am4charts.Legend()
    chart.legend.position = 'top'
    chart.legend.paddingBottom = 20
    chart.legend.labels.template.maxWidth = 95

    let xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
    xAxis.dataFields.category = 'category'
    xAxis.renderer.cellStartLocation = 0.1
    xAxis.renderer.cellEndLocation = 0.9
    xAxis.renderer.grid.template.location = 0;
    xAxis.renderer.grid.template.strokeWidth = 0;
    let yAxis = chart.yAxes.push(new am4charts.ValueAxis());
    yAxis.min = 0;

    function createSeries(value, name) {
      let series = chart.series.push(new am4charts.ColumnSeries())
      series.dataFields.valueY = value
      series.dataFields.categoryX = 'category'
      series.name = name

      series.events.on("hidden", arrangeColumns);
      series.events.on("shown", arrangeColumns);

      let bullet = series.bullets.push(new am4charts.LabelBullet())
      bullet.interactionsEnabled = false
      bullet.dy = 30;
      bullet.label.text = '{valueY}'
      bullet.label.fill = am4core.color('#ffffff')

      return series;
    }

    chart.data = [
      {
        category: 'Place #1',
        first: 40
      },
      {
        category: 'Place #2',
        first: 30
      },
      {
        category: 'Place #3',
        first: 27
      },
      {
        category: 'Place #4',
        first: 50
      }
    ]


    createSeries('first', 'The First');
    // createSeries('second', 'The Second');
    // createSeries('third', 'The Third');

    function arrangeColumns() {

      let series = chart.series.getIndex(0);

      let w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
      if (series.dataItems.length > 1) {
        let x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
        let x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
        let delta = ((x1 - x0) / chart.series.length) * w;
        if (am4core.isNumber(delta)) {
          let middle = chart.series.length / 2;

          let newIndex = 0;
          chart.series.each(function (series) {
            if (!series.isHidden && !series.isHiding) {
              series.dummyData = newIndex;
              newIndex++;
            }
            else {
              series.dummyData = chart.series.indexOf(series);
            }
          })
          let visibleCount = newIndex;
          let newMiddle = visibleCount / 2;

          chart.series.each(function (series) {
            let trueIndex = chart.series.indexOf(series);
            let newIndex = series.dummyData;

            let dx = (newIndex - trueIndex + middle - newMiddle) * delta

            series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
            series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
          })
        }
      }
    }
  }
}
