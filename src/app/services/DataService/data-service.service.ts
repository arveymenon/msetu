import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  private data = {};
  public Reportdata = {};  
  ParentId:any;
  
  constructor() {
    // this.data = localStorage.getItem('ZGF0YV9zZXJ2aWNlX2RhdGE=')
    // this.Reportdata = localStorage.getItem('UmVwb3J0X2RhdGFfc2VydmljZV9kYXRh')
    // this.ParentId = localStorage.getItem('cGFyZW50SWQ=')
  }

 setOption(option, value) {
    this.data[option] = value; 
    // localStorage.setItem('ZGF0YV9zZXJ2aWNlX2RhdGE=', JSON.stringify(this.data)) 
  }  
  
  getOption() { 
    return this.data;  
  }  

  
  setOptionForReport(value, childOption){
    this.Reportdata['option'] = value;
    this.Reportdata['childOption'] = childOption;
  }

  getOptionForReport() { 
    return this.Reportdata;  
  }  
}
