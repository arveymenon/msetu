import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  constructor() { }
  private data = {};
  public Reportdata = {};  
  
 setOption(option, value) {     
    this.data[option] = value;  
  }  
  
  getOption() { 
    return this.data;  
  }  

  setOptionForReport(value, childOption){
    this.Reportdata['option'] = value;
    this.Reportdata['childOption'] = childOption;
  }

  getOptionForReport() { 
    return this.Reportdata;  
  }  
}
