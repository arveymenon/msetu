import { Injectable } from '@angular/core';
import { Observable, empty } from 'rxjs';
import { CommonApiService } from '../common-api/common-api.service';
import { ApiService } from '../api/api.service';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MyLibraryService {

  constructor(
      private commonService: CommonApiService,
      private api: ApiService
  ) { }

  
  getMyLibraryData(url:any,data:any):Observable<any>{
    console.log(`${this.commonService.baseUrl}$`);
    return this.api.post(`${this.commonService.baseUrl}${url}`, data).pipe(
     catchError((err: any) => {
      //  this.toastr.error(err.message);
       return empty();
     }),
     map((res: any) => {            
       return res;            
     }));
   }
}
