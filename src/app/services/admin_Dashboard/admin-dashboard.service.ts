import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class AdminDashboardService {

  constructor(private commonService: CommonApiService, private api: ApiService) { }
  //seq 
  addSequence(params) { 
    return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/UpdateContentSequence",params);
  }
  addMarquee(params) { 
    return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/CRUDMarquee",params);
  }
  addNewUpdates(params) { 
    return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/CRUDNewsUpdates",params);
  }
  addWhatsNew(params){
    return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/ADMIN_CRUD_WhatsNew",params);
  }
  addAwards(params){
    return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/CRUDAwards",params);
  }

  // all get api of CMS
  getAdminDashboardCMS(params){
    return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/Get_CMSCRUDList",params);
  }
  //landing page CMS
 
  addNewsAndEvents(params){
    return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/CRUDNewsAndEvents",params);
  }
  addLandingPAgeVideo(params){
    return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/CRUDLatestVideos",params);
  }      
  addLandingPageImages(params){
    return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/CRUDLoginPage",params);
  }
 getHistoryDataAPI(params){
  return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/GetAllCMSHistory",params);
 }
 //-----------------inactive cms-----------------
 updateInactive(params){
  return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/UpdateCMSStatus",params);
 }
 //----------maintainance List api-----------------------------
 getMaintainanceList(params){
  return this.api.post(this.commonService.baseUrl + "Admin/GetMaintenanceNotificationList",params);
 }
addMaintainanceToList(params){
  return this.api.post(this.commonService.baseUrl + "Admin/AddMaintenanceNotification",params);
}
updateMaintainance(params){
  return this.api.post(this.commonService.baseUrl + "Admin/UpdateMaintenanceNotification",params)
}
deleteMaintainance(params){
  return this.api.post(this.commonService.baseUrl + "Admin/DeleteMaintenanceNotification",params)
}
viewHistoryData(params){
  return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/GetAllCMSLikesReport",params)
}
}

