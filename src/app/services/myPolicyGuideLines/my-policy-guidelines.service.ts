import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { CommonApiService } from '../common-api/common-api.service';
import { Observable, empty } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MyPolicyGuidelinesService {

  constructor(
    private commonService: CommonApiService,
    private api: ApiService
  ) { }

  getGuidelines(url:any):Observable<any>{
    let body = {
      "BusinessCategoryID":"2",
      "BusinessSubCategoryID":"0",
      "DocTypeId":"0"
    }
    return this.api.post(`${this.commonService.baseUrl}${url}`, body).pipe(
     catchError((err: any) => {
      //  this.toastr.error(err.message);
       return empty();
     }),
     map((res: any) => {            
       return res;            
     }));
   }
   getSurveyData(params){
    return this.api.post(this.commonService.baseUrl + "SupplierCodeOfConduct/Get_Servey", params); 
  }
  submitSurvey(params){
    return this.api.post(this.commonService.baseUrl + "SupplierCodeOfConduct/Insert_SupplierCodeOfConductDtls", params); 
  }
  getSurveyList(params){
    return this.api.post(this.commonService.baseUrl + "SupplierCodeOfConduct/GetAll_Servey", params); 
  }
}
