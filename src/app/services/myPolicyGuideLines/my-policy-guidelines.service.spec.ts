import { TestBed } from '@angular/core/testing';

import { MyPolicyGuidelinesService } from './my-policy-guidelines.service';

describe('MyPolicyGuidelinesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyPolicyGuidelinesService = TestBed.get(MyPolicyGuidelinesService);
    expect(service).toBeTruthy();
  });
});
