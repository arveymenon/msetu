import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from '../api/api.service';
import { Observable, empty } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JitService {

  constructor(private commonService: CommonApiService, public http:HttpClient, private toastr: ToastrService,
    private api: ApiService) { }

  call(url:any,data? :any):Observable<any>{
    return this.api.post(`${this.commonService.baseUrl}${url}`,data || null).pipe(
     catchError((err: any) => {
       this.toastr.error(err.message);
       return empty();
     }),
     map((res: any) => {            
       return res;            
     })
     );
   }
}
