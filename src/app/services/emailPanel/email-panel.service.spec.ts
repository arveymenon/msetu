import { TestBed } from '@angular/core/testing';

import { EmailPanelService } from './email-panel.service';

describe('EmailPanelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmailPanelService = TestBed.get(EmailPanelService);
    expect(service).toBeTruthy();
  });
});
