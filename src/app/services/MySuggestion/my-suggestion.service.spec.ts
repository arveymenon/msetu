import { TestBed } from '@angular/core/testing';

import { MySuggestionService } from './my-suggestion.service';

describe('MySuggestionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MySuggestionService = TestBed.get(MySuggestionService);
    expect(service).toBeTruthy();
  });
});
