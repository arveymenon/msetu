import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';
import { ApiService } from '../api/api.service';
import { Observable, of, empty } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { HttpClient } from '@angular/common/http';
import * as CryptoJS from 'crypto-js';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class MySuggestionService {

  constructor(private commonService: CommonApiService,public http:HttpClient, private toastr: ToastrService,
    private api: ApiService) {
      if(localStorage.getItem('WW0xR2RGcFJQVDA9')){
        let name = localStorage.getItem('WW0xR2RGcFJQVDA9');
        this.userDetails['name'] = CryptoJS.AES.decrypt(name,"").toString(CryptoJS.enc.Utf8);
      }
      if(localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09')){
        let VendorCode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
        this.userDetails['VendorCode'] = CryptoJS.AES.decrypt(VendorCode,"").toString(CryptoJS.enc.Utf8);
      }
      if(localStorage.getItem('VlROV2QyTkhlSEJhV0VwUFdWY3hiQT09')){
        let SupplierName = localStorage.getItem('VlROV2QyTkhlSEJhV0VwUFdWY3hiQT09');
        this.userDetails['SupplierName'] = CryptoJS.AES.decrypt(SupplierName,"").toString(CryptoJS.enc.Utf8);
      }
      if(localStorage.getItem('WTIwNWMxcFZiR3M9')){
        this.roleID = localStorage.getItem('WTIwNWMxcFZiR3M9');
        this.roleID = CryptoJS.AES.decrypt(this.roleID,"").toString(CryptoJS.enc.Utf8);
      }
     this.SideMenus = JSON.parse(localStorage.getItem('SideMenus'));
    
     }
     isLogOut:boolean=false;

    DateFormat:any="Default";
    supplier_headr_img:any=''
    roleID:any;
    SideMenuForReports=[];
    userDetails={};
    loginDetails:any;
    isBSCReport:boolean;
    showSwapLoader:boolean=false;
    SideMenus= JSON.parse(localStorage.getItem('SideMenus'));
    Favorites = JSON.parse(localStorage.getItem('favourites'))

    validationMsgNewDealForm=[
      {"id":1, "value":"Supplier Location"},
      {"id":2, "value":"Workshop Conducted By"},
      {"id":3, "value":"Date of Workshop"},
      {"id":4, "value":"Date of IdeaPunching"},
      {"id":5, "value":"Proposal Description"},
      {"id":6, "value":"Function of Component"},
      {"id":7, "value":"Vehicle System"},
      {"id":8, "value":"Add File"},
      {"id":9, "value":"Sector/M&M Division"},
      {"id":10, "value":"Model Affected"},
      {"id":11, "value":"Savings As Per Supplier"},
      {"id":12, "value":"Savings As Per M&M"},
      {"id":13, "value":"Investments In Lacks As Per Supplier"},
      {"id":14, "value":"Investments In Lacks As Per M&M"},
      {"id":15, "value":"Monthly Production"},
      {"id":16, "value":"Parts Suppliers To Plants"},
      {"id":17, "value":"Bench Mark"},
      {"id":18, "value":"Other Bench Mark"}
    ]

    validationMsgCurrentForm=[
      {"id":1, "value":"Part Number"},
      {"id":2, "value":"Part Description"},
      {"id":3, "value":"Qty/Veh"},
      {"id":4, "value":"Part Cost"},
      {"id":5, "value":"Weight"},
      {"id":6, "value":"RM Grade"},
      {"id":7, "value":"Add File"}
    ]

    validationMsgProposalForm=[
      {"id":1, "value":"Part Number"},
      {"id":2, "value":"Part Description"},
      {"id":3, "value":"Qty/Veh"},
      {"id":4, "value":"Part Cost"},
      {"id":5, "value":"Weight"},
      {"id":6, "value":"RM Grade"},
      {"id":7, "value":"Add File"}
    ]

    // headers = new Headers({"Strict-Transport-Security": "max-age=31536000","X-XSS-Protection": "1; mode=block","X-Content-Type-Options": "nosniff"})


  supplierMeetPost(url:any,data?:any){
    return this.http.post(this.commonService.baseUrl+url, data)
  }
  supplierMeetGet(url:any,data?:any){
    return this.http.get(this.commonService.baseUrl+url)
  }

  postMySuggestionData(url:any,data:any):Observable<any>{
   return this.api.post(`${this.commonService.baseUrl}${url}`,data).pipe(
    catchError((err: any) => {
      this.showSwapLoader=false;
      // this.toastr.error(err.message);
      return empty();
    }),
    map((res: any) => {  
      console.log(res)          
      return res;            
    })
    );
  }

  getMySuggestionData(url:any):Observable<any>{
    return this.api.get(`${this.commonService.baseUrl}${url}`).pipe(
      catchError((err: any) => {
        this.toastr.error(JSON.stringify(err.message));
        return of(null);
      }),
      map((res: any) => {            
        return res;            
      }));
  }

  isUndefinedORNull(data:any){
    if(data === undefined || data == null || data.length <= 0|| data ==""){
      return true;
    }else{
      return false;
    }
  }

  showError(msg){
  this.toastr.error(msg);
  }
  showAndHoldError(msg,holdTime) {
    this.toastr.error(msg,"",{timeOut: holdTime})
  }

  showAndHoldSuccess(msg,holdTime) {
    this.toastr.error(msg,"",{timeOut: holdTime})
  }
  showSuccess(msg){
    this.toastr.success(msg);
    }
  exportAsExcelFile(utils:any,excelFileName: string): void {
    var worksheet: XLSX.WorkSheet = XLSX.utils.table_to_sheet(utils._elementRef.nativeElement);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

   saveAsExcelFile(buffer: any, excelFileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, excelFileName + '_export_' + new  Date().getTime() + EXCEL_EXTENSION);
 }

 public exportJsonAsExcelFile(json: any[], excelFileName: string): void {
  const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
  const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
  const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
  this.saveAsExcelFile(excelBuffer, excelFileName);
}

downloadFile(name){
  return this.commonService.baseUrl1 + 'UploadedFiles/CMSUpload/' + name;
}

save(name){
  const a = document.createElement('a');
  a.href = URL.createObjectURL(name);
  a.download = 'title';
  document.body.appendChild(a);
  a.click();
 }

 public getDefaultFormat(): string
 {
   return "DD/MM/YYYY"; // add you own logic here
 }

 public getFormat(): string
  {
    return "MM/YYYY"; // add you own logic here
  }
  public getLocale(): string
  {
    return "en-US"; // add you own logic here
  }  
 
  
  







}
