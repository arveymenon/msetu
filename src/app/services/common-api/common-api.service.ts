import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ApiService } from '../api/api.service';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommonApiService {

   navTitle = new Subject<string>();

  //  baseUrl = "http://103.81.89.64:443/api/"; //VAPT  http://103.81.89.64:8001
  //  baseUrl1 = "http://103.81.89.64:443/";

  //  baseUrl = "https://supplier.mahindra.com:8443/api/"; //Live
  //  baseUrl1 = "https://supplier.mahindra.com:8443/";
  // baseUrl2 = "https://supplier.mahindra.com/";

  //  baseUrl = "https://103.81.89.68:8443/api/"; // VAPT 
  //  baseUrl1 = "https://103.81.89.68:8443/";

  //  baseUrl = "http://103.81.89.64:8001/api/"; // VAPT 2 
  //  baseUrl1 = "http://103.81.89.64:8001/";
  
  baseUrl = "http://172.32.1.199:8001/api/"; // UAT
  baseUrl1 = "http://172.32.1.199:8001/";
  baseUrl2 = "http://172.32.1.199:8050/";

  //  baseUrl = "https://10.2.151.122:8001/api/"; //UAT 2
  //  baseUrl1 = "https://10.2.151.122:8001/";

  //  baseUrl = "https://103.81.89.68:8443/api/"; //UAT 3
  //  baseUrl1 = "https://103.81.89.68:8443/";

  //  baseUrl = "https://10.2.151.122:8443/api/"; //UAT 4
  //  baseUrl1 = "https://10.2.151.122:8443/";

  constructor(private api: ApiService, private route: Router) { }

  getTodos(){
    return this.api.get('https://jsonplaceholder.typicode.com/todos/');
  }

  setNavTitle(title){
    this.navTitle.next(title);
  } 
  
  logout(){
    this.route.navigate(['/login'])
  }
  

  
}
