import { Injectable } from '@angular/core';
import { Idle, DEFAULT_INTERRUPTSOURCES  } from '@ng-idle/core'
import { Router } from '@angular/router';
import { CommonUtilityService } from '../common/common-utility.service';
import { MySuggestionService } from '../MySuggestion/my-suggestion.service';
import * as CryptoJS from 'crypto-js';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class IdleSessionService {

  username:any;

  constructor(
    public idle: Idle,
    public router: Router,
    public commonService: CommonUtilityService,public cs: MySuggestionService,
    private modalService: NgbModal
  ) {  
    if(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==')){
      this.username = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
      this.username = CryptoJS.AES.decrypt(this.username,"").toString(CryptoJS.enc.Utf8);
    }  
    // idle.setIdle(900);
    // idle.setTimeout(3600);

    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    
    idle.onIdleEnd.subscribe(() => {
      console.log('start watching')
      this.idle.watch();
    });

    idle.onTimeout.subscribe(()=>{
      
      this.modalService.dismissAll()

      localStorage.clear();

      let isUserLogin = btoa("isUserLogin");
      // this.cookieService.set(isUserLogin,btoa("No"));
      this.commonService.changeIsAuthenticate(false);
      this.router.navigate(['/login']);
       let Request =
       {
         "TokenId": this.username
       }
        this.cs.postMySuggestionData('Login/Logout',Request).subscribe((res:any)=>{
          // if(!this.cs.isUndefinedORNull(res)){
          localStorage.clear();
          let isUserLogin = btoa("isUserLogin");
          // this.cookieService.set(isUserLogin,btoa("No"));
          this.commonService.changeIsAuthenticate(false);
          this.router.navigate(['/login']);
          // }
      // else{
      //   return;
      // }
        })
    
    })
    
    this.idle.watch();
   }
}
