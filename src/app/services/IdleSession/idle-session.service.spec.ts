import { TestBed } from '@angular/core/testing';

import { IdleSessionService } from './idle-session.service';

describe('IdleSessionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IdleSessionService = TestBed.get(IdleSessionService);
    expect(service).toBeTruthy();
  });
});
