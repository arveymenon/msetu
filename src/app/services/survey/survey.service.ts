import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';
import { Observable, empty } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ApiService } from '../api/api.service';
@Injectable({
  providedIn: 'root'
})
export class SurveyService {

  constructor(
    public commonService: CommonApiService,
    public api: ApiService
  ) { }

  call(url:any, body?: any):Observable<any>{
    console.log(`${this.commonService.baseUrl}$`);
    return this.api.post(`${this.commonService.baseUrl}${url}`, body || null).pipe(
      catchError((err: any) => {
        //  this.toastr.error(err.message);
        return empty();
      }),
      map((res: any) => {
        return res;
      }));
  }

  createFormOptions(url:any):Observable<any>{
    console.log(`${this.commonService.baseUrl}$`);
    return this.api.post(`${this.commonService.baseUrl}${url}`, null).pipe(
      catchError((err: any) => {
        //  this.toastr.error(err.message);
        return empty();
      }),
      map((res: any) => {
        return res;
      }));
    }
    
  postCreateSurveyForm(url:any, data? : any){
    return this.api.post(`${this.commonService.baseUrl}${url}`, data).pipe(
      catchError((err: any) => {
        //  this.toastr.error(err.message);
        return empty();
      }),
      map((res: any) => {
        return res;
      }));
    }
      
  getSurveysOfType(url:any,data?: any){
    return this.api.post(`${this.commonService.baseUrl}${url}`, data).pipe(
      catchError((err: any) => {
        //  this.toastr.error(err.message);
        return empty();
      }),
      map((res: any) => {
        return res;
      }));
    }

  // specific Survey Questions
  getQuestions(url: any, data: any) {
    return this.api.post(`${this.commonService.baseUrl}${url}`, data).pipe(
      catchError((err: any) => {
        //  this.toastr.error(err.message);
        return empty();
      }),
      map((res: any) => {
        return res;
        })
      );
    }
    
    deleteSurveyQuestion(url: any, data: any){
      return this.api.post(`${this.commonService.baseUrl}${url}`, data).pipe(
        catchError((err: any) => {
          //  this.toastr.error(err.message);
          return empty();
        }),
        map((res: any) => {
          return res;
          })
        );
      }
    
    publishSurvey(url: any, data: any){
      return this.api.post(`${this.commonService.baseUrl}${url}`, data).pipe(
        catchError((err: any) => {
          //  this.toastr.error(err.message);
          return empty();
        }),
        map((res: any) => {
          return res;
          })
        );
      }
    
    getSurveySectionsAndCategories(url: any, data: any){
        return this.api.post(`${this.commonService.baseUrl}${url}`, data).pipe(
          catchError((err: any) => {
            //  this.toastr.error(err.message);
            return empty();
          }),
          map((res: any) => {
            return res;
            })
          );
        }

    // survey for My Survey
    getMySurveyQuestions(url: any, data: any) {
          return this.api.post(`${this.commonService.baseUrl}${url}`, data).pipe(
            catchError((err: any) => {
              //  this.toastr.error(err.message);
              return empty();
            }),
            map((res: any) => {
              return res;
              })
            );
          }

    postMySurveyQuestions(url: any, data: any){
      return this.api.post(`${this.commonService.baseUrl}${url}`, data).pipe(
        catchError((err: any) => {
          //  this.toastr.error(err.message);
          return empty();
        }),
        map((res: any) => {
          return res;
          })
        );
      }
  
    // survey for SSI Survey
    getSSISurveyQuestions(url: any, data: any) {
      return this.api.post(`${this.commonService.baseUrl}${url}`, data).pipe(
        catchError((err: any) => {
          //  this.toastr.error(err.message);
          return empty();
        }),
        map((res: any) => {
          return res;
          })
        );
      }

}
