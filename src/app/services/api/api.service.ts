import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { share, map, catchError } from 'rxjs/operators';
import { Observable, empty } from "rxjs";
import * as CryptoJS from 'crypto-js';
import { Config } from 'protractor';
import { Router } from '@angular/router';
import { CommonUtilityService } from '../common/common-utility.service';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  userName:any;
  JWT:any;
  constructor(
    private http: HttpClient,
    private router: Router,
    private commonService: CommonUtilityService,
    private toast: ToastrService) { 
   

  }
  get(url: string): Observable<any> {
    let apiHttp =  this.http.get(url, this.getHeaders(url)).pipe(share());
    apiHttp.subscribe(res=>console.log(res));
    return apiHttp;
  }
 
  post(url: string, body: any): Observable<any> {
    
    let headers = this.getHeaders(url);
    var apiHttp: any;

    if( (url == 'Get_CMSCRUDList') || url.includes('Login/Login') ){
      apiHttp = this.http.post(url, body, this.getHeaders(url)).pipe(share());
    }
    else{
      apiHttp = this.http
      .post(url, body, {
        // headers: headers.headers,
        observe: "response",
      })
      .pipe(share(),
        catchError((err: any) => {          
          //  this.toastr.error(err.message);
          if(err.status == 401){
            console.log(err)
            //  this.toast.error("Oops!! Something Went Wrong. Kindly Re-Login");
            // this.commonService.changeIsAuthenticate(false);
            // this.router.navigate(["/login"]);
          }
          return empty();
        }),
        map((res)=>{
        if(res.headers.get('authtoken')){
          let authToken = res.headers.get('authtoken')
          localStorage.setItem('JWT', authToken)
        }
        return res.body
      }))
    }
    return apiHttp;
  }

  put(url: string, body: any): Observable<any> {
    let apiHttp = this.http.put(url, body, this.getHeaders(url)).pipe(share());
    return apiHttp;
  }

  delete(url: string): Observable<any> {
    let apiHttp = this.http.delete(url, this.getHeaders(url)).pipe(share());
    return apiHttp;
  }

  getHeaders(url:any) {
    if(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==')){
      this.userName = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
      this.userName = CryptoJS.AES.decrypt(this.userName,"").toString(CryptoJS.enc.Utf8);
    }
    this.JWT = localStorage.getItem('JWT');
    if(url.includes('Get_CMSCRUDList') || url.includes('Login/Login')){
      return {
        headers: 
        new HttpHeaders({
        "Strict-Transport-Security": "max-age=31536000",
        "X-XSS-Protection": "1; mode=block",
        "X-Content-Type-Options": "nosniff",
        "x-frame-options":"MAHINDRA",
        "Content-Security-Policy":"<policy-directive>; <policy-directive>",
        // "UserName": this.userName,
        // "Authorization": this.JWT
       })
      }
    }
    else if(url.includes('Login/Logout')){
      return {
        headers: 
        new HttpHeaders({
        "Strict-Transport-Security": "max-age=31536000",
        "X-XSS-Protection": "1; mode=block",
        "X-Content-Type-Options": "nosniff",
        "x-frame-options":"MAHINDRA",
        "Content-Security-Policy":"<policy-directive>; <policy-directive>",
        "UserName": this.userName,
        "Authorization":'Bearer ' + this.JWT
       })
      }
    }
    else if(url.includes('stakeholderUpload')){
      // return {
      //   headers: 
      //   new HttpHeaders({
      //   "Strict-Transport-Security": "max-age=31536000",
      //   "X-XSS-Protection": "1; mode=block",
      //   "X-Content-Type-Options": "nosniff",
      //   "x-frame-options":"MAHINDRA",
      //   "Content-Security-Policy":"<policy-directive>; <policy-directive>",
      //   // "UserName": this.userName,
      //   "Authorization":'Bearer ' + this.JWT
      //  })
      // }
    }
    else{
      return {
        headers: 
        new HttpHeaders({
        "Strict-Transport-Security": "max-age=31536000",
        "X-XSS-Protection": "1; mode=block",
        "X-Content-Type-Options": "nosniff",
        "x-frame-options":"MAHINDRA",
        "Content-Security-Policy":"<policy-directive>; <policy-directive>",
        "UserName": this.userName,
        "Authorization":'Bearer ' + this.JWT
       })
      }
    }
  
  }
}
