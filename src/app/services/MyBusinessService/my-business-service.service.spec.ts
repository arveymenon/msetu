import { TestBed } from '@angular/core/testing';

import { MyBusinessServiceService } from './my-business-service.service';

describe('MyBusinessServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyBusinessServiceService = TestBed.get(MyBusinessServiceService);
    expect(service).toBeTruthy();
  });
});
