import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';
import { ApiService } from '../api/api.service';
import { Observable, empty } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { MySuggestionService } from '../MySuggestion/my-suggestion.service';

@Injectable({
  providedIn: 'root'
})
export class MyBusinessServiceService {

  postMySuggestionData: any;
  constructor(public cs:MySuggestionService,private toastr: ToastrService,private commonService: CommonApiService, private api: ApiService) { }
  //------------------------------ Reports Api -----------------------------------------------------------
  getAllReportNameList() {  
    return this.api.post(this.commonService.baseUrl + "ReportMapping/GetReportList","");
  }
  //------------------------------ MRQ Report ------------------------------------------------------------
  getMRQReport(params) {  
    return this.api.post(this.commonService.baseUrl + "MRQ/GetMRQReport",params);
  }
  getMRQYearDropDown() {  
    return this.api.post(this.commonService.baseUrl + "MRQ/GerYearDDL","");
  }
  getMRQVariantList(params) {  
    return this.api.post(this.commonService.baseUrl + "MRQ/GetVariantsbyUser",params);
  }
  getMRQVariantDetails(params){
    return this.api.post(this.commonService.baseUrl + "MRQ/GetVariantDetailsbyVariantId",params);
  }
  insertVariantDetails(params){
    return this.api.post(this.commonService.baseUrl + "MRQ/InsertVariantDetails",params);
  }
  updateVariantDetails(params){
    return this.api.post(this.commonService.baseUrl + "MRQ/UpdateVariantDetails",params);
  }
  deleteVariantDetails(params){
    return this.api.post(this.commonService.baseUrl + "MRQ/DeleteVariantDetails",params);
  }
   changeStatusAdminReport(params){
        return this.api.post(this.commonService.baseUrl + "ChangeManagementAdminReport/ApproveRejectCM",params);
 } 
  //------------------------------ end of MRQ Report ------------------------------------------------------
  //------------------------------  ODS Report ------------------------------------------------------------
  getODSReport(params) {  
    return this.api.post(this.commonService.baseUrl + "ODS/GetODSReport",params);
  }
   //------------------------------ end of ODS Report ------------------------------------------------------
  //------------------------------ ODS ASN Report ----------------------------------------------------------
  getODSASNReport(params) {  
    return this.api.post(this.commonService.baseUrl + "ODSASN/GetODSASNReport",params);
  }
  insertODSASNReport(params) {  
    return this.api.post(this.commonService.baseUrl + "ODSASN/InsertODSASNVariantDetails",params);
  }
  updateODSASNReport(params) {  
    return this.api.post(this.commonService.baseUrl + "ODSASN/UpdateODSASNVariantDetails",params);
  }
  GetODSASNVariantDetailsbyVariantId(params){
    return this.api.post(this.commonService.baseUrl + "ODSASN/GetODSASNVariantDetailsbyVariantId",params);
  }
  GetODSASNVariantsbyUser(params){
    return this.api.post(this.commonService.baseUrl + "ODSASN/GetODSASNVariantsbyUser",params);
  }
  DeleteODSASNVariantDetails(params){
    return this.api.post(this.commonService.baseUrl + "ODSASN/DeleteODSASNVariantDetails",params);
  }

 //------------------------------ end of ODS ASN Report ------------------------------------------------------
  //------------------------------Change Management Supplier Report ------------------------------------------------------------
  GetChangeManagementReport(params){
    return this.api.post(this.commonService.baseUrl + "ChangeManagementSupplierReport/GetChangeManagementReport",params);
  }
  ViewChangeManagementDataById(params){
    return this.api.post(this.commonService.baseUrl + "ChangeManagementSupplierReport/ViewChangeManagementDataById",params);
  }
//----------------------------end of CMS Report-----------------------------------------------------------------
//------------------------------LR & LA Report ------------------------------------------------------------
GetLRRAReport(params){
  return this.api.post(this.commonService.baseUrl + "LRRA/GetLRRAReport",params);
}
GetLRRAVariantsbyUser(params){
  return this.api.post(this.commonService.baseUrl + "LRRA/GetLRRAVariantsbyUser",params);
}
getLRRAVariantDetailsbyVariantId(params){
  return this.api.post(this.commonService.baseUrl + "LRRA/GetLRRAVariantDetailsbyVariantId",params);
}
updateLRRAVariantDetails(params){
  return this.api.post(this.commonService.baseUrl + "LRRA/UpdateLRRAVariantDetails",params);
}
//----------------------------end of LR & LA Report-----------------------------------------------------------------
//-------------------------------------VQI report-------------------------------------------------------------

getVQIReport(params){
  return this.api.post(this.commonService.baseUrl + "VQI/GetVQIReport",params);
}
updateVQIDetails(params){
  return this.api.post(this.commonService.baseUrl + "VQI/UpdateVQIVariantDetails",params);
}
GetVQIVariantsbyUser(params){
  return this.api.post(this.commonService.baseUrl + "VQI/GetVQIVariantsbyUser",params);
}
GetVQIVariantDetailsbyVariantId(params){
  return this.api.post(this.commonService.baseUrl + "VQI/GetVQIVariantDetailsbyVariantId",params);
}
//---------------------------------end of VQI report------------------------------------------------------
//------------------------ My Business -> Transactions ->OE Supplies -----------------------------------
uploadOEDocument(params){
  return this.api.post(this.commonService.baseUrl + "",params);
}
SummaryInventoryDetails(params){
  return this.api.post(this.commonService.baseUrl + "BS4BulkUpload/DownloadExcel",params);
}
getBS4Data(params)
{
  return this.api.post(this.commonService.baseUrl + "PrintSticker/DownloadExcel",params);
}
saveAndUpdateBS4(params){
  return this.api.post(this.commonService.baseUrl + "PrintSticker/Insert_BS4UploadByTable",params);
}
bulkUploadBS4(params){
 
  return this.api.post(this.commonService.baseUrl + "PrintSticker/Excel_Upload_Trans",params);
}
//------------------------end of spare supplies ---------------------------------------------------
//------------------------ My Business -> Transactions ->Spare Supplies -----------------------------------
getPrintPartSticker(params){
  return this.api.post(this.commonService.baseUrl + "PrintSticker/Get_PrintBarcode",params);
}
//------------------------end of spare supplies ----------------------------------------------------------
//------------------------ My Business -> Transactions ->NPD -----------------------------------
getVendorCode(){
  return this.api.get(this.commonService.baseUrl + "DCPUpload/GetCDMMMasters");
}

getSubFolders(params){
  return this.api.post(this.commonService.baseUrl + "DCPUpload/GetCDMMSubFolder",params);
}
submitCDMM(params){
  return this.api.post(this.commonService.baseUrl + "DCPUpload/Insert_CDMMUpload",params);
}
getDataCDMM(params){
  return this.api.post(this.commonService.baseUrl + "DCPUpload/Get_UploadCDMM",params);
}
getAdminDataCDMM(params){
    return this.api.post(this.commonService.baseUrl + "DCPUpload/Get_UploadCDMMExt",params);
  } 
//------------------------end of NPD----------------------------------------------------------------------
//------------------------ My Business -> Transactions -> Quality -->Change Management -----------------------------------


// =========== Added by Akshay Mindhe ===========


isExpanded:boolean=true;

// async postDataToMyBussiness(url:any,postData:any){
// const data = this.api.post(`${this.commonService.baseUrl}${url}`, postData).toPromise();
// return data;
// }

postDataToMyBussiness(url:any,data:any):Observable<any>{
  return this.api.post(`${this.commonService.baseUrl}${url}`, data).pipe(
   catchError((err: any) => {
     this.toastr.error(err.message);
     this.cs.showSwapLoader=false;
     return empty();
   }),
   map((res: any) => {            
     return res;            
   })
   );
 }

 validationMsg=[
  {"id":1, "value":"Add Title"},
  {"id":2, "value":"Upload Document"},
  {"id":3, "value":"Folder"}
]



//------------------------ My Business -> Transactions -> Quality -->Change Management -----------------------------------

getSupplierCodeList(){
  return this.api.get(this.commonService.baseUrl + "ChangeManagement/GetSupplier");
}

getsupplierCodeList(params){
  return this.api.post(this.commonService.baseUrl + "changemanagement/GetSupplierCode",params);
}
getSupplierDetails(params){
  return this.api.post(this.commonService.baseUrl + "ChangeManagement/GetSupplierName",params);
}
submitChangeManagement(params){
  // ChangeManagement/InsertChangeManagement
  return this.api.post(this.commonService.baseUrl + "ChangeManagement/Insert_ChangeMgt", params);
}
getChangesRelatedToData(){
  return this.api.get(this.commonService.baseUrl + "ChangeManagement/GetChangeRelatedTo");
}
//------------------------end of Change Management----------------------------------------------------------------------
//------------------------ My Business -> Transactions -> Quality -->COP Upload ---------------------------
getVendorList(params){
  return this.api.post(this.commonService.baseUrl + "COP/GetSubVendorsDDL", params);
}
getPartsDDL(params){
  return this.api.post(this.commonService.baseUrl + "COP/GetPartsDDL", params);
}
insertCOP(params){
  return this.api.post(this.commonService.baseUrl + "COP/UploadCOP", params);
}
getPartDetails(params){
  return this.api.post(this.commonService.baseUrl + "COP/GetPartDescription", params);
}
getCOPDetails(params){
  return this.api.post(this.commonService.baseUrl + "COP/GetCOPDetails", params);
}
updateCOP(params){
  return this.api.post(this.commonService.baseUrl + "COP/UpdateCopStatus", params);
}

getAdimCOPDetails(params){
  return this.api.post(this.commonService.baseUrl + "COP/Get_CopDetailsExt", params);
}
getMailFlag(params){
  return this.api.post(this.commonService.baseUrl + "cop/CheckCOPMailStatus", params);
}
updateMailFlag(params){
  return this.api.post(this.commonService.baseUrl + "cop/InsertCOPMailStatus", params);
}
//------------------------end of COP Upload-----------------------------------------------------------------
//-------------------------My Business ->Transactions ->Qulity-->DCP Upload---------------------------------

GetMasterUpload(){
  return this.api.get(this.commonService.baseUrl + "DCPUpload/GetMasterUpload");
}
insertDCPUpload(params){
  return this.api.post(this.commonService.baseUrl + "DCPUpload/Insert_DCPUpload",params);
}
getDCPUpload(params){
  return this.api.post(this.commonService.baseUrl + "DCPUpload/Get_UploadDCP",params);
}

  getDocumentReference(params) {
    return this.api.post(this.commonService.baseUrl + "DocumentManagement/GetMyDocs", params);
  } 

  getCMDataSource(params){
    return this.api.post(this.commonService.baseUrl + "ChangeManagement/Get_TransMapping", params);
  }
  submitCMData(params){
    return this.api.post(this.commonService.baseUrl + "ChangeManagement/Insert_TransMapping", params); 
  }
  getFolderList(params){
    return this.api.post(this.commonService.baseUrl + "DocumentManagement/GetFolders", params);
  }

  deleteData(params){
    return this.api.post(this.commonService.baseUrl + "ChangeManagement/delete_TransMapping", params); 
  }
  
  getCOPListDataSource(params){
    return this.api.post(this.commonService.baseUrl + "COP/getCopDashboard", params); 
  }

  sendCOPEmail(params){
    return this.api.post(this.commonService.baseUrl + "COP/COP_MailTrigger", params); 
  }
//--------------------------IATF api list-------------------------
getCertificateType(){
  return this.api.post(this.commonService.baseUrl + "IATF/IATFCertificateType",""); 
}

uploadIATFDocument(params){
  return this.api.post(this.commonService.baseUrl + "IATF/UploadIATF", params); 
}

getIATFUploadedData(params){
  return this.api.post(this.commonService.baseUrl + "IATF/GetAll_IATFByFilter", params); 
}
downloadIATF(params){
  return this.api.post(this.commonService.baseUrl + "IATF/IATFCertificatesDownload", params);
}
approveRejectDataForIATF(params){
  return this.api.post(this.commonService.baseUrl + "IATF/ApproveRejectATF", params); 
}
}
