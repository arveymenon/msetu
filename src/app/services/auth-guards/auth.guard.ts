import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  CanActivate,
  Router,
} from "@angular/router";
import { CommonUtilityService } from "../common/common-utility.service";
import { MySuggestionService } from "../MySuggestion/my-suggestion.service";
import { MyHelpDeskService } from "../myHelpDesk/my-help-desk.service";
import * as CryptoJS from 'crypto-js';
 
@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {
  commonService = new CommonUtilityService();
  roleId: any;
  route_array: any;
 
  constructor(
    public cs: MySuggestionService,
    public router: Router,
    private http: MyHelpDeskService
  ) {}
 
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    this.roleId = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8);
    let route = next.data["roles"];
    let user = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
    user = CryptoJS.AES.decrypt(user,"").toString(CryptoJS.enc.Utf8);
 
    let body = {
      userClicked: user,
      device: "windows",
      browser: localStorage.getItem("browser") || "chrome",
      moduleType: "OT",
      module: route[0],
    };
    this.http.call("Analytics/InsertAnalytics", body).subscribe()
 
    this.route_array = localStorage.getItem("route_array");
    if (!this.roleId) {
      this.commonService.changeIsAuthenticate(false);
      this.router.navigate(["/login"]);
      return false;
    } else if (this.roleId == 7 && route[0] == "adminDashboard") {
      return true;
    } else if (this.roleId != 7 && route[0] == "dashboard") {
      return true;
    } else {
      return true;
    }
    // else if (this.route_array.includes(route[0])) {
    //   return true;
    // }
  }

  // postAnalytics(module) {
  //   let body = {
  //     userClicked: localStorage.getItem("userToken"),
  //     device: "windows",
  //     browser: localStorage.getItem("browser") || "chrome",
  //     moduleType: "OT",
  //     module: module,
  //   };
  //   this.http.call("Analytics/InsertAnalytics", body);
  // }
}