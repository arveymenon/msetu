import { Injectable } from "@angular/core";
import { CommonApiService } from "../common-api/common-api.service";
import { ApiService } from "../api/api.service";
import { share } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: "root",
})
export class LoginServiceService {
  constructor(
    private commonService: CommonApiService,
    private api: ApiService,
    public http: HttpClient
  ) {}

  getLoginCMS(params) {
    return this.api.post(
      this.commonService.baseUrl + "CMSDashBoardCRUD/Get_CMSCRUDList",
      params
    );
}

  sso(body) :Observable<any>{
    return this.http.post(this.commonService.baseUrl + "Login/SSOLogin", body)
  }

  authenticate(params) {
    return this.api.post(this.commonService.baseUrl + "Login/Login", params);
  }
  sendOtp(params) {
    // return this.api.post(this.commonService.baseUrl + "Login/SendOTP", params);
    return this.api.post(this.commonService.baseUrl + "Login/ReSendOTP", params);
  }

  validateOTP(params) {
    return this.api.post(
      this.commonService.baseUrl + "login/ValidateOTP",
      params
    );
  }
}
