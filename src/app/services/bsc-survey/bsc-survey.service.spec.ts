import { TestBed } from '@angular/core/testing';

import { BscSurveyService } from './bsc-survey.service';

describe('BscSurveyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BscSurveyService = TestBed.get(BscSurveyService);
    expect(service).toBeTruthy();
  });
});
