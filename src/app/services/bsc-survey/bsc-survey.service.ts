import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';
import { ApiService } from '../api/api.service';
import { Observable, empty } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class BscSurveyService {

  constructor(
    private commonService: CommonApiService,
    private api: ApiService,
    private toastr: ToastrService,
) { }

  getOpenSurvey(url:any, data: any):Observable<any>{
    console.log(data);
    return this.api.post(`${this.commonService.baseUrl}${url}`, data).pipe(
    catchError((err: any) => {
       this.toastr.error(err.message);
      return empty();
    }),
    map((res: any) => {            
      return res;            
    }));
  }
  
  getSurveyQuestion(url: any){
    return this.api.post(`${this.commonService.baseUrl}${url}`,null).pipe(
    catchError((err: any) => {
       this.toastr.error(err.message);
      return empty();
    }),
    map((res: any) => {            
      return res;            
    }));
  }
  
  insertSurvey(url: any, data: any){
    return this.api.post(`${this.commonService.baseUrl}${url}`,data).pipe(
    catchError((err: any) => {
       this.toastr.error(err.message);
      return empty();
    }),
    map((res: any) => {            
      return res;            
    }));
  }

}
