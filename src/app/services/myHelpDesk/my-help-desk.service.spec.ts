import { TestBed } from '@angular/core/testing';

import { MyHelpDeskService } from './my-help-desk.service';

describe('MyHelpDeskService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyHelpDeskService = TestBed.get(MyHelpDeskService);
    expect(service).toBeTruthy();
  });
});
