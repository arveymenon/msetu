import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-notification-modal',
  templateUrl: './notification-modal.component.html',
  styleUrls: ['./notification-modal.component.scss']
})
export class NotificationModalComponent implements OnInit {
  @Input() public incidentNumber;
  constructor(
    private modal: NgbModal
  ) { }
 

  ngOnInit() {
    console.log(this.incidentNumber);
       setTimeout(()=>{
        this.dismiss();

  }, 20000);
  }

  dismiss(){
    this.modal.dismissAll();
  }

}
