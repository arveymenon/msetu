import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  
  constructor(private commonService: CommonApiService, private api: ApiService) { }
  getNewUpdates(params) {  
    return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/GetAllDashboardContent",params);
  }
  get() {  
    return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/GetAllDashboardContent","");
  }
  updateLikesAndView(params) {  
    return this.api.post(this.commonService.baseUrl + "CMSDashBoardCRUD/UpdateLikeViews",params);
  }

  getNotificationdata(params){
    return this.api.post(this.commonService.baseUrl + "Notification/GetNotifications",params);
  }
  updateNotification(params){
    return this.api.post(this.commonService.baseUrl + "Notification/UpdateNotifications",params);
  }

}
