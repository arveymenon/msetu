import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import * as CryptoJS from "crypto-js";
import { MySuggestionService } from '../MySuggestion/my-suggestion.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class ToastServiceService {
  private username: string
  private vendorCode: string
  private userId: string
  private srm: string

  // public url: SafeResourceUrl | {changingThisBreaksApplicationSecurity: string}
  public url: SafeResourceUrl | any
  
  constructor(public toast: ToastrService,public cs: MySuggestionService, private domSanitizer: DomSanitizer) {
    
    // while(!this.url){
      // }
      this.initValue()
    }
    
    async initValue(){
      this.username = CryptoJS.AES.decrypt(localStorage.getItem("WW0xR2RGcFJQVDA9"),"").toString(CryptoJS.enc.Utf8) ||'username'
      this.vendorCode = CryptoJS.AES.decrypt(localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09'),"").toString(CryptoJS.enc.Utf8) || 'vendorCode'
      this.userId = CryptoJS.AES.decrypt(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ=='),"").toString(CryptoJS.enc.Utf8).split('-')[0] || 'userId'
      this.srm = CryptoJS.AES.decrypt(localStorage.getItem('SM'),"").toString(CryptoJS.enc.Utf8) || 'sharda';
      const userId = this.userId.split('\\')[0] +','+ this.userId.split('\\')[1]
      
      this.setValues(
          "https://supplier.mahindra.com/CARA_WEB/CARA.html?username="+this.username+"&uname="+this.vendorCode+"&password=Password1&srmname="+this.srm+"&token="+userId+"&usersource=web&embedded=true"
        )
  }
    
  setValues(msg){
    this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(
      msg
      ) || 'testing';
  }

  success(msg){
    this.toast.success(msg)
  }

  error(msg){
    if(!this.url){
    }
    this.initValue().then(async ()=>{
      await console.log(this.url)
      if(typeof this.url.changingThisBreaksApplicationSecurity){
        console.log(this.url.changingThisBreaksApplicationSecurity)
        console.log(msg)
        this.setValues(
          this.url.changingThisBreaksApplicationSecurity+"&error="+msg
        )
      }
      // this.url = this.url+"&error="+msg
      if(msg !='')
      this.toast.error(msg)
      console.log(this.url)
  
      await this.openChatbot('')
    })
  }
  errorFromOtherPage(msg){
    if(!this.url){
    }
    this.initValue().then(async ()=>{
      await console.log(this.url)
      if(typeof this.url.changingThisBreaksApplicationSecurity){
        console.log(this.url.changingThisBreaksApplicationSecurity)
        console.log(msg)
        this.setValues(
          this.url.changingThisBreaksApplicationSecurity+"&error="+msg
        )
      }
      // this.url = this.url+"&error="+msg
      if(msg !='')
      this.toast.error(msg)
      console.log(this.url)
  
      await this.openChatbotFromOtherPage('True')
    })
  }
  
  show(msg){
    this.toast.show(msg)
  }
  
  chatbotvisibility: boolean = false;
  chatbotPage:boolean = false;
  openChatbot(page) {
    // alert(page);
    let analysticsRequest={
      "userClicked": this.username,//  localStorage.getItem('userToken'),
      "device": "windows",
      "browser": localStorage.getItem('browser'),
      "moduleType": "CA",
      "module": "CARA"
    }
    this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})
    this.chatbotvisibility = true;
    this.chatbotPage = false;
    this.access(page);
  }
  openChatbotFromOtherPage(page) {
    // alert(page);
    let analysticsRequest={
      "userClicked": this.username,//  localStorage.getItem('userToken'),
      "device": "windows",
      "browser": localStorage.getItem('browser'),
      "moduleType": "CA",
      "module": "CARA"
    }
    this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})
    this.chatbotvisibility = true;
    this.chatbotPage = true;
    this.access(page);
  }

  access(type) {
    // alert("call");
    let msg;
    if(type == 'dashboard')
      msg ='';
    var iframe = document.getElementById("iframe") as HTMLIFrameElement;
    var style = document.createElement("style");
    style.textContent =
      "body {" + "  background-color: red;" + "  overflow: hidden;" + "}";
    iframe.contentDocument.head.appendChild(style);
  }

  closeChatBot(){
    this.chatbotvisibility = false;
    this.chatbotPage = false;
    const userId = this.userId.split('\\')[0] +','+ this.userId.split('\\')[1]
    
    this.setValues(
      "https://supplier.mahindra.com/CARA_WEB/CARA.html?username="+this.username+"&uname="+this.vendorCode+"&password=Password1&srmname="+this.srm+"&token="+userId+"&usersource=web&embedded=true"
    )
  }
}
