import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonUtilityService {
  navchange: EventEmitter<any> = new EventEmitter(); 
  stakeholderNav: EventEmitter<any> = new EventEmitter(); 
  public IsAuthenticate:boolean=false;
  public showmenustakeholder:boolean=false;
  public fileSize = 20000000;
  // public IsLoginFlag:boolean;
  constructor() { }
  changeIsAuthenticate(val:boolean){
    this.IsAuthenticate=val;
 //   this.IsLoginFlag=val1;
    this.navchange.emit(this.IsAuthenticate);
  }
  
  showMenuForSatkeholder(val:boolean){
    this.showmenustakeholder=val;
    this.stakeholderNav.emit(this.showmenustakeholder);
  }
  // changeIsLoginFlag(val:boolean){
  //   this.IsLoginFlag=val;
  //   this.navchange.emit(this.IsLoginFlag);
  // }

}
