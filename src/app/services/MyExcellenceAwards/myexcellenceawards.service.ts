import { Injectable } from '@angular/core';
import { OnInit } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class MyexcellenceawardsService implements OnInit  {

  
  constructor(private commonService: CommonApiService, private api: ApiService) { }


  getexcellenceawards() { 
    // http://localhost:52586/api
   // return this.api.get("");
    return this.api.post(this.commonService.baseUrl+"ExcellenceAwards/InsertExcellenceAwards","");
  }

  ngOnInit(){
    
  }
}
