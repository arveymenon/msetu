import { TestBed } from '@angular/core/testing';

import { MyexcellenceawardsService } from './myexcellenceawards.service';

describe('MyexcellenceawardsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyexcellenceawardsService = TestBed.get(MyexcellenceawardsService);
    expect(service).toBeTruthy();
  });
});
