import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quick-links-landing',
  templateUrl: './quick-links-landing.component.html',
  styleUrls: ['./quick-links-landing.component.scss']
})
export class QuickLinksLandingComponent implements OnInit {
  images = [
    "https://picsum.photos/900/500?random&t=1",
    "https://picsum.photos/900/500?random&t=2",
    "https://picsum.photos/900/500?random&t=3"
  ]
  constructor() { }

  ngOnInit() {
  }

}
