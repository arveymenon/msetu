import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickLinksLandingComponent } from './quick-links-landing.component';

describe('QuickLinksLandingComponent', () => {
  let component: QuickLinksLandingComponent;
  let fixture: ComponentFixture<QuickLinksLandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickLinksLandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickLinksLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
