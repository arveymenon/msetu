import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierCodeConductComponent } from './supplier-code-conduct.component';

describe('SupplierCodeConductComponent', () => {
  let component: SupplierCodeConductComponent;
  let fixture: ComponentFixture<SupplierCodeConductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierCodeConductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierCodeConductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
