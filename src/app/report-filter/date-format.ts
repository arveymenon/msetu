import { Injectable } from '@angular/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as moment from 'moment';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';

@Injectable()
export class CustomDateAdapter extends MomentDateAdapter
{
  constructor(private cs: MySuggestionService)
  {
    super('en-US');
  }

  public format(date: moment.Moment, displayFormat: string): string
  { 
      let format;
      if(this.cs.DateFormat == 'Default'){
        format = this.cs.getDefaultFormat();
      }else{
         format = this.cs.getFormat();
      }
      const locale = this.cs.getLocale();

    return date.locale(locale).format(format);
  }
}