import { Component, OnInit,Injector } from '@angular/core';
import * as reportFilter from '../ReportFilter.json';
import { FormBuilder,Validators,FormControl } from '@angular/forms';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { DataServiceService } from '../services/DataService/data-service.service';
import { ApiService } from '../services/api/api.service';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import * as _moment from 'moment';
import { Moment} from 'moment';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {MatDatepicker} from '@angular/material/datepicker';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS} from '../../app/cmsdashboard/date-format';
import { CustomDateAdapter } from './date-format';

import * as CryptoJS from 'crypto-js';


const moment =  _moment;

// export const MY_FORMATS = {
//   parse: {
//     dateInput: 'MM/YYYY',
//   },
//   display: {
//     dateInput: 'MM/YYYY',
//     monthYearLabel: 'MMM YYYY',
//     dateA11yLabel: 'LL',
//     monthYearA11yLabel: 'MMMM YYYY',
//   },
// };

@Component({
  selector: 'app-report-filter',
  templateUrl: './report-filter.component.html',
  styleUrls: ['./report-filter.component.scss'],
  providers:
  [
    CustomDateAdapter, // so we could inject services to 'CustomDateAdapter'
    { provide: DateAdapter, useClass: CustomDateAdapter }, // Parse MatDatePicker Format
  ]
})
export class ReportFilterComponent implements OnInit {
  roleId = CryptoJS.AES.decrypt(localStorage.getItem('PR'),'').toString(CryptoJS.enc.Utf8)
  specialCharacters = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;

   constructor(private injector: Injector, public cs:MySuggestionService, public api:ApiService,public _dataService: DataServiceService,private _formBuilder: FormBuilder,public commonService: CommonUtilityService,private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer,private modalService: NgbModal) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon('excelIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/Excel_White_Icon.svg'));
   }

  FilterForm:any;
  FilterArray=[];
  sideMenu=[];
  varientDDLValue=[];
  array1=[
  {"value":"01","month":"January"},{"value":"02","month":"February"},{"value":"03","month":"March"},{"value":"04","month":"April"},{"value":"05","month":"May"},{"value":"06","month":"June"},
  {"value":"07","month":"July"},{"value":"08","month":"August"},{"value":"09","month":"September"},{"value":"10","month":"October"},
  {"value":"11","month":"November"},{"value":"12","month":"December"}];

  array2=[];
  Date:any;
  reportName:any;

  ngOnInit() {

    this.Date = new Date()

  this.cs.DateFormat="Default"; 
  this.getMRQYear();
   let arrayName =  this._dataService.Reportdata;
   this.reportName = arrayName['childOption'];
   this.FilterArray = reportFilter['default'][arrayName['option']][arrayName['childOption']];
   this.sideMenu = reportFilter['default'][arrayName['option']].SidemenuData;
   this.FilterForm =  this._formBuilder.group({});
   let formControlFields = [];
   for (let k = 0; k < reportFilter['default'][arrayName['option']][arrayName['childOption']].length; k++) {
           formControlFields.push({ name: reportFilter['default'][arrayName['option']][arrayName['childOption']][k].FormFieldName, control: new FormControl("", reportFilter['default'][arrayName['option']][arrayName['childOption']][k].isRequiredFiled=='true'?Validators.required:[]) });
          // formControlFields.push({ name: reportFilter['default'][arrayName['option']][arrayName['childOption']][k].FormFieldName, control: new FormControl("", [])}); 
        }
         formControlFields.forEach(f => this.FilterForm.addControl(f.name, f.control));
         
  }

  getMRQYear(){
    this.cs.postMySuggestionData("MRQ/GerYearDDL", "").subscribe((data: any) => {   
    this.array2 = data.ResponseData;  
    })
  }
  Month_From:any = new FormControl();
  Month_To = new FormControl();
  Schedule_Month_From:any = new FormControl();

  month_from='';
  month_to='';

  YearHandler(){
    this.cs.DateFormat="Default";
  }

  MonthHandler(){
    this.cs.DateFormat="Default";
  }

  DateHandler(){
    this.cs.DateFormat="Default"; 
  }

  chosenYearHandler(normalizedYear: Moment) {
    this.cs.DateFormat=""
    this.Month_From = new FormControl(moment(normalizedYear));
  }

  chosenMonthHandler(normalizedMonth: any,datepicker: MatDatepicker<Moment>) {
    this.cs.DateFormat=""
    this.Month_From = new FormControl(moment(normalizedMonth));
    datepicker.close();
  }

  chosenYearHandlerTo(normalizedYear: Moment) {
    this.cs.DateFormat=""
    this.Month_To = new FormControl(moment(normalizedYear));
  }

  chosenMonthHandlerTo(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    this.cs.DateFormat=""
    this.Month_To = new FormControl(moment(normalizedMonth));
    datepicker.close();
  }

  valueChanged(id){
    if(id == 1){
      this.cs.DateFormat="Default"
    }
    else{
      this.cs.DateFormat=''
    }
  }

  applicableForRoles(roles){
    if(roles && roles.length > 0){
      if(roles.includes(parseInt(this.roleId))){
        return true
      } else {
        return false
      }
    }else{
      return true
    }
  }

  validateKeyPress(string,byPass){
    if(!byPass){
      if(this.specialCharacters.test(string)){
        return false;
      }
    }
  }
}
