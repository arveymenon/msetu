import { Component, OnInit, ViewChild,AfterViewInit } from '@angular/core';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { MatIconRegistry, MatTableDataSource, MatPaginator } from '@angular/material';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataServiceService } from '../services/DataService/data-service.service';
import { ApiService } from '../services/api/api.service';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import { DateAdapter, MAT_DATE_FORMATS } from "@angular/material";
import { AppDateAdapter,APP_DATE_FORMATS } from '../cmsdashboard/date-format';
import { FormControl, FormArray, FormBuilder } from '@angular/forms';
import * as reportFilter from '../ReportFilter.json';
import * as moment from 'moment';
import { MyBusinessServiceService } from '../services/MyBusinessService/my-business-service.service';
import { ChangeRequestModalComponent } from '../modal/change-request-modal/change-request-modal.component';
import * as CryptoJS from 'crypto-js';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-report-new',
  templateUrl: './report-new.component.html',
  styleUrls: ['./report-new.component.scss'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]
})
export class ReportNewComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator
  DataSource = [
    ];
  displayedColumns =[];
  request={};
  VariantName: any;
  Insertrequest={};
  Updaterequest={};
  DatasourceResponseData:any=[];
  Getrequest={};
  CategoryName:any;
  SearchForm:any;
  searchRequest= {};
  array2=[];
  pageNo: number;
  pageSize: any;
  Variant: any;
  checkedData=[];
  vendorCode: any;
  Tier_II_list_DataSource :MatTableDataSource<any>;
  RoleID: any;
  JSONSource=[];
  browser: any;
  amendmentID: any;
  POvarientName: any;
  masterVendorCode = CryptoJS.AES.decrypt(localStorage.getItem('MV'), "").toString(CryptoJS.enc.Utf8);

  del_sch_adh_url: SafeResourceUrl 
  // ='http://10.142.9.12:8052/Reports/DelSchAdhReport.aspx?VendorCode='+ this.masterVendorCode;

  constructor(public myBusinessService:MyBusinessServiceService,private _formBuilder: FormBuilder,public cs:MySuggestionService, public api:ApiService, public _dataService: DataServiceService,public commonService: CommonUtilityService,
    private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer,private modalService: NgbModal, protected toastr: ToastrService) { 
    this.commonService.changeIsAuthenticate(true);
      
    matIconRegistry.addSvgIcon('viewIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/ViewIcon.svg'));
    matIconRegistry.addSvgIcon('excelIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/Excel_White_Icon.svg'));
    matIconRegistry.addSvgIcon('downloadIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/downloadicon.svg'));
  }
  CRMdisplayedColumns   : string[] =['CMRSupplierCode','CMRSupplierName','CMRLocation','CMRChangeMonth','CMRChangeRelated','CMRCommunication','CMRAffectPerformance','CMRPlant','action'];
  showFilter:boolean=true;
  VarientDropDownValue:any='';
  varientDDLValue=[]
  @ViewChild('filterReport')filterReport;
  @ViewChild('table')table:any;
  sideMenu=[];
  searchArray=[];
  reportPlant:any;
  UserId:any;
  array1=[{"value":"01","month":"January"},{"value":"02","month":"February"},{"value":"03","month":"March"},{"value":"04","month":"April"},{"value":"05","month":"May"},{"value":"06","month":"June"},
  {"value":"07","month":"July"},{"value":"08","month":"August"},{"value":"09","month":"September"},{"value":"10","month":"October"},
  {"value":"11","month":"November"},{"value":"12","month":"December"}
];
PrimaryRoleID:any;

supplierTierdisplayedColumns=["ID","vendorcode","VendorName","comodity","typofSupplies","vehicleModel","state","city"]

  ngOnInit() {
    this.UserId = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
    this.UserId = CryptoJS.AES.decrypt(this.UserId,"").toString(CryptoJS.enc.Utf8);
    this.vendorCode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
    this.vendorCode = CryptoJS.AES.decrypt(this.vendorCode,"").toString(CryptoJS.enc.Utf8);
    this.RoleID = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID,"").toString(CryptoJS.enc.Utf8)
    this.browser = localStorage.getItem('browser');
    this.PrimaryRoleID = localStorage.getItem('PR');
    this.PrimaryRoleID = CryptoJS.AES.decrypt(this.PrimaryRoleID,"").toString(CryptoJS.enc.Utf8);
    this.getMRQYear();
    let arrayName = this._dataService.Reportdata;
    this.searchArray = reportFilter['default'][arrayName['option']][arrayName['childOption']];
    let CategoryName: any = this._dataService.Reportdata;
    this.CategoryName = CategoryName.childOption;
    if (this.CategoryName == "Change management - Supplier Report" || this.CategoryName == "Change management - Plant wise report" || this.CategoryName == "Change management - Admin Report") {
      this.sideMenu = reportFilter['default'][arrayName['option']].SidemenuData;
      this.displayedColumns = this.searchArray[0].DatasourceColumns;
      localStorage.setItem('DatasourceResponseColumns', this.searchArray[0].DatasourceResponseColumns)
      this.callCMR({ pageIndex: 0, pageSize: 10, length: this.pageSize },false);
    }
    if (this.CategoryName == "Tier II list") {
      this.getqualityTierData({pageIndex:0});
    }
    if (this.CategoryName=='Del Sch Adh') {
      this.showFilter = false
      // this.getqualityTierData({pageIndex:0});
    }
    this.getPageChangeData({pageIndex:0});
    this.del_sch_adh_url= this.domSanitizer.bypassSecurityTrustResourceUrl('http://10.142.9.12:8052/Reports/DelSchAdhReport.aspx?VendorCode='+ this.masterVendorCode+'&Token='+this.UserId)
  }

  searchQualityData(data){
    let Request={
      "SupCode": this.vendorCode,
      "SchID": "FY 2020",
      "SupId":null,
      "GetAllData":"True",
      "SearchVal":data,
      "PageNo":1
    }
    this.showFilter=false
    this.cs.postMySuggestionData('QualityTier2/GetSupplierTierInfo',Request).subscribe((res:any)=>{
         if(!this.cs.isUndefinedORNull(res)&&res.Message=="Success"){
          if(res.ResponseData.length>0)
           this.pageSize = res.ResponseData[0].TotalCount;
           this.Tier_II_list_DataSource = new MatTableDataSource(res.ResponseData);
           this.Tier_II_list_DataSource.filterPredicate = (d: any, filter: string) => {
            const textToSearch = d['VendorName'] && d['VendorName'].toLowerCase() || '';
            return textToSearch.indexOf(filter) !== -1;
          };
           this.showFilter=false;
         }
    })
  }

  getqualityTierData(event){
    let Request={
      "SupCode": this.vendorCode,
      "SchID": "FY 2020",
      "SupId":null,
      "GetAllData":"True",
      "SearchVal":"",
      "PageNo":event.pageIndex+1
    }
    this.showFilter=false
    this.cs.postMySuggestionData('QualityTier2/GetSupplierTierInfo',Request).subscribe((res:any)=>{
         if(!this.cs.isUndefinedORNull(res)&&res.Message=="Success"){
           if(res.ResponseData.length>0)
           this.pageSize = res.ResponseData[0].TotalCount;
           this.Tier_II_list_DataSource = new MatTableDataSource(res.ResponseData);
           this.Tier_II_list_DataSource.filterPredicate = (d: any, filter: string) => {
            const textToSearch = d['VendorName'] && d['VendorName'].toLowerCase() || '';
            return textToSearch.indexOf(filter) !== -1;
          };
           this.showFilter=false;
         }
    })
  }

  applyFilter(filterValue: string) {
    this.Tier_II_list_DataSource.filter = filterValue.trim().toLowerCase();
  }

  callCMR(event,isExcel){
      this.pageNo = event.pageIndex;

    if(this.CategoryName == "Change management - Supplier Report"){
      let reqjson={
        "RequestBy": this.RoleID != 7? this.vendorCode : '',
        "PageNo" : isExcel? 0: this.pageNo+1
      }
      this.cs.postMySuggestionData("ChangeManagementSupplierReport/GetChangeManagementReport", reqjson).subscribe((data: any) => {
        this.pageSize = data.ResponseData['Count'];
        this.DataSource = data.ResponseData['ChangeManagementList'];
        this.showFilter=false;
        if(isExcel){
             let dataNew = data.ResponseData['ChangeManagementList'];
             console.log(dataNew);
             //Create workbook and worksheet
             let workbook = new Workbook();
             let worksheet = workbook.addWorksheet(this.CategoryName+'ReportData');

             let headers = Object.keys(data.ResponseData['ChangeManagementList'][0]);
             headers.splice(18,2)
            let headerRow = worksheet.addRow(headers);
               // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: 'pattern',
              pattern: 'solid',
              fgColor: { argb: '00FF0000' },
              bgColor: { argb: '00FF0000' }
            }
            cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
          })
           // Add Data and Conditional Formatting
           dataNew.forEach(d => {
             // var obj = JSON.parse(d);
             var values = Object.keys(d).map(function (key) { return d[key] || 'N/A'; });
             console.log(values);
             values[1] = moment(values[1]).format('DD-MM-YYYY');
             values[14] = moment(values[14]).format('DD-MM-YYYY');
             values.splice(18,2)
            let row = worksheet.addRow(values);
           });
            workbook.xlsx.writeBuffer().then((dataNew) => {
              let blob = new Blob([dataNew], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
              fs.saveAs(blob, this.CategoryName +'reportData.xlsx');
            });


          // this.cs.exportJsonAsExcelFile(data.ResponseData['ChangeManagementList'],'Changmangment Report')
        }
       })
    }
    else if(this.CategoryName == "Change management - Admin Report"){
      let reqjson={
        "PageNo" :isExcel?0:this.pageNo+1
      }
      this.cs.postMySuggestionData("ChangeManagementAdminReport/GetCMAdminReport", reqjson).subscribe((data: any) => {
        this.pageSize = data.ResponseData['Count'];
        if(!isExcel)
        this.DataSource = data.ResponseData['ChangeManagementList'];
        else{
          this.JSONSource = data.ResponseData['ChangeManagementList'];
          
          let dataNew = data.ResponseData['ChangeManagementList'];
             console.log(dataNew);
             //Create workbook and worksheet
             let workbook = new Workbook();
             let worksheet = workbook.addWorksheet(this.CategoryName+'ReportData');
             let columns=Object.keys(data.ResponseData['ChangeManagementList'][0]);

             let headers = Object.keys(data.ResponseData['ChangeManagementList'][0]);
             headers.splice(18,2)
            let headerRow = worksheet.addRow(headers);
               // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: 'pattern',
              pattern: 'solid',
              fgColor: { argb: '00FF0000' },
              bgColor: { argb: '00FF0000' }
            }
            cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
          })
          dataNew.forEach(d => {
            // var obj = JSON.parse(d);
            var values = Object.keys(d).map(function (key) { 
              return d[key];
            });
            values[1] = moment(values[1]).format('DD-MM-YYYY');
            values[14] = moment(values[14]).format('DD-MM-YYYY');
            values.splice(18,2)
           let row = worksheet.addRow(values);
          });
          workbook.xlsx.writeBuffer().then((dataNew) => {
            let blob = new Blob([dataNew], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
            fs.saveAs(blob, this.CategoryName +'reportData.xlsx');
          });

          // this.cs.exportJsonAsExcelFile(this.JSONSource,'Changmangment Report')
        }
         this.showFilter=false;
       })  
    }
    else if(this.CategoryName == "Change management - Plant wise report"){
      let reqjson={
        "PageNo" : isExcel? 0: this.pageNo+1,
        "SearchValue":this.reportPlant
      }
      this.cs.postMySuggestionData("ChangeManagementPlantWise/GetCMPlantWiseReport", reqjson).subscribe((data: any) => {
        this.pageSize = data.ResponseData['Count'];
        if(!isExcel)
        this.DataSource = data.ResponseData['ChangeManagementList'];
        else{
          this.JSONSource = data.ResponseData['ChangeManagementList'];
          
          let dataNew = data.ResponseData['ChangeManagementList'];
             console.log(dataNew);
             //Create workbook and worksheet
             let workbook = new Workbook();
             let worksheet = workbook.addWorksheet(this.CategoryName+'ReportData');
             let headers = Object.keys(data.ResponseData['ChangeManagementList'][0]);
              headers.splice(18,2)
            let headerRow = worksheet.addRow(headers);
               // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: 'pattern',
              pattern: 'solid',
              fgColor: { argb: '00FF0000' },
              bgColor: { argb: '00FF0000' }
            }
            cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
          })
          dataNew.forEach(d => {
            // var obj = JSON.parse(d);
            // var values = Object.keys(d).map(function (key) { return d[key]; });
            var values = Object.keys(d).map(function (key) { 
              return d[key];
            });
            values[1] = moment(values[1]).format('DD-MM-YYYY');
            values[14] = moment(values[14]).format('DD-MM-YYYY');
            values.splice(18,2)
           let row = worksheet.addRow(values);
          });
          workbook.xlsx.writeBuffer().then((dataNew) => {
            let blob = new Blob([dataNew], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
            fs.saveAs(blob, this.CategoryName +'reportData.xlsx');
          });

          // this.cs.exportJsonAsExcelFile(this.JSONSource,'Changmangment Report')
        }
         this.showFilter=false;
       })  
    }
  }

  viewContent(data){
    const modalRef = this.modalService.open(ChangeRequestModalComponent, {  size: 'xl' as 'lg'}  );
    modalRef.componentInstance.flag ='';
    modalRef.componentInstance.CategoryName =this.CategoryName;
    
    if(this.CategoryName =="Change management - Admin Report")
    modalRef.componentInstance.flag = "True";
    else if(this.CategoryName =="Change management")
    modalRef.componentInstance.flag = "False";
    else if(this.CategoryName =="Change management - Plant wise report")
    modalRef.componentInstance.flag ="False";
    modalRef.componentInstance.passedData = data;

    // modalRef.result.then((data) => {
    //   if(this.CategoryName =="Change management - Admin Report")
    //   this.callCMR({ pageIndex: 0, pageSize: 10, length: this.pageSize },false);
    // })
  }

  ngAfterViewInit(){  
    if(this.CategoryName != "Tier II list" && this.CategoryName !="Change management - Supplier Report" && this.CategoryName != "Change management - Plant wise report"){
    this.getVarientDDLbyUserName();
    this.sideMenu = this.filterReport.sideMenu;
    }
    this.del_sch_adh_url= this.domSanitizer.bypassSecurityTrustResourceUrl('http://10.142.9.12:8052/Reports/DelSchAdhReport.aspx?VendorCode='+ this.masterVendorCode)
    }

  getMRQYear(){
    this.cs.postMySuggestionData("MRQ/GerYearDDL", "").subscribe((data: any) => {   
    this.array2 = data.ResponseData;  
    })
  }

  filterCMR(item){
    this.reportPlant = item;
    this.callCMR({ pageIndex: 0, pageSize: 10, length: this.pageSize },false);
  }

  changeStatus(actionType){
    let id:string;
    id =('"' + this.checkedData.join('","') + '"');
    let reqjson={
      "approve_Reject_By": this.vendorCode,
      "ids":id,
      "actionType": actionType
    }
    this.myBusinessService.changeStatusAdminReport(reqjson).subscribe((resp:any) => {
      this.cs.showSuccess(resp.Message);
     
      console.log("----------->submitCDMM---->>",resp);
        
    }, (err) => {
      
    });
  }

  selectAll(e) {
    //this.selectAction ='';
    console.log("--"+e);
    if(e.checked){
      this.DataSource.filter(element => {
        element["check"] = true;
        this.checkedData.push(element.id)
      });
    }else{
      this.checkedData = []
      this.DataSource.filter(element => {
        element["check"] = false;
      });
    }
    
  }

  selectData(e, Id1) {
    console.log(Id1, "..Id..");
    let Id = Id1.id;
    console.log(e);
    if (e.checked) {
      if (this.checkedData.length > 0) {
        var result = this.checkedData.filter(x => x.id === Id);
        console.log(result);
        if (result.length > 0) {
        } else {
          let data = {
          ID: Id
          };
          this.checkedData.push(data.ID);
        }
      } else {
        let data = {
          ID: Id
        };
        this.checkedData.push(data.ID);
      }
    } else {
      if (this.checkedData.length > 0) {
        var result = this.checkedData.filter(x => x.id === Id);
        if (result.length > 0) {
          var index = this.checkedData.findIndex(x => x.id === Id);
          this.checkedData.splice(index, 1);
        }
      } else {
      }
    }
    console.log(this.checkedData, "this.checkedData");
  }

  allChecked: any;
  updateChangeManagementDataStatus(actionType){
    console.log(this.checkedData)
    this.checkedData.forEach((id, index)=>{
      let reqjson = {
        approve_Reject_By: this.vendorCode,
        ids: id,
        actionType: actionType,
        remark: ""
      };
      this.myBusinessService.changeStatusAdminReport(reqjson).subscribe(
        (resp: any) => {
          if(index == (this.checkedData.length - 1)){
            this.allChecked = false;
            this.toastr.success(resp.ResponseData);
            this.checkedData = []
            this.DataSource.filter(element => {
              element.check = false;
            });
          }
        },
        (err) => {}
      );
    })
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  open(content) {
    this.modalService.open(content, {  size: 'sm',centered:true, windowClass : "formModal"});
  }

  moreFilters(){
   this.showFilter=true;
   this.VarientDropDownValue='';
  }
  Json =[];
  download(){
    if(this.CategoryName=="Tier II list"){
      this.cs.exportAsExcelFile(this.table,'Report');
      return;
    }
    if(this.CategoryName == 'Change management - Admin Report' || this.CategoryName == 'Change management - Plant wise report' || this.CategoryName == 'Change management - Supplier Report'){
      this.callCMR({pageIndex:0},true)
    }
    if(this.DataSource.length >= 1)
    {
      this.searchReport(0,true,'');
     // this.cs.exportJsonAsExcelFile(this.Json,'report')
     // this.cs.exportAsExcelFile(this.table,'Report')
    }
    //this.cs.exportAsExcelFile(this.table,'Report')
    // else
    // this.cs.showError("No records found to download")
  }

  getRequestBody():Promise<any>{
    return new Promise((resolve:any)=>{
      this.request={};
      this.Getrequest={}
      this.Insertrequest={};
      this.Updaterequest={};
      console.log(this.filterReport);
      this.filterReport.FilterArray.forEach((data: any) => {
        this.request[data.requestBody] = this.filterReport.FilterForm.controls[data.FormFieldName].value;
        if(data.IsGet=='true'){
          if(data.FormFieldName.includes('Date'))
            this.Getrequest[data.requestBody] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls[data.FormFieldName].value)?moment(this.filterReport.FilterForm.controls[data.FormFieldName].value).format('YYYY-MM-DD HH:mm:ss'):"";   
          else
            this.Getrequest[data.requestBody] = this.filterReport.FilterForm.controls[data.FormFieldName].value;          
        }
        if(data.IsInsert=='true'){
          if(data.FormFieldName.includes('Date'))
          this.Insertrequest[data.requestBody] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls[data.FormFieldName].value)?moment(this.filterReport.FilterForm.controls[data.FormFieldName].value).format('YYYY-MM-DD HH:mm:ss'):"";   
          else
          this.Insertrequest[data.requestBody] = this.filterReport.FilterForm.controls[data.FormFieldName].value;
        }   
        if(data.IsUpdate=='true'){
          if(data.FormFieldName.includes('Date'))
          this.Updaterequest[data.requestBody] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls[data.FormFieldName].value)?moment(this.filterReport.FilterForm.controls[data.FormFieldName].value).format('YYYY-MM-DD HH:mm:ss'):"";   
          else
          this.Updaterequest[data.requestBody] = this.filterReport.FilterForm.controls[data.FormFieldName].value;
        }  
      })
      resolve();
    })
  }

  validateAllFormFields(formGroup){
      Object.keys(formGroup.controls).forEach(controlName =>{
        const control = formGroup.get(controlName);
        if(control instanceof FormControl){
          control.markAsTouched({onlySelf: true});
        }
        else if(control instanceof FormControl || control instanceof FormArray){
        this.validateAllFormFields(control);
        }
      });
  } 

  getFilterData(){
    if(this.filterReport.FilterForm.valid){
      this.searchArray = this.filterReport.FilterArray;
      this.SearchForm =  this._formBuilder.group({});
      let formControlFields = [];
      for (let k = 0; k <= this.filterReport.FilterArray.length; k++) {
        if(this.filterReport.FilterArray[k]){
          if(this.filterReport.FilterArray[k].inputType=='Date')
          {
            // this.filterReport.FilterForm.get(this.searchArray[k].FormFieldName).setValue(moment(this.filterReport.FilterForm.controls[this.searchArray[k].FormFieldName].value).format('YYYY-MM-DD HH:mm:ss'));
            formControlFields.push({ name: this.filterReport.FilterArray[k].FormFieldName, control: new FormControl(this.filterReport.FilterForm.controls[this.searchArray[k].FormFieldName].value._d)});
          }
          else
          formControlFields.push({ name: this.filterReport.FilterArray[k].FormFieldName, control: new FormControl(this.filterReport.FilterForm.controls[this.searchArray[k].FormFieldName].value)});
        }
      }
      formControlFields.forEach(f => this.SearchForm.addControl(f.name, f.control));
      this.getPageChangeData({ pageIndex: 0, pageSize: 10, length: this.pageSize })
    }else{
      this.validateAllFormFields(this.filterReport.FilterForm);
    }
  }

  getPageChangeData(event){
    this.getData(event);
  }

  getData(event) {
    this.pageNo = event.pageIndex;
    this.getRequestBody().then(() => {
      if(this.CategoryName != 'Retro Credit Note'){
        this.Getrequest["PageNo"] = 1;
        this.Getrequest["RoleID"] = this.PrimaryRoleID;
      } else {
        this.Getrequest["pageNo"] = 1;
        this.Getrequest["roleID"] = this.PrimaryRoleID;
      }



      // if (this.CategoryName == 'MRQ') {
      //   this.Getrequest["VendorCode"] = this.vendorCode;
      // }
      if(this.CategoryName == 'PO SA Amendments'){
        this.Getrequest["Fromdate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Amendment From Date'].value)?this.filterReport.FilterForm.controls['Amendment From Date'].value:null;
        this.Getrequest["Todate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Amendment To Date'].value)?this.filterReport.FilterForm.controls['Amendment To Date'].value:null;
        this.Getrequest["ValidityFromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Validity From Date'].value)?this.filterReport.FilterForm.controls['Validity From Date'].value:null;
        this.Getrequest["ValidityToDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Validity To Date'].value)?this.filterReport.FilterForm.controls['Validity To Date'].value:null;
        this.Getrequest["VendorCode"] = this.masterVendorCode;
        this.Getrequest["Plant"] = this.filterReport.FilterForm.controls['Plant'].value;
        this.Getrequest["Material"] = this.filterReport.FilterForm.controls['Material'].value;
        this.Getrequest["PONo"] = this.filterReport.FilterForm.controls['PONo'].value;
        this.Getrequest["AmendmentType"] = this.filterReport.FilterForm.controls['Amendment Type'].value;
        this.Getrequest["PageNo"] = 1;
        this.Getrequest["SubVendor"] = this.filterReport.FilterForm.controls['Vendor Code'].value || null;
      }
      if(this.CategoryName == 'Payment Report – Assets'){
        this.Getrequest["PageNo"] = 1;
        // this.Getrequest["RoleId "] = this.PrimaryRoleID;
      }
      if (this.CategoryName == 'ODS With ASN' || this.CategoryName == 'Supply ODS') {
        if (this.filterReport.FilterForm.controls['Delivery Date From'].value == null || this.filterReport.FilterForm.controls['Delivery Date From'].value == "" || this.filterReport.FilterForm.controls['Delivery Date From'].value == undefined) {
          this.Getrequest["FromDate"] = '';
        }
        else {
          this.Getrequest["FromDate"] = moment(this.filterReport.FilterForm.controls['Delivery Date From'].value).format('YYYY-MM-DD HH:mm:ss');
        }
        if (this.filterReport.FilterForm.controls['Delivery Date To'].value == null ||this.filterReport.FilterForm.controls['Delivery Date To'].value == "" || this.filterReport.FilterForm.controls['Delivery Date To'].value == undefined) {
          this.Getrequest["ToDate"] = '';
        }
        else {
          this.Getrequest["ToDate"] = moment(this.filterReport.FilterForm.controls['Delivery Date To'].value).format('YYYY-MM-DD HH:mm:ss');
        }
      }
      if (this.CategoryName == 'ODS' || this.CategoryName == 'Service ODS') {
          this.Getrequest["ReportingMonth"]='';
          this.Getrequest["PageNo"]=1;
      }
      if (this.CategoryName == 'VQI' || this.CategoryName == 'LR & RA' || this.CategoryName == 'ODS With ASN' || this.CategoryName == 'Supply ODS') {
        this.Getrequest["PageNo"] = this.pageNo + 1;
      }
     
      if (this.CategoryName == 'Payment Report') {
        this.Getrequest["chequeNo"] = "";
        this.Getrequest["hundiNO"] = "";
        this.Getrequest["ClearingDocNo"] = this.filterReport.FilterForm.controls['Clearing document No'].value;
      }
      if (this.CategoryName == "VA Inventory") {
        this.Getrequest["FromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Period From'].value) ? moment(this.filterReport.FilterForm.controls['Period From'].value).format('YYYY-MM-DD HH:mm:ss') : "";
        this.Getrequest["ToDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Period To'].value) ? moment(this.filterReport.FilterForm.controls['Period To'].value).format('YYYY-MM-DD HH:mm:ss') : "";
      }
      if (this.CategoryName == 'R&IR' || this.CategoryName == 'Service R&IR') {
        this.Getrequest["PONo"] = this.filterReport.FilterForm.controls['Purchase Order No.'].value;
        // this.Getrequest["SupplierInvNo"]=this.filterReport.FilterForm.controls['Supplier Inv No'].value;
      }
      if (this.CategoryName == 'ASN Performance') {
        // let master_vendor = localStorage.getItem('MV')
        this.Getrequest["FromDateofackASN"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Date From'].value) ? moment(this.filterReport.FilterForm.controls['Date From'].value).format('YYYY-MM-DD HH:mm:ss') : "";
        this.Getrequest["ToDateofackASN"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Date To'].value) ? moment(this.filterReport.FilterForm.controls['Date To'].value).format('YYYY-MM-DD HH:mm:ss') : "";
        this.Getrequest["DateASN"] = "";
        // this.Getrequest["MasterVendor"] = master_vendor;
      }
      if(this.CategoryName == "OMR"){
        this.Getrequest["DocFromDate"]=!this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Date From'].value)?moment(this.filterReport.FilterForm.controls['Date From'].value).format('YYYY-MM-DD HH:mm:ss'):"";
        this.Getrequest["DocToDate"]=!this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Date To'].value)?moment(this.filterReport.FilterForm.controls['Date To'].value).format('YYYY-MM-DD HH:mm:ss'):"";
        this.Getrequest["VariantId"]=this.VarientDropDownValue;
      }
      if(this.CategoryName == "Del Sch Adh"){
        this.Getrequest["Vendor"]= null;
        this.Getrequest["FromDate"]=this.filterReport.month_from;
        this.Getrequest["ToDate"]=this.filterReport.month_to;
        this.Getrequest["PONumber"]=this.filterReport.FilterForm.controls['PO No.'].value;
        this.Getrequest["SalesOrganizationSector"]=this.filterReport.FilterForm.controls['Sales Organization'].value;
      }
      if(this.CategoryName == "Short Receipt Quantity"){
        this.Getrequest["InvoiceFromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Invoice Date From'].value)?moment(this.filterReport.FilterForm.controls['Invoice Date From'].value).format('YYYY-MM-DD HH:mm:ss'):"";
        this.Getrequest["InvoiceToDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Invoice Date To'].value)?moment(this.filterReport.FilterForm.controls['Invoice Date To'].value).format('YYYY-MM-DD HH:mm:ss'):"";
        this.Getrequest["PCSFromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['PCS Date From'].value)?moment(this.filterReport.FilterForm.controls['PCS Date From'].value).format('YYYY-MM-DD HH:mm:ss'):"";
        this.Getrequest["PCSToDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['PCS Date To'].value)?moment(this.filterReport.FilterForm.controls['PCS Date To'].value).format('YYYY-MM-DD HH:mm:ss'):"";
        this.Getrequest["GRFromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['GR Date From'].value)?moment(this.filterReport.FilterForm.controls['GR Date From'].value).format('YYYY-MM-DD HH:mm:ss'):"";
        this.Getrequest["GRToDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['GR Date To'].value)?moment(this.filterReport.FilterForm.controls['GR Date To'].value).format('YYYY-MM-DD HH:mm:ss'):"";
      }
      
      if(this.CategoryName != 'PO SA Amendments' && this.CategoryName != 'ASN Performance' && this.CategoryName != 'Retro Credit Note' && this.CategoryName != 'Retro Debit Note')
        this.Getrequest["VendorCode"] = '';
      // if(this.CategoryName == 'MRQ')
      //   this.Getrequest["VendorCode"] = this.vendorCode;
      if(this.CategoryName != 'ODS' && this.CategoryName != 'MRQ'){
        this.displayedColumns = this.filterReport.FilterArray[0].DatasourceColumns;
        this.DatasourceResponseData = this.filterReport.FilterArray[0].DatasourceResponseColumns;
      }
      if(this.CategoryName == 'Retro Debit Note'){ 
        if(this.PrimaryRoleID == '7' || this.PrimaryRoleID == '9'){
          this.Getrequest["MasterVendor"] = null;
          this.Getrequest["Vendor"] = this.filterReport.FilterForm.controls['Vendor Code'].value || null;
        }else{
          this.Getrequest["Vendor"] = this.filterReport.FilterForm.controls['Vendor Code'].value || this.vendorCode;
          this.Getrequest["MasterVendor"] = this.vendorCode;
        }
        this.Getrequest["FromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Posting Date From'].value)?moment(this.filterReport.FilterForm.controls['Posting Date From'].value).format('YYYY-MM-DD HH:mm:ss'):"";
        this.Getrequest["ToDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Posting Date To'].value)?moment(this.filterReport.FilterForm.controls['Posting Date To'].value).format('YYYY-MM-DD HH:mm:ss'):"";
      }
      if(this.CategoryName == 'Retro Credit Note'){
        if(this.PrimaryRoleID == '7' || this.PrimaryRoleID == '9'){
          this.Getrequest["masterVendor"] = null;
          this.Getrequest["vendor"] = this.filterReport.FilterForm.controls['Vendor Code'].value || null;
        }else{
          this.Getrequest["vendor"] = this.filterReport.FilterForm.controls['Vendor Code'].value || this.vendorCode;
          this.Getrequest["masterVendor"] = this.vendorCode;
        }
        this.Getrequest["fromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Posting Date From'].value)?moment(this.filterReport.FilterForm.controls['Posting Date From'].value).format('YYYY-MM-DD HH:mm:ss'):"";
        this.Getrequest["toDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Posting Date To'].value)?moment(this.filterReport.FilterForm.controls['Posting Date To'].value).format('YYYY-MM-DD HH:mm:ss'):"";
      }
      if(this.CategoryName == 'ASN Performance' || this.CategoryName == 'MRQ' || this.CategoryName == 'ODS' || this.CategoryName == 'ODS With ASN' || this.CategoryName == 'Supply ODS' || this.CategoryName == 'R&IR' || this.CategoryName == 'Service R&IR' || this.CategoryName == 'Service ODS'){
          if(this.PrimaryRoleID == '7' || this.PrimaryRoleID == '9'){
            this.Getrequest["VendorCode"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Vendor Code'].value)?this.filterReport.FilterForm.controls['Vendor Code'].value : null;
          }else{
            this.Getrequest["VendorCode"] = this.vendorCode;
          }
      }
      if(this.CategoryName != 'PO SA Amendments' && this.CategoryName != 'ASN Performance' && this.CategoryName != 'MRQ' && this.CategoryName != 'R&IR' && this.CategoryName!='Service R&IR' && this.CategoryName != 'ODS' && this.CategoryName != 'ODS With ASN' && this.CategoryName != 'Supply ODS' && this.CategoryName != 'Retro Credit Note' && this.CategoryName != 'Retro Debit Note' && this.CategoryName != 'Service ODS'){
        this.Getrequest["VendorCode"] = this.vendorCode;
      }
      if (this.CategoryName == 'Payment Report') {
        this.Getrequest["VendorCode"] = this.vendorCode;
       }
        if (this.CategoryName == 'VQI') {
          if(this.filterReport.FilterForm.controls['Vendor Code'].value == null || this.filterReport.FilterForm.controls['Vendor Code'].value == "")
          this.Getrequest["VendorCode"]= this.masterVendorCode;
          else
        this.Getrequest["VendorCode"] = "";
        this.Getrequest["SubVendor"] = this.filterReport.FilterForm.controls['Vendor Code'].value || "";
      }
      // this.Getrequest["VendorCode"] = '';
      // if(this.CategoryName != 'Retro Credit Note' && this.CategoryName != 'Retro Debit Note'){
      //   this.Getrequest["VendorCode"] = this.vendorCode;
      // }
      this.cs.postMySuggestionData(this.filterReport.FilterArray[0].POSTAPIURL, this.Getrequest).subscribe((data: any) => {
        if(this.CategoryName == 'VQI')
        this.pageSize = data.ResponseData.Count;
        else
        this.pageSize = data.Count;

        if(this.CategoryName == 'ODS With ASN' || this.CategoryName == 'MRQ' || this.CategoryName == 'Supply ODS')
          this.pageSize = data.ResponseData.Count;

        if(this.CategoryName == 'LR & RA')
          this.pageSize = data.ResponseData.Count;
          
        if(this.CategoryName == 'PO SA Amendments')
          this.pageSize = data.count;
        
        if(this.CategoryName == 'Service ODS')
         this.pageSize = data.TotalCount;
          
        let analysticsRequest
        ={
          "userClicked": this.UserId,
          "device": "windows",
          "browser": this.browser,
          "moduleType": "MR",
          "module": this.CategoryName
        }
       this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})   
       this.searchRequest = _.cloneDeep(this.Getrequest);

        if (this.CategoryName == 'ODS' && !this.cs.isUndefinedORNull(data.ResponseData)){
          this.displayedColumns=Object.keys(data.ResponseData[0]);
          this.DatasourceResponseData=Object.keys(data.ResponseData[0]);
          localStorage.setItem('DatasourceResponseColumns',this.DatasourceResponseData)
        }
        if(this.CategoryName == 'ODS' && this.cs.isUndefinedORNull(data.ResponseData)){
          this.displayedColumns=["PLANT","Vendor Code","Purchase Order No","Item","MATERIAL","Description","Document Change No","Basic Price"];
        }
        if (this.CategoryName == 'MRQ' && !this.cs.isUndefinedORNull(data.ResponseData.MRQDetails)){
          this.displayedColumns=Object.keys(data.ResponseData.MRQDetails[0]);
          this.DatasourceResponseData=Object.keys(data.ResponseData.MRQDetails[0]);
          localStorage.setItem('DatasourceResponseColumns',this.DatasourceResponseData)
          
        }
        if(this.CategoryName == 'MRQ' && this.cs.isUndefinedORNull(data.ResponseData.MRQDetails)){
          this.displayedColumns=["Plant","Vendor","Material Description","Component","LTP Version",
          "Net Requirement Feb","Net Requirement Jan","Net Requirement Mar","Plant Description","Vendor Description"];
        }
        // if(this.CategoryName == 'Payment Report – Assets'){
        //   this.displayedColumns = this.filterReport.FilterArray[0].DatasourceColumns;
        //   this.DatasourceResponseData = this.filterReport.FilterArray[0].DatasourceResponseColumns;
        // }
      for (let key in this.Getrequest){
        this.Getrequest[key] = ""
      }
      if(this.filterReport.FilterArray[0].ResponseDatasourceName){
        this.DataSource = data.ResponseData[this.filterReport.FilterArray[0].ResponseDatasourceName];
        console.log(this.DataSource)
      } else{
         
          this.DataSource = data.ResponseData;
        }

        if(this.CategoryName == 'Service R&IR'){
          this.displayedColumns= [
            "Vendor",
            "Purchase Order No",
            "PO Item No",
            "Plant",
            "Material",
            "Material Desc",
            "Supplier Invoice No",
            "Entry Sheet No",
            "Service Confirmation no.",
            "SES Short Text",
            "GRN No",
            "GRN date",
            "GR Year",
            "SES Status",
            "Bill Passing date",
            "Bill Passing Status",
            "Payment Status",
            "Clearing Document no",
            "Receipt Qty",
            "Invoice Amount",
            "Total Invoice Amount",
            "Cleared Amount",
            "Purchase Order Quantity"
          ]
          this.DatasourceResponseData=data.ResponseData;
        }
   if(this.CategoryName == "MRQ" && data.ResponseData.MRQDetails[0].ErrorResponse){
    this.cs.showError("Record not found");
    return;
   }
        if (this.cs.isUndefinedORNull(data.ResponseData)) {
            this.cs.showError("Record not found");
            return;
       }
       else{
        this.showFilter=false;
        this.filterReport.FilterForm.reset();
       }
      });
    });
  }

  ResetForm(){
    this.filterReport.FilterForm.reset();
    this.VarientDropDownValue='';
  }

  getVarientDDLbyUserName(){
    this.request={};
    this.request={
      "UserID":this.UserId
    }

    if(this.CategoryName == 'PO SA Amendments'){
      let userName = this.UserId.substring(12);
      this.request={
        "UserID": userName
      }
    }

    // this.Insertrequest['UserID'] = 'DP168-K';
    this.cs.postMySuggestionData(this.filterReport.FilterArray[0].VarientDropDownValue, this.request).subscribe((data: any) => {
    this.varientDDLValue = data.ResponseData;
    localStorage.setItem('DatasourceResponseColumns',this.filterReport.FilterArray[0].DatasourceResponseColumns)
    })
  }

  getVarientReportByID(){
    this.request = {};
    let varientName;
    this.request = {
      "VariantId": this.VarientDropDownValue
    }
    if (this.CategoryName == 'PO SA Amendments') {

      this.varientDDLValue.filter(x => {
        if (x.VariantId == this.VarientDropDownValue) {
          varientName = x.VariantName
       
        }
      })
      this.POvarientName = varientName;
      this.request = {
        "VariantName": varientName
      } 
    }
    this.cs.postMySuggestionData(this.filterReport.FilterArray[0].VarientReportByUserName, this.request).subscribe((res: any) => {
      this.filterReport.FilterArray.forEach((data: any) => {
        this.filterReport.FilterForm.controls[data.FormFieldName].patchValue(res.ResponseData[0][data.responseBody]);
        if(this.CategoryName == 'PO SA Amendments'){
          this.amendmentID = res.ResponseData[0].POAmendmentsVarianId;
        }
      })    
    })
  }

  saveVarientReport(){
    this.getRequestBody().then(()=>{
      if(this.CategoryName == 'VA Inventory' || this.CategoryName == 'Short Receipt Quantity' || this.CategoryName == 'Del Sch Adh')
      this.Insertrequest["VariantId"]="";

      if(this.CategoryName == 'Retro Debit Note' || this.CategoryName == 'Retro Credit Note'){
        this.Insertrequest["variantId"] = 0,
        this.Insertrequest["variantName"] = this.VariantName,
        this.Insertrequest["userID"] = this.UserId
      }

      if(this.CategoryName=='PO SA Amendments'){
        this.Insertrequest['Flag'] = 1;
        this.Insertrequest["Plant"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Plant'].value)?this.filterReport.FilterForm.controls['Plant'].value:'';
        this.Insertrequest["UserID"] = this.UserId;
        this.Insertrequest["POAmendmentsVarianId"] = 0;
        this.Insertrequest["ValidityDate"] = null;
        this.Insertrequest["AmendmentDate"] = null;
        this.Insertrequest["ValidityFromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Validity From Date'].value)?this.filterReport.FilterForm.controls['Validity From Date'].value:null;
        this.Insertrequest["ValidityToDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Validity To Date'].value)?this.filterReport.FilterForm.controls['Validity To Date'].value:null;
        this.Insertrequest["FromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Amendment From Date'].value)?this.filterReport.FilterForm.controls['Amendment From Date'].value:null;
        this.Insertrequest["Todate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Amendment To Date'].value)?this.filterReport.FilterForm.controls['Amendment To Date'].value:null;
        
      }
      if(this.CategoryName == 'VQI'){
        if(this.filterReport.FilterForm.controls['Day Inetrval'].value == null || this.filterReport.FilterForm.controls['Day Inetrval'].value == undefined){
          this.Insertrequest["FromDate"] ='';
        }
        else{
          this.Insertrequest["FromDate"]=this.filterReport.FilterForm.controls['Day Inetrval'].value;
        }
        if(this.filterReport.FilterForm.controls['Day Inetrval To'].value == null || this.filterReport.FilterForm.controls['Day Inetrval To'].value == undefined){
          this.Insertrequest["ToDate"] ='';
        }
        else{
          this.Insertrequest["ToDate"]=this.filterReport.FilterForm.controls['Day Inetrval To'].value;
         }  
      }
      if(this.CategoryName == 'Bill Passsed-PnP'){
        this.Insertrequest["PostingDate"]='';
        this.Insertrequest["InvoiceDate"]='';
        this.Insertrequest["DueDate"]='';
      }  
      if(this.CategoryName == 'ODS' || this.CategoryName == 'Service ODS'){
        this.Insertrequest["variantName"]=this.VariantName;
        this.Insertrequest["calmonth"]='';
        this.Insertrequest["userID"]=this.UserId;
      }
      if(this.CategoryName == 'MRQ')
      this.Insertrequest["Calmonth"]='012020';
      if(this.CategoryName != 'ODS' && this.CategoryName != 'Retro Debit Note'){
        this.Insertrequest["VariantName"]=this.VariantName;
        this.Insertrequest["UserID"]=this.UserId;
      }
      if(this.CategoryName == 'OMR'){
        if(this.Insertrequest["DocumentToDate"]==""){
          this.Insertrequest["DocumentToDate"]=null;
        }
        if(this.Insertrequest["DocumentFromDate"]==""){
          this.Insertrequest["DocumentFromDate"]=null;
        }
        // this.Insertrequest["DocumentToDate"]=this.VariantName;
        // this.Insertrequest["DocumentFromDate"]=this.UserId;
      }
      this.cs.postMySuggestionData(this.filterReport.FilterArray[0].INSERTVARIENTDETAILS, this.Insertrequest).subscribe((data: any) => {
        this.getVarientDDLbyUserName();
        this.closeModal();
        this.VariantName=''
        this.filterReport.FilterForm.reset();
        this.cs.showSuccess("Data successfully inserted");
      })
    })
  }

  getVariantName(){
    for(let i=0;i<this.varientDDLValue.length;i++){
      if(this.varientDDLValue[i].VariantId == this.VarientDropDownValue){
        this.Variant = this.varientDDLValue[i].VariantName;
      }
    }
  }

  updateVarientDeatils(){
    this.getRequestBody().then(()=>{
      if(this.CategoryName == 'LR & RA'){
        this.Getrequest["RoleID"] = this.PrimaryRoleID;
      }
      if(this.CategoryName == 'VQI' || this.CategoryName == 'LR & RA'){
        this.getVariantName();
         this.Updaterequest["VariantName"] = this.Variant; 
       }
       if(this.CategoryName == 'MRQ' ){
         this.getVariantName();
         this.Updaterequest["Calmonth"]='012020'; 
         this.Updaterequest["VariantName"] = this.Variant;  
       }
       if(this.CategoryName == 'ODS' || this.CategoryName == 'Service ODS'){
         this.getVariantName();
         this.Updaterequest["calmonth"]='012020'; 
         this.Updaterequest["VariantName"] = this.Variant;   
       }
       if(this.CategoryName=='PO SA Amendments'){
        this.Updaterequest['Flag'] = 2;
        this.Updaterequest["Plant"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Plant'].value)?this.filterReport.FilterForm.controls['Plant'].value:'';
        this.Updaterequest["UserID"] = this.UserId;
        this.Updaterequest["POAmendmentsVarianId"] = this.amendmentID;
        this.Updaterequest["ValidityDate"] = null;
        this.Updaterequest["AmendmentDate"] = null;
        this.Updaterequest["ValidityFromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Validity From Date'].value)?this.filterReport.FilterForm.controls['Validity From Date'].value:null;
        this.Updaterequest["ValidityToDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Validity To Date'].value)?this.filterReport.FilterForm.controls['Validity To Date'].value:null;
        this.Updaterequest["FromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Amendment From Date'].value)?this.filterReport.FilterForm.controls['Amendment From Date'].value:null;
        this.Updaterequest["Todate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Amendment To Date'].value)?this.filterReport.FilterForm.controls['Amendment To Date'].value:null; 
        this.Updaterequest["VariantName"]=this.POvarientName;
        this.Updaterequest["VendorCode"] = this.vendorCode;
      }
      if(this.CategoryName == 'VA Inventory' || this.CategoryName == 'OMR' || this.CategoryName == "Short Receipt Quantity" || 
        this.CategoryName == 'R&IR' || this.CategoryName == 'Service R&IR'
       || this.CategoryName == "Bill Passsed-PnP" || this.CategoryName == "Del Sch Adh"){

        this.getVariantName();
        this.Updaterequest["UserID"]=this.UserId;
        this.Updaterequest["VariantName"]=this.Variant;
      }
      if(this.CategoryName == 'ASN Performance'){
        this.Updaterequest["DocumentFromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Date From'].value)?moment(this.filterReport.FilterForm.controls['Date From'].value).format('YYYY-MM-DD HH:mm:ss'):"";
        this.Updaterequest["DocumentToDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Date To'].value)?moment(this.filterReport.FilterForm.controls['Date To'].value).format('YYYY-MM-DD HH:mm:ss'):"";
      }
      
      if(this.CategoryName == 'Retro Debit Note' || this.CategoryName == 'Retro Credit Note'){
        this.getVariantName();
        this.Updaterequest["variantId"] = this.VarientDropDownValue,
        this.Updaterequest["variantName"] = this.Variant,
        this.Updaterequest["userID"] = this.UserId
      }

      if(this.CategoryName !='PO SA Amendments') 
      this.Updaterequest["VariantId"] = this.VarientDropDownValue;
      this.cs.postMySuggestionData(this.filterReport.FilterArray[0].UPDATEURL, this.Updaterequest).subscribe((data: any) => {
      
      if(data){
        this.getVarientDDLbyUserName();
        this.filterReport.FilterForm.reset();
        this.VarientDropDownValue='';
        this.cs.showSuccess("Data successfully updated");
      }
      })
    })
  }

  deleteVarientReport(){
    this.request={};
    this.request={
      "VariantId":this.VarientDropDownValue
    }
    if(this.CategoryName=='PO SA Amendments'){
      this.request['Flag'] = 3;
      this.request["UserID"] = this.UserId;
      this.request["Plant"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Plant'].value)?this.filterReport.FilterForm.controls['Plant'].value:'';
      this.request["POAmendmentsVarianId"] = this.amendmentID;
      this.request["ValidityDate"] = null;
      this.request["AmendmentDate"] = null;
      this.request["ValidityFromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Validity From Date'].value)?this.filterReport.FilterForm.controls['Validity From Date'].value:null;
      this.request["ValidityToDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Validity To Date'].value)?this.filterReport.FilterForm.controls['Validity To Date'].value:null;
      this.request["FromDate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Amendment From Date'].value)?this.filterReport.FilterForm.controls['Amendment From Date'].value:null;
      this.request["Todate"] = !this.cs.isUndefinedORNull(this.filterReport.FilterForm.controls['Amendment To Date'].value)?this.filterReport.FilterForm.controls['Amendment To Date'].value:null; 
      this.request["VariantName"]=this.POvarientName;
      this.request["VendorCode"] = this.vendorCode;
      this.request["Material"] = "";
      this.request["PONo"]="";
      this.request["AmendmentType"]="";
    }
    this.cs.postMySuggestionData(this.filterReport.FilterArray[0].DELETEURL, this.request).subscribe((data: any) => {
   if(data){
     this.cs.showSuccess("Data deleted successfully");
     this.getVarientDDLbyUserName();
     this.filterReport.FilterForm.reset();
     this.VarientDropDownValue='';
   }
    })
  }

  searchReport(pageNo,flag,searchFlag){

    // this.paginator = pageNo.pageIndex + 1;
    // this.searchRequest["PageNo"] = this.paginator;

    if(this.CategoryName != 'Change management - Admin Report' && this.CategoryName != 'Change management - Plant wise report' && this.CategoryName != 'Retro Debit Note' && this.CategoryName != "Change management - Supplier Report"){
      this.searchArray.forEach((data: any,i) => {
        if(this.CategoryName=='ASN Performance'? i<=1: i <= 3 ){
          if(data.FormFieldName.includes('Date'))
              this.searchRequest[data.requestBody] = !this.cs.isUndefinedORNull(this.SearchForm.controls[data.FormFieldName].value)?moment(this.SearchForm.controls[data.FormFieldName].value).format('YYYY-MM-DD HH:mm:ss'):"";   
            else
              this.searchRequest[data.requestBody] = this.SearchForm.controls[data.FormFieldName].value;          
          }
      })
    }

    if(this.CategoryName == 'Retro Credit Note'){
      this.searchRequest["roleID"] = this.PrimaryRoleID
      this.searchRequest["pageNo"] = pageNo == 0 ? 0 : (pageNo.pageIndex + 1 || 1);
      if(this.PrimaryRoleID == '7' || this.PrimaryRoleID == '9'){
        this.searchRequest["vendor"] = this.SearchForm.controls['Vendor Code'].value || null;
      } else {
        this.searchRequest["vendor"] = this.SearchForm.controls['Vendor Code'].value || this.vendorCode;
      }
      this.searchRequest["fromDate"] = !this.cs.isUndefinedORNull(this.SearchForm.controls['Posting Date From'].value)?moment(this.SearchForm.controls['Posting Date From'].value).format('YYYY-MM-DD HH:mm:ss'):"";
      this.searchRequest["toDate"] = !this.cs.isUndefinedORNull(this.SearchForm.controls['Posting Date To'].value)?moment(this.SearchForm.controls['Posting Date To'].value).format('YYYY-MM-DD HH:mm:ss'):"";

    } else {

      if(searchFlag == 'search'){
        this.searchRequest["PageNo"] = 1;
      }
      else if(searchFlag == 'page'){
        this.paginator = pageNo.pageIndex + 1;
        this.searchRequest["RoleID"] = this.PrimaryRoleID
        this.searchRequest["PageNo"] = this.paginator;
      }
      else if(searchFlag == '' ){
        this.searchRequest["PageNo"] = 0;
      }


      this.searchRequest["RoleID"] = this.PrimaryRoleID
    }

    

    if (this.CategoryName == 'Retro Debit Note') {
      // this.searchRequest["MasterVendor"] = this.vendorCode;
      if(this.PrimaryRoleID == '7' || this.PrimaryRoleID == '9'){
        this.searchRequest["vendor"] = this.SearchForm.controls['Vendor Code'].value || null;
      } else {
        this.searchRequest["vendor"] = this.SearchForm.controls['Vendor Code'].value || this.vendorCode;
      }
      this.searchRequest["FromDate"] = !this.cs.isUndefinedORNull(this.SearchForm.controls['Posting Date From'].value)?moment(this.SearchForm.controls['Posting Date From'].value).format('YYYY-MM-DD HH:mm:ss'):"";
      this.searchRequest["ToDate"] = !this.cs.isUndefinedORNull(this.SearchForm.controls['Posting Date To'].value)?moment(this.SearchForm.controls['Posting Date To'].value).format('YYYY-MM-DD HH:mm:ss'):"";
    }
    
    if(this.searchArray[0].ResponseDatasourceName && this.CategoryName != 'ASN Performance' && this.CategoryName != 'VQI' && this.CategoryName != 'MRQ' && this.CategoryName != 'ODS With ASN' && this.CategoryName != 'Supply ODS' && this.CategoryName != 'R&IR' && this.CategoryName != 'Service R&IR'){
      this.searchRequest["VendorCode"] = this.vendorCode;
      // this.searchRequest["PageNo"] = pageNo;
    }
   else

      if(this.CategoryName != 'ASN Performance' && this.CategoryName != 'MRQ' && this.CategoryName != 'VQI' && this.CategoryName != 'R&IR' && this.CategoryName != 'Service R&IR' && this.CategoryName != 'ODS With ASN' && this.CategoryName != 'Supply ODS' && this.CategoryName != 'ODS' && this.CategoryName != 'Service ODS' && this.CategoryName != 'Retro Credit Note' && this.CategoryName != 'Retro Debit Note'){
        this.searchRequest["VendorCode"] = this.vendorCode;
      }

      if(this.CategoryName == 'PO SA Amendments'){
      //  this.searchRequest["SubVendor"] = this.filterReport.FilterForm.controls['Vendor Code'].value || null;
        this.searchRequest["Fromdate"] = !this.cs.isUndefinedORNull(this.SearchForm.controls['Amendment From Date'].value)?this.SearchForm.controls['Amendment From Date'].value:null;
        this.searchRequest["Todate"] = !this.cs.isUndefinedORNull(this.SearchForm.controls['Amendment To Date'].value)?this.SearchForm.controls['Amendment To Date'].value:null;
        this.searchRequest["ValidityFromDate"] = !this.cs.isUndefinedORNull(this.SearchForm.controls['Validity From Date'].value)?this.SearchForm.controls['Validity From Date'].value:null;
        this.searchRequest["ValidityToDate"] = !this.cs.isUndefinedORNull(this.SearchForm.controls['Validity To Date'].value)?this.SearchForm.controls['Validity To Date'].value:null;
      }
      if (this.CategoryName == 'ASN Performance') {
        // let master_vendor = localStorage.getItem('MV')
        this.searchRequest["FromDateofackASN"] = !this.cs.isUndefinedORNull(this.SearchForm.controls['Date From'].value) ? moment(this.SearchForm.controls['Date From'].value).format('YYYY-MM-DD HH:mm:ss') : "";
        this.searchRequest["ToDateofackASN"] = !this.cs.isUndefinedORNull(this.SearchForm.controls['Date To'].value) ? moment(this.SearchForm.controls['Date To'].value).format('YYYY-MM-DD HH:mm:ss') : "";
        this.searchRequest["DateASN"] = "";
        // this.Getrequest["MasterVendor"] = master_vendor;
      }
      // if(this.CategoryName == 'Service ODS' && this.RoleID =='7'){
      //   alert(this.masterVendorCode);
      //   if(this.searchRequest['VendorCode'] != null && this.RoleID =='7')
      //   {
      //     this.searchRequest['VendorCode'] = null;
          
      //   }
      // }
      if(this.CategoryName == 'VQI'){
        console.log(this.searchRequest);
        
        if(this.searchRequest['VendorCode'] == null || this.searchRequest['VendorCode'] == "")
        {
          if(this.searchRequest['SubVendor'] == null || this.searchRequest['SubVendor'] == "")
          {
            this.searchRequest["VendorCode"]= this.masterVendorCode;
            this.searchRequest["SubVendor"] = null;
          }
          else{
            this.searchRequest["VendorCode"]= "";
            this.searchRequest["SubVendor"] = this.searchRequest['SubVendor'];
          }
          
        }
        else{
          this.searchRequest["SubVendor"] = this.searchRequest['VendorCode'] || null;
          this.searchRequest["VendorCode"] = "";
        }
      
        
      }
      
    this.cs.postMySuggestionData(this.searchArray[0].POSTAPIURL, this.searchRequest).subscribe((data: any) => {
      this.pageSize = data.Count || 0

      if(this.CategoryName == 'ODS With ASN' || this.CategoryName == 'Supply ODS' || this.CategoryName == 'MRQ' || this.CategoryName == 'VQI')
        this.pageSize = data.ResponseData.Count;

      if(this.CategoryName == 'PO SA Amendments')
        this.pageSize = data.count;
      if (this.CategoryName == 'ODS' && !this.cs.isUndefinedORNull(data.ResponseData)){
        this.displayedColumns=Object.keys(data.ResponseData[0]);
        this.DatasourceResponseData=Object.keys(data.ResponseData[0]);
        localStorage.setItem('DatasourceResponseColumns',this.DatasourceResponseData)
      }
      if(this.CategoryName == 'LR & RA')
        this.pageSize = data.ResponseData.Count;

      if(this.CategoryName == 'Service ODS')
      this.pageSize = data.TotalCount;  
        
      if(this.CategoryName == 'Retro Debit Note')
        this.pageSize = data.Count;

      if(this.CategoryName == 'ODS' && this.cs.isUndefinedORNull(data.ResponseData)){
        this.displayedColumns=["PLANT","Vendor Code","Purchase Order No","Item","MATERIAL","Description","Document Change No","Basic Price"];
      }

      if (this.CategoryName == 'MRQ' && !this.cs.isUndefinedORNull(data.ResponseData.MRQDetails)){
        this.displayedColumns=Object.keys(data.ResponseData.MRQDetails[0]);
        this.DatasourceResponseData=Object.keys(data.ResponseData.MRQDetails[0]);
        localStorage.setItem('DatasourceResponseColumns',this.DatasourceResponseData)
      }
      if(this.CategoryName == 'MRQ' && this.cs.isUndefinedORNull(data.ResponseData.MRQDetails)){
        this.displayedColumns=["Plant","Vendor","Material Description","Component","LTP Version",
        "Net Requirement Feb","Net Requirement Jan","Net Requirement Mar","Plant Description","Vendor Description"];
      }

      if(this.searchArray[0].ResponseDatasourceName){
        if(flag){
          
          // this.Json =data.ResponseData;
          // this.Json = data.ResponseData[this.searchArray[0].ResponseDatasourceName];
          // this.cs.exportJsonAsExcelFile(this.Json,'Report')

          // tanvi -new expoer to excel
          let dataNew = data.ResponseData[this.searchArray[0].ResponseDatasourceName];
        
           //Create workbook and worksheet
           let workbook = new Workbook();
           let worksheet = workbook.addWorksheet(this.CategoryName+'Report Data');
            //Add Header Row
            let header: any 
            if(this.CategoryName == 'Retro Debit Note' || this.CategoryName == 'Retro Credit Note'){
              header = Object.keys(dataNew[0])
            } else {
              header = this.displayedColumns
            }
          let headerRow = worksheet.addRow(header);
             // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: '00FF0000' },
            bgColor: { argb: '00FF0000' }
          }
          cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        })
        dataNew.forEach(d => {
          var values = Object.keys(d).map(function (key) { return d[key]; });
          console.log(values);
          if(this.CategoryName == 'LR & RA')
          values.splice(4,1);
          if(this.CategoryName == 'VQI')
          [values[0], values[1]] = [values[1], values[0]];
           let row = worksheet.addRow(values);
       });
          workbook.xlsx.writeBuffer().then((dataNew) => {
            let blob = new Blob([dataNew], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
            fs.saveAs(blob, this.CategoryName+'reportData.xlsx');
      });
        }
        else
        this.DataSource = data.ResponseData[this.searchArray[0].ResponseDatasourceName];
      }
      else{
       if(flag){
            // this.Json =data.ResponseData;
         //   this.cs.exportJsonAsExcelFile(this.Json,'Report')
             // tanvi -new expoer to excel
          let dataNew = data.ResponseData;
          console.log(dataNew);
         
          //Create workbook and worksheet
          let workbook = new Workbook();
          let worksheet = workbook.addWorksheet(this.CategoryName+'ReportData');
           //Add Header Row
          //  let header = ["Year", "Month", "Make", "Model", "Quantity", "Pct"];
          //  let headerRow = worksheet.addRow(header);
          let header: any 
          if(this.CategoryName == 'Retro Debit Note' || this.CategoryName == 'Retro Credit Note'){
            header = Object.keys(dataNew[0])
          }else{
            header = this.displayedColumns
          }
        let headerRow = worksheet.addRow(header);
            // Cell Style : Fill and Border
       headerRow.eachCell((cell, number) => {
         cell.fill = {
           type: 'pattern',
           pattern: 'solid',
           fgColor: { argb: '00FF0000' },
           bgColor: { argb: '00FF0000' }
         }
         cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
       })
        // Add Data and Conditional Formatting
        dataNew.forEach(d => {
          // var obj = JSON.parse(d);
          var values = Object.keys(d).map(function (key) { return d[key]; });
          console.log(values);
          console.log(this.CategoryName);
          
          if(this.CategoryName == 'R&IR'){
            values.splice(26,0,d['Incoterms']);
            values.splice(27,0,d['Incoterms1']);
            values.splice(34, 1);
            values.splice(35, 1);
            values.splice(34, 1,d['RejectedQty']);
            values.splice(35, 1,d['ReAcceptanceQty']);
          }

          // values.unshift(values.splice(7,1)[0])
          // this.swapIndex(values, 3, 4)
          // values.splice(5,0,values.splice(9,1)[0])
          // values.splice(6,0,values.splice(8,1)[0])
          
          if(this.CategoryName == "Bill Passed-PnP"){
            values.splice(4,0,values.splice(12,1)[0])
          }
          if(this.CategoryName == "Service ODS"){
            //   let a = values[2];
            //   let b = values[1];
            //  [values[1], values[2], values[3], values[4],values[5],values[6]] = 
            //  [values[6], values[5],values[4],values[3],b,a];
             
            //  values[7] = values[14];
            // //  values[9] = values[10];
            //  values.splice(14,1);
          }
          if(this.CategoryName == "Service R&IR"){
            
            //  [values[11], values[3],  values[4],values[10], values[5],values[13],values[6],  values[16],values[14],values[15] , values[21], values[0],values[19],values[18],values[7],values[8],  values[9],values[12]] = 
            //  [values[3],  values[0],  values[6],values[7],  values[8],values[9], values[10], values[11],values[12],values[13], values[15], values[14],values[18],values[21],values[22],values[23],values[24],values[25]];

             [values[3],  values[12],values[4], values[5], values[6], values[11],values[14],values[7], values[17],values[15],values[16],  values[0], values[22],values[18],values[19], values[8],values[9], values[10], values[13]] = 
             [values[0],  values[3], values[5], values[6], values[8], values[7], values[9], values[10],values[11],values[12],values[13], values[14], values[15],values[17],values[18],values[22],values[23],values[24],values[25]];
             
             values.splice(24,1);
             values.splice(25,1);
            //  values.splice(22,1);
             values.splice(23,1);
             values.splice(23,1);
          }
         
          // if(d['StorageLocation'] && this.CategoryName =='R&IR')  
          //   values.splice(4, 1);
                    
            if(d['Serial'] && this.CategoryName =='R&IR')
             values.splice(33, 1);
           let row = worksheet.addRow(values);

        });
         workbook.xlsx.writeBuffer().then((dataNew) => {
           let blob = new Blob([dataNew], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
           fs.saveAs(blob, this.CategoryName +'reportData.xlsx');
           });
          }
          else{
        this.DataSource = data.ResponseData;
        this.DataSource = [...this.DataSource];
          }
        }
    })
  }

}

