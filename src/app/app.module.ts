import { MyPolicyGuidelinesComponent } from "./my-policy-guidelines/my-policy-guidelines.component";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "src/material.module";
import { MainNavComponent } from "./main-nav/main-nav.component";
import { LayoutModule } from "@angular/cdk/layout";
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressBar,
  MatProgressBarModule,
} from "@angular/material";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { LoginComponent } from "./login/login.component";
import { HttpClientModule } from "@angular/common/http";
import { LandingNavbarComponent } from "./landing-navbar/landing-navbar.component";
import { AboutUsComponent } from "./about-us/about-us.component";
// import { MSetuPageComponent } from './m-setu-page/m-setu-page.component';
import { ContactUsComponent } from "./contact-us/contact-us.component";
// import { QuickLinksComponent } from './quick-links/quick-links.component';
import { AdminDashboardComponent } from "./admin-dashboard/admin-dashboard.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DatePipe, Location } from "@angular/common";
import { RiskDashboardComponent } from "./risk-dashboard/risk-dashboard.component";
import { LiquidityModalComponent } from "./modal/liquidity-modal/liquidity-modal.component";
import { SuggestionDashboardComponent } from "./suggestion-dashboard/suggestion-dashboard.component";
import { MySuggestionComponent } from "./my-suggestion/my-suggestion.component";
import { ToastrModule, ToastrService } from "ngx-toastr";
import { SearchModalComponent } from "./modal/search-modal/search-modal.component";
import { ContactModalComponent } from "./modal/contact-modal/contact-modal.component";
import { WhatsNewModalComponent } from "./modal/whats-new-modal/whats-new-modal.component";
import { CreateUserProfileComponent } from "./create-user-profile/create-user-profile.component";
import { VendorsComponent } from "./vendors/vendors.component";
import { RoleBasedAccessComponent } from "./role-based-access/role-based-access.component";
import { NgxBarcodeModule } from "ngx-barcode";
import { MsetuLandingComponent } from "./msetu-landing/msetu-landing.component";
import { QuickLinksLandingComponent } from "./quick-links-landing/quick-links-landing.component";
import { ReportNewComponent } from "./report-new/report-new.component";
import { MatDialogModule } from "@angular/material/dialog";
import { ExcellenceAwardModalComponent } from "./modal/excellence-award-modal/excellence-award-modal.component";
import { CookieService } from "ngx-cookie-service";
import { DailoughBoxComponent } from "./modal/dailough-box/dailough-box.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { EscalateMatrixModalComponent } from "./modal/escalate-matrix-modal/escalate-matrix-modal.component";
import { SafevideopipePipe } from "./login/safevideopipe.pipe";
import { ChangeRequestModalComponent } from "./modal/change-request-modal/change-request-modal.component";
import { SupplierMeetRegUserModalComponent } from "./modal/supplier-meet-reg-user-modal/supplier-meet-reg-user-modal.component";
import { AnimationService } from "./Supplier-Meet-common/animation.service";
import { LandingPageComponent } from "./Supplier-Meet-landing-page/landing-page.component";
import { CreateConcernComponent } from "./modal/create-concern/create-concern.component";
import { MonsoonUpdatesModalComponent } from "./modal/monsoon-updates-modal/monsoon-updates-modal.component";
import { CapabilitybuildingComponent } from "./capabilitybuilding/capabilitybuilding.component";
// import { SupplierAccessorMappingComponent } from './supplier-accessor-mapping/supplier-accessor-mapping.component';
import { NgIdleKeepaliveModule } from "@ng-idle/keepalive";
import { SupplierCodeConductComponent } from "./supplier-code-conduct/supplier-code-conduct.component";
// import { PdfViewerModule } from 'ng2-pdf-viewer';
import { SourceCodeConductListComponent } from "./source-code-conduct-list/source-code-conduct-list.component";
import { NotificationdashboardmodalComponent } from "./modal/notificationdashboardmodal/notificationdashboardmodal.component";

// for HttpClient import:
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { EscalationMatrixModalComponent } from './modal/escalation-matrix-modal/escalation-matrix-modal.component';
import { COPEmailModalComponent } from './modal/copemail-modal/copemail-modal.component';
import { UnderMaintainanceComponent } from './under-maintainance/under-maintainance.component';
import { LoginModalComponent } from './modal/login-modal/login-modal.component';
import { SafeHtmlPipePipe } from 'src/app/pipe/safeHtmlPipe/safe-html-pipe.pipe';
import { ExtCodeOfConductionComponent } from './ext-code-of-conduction/ext-code-of-conduction.component';
import { ToastServiceService } from "./services/toaster/toast-service.service";
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { StakeholderComponent } from './stakeholder/stakeholder.component';
import { StakeholderUploadComponent } from './stakeholder-upload/stakeholder-upload.component';
import { VendorListComponent } from './vendor-list/vendor-list.component';
import { SupplierListComponent } from './supplier-list/supplier-list.component';
import { CapabilityScoreboardComponent } from './capability-scoreboard/capability-scoreboard.component';
// import { RPAComponentComponent } from './rpacomponent/rpacomponent.component';



@NgModule({
  declarations: [
    AppComponent,
    // SafeHtmlPipePipe,
    
    UnderMaintainanceComponent,
    MainNavComponent,
    LandingNavbarComponent,COPEmailModalComponent,
    LandingPageComponent,
    LoginComponent,
    SafevideopipePipe,
    MonsoonUpdatesModalComponent,
    DashboardComponent,
    AdminDashboardComponent,
    MsetuLandingComponent,
    AboutUsComponent,
    QuickLinksLandingComponent,
    ContactUsComponent,
    MySuggestionComponent,
    SuggestionDashboardComponent,
    RiskDashboardComponent,
    CapabilitybuildingComponent,
    DailoughBoxComponent,
    
    DailoughBoxComponent,
    SupplierCodeConductComponent,
    SourceCodeConductListComponent,
    
    
    SupplierMeetRegUserModalComponent,
    CreateConcernComponent, // Also a Modal
    NotificationdashboardmodalComponent,
    LiquidityModalComponent,
    SearchModalComponent,
    ContactModalComponent,
    WhatsNewModalComponent,
    ExcellenceAwardModalComponent,
    EscalateMatrixModalComponent,
    ChangeRequestModalComponent,
    EscalationMatrixModalComponent,
    UnderMaintainanceComponent,
    LoginModalComponent,
    ExtCodeOfConductionComponent,
    EmployeeDetailsComponent,
    StakeholderComponent,
    StakeholderUploadComponent,
    VendorListComponent,
    SupplierListComponent,
    CapabilityScoreboardComponent,
    // RPAComponentComponent
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSortModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatFormFieldModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    HttpClientModule,
    MatListModule,
    NgxBarcodeModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    // Loader for HttpClient and Router use:
    LoadingBarRouterModule,
    LoadingBarHttpClientModule,
    LoadingBarModule,
    // for Core use:
    // PdfViewerModule,
    NgIdleKeepaliveModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: "toast-top-right",
      preventDuplicates: true,
    }),
  ],
  entryComponents: [
    MonsoonUpdatesModalComponent,COPEmailModalComponent,
    EscalationMatrixModalComponent,
    NotificationdashboardmodalComponent,
    ChangeRequestModalComponent,
    DailoughBoxComponent,
    CreateConcernComponent,
    LoginModalComponent
  ],
  providers: [
    DatePipe,
    AnimationService,
    CookieService,
    NgbModal,
    ToastServiceService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
