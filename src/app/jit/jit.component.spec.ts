import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JITComponent } from './jit.component';

describe('JITComponent', () => {
  let component: JITComponent;
  let fixture: ComponentFixture<JITComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JITComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JITComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
