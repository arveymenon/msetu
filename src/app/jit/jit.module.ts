import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JITComponent } from './jit.component';
import { JitRoutingModule } from './jit-routing/jit-routing.module';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/material.module';
import { MatPaginatorModule, MatDialogModule, MatSortModule, MatFormFieldModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule } from '@angular/material';
import { NgxBarcodeModule } from 'ngx-barcode';

import { JitprintComponent } from '../jitprint/jitprint.component';
import { JITPrintHistoryComponent } from '../jitprint-history/jitprint-history.component';
import { JITModelMasterComponent } from '../jitmodel-master/jitmodel-master.component';
import { JITPartSupplierMappingComponent } from '../jitpart-supplier-mapping/jitpart-supplier-mapping.component';
import { JITSupplierDetailsComponent } from '../jitsupplier-details/jitsupplier-details.component';
import { LayoutModule } from '@angular/cdk/layout';
// import { StylePaginatorDirective } from '../myBusiness/oesupplies/style-paginator.directive';

@NgModule({
  declarations: [
    JITComponent,
    JitprintComponent,
    JITPrintHistoryComponent,
    JITModelMasterComponent,
    JITPartSupplierMappingComponent,
    JITSupplierDetailsComponent,
    
  ],
  imports: [
    CommonModule,
    JitRoutingModule,

    MaterialModule,
    FormsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSortModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,

    NgxBarcodeModule,
  ]
})
export class JitModule { }
