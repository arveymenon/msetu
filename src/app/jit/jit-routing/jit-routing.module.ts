import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ROUTES, Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/services/auth-guards/auth.guard';
import { JITComponent } from '../jit.component';
import { JitprintComponent } from 'src/app/jitprint/jitprint.component';
import { JITPrintHistoryComponent } from 'src/app/jitprint-history/jitprint-history.component';
import { JITModelMasterComponent } from 'src/app/jitmodel-master/jitmodel-master.component';
import { JITPartSupplierMappingComponent } from 'src/app/jitpart-supplier-mapping/jitpart-supplier-mapping.component';
import { JITSupplierDetailsComponent } from 'src/app/jitsupplier-details/jitsupplier-details.component';

const routes: Routes = [
  {
    path: "",
    component: JITComponent,
    canActivate: [AuthGuard],
    data: { roles: ['JIT'] }
  },
  {
    path: "jitprint",
    pathMatch: 'prefix',
    component: JitprintComponent,
    canActivate: [AuthGuard],
    data: { roles: ['jitprint'] }
  },
  {
    path: "JITPrintHistory",
    component: JITPrintHistoryComponent,
    canActivate: [AuthGuard],
    data: { roles: ['JITPrintHistory'] }
  },
  {
    path: "JITModelMaster",
    component: JITModelMasterComponent,
    canActivate: [AuthGuard],
    data: { roles: ['JITModelMaster'] }
  },
  {
    path: "JITPartSupplierMapping",
    component: JITPartSupplierMappingComponent,
    canActivate: [AuthGuard],
    data: { roles: ['JITPartSupplierMapping'] }
  },
  {
    path: "JITSupplierDetails",
    component: JITSupplierDetailsComponent,
    canActivate: [AuthGuard],
    data: { roles: ['JITSupplierDetails'] }
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class JitRoutingModule { }
