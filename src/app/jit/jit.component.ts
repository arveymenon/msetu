import { Component, OnInit, ViewChild } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { MatIconRegistry, MatPaginator } from "@angular/material";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { SurveyService } from "../services/survey/survey.service";
import { JitService } from "../services/jit/jit.service";
import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";
import * as CryptoJS from 'crypto-js';
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";

import * as _ from "lodash";
import * as moment from "moment";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: "app-jit",
  templateUrl: "./jit.component.html",
  styleUrls: ["./jit.component.scss"]
})
export class JITComponent implements OnInit {
  userid = CryptoJS.AES.decrypt(localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ=='),"").toString(CryptoJS.enc.Utf8);
  @ViewChild(MatPaginator) paginator: MatPaginator;


  date = Date;
  moment = moment
  viewFilter = true;
  FilterForm: FormGroup;
  FilterAttributeForm: FormGroup;
  selectAllAttribute = new FormControl(false);

  public supplierCode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
  plantCodes = [];
  shopList = [];
  productionLineList = [];
  dataFromList = [];
  Aggregates = [];
  
  barCodeNumberTransaction = [];
  DataSource = [];
  displayedColumns: string[] = [
    // "selectCheck",
    "productionLine",
    "shopName",
    "DSN",
    "SPN",
    "BIN",
    "modelCode",
    "dateTime",
    "colour",
    "remarks",
    "country",
    "paintOutScan",
    "majorVariant",
    "VIN",
    "partNo",
    "partDescription",
    "dataFrom"
  ];

  public allDisplayedColumns: string[] = [
    // "selectCheck",
    "productionLine",
    "shopName",
    "DSN",
    "SPN",
    "BIN",
    "modelCode",
    "dateTime",
    "colour",
    "remarks",
    "country",
    "paintOutScan",
    "majorVariant",
    "VIN",
    "partNo",
    "partDescription",
    "dataFrom"
  ];
  
  pageSize = 200;

  public searchColOptions = _.cloneDeep(this.displayedColumns);

  barCodeNumberList: any = [];
  json=[];

  constructor(
    public commonService: CommonUtilityService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private modalService: NgbModal,
    public jitService: JitService,
    public formBuilder: FormBuilder,
    public cs: MySuggestionService,
    public toastr: ToastrService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "excelIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/excel.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "wordIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/word_Icon.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "printIcon",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/print.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "filter_white",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/filter_white.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "filter",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/filter.svg"
      )
    );
    this.searchColOptions.splice(0, 1);
    this.FilterForm = this.formBuilder.group({
      PlantCode: ['', Validators.required],
      ShopName: [],
      ProductionLine: [],
      DataFrom: [],
      Aggregates: [],
      SearchBy: [],
      SearchByValue: [],
      SearchByCutOff: []
    });

    this.FilterAttributeForm = this.formBuilder.group({
      productionLine: [true],
      shopName: [true],
      modelCode: [true],
      dateTime: [true],
      colour: [true],
      remarks: [true],
      country: [true],
      paintOutScan: [true],
      majorVariant: [true],
      VIN: [true],
      partNo: [true],
      partDescription: [true]
    });

    

    console.log(this.FilterForm);
  }
  roleId;

  ngOnInit() {
    this.roleId = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.roleId = CryptoJS.AES.decrypt(this.roleId,"").toString(CryptoJS.enc.Utf8);
    this.supplierCode = localStorage.getItem('WkcxV2RWcEhPWGxSTWpscldsRTlQUT09');
    this.supplierCode = CryptoJS.AES.decrypt(this.supplierCode,"").toString(CryptoJS.enc.Utf8);
    let body = { SupplierCode: this.supplierCode };
    this.jitService.call("PTLiveData/GetPlantCodeList", body).subscribe(res => {
      console.log(res);
      this.plantCodes = res.ResponseData;
    });

    this.jitService
      .call("PTLiveData/GetAggregatesList", body)
      .subscribe(res => {
        console.log(res);
        this.Aggregates = res.ResponseData;
      });

    this.FilterAttributeForm.valueChanges.subscribe(res => {
      console.log("Filter Attribute Form Changed");
      Object.keys(this.FilterAttributeForm.controls).forEach(attr => {
        console.log(this.FilterAttributeForm.get(attr).value);
        if (this.FilterAttributeForm.get(attr).value == false) {
          this.selectAllAttribute.patchValue(false);
        }
      });
    });
    let analysticsRequest={
      "userClicked":this.userid,//  localStorage.getItem('userToken'),
      "device": "windows",
      "browser": localStorage.getItem('browser'),
      "moduleType": "Document",
      "module": "MyLibary"
    }
   this.cs.postMySuggestionData('Analytics/InsertAnalytics',analysticsRequest).subscribe((res:any)=>{})
  }

  
  closeModal(content?: any) {
    console.log(this.FilterAttributeForm);
    this.modalService.dismissAll();

    const newDisplayColumns = _.cloneDeep(this.allDisplayedColumns);
    console.log(newDisplayColumns);

    Object.keys(this.FilterAttributeForm.controls).forEach(attribute => {
      console.log(attribute);
      if (this.FilterAttributeForm.value[attribute] == false) {
        console.log(this.FilterAttributeForm.value[attribute]);
        console.log(newDisplayColumns.findIndex(o => o == attribute));
        newDisplayColumns.splice(
          newDisplayColumns.findIndex(o => o == attribute),
          1
        );
      }
    });
    console.log(this.displayedColumns);
    console.log(this.allDisplayedColumns);
    this.displayedColumns = newDisplayColumns;
  }

  getProductionLineAndDataFrom(plantCode) {
    console.log(plantCode);
    let body = { PlantCode: plantCode };
    this.jitService
      .call("PTLiveData/GetProductionLineList", body)
      .subscribe(res => {
        console.log(res);
        this.productionLineList = res.ResponseData;
      });

    this.jitService.call("PTLiveData/GetDataFromList", body).subscribe(res => {
      console.log(res);
      this.dataFromList = res.ResponseData;
    });

    this.jitService.call("PTLiveData/GetShopList", body).subscribe(res => {
      console.log(res);
      this.shopList = res.ResponseData;
    });
  }

  open(content) {
    const modalOps: NgbModalOptions = {
      size: "xl" as "lg",
      centered: true,
      backdrop: "static",
      windowClass: "formModal"
    };

    this.modalService.open(content, modalOps);
  }

  exportAsExcelFile(table: any, name: any) {
    const temp_displayedColumns = _.cloneDeep(this.displayedColumns);
    this.displayedColumns.splice(0, 1);
    this.search(0).then(()=>{
      console.log('Got Response');
      // this.cs.exportAsExcelFile(table, name);
      this.cs.exportJsonAsExcelFile(this.DataSource, name);
      this.displayedColumns = temp_displayedColumns;
    },
    err=>{
      this.toastr.error('Some Error Occoured');
    })
  //   let body = {
  //     PageNo: 0,
  //     PlantCode: this.FilterForm.value.PlantCode,
  //     SupplierCode: this.supplierCode,
  //     ProductionLine: this.FilterForm.value.ProductionLine,
  //     ShopName: this.FilterForm.value.ShopName,
  //     DataFrom: this.FilterForm.value.DataFrom,
  //     Aggregates1: this.FilterForm.value.Aggregates
  //   };
  //   console.log(body);
  //   this.jitService.call("PTLiveData/GetPTLiveData", body).subscribe(res => {
  //     console.log(res);
  //     if (res.Message == "Success") {
  //       this.json = res.ResponseData
  //      this.cs.exportJsonAsExcelFile(this.json,"JITReport")

  // }
  //   })
  }
  clearSearchForm(){
    this.FilterForm.reset();
  }

  search(pageNo? : any) {
    if(this.FilterForm.valid){
      return new Promise((resolve, reject)=>{
        this.DataSource = [];
        let body = {
          PageNo: pageNo == 0?0:pageNo?pageNo:1,
          PlantCode: this.FilterForm.value.PlantCode,
          SupplierCode: this.supplierCode,
          ProductionLine: this.FilterForm.value.ProductionLine,
          ShopName: this.FilterForm.value.ShopName,
          DataFrom: this.FilterForm.value.DataFrom,
          Aggregates1: this.FilterForm.value.Aggregates
        };
        this.jitService.call("PTLiveData/GetPTLiveData", body).subscribe(res => {
          console.log(res);
          if (res.Message == "Success") {
            let dataSource = [];
            this.pageSize = res.DataCount ? res.DataCount : 200 || 0;
            for (const data of res.ResponseData) {
              dataSource.push({
                Barcode: data.BarcodeData,
                productionLine: data.ProductionLine,
                shopName: data.ShopName,
                DSN: data.DSN,
                SPN: data.SPN,
                BIN: data.BIN,
                modelCode: data.ModelCode,
                dateTime: moment(data.Dateandtime).format("DD-MM-YYYY hh:mm:s"),
                colour: data.Colour,
                remarks: data.Remarks,
                country: data.Country,
                paintOutScan: data.PaintoutScan,
                majorVariant: data.MajorVariant,
                VIN: data.VIN,
                partNo: data.PartNumber,
                partDescription: data.PartDescription,
                dataFrom: data.DataFrom,
                txnId: data.TransactionId
              });
            }
            this.DataSource = [...dataSource];
            
  
            this.updateGoto();
            if(res.ResponseData.length == dataSource.length){
              resolve();
            }
  
            console.log(res);
            console.log(this.FilterForm.get("SearchBy").value);
            if (
              this.FilterForm.get("SearchBy").value &&
              this.FilterForm.get("SearchByValue").value
            ) {
              this.DataSource = this.DataSource.filter(o =>
                o[this.FilterForm.get("SearchBy").value]
                  .toLowerCase()
                  .includes(
                    this.FilterForm.get("SearchByValue").value.toLowerCase()
                  )
              );
            }
            this.modalService.dismissAll();
    
            if (res.ResponseData.length > 0) {
              this.viewFilter = false;
            }else{
              this.toastr.error('Provided Data does not have sufficient data to be shown');
            }
          } else {
            reject('Some Error Occoured');
          }
        });
        
      })
    }else{
      this.toastr.error('Kindly Select A Plant');
    }
  }

  goToControl= new FormControl(1)
  goTo: number;
  pageNumbers = [];
  

  goToChange(value? : any) {
    this.paginator.pageIndex = this.goToControl.value;
    console.log(value)
    console.log(this.paginator.pageIndex)
    // this.goTo = this.paginator.pageIndex;
    this.search(this.paginator.pageIndex)
    // this.goToControl.setValue(this.paginator.pageIndex);
  }


  updateGoto() {
    this.goTo = 1;
    this.pageNumbers = [];
    var totalLength= this.pageSize / 10;
    var ceiledLength =Math.ceil(totalLength);
    console.log(ceiledLength);
    for (let i = 1; i <= ceiledLength; i++) {
      this.pageNumbers.push(i);
    }
  }

  // ---------------------------------Filter Modal------------------------------------------------------------

  selectAll() {
    console.log(this.selectAllAttribute);
    console.log(this.FilterAttributeForm);
    const setValue = this.selectAllAttribute.value == true ? false : true;

    this.selectAllAttribute.patchValue(setValue);
    Object.keys(this.FilterAttributeForm.controls).forEach(attr => {
      console.log(this.FilterAttributeForm.get(attr).value);
      this.FilterAttributeForm.get(attr).setValue(setValue);
    });
  }

  //-------------------------------------------Bar code-------------------------------------------------------------
  elementType = "svg";
  // values:any=['someValue12340987','tanvi'];
  format = "CODE39";
  lineColor = "#000000";
  width = 1;
  height = 100;
  displayValue = true;
  fontOptions = "";
  font = "monospace";
  textAlign = "center";
  textPosition = "bottom";
  textMargin = 2;
  fontSize = 20;
  background = "#ffffff";
  margin = 10;
  marginTop = 10;
  marginBottom = 10;
  marginLeft = 10;
  marginRight = 10;

  codeList: string[] = [
    "",
    "CODE128",
    "CODE128A",
    "CODE128B",
    "CODE128C",
    "UPC",
    "EAN8",
    "EAN5",
    "EAN2",
    "CODE39",
    "ITF14",
    "MSI",
    "MSI10",
    "MSI11",
    "MSI1010",
    "MSI1110",
    "pharmacode",
    "codabar"
  ];

  selectToPrintBarcode(event, element) {
    console.log(event);
    console.log(element);

    let index = this.barCodeNumberList.findIndex(o => o == element.partNo);
    if (index > -1) {
      this.barCodeNumberList.splice(index, 1);
      this.barCodeNumberTransaction.splice(index, 1);
    } else {
      this.barCodeNumberList.push(element.partNo);
      this.barCodeNumberTransaction.push(element);
    }
    
    console.log(this.barCodeNumberList);
    console.log(this.barCodeNumberTransaction);
  }

  printComponent(): void {
    let printContents, popupWin;
    printContents = document.getElementById("component1").innerHTML;
    popupWin = window.open("", "_blank", "top=0,left=0,height=100%,width=auto");
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
           <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();

    for(const txn of this.barCodeNumberTransaction){
      this.jitService.call('GetPTLiveData/UpdateManualPrintStatus',{SrNo: txn.txnId}).subscribe(res=>{
        console.log('GetPTLiveData/UpdateManualPrintStatus', 'Updated');
        console.log(res)
      })
    }
  }
}
