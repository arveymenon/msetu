import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonApiService } from '../services/common-api/common-api.service';
import { CommonUtilityService } from '../services/common/common-utility.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MySuggestionService } from '../services/MySuggestion/my-suggestion.service';
import * as XLSX from 'xlsx';
import { MatDialog, MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
// import { IDropdownSettings } from 'ng-multiselect-dropdown';
import * as CryptoJS from 'crypto-js';
import { DailoughBoxComponent } from '../modal/dailough-box/dailough-box.component';

@Component({
  selector: 'app-create-user-profile',
  templateUrl: './create-user-profile.component.html',
  styleUrls: ['./create-user-profile.component.scss']
})
export class CreateUserProfileComponent implements OnInit {
  createUser: FormGroup;
  uploaded_by_Excel: boolean = false;
  Excel_File: any = [];
  uploaded_by_Form: boolean = true;
  @ViewChild('form') form: any;
  @ViewChild('fileLabel') fileLabel: any;
  vendorDetails:any=[];
  Roles=[];
  username: any;
  RoleID: any;
  master_vendors=[];
  mas_vendore:any;
  dropdownSettings: any = {};
  vendors=[];
  supplier_roles=[];
  selectedItems=[];
  selectedItems1:any
  isDDLTouched:boolean=false;
  excel_headers=[];
  primary_roles=[];

  constructor(
    public dialog: MatDialog,
    public commonService: CommonUtilityService,
    public formBuilder: FormBuilder, public cs: MySuggestionService,
    private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer,private toastr: ToastrService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon('excelIcon',domSanitizer.bypassSecurityTrustResourceUrl('../assets/Icons/innerPages/Excel_White_Icon.svg'));
  }

  ngOnInit() {
    this.RoleID = localStorage.getItem('WTIwNWMxcFZiR3M9');
    this.RoleID = CryptoJS.AES.decrypt(this.RoleID,"").toString(CryptoJS.enc.Utf8);
    this.username = localStorage.getItem('WkZoT2JHTnNVblpoTWxaMQ==');
    this.username = CryptoJS.AES.decrypt(this.username,"").toString(CryptoJS.enc.Utf8)
    this.createForm();
    this.getVendorDetailsByUserName();
    this.getRoles();
    this.dropdownSettings={
      singleSelection: false,
      idField: 'RoleId',
      textField: 'RoleName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      // allowSearchFilter: true
    }
    if(this.RoleID == '7'){
      this.excel_headers = ["FirstName", "LastName", "WorkEmail", "MobileNumber", "Role","MainVendorCode","SupplierCode","UserName","PrimaryRole","IsOTPAllow"];
      this.getMasterVendors();
      // this.getRolesForSupplier(false);
    }
    if(this.RoleID != 7){
      this.getRolesForSupplier();
      this.excel_headers = ["UserName", "FirstName", "LastName", "WorkEmail", "MobileNumber","Role","PrimaryRole"];
    }
  }
OTPFlag:any = 'N';
  changeOTPFlag(flag) {
    if(flag)
    this.OTPFlag = 'Y';
    else
    this.OTPFlag = 'N';
  }
  getRolesForSupplier() {
  let Request= {
      "IsSupplierRole":true
  }
    this.cs.postMySuggestionData('CRUDMasterRole/GetSuppliersRoles', Request).
      subscribe(
        (res: any) => {
          if (!this.cs.isUndefinedORNull(res)) {
            this.supplier_roles = res.ResponseData;
          } else {
            this.cs.showSwapLoader = false;

              this.openDialogForError('Something went wrong please try again later!')
            // this.cs.showError('Something went wrong please try again later!')
          }
        },
        (error) => {
          this.cs.showSwapLoader = false;
          console.log(error.message);
        }
      )
  }

  getMasterVendors() {
    this.cs.postMySuggestionData('Vendor/GetMasterVendorDDL', '').
      subscribe(
        (res: any) => {
          if (!this.cs.isUndefinedORNull(res)) {
            this.master_vendors = res.ResponseData;
          } else {
            this.cs.showSwapLoader = false;
              this.openDialogForError('Something went wrong please try again later!')
          }
        },
        (error) => {
          this.cs.showSwapLoader = false;
          console.log(error.message);
        }
      )
  }

  getVendorsByMaster(event){
   let Request= {
      "MasterVendorCode" : event.value
  }
    this.cs.postMySuggestionData('VendorEmployee/GetVendorsByMasterVendor', Request).
    subscribe(
      (res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.vendors = res.ResponseData;
        } else {
          this.cs.showSwapLoader = false;
            this.openDialogForError('Something went wrong please try again later!')  
        }
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
      }
    )
  }

  createForm() {
    this.createUser = this.formBuilder.group({
      mas_vendore:['',this.RoleID == 7?Validators.required:[]],
      Vendore_Code:['',this.RoleID == 7?Validators.required:[]],
      user_name: ['', Validators.required],
      first_name: ['', Validators.required],
      work_phone: ['', [Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      work_email: ['', [Validators.required,Validators.email,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      last_name: ['', Validators.required],
      designation: ['', Validators.required],
      upload_excel: [{ value: '', disabled: true }, Validators.required],
    });
  }

  getRoles(){
    this.cs.postMySuggestionData('CRUDMasterRole/GetRoleDDL', '').
    subscribe(
      (res: any) => {
        this.Roles = res.ResponseData;
      },
      (error) => {
        console.log(error.message);
      }
    )
  }

  detectFiles(evt) {
    this.form.resetForm();
    let target: any = <any>(evt.target);
    var headers = [];
    for (let file of target.files) {
      let ext = file.name.split(".").pop().toLowerCase();
      let reader = new FileReader();
      reader.onload = (e: any) => {
        if (ext != 'xlsx') {
          this.openDialogForError("Kindly select excel file")
          // alert("Kindly select excel file");
          return false;
        }
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        var range = XLSX.utils.decode_range(ws['!ref']);
        var C, R = range.s.r;
        let excel_headers = this.excel_headers;
        excel_headers = excel_headers.map(v => v.toLowerCase().replace(/ /g, ""));
        for (C = range.s.c; C <= range.e.c; ++C) {
          var cell = ws[XLSX.utils.encode_cell({ c: C, r: R })]
          var hdr = "UNKNOWN " + C;
          if (cell && cell.t && excel_headers.includes(cell.v.toLowerCase().replace(/ /g, "")))
            hdr = XLSX.utils.format_cell(cell);
          else {
            this.openDialogForError('Kindly refer/used sample template and try to upload');
            return false;
          }
          headers.push(hdr);
        }
        this.Upload(target.files[0], evt,headers)
      }
      reader.readAsBinaryString(target.files[0]);
    }
  }

  Upload(data, evt,headers) {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      let arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      console.log(XLSX.utils.sheet_to_json(worksheet, { raw: true }));
      let array = [];
      array = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      if (array.length == 0)
        return this.openDialogForError("Kindly fill data in excel");
      else
        this.validateExcel(array, evt,headers);
    }
    fileReader.readAsArrayBuffer(data);
  }

  validateExcel(array, evt,headers) {
    for (let i = 0; i < this.excel_headers.length; i++) {
      for (let j = 0; j < array.length; j++) {
        if (this.cs.isUndefinedORNull(array[j][headers[i]])) {
          alert("Please enter values under " + this.excel_headers[i] + " at line number " + (j + 2));
          return false;
        }
      }
    }
    this.Excel_File = [];
    this.form.resetForm();
    this.uploaded_by_Excel = true;
    this.uploaded_by_Form = false;
    this.createUser.get('upload_excel').setValue(evt.target.files[0].name);
    this.Excel_File.push(evt.target.files[0]);
  }

  isFormvalid() {
    if (this.createUser.valid) {
      this.uploaded_by_Excel = false;
      this.Excel_File = [];
      this.createUser.get('upload_excel').setValue('');
      this.uploaded_by_Form = true;
    }
  }

  emailValidate() {
    this.uploaded_by_Excel = false;
  }

  numberOnly(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    console.log(charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }  
  return true
    
  }

  DownloadTemplate() {
    if(this.RoleID==7)
    window.open('../../assets/Adminregistration.xlsx', '_blank');
    else
    window.open('../../assets/Employee.xlsx', '_blank');
  }
  AcceptlettersOnly(event){
        const charCode = (event.which) ? event.which : event.keyCode;
        if(!(charCode >= 65 && charCode <=90) && !(charCode >=97&& charCode <=122) && (charCode != 32 && charCode != 0 ) ) { 
          event.preventDefault(); 
      }
 } 
 
  getVendorDetailsByUserName(){
    let postData={
      "UserName":this.username
    }

    this.cs.postMySuggestionData('VendorEmployee/VendorDetailsByUserName',postData).subscribe(
      (res: any) => {
        if (!this.cs.isUndefinedORNull(res)) {
          this.vendorDetails = res.ResponseData;
        } else {
          this.openDialogForError('Something went wrong please try again later!');
          // alert("Something went wrong!")
        }
      },
      (error) => {
        this.cs.showSwapLoader = false;
        console.log(error.message);
        this.form.resetForm();
      }
    )
    
  }

  onItemSelect(item: any) {
    this.selectedItems.push(item.RoleId);
    this.primary_roles.push(item);
  }

  onItemDeSelect(deselectedSID){
    this.selectedItems = this.selectedItems.filter(s => s != deselectedSID.RoleId);
    this.primary_roles = this.primary_roles.filter(id => id.RoleId != deselectedSID.RoleId)
    if(this.selectedItems.length==0)
    this.isDDLTouched = true;
  }

  submit() {
    let formData;
    let url;
    if (!this.createUser.valid && !this.uploaded_by_Excel && this.RoleID != 7) {
      return;
    }
    else if (this.uploaded_by_Form && !this.uploaded_by_Excel) {
      if(this.RoleID != 7 && this.selectedItems.length != 0){
        url = "VendorEmployee/VendorEmployeeRegistration";
        formData = {
          "userName": this.createUser.controls.user_name.value,
          "supplierCode": this.cs.isUndefinedORNull(this.vendorDetails)?'': this.vendorDetails.VendorCode,
          "designation": this.selectedItems.join(','),
          "PrimaryRole": this.createUser.controls.designation.value,
          "workEmail": this.createUser.controls.work_email.value,
          "workPhone": this.createUser.controls.work_phone.value,
          "cellPhone": this.createUser.controls.work_phone.value,
          "firstName": this.createUser.controls.first_name.value,
          "lastName": this.createUser.controls.last_name.value,
          "MainVendorCode":this.cs.isUndefinedORNull(this.vendorDetails)?'': this.vendorDetails.MainVendorCode,
          "preferredName": this.createUser.controls.first_name.value + ' ' + this.createUser.controls.last_name.value,
          "supplierToPlants": this.cs.isUndefinedORNull(this.vendorDetails)?'': this.vendorDetails.SuppliersToPlants,
          "supplierToSectors":this.cs.isUndefinedORNull(this.vendorDetails)?'':  this.vendorDetails.SuppliersToSectors,
          "cordysUserId":this.cs.isUndefinedORNull(this.vendorDetails)?'':  this.vendorDetails.CordysUserID,
          "commodity": (this.cs.isUndefinedORNull(this.vendorDetails)||this.cs.isUndefinedORNull(this.vendorDetails.Commodity))?'Other':this.vendorDetails.Commodity,
          "supplierName": this.cs.isUndefinedORNull(this.vendorDetails)?'': this.vendorDetails.SupplierName,
          "officeLocation": this.cs.isUndefinedORNull(this.vendorDetails)?'': this.vendorDetails.OfficeLocation,
          "createdBy": this.username,
          "createdDate": new Date(),
          "spsLocation": this.cs.isUndefinedORNull(this.vendorDetails)?'': this.vendorDetails.SPSLocation,
          "manager": this.cs.isUndefinedORNull(this.vendorDetails)?'': this.vendorDetails.Manager,
          "function": this.cs.isUndefinedORNull(this.vendorDetails)?'': this.vendorDetails.RoleName,
          "muspDepartment": this.cs.isUndefinedORNull(this.vendorDetails)?'': this.vendorDetails.RoleName,
          "isSupplier": "true",//this.createUser.controls.IsSupplier.value,
          "supplierPrivateSiteCollectionURL": '',
          "city": this.cs.isUndefinedORNull(this.vendorDetails)?'': this.vendorDetails.City,
          "state": this.cs.isUndefinedORNull(this.vendorDetails)?'': this.vendorDetails.State,
          "isActive": "true"
        }
      }
      else if(this.selectedItems.length != 0){
        url = "VendorEmployee/VendorRegistrationByAdmin";
        formData={
          "userName": this.createUser.controls.user_name.value,
          "supplierCode": this.createUser.controls.Vendore_Code.value,
          "designation": this.selectedItems.join(','),
          "PrimaryRole": this.createUser.controls.designation.value,
          "workEmail": this.createUser.controls.work_email.value,
          "workPhone":  this.createUser.controls.work_phone.value,
          "cellPhone": this.createUser.controls.work_phone.value,
          "firstName": this.createUser.controls.first_name.value,
          "lastName": this.createUser.controls.last_name.value,
          "preferredName": this.createUser.controls.first_name.value + ' ' + this.createUser.controls.last_name.value,
          "createdBy": this.username,
          "MainVendorCode": this.createUser.controls.mas_vendore.value,
          "IsOTPAllow" :this.OTPFlag
      }
      }
      else{
        return;
      }
    }
    else if (!this.uploaded_by_Form && this.uploaded_by_Excel) {
      // alert("Excel");
      if(this.RoleID ==7){
        url="VendorEmployee/BulkUploadVendorEmployeeByAdmin";
      formData = new FormData;
      formData.append('ExcelFile', this.Excel_File[0]);
      formData.append('CreatedBy', this.username);
      }else{
        url="VendorEmployee/BulkUploadVendorEmployeeDetails";
        formData = new FormData;
        formData.append('ExcelFile', this.Excel_File[0]);
        formData.append('CreatedBy', this.username);
        formData.append('VendorCode', this.vendorDetails.VendorCode);
      }
    }
    this.cs.postMySuggestionData(url, formData).
      subscribe(
        (res: any) => {
          if (!this.cs.isUndefinedORNull(res)) {

            if(res.ID == 1){
              this.cs.showSwapLoader = false;
              this.openDialogForError('Success');
              // this.cs.showAndHoldSuccess('Success',16000);
              this.selectedItems=[];
              this.selectedItems1='';
              this.primary_roles=[];
              this.isDDLTouched=false;
              this.form.resetForm();
            }
            else{
              this.cs.showSwapLoader = false;
              this.selectedItems=[];
              this.selectedItems1='';
              this.primary_roles=[];
              this.isDDLTouched=false;
              this.form.resetForm();
              if(res.Message =='Error')
              this.openDialogForError("Please fill all mandatory fields");
              else
              this.openDialogForError(res.Message);
              // setTimeout(()=>{ 
              //   this.cs.showAndHoldError(res.Message,8000);
              //  }, 5000)
              
              // // this.cs.showError(res.Message);
            }
          } else {
            this.cs.showSwapLoader = false;
            this.form.resetForm();
            this.openDialogForError(res.Message);
              // this.cs.showAndHoldError('Something went wrong please try again later!',8000);
          }
        },
        (error) => {
          this.cs.showSwapLoader = false;
          this.openDialogForError('Failed');
          // this.cs.showAndHoldError('Failed',16000);
          console.log(error.message);
          this.form.resetForm();
        }
      )

  }
  openDialogForError(value): void {
    const dialogRef = this.dialog.open(DailoughBoxComponent, {
      width: "300px",
      height: "130px",
      data: {
        messege: value       
          },
    });

    dialogRef.afterClosed().subscribe(async (data) => {
      // await this.updateEmployees(value)
      // this.activeModal.close(data);
      // this.getEmployeeApprovalData(1,'')
    });
  }

}
