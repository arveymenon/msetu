import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationAlertComponent } from './confirmation-alert/confirmation-alert.component';
import { MaterialModule } from 'src/material.module';

@NgModule({
  declarations: [
    ConfirmationAlertComponent
  ],
  providers:[
    ConfirmationAlertComponent
  ],
  entryComponents:[
    ConfirmationAlertComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ]
})
export class UtilityComponentsModule { }