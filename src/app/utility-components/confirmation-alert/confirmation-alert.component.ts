import { Component, OnInit, Input, ViewChild, Output, EventEmitter, TemplateRef, AfterViewInit, SimpleChanges, OnChanges, ElementRef, ViewChildren } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-confirmation-alert',
  templateUrl: './confirmation-alert.component.html',
  styleUrls: ['./confirmation-alert.component.sass']
})
export class ConfirmationAlertComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChild('#confirmationAlert') confirmationAlert: TemplateRef<any>;
  // response: Observable<any>;
  @Input('title') title: String;
  @Input('message') message: String;
  @Input('confirmButton') confirmButton: String;
  @Input('color') color: String;
  constructor(
    public modalService: NgbModal) {
   }
  
  ngOnInit() {
  }
  
  ngAfterViewInit() {
  }
  ngOnChanges() {
    console.log('Changes Happening components')
  }

  
  returningValue = new Observable(obs=>{
    obs.next(this.returningValue)
  })
  
  response(){
    return new Observable(obs=>{
      obs.next(this.returningValue)
    })
  }

  showAlert(title: String, message: String, confirmButton?: String, color?:String){
    // this.ngOnInit()
    console.log(this.confirmationAlert)
    if(title && message){
      this.title = title
      this.message = message
      this.confirmButton = color || 'Ok'
      this.color = color || 'white'
      console.log('Open Modal')
      // document.getElementById('clickable').click()
      this.modalService.open(ConfirmationAlertComponent, {
        size: "xl" as "lg",
        centered: true
      });
    } else {
     console.log('Kindly send title and message params') 
    }
  }

  click(val){
    this.returningValue = val
    // this.response()
    this.modalService.dismissAll()
  }


  getMessage(){ 
    return new Observable((obs)=> {
      console.log("getting")
      // obs.next(this.response)
    })
  }

}
