import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { CommonUtilityService } from "../services/common/common-utility.service";
import { ToastrService } from "ngx-toastr";
import {
  MatIconRegistry,
  MatPaginator,
  MatTableDataSource,
  MatSort,
} from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { MySuggestionService } from "../services/MySuggestion/my-suggestion.service";
import { object } from "@amcharts/amcharts4/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from "@angular/forms";
import { MyBusinessServiceService } from "../services/MyBusinessService/my-business-service.service";
import { IfStmt } from "@angular/compiler";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { any, insert } from "@amcharts/amcharts4/.internal/core/utils/Array";

@Component({
  selector: "app-cmsdocument",
  templateUrl: "./cmsdocument.component.html",
  styleUrls: ["./cmsdocument.component.scss"],
})
export class CmsdocumentComponent implements OnInit {
  menu: any = [];
  myBussinessMenu: any = [];
  showDiv: boolean = true;
  finalMenu: any = [];
  key: any = [];
  @ViewChild("form") form: any;
  childMenu: any = [];
  OESuppliesForm: any;
  isPolicyGuideLines: boolean = false;
  CommonForm: any;
  Files: any = [];
  isExpanded: boolean = false;
  Category_Name: string = "";
  activeChild = 0;
  Sidemenu_Data: any = [];
  folderData: any = [];
  showFilter: boolean = true;
  activeClass: any;
  mypolicy : any;
  editableMyPloicy: any
  isChild: boolean = true;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild("subMenu") subMenu: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = [
    "title",
    "folder",
    "signDoc",
    "docname",
    "date",
    "action",
  ];
  @ViewChild("input") input: any;
  showError: boolean = false;
  DataSource = [];
  show: boolean = true;
  showCol: boolean = false;
  titleShow: boolean = true;
  docShow: boolean = false;
  titleCol: boolean = false;
  uploadpdf: boolean = false;
  isUploadImage: boolean = false;
  isMonsoonUpdate: boolean = false;
  tooltipText: string;
  docCol: boolean = true;
  MyPolicyform: FormGroup;
  
  constructor(
    public cs: MyBusinessServiceService,
    public cs1: MySuggestionService,
    private _formBuilder: FormBuilder,
    public router: Router,
    public commonService: CommonUtilityService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private modalService: NgbModal,
    public toastr: ToastrService
  ) {
    this.commonService.changeIsAuthenticate(true);
    matIconRegistry.addSvgIcon(
      "trash",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/delete.svg"
      )
    );
    matIconRegistry.addSvgIcon(
      "Edit",
      domSanitizer.bypassSecurityTrustResourceUrl(
        "../assets/Icons/innerPages/Edit.svg"
      )
    );
  }

  ngOnInit() {
   
    this.createForm();
    this.Menu(1);
    this.createupdateform();
  
  }

  createForm() {
    this.CommonForm = this._formBuilder.group({
      Add_Title: ["", Validators.required],
      Upload_Document: ["", Validators.required],
      Upload_Document_signature: ["", Validators.required],
      Folder: [""],
    });
  }

  Menu(id): Promise<any> {
    return new Promise((resolve: any) => {
      if (id < 10) {
        this.cs1.showSwapLoader = true;
        this.getMenu(id).then((id: any) => {
          this.Menu(id + 1);
          if (id == 5) this.myBusinessMenu();
        });
      }
    });
  }

  getMenu(id: number): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs1.showSwapLoader = true;
      let postData = {
        BusinessCategoryID: id,
        BusinessSubCategoryID: "0",
        DocTypeId: "0",
      };
      this.cs
        .postDataToMyBussiness(
          "DocumentManagement/Get_DocumentManagementList",
          postData
        )
        .subscribe(
          (res: any) => {
            this.menu.push(res.ResponseData);
            this.cs1.showSwapLoader = false;
            resolve(id);
          },
          (error) => {
            this.cs1.showSwapLoader = false;
            this.cs1.showError(error.message);
            return;
          }
        );
    });
  }
  
  createupdateform(){
  this.MyPolicyform = this._formBuilder.group({
    Add_Title: ["", Validators.required],
    Uploaded_Document: ["", Validators.required]
  });
}

  getDocumentData(data, isChild) {
    this.cs1.showSwapLoader = true;
    let postData = {
      BusinessCategoryID: isChild
        ? data[0].BusinessCategoryID
        : data[0][0].BusinessCategoryID,
      BusinessSubCategoryID: isChild ? data[0].BusinessSubCategoryID : 0,
    };
    this.cs1
      .postMySuggestionData("DocumentManagement/Get_CMSDocs", postData)
      .subscribe(
        (res: any) => {
          this.cs1.showSwapLoader = false;
          this.DataSource = res.ResponseData;
          this.DataSource = [...this.DataSource];
          
        },
        (error) => {
          this.cs1.showSwapLoader = false;
          console.log(error.message);
        }
      );
  }

  getFolderData(data, isChild) {
    this.cs1.showSwapLoader = true;
    let folderData = {
      BusinessCategoryID: isChild
        ? data[0].BusinessCategoryID
        : data[0][0].BusinessCategoryID,
      BusinessSubCategoryID: isChild ? data[0].BusinessSubCategoryID : 0,
    };
    this.cs
      .postDataToMyBussiness("DocumentManagement/GetFolders", folderData)
      .subscribe(
        (res: any) => {
          this.cs1.showSwapLoader = false;
          this.folderData = res.ResponseData;
        },
        (error) => {
          this.cs1.showSwapLoader = false;
          this.cs1.showError(error.message);
          console.log(error.message);
        }
      );
  }

  validateForm(Form: FormGroup) {
    return new Promise((resolve: any, reject: any) => {
      var key: any = object.keys(Form.controls);
      for (let i = 0; i < key.length; i++) {
        const abstractcontrol = Form.controls[key[i]];
        if (abstractcontrol && !abstractcontrol.valid) {
          this.cs1.showError(
            "Please " +
              (key[i].includes("Upload_") || key[i].includes("Add_")
                ? " "
                : "Enter ") +
              this.cs.validationMsg[i].value
          );
          return;
        }
      }
      resolve();
    });
  }

  myBusinessMenu() {
    // this.subMenu._element.nativeElement.click();
  }

  deleteData(data) {
    let docID = {
      Doc_Id: data.Doc_Id,
    };
    this.cs
      .postDataToMyBussiness("DocumentManagement/Delete_CMSDoc", docID)
      .subscribe(
        (res: any) => {
          if (res.ResponseData[0].Status == "Deleted") {
            this.cs1.showSwapLoader = false;
            this.cs1.showSuccess(res.ResponseData[0].Status);
            this.getDocumentData(this.Sidemenu_Data, this.isChild);
          } else {
            this.cs1.showSwapLoader = false;
            this.cs1.showSuccess(res.Message);
          }
        },
        (error) => {
          this.cs1.showSwapLoader = false;
          this.cs1.showError(error.message);
          console.log(error.message);
        }
      );
  }

  navigate(data: any, isChild: boolean) {
    this.form.resetForm();
    if ((this.isChild = isChild)) {
      this.isExpanded = true;
      this.Category_Name = data.BusinessSubCategoryName;
      this.activeClass = data.BusinessSubCategoryID;
      if (data.BusinessCategoryID == 1 || data.BusinessCategoryID == 3)
        this.showFilter = true;
      else this.showFilter = false;
    } else {
      this.isExpanded = false;
      this.Category_Name = data[0].BusinessCategoryName;
      this.activeClass = data[0].BusinessCategoryID;
      if (data[0].BusinessCategoryID == 1 || data[0].BusinessCategoryID == 3)
        this.showFilter = true;
      else this.showFilter = false;
    }
    this.Sidemenu_Data = [];
    this.Files = [];
    this.CommonForm.reset();
    this.showError = false;
    this.Sidemenu_Data.push(data);
    this.getDocumentData(this.Sidemenu_Data, this.isChild);
    this.getFolderData(this.Sidemenu_Data, this.isChild);
    if (
      this.Category_Name == "OE Supplies" ||
      this.Category_Name == "My library"
    ) {
      this.show = true;
      this.showCol = false;
    } else {
      this.show = false;
      this.showCol = true;
    }
    if (
      this.Category_Name == "Escalation Matrix" ||
      this.Category_Name == "Monsoon Updates" ||
      this.Category_Name == "Code of Conduct"
    ) {
      this.titleShow = false;
      this.titleCol = true;
      if (this.Category_Name == "Code of Conduct") {
        this.docCol = false;
        this.docShow = true;
      } else {
        this.docCol = true;
        this.docShow = false;
      }
    } else {
      this.titleShow = true;
      this.titleCol = false;
    }
    if (this.Category_Name == "Escalation Matrix") {
      this.tooltipText = "File format should be .png/.jpg/.jpeg";
    } else if (this.Category_Name == "Monsoon Updates") {
      this.tooltipText = "File format should be .pdf";
    } else if (this.Category_Name == "Code of Conduct") {
      this.tooltipText = "File format should be .pdf";
    } else {
      this.tooltipText = "File format should be .png/.jpg/.jpeg/.pdf/.xlsx";
    }
  }
filename : any;
  detectFilesSign(event) {
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let ext = file.name.split(".").pop().toLowerCase();
        let reader = new FileReader();
        reader.onload = (e: any) => {
          // if(this.Category_Name == 'Escalation Matrix'){
          if (ext == "pdf") {
            this.Files.push(file);
            
            this.CommonForm.get("Upload_Document_signature").setValue(
              file.name
            );
          } else {
            // alert("Please select proper format");
            this.cs1.showError("Please select proper format");
            //  this.utilityProvider.presentToast('Please select file in proper format','4000','top')
          }
          // }
        };
        reader.readAsDataURL(file);
      }
    }
  }
  
  detectFiles(event: any) {
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let ext = file.name.split(".").pop().toLowerCase();
        let reader = new FileReader();
        reader.onload = (e: any) => {
          if (this.Category_Name == "Escalation Matrix") {
            if (ext == "jpg" || ext == "jpeg" || ext == "png") {
              this.Files.push(file);
              this.CommonForm.get("Upload_Document").setValue(file.name);
            } else {
              // alert("Please select proper format");
              this.cs1.showError("Please select proper format");
              //  this.utilityProvider.presentToast('Please select file in proper format','4000','top')
            }
          } else if (
            this.Category_Name == "Monsoon Updates" ||
            this.Category_Name == "Code of Conduct"
          ) {
            if (ext == "pdf") {
              this.Files.push(file);
              
              this.CommonForm.get("Upload_Document").setValue(file.name);
            } else {
              this.cs1.showError("Please select proper format");
              //  this.utilityProvider.presentToast('Please select file in proper format','4000','top')
            }
          } else {
            if (
              ext == "jpg" ||
              ext == "jpeg" ||
              ext == "png" ||
              ext == "pdf" ||
              ext == "xlsx"
            ) {
              this.Files.push(file);
               this.CommonForm.get("Upload_Document").setValue(file.name);
               this.MyPolicyform.get("Uploaded_Document").setValue(file.name);
               this.filename = file.name;
              //  alert(this.filename);
            } else {
              this.cs1.showError("Please select proper format");
              //  this.utilityProvider.presentToast('Please select file in proper format','4000','top')
            }
          }
        };
        reader.readAsDataURL(file);
      }
    }
  }

  showDocuments(element) {
    window.open(element.Doc_Link, "_blank");
  }

  postData(form: any, files: any) {
    if (
      this.Category_Name == "Escalation Matrix" ||
      this.Category_Name == "Monsoon Updates" ||
      this.Category_Name == "Code of Conduct"
    ) {
      this.CommonForm.controls["Add_Title"].clearValidators();
      this.CommonForm.controls["Add_Title"].updateValueAndValidity();
      if (this.Category_Name != "Code of Conduct") {
        this.CommonForm.controls["Upload_Document_signature"].clearValidators();
        this.CommonForm.controls[
          "Upload_Document_signature"
        ].updateValueAndValidity();
      }
      if (form.valid) {
        let formData: FormData = new FormData();
        let postData = {
          Doc_Name: files[0].name,
          Parent_Id: this.CommonForm.controls.Folder.value
            ? this.CommonForm.controls.Folder.value
            : 0,
          Doc_Type: "File",
          File_Type: files[0].type,
          Doc_Link: this.cs1.isUndefinedORNull(files[0].webkitRelativePath)
            ? ""
            : files[0].webkitRelativePath,
          File_Size: files[0].size,
          BusinessCategoryID: this.isChild
            ? this.Sidemenu_Data[0].BusinessCategoryID
            : this.Sidemenu_Data[0][0].BusinessCategoryID,
          BusinessSubCategoryID: this.isChild
            ? this.Sidemenu_Data[0].BusinessSubCategoryID
            : 0,
          Title: this.CommonForm.controls.Add_Title.value
            ? this.CommonForm.controls.Add_Title.value
            : 0,
        };
        formData.append("reqjson", JSON.stringify(postData));
        formData.append("File", this.Files[0]);
        this.cs
          .postDataToMyBussiness(
            "DocumentManagement/Upload_DocumentCMS",
            formData
          )
          .subscribe(
            (res: any) => {
              if (res.ResponseData[0].Status == "Insertted") {
                this.cs1.showSwapLoader = false;
                this.showError = false;
                this.cs1.showSuccess("File successfully added");
                this.getDocumentData(this.Sidemenu_Data, this.isChild);
                this.Files = [];
                this.form.resetForm();
              } else {
                this.cs1.showSwapLoader = false;
                this.cs1.showError(res.Message);
                this.Sidemenu_Data = [];
                this.Files = [];
                this.CommonForm.reset();
              }
            },
            (error) => {
              this.cs1.showSwapLoader = false;
              this.cs1.showError(error.message);
              console.log(error.message);
            }
          );
      }
    }
    if (
      this.Category_Name != "Escalation Matrix" &&
      this.Category_Name != "Monsoon Updates" &&
      this.Category_Name != "Code of Conduct"
    ) {
      this.CommonForm.controls["Upload_Document_signature"].clearValidators();
      this.CommonForm.controls[
        "Upload_Document_signature"
      ].updateValueAndValidity();
      if (!form.valid) {
        return;
      }
      if (form.valid) {
        let formData: FormData = new FormData();
        let postData = {
          Doc_Name: files[0].name,
          Parent_Id: this.CommonForm.controls.Folder.value
            ? this.CommonForm.controls.Folder.value
            : 0,
          Doc_Type: "File",
          File_Type: files[0].type,
          //  files[0].webkitRelativePath,
          Doc_Link: this.cs1.isUndefinedORNull(files[0].webkitRelativePath)
            ? ""
            : files[0].webkitRelativePath,
          File_Size: files[0].size,
          BusinessCategoryID: this.isChild
            ? this.Sidemenu_Data[0].BusinessCategoryID
            : this.Sidemenu_Data[0][0].BusinessCategoryID,
          BusinessSubCategoryID: this.isChild
            ? this.Sidemenu_Data[0].BusinessSubCategoryID
            : 0,
          Title: this.CommonForm.controls.Add_Title.value
            ? this.CommonForm.controls.Add_Title.value
            : 0,
        };
        formData.append("reqjson", JSON.stringify(postData));
        formData.append("File", this.Files[0]);
        this.cs
          .postDataToMyBussiness(
            "DocumentManagement/Upload_DocumentCMS",
            formData
          )
          .subscribe(
            (res: any) => {
              console.log(res);
              if (res.ResponseData[0].Status == "Insertted") {
                this.cs1.showSwapLoader = false;
                this.showError = false;
                this.cs1.showSuccess("File successfully added");
                this.getDocumentData(this.Sidemenu_Data, this.isChild);
                this.Files = [];
                this.form.resetForm();
              } else {
                this.cs1.showSwapLoader = false;
                this.cs1.showError(res.Message);
                this.Sidemenu_Data = [];
                this.Files = [];
                this.CommonForm.reset();
              }
            },
            (error) => {
              this.cs1.showSwapLoader = false;
              this.cs1.showError(error.message);
              console.log(error.message);
            }
          );
      }
    } else {
      this.showError = true;
    }
  }

  postCOCData(form, files, i) {
    this.CommonForm.controls["Add_Title"].clearValidators();
    this.CommonForm.controls["Add_Title"].updateValueAndValidity();
    if (form.valid) {
      let formData: FormData = new FormData();
      let postData = {
        Doc_Name: files[i].name,
        Parent_Id: this.CommonForm.controls.Folder.value
          ? this.CommonForm.controls.Folder.value
          : 0,
        Doc_Type: "File",
        File_Type: files[i].type,
        Doc_Link: this.cs1.isUndefinedORNull(files[i].webkitRelativePath)
          ? ""
          : files[i].webkitRelativePath,
        File_Size: files[i].size,
        BusinessCategoryID: this.isChild
          ? this.Sidemenu_Data[0].BusinessCategoryID
          : this.Sidemenu_Data[0][0].BusinessCategoryID,
        BusinessSubCategoryID: this.isChild
          ? this.Sidemenu_Data[0].BusinessSubCategoryID
          : 0,
        Title: this.CommonForm.controls.Add_Title.value
          ? this.CommonForm.controls.Add_Title.value
          : 0,
      };
      formData.append("reqjson", JSON.stringify(postData));
      formData.append("File", this.Files[i]);
      this.cs
        .postDataToMyBussiness(
          "DocumentManagement/Upload_DocumentCMS",
          formData
        )
        .subscribe(
          (res: any) => {
            if (res.ResponseData[0].Status == "Insertted") {
              this.cs1.showSwapLoader = false;
              this.showError = false;
              this.cs1.showSuccess("File successfully added");
              if (i == 0) this.postCOCData(form, files, 1);
              if (i == 1)
                this.getDocumentData(this.Sidemenu_Data, this.isChild);
              this.Files = [];
              this.form.resetForm();
            } else {
              this.cs1.showSwapLoader = false;
              this.cs1.showError(res.Message);
              this.Sidemenu_Data = [];
              this.Files = [];
              this.CommonForm.reset();
            }
          },
          (error) => {
            this.cs1.showSwapLoader = false;
            this.cs1.showError(error.message);
            console.log(error.message);
          }
        );
    }
  }

  RoleDocs = []; // Mapping List

  Roles = [];
  Role = new FormControl('',Validators.required);

  Docs = [];
  Document = new FormControl('',Validators.required);
  searchDocuments = new FormControl();

  editRoleDoc: any;

  openMappingModal(content) {
    this.modalService.open(content, {
      size: "xl" as "lg",
      centered: true,
      windowClass: "formModal",
    });
    this.GetRoleDocument(1);
    this.searchDocumentsName("");
    this.searchDocuments.valueChanges.subscribe((res) => {
      console.log(res);
      this.searchDocumentsName(res);
      // if(res.length < 1){
      // }else{
      //   this.searchDocumentsName(res)
      // }
    });
  }
  GetRoleDocument(PageNo) {
    this.cs
      .postDataToMyBussiness("DocumentManagement/GetAllRoleDocument", {
        pageNo: PageNo,
      })
      .subscribe((res) => {
        console.log(res);
        this.RoleDocs = res.ResponseData;
      });
    this.cs
      .postDataToMyBussiness("AdminCrudSurvey/Get_SurveyMasterDetails", {
        pageNo: PageNo,
      })
      .subscribe((res) => {
        console.log(res);
        this.Roles = res.ResponseData._GetRoleMaster;
      });
  }

  searchDocumentsName(docname) {
    this.cs
      .postDataToMyBussiness("DocumentManagement/GetMsetuDocument", {
        DocName: docname,
      })
      .subscribe((res) => {
        console.log(res);
        this.Docs = res.ResponseData;
      });
  }

  createMapping() {
    if(this.Role.valid && this.Document.valid){
      let Req = {
        roleID: this.Role.value,
        doc_Id: this.Document.value,
        createdBy: "admin",
      };
      this.cs
        .postDataToMyBussiness("DocumentManagement/InsertRoleDocument", Req)
        .subscribe((res) => {
          console.log(res);
          if(res.Id == 1){
            this.cs1.showSuccess("Mapping Inserted Successfully");
            this.GetRoleDocument(1);
            this.resetForm()
          }else{
            this.cs1.showError(res.Message)
          }
        });
    }
  }

  edit(roleDoc) {
    this.Document.setValue(roleDoc.Doc_Id);
    this.Role.setValue(roleDoc.RoleID);
    this.editRoleDoc = roleDoc;
  }

  updateForm() {
    if(this.Role.valid && this.Document.valid){
      let Req = {
        roleDocID: this.editRoleDoc.RoleDocID,
        roleID: this.Role.value,
        doc_Id: this.Document.value,
        createdBy: "admin",
      };
      this.cs
        .postDataToMyBussiness("DocumentManagement/UpdateRoleDocument", Req)
        .subscribe((res) => {
          console.log(res);
          if(res.Message != "Mapping exist"){
            this.cs1.showSuccess("Mapping Inserted Successfully");
            this.GetRoleDocument(1)
            this.resetForm()
            this.Docs = res.ResponseData;
          }else{

          }
        });
    }
  }

  

  // toggleActive(){
    
  // }

  updateStatus(RoleDoc) {
    console.log(RoleDoc)
    // if(this.Role.valid && this.Document.valid){
    let Req = RoleDoc;
    Req.IsActive = !Req.IsActive
    this.cs
      .postDataToMyBussiness("DocumentManagement/UpdateRoleDocument", Req)
      .subscribe((res) => {
        console.log(res);
      });
    // }
  }

  resetForm() {
    this.editRoleDoc = null;
    this.Document.reset();
    this.Role.reset();
  }

  closeModal() {
    this.modalService.dismissAll();
      this.editableMyPloicy = null
      this.MyPolicyform.reset();
      this.modalService.dismissAll();
  }
  closeModalmypolicy() {
    this.modalService.dismissAll();
      this.editableMyPloicy = null
      this.MyPolicyform.reset();
      this.modalService.dismissAll();
      this.getDocumentData
  }

  openModal(content, mypolicy: any) {
    console.log("=======mypolicy======",mypolicy);
    this.mypolicy = mypolicy;
    this.modalService.open(content, {
      size: "xl" as "lg",
      centered: true,
      backdrop: "static",
      windowClass: "formModal",
    });
    if (mypolicy) {
      this.editpolicy(mypolicy);
    }
  }
  editpolicy(mypolicy) {
    console.log("===================",this.mypolicy)
    this.editableMyPloicy = mypolicy
    // this.MyPolicyform = this._formBuilder.group({
    //   Add_Title: ['', Validators.required],
    //   Uploaded_Document: ['', Validators.required]
    // });
    this.MyPolicyform.get("Add_Title").setValue(this.editableMyPloicy.Title);
    this.MyPolicyform.get("Uploaded_Document").setValue(this.editableMyPloicy.Doc_Name);
  }
  

  async updateMyPloicy(form, files){
    let formData: FormData = new FormData();
    let postData ; 
    if(!this.Files[0]) {
      postData = {
        Doc_Name: null,
        Doc_Type: null,
        File_Size: null,
        Title: this.MyPolicyform.controls.Add_Title.value? this.MyPolicyform.controls.Add_Title.value: 0,
        Doc_Id: this.mypolicy.Doc_Id
    }
    formData.append("File", "");
  }
    else{
      postData = {
        Doc_Name: this.filename,
        Doc_Type: "File",
        File_Size: this.Files[0].size,
        Title: this.MyPolicyform.controls.Add_Title.value? this.MyPolicyform.controls.Add_Title.value: 0,
        Doc_Id: this.mypolicy.Doc_Id
      }
      formData.append("File", this.Files[0]);
    }
    formData.append("reqjson", JSON.stringify(postData));
    // formData.append("File", this.Files[i]);
    this.cs
      .postDataToMyBussiness("DocumentManagement/Update_DocumentCMS",formData).subscribe(
        (res: any) => {
          this.cs1.showSuccess("Updated Sucessfully")
            this.getDocumentData(this.Sidemenu_Data, this.isChild);
          this.Files = [];
        },
        (error) => {
          this.cs1.showSwapLoader = false;
          this.cs1.showError(error.message);
          console.log(error.message);
        }
      );
}
}
