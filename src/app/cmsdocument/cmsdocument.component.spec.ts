import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmsdocumentComponent } from './cmsdocument.component';

describe('CmsdocumentComponent', () => {
  let component: CmsdocumentComponent;
  let fixture: ComponentFixture<CmsdocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmsdocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmsdocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
