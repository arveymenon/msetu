import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewQuestionListModalComponent } from './view-question-list-modal.component';

describe('ViewQuestionListModalComponent', () => {
  let component: ViewQuestionListModalComponent;
  let fixture: ComponentFixture<ViewQuestionListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewQuestionListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewQuestionListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
