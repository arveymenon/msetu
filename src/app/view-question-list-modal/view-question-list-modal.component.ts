import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SurveyService } from '../services/survey/survey.service';

@Component({
  selector: 'app-view-question-list-modal',
  templateUrl: './view-question-list-modal.component.html',
  styleUrls: ['./view-question-list-modal.component.scss']
})
export class ViewQuestionListModalComponent implements OnInit {
  @Input() public surveyId;

  public questions: [];
  
  pageNo: any = 0;
  pageSize = 100;

  constructor(
    public modalService:NgbModal,
    public surveyService: SurveyService) { }

  ngOnInit() {

    this.getQuestions(0);
  }
  
  getQuestions(pageNo?: any){
    const reqjson = {
      SurveyID: this.surveyId,
      PageNo: pageNo + 1 || 1
    }
    console.log(reqjson);
    this.surveyService.getQuestions('AdminCrudSurvey/Get_Questions', reqjson).subscribe((resp: any) => {
    // this.printPartDatasource = resp.ResponseData;
      console.log(resp);
      if  (resp.Message == 'Success') {
        this.pageSize = resp.TotalCount || 100;
        this.questions = resp.ResponseData.reverse();
        if(resp.PageSize){
          this.pageSize = resp.PageSize
        }
        }
      }, (err) => {
  
      });
  }

  getPageChangeData($event){
    console.log($event)
    this.getQuestions($event.pageIndex)
  }

  closeModal(){
    this.modalService.dismissAll();
  }
}
